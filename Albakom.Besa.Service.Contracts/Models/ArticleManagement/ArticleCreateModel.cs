﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ArticleCreateModel : IArticle
    {
        #region Properties

        public String Name { get; set; }
        public Boolean ProductSoldByMeter { get; set; }

        #endregion

        #region Constructor

        public ArticleCreateModel()
        {

        }

        #endregion
    }
}
