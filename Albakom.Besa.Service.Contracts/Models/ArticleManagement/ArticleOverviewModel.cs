﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ArticleOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean ProductSoldByMeter { get; set; }
        public Boolean IsInUse { get; set; }

        #endregion

        #region Constructor

        public ArticleOverviewModel()
        {

        }

        #endregion
    }
}
