﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public interface IArticle
    {
         String Name { get; set; }
         Boolean ProductSoldByMeter { get; set; }
    }
}
