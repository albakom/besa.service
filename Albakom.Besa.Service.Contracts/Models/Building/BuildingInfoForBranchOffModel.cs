﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingInfoForBranchOffModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public GPSCoordinate Coordinate { get; set; }
        public String StreetName { get; set; }
        public ProjectAdapterColor MainColor { get; set; }
        public ProjectAdapterColor BranchColor { get; set; }

        #endregion
    }
}
