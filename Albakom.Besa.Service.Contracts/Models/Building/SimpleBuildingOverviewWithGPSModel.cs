﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleBuildingOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String StreetName { get; set; }
        public Int32 HouseholdUnits { get; set; }
        public Int32 CommercialUnits { get; set; }

        #endregion

        #region Constructor

        public SimpleBuildingOverviewModel()
        {

        }

        #endregion
    }
}
