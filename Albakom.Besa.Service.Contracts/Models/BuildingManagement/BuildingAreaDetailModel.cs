﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingAreaDetailModel
    {
        public Int32 Id { get; set; }
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }
        public String AreaName { get; set; }
        public IEnumerable<BuildingOverviewModel> Buildings { get; set; }
        public IEnumerable<StreetOverviewModel> Streets { get; set; }
        public IEnumerable<EmptySpaceOverviewModel> EmptySpaces { get; set; }
    }
}
