﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingAreaOverviewModel
    {
        public Int32 Id { get; set; }
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }
        public String AreaName { get; set; }
    }
}
