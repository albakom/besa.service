﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingCreateModel
    {
        public String StreetName { get; set; }
        public String StreetNumber { get; set; }
        public Int32 HouseholdUnits { get; set; }
        public Int32 CommercialUnits { get; set; }
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }
        public IEnumerable<Int32> FileIds { get; set; }
        public String Comment { get; set; }
    }
}
