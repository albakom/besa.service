﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingDeleteModel
    {
        public Int32 Id { get; set; }
        public String Comment { get; set; }
    }
}
