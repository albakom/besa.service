﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingOverviewModel
    {
        public Int32 Id { get; set; }
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }

    }
}
