﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingSplitModel
    {
        public Int32 Id { get; set; }
        public IEnumerable<GPSCoordinate> SplitCoordinates { get; set; }
    }
}
