﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class EmptySpaceCreateModel
    {
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }
        public String StreetName { get; set; }
        public IEnumerable<Int32> FileIds { get; set; }
        public String Comment { get; set; }
    }
}
