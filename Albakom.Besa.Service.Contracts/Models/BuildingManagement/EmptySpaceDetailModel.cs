﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class EmptySpaceDetailModel
    {
        public Int32 Id { get; set; }
        public IEnumerable<GPSCoordinate> Coordinates { get; set; }
        public String StreetName { get; set; }
        public IEnumerable<Int32> FileIds { get; set; }
        public String Comment { get; set; }
    }   
}
