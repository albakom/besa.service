﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class StreetEditModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }
}
