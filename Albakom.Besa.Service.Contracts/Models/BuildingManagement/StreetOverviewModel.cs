﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class StreetOverviewModel
    {
        public Int32 Id { get; set; }
        public String StreetName { get; set; }
    }
}
