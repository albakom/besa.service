﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CSVContactImportInfoModel  : CSVImportInfoModel<PersonProperty>
    {
        #region Properties

        public String DefaultCity { get; set; }
        public String DefaultPostalCode { get; set; }
        public Boolean UniqueSurnameAndLastname { get; set; }

        #endregion

        #region Constructor

        public CSVContactImportInfoModel()
        {

        }

        #endregion
    }
}
