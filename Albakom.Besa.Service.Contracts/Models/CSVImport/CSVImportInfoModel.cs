﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CSVImportInfoModel<TProperty> where TProperty : struct
    {
        #region Properties

        public IList<String[]> Lines { get; set; }
        public IDictionary<TProperty, Int32> Mappings { get; set; }
        
        #endregion

        #region Constructor

        public CSVImportInfoModel()
        {
            Mappings = new Dictionary<TProperty, Int32>();
            Lines = new List<String[]>();
        }

        #endregion
    }
}
