﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CSVImportProcedureResultModel
    {
        #region Properties

        public IEnumerable<CSVProcedureImportError> Errors { get; set; }
        public IEnumerable<Int32> JobIds { get; set; }
        public IEnumerable<Int32> ProcedureIds { get; set; }

        #endregion

        #region Constructor

        public CSVImportProcedureResultModel()
        {

        }

        #endregion
    }
}
