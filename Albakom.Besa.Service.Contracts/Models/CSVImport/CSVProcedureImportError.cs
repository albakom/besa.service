﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ErrorTypes
    {
        ContactNotReadable = 0,
        ContactNotFound = 1,
        StreetNotReadable = 2,
        StreetNumberNotReadable = 3,
        BuildingNotFound = 4,
        AppointmentNotReadable = 5,
    }

    public class CSVProcedureImportError
    {
        #region Properties

        public Int32 ItemIndex { get; set; }
        public ErrorTypes Type { get; set; }
        public String Description { get; set; }

        #endregion

        #region Constructor

        public CSVProcedureImportError()
        {

        }

        #endregion
    }
}
