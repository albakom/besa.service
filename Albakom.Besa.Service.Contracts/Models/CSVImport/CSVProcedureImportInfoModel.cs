﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CSVProcedureImportInfoModel : CSVImportInfoModel<ProcedureImportProperties>
    {
        #region Properties

        public Boolean RolebackIfErrorHappend { get; set; }
        public Boolean SkipError { get; set; }
        public String AsSoonAsPossibleString { get; set; }

        #endregion

        #region Constructor

        public CSVProcedureImportInfoModel()
        {

        }

        #endregion

    }
}
