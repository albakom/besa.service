﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum BranchableTypes
    {
        Cabinet = 1,
        Sleeve = 2,
    }

    public class SimpleBranchableWithGPSModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public BranchableTypes Type {get; set; }
        public GPSCoordinate Coordinate { get; set; }

        #endregion

        #region Constructor

        public SimpleBranchableWithGPSModel()
        {

        }

        #endregion
    }
}
