﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleCabinetOverviewModel
    {
        #region Constructor

        public Int32 Id { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public SimpleCabinetOverviewModel()
        {

        }

        #endregion
    }
}
