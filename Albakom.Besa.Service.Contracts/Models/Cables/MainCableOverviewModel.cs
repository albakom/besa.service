﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MainCableOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean JobExists { get; set; }

        #endregion

        #region Constructor

        public MainCableOverviewModel()
        {

        }

        #endregion
    }
}
