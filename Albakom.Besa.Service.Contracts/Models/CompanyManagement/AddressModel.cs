﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class AddressModel
    {
        #region Properties

        public String Street { get; set; }
        public String StreetNumber { get; set; }
        public String City { get; set; }
        public String PostalCode { get; set; }

        #endregion

        #region Constructor

        public AddressModel()
        {

        }

        #endregion
    }
}
