﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CompanyCreateModel : ICompanyModel
    {
        #region Properties

        public String Name { get; set; }
        public AddressModel Address { get; set; }
        public String Phone { get; set; }


        #endregion

        #region Constructor

        public CompanyCreateModel()
        {

        }

        #endregion
    }
}
