﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CompanyDetailModel : ICompanyModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Phone { get; set; }
        public AddressModel Address { get; set; }

        public IEnumerable<SimpleEmployeeModel> Employees { get; set; }

        #endregion

        #region Constructor

        public CompanyDetailModel()
        {
            Employees = new List<SimpleEmployeeModel>();
        }

        #endregion
    }
}
