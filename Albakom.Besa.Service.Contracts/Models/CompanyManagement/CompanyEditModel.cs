﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CompanyEditModel : ICompanyModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public AddressModel Address { get; set; }
        public String Phone { get; set; }

        public ICollection<Int32> AddedEmployees { get; set; }
        public ICollection<Int32> RemovedEmployees { get; set; }

        #endregion

        #region Constructor

        public CompanyEditModel()
        {

        }

        #endregion
    }
}
