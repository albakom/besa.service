﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CompanyOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 EmployeeCount { get; set; }

        #endregion

        #region Constructor



        #endregion
    }
}
