﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public interface ICompanyModel
    {
        #region Properties

         String Name { get; set; }
         AddressModel Address { get; set; }
         String Phone { get; set; }

        #endregion
    }
}
