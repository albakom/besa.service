﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleEmployeeModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Surname { get; set; }
        public String Lastname { get; set; }

        #endregion

        #region Constructor

        public SimpleEmployeeModel()
        {

        }

        #endregion
    }
}
