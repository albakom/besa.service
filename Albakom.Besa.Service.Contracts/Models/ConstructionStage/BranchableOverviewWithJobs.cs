﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BranchableOverviewWithJobs<T> where T : BoundJobOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Coordinate { get; set; }
        public Double DoingPercentage { get; set; }

        public IEnumerable<T> Jobs { get; set; }

        #endregion

        #region Constructor

        public BranchableOverviewWithJobs()
        {
            Jobs = new List<T>();
        }

        #endregion
    }
}
