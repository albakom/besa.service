﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BranchableSpliceOverviewModel
    {
        #region Properties

        public SimpleBranchableWithGPSModel Branchable { get; set; }
        public IEnumerable<SpliceEntryModel> Splices { get; set; }

        #endregion

        #region Constructor

        public BranchableSpliceOverviewModel()
        {
            Splices = new List<SpliceEntryModel>();
        }

        #endregion
    }
}
