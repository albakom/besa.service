﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageCreateModel
    {
        #region Properties

        public String Name { get; set; }
        public Boolean CreateJobs { get; set; }
        public IEnumerable<Int32> Cabinets { get; set; }
        public IEnumerable<Int32> Sleeves { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageCreateModel()
        {

        }

        #endregion
    }
}
