﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageDetailModel<T> where T: BoundJobOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Double DoingPercentage { get; set; }
        public IEnumerable<BranchableOverviewWithJobs<T>> Branchables { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageDetailModel()
        {
            Branchables = new List<BranchableOverviewWithJobs<T>>();
        }

        #endregion
    }
}
