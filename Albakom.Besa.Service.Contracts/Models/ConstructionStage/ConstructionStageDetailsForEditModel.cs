﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageDetailsForEditModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public IEnumerable<SimpleCabinetOverviewModel> Branchables { get; set; }
        public IDictionary<Int32,IEnumerable<SimpleBuildingOverviewModel>> Buildings { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageDetailsForEditModel()
        {
            Branchables = new List<SimpleCabinetOverviewModel>();
            Buildings = new Dictionary<Int32, IEnumerable<SimpleBuildingOverviewModel>>();
        }

        #endregion
    }
}
