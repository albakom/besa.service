﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageForProjectManagementOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 BuildingsWithoutBranchOffJobs { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageForProjectManagementOverviewModel()
        {

        }

        #endregion
    }
}
