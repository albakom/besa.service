﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageOverviewForJobs
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public IEnumerable<SimpleCabinetOverviewModel> Branchables { get; set; }
        public Double DoingPercentage { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageOverviewForJobs()
        {

        }

        #endregion
    }
}
