﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Double DoingPercentage { get; set; }
        public IEnumerable<StreetSummeryViewModel> Streets { get; set; }
        public Int32 HouseholdsUnits { get; set; }
        public Int32 CommercialUnits { get; set; }
        public Int32 CabinetAmount { get; set; }
        public Int32 SleeveAmount { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageOverviewModel()
        {

        }

        #endregion

    }
}
