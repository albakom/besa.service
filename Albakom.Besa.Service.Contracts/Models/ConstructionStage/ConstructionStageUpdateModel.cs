﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ConstructionStageUpdateModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean CreateBranchOffJobs { get; set; }
        public IEnumerable<Int32> AddedBranchables { get; set; }
        public IEnumerable<Int32> RemovedBranchables { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageUpdateModel()
        {
            AddedBranchables = new List<Int32>();
            RemovedBranchables = new List<Int32>();
        }

        #endregion


    }
}
