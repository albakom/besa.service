﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FiberSpliceCreateModel
    {
        #region Properties

        public ProjectAdapterSpliceModel SpliceModel { get; set; }
        public Int32? FirstFiberId { get; set; }
        public Int32? SecondFiberId { get; set; }
        public Int32 BranchableId { get; set; }

        #endregion

        #region Constructor

        public FiberSpliceCreateModel()
        {

        }

        #endregion
    }
}
