﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class GetDataFromAdapterResult
    {
        #region Properties

        public Int32 BuildingAmount { get; set; }
        public Int32 CabinentAmount { get; set; }
        public Int32 SleevesAmount { get; set; }

        #endregion

        #region Constructor

        public GetDataFromAdapterResult()
        {

        }

        #endregion
    }
}
