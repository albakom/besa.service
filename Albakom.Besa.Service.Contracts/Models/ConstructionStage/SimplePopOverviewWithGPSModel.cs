﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimplePopOverviewWithGPSModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Location { get; set; }

        #endregion

        #region Constructor

        public SimplePopOverviewWithGPSModel()
        {

        }

        #endregion
    }
}
