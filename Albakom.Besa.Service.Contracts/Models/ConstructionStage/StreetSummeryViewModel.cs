﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class StreetSummeryViewModel
    {
        #region Properties

        public String StreetName { get; set; }
        public String Start { get; set; }
        public String End { get; set; }

        #endregion

        #region Constructor

        public StreetSummeryViewModel()
        {

        }

        #endregion
    }
}
