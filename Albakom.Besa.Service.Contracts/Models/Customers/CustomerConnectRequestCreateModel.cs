﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CustomerConnectRequestCreateModel
    {
        #region Properties

        public Boolean OnlyHouseConnection { get; set; }
        public Int32? FlatId { get; set; }
        public Int32? BuildingId { get; set; }
        public Int32? CustomerPersonId { get; set; }
        public Int32? PropertyOwnerPersonId { get; set; }
        public Int32? ArchitectPersonId { get; set; }
        public Int32? WorkmanPersonId { get; set; } 
     
        public String DescriptionForHouseConnection { get; set; }

        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectioOfOtherMediaRequired { get; set; }

        public ExplainedBooleanModel SubscriberEndpointNearConnectionPoint { get; set; }

        public Double? DuctAmount { get; set; }
        public Double? SubscriberEndpointLength { get; set; }

        public ExplainedBooleanModel ActivePointNearSubscriberEndpoint { get; set; }
        public ExplainedBooleanModel PowerForActiveEquipment { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        public Boolean InformSalesAfterFinish { get; set; }
        public Boolean ActivationAsSoonAsPossible { get; set; }

        #endregion

        #region Constructor

        public CustomerConnectRequestCreateModel()
        {
          
        }

        #endregion
    }
}
