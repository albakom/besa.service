﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CustomerInfoModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Lastname { get; set; }

        #endregion

        #region Constructor

        public CustomerInfoModel()
        {

        }

        #endregion
    }
}
