﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum PersonTypes
    {
        Male,
        Female,
    }

    public class PersonInfo
    {
        #region Properties

        public Int32 Id { get; set; }
        public PersonTypes Type { get; set; }
        public String CompanyName { get; set; }
        public String Surname { get; set; }
        public String Lastname { get; set; }
        public AddressModel Address { get; set; }
        public String EmailAddress { get; set; }
        public String Phone { get; set; }

        #endregion

        #region Constructor

        public PersonInfo()
        {
            Address = new AddressModel();
        }

        #endregion
    }
}
