﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class RequestFilterModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public Int32? OwnerContactId { get; set; }
        public Boolean? OnlyConnection { get; set; }

        #endregion

        #region Constructor

        public RequestFilterModel()
        {

        }

        #endregion
    }
}
