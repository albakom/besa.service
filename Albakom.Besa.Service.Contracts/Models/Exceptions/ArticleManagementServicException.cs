﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ArticleManagementServicExceptionReasons
    {
        NotFound,
        InvalidData,
        NoData,
        DeleteNeedToBeForced,
        UserNotFound,
    }

    public class ArticleManagementServicException : BesaServiceException
    {
        #region Properties

        public ArticleManagementServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ArticleManagementServicException(ArticleManagementServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ArticleManagementServicException(ArticleManagementServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
