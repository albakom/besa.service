﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public abstract class BesaServiceException : Exception
    {
        #region Properties

        public String Description { get; protected set; }


        #endregion

        public BesaServiceException() { }
        public BesaServiceException(string message) : base(message) { }
        public BesaServiceException(string message, Exception inner) : base(message, inner) { }
        protected BesaServiceException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
   
}
