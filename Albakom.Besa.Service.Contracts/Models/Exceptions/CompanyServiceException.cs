﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ComponyServiceExceptionReasons
    {
        CreateModelIsNull,
        InvalidEditModel,
        EditModelIsNull,
        NotFound,
        UserNotCompanyMember,
        UserAlreadyMember,
        UserNotFound
    }

    public class CompanyServiceException : BesaServiceException
    {
        #region Properties

        public ComponyServiceExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public CompanyServiceException(ComponyServiceExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public CompanyServiceException(ComponyServiceExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
