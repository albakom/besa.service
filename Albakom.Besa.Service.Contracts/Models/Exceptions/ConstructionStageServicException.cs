﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ConstructionStageServicExceptionReasons
    {
        ModelIsNull,
        InvalidCreateModel,
        NotFound,
        DataUpdateInvalid,
        InvalidDataFromAdapter,
        InvalidBindJobModel,
        InvalidOperation,
        NameInUse,
        InvalidFilterModel,
        UserNotFound
    }

    public class ConstructionStageServicException : BesaServiceException
    {
        #region Properties

        public ConstructionStageServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageServicException(ConstructionStageServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ConstructionStageServicException(ConstructionStageServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
