﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum CustomerServicExceptionReasons
    {
        NotFound,
        InvalidData,
        NoData,
        DeleteNeedToBeForced,
        InvalidOperation,
        InvalidImportData,
        UserNotFound,
    }

    public class CustomerServicException : BesaServiceException
    {
        #region Properties

        public CustomerServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public CustomerServicException(CustomerServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public CustomerServicException(CustomerServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
