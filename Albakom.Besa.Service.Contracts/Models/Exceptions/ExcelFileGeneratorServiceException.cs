﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ExcelFileGeneratorServicExceptionReasons
    {
        UnableToGetSpliceResultFromService
    }

    public class ExcelFileGeneratorServiceException : BesaServiceException
    {
        #region Properties

        public ExcelFileGeneratorServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ExcelFileGeneratorServiceException(ExcelFileGeneratorServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ExcelFileGeneratorServiceException(ExcelFileGeneratorServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
