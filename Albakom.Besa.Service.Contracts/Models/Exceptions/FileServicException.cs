﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum FileServicExceptionReasons
    {
        NotFound,
        NoModel,
        InvalidModel,
        InvalidFileData,
        FileNotSaved,
        InvalidParameter,
        NotImplemented,
        UserNotFound,
    }

    public class FileServicException : BesaServiceException
    {
        #region Properties

        public FileServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public FileServicException(FileServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public FileServicException(FileServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
