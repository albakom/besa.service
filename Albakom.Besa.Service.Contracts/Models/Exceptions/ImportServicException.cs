﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ImportExceptionReasons
    {
        NoData,
        NoImportInfo,
        InvalidImportModel
    }

    public class ImportServicException : BesaServiceException
    {
        #region Properties

        public ImportExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ImportServicException(ImportExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ImportServicException(ImportExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
