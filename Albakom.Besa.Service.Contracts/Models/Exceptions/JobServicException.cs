﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum JobServicExceptionReasons
    {
        NotFound,
        NoModel,
        InvalidFinishedModel,
        InvalidAcknowlegdeModel,
        InvalidOperation,
        InvalidConnectBuildingJobCreateModel,
        InvalidBindJobModel,
        WrongRole,
        InvalidData,
        InvalidAmount,
        InvalidFilterModel,
    }

    public class JobServicException : BesaServiceException
    {
        #region Properties

        public JobServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public JobServicException(JobServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public JobServicException(JobServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
