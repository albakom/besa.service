﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum MessageServiceExceptionReasons
    {
        UserNotFound,
        InvalidFilter,
        InvalidOperation,
        NotFound,
    }

    public class MessageServiceException : BesaServiceException
    {
        #region Properties

        public MessageServiceExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public MessageServiceException(MessageServiceExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public MessageServiceException(MessageServiceExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
