﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ProcedureServicExceptionReasons
    {
        NotFound,
        NoFilter,
        InvalidFilterModel
    }

    public class ProcedureServiceException : BesaServiceException
    {
        #region Properties

        public ProcedureServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ProcedureServiceException(ProcedureServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ProcedureServiceException(ProcedureServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
