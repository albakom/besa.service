﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum ProtocolServicExceptionReasons
    {
        UserNotFound,
        NoFilterModel,
        InvalidFilterModel
    }

    public class ProtocolServiceException : BesaServiceException
    {
        #region Properties

        public ProtocolServicExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public ProtocolServiceException(ProtocolServicExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public ProtocolServiceException(ProtocolServicExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
