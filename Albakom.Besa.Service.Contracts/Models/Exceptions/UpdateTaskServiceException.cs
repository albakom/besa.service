﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum UpdateTaskServiceExceptionReasons
    {
        UserNotFound,
        ElementNotFound,
        ModelIsNull,
        InvalidOperation,
        TaskTypeNotFound,
        TaskNotFound,
        NotFound,
        InvalidModel
    }

    public class UpdateTaskServiceException : BesaServiceException
    {
        #region Properties

        public UpdateTaskServiceExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskServiceException(UpdateTaskServiceExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public UpdateTaskServiceException(UpdateTaskServiceExceptionReasons reasons) : this(reasons, String.Empty)
        {

        }

        #endregion
    }
}
