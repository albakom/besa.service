﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models.Exceptions
{
    public enum UserServiceExceptionReasons
    {
        UserNotFound,
        UserAcccessRequestIsNull,
        InvalidAccessRequestModel,
        AccessRequestAlreadyExists,
        NoAuthId,
        AccessRequestNotFound,
        GrantAccessIsNull,
        InvalidGrantAccessModel,
        UserAlreadyExists,
        UserAlreadySuspended,
        EditModelIsNull,
        EditModelIsInvalid,
        BelongsToNoCompany,
    }

    public class UserServiceException : BesaServiceException
    {
        #region Properties

        public UserServiceExceptionReasons Reason { get; set; }

        #endregion

        #region Constructor

        public UserServiceException(UserServiceExceptionReasons reasons, String description) : base($"unable to complete service operation. reason {reasons}. details: {description}")
        {
            Description = description;
            Reason = reasons;
        }

        public UserServiceException(UserServiceExceptionReasons reasons) : this(reasons,String.Empty)
        {

        }

        #endregion
    }
}
