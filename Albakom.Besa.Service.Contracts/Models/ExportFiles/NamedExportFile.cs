﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class NamedExportFile
    {
        #region Properties

        public Byte[] Data { get; set; }
        public String Name { get; set; }
        public String MimeType { get; set; }

        #endregion

        #region Constructor

        public NamedExportFile()
        {

        }

        #endregion
    }
}
