﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum FileAccessObjects
    {
        Building = 1,
        BranchOffJob = 2,
        HouseConnectionJob = 3,
        InjectJob = 4,
        PrepareBranchableJob = 5,
        SpliceInBranchableJob = 6,
        SpliceInPopJob = 7,
        ActivationJob = 8,
    }

    public class FileAccessModel
    {
        #region Properties

        public Int32 ObjectId { get; set; }
        public FileAccessObjects ObjectType { get; set; }

        #endregion

        #region Constructor

        public FileAccessModel()
        {

        }

        #endregion
    }
}
