﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FileAccessWithIdModel
    {
        #region Properties

        public Int32 ObjectId { get; set; }
        public FileAccessObjects ObjectType { get; set; }
        public Int32 Id { get; set; }

        #endregion

        #region Constructor

        public FileAccessWithIdModel()
        {

        }

        #endregion
    }
}
