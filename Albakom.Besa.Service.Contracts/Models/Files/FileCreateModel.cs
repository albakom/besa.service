﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FileCreateModel
    {
        #region Properties

        public String Name { get; set; }
        public String Extention { get; set; }
        public String MimeType { get; set; }
        public Int32 Size { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public String StorageUrl { get; set; }
        public FileTypes Type { get; set; }

        #endregion

        #region Constructor

        public FileCreateModel()
        {

        }

        #endregion
    }
}
