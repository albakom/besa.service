﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FileEditModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public IEnumerable<FileAccessModel> AccessList { get; set; }

        #endregion

        #region Constructor

        public FileEditModel()
        {
            AccessList = new List<FileAccessModel>();
        }

        #endregion
    }
}
