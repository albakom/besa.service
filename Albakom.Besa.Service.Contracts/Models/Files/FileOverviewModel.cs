﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FileOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Extention { get; set; }
        public FileTypes Type { get; set; }
        public Int32 Size { get; set; }

        #endregion

        #region Constructor

        public FileOverviewModel()
        {

        }

        #endregion
    }
}
