﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum FileTypes
    {
        Unknown,
        Image,
        PDF,
        Excel,
        Audio,
        Video,
        Other,
        CSV,
        Word,
        Calender,
        JSON,
        Powerpoint,
        Visio,
        Archiv,
        Text,
        XML,
    }
}
