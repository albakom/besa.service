﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FileWithStreamModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Extention { get; set; }
        public String MimeType { get; set; }
        public Stream Data { get; set; }

        #endregion

        #region Constructor

        public FileWithStreamModel()
        {

        }

        #endregion
    }
}
