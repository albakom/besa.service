﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleFlatOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public Int32 BuildingId { get; set; }
        public String Number { get; set; }
        public String Description { get; set; }
        public String Floor { get; set; }

        #endregion

        #region Constructor

        public SimpleFlatOverviewModel()
        {

        }

        #endregion
    }
}
