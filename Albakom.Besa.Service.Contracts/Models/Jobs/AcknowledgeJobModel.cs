﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class AcknowledgeJobModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String UserAuthId { get; set; }
        public DateTimeOffset Timestamp { get; set; }

        #endregion

        #region Constructor

        public AcknowledgeJobModel()
        {

        }

        #endregion
    }
}
