﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ActivationJobIpAddressRequirements
    {
        None = 1,
        Slash29 = 2,
        Slash28 = 3,
        Slash27 = 3,
    }

    public class ActivationJobCreateModel
    {
        #region Properties

        public Int32 FlatId { get; set; }
        public Int32 CustomerContactId { get; set; }
        public Int32? DeviceId { get; set; }
        public Boolean UnchagedIpAdress { get; set; }
        public ActivationJobIpAddressRequirements AddressRequirement { get; set; }

        #endregion

        #region Constructor

        public ActivationJobCreateModel()
        {

        }

        #endregion
    }
}
