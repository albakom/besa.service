﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ActivationJobDetailModel : JobDetailModel<FinishActivationJobDetailModel>
    {
        #region Properties

        public PersonInfo Customer { get; set; }
        public SimpleBuildingOverviewWithGPSModel Building { get; set; }
        public SimpleFlatOverviewModel Flat { get; set; }
        public IEnumerable<FiberEndpointModel> Endpoints { get; set; }
        public SimplePopOverviewWithGPSModel Pop { get; set; }

        #endregion

        #region Constructor

        public ActivationJobDetailModel()
        {

        }

        #endregion

    }
}
