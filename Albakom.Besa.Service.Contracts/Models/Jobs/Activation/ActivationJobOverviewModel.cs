﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ActivationJobOverviewModel : BoundJobOverviewModel
    {
        #region Properties

        public SimpleBuildingOverviewModel Building { get; set; }
        public PersonInfo Customer { get; set; }
        public String PoPName { get; set; }

        #endregion

        #region Constructor

        public ActivationJobOverviewModel()
        {

        }

        #endregion
    }
}
