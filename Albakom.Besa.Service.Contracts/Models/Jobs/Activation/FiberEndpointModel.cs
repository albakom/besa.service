﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FiberEndpointModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String ModuleName { get; set; }
        public String RowNumber { get; set; }
        public String ColumnNumber { get; set; }

        public String Caption { get; set; }

        #endregion

        #region Constructor

        public FiberEndpointModel()
        {

        }

        #endregion
    }
}
