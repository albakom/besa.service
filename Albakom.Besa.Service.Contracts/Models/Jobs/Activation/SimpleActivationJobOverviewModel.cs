﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleActivationJobOverviewModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public PersonInfo Customer { get; set; }
        public SimpleFlatOverviewModel Flat { get; set; }
        public SimpleBuildingOverviewModel Building { get; set; }

        #endregion

        #region Constructor

        public SimpleActivationJobOverviewModel()
        {

        }

        #endregion
    }
}
