﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BindJobModel
    {
        #region Properties

        public IEnumerable<Int32> JobIds { get; set; }
        public Int32 CompanyId { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public BindJobModel()
        {

        }

        #endregion
    }
}
