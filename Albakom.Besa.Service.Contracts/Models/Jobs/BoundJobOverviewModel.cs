﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class BoundJobOverviewModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public JobStates State { get; set; }
        public String BoundedCompanyName { get; set; }

        #endregion
    }
}
