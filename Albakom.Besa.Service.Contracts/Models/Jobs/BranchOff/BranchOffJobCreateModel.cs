﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BranchOffJobCreateModel
    {
        #region Properites

        public Int32 BuildingId { get; set; }

        #endregion

        #region Constructor

        public BranchOffJobCreateModel()
        {

        }

        #endregion
    }
}
