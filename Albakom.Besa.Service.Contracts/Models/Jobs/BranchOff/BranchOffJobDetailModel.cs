﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BranchOffJobDetailModel : JobDetailModel<FinishBranchOffDetailModel>
    {
        #region Properties

        public BuildingInfoForBranchOffModel BuildingInfo { get; set; }

        #endregion

        #region Constructor

        public BranchOffJobDetailModel()
        {

        }

        #endregion
    }
}
