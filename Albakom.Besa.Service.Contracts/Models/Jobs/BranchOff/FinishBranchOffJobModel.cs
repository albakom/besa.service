﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FinishBranchOffJobModel : FinishedJobModel
    {
        #region Properties

        public ExplainedBooleanModel DuctChanged { get; set; }

        #endregion

        #region Constructor

        public FinishBranchOffJobModel() : base()
        {
            DuctChanged = new ExplainedBooleanModel();
        }
        
        #endregion
    }
}
