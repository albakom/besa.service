﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CollectionJobDetailsModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public DateTimeOffset End { get; set; }
        public Double DoingPercentage { get; set; }

        public IEnumerable<SimpleJobOverview> Jobs { get; set; }
        public JobStates State { get; set; }

        #endregion

        #region Constructor

        public CollectionJobDetailsModel()
        {
            Jobs = new List<SimpleJobOverview>();
        }

        #endregion
    }
}
