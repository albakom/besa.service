﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FinishJobAdministrativeModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String UserAuthId { get; set; }
        public DateTimeOffset? FinishedAt { get; set; }
        public Boolean CreateNextJobInPipeLine { get; set; }

        #endregion

        #region Constructor

        public FinishJobAdministrativeModel()
        {

        }

        #endregion
    }
}
