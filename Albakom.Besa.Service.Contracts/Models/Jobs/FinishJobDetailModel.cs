﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class FinishJobDetailModel 
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String Comment { get; set; }
        public ExplainedBooleanModel ProblemHappend { get; set; }
        public UserOverviewModel FinishedBy { get; set; }
        public DateTimeOffset FinishedAt { get; set; }
        public IEnumerable<FileOverviewModel> Files { get; set; }
        public UserOverviewModel AcceptedBy { get; set; }
        public DateTimeOffset? AcceptedAt { get; set; }

        #endregion

        #region Constructor

        public FinishJobDetailModel()
        {

        }

        #endregion
    }
}
