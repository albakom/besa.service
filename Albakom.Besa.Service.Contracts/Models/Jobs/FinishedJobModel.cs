﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class FinishedJobModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String Comment { get; set; }
        public String UserAuthId { get; set; }
        public IEnumerable<Int32> FileIds { get; set; }

        public ExplainedBooleanModel ProblemHappend { get; set; }

        public DateTimeOffset Timestamp { get; set; }
        
        #endregion

        #region Constructor

        public FinishedJobModel()
        {
            FileIds = new List<Int32>();
            ProblemHappend = new ExplainedBooleanModel();
        }

        #endregion
    }
}
