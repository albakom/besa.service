﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FinishBuildingConnenctionDetailModel : FinishJobDetailModel
    {
        #region Properties

        public ExplainedBooleanModel DuctChanged { get; set; }

        #endregion

        #region Constructor

        public FinishBuildingConnenctionDetailModel()
        {

        }

        #endregion
    }
}
