﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class HouseConnenctionJobCreateModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public Boolean OnlyHouseConnection { get; set; }
        public Int32? CustomerContactId { get; set; }
        public Int32 OwnerContactId { get; set; }
        public Int32? WorkmanContactId { get; set; }
        public Int32? ArchitectContactId { get; set; }

        public String Description { get; set; }
        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectionOfOtherMediaRequired { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        public Boolean CreateProcedureIfNotAlreadyExists { get; set; }

        public Int32 FlatId { get; set; }

        #endregion

        #region Constructor

        public HouseConnenctionJobCreateModel()
        {

        }


        #endregion
    }
}
