﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class HouseConnenctionJobDetailModel : JobDetailModel<FinishBuildingConnenctionDetailModel>
    {
        #region Properties

        public BuildingInfoForBranchOffModel BuildingInfo { get; set; }
        public Boolean OnlyHouseConnection { get; set; }
        public PersonInfo Customer { get; set; }
        public PersonInfo Owner { get; set; }
        public PersonInfo Workman { get; set; }
        public PersonInfo Architect  { get; set; }

        public String Description { get; set; }
        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectionOfOtherMediaRequired { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        #endregion

        #region Constructor

        public HouseConnenctionJobDetailModel()
        {

        }


        #endregion
    }
}
