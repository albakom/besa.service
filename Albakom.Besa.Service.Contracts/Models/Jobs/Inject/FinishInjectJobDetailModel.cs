﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FinishInjectJobDetailModel : FinishJobDetailModel
    {
        #region Properties

        public Double Lenght { get; set; }

        #endregion

        #region Constructor

        public FinishInjectJobDetailModel()
        {

        }

        #endregion
    }
}
