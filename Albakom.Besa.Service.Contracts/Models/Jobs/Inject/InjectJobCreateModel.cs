﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InjectJobCreateModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public Int32 CustomerContactId { get; set; }

        #endregion

        #region Constructor

        public InjectJobCreateModel()
        {

        }

        #endregion
    }
}
