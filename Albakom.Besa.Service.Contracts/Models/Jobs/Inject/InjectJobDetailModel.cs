﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InjectJobDetailModel : JobDetailModel<FinishInjectJobDetailModel>
    {
        #region Properties

        public PersonInfo Customer { get; set; }
        public Double ExpectedLength { get; set; }

        public ProjectAdapterColor InjectDuctColor { get; set; }
        public ProjectAdapterCableTypeModel Cable { get; set; }

        public SimpleBranchableWithGPSModel Branchable { get; set; }

        public SimpleBuildingOverviewWithGPSModel BuildingInfo { get; set; }

        #endregion

        #region Constructor

        public InjectJobDetailModel()
        {

        }

        #endregion
    }
}
