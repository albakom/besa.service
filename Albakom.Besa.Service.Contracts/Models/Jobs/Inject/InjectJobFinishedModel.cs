﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InjectJobFinishedModel : FinishedJobModel
    {
        #region Properties

        public Double Length { get; set; }

        #endregion

        #region Constructor

        public InjectJobFinishedModel()
        {

        }

        #endregion
    }
}
