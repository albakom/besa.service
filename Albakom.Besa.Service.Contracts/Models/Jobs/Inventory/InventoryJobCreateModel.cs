﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InventoryJobCreateModel
    {
        #region Properties

        public Dictionary<Int32,Double> Articles { get; set; }
        public Int32 IssuanceToId { get; set; }
        public DateTimeOffset PickupAt { get; set; }

        #endregion

        #region Constructor

        public InventoryJobCreateModel()
        {
            Articles = new Dictionary<Int32, Double>();
        }

        #endregion
    }
}
