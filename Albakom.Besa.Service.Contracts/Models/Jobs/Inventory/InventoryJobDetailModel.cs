﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InventoryJobDetailModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public PersonInfo Issuer { get; set; }
        public IEnumerable<InventoryJobPositionModel> Positions { get; set; }

        #endregion
    }
}
