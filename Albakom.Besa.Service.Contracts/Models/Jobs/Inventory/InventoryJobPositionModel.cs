﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class InventoryJobPositionModel
    {
        #region Properties

        public Int32 ArticleId { get; set; }
        public String ArticleName { get; set; }
        public Double Amount { get; set; }

        #endregion

        #region Constructor

        public InventoryJobPositionModel()
        {

        }

        #endregion
    }
}
