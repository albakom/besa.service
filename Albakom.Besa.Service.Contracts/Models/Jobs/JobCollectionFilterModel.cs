﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum JobCollectionSortProperties
    {
        FinishedTill = 1,
        Name = 2,
        CompanyName = 3,
        DoingPercentage = 4,
    }

    public class JobCollectionFilterModel
    {
        #region Properties

        public SortDirections SortDirection { get; set; }
        public JobCollectionSortProperties SortProperty { get; set; }

        public Int32 Start { get; set; }
        public Int32 Amount { get; set; }

        public String Query { get; set; }

        #endregion

        #region Constructor

        public JobCollectionFilterModel()
        {

        }

        #endregion
    }
}
