﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class JobCollectionOverviewModel
    {
        #region Properties

        public Int32 CollectionJobId { get; set; }
        public SimpleCompanyOverviewModel BoundedTo { get; set; }
        public DateTimeOffset FinishedTill { get; set; }
        public IEnumerable<SimpleJobOverview> Jobs { get; set; }
        public Double Percentage { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public JobCollectionOverviewModel()
        {
            Jobs = new List<SimpleJobOverview>();
        }

        #endregion
    }
}
