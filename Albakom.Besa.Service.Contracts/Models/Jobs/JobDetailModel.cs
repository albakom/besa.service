﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class JobDetailModel<TFinish> where TFinish: FinishJobDetailModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public TFinish FinishInfo { get; set; }
        public JobStates State { get; set; }
        public IEnumerable<FileOverviewModel> Files { get; set; }

        #endregion

        #region Constructor

        public JobDetailModel()
        {
            Files = new List<FileOverviewModel>();
        }

        #endregion
    }
}
