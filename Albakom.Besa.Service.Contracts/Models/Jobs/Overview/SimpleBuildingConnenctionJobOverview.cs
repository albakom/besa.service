﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum BuildingConnenctionTypes
    {
        BranchOff = 10,
        BuildingConnection = 20,
    }

    public class SimpleBuildingConnenctionJobOverview : BoundJobOverviewModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public String StreetName { get; set; }
        public BuildingConnenctionTypes ConnectionJobType { get; set; }

        #endregion

        #region Constructor

        public SimpleBuildingConnenctionJobOverview()
        {

        }

        #endregion

    }
}
