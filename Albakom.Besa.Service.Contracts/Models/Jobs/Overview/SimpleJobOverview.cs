﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleJobOverview
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public JobTypes JobType { get; set; }
        public JobStates State { get; set; }

        #endregion

        #region Constructor

        public SimpleJobOverview()
        {

        }

        #endregion
    }
}
