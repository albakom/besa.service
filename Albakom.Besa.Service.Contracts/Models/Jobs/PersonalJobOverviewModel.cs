﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum JobTypes
    {
        BranchOff = 10,
        HouseConnenction = 20,
        Inject = 30,
        SpliceInBranchable = 40,
        SpliceInPoP = 44,
        PrepareBranchableForSplice = 48,
        ActivationJob = 60,
        Inventory = 70,
        ContactAfterFinished = 80,
    }

    public class PersonalJobOverviewModel
    {
        #region Properties

        public JobTypes JobType { get; set; }
        public String Name { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public Int32 TaskAmount { get; set; }
        public Int32? JobId { get; set; }
        public Int32? CollectionJobId { get; set; }

        #endregion

        #region Constructor

        public PersonalJobOverviewModel()
        {

        }

        #endregion
    }
}
