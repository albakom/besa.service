﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class RequestDetailModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public Boolean OnlyHouseConnection { get; set; }
        public SimpleBuildingOverviewModel Building { get; set; }
        public SimpleFlatOverviewModel Flat { get; set; }

        public PersonInfo Customer { get; set; }
        public PersonInfo Owner { get; set; }
        public PersonInfo Architect { get; set; }
        public PersonInfo Workman { get; set; }

        public String DescriptionForHouseConnection { get; set; }

        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectionOfOtherMediaRequired { get; set; }

        public ExplainedBooleanModel SubscriberEndpointNearConnectionPoint { get; set; }

        public Double? DuctAmount { get; set; }
        public Double? SubscriberEndpointLength { get; set; }

        public ExplainedBooleanModel ActivePointNearSubscriberEndpoint { get; set; }
        public ExplainedBooleanModel PowerForActiveEquipment { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        public Boolean IsInProgress { get; set; }
        public SimpleJobOverview MostRecentJob { get; set; }

        #endregion

        #region Constructor

        public RequestDetailModel()
        {

        }

        #endregion
    }
}
