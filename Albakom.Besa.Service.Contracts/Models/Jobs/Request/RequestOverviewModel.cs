﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class RequestOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public Boolean OnlyHouseConnection { get; set; }
        public PersonInfo PrimaryPerson { get; set; }
        public SimpleBuildingOverviewModel Building { get; set; }

        #endregion

        #region Constructor

        public RequestOverviewModel()
        {

        }

        #endregion
    }
}
