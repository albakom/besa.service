﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterFinishedFilterModel
    {
        #region Properties

        public String Query { get; set; }
        public Int32 Amount { get; set; }
        public Int32 Start { get; set; }
        public Boolean? OpenState { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedFilterModel()
        {

        }

        #endregion
    }
}
