﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterFinishedFinishModel :  FinishedJobModel
    {
        #region Properties

        public Int32? RequestId { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedFinishModel()
        {

        }

        #endregion
    }
}
