﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterFinishedJobModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public Int32 ContactId { get; set; }

        public Int32? BuildingId { get; set; }
        public Int32? FlatId { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedJobModel()
        {

        }

        #endregion
    }
}
