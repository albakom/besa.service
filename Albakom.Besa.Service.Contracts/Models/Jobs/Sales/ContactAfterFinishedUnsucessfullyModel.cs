﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterFinishedUnsucessfullyModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String Comment { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedUnsucessfullyModel()
        {

        }

        #endregion
    }
}
