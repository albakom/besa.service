﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterSalesJobDetails
    {
        #region Properties

        public Int32 Id { get; set; }
        public Boolean Closed { get; set; }
        public SimpleBuildingOverviewWithGPSModel Building { get; set; }
        public SimpleFlatOverviewModel Flat { get; set; }
        public String CloseComment { get; set; }
        public Int32 RelatedProcedureId { get; set; }
        public DateTimeOffset? FinishedAt { get; set; }
        public PersonInfo Contact { get; set; }

        #endregion

        #region Constructor

        public ContactAfterSalesJobDetails()
        {

        }

        #endregion
    }
}
