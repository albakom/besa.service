﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ContactAfterSalesJobOverview 
    {
        #region Properties

        public Int32 Id { get; set; }
        public Boolean IsFinished { get; set; }
        public PersonInfo Contact { get; set; }
        public SimpleBuildingOverviewModel Building { get; set; }

        #endregion

        #region Constructor

        public ContactAfterSalesJobOverview()
        {

        }

        #endregion
    }
}
