﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CreateContactAfterFinishedJobModel
    {
        #region Properties

        public Int32? BuildingId { get; set; }
        public Int32? FlatId { get; set; }
        public Int32 RelatedProcedureId { get; set; }

        #endregion

        #region Constructor

        public CreateContactAfterFinishedJobModel()
        {

        }

        #endregion
    }
}
