﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class RequestCustomerConnectionByContactModel
    {
        #region Properties
        
        public Int32 JobId { get; set; }

        public ExplainedBooleanModel SubscriberEndpointNearConnectionPoint { get; set; }

        public Double? DuctAmount { get; set; }
        public Double? SubscriberEndpointLength { get; set; }

        public ExplainedBooleanModel ActivePointNearSubscriberEndpoint { get; set; }
        public ExplainedBooleanModel PowerForActiveEquipment { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        public Boolean ActivationAsSoonAsPossible { get; set; }

        #endregion

        #region Constructor

        public RequestCustomerConnectionByContactModel()
        {

        }

        #endregion
    }
}
