﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceBranchableJobCreateModel
    {
        #region Properties

        public Int32 FlatId { get; set; }
        public Int32 CustomerContactId { get; set; }

        #endregion

        #region Constructor

        public SpliceBranchableJobCreateModel()
        {

        }

        #endregion
    }
}
