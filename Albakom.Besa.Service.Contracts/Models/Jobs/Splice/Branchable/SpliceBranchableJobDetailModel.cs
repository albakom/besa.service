﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceBranchableJobDetailModel : JobDetailModel<FinishSpliceBranchableJobDetailModel>
    {
        #region Properties

        public BranchableTypes BranchableType { get; set; }
        public SpliceCableInfoModel BuildingCable { get; set; }
        public SpliceCableInfoModel MainCable { get; set; }
        public Int32 TrayNumberForFiberBuffer { get; set; }
        public String Caption { get; set; }

        public PersonInfo Customer { get; set; }
        public ProjectAdapterColor DuctColor { get; set; }
        public SimpleBranchableWithGPSModel Branchable { get; set; }
        public SimpleBuildingOverviewWithGPSModel BuildingInfo { get; set; }

        public IEnumerable<SpliceEntryModel> Entries { get; set; }

        #endregion

        #region Constructor

        public SpliceBranchableJobDetailModel()
        {

        }

        #endregion
    }
}
