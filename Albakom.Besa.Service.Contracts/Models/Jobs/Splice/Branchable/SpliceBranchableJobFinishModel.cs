﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceBranchableJobFinishModel : FinishedJobModel
    {
        #region Properties

        public Double CableMetric { get; set; }

        #endregion

        #region Constructor

        public SpliceBranchableJobFinishModel()
        {

        }

        #endregion

    }
}
