﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceBranchableJobOverviewModel : BoundJobOverviewModel
    {
        #region Properties

        public SimpleBuildingOverviewModel Building { get; set; }
        public SimpleBranchableWithGPSModel Branchable { get; set; }
        public PersonInfo Customer { get; set; }

        #endregion

        #region Contructor

        public SpliceBranchableJobOverviewModel()
        {

        }

        #endregion
    }
}
