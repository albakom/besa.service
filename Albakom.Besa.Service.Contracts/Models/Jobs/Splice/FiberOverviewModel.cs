﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FiberOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }

        public String Name { get; set; }
        public Int32 BundleNumber { get; set; }
        public Int32 NumberinBundle { get; set; }
        public String Color { get; set; }
        public Int32 FiberNumber { get; set; }
        public Int32 CableId { get; set; }

        #endregion

        #region Constructor

        public FiberOverviewModel()
        {

        }

        #endregion
    }
}
