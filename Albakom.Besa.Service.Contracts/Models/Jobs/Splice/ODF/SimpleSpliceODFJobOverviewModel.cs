﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimpleSpliceODFJobOverviewModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public String CableName { get; set; }

        #endregion

        #region Constructor

        public SimpleSpliceODFJobOverviewModel()
        {

        }

        #endregion
    }
}
