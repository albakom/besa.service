﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceODFJobCreateModel
    {
        #region Properties

        public Int32 MainCableId { get; set; }

        #endregion

        #region Constructor

        public SpliceODFJobCreateModel()
        {

        }

        #endregion
    }
}
