﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceODFJobDetailModel : JobDetailModel<FinishSpliceODFJobDetailModel>
    {
        #region Properties

        public String PopName { get; set; }
        public SimplePopOverviewWithGPSModel Pop { get; set; }
        public SpliceCableInfoModel MainCable { get; set; }
        public String ModulCaption { get; set; }
        public Int32 TrayNumberForFiberBuffer { get; set; }

       // public IEnumerable<SpliceEntryModel> Entries { get; set; }

        #endregion

        #region Constructor

        public SpliceODFJobDetailModel()
        {

        }

        #endregion
    }
}
