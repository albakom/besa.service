﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceODFJobOverviewModel : BoundJobOverviewModel
    {
        #region Properties

        public String PopName { get; set; }
        public GPSCoordinate Location { get; set; }
        public SpliceCableInfoModel MainCable { get; set; }

        #endregion

        #region Constructor

        public SpliceODFJobOverviewModel()
        {

        }

        #endregion
    }
}
