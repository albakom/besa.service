﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class FiberBundleModel
    {
        #region Properites

        public Int32 Number { get; set; }
        public String Color { get; set; }

        #endregion

        #region Constructor

        public FiberBundleModel()
        {

        }

        #endregion
    }
}
