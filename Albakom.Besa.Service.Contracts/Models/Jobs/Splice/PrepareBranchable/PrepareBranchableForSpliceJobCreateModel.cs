﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class PrepareBranchableForSpliceJobCreateModel
    {
        #region Properties

        public Int32 BranchableId { get; set; }

        #endregion

        #region Constructor

        public PrepareBranchableForSpliceJobCreateModel()
        {

        }

        #endregion
    }
}
