﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class PrepareBranchableForSpliceJobDetailModel : JobDetailModel<FinishPrepareBranchableForSpliceJobDetailModel>
    {
        #region Properties

        public SimpleBranchableWithGPSModel Branchable { get; set; }
        public SpliceCableInfoModel MainCable { get; set; }
        public String SpliceOverviewTableUrl { get; set; }
        public IEnumerable<FiberBundleModel> UntouchedBundles { get; set; }
        public IEnumerable<FiberBundleModel> BundlesForCustomer { get; set; }
        public FiberBundleModel BundleForLoop { get; set; }
        public IEnumerable<SpliceEntryModel> SplicesForLoop { get; set; }

        public IEnumerable<SpliceEntryModel> Splices { get; set; }

        #endregion

        #region Constructor

        public PrepareBranchableForSpliceJobDetailModel()
        {
            UntouchedBundles = new List<FiberBundleModel>();
            BundlesForCustomer = new List<FiberBundleModel>();
            SplicesForLoop = new List<SpliceEntryModel>();
            Splices = new List<SpliceEntryModel>();
        }

        #endregion
    }
}
