﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class PrepareBranchableForSpliceJobFinishModel : FinishedJobModel
    {
        #region Properties

        public Int32 BranchableId { get; set; }

        #endregion

        #region Constructor

        public PrepareBranchableForSpliceJobFinishModel()
        {

        }

        #endregion
    }
}
