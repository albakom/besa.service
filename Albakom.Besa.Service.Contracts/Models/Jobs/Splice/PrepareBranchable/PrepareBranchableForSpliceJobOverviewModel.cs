﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class PrepareBranchableForSpliceJobOverviewModel : BoundJobOverviewModel
    {
        #region Properties

        public SimpleBranchableWithGPSModel Branchable { get; set; }

        #endregion

        #region Constructor

        public PrepareBranchableForSpliceJobOverviewModel()
        {

        }

        #endregion
    }
}
