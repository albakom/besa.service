﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SimplePrepareBranchableForSpliceJobOverviewModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public SimpleBranchableWithGPSModel Branchable { get; set; }

        #endregion

        #region Constructor

        public SimplePrepareBranchableForSpliceJobOverviewModel()
        {

        }

        #endregion
    }
}
