﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceCableInfoModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 FiberAmount { get; set; }

        #endregion

        #region Constructor

        public SpliceCableInfoModel()
        {

        }

        #endregion
    }
}
