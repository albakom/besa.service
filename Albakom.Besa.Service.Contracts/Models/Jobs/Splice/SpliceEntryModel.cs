﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class SpliceEntryModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public FiberOverviewModel FirstFiber { get; set; }
        public FiberOverviewModel SecondFiber { get; set; }

        public Int32 Tray { get; set; }
        public Int32 Number { get; set; }
        public SpliceTypes Type { get; set; }

        #endregion

        #region Constructor

        public SpliceEntryModel()
        {

        }

        #endregion
    }
}
