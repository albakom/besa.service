﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum MarkedAsRead
    {
        All = 0,
        OnlyRead = 1,
        OnlyUnread = 2
    }
}
