﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionCollectionJobDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public JobCollectionOverviewModel CollectionJob { get; set; }

        #endregion

        #region Constructor

        public MessageActionCollectionJobDetailsModel()
        {

        }

        #endregion
    }
}
