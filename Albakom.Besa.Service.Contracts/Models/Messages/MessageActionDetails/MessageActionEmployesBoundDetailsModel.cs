﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionEmployesBoundDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public Boolean Removed { get; set; }
        public UserOverviewModel Employee { get; set; }
        public CompanyOverviewModel Company { get; set; }

        #endregion

        #region Constructor

        public MessageActionEmployesBoundDetailsModel()
        {

        }

        #endregion
    }
}
