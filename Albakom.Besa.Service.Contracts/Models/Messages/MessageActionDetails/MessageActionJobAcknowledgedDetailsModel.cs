﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionJobAcknowledgedDetailsModel : MessageActionJobBasedDetailsModel
    {
        #region Properties

        public UserOverviewModel AcepptedBy { get; set; }
        public DateTimeOffset AcceptedAt { get; set; }
        
        #endregion

        #region Constructor

        public MessageActionJobAcknowledgedDetailsModel()
        {

        }

        #endregion
    }
}
