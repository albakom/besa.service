﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class MessageActionJobBasedDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public SimpleJobOverview JobOverview { get; set; }
        
        #endregion

        #region Constructor

        public MessageActionJobBasedDetailsModel()
        {

        }

        #endregion
    }
}
