﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionJobDiscardDetailsModel : MessageActionJobBasedDetailsModel
    {
        #region Properties

        public UserOverviewModel DiscaredBy { get; set; }
        public DateTimeOffset DiscardedAt { get; set; }
        public String Reason { get; set; }

        #endregion

        #region Constructor

        public MessageActionJobDiscardDetailsModel()
        {

        }

        #endregion
    }
}
