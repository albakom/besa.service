﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionJobFinishedDetailsModel : MessageActionJobBasedDetailsModel
    {
        #region Properties

        public UserOverviewModel FinishedBy { get; set; }
        public DateTimeOffset FinishedAt { get; set; }
        public ExplainedBooleanModel ProblemHappend { get; set; }

        #endregion

        #region Constructor

        public MessageActionJobFinishedDetailsModel()
        {

        }

        #endregion
    }
}
