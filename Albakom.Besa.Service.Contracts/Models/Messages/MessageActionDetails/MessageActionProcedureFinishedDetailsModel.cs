﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionProcedureFinishedDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public ProcedureOverviewModel Procedure { get; set; }
        
        #endregion

        #region Constructor

        public MessageActionProcedureFinishedDetailsModel()
        {

        }

        #endregion
    }
}
