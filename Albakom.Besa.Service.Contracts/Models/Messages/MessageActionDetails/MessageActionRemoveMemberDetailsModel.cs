﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionRemoveMemberDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public UserOverviewModel Member { get; set; }
        
        #endregion

        #region Constructor

        public MessageActionRemoveMemberDetailsModel()
        {

        }

        #endregion
    }
}
