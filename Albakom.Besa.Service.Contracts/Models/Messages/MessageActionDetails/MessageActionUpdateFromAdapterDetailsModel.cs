﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageActionUpdateFromAdapterDetailsModel : MessageActionDetailsModel
    {
        #region Properties

        public ProtocolLevels Level { get; set; }
        public MessageActions Action { get; set; }

        #endregion

        #region Constructor

        public MessageActionUpdateFromAdapterDetailsModel()
        {

        }

        #endregion
    }
}
