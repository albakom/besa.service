﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageCreateModel
    {
        #region Properties

        public Int32 ReceiverId { get; set; }
        public Int32? SenderId { get; set; }
        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedObjectType { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public Int32 ProtocolEntryId { get; set; }

        public MessageActionDetailsModel Details { get; set; }

        #endregion

        #region Constructor

        public MessageCreateModel()
        {

        }

        #endregion
    }
}
