﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageFilterModel
    {
        #region Properties

        public DateTimeOffset? StartTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        public HashSet<MessageActions> Actions { get; set; }
        public HashSet<MessageRelatedObjectTypes> Types { get; set; }
        public Int32 Amount { get; set; }
        public Int32 Start { get; set; }
        public String Query { get; set; }
        public MarkedAsRead MarkedAsRead { get; set; }

        #endregion

        #region Constructor

        public MessageFilterModel()
        {
            Actions = new HashSet<MessageActions>();
            Types = new HashSet<MessageRelatedObjectTypes>();
        }

        #endregion
    }
}
