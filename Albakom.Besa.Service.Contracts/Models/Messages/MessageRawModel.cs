﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class MessageRawModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public UserOverviewModel SourceUser { get; set; }
        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedObjectType { get; set; }

        public String Details { get; set; }
        public Boolean MarkedAsRead { get; set; }

        #endregion

        #region Constructor

        public MessageRawModel()
        {

        }

        #endregion

    }
}
