﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProtocolFilterModel
    {
        #region Properties

        public DateTimeOffset? StartTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        public ICollection<MessageActions> Actions { get; set; }
        public ICollection<MessageRelatedObjectTypes> Types { get; set; }
        public Int32 Amount { get; set; }
        public Int32 Start { get; set; }

        #endregion

        #region Constructor

        public ProtocolFilterModel()
        {
            Actions = new HashSet<MessageActions>();
            Types = new HashSet<MessageRelatedObjectTypes>();
        }

        #endregion

    }
}
