﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ExplainedBooleanModel
    {
        #region Properties

        public Boolean Value { get; set; }
        public String Description { get; set; }

        #endregion

        #region Constructor

        public ExplainedBooleanModel()
        {

        }

        #endregion
    }
}
