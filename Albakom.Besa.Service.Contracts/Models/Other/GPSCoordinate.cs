﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class GPSCoordinate
    {
        #region Properties

        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        #endregion

        #region Constructor

        public GPSCoordinate()
        {

        }

        #endregion
    }
}
