﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ServiceErrros
    {
        NoError,
        Unvaible,
        NotFound,
        NotAuthorize,
        UnknowError,
        NotSpecified,
        TokenUnvaible,
    }

    public class ServiceResult<T>
    {
        #region Properties

        public ServiceErrros Error { get; set; }
        public T Data { get; set; }

        public Boolean HasError
        {
            get { return Error != ServiceErrros.NoError; }
        }


        #endregion

        #region Constructor

        public ServiceResult(T result)
        {
            Data = result;
            Error = ServiceErrros.NoError;
        }

        public ServiceResult(ServiceErrros error)
        {
            if(error == ServiceErrros.NoError) { throw new ArgumentException("error can not be 'no error'"); }

            Error = error;
            Data = default(T);
        }

        #endregion

        #region Methods

        

        #endregion
    }
}
