﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public interface IStorageContraints
    {
        #region Properties

        #region User

        Int32 MaxUserMailChars { get; }
        Int32 MaxAuthServiceIdChars { get; }
        Int32 MaxUserPhoneChars { get; }
        Int32 MaxUserSurnameChars { get; }
        Int32 MaxUserLastnameChars { get; }

        #endregion

        #region Companies

        Int32 MaxCompanyNameChars { get; }
        Int32 MaxCompanyPhoneChars { get; }
        Int32 MaxAddressStreetChars { get; }
        Int32 MaxAddressStreetNumberChars { get; }
        Int32 MaxAddressCityChars { get; }
        Int32 MinAddressZipCodeChars { get; }
        Int32 MaxAddressZipCodeChars { get; }

        #endregion

        #region Construction Stages

        Int32 MaxLengthOfConstructionStageName { get; }
        Int32 MaxLengthOfProjectMembers { get; }

        #endregion

        #region Jobs

        Int32 MaxLengthCollectionJobName { get; }
        Int32 MaxHouseConnenctionDescriptionChars { get; }

        #endregion

        #region Article

        Int32 MaxArticleNameChars { get; }

        #endregion

        #region Flats

        Int32 MaxFlatNumberChars { get; }
        Int32 MaxFlatDescriptionChars { get; }
        Int32 MaxFlatFloorChars { get; }

        #endregion

        #region Job finished

        Int32 MaxJobFinishedChars { get; }

        #endregion

        #region Files

        Int32 MaxFileNameChars { get; }

        #endregion

        #endregion

    }
}
