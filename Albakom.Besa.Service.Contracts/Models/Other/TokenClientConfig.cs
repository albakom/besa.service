﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class TokenClientConfig
    {
        #region Properties

        public String AuthUrl { get; set; }
        public String ClientId { get; set; }
        public String ClientSecret { get; set; }
        public String Scope { get; set; }

        #endregion

        #region Constructor

        public TokenClientConfig()
        {

        }

        #endregion
    }
}
