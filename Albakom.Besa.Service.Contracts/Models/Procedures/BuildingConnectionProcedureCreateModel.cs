﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingConnectionProcedureCreateModel : ProcedureCreateModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public Int32 OwnerContactId { get; set; }
        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }
        public Boolean InformSalesAfterFinish { get; set; }

        #endregion

        #region Constructor

        public BuildingConnectionProcedureCreateModel()
        {
            base.Start = ProcedureStates.RequestCreated;
            base.ExpectedEnd = ProcedureStates.ConnectBuildingJobAcknlowedged;
        }

        #endregion
    }
}
