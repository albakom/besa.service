﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class BuildingConnectionProcedureDetailModel : ProcedureDetailModel
    {
        #region Properties

        public SimpleBuildingOverviewWithGPSModel Building { get; set; }
        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }
        public Boolean InformSalesAfterFinish { get; set; }

        #endregion

        #region Constructor

        public BuildingConnectionProcedureDetailModel()
        {

        }

        #endregion
    }
}
