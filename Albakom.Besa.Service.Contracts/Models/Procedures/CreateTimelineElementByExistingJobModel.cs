﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CreateTimelineElementByExistingJobModel
    {
        #region Properties

        public Int32 JobId { get; set; }
        public Int32 ProcedureId { get; set; }
        public ProcedureStates State { get; set; }
        public ProcedureTimelineCategories Category { get; set; }

        #endregion

        #region Constructor

        public CreateTimelineElementByExistingJobModel()
        {

        }

        public CreateTimelineElementByExistingJobModel(Int32 jobId, Int32 procedureId, ProcedureTimelineCategories category, ProcedureStates state) : this()
        {
            JobId = jobId;
            ProcedureId = procedureId;
            State = state;
            Category = category;
        }

        #endregion
    }
}
