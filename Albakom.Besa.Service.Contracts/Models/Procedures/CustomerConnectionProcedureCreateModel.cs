﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CustomerConnectionProcedureCreateModel : ProcedureCreateModel
    {
        #region Properties

        public Int32 FlatId { get; set; }
        public Int32 CustomerContactId { get; set; }
        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }
        public Boolean ContractInStock { get; set; }
        public Boolean AsSoonAsPossible { get; set; }

        #endregion

        #region Constructor

        public CustomerConnectionProcedureCreateModel()
        {
            base.Start = ProcedureStates.RequestCreated;
            base.ExpectedEnd = ProcedureStates.ActivatitionJobAcknlowedged;
        }

        #endregion
    }
}
