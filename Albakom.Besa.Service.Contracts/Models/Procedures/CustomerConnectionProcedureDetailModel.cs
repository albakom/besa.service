﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class CustomerConnectionProcedureDetailModel : ProcedureDetailModel
    {
        #region Properties

        public SimpleFlatOverviewModel Flat { get; set; }
        public SimpleBuildingOverviewWithGPSModel Building { get; set; }
        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }
        public Boolean ContractInStock { get; set; }

        public Boolean ActivationAsSoonAsPossible { get; set; }

        #endregion

    #region Constructor

    public CustomerConnectionProcedureDetailModel()
        {

        }

        #endregion
    }
}
