﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProcedureCreateModel
    {
        #region Properties

        public String Name { get; set; }
        public ProcedureStates Start { get; set; }
        public ProcedureStates ExpectedEnd { get; set; }

        public ProcedureTimelineCreateModel FirstElement { get; set; }

        #endregion

        #region Constructor

        public ProcedureCreateModel()
        {

        }

        #endregion
    }
}
