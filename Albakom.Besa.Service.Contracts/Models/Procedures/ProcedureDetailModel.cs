﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class ProcedureDetailModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public PersonInfo Contact { get; set; }
        public IEnumerable<ProcedureTimelineElementDetailModel> Timeline { get; set; }
        public Boolean IsClosed { get; set; }
        public ProcedureStates Start { get; set; }
        public ProcedureStates Current { get; set; }
        public ProcedureStates ExpectedEnd { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public ProcedureDetailModel()
        {
            Timeline = new List<ProcedureTimelineElementDetailModel>();
        }

        #endregion
    }
}
