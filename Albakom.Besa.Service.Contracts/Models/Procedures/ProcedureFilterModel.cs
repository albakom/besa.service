﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum FilterOperations
    {
        Equal = 1,
        Unqual = 2,
        Greater = 3,
        Smaller = 4
    }

    public enum ProcedureSortProperties
    {
        BuildingName = 1,
        State = 2,
        Appointment = 3,
    }

    public enum SortDirections
    {
        Ascending = 1,
        Descending = 2,
    }

    public class ProcedureFilterModel
    {
        #region Properties

        public Int32 Start { get; set; }
        public Int32 Amount { get; set; }

        public Boolean? OpenState { get; set; }
        public Boolean? AsSoonAsPossibleState { get; set; }
        public Boolean? InformSalesAfterFinishedState { get; set; }

        public String Query { get; set; }


        public ProcedureStates? State { get; set; }
        public FilterOperations? StateOperator { get; set; }

        public ProcedureTypes? Type { get; set; }

        public Int32? BuildingUnitAmount { get; set; }
        public FilterOperations? BuildingUnitAmountOperation { get; set; }

        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        #endregion

        #region Constructor

        public ProcedureFilterModel()
        {

        }

        #endregion
    }
}
