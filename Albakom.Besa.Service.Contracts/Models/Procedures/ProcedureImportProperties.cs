﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ProcedureImportProperties
    {
        ContactSurname = 1,
        ContactLastname = 2,
        ContactPhone = 3,
        ContactId = 4,
        Street = 5,
        ActiveCustomer = 14,
        StreetNumber = 6,
        Aggrement = 7,
        Contract = 8,
        Appointment = 9,
        ConnectBuildingFinished = 10,
        InjectFinished = 11,
        SpliceFinished = 12,
        ActivationFinished = 13,
        InformSalesAfterFinished = 15,
    }
}
