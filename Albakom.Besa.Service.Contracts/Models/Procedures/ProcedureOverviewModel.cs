﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ProcedureTypes
    {
        HouseConnection = 1,
        CustomerConnection = 2,
    }

    public class ProcedureOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public ProcedureStates Start { get; set; }
        public ProcedureStates End { get; set; }
        public ProcedureStates Current { get; set; }
        public String Name { get; set; }
        public Boolean IsOpen { get; set; }
        public PersonInfo Contact { get; set; }
        public ProcedureTypes Type { get; set; }
        public SimpleBuildingOverviewModel Building { get; set; }

        #endregion

        #region Constructor

        public ProcedureOverviewModel()
        {

        }

        #endregion
    }
}
