﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum ProcedureTimelineCategories
    {
        Created = 1,
        Finished = 2,
        Acknowledged = 3,
        Discarded = 4,
    }
}
