﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProcedureTimelineCreateModel
    {
        #region Constructor

        public Int32 ProcedureId { get; set; }
        public ProcedureStates State { get; set; }
        public String Comment { get; set; }
        public Int32? RelatedJobId { get; set; }
        public Int32? RelatedRequestId { get; set; }
        public DateTimeOffset? Timestamp { get; set; }

        #endregion

        #region Constructor

        public ProcedureTimelineCreateModel()
        {

        }

        #endregion
    }
}
