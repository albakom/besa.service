﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProcedureTimelineElementDetailModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public ProcedureStates State { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public Int32? RelatedJobId { get; set; }
        public JobTypes? RelatedJobType { get; set; }
        public Int32? RelatedRequestId { get; set; }
        public String Comment { get; set; }

        #endregion

        #region Constructor

        public ProcedureTimelineElementDetailModel()
        {

        }

        #endregion
    }
}
