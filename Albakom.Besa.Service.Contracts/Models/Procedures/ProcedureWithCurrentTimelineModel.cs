﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProcedureWithCurrentTimelineModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public ProcedureStates ExpectedEnd { get; set; }
        public ProcedureTimelineElementDetailModel CurrentTimelineElement { get; set; }

        #endregion

        #region Constructor

        public ProcedureWithCurrentTimelineModel()
        {

        }

        #endregion
    }
}
