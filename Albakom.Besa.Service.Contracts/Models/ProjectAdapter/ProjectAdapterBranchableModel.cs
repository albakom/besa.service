﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class ProjectAdapterBranchableModel : AdapterModel
    {
        #region Properties

        public String Name { get; set; }
        public GPSCoordinate Coordinate { get; set; }
        public IEnumerable<ProjectAdapterBuildingModel> Buildings { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterBranchableModel()
        {
            Buildings = new List<ProjectAdapterBuildingModel>();
        }

        #endregion
    }
}
