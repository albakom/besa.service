﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterBuildingModel : AdapterModel
    {
        #region Properties

        public String Street { get; set; }
        public Int32 HouseholdUnits { get; set; }
        public Int32 CommercialUnits { get; set; }
        public GPSCoordinate Coordinate { get; set; }

        public ProjectAdapterColor MicroductColor { get; set; }
        public ProjectAdapterColor HouseConnectionColor { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterBuildingModel()
        {

        }

        #endregion
    }
}
