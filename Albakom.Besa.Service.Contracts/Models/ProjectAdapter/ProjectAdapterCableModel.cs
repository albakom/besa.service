﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterCableModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public String ProjectAdapterCableTypeId { get; set; }
        public ProjectAdapterColor DuctColor { get; set; }
        public Double Lenght { get; set; }

        public String ConnectedBuildingAdapterId { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterCableModel()
        {

        }

        #endregion
    }
}
