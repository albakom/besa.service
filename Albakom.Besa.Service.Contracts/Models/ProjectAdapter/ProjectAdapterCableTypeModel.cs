﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterCableTypeModel : AdapterModel
    {
        #region Properties

        public String Name { get; set; }
        public Int32 FiberAmount { get; set; }
        public Int32 FiberPerBundle { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterCableTypeModel()
        {

        }

        #endregion
    }
}
