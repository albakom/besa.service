﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterColor
    {
        #region Properties

        public String Name { get; set; }
        public String RGBHexValue { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterColor()
        {

        }

        #endregion
    }
}
