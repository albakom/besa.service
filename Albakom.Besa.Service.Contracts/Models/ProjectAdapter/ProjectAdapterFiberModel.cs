﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterFiberModel
    {
        #region Properites

        public String ProjectAdapterId { get; set; }
        public String CableProjectAdapterId { get; set; }
        public String Name { get; set; }
        public Int32 BundleNumber { get; set; }
        public Int32 NumberinBundle { get; set; }
        public String Color { get; set; }
        public Int32 FiberNumber { get; set; }

        #endregion

        #region Contrcutor

        public ProjectAdapterFiberModel()
        {

        }

        #endregion
    }
}
