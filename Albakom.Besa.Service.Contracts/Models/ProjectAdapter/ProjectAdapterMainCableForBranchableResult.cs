﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterMainCableForBranchableResult
    {
        #region Properties

        public String ProjectAdapterBranchableId { get; set; }
        public String ProjectAdapterCableId { get; set; }
        public String BranchableEndCableProejctAdapterId { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterMainCableForBranchableResult()
        {

        }

        #endregion
    }
}
