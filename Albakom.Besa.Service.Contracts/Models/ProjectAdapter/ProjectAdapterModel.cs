﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public abstract class AdapterModel
    {
        #region Properties

        public String AdapterId { get; set; }

        #endregion

        #region Constructor

        public AdapterModel()
        {

        }

        #endregion
    }
}
