﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterPoPModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Location { get; set; }

        public IEnumerable<String> Branchables { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterPoPModel()
        {

        }

        #endregion
    }
}
