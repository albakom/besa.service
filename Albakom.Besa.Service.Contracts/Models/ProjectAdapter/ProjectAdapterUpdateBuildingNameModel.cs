﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterUpdateBuildingNameModel : AdapterModel
    {
        #region Properties

        public String Name { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterUpdateBuildingNameModel()
        {

        }

        #endregion
    }
}
