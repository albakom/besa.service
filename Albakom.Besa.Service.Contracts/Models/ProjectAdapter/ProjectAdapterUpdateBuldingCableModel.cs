﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProjectAdapterUpdateBuldingCableModel : AdapterModel
    {
        #region Properties

        public Double Length { get; set; }
        public String AdapterCableTypeId { get; set; }

        #endregion
    }
}
