﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum SpliceTypes
    {
        LoopUntouched = 100,
        LoopTouched = 85,
        Normal = 1 ,
        Buffer = 90,
        Deposit = 80,
        Unkown = 101,
    }
}
