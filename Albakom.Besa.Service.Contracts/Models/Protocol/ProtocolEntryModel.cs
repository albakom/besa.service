﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProtocolEntryModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes Type { get; set; }
        public Int32 RelatedObjectId { get; set; }
        public UserOverviewModel SourcedBy { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public ProtocolEntryModel()
        {

        }

        #endregion
    }
}
