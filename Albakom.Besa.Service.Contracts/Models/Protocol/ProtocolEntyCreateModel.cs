﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProtocolEntyCreateModel
    {
        #region Properties

        public DateTimeOffset Timestamp { get; set; }
        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedType { get; set; }
        public Int32? RelatedObjectId { get; set; }
        public Int32? TriggeredByUserId { get; set; }
        public ProtocolLevels Level { get; set; }

        #endregion

        #region Constructor

        public ProtocolEntyCreateModel()
        {
            Level = ProtocolLevels.Sucesss;
        }

        public ProtocolEntyCreateModel(MessageActions action, ProtocolLevels level) : this(action, MessageRelatedObjectTypes.NotSpecified, null, level)
        {

        }

        public ProtocolEntyCreateModel(MessageActions action, MessageRelatedObjectTypes type, Int32? objectId, Int32? userId, ProtocolLevels level) : this()
        {
            Timestamp = DateTimeOffset.Now;
            Action = action;
            RelatedType = type;
            RelatedObjectId = objectId;
            TriggeredByUserId = userId;
            Level = level;
        }

        public ProtocolEntyCreateModel(MessageActions action, MessageRelatedObjectTypes type, Int32 objectId)
            : this(action, type, objectId, null, ProtocolLevels.Sucesss)
        {
        }

        public ProtocolEntyCreateModel(MessageActions action, MessageRelatedObjectTypes type, Int32? objectId, ProtocolLevels level)
   : this(action, type, objectId, null, level)
        {
            Level = level;
        }

        public ProtocolEntyCreateModel(MessageActions action) : this(action, MessageRelatedObjectTypes.NotSpecified, null, null, ProtocolLevels.Sucesss)
        {

        }

        #endregion
    }
}
