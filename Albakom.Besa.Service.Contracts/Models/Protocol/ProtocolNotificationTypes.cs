﻿using System;

namespace Albakom.Besa.Service.Contracts.Models
{
    [Flags]
    public enum ProtocolNotificationTypes
    {
        NoAction = 0,
        Message = 1,
        Email = 2,
        SMS = 4,
        Push = 8,
        AllWithoutSMS = Message | Email | Push,
        All = Message | Email | SMS | Push

    }
}
