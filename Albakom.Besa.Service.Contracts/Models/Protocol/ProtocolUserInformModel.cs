﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class ProtocolUserInformModel
    {
        #region Properties

        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedType { get; set; }
        public ProtocolNotificationTypes NotifcationType { get; set; }

        #endregion

        #region Constructor

        public ProtocolUserInformModel()
        {

        }

        #endregion

    }
}
