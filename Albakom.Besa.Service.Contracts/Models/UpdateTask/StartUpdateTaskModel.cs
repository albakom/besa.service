﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class StartUpdateTaskModel
    {
        #region Properties

        public IEnumerable<Int32> ObjectIds { get; set; }
        public UpdateTasks TaskType { get; set; }
        public String UserAuthId { get; set; }

        #endregion

        #region Constructor

        public StartUpdateTaskModel()
        {
            ObjectIds = new List<Int32>();
        }

        #endregion
    }
}
