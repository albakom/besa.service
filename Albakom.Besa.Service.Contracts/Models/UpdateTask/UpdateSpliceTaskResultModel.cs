﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateSpliceTaskResultModel : UpdateTaskResultModel
    {
        #region Properties

        public ICollection<Int32> AddedFiberIds { get; set; }
        public ICollection<Int32> RemovedFiberIds { get; set; }
        public ICollection<Int32> UpdatedFiberIds { get; set; }

        #endregion

        #region Constructor

        public UpdateSpliceTaskResultModel()
        {
            AddedFiberIds = new List<Int32>();
            RemovedFiberIds = new List<Int32>();
            UpdatedFiberIds = new List<Int32>();
        }

        #endregion
    }
}
