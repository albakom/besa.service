﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTasFinishedStateChangedModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public DateTimeOffset FinishedAt { get; set; }

        #endregion

        #region Constructor

        public UpdateTasFinishedStateChangedModel()
        {

        }

        #endregion
    }
}
