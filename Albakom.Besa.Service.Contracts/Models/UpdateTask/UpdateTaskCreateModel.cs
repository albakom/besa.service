﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskCreateModel
    {
        #region Properties

        public String UserAuthId { get; set; }
        public UpdateTasks Task { get; set; }
        public IDictionary<String, Int32> Elements { get; set; }
        public DateTimeOffset Timestamp { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskCreateModel()
        {
            Elements = new Dictionary<String, Int32>();
        }

        #endregion
    }
}
