﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskDetailModel
    {
        #region Properties

        public UpdateTaskOverviewModel Overview { get; set; }
        public IEnumerable<UpdateTaskElementOverviewModel> Elements { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskDetailModel()
        {
            Elements = new List<UpdateTaskElementOverviewModel>();
        }

        #endregion
    }
}
