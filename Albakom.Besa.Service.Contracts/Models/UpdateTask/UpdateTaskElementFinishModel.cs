﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskElementFinishModel
    {
        #region Properties

        public Int32 ElementId { get; set; }
        public UpdateTaskElementStates State { get; set; }
        public UpdateTaskResultModel Result { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskElementFinishModel()
        {

        }

        #endregion
    }
}
