﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskElementModel
    {
        #region Properties

        public Int32 Id { get; set; }

        public Int32 ObjectId { get; set; }
        public String AdapterId { get; set; }

        #endregion
    }
}
