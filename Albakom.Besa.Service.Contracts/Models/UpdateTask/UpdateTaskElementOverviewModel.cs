﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskElementOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }

        public DateTimeOffset? Started { get; set; }
        public DateTimeOffset? Ended { get; set; }
        public UpdateTaskElementStates State { get; set; }
        public UpdateTaskResultModel Result { get; set; }
        public UpdateTaskObjectOverviewModel RelatedObject { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskElementOverviewModel()
        {

        }

        #endregion
    }
}
