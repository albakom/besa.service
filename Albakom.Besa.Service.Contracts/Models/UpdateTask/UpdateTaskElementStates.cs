﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum UpdateTaskElementStates
    {
        NotStarted = 0,
        InProgress = 1,
        Error = 2,
        Waring = 3,
        Success = 4,
        Canceled = 5,
    }
}
