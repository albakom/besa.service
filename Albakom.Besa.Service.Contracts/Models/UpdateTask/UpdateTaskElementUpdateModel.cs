﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskElementUpdateModel
    {
        #region Properties

        public Int32 ElementId { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public UpdateTaskElementStates State { get; set; }
        public String Result { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskElementUpdateModel()
        {

        }

        #endregion
    }
}
