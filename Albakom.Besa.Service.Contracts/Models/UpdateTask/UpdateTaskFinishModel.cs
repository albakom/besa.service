﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskFinishModel
    {
        #region Properties

        public Int32 TaskId { get; set; }
        public Boolean Cancel { get; set; }
        public DateTimeOffset Timestamp { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskFinishModel()
        {

        }

        #endregion
    }
}
