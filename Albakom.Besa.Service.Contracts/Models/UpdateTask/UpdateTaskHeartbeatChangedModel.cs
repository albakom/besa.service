﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskHeartbeatChangedModel
    {
        #region Properties

        public Int32 TaskId { get; set; }
        public DateTimeOffset Value { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskHeartbeatChangedModel()
        {

        }

        #endregion
    }
}
