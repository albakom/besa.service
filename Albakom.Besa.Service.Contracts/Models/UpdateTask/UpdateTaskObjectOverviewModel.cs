﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskObjectOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public MessageRelatedObjectTypes Type { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskObjectOverviewModel()
        {

        }

        #endregion
    }
}
