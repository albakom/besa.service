﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public UpdateTasks TaskType { get; set; }
        public DateTimeOffset Started { get; set; }
        public DateTimeOffset? Endend { get; set; }
        public DateTimeOffset LastHearbeat { get; set; }
        public Boolean WasCanceled { get; set; }
        public SimpleUserOverviewModel CreatedBy { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskOverviewModel()
        {

        }

        #endregion
    }
}
