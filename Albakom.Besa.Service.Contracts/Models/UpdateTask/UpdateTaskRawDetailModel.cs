﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskRawDetailModel
    {
        #region Properties

        public UpdateTaskOverviewModel Overview { get; set; }
        public IEnumerable<UpdateTaskElementRawOverviewModel> Elements { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskRawDetailModel()
        {
            Elements = new List<UpdateTaskElementRawOverviewModel>();
        }

        #endregion

    }
}
