﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UpdateTaskResultModel
    {
        #region Properties

        public String Warning { get; set; }
        public String Error { get; set; }
        public ICollection<Int32> UpdatedIds { get; set; }
        public ICollection<Int32> AddedIds { get; set; }
        public ICollection<Int32> DeletedIds { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskResultModel()
        {
            UpdatedIds = new List<Int32>();
            AddedIds = new List<Int32>();
            DeletedIds = new List<Int32>();

            Error = String.Empty;
            Warning = String.Empty;
        }

        #endregion
    }
}
