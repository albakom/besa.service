﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum UpdateTasks
    {
        UpdateBuildings = 1,
        CableTypes = 2,
        Cables = 3,
        PoPs = 4,
        MainCableForPops = 5,
        PatchConnections = 6,
        Splices = 7,
        MainCableForBranchable = 8,
    }
}
