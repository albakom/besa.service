﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    [Flags]
    public enum BesaRoles
    {
        Unknown = 0,
        Admin = 1,
        ProjectManager = 2,
        CivilWorker = 4,
        AirInjector = 8,
        Sales = 16,
        Inventory = 32,
        Splice = 64,
        ActiveNetwork = 128,
        All = Admin | ProjectManager | CivilWorker | AirInjector | Sales | Inventory | Splice | ActiveNetwork
    }
}
