﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public interface IUserData
    {
        String Surname { get; set; }
        String Lastname { get; set; }
        String Phone { get; set; }
        String EMailAddress { get; set; }
        Int32? CompanyId { get; set; }
    }
}
