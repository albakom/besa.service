﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public enum MemberStatus
    {
        NoMember = 0,
        RequestedOutstanding = 1,
        IsMember = 2
    }
}
