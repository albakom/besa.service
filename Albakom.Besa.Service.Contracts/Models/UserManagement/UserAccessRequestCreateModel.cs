﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UserAccessRequestCreateModel : IUserData
    {
        #region Properties

        public String Surname { get; set; }
        public String Lastname { get; set; }
        public String Phone { get; set; }
        public String EMailAddress { get; set; }
        public Int32? CompanyId { get; set; }
        public DateTimeOffset GeneratedAt { get; set; }
        public String AuthServiceId { get; set; }

        #endregion

        #region Constructor

        public UserAccessRequestCreateModel()
        {

        }

        #endregion

    }
}
