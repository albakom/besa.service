﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UserAccessRequestOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Surname { get; set; }
        public String Lastname { get; set; }
        public String Phone { get; set; }
        public String EMailAddress { get; set; }
        public SimpleCompanyOverviewModel CompanyInfo { get; set; }
        public DateTimeOffset GeneratedAt { get; set; }

        #endregion

        #region Constructor

        public UserAccessRequestOverviewModel()
        {

        }

        #endregion
    }
}
