﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UserInfoModel
    {
        #region Properties

        public Boolean IsManagement { get; set; }
        public BesaRoles Roles { get; set; }
        public String Surname { get; set; }
        public String Lastname { get; set; }
        public SimpleCompanyOverviewModel Company { get; set; }
        public IEnumerable<MessageModel> UnreadMessage { get; set; }

        #endregion

        #region Constructor

        public UserInfoModel()
        {

        }
        
        #endregion
    }
}
