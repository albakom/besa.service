﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Contracts.Models
{
    public class UserOverviewModel
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Surname { get; set; }
        public String Lastname { get; set; }
        public String Phone { get; set; }
        public String EMailAddress { get; set; }
        public Boolean IsMember { get; set; }
        public SimpleCompanyOverviewModel CompanyInfo { get; set; }

        #endregion

        #region Constructor

        public UserOverviewModel()
        {

        }

        #endregion
    }
}
