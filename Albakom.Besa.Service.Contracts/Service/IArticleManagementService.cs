﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IArticleManagementService
    {
        Task<IEnumerable<ArticleOverviewModel>> GetAllArticles();
        Task<ArticleOverviewModel> CreateArticle(ArticleCreateModel model, String userAuthId);
        Task<ArticleOverviewModel> EditArticle(ArticleEditModel model, String userAuthId);
        Task<ArticleDetailModel> GetDetails(Int32 articleId);
        Task<Boolean> CheckIfArticleIsInUse(Int32 articleId);
        Task<Boolean> DeleteArticle(Int32 articleId, Boolean force, String userAuthId);
    }
}
