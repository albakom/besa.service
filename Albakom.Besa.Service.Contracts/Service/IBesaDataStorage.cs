﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IBesaDataStorage
    {
        #region Generell

        void Restore();

        #endregion

        #region Constraints

        IStorageContraints Constraints { get; }

        #endregion

        #region User-Management

        Task<Boolean> CheckIfUserExists(String authID);
        Task<Boolean> CheckIfUserExists(Int32 userId);
        Task<Boolean> CheckIfUsersExists(ICollection<Int32> userIdsToCheck);
        Task<Boolean> CheckIfUserHasRole(String authID, BesaRoles role);
        Task<Boolean> CheckIfUserAccessRequestExists(String authServiceId);
        Task<Boolean> CheckIfUserAccessRequestExists(Int32 authRequestId);
        Task<Boolean> CheckIfUserHasCompany(String authId);

        Task<Int32> CreateUserAccessRequest(UserAccessRequestCreateModel model);
        Task<IEnumerable<UserAccessRequestOverviewModel>> GetAllUserAccessRequest();
        Task<UserAccessRequestOverviewModel> GetUserRequestOverviewById(Int32 requestId);
        Task<String> GetAuthServiceIdByAccessRequestId(Int32 authRequestId);
        Task DeleteAuthRequest(Int32 authRequestId);
        Task<Int32> CreateUser(UserCreateModel createModel);
        Task<BesaRoles> GetUserRoles(String authServiceId);
        Task<Boolean> CheckIfUserIsManagement(String requesterAuthId);
        Task<Boolean> RemoveAuthIdFromUser(Int32 userID);
        Task<Boolean> CheckIfHasAuthId(Int32 userID);
        Task<IEnumerable<UserOverviewModel>> GetAllUsers();
        Task<String> GetAuthServiceIdByUserId(Int32 userId);
        Task<UserOverviewModel> GetUserOverviewById(Int32 userId);
        Task<BesaRoles> GetUserRoleById(Int32 userId);
        Task EditUser(UserEditModel model);
        Task<UserEditModel> GetUserDataForEdit(Int32 userId);
        Task<UserInfoModel> GetUserInfoModel(String authServiceId);

        Task<Int32> GetUserIdByAuthId(String authId);
        Task<IDictionary<Int32, BesaRoles>> GetUserRoleRelations();

        #endregion

        #region Companies

        Task<Boolean> CheckIfCompanyExists(Int32 id);
        Task<Boolean> CheckIfCompanyNameExists(String name);
        Task<Boolean> CheckIfCompanyNameExists(String name, Int32 existingCompanyId);

        Task<Int32> CreateCompany(CompanyCreateModel company);
        Task<CompanyOverviewModel> GetCompanyOverviewById(Int32 id);
        Task<IEnumerable<CompanyOverviewModel>> GetAllCompanies();
        Task<Boolean> CheckIfUserBelongsToCompany(Int32 companyId, Int32 employeeId);
        Task<Boolean> CheckIfUsersBelongsToCompany(Int32 companyId, ICollection<Int32> removedEmployees);
        Task AddEmployeesToCompany(IEnumerable<Int32> employeeIds, Int32 companyId);
        Task RemoveEmployeesFromCompany(IEnumerable<Int32> employeeIds);
        Task<Boolean> AddEmployeeToCompany(Int32 companyId, Int32 employeeId);
        Task<Boolean> RemoveEmployeeFromCompany(Int32 employeeId);
        Task EditCompany(CompanyEditModel company);
        Task<Boolean> DeleteCompany(Int32 companyId);
        Task<CompanyDetailModel> GetCompanyDetails(Int32 companyId);

        #endregion

        #region Construction Sides

        Task<IEnumerable<ConstructionStageOverviewModel>> GetAllConstructionStages();
        Task<Boolean> CheckIfConstructionStageNameExists(String name, Int32? currentId);
        Task<Boolean> CheckIfBuildingsExists(IEnumerable<Int32> buildings);
        Task<Boolean> CheckIfCabinetsExists(IEnumerable<Int32> cabinets);
        Task<Boolean> CheckIfSleevesExists(IEnumerable<Int32> sleeves);
        Task<Int32> CreateConstructionStage(ConstructionStageCreateModel model);
        Task<ConstructionStageOverviewModel> GetConstructionStageById(Int32 constructionStageId);
        Task<Boolean> CheckIfConstructionStageExists(Int32 constructionStageId);
        Task<Boolean> DeleteConstructionStage(Int32 constructionStageId);
        Task<IEnumerable<SimpleBuildingOverviewModel>> SearchStreets(String query, Int32? amount);
        Task<IEnumerable<SimpleSleeveOverviewModel>> SearchSleeves(String query, Int32? amount);
        Task<IEnumerable<SimpleCabinetOverviewModel>> SearchCabinets(String query, Int32? amount);
        Task<Int32> GetBuildingAmount();
        Task<Int32> CreateCabinets(IEnumerable<ProjectAdapterCabinetModel> data);
        Task<Int32> CreateSleeves(IEnumerable<ProjectAdapterSleeveModel> data);
        Task<Boolean> CheckIfBranchableExists(Int32 branchableId);
        Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsByBranchable(Int32 branchableId);
        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForCilivWork(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview>> GetConstructionStageDetailsForCivilWork(Int32 constructionStageId, Boolean onlyUnboundedJob);
        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForInject(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<InjectJobOverviewModel>> GetConstructionStageDetailsForInject(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForSplice(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<SpliceBranchableJobOverviewModel>> GetConstructionStageDetailsForSplice(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task<ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel>> GetConstructionStageDetailsForPrepareBranchable(Int32 constructionStageId, Boolean onlyUnboundedJob);
        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForPrepareBranchable(Boolean onlyUnboundedJob);

        Task<IEnumerable<ConstructionStageForProjectManagementOverviewModel>> GetConstructionStageOverviewForProjectManagement();
        Task<Boolean> CheckIfFlatExists(Int32 flatId);
        Task<Int32> GetBuildingIdByFlatId(Int32 flatId);

        Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsWithOneUnitAndWithoutFlats();
        Task<IEnumerable<Int32>> GetBuildingsWithOneUnit();

        Task<IEnumerable<Int32>> CreateFlats(IEnumerable<FlatCreateModel> flatsToCreate);
        Task<Boolean> CheckIfBuildingExists(Int32 buildingId);
        Task<Boolean> CheckIfBuildingExists(String streetquery);

        Task<Boolean> CheckIfBuildingHasSpaceForAFlat(Int32 buildingId);
        Task<Int32> CreateFlat(FlatCreateModel flat);
        Task<SimpleFlatOverviewModel> GetFlatOverviewById(Int32 flatId);
        Task<ICollection<String>> GetAllCableProjectAdapterIds();
        Task<Boolean> UpdateCableType(ProjectAdapterCableTypeModel item);
        Task<Int32> AddCableType(ProjectAdapterCableTypeModel item);
        Task<Boolean> DeleteCableTypes(ICollection<String> cableTypeProjectIds);
        Task<IEnumerable<String>> GetBuildingProjectAdapterIds();
        Task<Boolean> UpdateBuildingWithCableInfo(ProjectAdapterUpdateBuldingCableModel updateData);
        Task<Boolean> UpdateBuildingName(ProjectAdapterUpdateBuildingNameModel data);

        Task<Boolean> CheckIfBuildingConnenctionIsFinishedByFlatId(Int32 flatId);
        Task<Boolean> CheckIfSpliceIsFinishedByFlatId(Int32 flatId);
        Task<Boolean> CheckIfBranchableIsPreparedForSplice(Int32 branchableId);
        Task<Boolean> CheckIfCableExists(Int32 mainCableId);
        Task<Boolean> CheckIfODFSpliceIsFinished(Int32 mainCableId);
        Task<Boolean> CheckIfPrepareBranchableJobExists(Int32 jobId);
        Task<Boolean> CheckIfSpliceODFJobExists(Int32 jobId);

        Task<IDictionary<String, Int32>> GetBuildingIdsAndProjectAdapterIds();
        Task<IDictionary<String, Int32>> GetBranchablesIdsAndProjectAdapterIds();
        Task<Int32> CreateCabinet(ProjectAdapterCabinetModel model);
        Task<Int32> CreateSleeve(ProjectAdapterSleeveModel model);
        Task<Boolean> UpdateBranchableFromAdapter(Int32 branchableId, ProjectAdapterBranchableModel adapterModel);
        Task<Boolean> UpdateBuildingFromAdapter(Int32 buildingId, ProjectAdapterBuildingModel adapterModel, Int32 branchableId);
        Task<Int32> CreateBuilding(ProjectAdapterBuildingModel model, Int32 branchableId);
        Task<Boolean> DeleteBuilding(Int32 buildingId);
        Task<Boolean> DeleteBranchable(Int32 branchableId);
        Task<Boolean> CheckIfCableIsInjected(Int32 buildingId);
        Task<Boolean> CheckIfCableIsInjectedByFlatId(Int32 flatId);
        Task<String> GetBuildingStreetNameById(Int32 buildingId);
        Task<String> GetBuildingStreetNameByFlatId(Int32 flatId);
        Task<SimpleBuildingOverviewModel> GetBuildingByStreetName(String streetquery);

        Task<IEnumerable<MainCableOverviewModel>> GetMainCableWithJobAssocatiation();

        Task<IDictionary<String, Int32>> GetPopsWithProjectAdapterId();
        Task<Int32> AddPoPFromAdapter(ProjectAdapterPoPModel popFromAdapter);
        Task<Boolean> UpdatePoPFromAdapter(Int32 popId, ProjectAdapterPoPModel popFromAdapter);
        Task<Boolean> CheckIfBranchableExistsByAdapterId(String branchableAdapterId);
        Task<Boolean> DeletePopById(Int32 popId);
        Task<Boolean> UpdatePoPIdFromBranchable(String branchableAdapterId, Int32 popId);

        Task<IDictionary<String, Int32>> GetBranchablesWithProjectAdapterId();
        Task<IDictionary<String, Int32>> GetCableFromBranchbaleWithProjectAdapterId(Int32 branchableId);
        Task<Boolean> CheckIfCableTypeExistsByProjectAdapterId(String cableTypeProjectAdapterId);
        Task<Int32> GetCableTypeIdByProjectAdapterId(String cableTypeProjectAdapterId);
        Task<Boolean> UpdateCableFromAdapter(Int32 cableId, ProjectAdapterCableModel cableItem, Int32 branchableId, Int32 cableTypeId);
        Task<Int32> AddCableFromAdapter(ProjectAdapterCableModel cableItem, Int32 branchableId, Int32 cableTypeId);
        Task<Boolean> DeleteCable(Int32 cableId);

        Task<Boolean> CheckIfBuildingExistsByProjectAdapterId(String buildingPorjectAdapterId);
        Task<Int32> GetBuildingIdByProjectAdapterId(String buildingPorjectAdapterId);
        Task<Boolean> UpdateBuildingCableRelation(Int32 cableId, Int32 buildingId);

        Task<IEnumerable<Int32>> GetCableIdsByProjectIds(IEnumerable<String> projectAdapterIds);
        Task<Boolean> UpdatePoPCableRelation(Int32 popId, IEnumerable<Int32> cableIds);

        Task<IDictionary<String, Int32>> GetCablesWithFlatIDs();
        Task<Boolean> DeletePatchConnectionByFlatId(Int32 flatId);
        Task<IEnumerable<Int32>> AddPatchConnectionByFlatId(Int32 flatId, IEnumerable<ProjectAdapterPatchModel> data);

        Task<IDictionary<String, Int32>> GetSplicesFromBranchbaleWithProjectAdapterId(Int32 branchableId);
        Task<Boolean> CheckIfCableExistsByProjectId(String cableProjectAdapterId);
        Task<Int32> GetCableIdByProjectId(String cableProjectAdapterId);
        Task<Boolean> CheckIfFiberExistsByProjectAdapterId(String projectAdapterId, Int32 cableId);
        Task<Boolean> UpdateFiber(ProjectAdapterFiberModel fiberProjectModel);
        Task<Int32> AddFiberFromAdapter(ProjectAdapterFiberModel fiberProjectModel, Int32 cableId);
        Task<Boolean> DeleteSpliceAndFibers(Int32 spliceId);
        Task<Boolean> UpdateSpliceFromAdapter(Int32 spliceId, ProjectAdapterSpliceModel spliceItem, Int32 branchableId);
        Task<Int32> AddSpliceFromAdapter(FiberSpliceCreateModel createModel);

        Task<Boolean> SetCableToFlatByBuildingId(Int32 buildingID);
        Task<Boolean> UpdateBranchableCableRelation(Int32 branchableId, Int32 cableId);

        Task<ConstructionStageDetailsForEditModel> GetDetailsForEdit(Int32 constructionStageId);
        Task<IEnumerable<Int32>> GetBranchableIdsByConstructionStageId(Int32 id);
        Task<Boolean> UpdateConstructionStage(ConstructionStageUpdateModel model);
        Task<IDictionary<Int32, String>> GetAllCablesWithProjectAdapterId();
        Task<Int32> GetBranchableIdByProjectAdapterId(String projectAdapterId);
        Task<IDictionary<String, Int32>> GetFibersWithProjectAdapterId();

        Task<BranchableSpliceOverviewModel> GetBranchableSpliceOverview(Int32 branchableId);

        Task<Boolean> SetBranachableToCable(String projectAdapterId, Int32 branchableId);
        Task<Boolean> DeleteCable(String projectAdapterId);

        Task<Boolean> CheckIfBranchablesExists(IEnumerable<Int32> ids);
        Task<Boolean> CheckIfPopsExists(IEnumerable<Int32> ids);
        Task<Boolean> CheckIfCablesExists(IEnumerable<Int32> ids);

        Task<IDictionary<String, Int32>> GetCableProjectAdapterIds(IEnumerable<Int32> ids);
        Task<IDictionary<String, Int32>> GetPopsWithProjectAdapterId(IEnumerable<Int32> ids);
        Task<IDictionary<String, Int32>> GetBranchablesWithProjectAdapterId(IEnumerable<Int32> ids);

        Task<Int32?> GetFlatIdByCableId(Int32 cableId);

        #endregion

        #region Jobs

        Task<IEnumerable<Int32>> GetBuildingIdsWithoutBranchOffJob(Int32 constructionStageId);
        Task CreateBranchOffJobs(IEnumerable<BranchOffJobCreateModel> jobsToCreate);
        Task<Boolean> CheckIfJobsExists(IEnumerable<Int32> jobIds);
        Task<Boolean> CheckIfJobExists(Int32 jobId);
        Task<Boolean> CheckIfJobIsFinished(Int32 jobId);
        Task<Boolean> CheckIfJobIsAcknowledged(Int32 jobId);

        Task<Boolean> CheckIfJobsAreUnbound(IEnumerable<Int32> jobIds);
        Task<Int32> BindJobs(IEnumerable<Int32> jobIds, Int32 companyId, DateTimeOffset endDate, String name);
        Task<IEnumerable<PersonalJobOverviewModel>> GetJobOverviewForUser(String authId);
        Task<Boolean> CheckIfBranchOffJobExist(Int32 jobId);
        Task<BranchOffJobDetailModel> GetBranchOffJobDetails(Int32 jobId);
        Task<Boolean> CheckIfCollectionJobExists(Int32 collectionJobId);
        Task<CollectionJobDetailsModel> GetCollectionJobDetails(Int32 collectionJobId);

        Task<Boolean> CheckIfBuildingConnenctionIsFinished(Int32 buildingId);
        Task<Boolean> CheckIfContactInfosExists(IEnumerable<Int32> personInfoIds);
        Task<Int32> CreateCustomerConnenctionRequest(CustomerConnectRequestCreateModel jobModel);

        Task<IEnumerable<PersonInfo>> SearchContacts(String query, Int32 amount);
        Task<Int32> CreateContact(PersonInfo contact);
        Task<Boolean> FinishBranchOffJob(FinishBranchOffJobModel model);
        Task<Boolean> FinishBuildingConnenctionJob(FinishBuildingConnenctionJobModel model);

        Task<Boolean> AcknowledgeJob(AcknowledgeJobModel model);
        Task<FinishBranchOffDetailModel> GetBranchOffFinishedJobDetails(Int32 jobId);
        Task<FinishBuildingConnenctionDetailModel> GetConnectionBuildingFinishedJobDetails(Int32 jobId);

        Task<IEnumerable<SimpleJobOverview>> GetJobsToAcknowledge();
        Task<Boolean> DiscardJob(Int32 jobId);

        Task<IEnumerable<RequestOverviewModel>> GetOpenRequests();
        Task<Boolean> CheckIfCustomerRequestExists(Int32 requestId);
        Task<Boolean> CheckIfCustomerRequestIsClosed(Int32 requestId);
        Task<Boolean> DeleteCustomerRequest(Int32 requestId);
        Task<RequestDetailModel> GetCustomerRequestDetails(Int32 requestId);
        Task<Int32> CreateHouseConnenctionJob(HouseConnenctionJobCreateModel jobCreateModel);
        Task<Boolean> CloseRequest(Int32 requestId);

        Task<Int32> GetBranchOffJobIdByBuildingId(Int32 id);
        Task<Boolean> CheckIfBranchoffJobIsBoundByBuildingId(Int32 id);
        Task<Int32> GetCollectionJobId(Int32 jobId);
        Task<Boolean> AddJobToCollectionJob(Int32 collectionJobId, Int32 jobId);

        Task<Dictionary<Int32, String>> SearchBuildingsAndGetBranchOffJobIds(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchBuildingsAndGetConnenctionJobIds(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchBuildingsAndGetInjectJobIds(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchBranchablesAndGetPrepareJobId(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchBuildingsAndGetActivationJobId(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchBuildingsAndGetSplicesJobsIds(String query, Int32 amount);
        Task<Dictionary<Int32, String>> SearchMainCablesAndGetODFSpliceJobIds(String query, Int32 amount);

        Task<Boolean> CheckIfBuildingConnenctionJobExist(Int32 jobId);
        Task<HouseConnenctionJobDetailModel> GetBuildingConnenctionJobDetails(Int32 jobId);

        Task<Int32> CreateInjectJob(InjectJobCreateModel model);
        Task<JobTypes> GetJobType(Int32 jobId);
        Task<Boolean> CheckIfOnlyHouseConnenctionIsNeeded(Int32 connenctBuildingJobId);
        Task<Int32> GetBuildingIdByConnectionJobId(Int32 connenctBuildingJobId);
        Task<Int32> GetCustomerContactIdByConnenctionJobId(Int32 connenctBuildingJobId);

        Task<Boolean> FinishInjectJob(InjectJobFinishedModel model);
        Task<Boolean> CheckIfInjectJobExist(Int32 jobId);
        Task<InjectJobDetailModel> GetInjectJobDetails(Int32 jobId);
        Task<FinishInjectJobDetailModel> GetInjectFinishedJobDetails(Int32 id);

        Task<Boolean> FinishSpliceBranchable(SpliceBranchableJobFinishModel model);
        Task<Boolean> FinishPrepareBranchableForSplice(PrepareBranchableForSpliceJobFinishModel model);
        Task<Boolean> FinishSpliceODF(SpliceODFJobFinishModel model);
        Task<Int32> CreateSpliceBranchableJob(SpliceBranchableJobCreateModel model);
        Task<Int32> CreatePrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobCreateModel model);
        Task<Int32> CreateSpliceODFJob(SpliceODFJobCreateModel model);

        Task<Boolean> CheckIfSpliceBranchableJobExist(Int32 jobId);
        Task<SpliceBranchableJobDetailModel> GetSpliceBranchableJobDetails(Int32 jobId);
        Task<FinishSpliceBranchableJobDetailModel> GetSpliceBranchableFinishedJobDetails(Int32 jobId);
        Task<PrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceJobDetails(Int32 jobId);
        Task<FinishPrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceFinishedJobDetails(Int32 id);
        Task<SpliceODFJobDetailModel> GetSpliceODFJobDetails(Int32 jobId);
        Task<FinishSpliceODFJobDetailModel> GetSpliceODFFinishedJobDetails(Int32 id);

        Task<Int32?> GetConnectBuildingJobIdByBuildingId(Int32 buildingId);
        Task<Int32?> GetConnectBuildingJobIdByFlatId(Int32 flatId);
        Task<Int32?> GetRequestIdByFilterValues(RequestFilterModel requestFilterModel);
        Task<Int32> FinishAndAcknowledgeJobAdministrative(FinishJobAdministrativeModel model);

        Task<Int32> CreateActivationJob(ActivationJobCreateModel jobCreateModel);

        Task DeleteJobFinished(IEnumerable<Int32> jobFinisehdIds);
        Task DeleteJobs(IEnumerable<Int32> jobIds);

        Task<Boolean> CheckIfActivationJobExitsByFlatId(Int32 flatId);
        Task<Int32?> GetInjectJobIdByBuildingId(Int32 buildingId);
        Task<Int32?> GetInjectJobIdByFlatId(Int32 flatId);

        Task<IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel>> GetOpenPrepareBranchableForSpliceJobsOverview();
        Task<IEnumerable<SimpleSpliceODFJobOverviewModel>> GetOpenSpliceODFJobsOverview();
        Task<IEnumerable<SimpleActivationJobOverviewModel>> GetOpenActivationJobsOverview();

        Task<Boolean> FinishActivationJob(ActivationJobFinishModel model);

        Task<IEnumerable<SpliceODFJobOverviewModel>> GetOpenSpliceODFJobs();
        Task<IEnumerable<ActivationJobOverviewModel>> GetOpenActivationJobsAsOverview();

        Task<Boolean> CheckIfActivationJobExists(Int32 jobId);
        Task<ActivationJobDetailModel> GetActivationJobDetails(Int32 jobId);
        Task<FinishActivationJobDetailModel> GetActivationFinishedJobDetails(Int32 id);

        Task<IEnumerable<InventoryJobOverviewModel>> GetOpenIventoryJobs();
        Task<Boolean> CheckIfInventoryJobExists(Int32 jobId);
        Task<InventoryJobDetailModel> GetInventoryobDetails(Int32 jobId);
        Task<Int32> FinishInventoryJob(InventoryFinishedJobModel model);

        Task<Int32> GetBuildingIdByBranchOffJobId(Int32 branchOffJobId);
        Task<IEnumerable<SimpleJobOverview>> SearchBranchOffJobByBuildingNameQuery(String query, JobStates? expectedState, Int32 amount);

        Task<SimpleJobOverview> GetMostRecentJobByBuildingId(Int32 buildingId);
        Task<SimpleJobOverview> GetMostRecentJobByFlatId(Int32 flatId, Int32? buildingId);
        Task<Boolean> DeleteCollectionJob(Int32 collectionJobId);

        Task<IEnumerable<JobCollectionOverviewModel>> GetJobCollectionOverview(JobCollectionFilterModel filterModel);

        Task<Int32?> GetSpliceBranachableJobIdByFlatId(Int32 flatId);
        Task<Int32?> GetActivationJobIdByFlatId(Int32 flatId);
        Task<SimpleJobOverview> GetSimpleJobOverviewById(Int32 jobId);

        Task<Int32> CreateContactAfterFinishedJob(CreateContactAfterFinishedJobModel model);
        Task<Boolean> CheckIfCustomerContactJobExists(Int32 jobId);
        Task<ContactAfterFinishedJobModel> GetContactAfterFinishedJobModelById(Int32 jobId);
        Task<Int32> FinishContactAfterFinishedJob(ContactAfterFinishedFinishModel finishedModel);

        Task<IEnumerable<ContactAfterSalesJobOverview>> GetContactAfterSalesJobs(ContactAfterFinishedFilterModel filter);
        Task<ContactAfterSalesJobDetails> GetContactAfterSalesJobDetail(Int32 jobId);

        Task<IEnumerable<Int32>> GetAllJobIdsFromCollectionJobId(Int32 collectionJobId);

        Task<IEnumerable<Int32>> GetCompanyUserIdsByCollectionJobId(Int32 collectionJobId);
        Task<Int32> GetJobFinisherUserIdByJobId(Int32 jobId);

        #endregion

        #region Articles

        Task<Boolean> CheckIfArticleIsInUse(Int32 articleId);
        Task<Boolean> CheckIfArticleExists(Int32 articleId);
        Task<bool> CheckIfArticlesExists(IEnumerable<Int32> ids);

        Task<Boolean> CheckIfArticleNameExists(String name, Int32? existingId);

        Task<ArticleDetailModel> GetArticleDetails(Int32 articleId);
        Task<IEnumerable<ArticleOverviewModel>> GetAllArticles();
        Task<ArticleOverviewModel> GetArticleOverviewById(Int32 id);

        Task<Boolean> DeleteArticle(Int32 articleId);
        Task<Int32> CreateArticle(ArticleCreateModel model);
        Task EditArticle(ArticleEditModel model);
        Task<IEnumerable<SimpleFlatOverviewModel>> GetFlatsByBuildingId(Int32 buildingId);

        Task<Int32> CreateInventoryJob(InventoryJobCreateModel model);

        #endregion

        #region Files

        Task<Boolean> CheckIfFileExists(IEnumerable<Int32> fileIds);
        Task<Boolean> CheckIfFileExists(Int32 fileId);
        Task<Int32> CreateFile(FileCreateModel model);
        Task<FileModel> GetFileInfo(Int32 fileId);
        Task<IEnumerable<String>> GetFileUrlsByJobId(Int32 jobId);
        Task<IEnumerable<FileOverviewModel>> GetFilesSortByName(Int32 start, Int32 amount);
        Task<IEnumerable<FileOverviewModel>> SearchFiles(String query, Int32 start, Int32 amount);
        Task<FileDetailModel> GetFileDetails(Int32 fileId);
        Task<Boolean> RenameFile(Int32 fileId, String name);
        Task<Boolean> DeleteFileAccess(Int32 fileId);
        Task<ICollection<FileAccessWithIdModel>> GetFileAccessList(Int32 fileId);
        Task<Int32> AddFileAccessEntry(Int32 fileId, FileAccessModel item);
        Task<Boolean> DeleteFileAcessEntries(IEnumerable<Int32> fileAccessEntryIds);
        Task<IEnumerable<FileAccessWithIdModel>> GetAllAccessEntries();
        Task<Boolean> DeleteFileAcessEntry(Int32 id);
        Task<Boolean> DeleteFile(Int32 fileId);
        Task<String> GetFileUrlById(Int32 fileId);



        #endregion

        #region Contacts

        Task<Boolean> CheckIfContactExists(Int32 contactId);
        Task<Boolean> EditContact(PersonInfo model);
        Task<PersonInfo> GetContactById(Int32 id);
        Task<Boolean> CheckIfContactIsInUse(Int32 contactId);
        Task<Boolean> DeleteContact(Int32 contactId);
        Task<IEnumerable<PersonInfo>> GetContactsSortByLastname(Int32 start, Int32 amount);
        Task<Boolean> CheckIfContactExists(String surname, String lastname);
        Task<PersonInfo> GetContactByName(String surname, String lastname);

        #endregion

        #region Procedures

        Task<Int32> CreateBuildingConnectionProcedure(BuildingConnectionProcedureCreateModel model);
        Task<Int32> CreateCustomerConnectionProcedure(CustomerConnectionProcedureCreateModel model);
        Task<Int32?> GetProcecureIdByRequestId(Int32 requestId);
        Task<Int32?> GetProcecureIdByJobId(Int32 jobId);
        Task<Int32> AddProcedureTimelineElementIfNessesary(ProcedureTimelineCreateModel procedureTimeline);

        Task<Boolean> MarkProcedureAsFinished(Int32 procedureId);
        Task<Boolean> CheckCheckIfProcedureIsFinished(Int32 procedureId);
        Task DeleteProcedureTimelineElements(IEnumerable<Int32> proceduresTimelineElements);
        Task DeleteProcedure(Int32 procedureId);
        Task DeleteProcedures(IEnumerable<Int32> procedureIds);

        Task<IEnumerable<ProcedureOverviewModel>> GetProcedures(ProcedureFilterModel filter, ProcedureSortProperties property, SortDirections direction);
        Task<Boolean> CheckIfBuildingConnectionProcedureExists(Int32 procedureId);
        Task<Boolean> CheckIfCustomerConnectionProcedureExists(Int32 procedureId);
        Task<BuildingConnectionProcedureDetailModel> GetConnectBuildingDetails(Int32 procedureId);
        Task<CustomerConnectionProcedureDetailModel> GetConnectCustomerDetails(Int32 procedureId);

        Task<SpliceBranchableJobCreateModel> GetCreateSpliceJobInfoByCustomerConnectionProcedureId(Int32 procedureId);
        Task<ActivationJobCreateModel> GetCreateActivationJobInfoByCustomerConnectionProcedureId(Int32 procedureId);

        Task<Boolean> CheckIfCustomerConnectionProcedureExistsByFlatId(Int32 flatId);
        Task<Int32> GetCustomerConnectionProcedureByFlatId(Int32 flatId);

        Task<Boolean> CheckIfBuildingConnectionProcedureExistsByBuildingId(Int32 buildingId);
        Task<Int32> GetBuildingConnectionProcedureByBuildingId(Int32 buildingId);

        Task<IEnumerable<ProcedureWithCurrentTimelineModel>> GetOpenProceduresWithCurrentTimelineelement();
        Task<Boolean> CheckIfContactAfterFinishedJobShouldCreated(Int32 procedureId);
        Task<ProcedureTypes> GetProcedureTypeById(Int32 procedureId);

        #endregion

        #region Protocol

        Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model);
        Task<Boolean> UpdateEndingForMainCable(Int32 cableId, Int32 branchableId);

        Task<IDictionary<Int32, ProtocolNotificationTypes>> GetUsersToInform(MessageRelatedObjectTypes relatedType, MessageActions action);
        Task<IDictionary<Int32, ProtocolNotificationTypes>> GetUsersToInform(MessageRelatedObjectTypes type, MessageActions action, IEnumerable<Int32> userIds);
        Task<ProtocolNotificationTypes> GetProtocolNofitcation(MessageRelatedObjectTypes type, MessageActions action, Int32 userId);

        Task<IEnumerable<ProtocolEntryModel>> GetProtocolEntries(ProtocolFilterModel filterModel);
        Task<IEnumerable<ProtocolUserInformModel>> GetUserProtocolInformation(String userAuthId);
        Task<ICollection<ProtocolUserInformModel>> GetUserProtocolInformation(Int32 userId);

        Task<Boolean> UpdateProtocolNotification(String userAuthId, ProtocolUserInformModel item);
        Task<Boolean> CreateProtocolNotificaiton(Int32 userId, ProtocolUserInformModel expectedNotification);
        Task<Boolean> DeleteProtocolNotification(Int32 userId, ProtocolUserInformModel notificationToDelete);

        #endregion

        #region UpdateTasks

        Task<Boolean> CheckIfOpenUpdateTasksExists(UpdateTasks taskType);
        Task<Int32> CreateUpdateTask(UpdateTaskCreateModel createModel);
        Task<IEnumerable<UpdateTaskElementModel>> GetOpenUpdateTaskElement(UpdateTasks taskType);
        Task<Boolean> CheckIfUpdateTaskElementExists(Int32 elementId);
        Task<Boolean> SetUpdateTaskElementAsProcessing(Int32 elementId);
        Task<Boolean> EditUpdateTaskElement(UpdateTaskElementUpdateModel updateModel);
        Task<Int32> GetUpdateTaskIdByTaskTye(UpdateTasks taskType);
        Task<Boolean> CheckIfTaskHasOpenElements(UpdateTasks taskType);
        Task<Boolean> EditUpdateTask(UpdateTaskFinishModel finishModel);
        Task<Boolean> CheckIfTaskIsCanceledByElementId(Int32 elementId);
        Task<Boolean> CheckIfTaskElementIsAlreadyFinished(Int32 elementId);
        Task<Boolean> ResetUpdateTaskElement(Int32 elementId);
        Task<IEnumerable<Int32>> GetUpdateTaskElementsInProgressSince(DateTimeOffset timestamp);
        Task<Boolean> CheckIfTaskHeartBeatIsOlderThen(DateTimeOffset timestamp, UpdateTasks taskType);
        Task<Boolean> CheckIfTaskIsLocked(UpdateTasks taskType);
        Task<Boolean> SetLockStateOfTask(Int32 taskId,Boolean value);
        Task<Int32> GetUpdateTaskIdByElementId(Int32 taskElementId);
        Task<Boolean> UpdateHeartbeatByTaskId(Int32 taskId, DateTimeOffset hearbeat);

        Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllBranchablesAsUpdateTaskObjectOverviewModel();
        Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllPoPsAsUpdateTaskObjectOverviewModel();
        Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllFlatCablesAsUpdateTaskObjectOverviewModel();
        Task<Boolean> CheckIfUpdateTasksExists(Int32 taskId);
        Task<Boolean> CheckIfUpdateTaskIsCanceledByTaskId(Int32 taskId);
        Task<Boolean> CheckIfUpdateTaskIsFinished(Int32 taskId);
        Task<Boolean> CancelUpdateTask(Int32 taskId);
        Task<UpdateTaskRawDetailModel> GetUpdateTaskDetails(Int32 taskId);
        Task<IEnumerable<UpdateTaskOverviewModel>> GetUpdateTasks(UpdateTasks? taskType);
        Task<UpdateTaskElementRawOverviewModel> GetUpdateTaskElementOverviewById(Int32 elementId);
        Task<UpdateTaskOverviewModel> GetUpdateTaskOverviewById(Int32 id);

        #endregion

        #region Messages

        Task<Int32> CreateMessage(MessageRawCreateModel createModel);
        Task<MessageRawModel> GetMessageModelById(Int32 messageId);

        Task<String> GetMessageObjectName(Int32 objectId, MessageRelatedObjectTypes relatedType);
        Task<MessageActionJobFinishedDetailsModel> GetMessageActionJobFinishedDetails(Int32 jobId);
        Task<MessageActionJobDiscardDetailsModel> GetMessageActionJobDiscardedDetails(Int32 jobId);
        Task<MessageActionJobAcknowledgedDetailsModel> GetMessageActionJobAcknowledgedDetails(Int32 jobId);
        Task<MessageActionJobBoundDetailsModel> GetMessageActionJobBoundDetails(Int32 jobId);
        Task<MessageActionEmployesBoundDetailsModel> GetMessageActionEmployesBoundDetails(Int32 protocolEntryId);
        Task<MessageActionJobFinishJobAdministrativeDetailModel> GetMessageActionJobFinishJobAdministrativeDetail(Int32 jobId);
        Task<MessageActionRemoveMemberDetailsModel> GetMessageActionRemoveMemberDetails(Int32 userId);
        Task<MessageActionDetailsModel> GetMessageActionProcedureFinishedDetails(Int32 procedureid);
        Task<MessageActionCollectionJobDetailsModel> GetMessageActionCollectionJobDetails(Int32 collectionJobId);

        Task<IEnumerable<MessageRawModel>> GetMessages(Boolean onlyUnreaded, String userAuthId);
        Task<IEnumerable<MessageRawModel>> GetMessages(MessageFilterModel filterModel, String userAuthId);
        Task<Boolean> CheckIfMessageBelongsToUser(Int32 messageId, String userAuthId);
        Task<Boolean> SetReadStateToMessage(Boolean readState, Int32 messageId);
        Task<Boolean> CheckIfMessageExists(Int32 messageId);

        #endregion
    }
}
