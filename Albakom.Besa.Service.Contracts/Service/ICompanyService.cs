﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface ICompanyService
    {
        Task<CompanyOverviewModel> CreateCompany(CompanyCreateModel company, String userAuthId);
        Task<IEnumerable<CompanyOverviewModel>> GetAllCompanies();
        Task<Boolean> RemoveEmployee(Int32 companyId, Int32 employeeId, String userAuthId);
        Task<Boolean> AddEmployee(Int32 companyId, Int32 employeeId, String userAuthId);
        Task<Boolean> DeleteCompany(Int32 companyId, String userAuthId);
        Task<CompanyOverviewModel> EditCompany(CompanyEditModel company, String userAuthId);
        Task<CompanyDetailModel> GetCompanyDetails(Int32 companyId);
    }
}
