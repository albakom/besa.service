﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IConstructionStageService
    {
        Task<GetDataFromAdapterResult> GetDataFromAdapter(Boolean force, String userAuthId);
        Task<IEnumerable<ConstructionStageOverviewModel>> GetAllConstructionStages();
        Task<IEnumerable<SimpleBuildingOverviewModel>> SearchStreet(String query, Int32? amount);
        Task<IEnumerable<SimpleCabinetOverviewModel>> SearchCabinets(String query, Int32? amount);
        Task<IEnumerable<SimpleSleeveOverviewModel>> SearchSleeves(String query, Int32? amount);
        Task<ConstructionStageOverviewModel> CreateConstructionStage(ConstructionStageCreateModel model, String userAuthId);
        //Task<ConstructionStageOverviewModel> EditConstructionStage(ConstructionStageCreateModel model);
        Task<Boolean> DeleteConstructionStage(Int32 constructionStageId, String userAuthId);
        Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsByBranchable(Int32 branchableId);
        Task CreateBranchOffJob(Int32 constructionStageId, String userAuthId);

        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForCilivWork(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview>> GetCivilWorkDetail(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForInject(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<InjectJobOverviewModel>> GetInjectDetails(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task<IEnumerable<ConstructionStageForProjectManagementOverviewModel>> GetOverviewForProjectManagement();

        Task<IEnumerable<Int32>> AddFlatsToBuildingWithOneUnit();
        Task<SimpleFlatOverviewModel> AddFlatToBuilding(FlatCreateModel flat, String userAuthId);
        Task<IEnumerable<SimpleFlatOverviewModel>> GetFlatsByBuildingId(Int32 buildingId);
        Task<Boolean> CheckIfBuildingIsConnected(Int32 buildingId);

        Task<IEnumerable<ProjectAdapterCableTypeModel>> UpdateAllCableTypesFromAdapter(String userAuthId);
        Task<Boolean> UpdateBuildingCableInfoFromAdapter(String userAuthId);
        Task<Boolean> UpdateBuildingNameFromAdapter(String userAuthId);

        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForSplice(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<SpliceBranchableJobOverviewModel>> GetSpliceDetails(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForPrepareBranchable(Boolean onlyUnboundedJob);
        Task<ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel>> GetPrepareBranchableDetails(Int32 constructionStageId, Boolean onlyUnboundedJob);

        Task UpdateBuildigsFromAdapter(String userAuthId);
        Task<IEnumerable<MainCableOverviewModel>> GetMainCables();

        Task<Boolean> UpdateCablesFromAdapter(String userAuthId);
        Task<Boolean> UpdatePopInformation(String userAuthId);
        Task<Boolean> UpdateMainCables(String userAuthId);
        Task<Boolean> UpdatePatchConnections(String userAuthId);
        Task<Boolean> UpdateSplices(String userAuthId);
        Task<Boolean> UpdateMainCableForBranchable(String userAuthId);
        Task<Boolean> UpdateAllDataFromProjectAdapter(String userAuthId);

        Task SetCableToFlatsWithOneBuildingUnit();

        Task<ConstructionStageDetailsForEditModel> GetDetailsForEdit(Int32 constructionStageId);
        Task<ConstructionStageDetailsForEditModel> UpdateConstructionStage(ConstructionStageUpdateModel model, String userAuthId);

        Task<IEnumerable<Int32>> CleanupCables();

        Task<BranchableSpliceOverviewModel> GetBranchableSpliceOverview(Int32 branchableId);

        Task<Boolean> UpdateSplices(UpdateTaskElementModel elementModel, String userAuthId);
        Task<IDictionary<String, Int32>> GetAllBranchableWithProjectAdapterIds();
    }
}
