﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface ICustomerService
    {
        Task<Int32> CreateCustomerConnectRequest(CustomerConnectRequestCreateModel model, String userAuthId);
        Task<IEnumerable<PersonInfo>> SearchContacts(String query, Int32 amount);
        Task<Int32> CreateContact(PersonInfo contact, String userAuthId);
        Task<PersonInfo> EditContact(PersonInfo model, String userAuthId);
        Task<Boolean> CheckIfContactIsInUse(Int32 contactId);
        Task<PersonInfo> GetContactById(Int32 id);
        Task<Boolean> DeleteContact(Int32 contactId, String userAuthId);
        Task<IEnumerable<PersonInfo>> GetContacts(Int32 start, Int32 amount);
        Task<IEnumerable<Int32>> ImportContactsFromCSV(CSVContactImportInfoModel infoModel, String userAuthId);
    }
}
