﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IExcelFileGeneratorService
    {
        Task<NamedExportFile> GenerateSpliceOverview(Int32 branchableId);
    }
}
