﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IFileManager
    {
        Task<String> Upload(FileCreateModel model, Stream data, String containerName);
        Task<Stream> Download(String url, Int32 size);
        Task Delete(IEnumerable<String> urls);
    }
}
