﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IFileService
    {
        Task<Int32> CreateFile(FileCreateModel model, Stream fileData, String userAuthId);
        Task<FileWithStreamModel> GetFileWithStream(Int32 fileId);
        Task<IEnumerable<FileOverviewModel>> GetFiles(Int32 start, Int32 amount);
        Task<IEnumerable<FileOverviewModel>> SearchFiles(String query, Int32 start, Int32 amount);
        Task<IEnumerable<FileObjectOverviewModel>> SearchObjectForAccess(FileAccessObjects type, String query, Int32 amount);
        Task<FileDetailModel> GetDetails(Int32 fileId);
        Task<FileDetailModel> EditFile(FileEditModel model, String userAuthId);
        Task RemoveOrphansAccessEntries();
        Task<Boolean> DeleteFile(Int32 fileId, String userAuthId);
    }
}
