﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IHttpHandler
    {
        Task<HttpResponseMessage> GetAsync(String url);
        void AddHeader(String header, String value);
    }
}
