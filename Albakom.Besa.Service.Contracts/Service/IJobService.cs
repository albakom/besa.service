﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IJobService
    {
        Task<BranchOffJobDetailModel> GetBranchOffJobDetails(Int32 jobId, String requesterAuthId);
        Task<HouseConnenctionJobDetailModel> GetBuildingConnenctionJobDetails(Int32 jobId, String requesterAuthId);
        Task<CollectionJobDetailsModel> GetCollectionJobDetails(Int32 collectionJobId);
        Task<Boolean> FinishBranchOffJobModel(FinishBranchOffJobModel model);
        Task<Boolean> FinishBuildingConnenction(FinishBuildingConnenctionJobModel model);
        Task<Boolean> AcknowledgeJob(AcknowledgeJobModel model);
        Task<IEnumerable<SimpleJobOverview>> GetJobsToAcknowledge();
        Task<Boolean> DiscardJob(Int32 jobId, String userAuthId);

        Task<IEnumerable<RequestOverviewModel>> GetOpenRequests();
        Task<RequestDetailModel> GetRequestDetails(Int32 requestId);
        Task<Int32> CreateHouseConnenctionJobByRequest(Int32 requestId, Boolean boundLikeBranchOff, String userAuthId);
        Task<Boolean> DeleteRequest(Int32 requestId, String userAuthId);

        Task<Int32> CreateInjectJob(InjectJobCreateModel model, String userAuthId);

        Task<InjectJobDetailModel> GetInjectJobDetails(Int32 jobId, String requesterAuthId);
        Task<Boolean> FinishInjectJob(InjectJobFinishedModel model);
        Task<Int32> CreateHouseConnenctionJob(HouseConnenctionJobCreateModel model,String userAuthId);
        Task<Boolean> BindJobs(BindJobModel model, String userAuhtId);

        Task<Boolean> FinishSpliceBranchableJob(SpliceBranchableJobFinishModel model);
        Task<Boolean> FinishPrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobFinishModel model);
        Task<Boolean> FinishSpliceODFJob(SpliceODFJobFinishModel model);
        Task<IEnumerable<JobCollectionOverviewModel>> GetJobCollectionOverview(JobCollectionFilterModel filterModel);
        Task<Int32> CreateSpliceBranchableJob(SpliceBranchableJobCreateModel model, String userAuthId);
        Task<Int32> CreatePrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobCreateModel model, String userAuthId);
        Task<Int32> CreateSpliceODFJob(SpliceODFJobCreateModel model, String userAuthId);

        Task<SpliceBranchableJobDetailModel> GetSpliceBranchableJobDetails(Int32 jobId, String requesterAuthId);
        Task<PrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceJobDetails(Int32 jobId, String requesterAuthId);
        Task<SpliceODFJobDetailModel> GetSpliceODFJobDetails(Int32 jobId, String requesterAuthId);
        Task<Boolean> CheckIfBuildingHasCable(Int32 buildingId);

        Task<Boolean> FinishJobAdministrative(FinishJobAdministrativeModel model);

        Task<Int32> CreateInventoryJob(InventoryJobCreateModel model, String userAuthId);
        Task<Int32> CreateActivationJob(ActivationJobCreateModel model, String userAuthId);
        Task<Boolean> FinishActivationJob(ActivationJobFinishModel model);

        Task<IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel>> GetOpenPrepareBranchableForSpliceJobsOverview();
        Task<IEnumerable<SimpleSpliceODFJobOverviewModel>> GetOpenSpliceODFJobsOverview();
        Task<IEnumerable<SimpleActivationJobOverviewModel>> GetOpenActivationJobsOverview();

        Task<IEnumerable<SpliceODFJobOverviewModel>> GetOpenSpliceODFJobs();
        Task<IEnumerable<ActivationJobOverviewModel>> GetOpenActivationJobsAsOverview();
        Task<ActivationJobDetailModel> GetActivationJobDetails(Int32 jobId, String requesterAuthId);

        Task<IEnumerable<InventoryJobOverviewModel>> GetOpenIventoryJobs();
        Task<InventoryJobDetailModel> GetInventoryobDetails(Int32 jobId);
        Task<Boolean> FinishInventoryJob(InventoryFinishedJobModel model);
        Task<Int32> CopyFinishFromBranchOffToConnectBuildingJob(Int32 branchOffJobId, String userAuthId);

        Task<IEnumerable<SimpleJobOverview>> SearchBranchOffJobByBuildingNameQuery(String query, JobStates? expectedState, Int32 amount);

        Task<SimpleJobOverview> CreateJobByRequestId(Int32 requestId, String userAuthId);
        Task<Boolean> DeleteCollectionJob(Int32 collectionJobId, String userAuthId);

        Task<Int32> CreateRequestFromContactCustomerJob(RequestCustomerConnectionByContactModel model, String authId);
        Task<Boolean> FinishContactAfterFinished(ContactAfterFinishedUnsucessfullyModel model, String userAuthId);
        Task<IEnumerable<ContactAfterSalesJobOverview>> GetContactAfterSalesJobs(ContactAfterFinishedFilterModel filter);
        Task<ContactAfterSalesJobDetails> GetContactAfterSalesJobDetail(Int32 jobId);
        
    }
}
