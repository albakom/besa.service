﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IMessageService
    {
        Task<IEnumerable<MessageModel>> GetMessages(Boolean onlyUnreaded, String userAuthId);
        Task<IEnumerable<MessageModel>> GetMessages(MessageFilterModel filterModel, String userAuthId);
        Task<Boolean> MarkMessageAsRead(Int32 messageId, String userAuthId);
    }
}
