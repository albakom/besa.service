﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IProcedureService
    {
        Task<IEnumerable<ProcedureOverviewModel>> GetProcedures(ProcedureFilterModel filterModel, ProcedureSortProperties property, SortDirections direction);
        Task<BuildingConnectionProcedureDetailModel> GetConnectBuildingDetails(Int32 id);
        Task<CustomerConnectionProcedureDetailModel> GetConnectCustomerDetails(Int32 id);
        Task<IEnumerable<Int32>> FinishProceduresThatShouldAlreadyFinished();
    }
}
