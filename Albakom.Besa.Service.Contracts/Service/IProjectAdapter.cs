﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IProjectAdapter
    {
        Task<ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>> GetAllSleeves();
        Task<ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>> GetAllCabinets();
        Task<ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>>> GetAllCableTypes();
        Task<ServiceResult<ProjectAdapterUpdateBuldingCableModel>> GetBuildingCableInfo(String projectAdapterId);
        Task<ServiceResult<ProjectAdapterUpdateBuildingNameModel>> GetBuildingNameUpdate(String projectAdapterId);

        Task<ServiceResult<IEnumerable<ProjectAdapterPoPModel>>> GetAllPoPs();
        Task<ServiceResult<IEnumerable<ProjectAdapterCableModel>>> GetCableByBranchableId(String projectAdapterId);
        Task<ServiceResult<IEnumerable<String>>> GetMainCableStartingFromPop(String popProjectAdapterId);
        Task<ServiceResult<IEnumerable<ProjectAdapterPatchModel>>> GetConnectionByCableId(String cableProjectAdatperID);
        Task<ServiceResult<IEnumerable<ProjectAdapterSpliceModel>>> GetSplicesForBranchable(String branchableProjectAdapterId);
        Task<ServiceResult<ProjectAdapterMainCableForBranchableResult>> GetMainCableForBranchable(String projectAdapterId);


    }
}
