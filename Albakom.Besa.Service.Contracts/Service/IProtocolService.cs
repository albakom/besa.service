﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IProtocolService
    {
        Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model);
        Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model, String userAuthId);

        Task<IEnumerable<ProtocolEntryModel>> GetProtocolEntries(ProtocolFilterModel filterModel);

        Task<IEnumerable<ProtocolUserInformModel>> GetUserProtocolInformations(String userAuthId);
        Task<IEnumerable<ProtocolUserInformModel>> UpdateUserProtocolInformations(String userAuthId, IEnumerable<ProtocolUserInformModel> models);
        Task<IDictionary<Int32, IEnumerable<ProtocolUserInformModel>>> RestoreProtocolNotifications();
        Task<IEnumerable<ProtocolUserInformModel>> RestoreProtocolNotifications(Int32 userId, BesaRoles role);

    }
}
