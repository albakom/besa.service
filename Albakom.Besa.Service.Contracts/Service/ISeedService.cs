﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface ISeedService
    {
        Task SeedCompanies(Int32 amount);
        Task SeedContacts(Int32 amount);
        Task SeedContactAfterFinsiehdJobs(Int32 amount, String userAuthId);
    }
}
