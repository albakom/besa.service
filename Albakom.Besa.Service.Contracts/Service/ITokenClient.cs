﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface ITokenClient
    {
        bool IsReady();
        Task<bool> Prepare();
        bool HasToken();
        Task<bool> RequestToken();
        string GetToken();
    }
}
