﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IUpdateTaskService
    {
        Task<IEnumerable<UpdateTaskElementModel>> GetOpenElements(UpdateTasks taskType, String userAuthId);
        Task<Boolean> StartUpdateTaskElement(Int32 elementId);
        Task<Boolean> FinishUpdateElement(UpdateTaskElementFinishModel model);
        Task<Boolean> FinishTask(UpdateTasks taskType);
        Task<Boolean> CheckIfTaskIsCanceledByElementId(Int32 id);
        Task<Boolean> RestoreLockedUpdateTaskElements();
        Task<Boolean> CheckIfUpdateTaskIsLocked(UpdateTasks taskType);
        Task<Boolean> LockUpdateTask(UpdateTasks taskType);
        Task<IEnumerable<Int32>> UnlockedTaskWithDeadlock();
        Task<DateTimeOffset> UpdateHeartbeatByTaskElementId(Int32 taskElementId);

        Task<IEnumerable<UpdateTaskOverviewModel>> GetUpdateTasks(UpdateTasks? taskType);
        Task<UpdateTaskDetailModel> GetUpdateTaskDetails(Int32 taskId);
        Task<Boolean> CancelUpdateTask(Int32 taskId);
        Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetPossibleObjectForTaskType(UpdateTasks taskType);
        Task<Int32> CreateUpdateTask(StartUpdateTaskModel model);
        Task<Int32> GetTaskIdByElementId(Int32 elementId);
    }
}
