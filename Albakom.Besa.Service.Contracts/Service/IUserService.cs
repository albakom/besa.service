﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IUserService
    {
        Task<Boolean> CheckIfUserHasRole(String authID, BesaRoles role);
        Task<Boolean> CheckIfUserExists(String authServiceId);
        Task CreateAccessRequest(UserAccessRequestCreateModel model);
        Task<MemberStatus> GetMemberStatus(String authServiceId);
        Task<IEnumerable<UserAccessRequestOverviewModel>> GetAllAccessRequests();
        Task<Int32> GrantAccess(GrantUserAccessModel model, String authServiceId);
        Task<BesaRoles> GetRolesByAuthServiceId(String authServiceId);

        Task<IEnumerable<UserOverviewModel>> GetAllUsers();
        Task<Boolean> RemoveMemberStatus(Int32 userID, String authServiceId);
        Task<UserEditModel> GetUserDataForEdit(Int32 userId);
        Task<UserOverviewModel> EditUser(UserEditModel model, String authServiceId);

        Task<IEnumerable<PersonalJobOverviewModel>> GetJobsForMember(String authId);
        Task<UserInfoModel> GetUserInfoModel(String authServiceId);
    }
}
