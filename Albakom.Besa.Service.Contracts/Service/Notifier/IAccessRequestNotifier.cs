﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IAccessRequestNotifier
    {
        Task NewRequestAdded(UserAccessRequestOverviewModel model);
        Task AccessGranted(String authSerivceId);
        Task RemoveAccess(String authSerivceId);
        Task RoleChanged(String authSerivceId, BesaRoles newRole);
    }
}
