﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Contracts.Service
{
    public interface IUpdateTaskNotifier
    {
        Task UpdateTaskAdded(UpdateTaskOverviewModel model);
        Task UpdateTaskCanceled(Int32 taskId);
        Task TaskElementChanged(UpdateTaskElementOverviewModel model);
        Task HeartbeatChanged(UpdateTaskHeartbeatChangedModel model);
        Task TaskLockDetected(Int32 taskId);
        Task UpdateTaskFinished(UpdateTasFinishedStateChangedModel model);
    }
}
