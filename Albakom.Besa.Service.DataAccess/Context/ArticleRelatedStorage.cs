﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public Task<Boolean> CheckIfArticleIsInUse(Int32 articleId)
        {
            Int32 amount = 0;
            return Task.FromResult(amount > 0);
        }

        public async Task<Boolean> CheckIfArticleExists(Int32 articleId)
        {
            Int32 amount = await Articles.CountAsync(x => x.Id == articleId);
            return amount > 0;
        }

        public async Task<bool> CheckIfArticlesExists(IEnumerable<Int32> ids)
        {
            Int32 amount = await Articles.Select(x => x.Id).Intersect(ids).CountAsync();
            return amount == ids.Count();
        }

        public async Task<Boolean> CheckIfArticleNameExists(String name, Int32? existingId)
        {
            String normalizeName = name.Trim().ToLower();

            IQueryable<ArticleDataModel> query = Articles.Where(x => x.Name.ToLower() == normalizeName);
            if (existingId.HasValue == true)
            {
                query = query.Where(x => x.Id != existingId.Value);
            }

            Int32 amount = await query.CountAsync();
            return amount > 0;
        }

        public async Task<ArticleDetailModel> GetArticleDetails(Int32 articleId)
        {
            ArticleDetailModel result = await (from article in Articles
                                               where article.Id == articleId
                                               select new ArticleDetailModel
                                               {
                                                   Id = article.Id,
                                                   Name = article.Name,
                                                   ProductSoldByMeter = article.ProductSoldByMeter,
                                               }).FirstAsync();

            return result;
        }

        public async Task<IEnumerable<ArticleOverviewModel>> GetAllArticles()
        {
            IEnumerable<ArticleOverviewModel> result = await Articles
                .OrderBy(x => x.Name)
                .Select(x => GetArticleOverviewModel(x)).ToListAsync();

            return result;
        }

        public async Task<ArticleOverviewModel> GetArticleOverviewById(Int32 id)
        {
            ArticleOverviewModel result = await Articles
                .Where(x => x.Id == id)
                .Select(x => GetArticleOverviewModel(x)).FirstAsync();

            return result;
        }

        public async Task<Boolean> DeleteArticle(Int32 articleId)
        {
            ArticleDataModel model = await Articles.FirstAsync(x => x.Id == articleId);
            Articles.Remove(model);
            Int32 changeAmount = await SaveChangesAsync();

            return changeAmount > 0;
        }

        public async Task<Int32> CreateArticle(ArticleCreateModel model)
        {
            ArticleDataModel dataModel = new ArticleDataModel(model);
            Articles.Add(dataModel);

            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task EditArticle(ArticleEditModel model)
        {
            ArticleDataModel dataModel = await Articles.FirstAsync(x => x.Id == model.Id);
            dataModel.Update(model);

            await SaveChangesAsync();
        }

        //#region Get-Overviews

        private ArticleOverviewModel GetArticleOverviewModel(ArticleDataModel dataModel)
        {
            ArticleOverviewModel result = new ArticleOverviewModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name,
                ProductSoldByMeter = dataModel.ProductSoldByMeter,
                IsInUse = false,
            };

            return result;

            //#endregion
        }
    }
}
