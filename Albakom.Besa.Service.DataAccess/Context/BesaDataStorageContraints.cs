﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public class BesaDataStorageContraints : IStorageContraints
    {
        #region Properties

        #region User

        public int MaxUserMailChars => MaxUserMailCharsInternal;
        public int MaxAuthServiceIdChars => MaxUserAuthServiceIdInternal;
        public int MaxUserPhoneChars => MaxUserPhoneCharsInternal;
        public int MaxUserSurnameChars => MaxUserSurnameInternal;
        public int MaxUserLastnameChars => MaxUserLastnameInternal;

        #region Companies

        public int MaxCompanyNameChars => MaxCompanyNameCharsInternal;
        public int MaxCompanyPhoneChars => MaxCompanyPhoneCharsInternal;
        public int MaxAddressStreetChars => MaxAddressStreetCharsInternal;
        public int MaxAddressStreetNumberChars => MaxAddressStreetNumberCharsInternal;
        public int MaxAddressCityChars => MaxAddressCityCharsInternal;
        public int MinAddressZipCodeChars => MinAddressZipCodeCharsInternal;
        public int MaxAddressZipCodeChars => MaxAddressZipCodeCharsInternal;

        #endregion

        #region  Construction Stages

        public int MaxLengthOfConstructionStageName => MaxLengthOfConstructionStageNameInternal;
        public int MaxLengthOfProjectMembers => MaxLengthOfProjectMembersInternal;

        #endregion

        #region Jobs

        public int MaxLengthCollectionJobName => MaxLengthCollectionJobNameInternal;
        public int MaxHouseConnenctionDescriptionChars => MaxHouseConnenctionDescriptionCharsInternal;

        #endregion

        #region Article

        public Int32 MaxArticleNameChars => MaxArticleNameCharsInternal;

        #endregion

        #region Flats

        public Int32 MaxFlatFloorChars => MaxFlatFloorCharsInternal;
        public Int32 MaxFlatDescriptionChars => MaxFlatDescriptionCharsInternal;
        public Int32 MaxFlatNumberChars => MaxFlatNumberCharsInternal;

        #endregion

        #region Job finisheds

        public  Int32 MaxJobFinishedChars => MaxJobFinishedCharsInternal;

        #endregion

        #region Files

        public Int32 MaxFileNameChars => MaxFileNameCharsInternal;

        #endregion

        #endregion

        #endregion

        #region Static-Properties

        #region User

        public const Int32 MaxUserMailCharsInternal = 255;
        public const Int32 MaxUserPhoneCharsInternal = 32;
        public const Int32 MaxUserAuthServiceIdInternal = 255;
        public const Int32 MaxUserSurnameInternal = 64;
        public const Int32 MaxUserLastnameInternal = 64;

        #endregion

        #region Company

        public const Int32 MaxCompanyNameCharsInternal = 255;
        public const Int32 MaxCompanyPhoneCharsInternal = 64;
        public const Int32 MaxAddressStreetCharsInternal = 255;
        public const Int32 MaxAddressStreetNumberCharsInternal = 8;
        public const Int32 MaxAddressCityCharsInternal = 64;
        public const Int32 MinAddressZipCodeCharsInternal = 4;
        public const Int32 MaxAddressZipCodeCharsInternal = 5;

        #endregion

        #region Construction Stages

        public const Int32 MaxLengthOfProjectMembersInternal = 128;
        public const Int32 MaxLengthOfConstructionStageNameInternal = 64;

        #endregion

        #region Jobs

        public const Int32 MaxLengthCollectionJobNameInternal = 128;
        public const Int32 MaxHouseConnenctionDescriptionCharsInternal = 1000;

        #endregion

        #region Article

        public const Int32 MaxArticleNameCharsInternal = 256;

        #endregion

        #region Flats

        public const Int32 MaxFlatFloorCharsInternal = 128;
        public const Int32 MaxFlatDescriptionCharsInternal = 1024;
        public const Int32 MaxFlatNumberCharsInternal = 128;

        #endregion

        #region Job finisheds

        public const Int32 MaxJobFinishedCharsInternal = 1000;

        #endregion

        #region Files

        public const Int32 MaxFileNameCharsInternal = 255;

        #endregion

       

        #endregion

        #region Constructor

        static BesaDataStorageContraints()
        {

        }

        #endregion
    }
}
