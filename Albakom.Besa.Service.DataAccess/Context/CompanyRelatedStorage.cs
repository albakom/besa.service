﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Boolean> CheckIfCompanyExists(Int32 id)
        {
            Int32 amount = await Companies.CountAsync(x => x.Id == id);
            return amount > 0;
        }

        private async Task<Boolean> CheckIfCompanyNameExists(String name, Int32? existingCompanyId)
        {
            String parsedName = name.Trim().ToLower();

            IQueryable<CompanyDataModel> preResult = Companies.Where(x => x.Name.ToLower() == parsedName);
            if(existingCompanyId.HasValue == true)
            {
                preResult = preResult.Where(x => x.Id != existingCompanyId);
            }

            Int32 result = await preResult.CountAsync();
            return result > 0;
        }

        public Task<Boolean> CheckIfCompanyNameExists(String name)
        {
            return CheckIfCompanyNameExists(name, null);
        }

        public  Task<Boolean> CheckIfCompanyNameExists(String name, Int32 existingCompanyId)
        {
            return CheckIfCompanyNameExists(name, existingCompanyId as Nullable<Int32>);
        }

        public async Task<Int32> CreateCompany(CompanyCreateModel company)
        {
            CompanyDataModel dataModel = new CompanyDataModel(company);
            Companies.Add(dataModel);
            await SaveChangesAsync();

            return dataModel.Id;
        }

        private CompanyOverviewModel GetCompanyOverviewByDataModel(CompanyDataModel dataModel)
        {
            return new CompanyOverviewModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name,
                EmployeeCount = dataModel.Employes.Count()
            };
        }

        public async Task<CompanyOverviewModel> GetCompanyOverviewById(Int32 id)
        {
            CompanyOverviewModel result = await Companies.Include(x => x.Employes).Where(x => x.Id == id).Select(x => GetCompanyOverviewByDataModel(x)).FirstAsync();
            return result;
        }

        public async Task<IEnumerable<CompanyOverviewModel>> GetAllCompanies()
        {
            IEnumerable<CompanyOverviewModel> result = await Companies.Include(x => x.Employes).Select(x => GetCompanyOverviewByDataModel(x)).ToListAsync();
            return result;
        }

        public async Task<Boolean> CheckIfUserBelongsToCompany(Int32 companyId, Int32 employeeId)
        {
            Int32 amount = await Users.CountAsync(x => x.CompanyId.HasValue == true && x.CompanyId.Value == companyId && x.ID == employeeId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfUsersBelongsToCompany(Int32 companyId, ICollection<Int32> removedEmployees)
        {
            Int32 amount = await Companies.Include(x => x.Employes).Where(x => x.Id == companyId).Select(x => x.Employes.Select(y => y.ID).Intersect(removedEmployees).Count()).FirstAsync();
            return amount == removedEmployees.Count;
        }

        private async Task ChangeEmployees(IEnumerable<Int32> employeeIds, Int32? companyId)
        {
            IEnumerable<UserDataModel> users = await Users.Where(x => employeeIds.Contains(x.ID) == true).ToListAsync();

            foreach (UserDataModel item in users)
            {
                item.CompanyId = companyId;
            }

            await SaveChangesAsync();
        }

        public Task AddEmployeesToCompany(IEnumerable<Int32> employeeIds, Int32 companyId)
        {
            return ChangeEmployees(employeeIds, companyId);
        }

        public Task RemoveEmployeesFromCompany(IEnumerable<Int32> employeeIds)
        {
            return ChangeEmployees(employeeIds, null);
        }

        private async Task<Boolean> ChangeEmployees(Int32? companyId, Int32 employeeId)
        {
            UserDataModel user = await Users.FirstAsync(x => x.ID == employeeId);
            user.CompanyId = companyId;

            Int32 result = await SaveChangesAsync();
            return result > 0;
        }

        public Task<Boolean> AddEmployeeToCompany(Int32 companyId, Int32 employeeId)
        {
            return ChangeEmployees(companyId, employeeId);
        }

        public Task<Boolean> RemoveEmployeeFromCompany(Int32 employeeId)
        {
            return ChangeEmployees(null, employeeId);
        }

        public async Task EditCompany(CompanyEditModel company)
        {
            CompanyDataModel dataModel = await Companies.FirstAsync(x => x.Id == company.Id);
            dataModel.Update(company);

            await SaveChangesAsync();
        }

        public async Task<Boolean> DeleteCompany(Int32 companyId)
        {
            CompanyDataModel dataModel = await Companies.FirstAsync(x => x.Id == companyId);
            Companies.Remove(dataModel);

            Int32 result = await SaveChangesAsync();
            return result > 0;
        }

        public async Task<CompanyDetailModel> GetCompanyDetails(Int32 companyId)
        {
            CompanyDetailModel result = await (from company in Companies
                                         where company.Id == companyId
                                         select new CompanyDetailModel
                                         {
                                             Id  = company.Id,
                                             Address = new AddressModel
                                             {
                                                 City = company.City,
                                                 PostalCode = company.PostalCode,
                                                 Street = company.Street,
                                                 StreetNumber = company.StreetNumber,
                                             },
                                             Name = company.Name,
                                             Phone = company.Phone,
                                             Employees = company.Employes.Select(x => new SimpleEmployeeModel
                                             {
                                                 Id = x.ID,
                                                 Surname = x.Surname,
                                                 Lastname = x.Lastname,
                                             }),
                                         }).FirstAsync();

            return result;
        }

        #region private Helper for selection

        private SimpleCompanyOverviewModel GetSimpleCompanyOverview(CompanyDataModel dataModel)
        {
            return dataModel == null ? null : new SimpleCompanyOverviewModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name
            };
        }

        #endregion
    }
}
