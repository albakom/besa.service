﻿using Albakom.Besa.Service.Contracts.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        internal static String NormalizeName(String input)
        {
            return String.IsNullOrEmpty(input) == true ? String.Empty : input.Trim().ToLower();
        }

        private IQueryable<ConstructionStageDataModel> GetConstructionStagesWithIncludes()
        {
            return ConstructionStages.Include(x => x.Branchables).ThenInclude(x => x.Buildings);
        }

        public async Task<IEnumerable<ConstructionStageOverviewModel>> GetAllConstructionStages()
        {
            IEnumerable<ConstructionStageOverviewModel> result = await GetConstructionStagesWithIncludes().
                OrderBy(x => x.Name).
                Select(x => GetConstructionStageOverviewModel(x)).ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfConstructionStageNameExists(String name, Int32? constructionStageId)
        {
            String parsedName = name.Trim().ToLower();

            IQueryable<ConstructionStageDataModel> query = ConstructionStages.Where(x => x.Name.ToLower() == parsedName);
            if (constructionStageId.HasValue == true)
            {
                query = query.Where(x => x.Id != constructionStageId.Value);
            }

            Int32 result = await query.CountAsync();
            return result > 0;
        }

        public async Task<Boolean> CheckIfBuildingsExists(IEnumerable<Int32> buildingIds)
        {
            Int32 result = await Buildings.Select(x => x.Id).Intersect(buildingIds).CountAsync();
            return result == buildingIds.Count();
        }

        public async Task<Boolean> CheckIfCabinetsExists(IEnumerable<Int32> cabinetsIds)
        {
            Int32 result = await StreetCabintes.Select(x => x.Id).Intersect(cabinetsIds).CountAsync();
            return result == cabinetsIds.Count();
        }

        public async Task<Boolean> CheckIfSleevesExists(IEnumerable<Int32> sleevesIds)
        {
            Int32 result = await Sleeves.Select(x => x.Id).Intersect(sleevesIds).CountAsync();
            return result == sleevesIds.Count();
        }

        public async Task<Int32> CreateConstructionStage(ConstructionStageCreateModel model)
        {
            ConstructionStageDataModel dataModel = new ConstructionStageDataModel(model);
            ConstructionStages.Add(dataModel);

            IEnumerable<Int32> ids = model.Cabinets.Union(model.Sleeves);
            List<BranchableDataModel> branchables = await Branchables.Where(x => ids.Contains(x.Id) == true).ToListAsync();

            foreach (BranchableDataModel item in branchables)
            {
                item.ConstructionStage = dataModel;
            }

            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<ConstructionStageOverviewModel> GetConstructionStageById(Int32 constructionStageId)
        {
            ConstructionStageOverviewModel result = await GetConstructionStagesWithIncludes()
                .Where(x => x.Id == constructionStageId)
                .Select(x => GetConstructionStageOverviewModel(x))
                .FirstAsync();

            return result;
        }

        public async Task<Boolean> CheckIfConstructionStageExists(Int32 constructionStageId)
        {
            Int32 result = await ConstructionStages.CountAsync(x => x.Id == constructionStageId);
            return result > 0;
        }

        public async Task<Boolean> DeleteConstructionStage(Int32 constructionStageId)
        {
            ConstructionStageDataModel dataModel = await ConstructionStages.FirstAsync(x => x.Id == constructionStageId);

            ConstructionStages.Remove(dataModel);
            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<SimpleBuildingOverviewModel>> SearchStreets(String query, Int32? amount)
        {
            query = $"%{NormalizeName(query)}%";

            IQueryable<SimpleBuildingOverviewModel> preResult = Buildings
                .Where(x => EF.Functions.Like(x.NormalizedStreetName, query) == true)
                .Select(x => GetSimpleBuildingOverviewModel(x)
                );

            if (amount.HasValue == true)
            {
                preResult = preResult.Take(amount.Value);
            }

            List<SimpleBuildingOverviewModel> result = await preResult.ToListAsync();
            return result;
        }

        private IQueryable<T> SearchBranchableDataModel<T>(IQueryable<T> source, String query, Int32? amount) where T : BranchableDataModel
        {
            query = $"%{NormalizeName(query)}%";

            source = source.Include(x => x.Buildings);

            IQueryable<T> preResult = source
              .Where(x => EF.Functions.Like(x.NormalizedName, query) == true || x.Buildings.Count(y => EF.Functions.Like(y.NormalizedStreetName, query) == true) > 0);

            if (amount.HasValue == true)
            {
                preResult = preResult.Take(amount.Value);
            }

            return preResult;
        }

        public async Task<IEnumerable<SimpleCabinetOverviewModel>> SearchCabinets(String query, Int32? amount)
        {
            IQueryable<CabinetDataModel> preResult = SearchBranchableDataModel(StreetCabintes, query, amount);

            List<SimpleCabinetOverviewModel> result = await preResult
                .Select(x => new SimpleCabinetOverviewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<SimpleSleeveOverviewModel>> SearchSleeves(String query, Int32? amount)
        {
            IQueryable<SleeveDataModel> preResult = SearchBranchableDataModel(Sleeves, query, amount);

            List<SimpleSleeveOverviewModel> result = await preResult
                .Select(x => new SimpleSleeveOverviewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToListAsync();

            return result;
        }

        public async Task<Int32> CreateCabinets(IEnumerable<ProjectAdapterCabinetModel> data)
        {
            IEnumerable<CabinetDataModel> dataModels = data.Select(x => new CabinetDataModel(x)).ToList();
            StreetCabintes.AddRange(dataModels);

            Int32 result = await SaveChangesAsync();
            return result;
        }

        public async Task<Int32> CreateSleeves(IEnumerable<ProjectAdapterSleeveModel> data)
        {
            IEnumerable<SleeveDataModel> dataModels = data.Select(x => new SleeveDataModel(x)).ToList();
            Sleeves.AddRange(dataModels);

            Int32 result = await SaveChangesAsync();
            return result;
        }

        public async Task<Int32> GetBuildingAmount()
        {
            Int32 result = await Buildings.CountAsync();
            return result;
        }

        public async Task<Boolean> CheckIfBranchableExists(Int32 branchableId)
        {
            Int32 result = await Branchables.CountAsync(x => x.Id == branchableId);
            return result > 0;
        }

        public async Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsByBranchable(Int32 branchableId)
        {
            IEnumerable<SimpleBuildingOverviewModel> result = await Buildings.Where(x => x.BranchableId == branchableId)
                .Select(x => GetSimpleBuildingOverviewModel(x)).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForCilivWork(Boolean onlyUnboundedJob)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await (from item in ConstructionStages
                                                                          .Include(x => x.Branchables).ThenInclude(x => x.Buildings).ThenInclude(x => x.BranchOffJob).ThenInclude(x => x.FinishedModel)
                                                                          .Include(x => x.Branchables).ThenInclude(x => x.Buildings).ThenInclude(x => x.ConnectionJob).ThenInclude(x => x.FinishedModel)
                                                                          let unboundedJobAmount = item.Branchables.Sum(x => x.Buildings.Count(y => (y.BranchOffJob != null || y.ConnectionJob != null) && (y.BranchOffJob.CollectionId.HasValue == true || y.ConnectionJob.CollectionId.HasValue)))
                                                                          let jobAmount = item.Branchables.Sum(x => x.Buildings.Count(y => y.BranchOffJob != null || y.ConnectionJob != null))
                                                                          let finishJobs = item.Branchables.Sum(x => x.Buildings.Count(y => (y.InjectJob != null && y.InjectJob.FinishedModel != null) || (y.ConnectionJob != null && y.ConnectionJob.FinishedModel != null)))
                                                                          where onlyUnboundedJob == false || unboundedJobAmount > 0
                                                                          orderby item.Name
                                                                          select new ConstructionStageOverviewForJobs
                                                                          {
                                                                              Id = item.Id,
                                                                              Name = item.Name,
                                                                              Branchables = item.Branchables.Select(x => GetSimpleCabinetOverviewModel(x)),
                                                                              DoingPercentage = jobAmount == 0 || finishJobs == 0 ? 0.0 : (Double)finishJobs / jobAmount,
                                                                          }
                ).ToListAsync();

            return result;
        }

        private void CalculateDoingPercentages<T>(ConstructionStageDetailModel<T> result) where T : BoundJobOverviewModel
        {
            foreach (BranchableOverviewWithJobs<T> item in result.Branchables)
            {
                Int32 jobAmount = item.Jobs.Count();
                if (jobAmount == 0) { continue; }
                item.DoingPercentage = (Double)item.Jobs.Count(x => x.State == JobStates.Finished || x.State == JobStates.Acknowledged) / jobAmount * 100;
            }

            result.DoingPercentage = result.Branchables.Average(x => x.DoingPercentage);
        }

        public async Task<ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview>> GetConstructionStageDetailsForCivilWork(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview> result = await (from item in ConstructionStages
                                                                                               where item.Id == constructionStageId
                                                                                               orderby item.Name
                                                                                               select new ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview>
                                                                                               {
                                                                                                   Id = item.Id,
                                                                                                   Name = item.Name,
                                                                                                   DoingPercentage = 0.0,
                                                                                                   Branchables = item.Branchables.Select(x => new BranchableOverviewWithJobs<SimpleBuildingConnenctionJobOverview>
                                                                                                   {
                                                                                                       Id = x.Id,
                                                                                                       DoingPercentage = 0.0,
                                                                                                       Name = x.Name,
                                                                                                       Coordinate = new GPSCoordinate
                                                                                                       {
                                                                                                           Latitude = x.Latitude,
                                                                                                           Longitude = x.Longitude,
                                                                                                       },
                                                                                                       Jobs = (from building in x.Buildings
                                                                                                               let jobId = building.ConnectionJob != null ? building.ConnectionJob.Id : building.BranchOffJob.Id
                                                                                                               let boundedCompanyName = building.ConnectionJob != null ? building.ConnectionJob.Collection.Company.Name : building.BranchOffJob.Collection.Company.Name
                                                                                                               let connenctionJobState = building.BranchOffJob != null && building.ConnectionJob.FinishedModel != null ? (building.ConnectionJob.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished) : JobStates.Open
                                                                                                               let branchOffJobState = building.BranchOffJob != null && building.BranchOffJob.FinishedModel != null ? (building.BranchOffJob.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished) : JobStates.Open
                                                                                                               where (building.ConnectionJob != null || building.BranchOffJob != null) &&
                                                                                                                 (onlyUnboundedJob == false || (building.BranchOffJob.CollectionId.HasValue == false && building.ConnectionJob.CollectionId.HasValue == false))
                                                                                                               select new SimpleBuildingConnenctionJobOverview
                                                                                                               {
                                                                                                                   BuildingId = building.Id,
                                                                                                                   ConnectionJobType = building.ConnectionJob != null ? BuildingConnenctionTypes.BuildingConnection : BuildingConnenctionTypes.BranchOff,
                                                                                                                   JobId = jobId,
                                                                                                                   StreetName = building.StreetName,
                                                                                                                   BoundedCompanyName = boundedCompanyName,
                                                                                                                   State = building.ConnectionJob != null ? connenctionJobState : branchOffJobState
                                                                                                               }

                                                                                                               ).ToList(),
                                                                                                   }).ToList(),
                                                                                               }

               ).FirstAsync();

            CalculateDoingPercentages(result);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForInject(Boolean onlyUnboundedJob)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await (from item in ConstructionStages.Include(x => x.Branchables).ThenInclude(x => x.Buildings).ThenInclude(x => x.InjectJob).ThenInclude(x => x.FinishedModel)
                                                                          let unboundedJobAmount = item.Branchables.Sum(x => x.Buildings.Count(y => y.InjectJob != null && y.InjectJob.CollectionId.HasValue == true))
                                                                          let jobAmount = item.Branchables.Sum(x => x.Buildings.Count(y => y.InjectJob != null))
                                                                          let finishJobs = item.Branchables.Sum(x => x.Buildings.Count(y => y.InjectJob != null && y.InjectJob.FinishedModel != null))
                                                                          where onlyUnboundedJob == false || unboundedJobAmount > 0
                                                                          orderby item.Name
                                                                          select new ConstructionStageOverviewForJobs
                                                                          {
                                                                              Id = item.Id,
                                                                              Name = item.Name,
                                                                              Branchables = item.Branchables.Select(x => GetSimpleCabinetOverviewModel(x)),
                                                                              DoingPercentage = jobAmount == 0 ? 0.0 : (Double)finishJobs / jobAmount,
                                                                          }
                ).ToListAsync();

            return result;
        }

        public async Task<ConstructionStageDetailModel<InjectJobOverviewModel>> GetConstructionStageDetailsForInject(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            ConstructionStageDetailModel<InjectJobOverviewModel> result = await (from item in ConstructionStages
                                                                     .Include(x => x.Branchables).ThenInclude(x => x.Buildings).ThenInclude(x => x.BranchOffJob).ThenInclude(x => x.FinishedModel)
                                                                                 where item.Id == constructionStageId
                                                                                 orderby item.Name
                                                                                 select new ConstructionStageDetailModel<InjectJobOverviewModel>
                                                                                 {
                                                                                     Id = item.Id,
                                                                                     Name = item.Name,
                                                                                     DoingPercentage = 0.0,
                                                                                     Branchables = item.Branchables.Select(x => new BranchableOverviewWithJobs<InjectJobOverviewModel>
                                                                                     {
                                                                                         Id = x.Id,
                                                                                         DoingPercentage = 0.0,
                                                                                         Name = x.Name,
                                                                                         Coordinate = new GPSCoordinate
                                                                                         {
                                                                                             Latitude = x.Latitude,
                                                                                             Longitude = x.Longitude,
                                                                                         },
                                                                                         Jobs = (from building in x.Buildings
                                                                                                 let boundedCompanyName = building.InjectJob.Collection.Company.Name
                                                                                                 let jobState = building.InjectJob != null && building.InjectJob.FinishedModel != null ? (building.InjectJob.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished) : JobStates.Open
                                                                                                 where (building.InjectJob != null &&
                                                                                                   (onlyUnboundedJob == false || building.InjectJob.CollectionId.HasValue == false))
                                                                                                 select new InjectJobOverviewModel
                                                                                                 {
                                                                                                     Building = GetSimpleBuildingOverviewModel(building),
                                                                                                     JobId = building.InjectJob.Id,
                                                                                                     State = jobState
                                                                                                 }

                                                                                                 ).ToList(),
                                                                                     }).ToList(),
                                                                                 }

               ).FirstAsync();

            CalculateDoingPercentages(result);
            return result;
        }


        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForSplice(Boolean onlyUnboundedJob)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await (from item in ConstructionStages
                                                                          let unboundedJobAmount = item.Branchables.Sum(x => x.Buildings.Sum(y => y.Flats.Count(z => z.BranchableSpliceJob != null && z.BranchableSpliceJob.CollectionId.HasValue == true)))
                                                                          let jobAmount = item.Branchables.Sum(x => x.Buildings.Sum(y => y.Flats.Count(z => z.BranchableSpliceJob != null)))
                                                                          let finishJobs = item.Branchables.Sum(x => x.Buildings.Sum(y => y.Flats.Count(z => z.BranchableSpliceJob != null && z.BranchableSpliceJob.FinishedModel != null)))
                                                                          where onlyUnboundedJob == false || unboundedJobAmount > 0
                                                                          orderby item.Name
                                                                          select new ConstructionStageOverviewForJobs
                                                                          {
                                                                              Id = item.Id,
                                                                              Name = item.Name,
                                                                              Branchables = item.Branchables.Select(x => GetSimpleCabinetOverviewModel(x)),
                                                                              DoingPercentage = jobAmount == 0 ? 0.0 : (Double)finishJobs / jobAmount,
                                                                          }
              ).ToListAsync();

            return result;
        }


        public async Task<ConstructionStageDetailModel<SpliceBranchableJobOverviewModel>> GetConstructionStageDetailsForSplice(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            ConstructionStageDetailModel<SpliceBranchableJobOverviewModel> result = await (from item in ConstructionStages
                                                                                           where item.Id == constructionStageId
                                                                                           orderby item.Name
                                                                                           select new ConstructionStageDetailModel<SpliceBranchableJobOverviewModel>
                                                                                           {
                                                                                               Id = item.Id,
                                                                                               Name = item.Name,
                                                                                               DoingPercentage = 0.0,
                                                                                               Branchables = item.Branchables.Select(x => new BranchableOverviewWithJobs<SpliceBranchableJobOverviewModel>
                                                                                               {
                                                                                                   Id = x.Id,
                                                                                                   DoingPercentage = 0.0,
                                                                                                   Name = x.Name,
                                                                                                   Coordinate = new GPSCoordinate
                                                                                                   {
                                                                                                       Latitude = x.Latitude,
                                                                                                       Longitude = x.Longitude,
                                                                                                   },
                                                                                                   Jobs = (from job in SpliceBranchableJobs
                                                                                                           let boundedCompanyName = job.Collection.Company.Name
                                                                                                           let jobState = job.FinishedModel != null ? (job.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished) : JobStates.Open
                                                                                                           where (x.Buildings.Select(y => y.Id).Contains(job.Flat.BuildingId) == true) &&
                                                                                                             (onlyUnboundedJob == false || job.CollectionId.HasValue == false)
                                                                                                           select new SpliceBranchableJobOverviewModel
                                                                                                           {
                                                                                                               Building = GetSimpleBuildingOverviewModel(job.Flat.Building),
                                                                                                               JobId = job.Id,
                                                                                                               State = jobState
                                                                                                           }

                                                                                                           ).ToList(),
                                                                                               }).ToList(),
                                                                                           }

             ).FirstAsync();

            CalculateDoingPercentages(result);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetConstructionStageOverviewForPrepareBranchable(Boolean onlyUnboundedJob)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await (from item in ConstructionStages
                                                                          let unboundedJobAmount = item.Branchables.Count(x => x.PrepareJob != null && x.PrepareJob.CollectionId.HasValue == true)
                                                                          let jobAmount = item.Branchables.Count(x => x.PrepareJob != null)
                                                                          let finishJobs = item.Branchables.Count(x => x.PrepareJob != null && x.PrepareJob.FinishedModel != null)
                                                                          where onlyUnboundedJob == false || unboundedJobAmount > 0
                                                                          orderby item.Name
                                                                          select new ConstructionStageOverviewForJobs
                                                                          {
                                                                              Id = item.Id,
                                                                              Name = item.Name,
                                                                              Branchables = item.Branchables.Select(x => GetSimpleCabinetOverviewModel(x)),
                                                                              DoingPercentage = jobAmount == 0 ? 0.0 : (Double)finishJobs / jobAmount,
                                                                          }
             ).ToListAsync();

            return result;
        }

        public async Task<ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel>> GetConstructionStageDetailsForPrepareBranchable(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel> result = await (from item in ConstructionStages
                                                                                                     where item.Id == constructionStageId
                                                                                                     orderby item.Name
                                                                                                     select new ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel>
                                                                                                     {
                                                                                                         Id = item.Id,
                                                                                                         Name = item.Name,
                                                                                                         DoingPercentage = 0.0,
                                                                                                         Branchables = item.Branchables.Select(x => new BranchableOverviewWithJobs<PrepareBranchableForSpliceJobOverviewModel>
                                                                                                         {
                                                                                                             Id = x.Id,
                                                                                                             DoingPercentage = 0.0,
                                                                                                             Name = x.Name,
                                                                                                             Coordinate = new GPSCoordinate
                                                                                                             {
                                                                                                                 Latitude = x.Latitude,
                                                                                                                 Longitude = x.Longitude,
                                                                                                             },
                                                                                                             Jobs = (from job in PrepareBranchableForSpliceJobs
                                                                                                                     let boundedCompanyName = job.Collection.Company.Name
                                                                                                                     let jobState = job.FinishedModel != null ? (job.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished) : JobStates.Open
                                                                                                                     where job.BranchableId == x.Id &&
                                                                                                                       (onlyUnboundedJob == false || job.CollectionId.HasValue == false)
                                                                                                                     select new PrepareBranchableForSpliceJobOverviewModel
                                                                                                                     {
                                                                                                                         Branchable = GetSimpleBranchableWithGPSModel(x),
                                                                                                                         JobId = job.Id,
                                                                                                                         State = jobState
                                                                                                                     }

                                                                                                                     ).ToList(),
                                                                                                         }).ToList(),
                                                                                                     }

              ).FirstAsync();

            CalculateDoingPercentages(result);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageForProjectManagementOverviewModel>> GetConstructionStageOverviewForProjectManagement()
        {
            IEnumerable<ConstructionStageForProjectManagementOverviewModel> result = await (from stage in ConstructionStages
                                                                                            let joblessAmount = stage.Branchables.Sum(x => x.Buildings.Count(y => y.BranchOffJob == null))
                                                                                            where joblessAmount > 0
                                                                                            orderby stage.Name
                                                                                            select new ConstructionStageForProjectManagementOverviewModel
                                                                                            {
                                                                                                Id = stage.Id,
                                                                                                Name = stage.Name,
                                                                                                BuildingsWithoutBranchOffJobs = joblessAmount,
                                                                                            }).ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfFlatExists(Int32 flatId)
        {
            Int32 amount = await Flats.CountAsync(x => x.Id == flatId);
            return amount > 0;
        }

        public async Task<Int32> GetBuildingIdByFlatId(Int32 flatId)
        {
            Int32 buildingId = await Flats.Where(X => X.Id == flatId).Select(x => x.BuildingId).FirstAsync();
            return buildingId;
        }


        public async Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsWithOneUnitAndWithoutFlats()
        {
            IEnumerable<SimpleBuildingOverviewModel> result = await (from building in Buildings
                                                                     let units = building.CommercialUnits + building.HouseholdUnits
                                                                     where units == 1 && building.Flats.Count() == 0
                                                                     select GetSimpleBuildingOverviewModel(building)).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<Int32>> GetBuildingsWithOneUnit()
        {
            IEnumerable<Int32> result = await (from building in Buildings
                                               let units = building.CommercialUnits + building.HouseholdUnits
                                               where units == 1
                                               select building.Id).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<Int32>> CreateFlats(IEnumerable<FlatCreateModel> flatsToCreate)
        {
            List<FlatDataModel> dataModels = flatsToCreate.Select(x => new FlatDataModel(x)).ToList();
            Flats.AddRange(dataModels);
            await SaveChangesAsync();

            return dataModels.Select(x => x.Id).ToList();
        }

        public async Task<Boolean> CheckIfBuildingExists(Int32 buildingId)
        {
            Int32 amount = await Buildings.CountAsync(x => x.Id == buildingId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfBuildingExists(String streetquery)
        {
            String parsedQuery = streetquery.Trim().ToLower();
            Int32 amount = await Buildings.CountAsync(x => x.StreetName.ToLower() == parsedQuery);
            return amount > 0;
        }

        public async Task<SimpleBuildingOverviewModel> GetBuildingByStreetName(String streetquery)
        {
            String parsedQuery = streetquery.Trim().ToLower();
            SimpleBuildingOverviewModel result = await Buildings.Where(x => x.StreetName.ToLower() == parsedQuery).Select(x => GetSimpleBuildingOverviewModel(x)).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfBuildingHasSpaceForAFlat(Int32 buildingId)
        {
            Int32 spaceLeft = await Buildings.Where(x => x.Id == buildingId).Select(x => (x.HouseholdUnits + x.CommercialUnits) - x.Flats.Count).FirstAsync();
            return spaceLeft > 0;
        }

        public async Task<Int32> CreateFlat(FlatCreateModel flat)
        {
            FlatDataModel flatDataModel = new FlatDataModel(flat);
            Flats.Add(flatDataModel);

            await SaveChangesAsync();
            return flatDataModel.Id;
        }

        public async Task<SimpleFlatOverviewModel> GetFlatOverviewById(Int32 flatId)
        {
            SimpleFlatOverviewModel result = await Flats.Where(x => x.Id == flatId).Select(x => GetSimpleFlatModelOverviewModel(x)).FirstAsync();
            return result;
        }

        public async Task<IEnumerable<SimpleFlatOverviewModel>> GetFlatsByBuildingId(Int32 buildingId)
        {
            IEnumerable<SimpleFlatOverviewModel> result = await (from flat in Flats
                                                                 where flat.BuildingId == buildingId
                                                                 select GetSimpleFlatModelOverviewModel(flat)).ToListAsync();

            return result;
        }

        public async Task<ICollection<String>> GetAllCableProjectAdapterIds()
        {
            ICollection<String> result = await CableTypes.Select(x => x.AdapterId).ToListAsync();
            return result;
        }

        public async Task<Boolean> UpdateCableType(ProjectAdapterCableTypeModel item)
        {
            FiberCableTypeDataModel dataModel = await CableTypes.FirstAsync(x => x.AdapterId == item.AdapterId);
            dataModel.Update(item);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> AddCableType(ProjectAdapterCableTypeModel item)
        {
            FiberCableTypeDataModel dataModel = new FiberCableTypeDataModel(item);
            CableTypes.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> DeleteCableTypes(ICollection<String> cableTypeProjectIds)
        {
            List<FiberCableTypeDataModel> cableTypes = await CableTypes.Where(x => cableTypeProjectIds.Contains(x.AdapterId) == true).ToListAsync();
            CableTypes.RemoveRange(cableTypes);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<String>> GetBuildingProjectAdapterIds()
        {
            IEnumerable<String> result = await Buildings.Select(x => x.ProjectId).ToListAsync();
            return result;
        }

        public async Task<Boolean> UpdateBuildingWithCableInfo(ProjectAdapterUpdateBuldingCableModel updateData)
        {
            BuildingDataModel buildingData = await Buildings.FirstAsync(x => x.ProjectId == updateData.AdapterId);
            FiberCableTypeDataModel cableTypeDataModel = await CableTypes.FirstAsync(x => x.AdapterId == updateData.AdapterCableTypeId);
            buildingData.UpdateCableInfo(updateData, cableTypeDataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> UpdateBuildingName(ProjectAdapterUpdateBuildingNameModel updateData)
        {
            BuildingDataModel buildingData = await Buildings.FirstAsync(x => x.ProjectId == updateData.AdapterId);
            buildingData.UpdateName(updateData);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IDictionary<String, Int32>> GetBuildingIdsAndProjectAdapterIds()
        {
            Dictionary<String, Int32> result = await Buildings.ToDictionaryAsync(x => x.ProjectId, x => x.Id);
            return result;
        }

        public async Task<IDictionary<String, Int32>> GetBranchablesIdsAndProjectAdapterIds()
        {
            Dictionary<String, Int32> result = await Branchables.ToDictionaryAsync(x => x.ProjectId, x => x.Id);
            return result;
        }

        public async Task<Int32> CreateCabinet(ProjectAdapterCabinetModel model)
        {
            CabinetDataModel cabinetDataModel = new CabinetDataModel(model);
            StreetCabintes.Add(cabinetDataModel);

            await SaveChangesAsync();
            return cabinetDataModel.Id;
        }

        public async Task<Int32> CreateSleeve(ProjectAdapterSleeveModel model)
        {
            SleeveDataModel dataModel = new SleeveDataModel(model);
            Sleeves.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> UpdateBranchableFromAdapter(Int32 branchableId, ProjectAdapterBranchableModel adapterModel)
        {
            BranchableDataModel buildingData = await Branchables.FirstAsync(x => x.Id == branchableId);
            buildingData.Update(adapterModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> UpdateBuildingFromAdapter(Int32 buildingId, ProjectAdapterBuildingModel adapterModel, Int32 branchableId)
        {
            BuildingDataModel buildingData = await Buildings.FirstAsync(x => x.Id == buildingId);
            buildingData.Update(adapterModel);

            Boolean branchableChanged = buildingData.BranchableId != branchableId;
            buildingData.BranchableId = branchableId;

            await SaveChangesAsync();
            return branchableChanged;
        }

        public async Task<Int32> CreateBuilding(ProjectAdapterBuildingModel model, Int32 branchableId)
        {
            BranchableDataModel branchable = await Branchables.FirstAsync(x => x.Id == branchableId);
            BuildingDataModel building = new BuildingDataModel(model, branchable);

            Buildings.Add(building);
            await SaveChangesAsync();
            return building.Id;
        }

        public async Task<Boolean> DeleteBuilding(Int32 buildingId)
        {
            Int32 changes = 0;
            BuildingDataModel dataModel = await Buildings
                .Include(x => x.Flats).ThenInclude(x => x.BranchableSpliceJob)
                .Include(x => x.Flats).ThenInclude(x => x.Cable)
                .Include(x => x.Flats).ThenInclude(x => x.ActivationJob)
                .Include(x => x.Flats).ThenInclude(x => x.Connections)
                .Include(x => x.Flats).ThenInclude(x => x.ConnectionProcedures)
                .Include(x => x.BranchOffJob)
                .Include(x => x.ConnectionJob)
                .Include(x => x.InjectJob)
                .Include(x => x.ConnectionProcedures)
                .Include(x => x.FiberCable)
                .FirstAsync(x => x.Id == buildingId);

            if (dataModel.Flats != null && dataModel.Flats.Count > 0)
            {
                foreach (FlatDataModel item in dataModel.Flats)
                {
                    if (item.BranchableSpliceJob != null)
                    {
                        Jobs.Remove(item.BranchableSpliceJob);
                        changes += await SaveChangesAsync();
                    }
                    if (item.Cable != null)
                    {
                        item.Cable.FlatId = null;
                        changes += await SaveChangesAsync();
                    }
                    if (item.ActivationJob != null)
                    {
                        Jobs.Remove(item.ActivationJob);
                        changes += await SaveChangesAsync();
                    }
                    if (item.Connections != null && item.Connections.Count > 0)
                    {
                        FiberConnenctions.RemoveRange(item.Connections);
                        changes += await SaveChangesAsync();
                    }
                    if (item.ConnectionProcedures != null && item.ConnectionProcedures.Count > 0)
                    {
                        CustomerConnectionProcedures.RemoveRange(item.ConnectionProcedures);
                        changes += await SaveChangesAsync();
                    }
                }

                changes += await SaveChangesAsync();
                Flats.RemoveRange(dataModel.Flats);
                changes += await SaveChangesAsync();
            }

            if (dataModel.FiberCable != null)
            {
                dataModel.FiberCable.BuildingId = null;
                changes += await SaveChangesAsync();
            }
            if (dataModel.InjectJob != null) { Jobs.Remove(dataModel.InjectJob); }
            if (dataModel.ConnectionJob != null) { Jobs.Remove(dataModel.ConnectionJob); }
            if (dataModel.BranchOffJob != null) { Jobs.Remove(dataModel.BranchOffJob); }
            if (dataModel.ConnectionProcedures != null && dataModel.ConnectionProcedures.Count > 0)
            {
                BuildingConnectionProcedures.RemoveRange(dataModel.ConnectionProcedures);
            }

            await SaveChangesAsync();

            Buildings.Remove(dataModel);
            changes += await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> DeleteBranchable(Int32 branchableId)
        {
            Int32 changes = 0;

            BranchableDataModel branchable = await Branchables.Include(x => x.PrepareJob).FirstAsync(x => x.Id == branchableId);
            Branchables.Remove(branchable);

            List<FiberCableDataModel> fiberCables = await FiberCables.Where(x => x.MainCableForBranchableId == branchableId).ToListAsync();
            if (fiberCables.Count > 0)
            {
                foreach (FiberCableDataModel item in fiberCables)
                {
                    item.MainCableForBranchableId = null;
                }

                changes += await SaveChangesAsync();
            }

            List<FiberCableDataModel> fiberCables2 = await FiberCables.Where(x => x.StartingAtBranchableleId == branchableId).ToListAsync();
            if (fiberCables2.Count > 0)
            {
                foreach (FiberCableDataModel item in fiberCables2)
                {
                    item.StartingAtBranchableleId = null;
                }

                changes += await SaveChangesAsync();
            }

            if (branchable.PrepareJob != null)
            {
                Jobs.Remove(branchable.PrepareJob);
                changes += await SaveChangesAsync();
            }

            changes += await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfCableIsInjectedByFlatId(Int32 flatId)
        {
            Int32 buildingId = await GetBuildingIdByFlatId(flatId);
            Boolean result = await CheckIfCableIsInjected(buildingId);
            return result;
        }

        public async Task<Boolean> CheckIfCableIsInjected(Int32 buildingId)
        {
            FinishedJobDataModel finishedModel = await InjectJobs.Include(x => x.FinishedModel).Where(x => x.BuildingId == buildingId).Select(x => x.FinishedModel).FirstOrDefaultAsync();
            return finishedModel == null ? false : finishedModel.AcceptedAt.HasValue;
        }

        public async Task<String> GetBuildingStreetNameById(Int32 buildingId)
        {
            String name = await Buildings.Where(x => x.Id == buildingId).Select(x => x.StreetName).FirstAsync();
            return name;
        }

        public async Task<String> GetBuildingStreetNameByFlatId(Int32 flatId)
        {
            Int32 buildingId = await GetBuildingIdByFlatId(flatId);
            String result = await GetBuildingStreetNameById(buildingId);
            return result;
        }

        public async Task<IEnumerable<MainCableOverviewModel>> GetMainCableWithJobAssocatiation()
        {
            IEnumerable<MainCableOverviewModel> result = await (from cable in FiberCables
                                                                where cable.StartingAdPopId.HasValue == true
                                                                orderby cable.Name
                                                                select new MainCableOverviewModel
                                                                {
                                                                    Id = cable.Id,
                                                                    Name = cable.Name,
                                                                    JobExists = cable.SpliceODFJob != null
                                                                }
                                ).ToListAsync();

            return result;
        }

        public async Task<IDictionary<String, Int32>> GetPopsWithProjectAdapterId()
        {
            IDictionary<String, Int32> result = await Pops.ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);
            return result;
        }

        public async Task<Int32> AddPoPFromAdapter(ProjectAdapterPoPModel popFromAdapter)
        {
            PoPDataModel dataModel = new PoPDataModel(popFromAdapter);
            Pops.Add(dataModel);
            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<Boolean> UpdatePoPFromAdapter(Int32 popId, ProjectAdapterPoPModel popFromAdapter)
        {
            PoPDataModel dataModel = await Pops.FirstAsync(x => x.Id == popId);
            dataModel.Update(popFromAdapter);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfBranchableExistsByAdapterId(String branchableAdapterId)
        {
            Int32 result = await Branchables.CountAsync(x => x.ProjectId == branchableAdapterId);
            return result > 0;
        }

        public async Task<Boolean> DeletePopById(Int32 popId)
        {
            PoPDataModel dataModel = await Pops.FirstAsync(x => x.Id == popId);
            Pops.Remove(dataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> UpdatePoPIdFromBranchable(String branchableAdapterId, Int32 popId)
        {
            BranchableDataModel branchable = await Branchables.FirstAsync(x => x.ProjectId == branchableAdapterId);
            branchable.PoPId = popId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IDictionary<String, Int32>> GetBranchablesWithProjectAdapterId()
        {
            Dictionary<String, Int32> result = await Branchables.ToDictionaryAsync(x => x.ProjectId, x => x.Id);
            return result;
        }

        public async Task<IDictionary<String, Int32>> GetCableFromBranchbaleWithProjectAdapterId(Int32 branchableId)
        {
            Dictionary<String, Int32> result = await FiberCables
                .Where(x => x.StartingAtBranchableleId.HasValue == true && x.StartingAtBranchableleId == branchableId)
                .ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);

            return result;
        }

        public async Task<Boolean> CheckIfCableTypeExistsByProjectAdapterId(String cableTypeProjectAdapterId)
        {
            Int32 result = await CableTypes.CountAsync(x => x.AdapterId == cableTypeProjectAdapterId);
            return result > 0;
        }

        public async Task<Int32> GetCableTypeIdByProjectAdapterId(String cableTypeProjectAdapterId)
        {
            Int32 result = await CableTypes.Where(x => x.AdapterId == cableTypeProjectAdapterId).Select(x => x.Id).FirstAsync();
            return result;
        }

        public async Task<Boolean> UpdateCableFromAdapter(Int32 cableId, ProjectAdapterCableModel cableItem, Int32 branchableId, Int32 cableTypeId)
        {
            FiberCableDataModel dataModel = await FiberCables.FirstAsync(x => x.Id == cableId);
            dataModel.StartingAtBranchableleId = branchableId;
            dataModel.Update(cableItem);
            dataModel.CableTypeId = cableTypeId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> AddCableFromAdapter(ProjectAdapterCableModel cableItem, Int32 branchableId, Int32 cableTypeId)
        {
            FiberCableDataModel dataModel = new FiberCableDataModel(cableItem);
            dataModel.CableTypeId = cableTypeId;
            dataModel.StartingAtBranchableleId = branchableId;

            FiberCables.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> DeleteCable(Int32 cableId)
        {
            Int32 changes = 0;

            List<FiberDataModel> fibers = await Fibers.Where(x => x.CableId == cableId).ToListAsync();
            List<Int32> fiberIds = fibers.Select(x => x.Id).ToList();

            List<FiberSpliceDataModel> splices = await Splices.Where(x => (x.FirstFiberId.HasValue == true && fiberIds.Contains(x.FirstFiberId.Value) == true) || (x.SecondFiberId.HasValue == true && fiberIds.Contains(x.SecondFiberId.Value) == true)).ToListAsync();
            Splices.RemoveRange(splices);
            changes += await SaveChangesAsync();

            Fibers.RemoveRange(fibers);
            changes += await SaveChangesAsync();

            FiberCableDataModel dataModel = await FiberCables.FirstAsync(x => x.Id == cableId);
            FiberCables.Remove(dataModel);

            changes += await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfBuildingExistsByProjectAdapterId(String buildingPorjectAdapterId)
        {
            Int32 result = await Buildings.CountAsync(x => x.ProjectId == buildingPorjectAdapterId);
            return result > 0;
        }

        public async Task<Int32> GetBuildingIdByProjectAdapterId(String buildingPorjectAdapterId)
        {
            Int32 result = await Buildings.Where(x => x.ProjectId == buildingPorjectAdapterId).Select(x => x.Id).FirstAsync();
            return result;
        }

        public async Task<Boolean> UpdateBuildingCableRelation(Int32 cableId, Int32 buildingId)
        {
            FiberCableDataModel oldCable = await FiberCables.FirstOrDefaultAsync(x => x.BuildingId == buildingId);
            if (oldCable != null)
            {
                oldCable.BuildingId = null;
                await SaveChangesAsync();
            }

            FiberCableDataModel fiberCableDataModel = await FiberCables.FirstAsync(x => x.Id == cableId);
            fiberCableDataModel.BuildingId = buildingId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<Int32>> GetCableIdsByProjectIds(IEnumerable<String> projectAdapterIds)
        {
            IEnumerable<Int32> result = await FiberCables.Where(x => projectAdapterIds.Contains(x.ProjectAdapterId)).Select(x => x.Id).ToListAsync();
            return result;
        }

        public async Task<Boolean> UpdatePoPCableRelation(Int32 popId, IEnumerable<Int32> cableIds)
        {
            Int32 counter = 0;
            Int32 updates = 0;

            foreach (Int32 cableId in cableIds)
            {
                FiberCableDataModel fiberCableDataModel = await FiberCables.FirstAsync(x => x.Id == cableId);
                fiberCableDataModel.StartingAdPopId = popId;

                counter += 1;
                if (counter == 10)
                {
                    updates += await SaveChangesAsync();
                    counter = 0;
                }
            }

            updates += await SaveChangesAsync();

            return updates > 0;
        }

        public async Task<IDictionary<String, Int32>> GetCablesWithFlatIDs()
        {
            Dictionary<String, Int32> result = await FiberCables.Where(x => x.FlatId.HasValue == true).ToDictionaryAsync(x => x.ProjectAdapterId, x => x.FlatId.Value);
            return result;
        }

        public async Task<Boolean> DeletePatchConnectionByFlatId(Int32 flatId)
        {
            List<FiberConnenctionDataModel> dataModels = await FiberConnenctions.Where(x => x.FlatId == flatId).ToListAsync();
            FiberConnenctions.RemoveRange(dataModels);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<Int32>> AddPatchConnectionByFlatId(Int32 flatId, IEnumerable<ProjectAdapterPatchModel> data)
        {
            List<FiberConnenctionDataModel> dataModels = data.Select(x => new FiberConnenctionDataModel(x, flatId)).ToList();
            FiberConnenctions.AddRange(dataModels);

            Int32 changes = await SaveChangesAsync();
            return dataModels.Select(x => x.Id).ToList();
        }

        public async Task<IDictionary<String, Int32>> GetSplicesFromBranchbaleWithProjectAdapterId(Int32 branchableId)
        {
            IDictionary<String, Int32> result = await Splices.Where(x => x.BranchableId == branchableId)
                .ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);

            return result;
        }

        public async Task<Boolean> CheckIfCableExistsByProjectId(String cableProjectAdapterId)
        {
            Int32 result = await FiberCables.CountAsync(x => x.ProjectAdapterId == cableProjectAdapterId);
            return result > 0;
        }

        public async Task<Int32> GetCableIdByProjectId(String cableProjectAdapterId)
        {
            Int32 result = await FiberCables.Where(x => x.ProjectAdapterId == cableProjectAdapterId).Select(x => x.Id).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfFiberExistsByProjectAdapterId(String projectAdapterId, Int32 cableId)
        {
            Int32 result = await Fibers.CountAsync(x => x.ProjectAdapterId == projectAdapterId && x.CableId == cableId);
            return result > 0;
        }

        public async Task<Boolean> UpdateFiber(ProjectAdapterFiberModel fiberProjectModel)
        {
            FiberDataModel dataModel = await Fibers.FirstAsync(x => x.ProjectAdapterId == fiberProjectModel.ProjectAdapterId);
            dataModel.Update(fiberProjectModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        private async Task<Boolean> CheckIfFiberHasOtherSplices(Int32 spliceId, Int32 fiberId)
        {
            Int32 otherSplicesOfFiberAmount = await Splices
                    .Where(x => x.Id != spliceId && (x.FirstFiberId == fiberId || x.SecondFiberId == fiberId)).CountAsync();

            return otherSplicesOfFiberAmount > 0;
        }

        public async Task<Boolean> DeleteSpliceAndFibers(Int32 spliceId)
        {
            FiberSpliceDataModel splice = await Splices.Include(x => x.FirstFiber).Include(x => x.SecondFiber).FirstAsync(x => x.Id == spliceId);
            Splices.Remove(splice);

            if (splice.FirstFiber != null)
            {
                if (await CheckIfFiberHasOtherSplices(spliceId, splice.FirstFiber.Id) == false)
                {
                    Fibers.Remove(splice.FirstFiber);
                }
            }
            if (splice.SecondFiber != null)
            {
                if (await CheckIfFiberHasOtherSplices(spliceId, splice.SecondFiber.Id) == false)
                {
                    Fibers.Remove(splice.SecondFiber);
                }
            }

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> UpdateSpliceFromAdapter(Int32 spliceId, ProjectAdapterSpliceModel spliceItem, Int32 branchableId)
        {
            FiberSpliceDataModel dataModel = await Splices.FirstAsync(x => x.Id == spliceId);
            dataModel.Update(spliceItem, branchableId);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> AddFiberFromAdapter(ProjectAdapterFiberModel fiberProjectModel, Int32 cableId)
        {
            FiberDataModel fiberModel = new FiberDataModel(fiberProjectModel, cableId);
            Fibers.Add(fiberModel);

            await SaveChangesAsync();
            return fiberModel.Id;
        }

        public async Task<Int32> AddSpliceFromAdapter(FiberSpliceCreateModel createModel)
        {
            FiberSpliceDataModel dataModel = new FiberSpliceDataModel(createModel);
            Splices.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> SetCableToFlatByBuildingId(Int32 buildingID)
        {
            Boolean changed = false;
            Int32 flatAmount = await Buildings.Where(x => x.Id == buildingID && x.FiberCable != null).Select(x => x.Flats.Count).FirstOrDefaultAsync();
            if (flatAmount != 1) { return false; }

            FlatDataModel flat = await Flats.Where(x => x.BuildingId == buildingID).FirstAsync();
            FiberCableDataModel cable = await FiberCables.Where(x => x.BuildingId == buildingID).FirstAsync();

            FiberCableDataModel oldCableModel = await FiberCables.FirstOrDefaultAsync(x => x.FlatId == flat.Id);
            if (oldCableModel != null)
            {
                if (oldCableModel.Id == cable.Id) { return false; }
                oldCableModel.StartingFlat = null;
            }

            cable.StartingFlat = flat;

            changed = await SaveChangesAsync() > 0;
            return changed;
        }

        public async Task<Boolean> UpdateBranchableCableRelation(Int32 branchableId, Int32 cableId)
        {
            FiberCableDataModel exitingRelation = await FiberCables.FirstOrDefaultAsync(x => x.MainCableForBranchableId == branchableId);
            if (exitingRelation != null)
            {
                exitingRelation.MainCableForBranchableId = null;
                await SaveChangesAsync();
            }

            FiberCableDataModel fiberCableDataModel = await FiberCables.FirstAsync(x => x.Id == cableId);
            fiberCableDataModel.MainCableForBranchableId = branchableId;
            fiberCableDataModel.StartingAtBranchableleId = branchableId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<ConstructionStageDetailsForEditModel> GetDetailsForEdit(Int32 constructionStageId)
        {
            ConstructionStageDetailsForEditModel result = await ConstructionStages.Select(x => new ConstructionStageDetailsForEditModel
            {
                Name = x.Name,
                Id = x.Id,
                Branchables = x.Branchables.Select(y => GetSimpleCabinetOverviewModel(y)),
                Buildings = x.Branchables.ToDictionary(y => y.Id, y => y.Buildings.Select(z => GetSimpleBuildingOverviewModel(z)))
            }).Where(x => x.Id == constructionStageId).FirstAsync();

            return result;
        }

        public async Task<IEnumerable<Int32>> GetBranchableIdsByConstructionStageId(Int32 id)
        {
            IEnumerable<Int32> result = await Branchables.Where(x => x.ConstructionStageId == id).Select(x => x.Id).ToListAsync();
            return result;
        }

        public async Task<Boolean> UpdateConstructionStage(ConstructionStageUpdateModel model)
        {
            ConstructionStageDataModel constructionStageDataModel = await ConstructionStages.FirstAsync(x => x.Id == model.Id);
            constructionStageDataModel.Name = model.Name;

            foreach (Int32 branchableId in model.AddedBranchables)
            {
                BranchableDataModel branchable = await Branchables.FirstAsync(x => x.Id == branchableId);
                branchable.ConstructionStage = constructionStageDataModel;
            }

            foreach (Int32 branchableId in model.RemovedBranchables)
            {
                BranchableDataModel branchable = await Branchables.FirstAsync(x => x.Id == branchableId);
                branchable.ConstructionStageId = null;
            }

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IDictionary<Int32, String>> GetAllCablesWithProjectAdapterId()
        {
            Dictionary<Int32, String> result = await FiberCables.ToDictionaryAsync(x => x.Id, x => x.ProjectAdapterId);
            return result;
        }

        public async Task<Int32> GetBranchableIdByProjectAdapterId(String projectAdapterId)
        {
            Int32 result = await Branchables.Where(x => x.ProjectId == projectAdapterId).Select(x => x.Id).FirstAsync();
            return result;
        }

        public async Task<Boolean> UpdateEndingForMainCable(Int32 cableId, Int32 branchableId)
        {
            FiberCableDataModel cable = await FiberCables.FirstAsync(x => x.Id == cableId);
            cable.EndingAtBranchableId = branchableId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IDictionary<String, Int32>> GetFibersWithProjectAdapterId()
        {

            Int32 chunkAmount = 5000;
            Int32 row = 0;

            IEnumerable<KeyValuePair<String, Int32>> result = new List<KeyValuePair<string, int>>(chunkAmount);

            Dictionary<String, Int32> partialResult = null;
            do
            {
                partialResult = await
                    Fibers.OrderBy(x => x.Id).Skip(row * chunkAmount)
                    .Take(chunkAmount)
                    .ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);

                result = result.Union(partialResult);

                row++;

            } while (partialResult.Count == chunkAmount);

            return result.ToDictionary(x => x.Key, x => x.Value);
        }

        public async Task<BranchableSpliceOverviewModel> GetBranchableSpliceOverview(Int32 branchableId)
        {
            SimpleBranchableWithGPSModel simpleBranchable = await Branchables.Where(x => x.Id == branchableId).Select(x => GetSimpleBranchableWithGPSModel(x)).FirstAsync();

            List<SpliceEntryModel> splices = await Splices.Include(x => x.FirstFiber).Include(x => x.SecondFiber).Where(x =>
                x.FirstFiber.Cable.StartingAtBranchableleId == branchableId ||
                x.SecondFiber.Cable.StartingAtBranchableleId == branchableId
                  ).OrderBy(x => x.Type).ThenBy(x => x.TrayNumber).ThenBy(x => x.NumberInTray).ThenBy(x => x.FirstFiber.Name).ThenBy(x => x.FirstFiber.FiberNumber)
                  .Select(x => GetSpliceEntryModel(x)).ToListAsync();

            return new BranchableSpliceOverviewModel
            {
                Branchable = simpleBranchable,
                Splices = splices,
            };
        }

        public async Task<Boolean> SetBranachableToCable(String projectAdapterId, Int32 branchableId)
        {
            FiberCableDataModel dataModel = await FiberCables.FirstAsync(x => x.ProjectAdapterId == projectAdapterId);
            dataModel.StartingAtBranchableleId = branchableId;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> DeleteCable(String projectAdapterId)
        {
            FiberCableDataModel dataModel = await FiberCables.FirstAsync(x => x.ProjectAdapterId == projectAdapterId);
            FiberCables.Remove(dataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfBranchablesExists(IEnumerable<Int32> ids)
        {
            Int32 amount = await Branchables.Select(x => x.Id).Intersect(ids).CountAsync();
            return amount == ids.Count();
        }

        public async Task<Boolean> CheckIfPopsExists(IEnumerable<Int32> ids)
        {
            Int32 amount = await Pops.Select(x => x.Id).Intersect(ids).CountAsync();
            return amount == ids.Count();
        }

        public async Task<Boolean> CheckIfCablesExists(IEnumerable<Int32> ids)
        {
            Int32 amount = await FiberCables.Select(x => x.Id).Intersect(ids).CountAsync();
            return amount == ids.Count();
        }

        public async Task<IDictionary<String, Int32>> GetCableProjectAdapterIds(IEnumerable<Int32> ids)
        {
            IDictionary<String, Int32> result = await FiberCables.Where(x => ids.Contains(x.Id)).ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);
            return result;
        }

        public async Task<IDictionary<String, Int32>> GetPopsWithProjectAdapterId(IEnumerable<Int32> ids)
        {
            IDictionary<String, Int32> result = await Pops.Where(x => ids.Contains(x.Id)).ToDictionaryAsync(x => x.ProjectAdapterId, x => x.Id);
            return result;
        }

        public async Task<IDictionary<String, Int32>> GetBranchablesWithProjectAdapterId(IEnumerable<Int32> ids)
        {
            IDictionary<String, Int32> result = await Branchables.Where(x => ids.Contains(x.Id)).ToDictionaryAsync(x => x.ProjectId, x => x.Id);
            return result;
        }

        public async Task<Int32?> GetFlatIdByCableId(Int32 cableId)
        {
            Int32? result = await FiberCables.Where(x => x.Id == cableId).Select(x => x.FlatId).FirstAsync();
            return result;
        }

        #region OverviewModels

        private SimpleFlatOverviewModel GetSimpleFlatModelOverviewModel(FlatDataModel flat)
        {
            return flat == null ? new SimpleFlatOverviewModel() : new SimpleFlatOverviewModel
            {
                Id = flat.Id,
                BuildingId = flat.BuildingId,
                Description = flat.Description,
                Floor = flat.Floor,
                Number = flat.Number,
            };
        }

        private SimpleCabinetOverviewModel GetSimpleCabinetOverviewModel(BranchableDataModel dataModel)
        {
            return new SimpleCabinetOverviewModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name
            };
        }

        private ConstructionStageOverviewModel GetConstructionStageOverviewModel(ConstructionStageDataModel dataModel)
        {
            return new ConstructionStageOverviewModel
            {
                CabinetAmount = dataModel.Branchables.OfType<CabinetDataModel>().Count(),
                SleeveAmount = dataModel.Branchables.OfType<SleeveDataModel>().Count(),
                Name = dataModel.Name,
                Id = dataModel.Id,
                CommercialUnits = dataModel.Branchables.Sum(x => x.Buildings.Sum(y => y.CommercialUnits)),
                HouseholdsUnits = dataModel.Branchables.Sum(x => x.Buildings.Sum(y => y.HouseholdUnits)),
                DoingPercentage = 0.0,
                Streets = new List<StreetSummeryViewModel>(),
            };
        }

        private SimpleBuildingOverviewModel GetSimpleBuildingOverviewModel(BuildingDataModel buildingData)
        {
            return new SimpleBuildingOverviewModel
            {
                Id = buildingData.Id,
                CommercialUnits = buildingData.CommercialUnits,
                HouseholdUnits = buildingData.HouseholdUnits,
                StreetName = buildingData.StreetName,
            };
        }

        private SimpleBranchableWithGPSModel GetSimpleBranchableWithGPSModel(BranchableDataModel branchable)
        {
            return new SimpleBranchableWithGPSModel
            {
                Id = branchable.Id,
                Coordinate = new GPSCoordinate { Latitude = branchable.Latitude, Longitude = branchable.Longitude, },
                Name = branchable.Name,
                Type = branchable is CabinetDataModel ? BranchableTypes.Cabinet : BranchableTypes.Sleeve,
            };
        }

        private ProjectAdapterCableTypeModel GetProjectAdapterCableModel(FiberCableTypeDataModel dataModel)
        {
            return dataModel != null ? new ProjectAdapterCableTypeModel
            {
                AdapterId = dataModel.AdapterId,
                FiberAmount = dataModel.FiberAmount,
                FiberPerBundle = dataModel.FiberPerBundle,
                Name = dataModel.Name
            } : null;
        }

        private SimpleBuildingOverviewWithGPSModel GetSimpleBuildingOverviewWithGPSModel(BuildingDataModel building)
        {
            return new SimpleBuildingOverviewWithGPSModel
            {
                Id = building.Id,
                CommercialUnits = building.CommercialUnits,
                HouseholdUnits = building.HouseholdUnits,
                StreetName = building.StreetName,
                Coordinate = new GPSCoordinate
                {
                    Latitude = building.Latitude,
                    Longitude = building.Longitude
                }
            };
        }

        private FiberEndpointModel GetFiberEndpointModel(FiberConnenctionDataModel connenctionDataModel)
        {
            return connenctionDataModel != null ? new FiberEndpointModel
            {
                Id = connenctionDataModel.Id,
                Caption = connenctionDataModel.Caption,
                ColumnNumber = connenctionDataModel.ColumnNumber,
                ModuleName = connenctionDataModel.ModuleName,
                RowNumber = connenctionDataModel.RowNumber,
            } : null;
        }

        private SimplePopOverviewWithGPSModel GetSimplePopOverviewWithGPSModel(PoPDataModel dataModel)
        {
            return dataModel != null ? new SimplePopOverviewWithGPSModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name,
                Location = new GPSCoordinate { Latitude = dataModel.Latitude, Longitude = dataModel.Longitude },
            } : null;
        }

        private SpliceCableInfoModel GetSpliceCableInfoModel(FiberCableDataModel dataModel)
        {
            return dataModel != null ? new SpliceCableInfoModel
            {
                Id = dataModel.Id,
                Name = dataModel.Name,
                FiberAmount = dataModel.CableType != null ? dataModel.CableType.FiberAmount : -dataModel.CableTypeId,
            } : null;
        }

        private SpliceEntryModel GetSpliceEntryModel(FiberSpliceDataModel dataModel)
        {
            return dataModel != null ? new SpliceEntryModel
            {
                Id = dataModel.Id,
                Number = dataModel.NumberInTray,
                Tray = dataModel.TrayNumber,
                FirstFiber = GetFiberOverviewModel(dataModel.FirstFiber),
                SecondFiber = GetFiberOverviewModel(dataModel.SecondFiber),
                Type = dataModel.Type
            } : null;
        }

        private FiberOverviewModel GetFiberOverviewModel(FiberDataModel dataModel)
        {
            return dataModel != null ? new FiberOverviewModel
            {
                BundleNumber = dataModel.BundleNumber,
                Color = dataModel.Color,
                FiberNumber = dataModel.FiberNumber,
                Id = dataModel.Id,
                Name = dataModel.Name,
                NumberinBundle = dataModel.NumberinBundle,
                CableId = dataModel.CableId,

            } : null;
        }

        #endregion
    }
}
