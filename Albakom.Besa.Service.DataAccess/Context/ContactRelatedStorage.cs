﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Boolean> CheckIfContactExists(Int32 contactId)
        {
            Int32 result = await ContactInfos.CountAsync(x => x.Id == contactId);
            return result > 0;
        }

        public async Task<Boolean> EditContact(PersonInfo model)
        {
            ContactInfoDataModel dataModel = await ContactInfos.FirstAsync(x => x.Id == model.Id);
            dataModel.Update(model);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<PersonInfo> GetContactById(Int32 id)
        {
            PersonInfo result = await ContactInfos.Where(x => x.Id == id).Select(x => GetPersonInfo(x)).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfContactIsInUse(Int32 contactId)
        {
            Int32 usedByConnectionJob = await ConnectBuildingJobs
                 .CountAsync(x => x.OwnerContactId == contactId || x.CustomerContactId == contactId || x.WorkmanContactId == contactId || x.ArchitectContactId == contactId);

            Int32 requestUsed = await CustomerRequests
                 .CountAsync(x => x.PropertyOwnerPersonId == contactId || x.CustomerPersonId == contactId || x.WorkmanPersonId == contactId || x.ArchitectPersonId == contactId);

            Int32 sum = usedByConnectionJob + requestUsed;
            return sum > 0;
        }

        public async Task<Boolean> DeleteContact(Int32 contactId)
        {
            ContactInfoDataModel dataModel = await ContactInfos.FirstAsync(x => x.Id == contactId);
            ContactInfos.Remove(dataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<PersonInfo>> GetContactsSortByLastname(Int32 start, Int32 amount)
        {
            IEnumerable<PersonInfo> result = await ContactInfos.OrderBy(x => x.Lastname).Select(x => GetPersonInfo(x)).Skip(start).Take(amount).ToListAsync();
            return result;
        }

        private IQueryable<ContactInfoDataModel> GetConactsBySurnameOrLastname(String surname, String lastname)
        {
            String parsedSurname = surname.Trim().ToLower();
            String parsedLastname = lastname.Trim().ToLower();

            IQueryable<ContactInfoDataModel> contacts = ContactInfos;
            if (String.IsNullOrEmpty(parsedSurname) == false)
            {
                contacts = contacts.Where(x => x.Surname.ToLower() == parsedSurname);
            }
            if (String.IsNullOrEmpty(parsedLastname) == false)
            {
                contacts = contacts.Where(x => x.Lastname.ToLower() == parsedLastname);
            }

            return contacts;
        }

        public async Task<Boolean> CheckIfContactExists(String surname, String lastname)
        {
            IQueryable<ContactInfoDataModel> query = GetConactsBySurnameOrLastname(surname, lastname);
            Int32 result = await query.CountAsync();
            return result > 0;
        }

        public async Task<PersonInfo> GetContactByName(String surname, String lastname)
        {
            IQueryable<ContactInfoDataModel> query = GetConactsBySurnameOrLastname(surname, lastname);
            PersonInfo result = await query.Select(x => GetPersonInfo(x)).FirstAsync();
            return result;
        }

    }
}
