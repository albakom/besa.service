﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Boolean> CheckIfFileExists(IEnumerable<Int32> fileIds)
        {
            Int32 amount = await Files.Select(x => x.Id).Intersect(fileIds).CountAsync();
            return amount == fileIds.Count();
        }

        public async Task<Boolean> CheckIfFileExists(Int32 fileId)
        {
            Int32 amount = await Files.CountAsync(x => x.Id == fileId);
            return amount > 0;
        }

        public async Task<Int32> CreateFile(FileCreateModel model)
        {
            FileDataModel dataModel = new FileDataModel(model);
            Files.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<FileModel> GetFileInfo(Int32 fileId)
        {
            FileModel fileModel = await Files.Where(x => x.Id == fileId).Select(x => new FileModel
            {
                Id = x.Id,
                Extention = x.Extention,
                MimeType = x.MimeType,
                Type = x.Type,
                Name = x.Name,
                Size = x.Size,
                StorageUrl = x.StorageUrl,
            }).FirstAsync();

            return fileModel;
        }

        public async Task<IEnumerable<String>> GetFileUrlsByJobId(Int32 jobId)
        {
            IEnumerable<String> result = await Files.Where(x => x.JobId == jobId).Select(x => x.StorageUrl).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<FileOverviewModel>> GetFilesSortByName(Int32 start, Int32 amount)
        {
            IEnumerable<FileOverviewModel> result = await(from file in Files
                                                          orderby file.Name
                                                          where file.JobId.HasValue == false
                                                          select GetFileFileOverviewModel(file)
                                               ).Skip(start).Take(amount).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<FileOverviewModel>> SearchFiles(String query, Int32 start, Int32 amount)
        {
            String parsedQuery = $"%{query.ToLower()}%";

            IEnumerable<FileOverviewModel> result = await (from file in Files
                                                           where file.JobId.HasValue == false && EF.Functions.Like(file.Name, parsedQuery) == true
                                                           orderby file.Name
                                                           select GetFileFileOverviewModel(file)
                                                           ).Skip(start).Take(amount).ToListAsync();

            return result;
        }

        public async Task<FileDetailModel> GetFileDetails(Int32 fileId)
        {
            FileDetailModel result = await Files.Include(x => x.AccessList).Where(x => x.Id == fileId).Select(x => new FileDetailModel
            {
                Id = x.Id,
                Extention = x.Extention,
                Name = x.Name,
                Size = x.Size,
                Type = x.Type,
                AccessControl = x.AccessList.Select(y => new FileAccessOverviewModel
                {
                    ObjectId = y.ObjectId,
                    ObjectType = y.ObjectType
                }).ToList(),
            }).FirstAsync();

            foreach (FileAccessOverviewModel item in result.AccessControl)
            {
                if (item.ObjectType == FileAccessObjects.Building)
                {
                    item.ObjectName = await Buildings.Where(x => x.Id == item.ObjectId).Select(x => x.StreetName).FirstOrDefaultAsync();
                }
                else if (item.ObjectType == FileAccessObjects.BranchOffJob)
                {
                    item.ObjectName = await BranchOffJobs.Include(x => x.Building).Where(x => x.Id == item.ObjectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                }
                else if (item.ObjectType == FileAccessObjects.HouseConnectionJob)
                {
                    item.ObjectName = await ConnectBuildingJobs.Include(x => x.Building).Where(x => x.Id == item.ObjectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                }
                else if (item.ObjectType == FileAccessObjects.InjectJob)
                {
                    item.ObjectName = await InjectJobs.Include(x => x.Building).Where(x => x.Id == item.ObjectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                }
                else if(item.ObjectType == FileAccessObjects.ActivationJob)
                {
                    item.ObjectName = await ActivationJobs.Where(x => x.Id == item.ObjectId).Select(x => x.Flat.Building.StreetName).FirstOrDefaultAsync();
                }
            }

            return result;
        }

        public async Task<Boolean> RenameFile(Int32 fileId, String name)
        {
            FileDataModel file = await Files.FirstAsync(x => x.Id == fileId);
            file.Name = name;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> DeleteFileAccess(Int32 fileId)
        {
            List<FileAccessEntryDataModel> entries = await FileAccessEntries.Where(x => x.FileId == fileId).ToListAsync();
            FileAccessEntries.RemoveRange(entries);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<ICollection<FileAccessWithIdModel>> GetFileAccessList(Int32 fileId)
        {
            ICollection<FileAccessWithIdModel> result = await FileAccessEntries.Where(x => x.FileId == fileId).Select(x => GetFileAccessWithIdModel(x)).ToListAsync();
            return result;
        }

        public async Task<Int32> AddFileAccessEntry(Int32 fileId, FileAccessModel item)
        {
            FileAccessEntryDataModel fileAccessEntryDataModel = new FileAccessEntryDataModel
            {
                FileId = fileId,
                ObjectId = item.ObjectId,
                ObjectType = item.ObjectType,
            };

            FileAccessEntries.Add(fileAccessEntryDataModel);
            await SaveChangesAsync();

            return fileAccessEntryDataModel.Id;
        }

        public async Task<Boolean> DeleteFileAcessEntries(IEnumerable<Int32> fileAccessEntryIds)
        {
            List<FileAccessEntryDataModel> entries = await FileAccessEntries.Where(x => fileAccessEntryIds.Contains(x.Id)).ToListAsync();
            FileAccessEntries.RemoveRange(entries);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<FileAccessWithIdModel>> GetAllAccessEntries()
        {
            ICollection<FileAccessWithIdModel> result = await FileAccessEntries.Select(x => GetFileAccessWithIdModel(x)).ToListAsync();
            return result;
        }

        public async Task<Boolean> DeleteFileAcessEntry(Int32 id)
        {
            FileAccessEntryDataModel entry = await FileAccessEntries.FirstAsync(x => x.Id == id);
            FileAccessEntries.Remove(entry);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        private async Task<IEnumerable<FileOverviewModel>> GetFileAllowedForBuilding(Int32 buildingId)
        {
            IEnumerable<FileOverviewModel> files = await FileAccessEntries.Include(x => x.File)
                .Where(x => x.ObjectType == FileAccessObjects.Building && x.ObjectId == buildingId)
                .Select(x => x.File).Distinct().Select(x => GetFileFileOverviewModel(x)).ToListAsync();

            return files;
        }

        private async Task<IEnumerable<FileOverviewModel>> GetFileAllowedForObject(Int32 objectId, FileAccessObjects objectType)
        {
            IEnumerable<FileOverviewModel> files = await FileAccessEntries.Include(x => x.File)
                .Where(x => x.ObjectType == objectType && x.ObjectId == objectId)
                .Select(x => x.File).Distinct().Select(x => GetFileFileOverviewModel(x)).ToListAsync();

            return files;
        }

        private async Task<IEnumerable<FileOverviewModel>> GetFilesAllowedForJob(Int32? buildingId, Int32 jobId, JobTypes type)
        {
            FileAccessObjects accessObjectType = FileAccessObjects.BranchOffJob;
            switch (type)
            {
                case JobTypes.BranchOff:
                    accessObjectType = FileAccessObjects.BranchOffJob;
                    break;
                case JobTypes.HouseConnenction:
                    accessObjectType = FileAccessObjects.HouseConnectionJob;
                    break;
                case JobTypes.Inject:
                    accessObjectType = FileAccessObjects.InjectJob;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    accessObjectType = FileAccessObjects.PrepareBranchableJob;
                    break;
                case JobTypes.SpliceInBranchable:
                    accessObjectType = FileAccessObjects.SpliceInBranchableJob;
                    break;
                case JobTypes.SpliceInPoP:
                    accessObjectType = FileAccessObjects.SpliceInPopJob;
                    break;
                case JobTypes.ActivationJob:
                    accessObjectType = FileAccessObjects.ActivationJob;
                    break;
                default:
                    break;
            }

            List<FileOverviewModel> result = new List<FileOverviewModel>();
            if (buildingId.HasValue == true)
            {
                IEnumerable<FileOverviewModel> files = await GetFileAllowedForBuilding(buildingId.Value);
                result.AddRange(files);
            }

            IEnumerable<FileOverviewModel> jobRelatedFiles = await GetFileAllowedForObject(jobId, accessObjectType);
            result.AddRange(jobRelatedFiles);

            return result;
        }

        public async Task<Boolean> DeleteFile(Int32 fileId)
        {
            FileDataModel fileDataModel = await Files.FirstAsync(x => x.Id == fileId);
            Files.Remove(fileDataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<String> GetFileUrlById(Int32 fileId)
        {
            String result = await Files.Where(x => x.Id == fileId).Select(x => x.StorageUrl).FirstAsync();
            return result;
        }

        #region Overview-Models

        private FileAccessWithIdModel GetFileAccessWithIdModel(FileAccessEntryDataModel entryDataModel)
        {
            return new FileAccessWithIdModel
            {
                Id = entryDataModel.Id,
                ObjectId = entryDataModel.ObjectId,
                ObjectType = entryDataModel.ObjectType,
            };
        }

        private FileOverviewModel GetFileFileOverviewModel(FileDataModel file)
        {
            return new FileOverviewModel
            {
                Id = file.Id,
                Extention = file.Extention,
                Name = file.Name,
                Size = file.Size,
                Type = file.Type,
            };
        }

        private IEnumerable<FileOverviewModel> GetFileFileOverviewModel(IEnumerable<FileDataModel> files)
        {
            return files.Select(x => new FileOverviewModel
            {
                Id = x.Id,
                Extention = x.Extention,
                Name = x.Name,
                Size = x.Size,
                Type = x.Type,
            });
        }

        #endregion

    }
}
