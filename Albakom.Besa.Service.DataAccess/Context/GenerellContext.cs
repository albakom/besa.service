﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : DbContext, IBesaDataStorage
    {
        #region Properties

        public IStorageContraints Constraints { get; private set; }

        #endregion

        #region Sets

        public virtual DbSet<UserDataModel> Users { get; set; }
        public virtual DbSet<UserAccessRequestDataModel> UserAccessRequests { get; set; }
        public virtual DbSet<CompanyDataModel> Companies { get; set; }
        public virtual DbSet<BuildingDataModel> Buildings { get; set; }
        public virtual DbSet<CabinetDataModel> StreetCabintes { get; set; }
        public virtual DbSet<SleeveDataModel> Sleeves { get; set; }
        public virtual DbSet<ConstructionStageDataModel> ConstructionStages { get; set; }
        public virtual DbSet<BranchableDataModel> Branchables { get; set; }
        public virtual DbSet<JobDataModel> Jobs { get; set; }
        public virtual DbSet<CollectionJobDataModel> CollectionJobs { get; set; }
        public virtual DbSet<BranchOffJobDataModel> BranchOffJobs { get; set; }
        public virtual DbSet<ConnectBuildingJobDataModel> ConnectBuildingJobs { get; set; }
        public virtual DbSet<InjectJobDataModel> InjectJobs { get; set; }
        public virtual DbSet<ArticleDataModel> Articles { get; set; }
        public virtual DbSet<FlatDataModel> Flats { get; set; }
        public virtual DbSet<ContactInfoDataModel> ContactInfos { get; set; }
        public virtual DbSet<CustomerConnenctionRequestDataModel> CustomerRequests { get; set; }
        public virtual DbSet<FileDataModel> Files { get; set; }
        public virtual DbSet<FileAccessEntryDataModel> FileAccessEntries { get; set; }
        public virtual DbSet<FiberCableTypeDataModel> CableTypes { get; set; }
        public virtual DbSet<FiberCableDataModel> FiberCables { get; set; }
        public virtual DbSet<FiberSpliceDataModel> Splices { get; set; }

        public virtual DbSet<FinishedJobDataModel> FinishedJobs { get; set; }
        public virtual DbSet<ConnectBuildingFinishedDataModel> FinishedConnectionBuildingJobs { get; set; }
        public virtual DbSet<BranchOffJobFinishedDataModel> FinishedBranchOffJobs { get; set; }
        public virtual DbSet<InjectJobFinishedDataModel> FinishedInjectJobs { get; set; }

        public virtual DbSet<SpliceBranchableJobFinishedDataModel> FinishedSpliceBranchableJobs { get; set; }
        public virtual DbSet<SpliceBranchableJobDataModel> SpliceBranchableJobs { get; set; }

        public virtual DbSet<PrepareBranchableJobDataModel> PrepareBranchableForSpliceJobs { get; set; }
        public virtual DbSet<PrepareBranchableJobFinishedDataModel> FinishedPrepareBranchableForSpliceJobs { get; set; }

        public virtual DbSet<SpliceODFJobDataModel> SpliceODFJobs { get; set; }
        public virtual DbSet<SpliceODFJobFinishedDataModel> FinishedSpliceODFJobs { get; set; }

        public virtual DbSet<ActivationJobDataModel> ActivationJobs { get; set; }
        public virtual DbSet<ActivationJobFinishedDataModel> FinishedActivationJobs { get; set; }

        public virtual DbSet<ProcedureDataModel> Procedures { get; set; }
        public virtual DbSet<ProcedureTimeLineElementDataModel> ProcedureTimeLineElements { get; set; }

        public virtual DbSet<BuildingConnectionProcedureDataModel> BuildingConnectionProcedures { get; set; }
        public virtual DbSet<CustomerConnectionProcedureDataModel> CustomerConnectionProcedures { get; set; }

        public virtual DbSet<IventoryJobArticleRelationDataModel> InventoryJobArticleRelations { get; set; }
        public virtual DbSet<InventoryJobDataModel> InventoryJobs { get; set; }

        public virtual DbSet<PoPDataModel> Pops { get; set; }
        public virtual DbSet<FiberDataModel> Fibers { get; set; }
        public virtual DbSet<FiberConnenctionDataModel> FiberConnenctions { get; set; }

        public virtual DbSet<InventoryJobFinishedDataModel> FinishedInventoryJobs { get; set; }
        public virtual DbSet<ProtocolEntryDataModel> ProtocolEntries { get; set; }

        public virtual DbSet<ContactAfterFinishedJobDataModel> ContactAfterFinishedJobs { get; set; }
        public virtual DbSet<ContactAfterFinishedJobFinishedDataModel> FinishedContactAfterFinishedJobs { get; set; }

        public virtual DbSet<UpdateTaskDataModel> UpdateTasks { get; set; }
        public virtual DbSet<UpdateTaskElementDataModel> UpdateTaskElements { get; set; }

        public virtual DbSet<ProtocolUserInformDataModel> ProtocolInformEntries { get; set; }
        public virtual DbSet<MessageDataModel> Messages { get; set; }

        #endregion

        #region Constructor

        public BesaDataStorage(DbContextOptions<BesaDataStorage> options) : base(options)
        {
            Constraints = new BesaDataStorageContraints();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<JobDataModel>().HasOne(p => p.FinishedModel).WithOne(i => i.Job).HasForeignKey<FinishedJobDataModel>(b => b.JobId).IsRequired(true);

            modelBuilder.Entity<FinishedJobDataModel>().HasOne(u => u.DoneBy).WithMany(x => x.FinishedJobs).HasForeignKey(x => x.JobberId).IsRequired(true);
            modelBuilder.Entity<FinishedJobDataModel>().HasOne(u => u.Accepter).WithMany(x => x.AcceptedJobs).HasForeignKey(x => x.AccepterId).IsRequired(false);

            modelBuilder.Entity<BuildingDataModel>().HasOne(u => u.ConnectionJob).WithOne(x => x.Building).HasForeignKey<ConnectBuildingJobDataModel>(x => x.BuildingId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<BuildingDataModel>().HasOne(u => u.BranchOffJob).WithOne(x => x.Building).HasForeignKey<BranchOffJobDataModel>(x => x.BuildingId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<BuildingDataModel>().HasOne(u => u.InjectJob).WithOne(x => x.Building).HasForeignKey<InjectJobDataModel>(x => x.BuildingId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<BuildingDataModel>().HasOne(u => u.FiberCable).WithOne(x => x.ForBuilding).HasForeignKey<FiberCableDataModel>(x => x.BuildingId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<FlatDataModel>().HasOne(u => u.BranchableSpliceJob).WithOne(x => x.Flat).HasForeignKey<SpliceBranchableJobDataModel>(x => x.FlatId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<FiberCableDataModel>().HasOne(u => u.SpliceODFJob).WithOne(x => x.Cable).HasForeignKey<SpliceODFJobDataModel>(x => x.CableId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<BranchableDataModel>().HasOne(u => u.PrepareJob).WithOne(x => x.Branchable).HasForeignKey<PrepareBranchableJobDataModel>(x => x.BranchableId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<FlatDataModel>().HasOne(u => u.ActivationJob).WithOne(x => x.Flat).HasForeignKey<ActivationJobDataModel>(x => x.FlatId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<FlatDataModel>().HasOne(u => u.Cable).WithOne(x => x.StartingFlat).HasForeignKey<FiberCableDataModel>(x => x.FlatId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<BranchableDataModel>().HasOne(u => u.MainCable).WithOne(x => x.MainCableForBranchable).HasForeignKey<FiberCableDataModel>(x => x.MainCableForBranchableId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<BranchableDataModel>().HasMany(x => x.StartingCables).WithOne(x => x.StartingAtBranchable).HasForeignKey(x => x.StartingAtBranchableleId).IsRequired(false);
            modelBuilder.Entity<BranchableDataModel>().HasMany(x => x.EndingCables).WithOne(x => x.EndingAtBranchable).HasForeignKey(x => x.EndingAtBranchableId).IsRequired(false);

            modelBuilder.Entity<FiberDataModel>().HasMany(x => x.RelatedSplicesAsFirstFiber).WithOne(x => x.FirstFiber).HasForeignKey(x => x.FirstFiberId).IsRequired(false);
            modelBuilder.Entity<FiberDataModel>().HasMany(x => x.RelatedSplicesAsLastFiber).WithOne(x => x.SecondFiber).HasForeignKey(x => x.SecondFiberId).IsRequired(false);

            modelBuilder.Entity<FiberDataModel>().HasIndex(x => x.ProjectAdapterId).IsUnique(true);
            modelBuilder.Entity<FiberSpliceDataModel>().HasIndex(x => x.ProjectAdapterId).IsUnique(true);
            modelBuilder.Entity<FiberCableDataModel>().HasIndex(x => x.ProjectAdapterId).IsUnique(true);
            modelBuilder.Entity<FiberCableTypeDataModel>().HasIndex(x => x.AdapterId).IsUnique(true);
            modelBuilder.Entity<BranchableDataModel>().HasIndex(x => x.ProjectId).IsUnique(true);
            modelBuilder.Entity<BuildingDataModel>().HasIndex(x => x.ProjectId).IsUnique(true);
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<CustomerDataModel>()
        //        .HasOne(cust => cust.ConnenctionJob)
        //        .WithOne(job => job.Customer)
        //        .HasForeignKey<CustomerConnenctionJobDataModel>(x => x.CustomerId)
        //         .IsRequired(false)
        //        .OnDelete(DeleteBehavior.SetNull);

        //    modelBuilder.Entity<CustomerConnenctionJobDataModel>()
        //        .HasOne(job => job.Customer)
        //        .WithOne(customer => customer.ConnenctionJob)
        //        .HasForeignKey<CustomerDataModel>(x => x.ConnenctionJobId)
        //       .IsRequired(false)
        //        .OnDelete(DeleteBehavior.SetNull);

        //    base.OnModelCreating(modelBuilder);
        //}

        #endregion

        #region Restore

        public void Restore()
        {
            List<EntityEntry> changedEntriesCopy = base.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (EntityEntry entity in changedEntriesCopy)
            {
                this.Entry(entity.Entity).State = EntityState.Detached;
            }
        }

        #endregion
    }
}
