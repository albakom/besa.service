﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        private static Dictionary<Int32, JobTypes> _jobTypeDictionary = new Dictionary<Int32, JobTypes>();

        private class JobOverviewPreResult
        {
            public Int32 Id { get; set; }
            public JobDataModel Data { get; set; }
            public JobStates State { get; set; }

            public JobOverviewPreResult()
            {

            }
        }

        public async Task<IEnumerable<Int32>> GetBuildingIdsWithoutBranchOffJob(Int32 constructionStageId)
        {
            IEnumerable<Int32> buildingIds = await Buildings.Include(x => x.Branchable).Include(x => x.BranchOffJob)
                .Where(x => x.Branchable.ConstructionStageId == constructionStageId && x.BranchOffJob == null)
                .Select(x => x.Id).ToListAsync();

            return buildingIds;
        }

        public async Task CreateBranchOffJobs(IEnumerable<BranchOffJobCreateModel> jobsToCreate)
        {
            Dictionary<Int32, BranchOffJobCreateModel> buildingIds = jobsToCreate.ToDictionary(x => x.BuildingId, x => x);
            IEnumerable<BuildingDataModel> building = await Buildings.Where(x => buildingIds.Keys.Contains(x.Id) == true).ToListAsync();

            foreach (BuildingDataModel item in building)
            {
                BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel(buildingIds[item.Id]);
                item.BranchOffJob = jobDataModel;

                BranchOffJobs.Add(jobDataModel);
            }

            await SaveChangesAsync();
        }

        public async Task<Boolean> CheckIfJobsExists(IEnumerable<Int32> jobIds)
        {
            Int32 amount = await Jobs.Select(x => x.Id).Intersect(jobIds).CountAsync();
            return amount == jobIds.Count();
        }

        public async Task<Boolean> CheckIfJobsAreUnbound(IEnumerable<Int32> jobIds)
        {
            Int32 amount = await Jobs.Where(x => x.CollectionId == null).Select(x => x.Id).Intersect(jobIds).CountAsync();
            return amount == jobIds.Count();
        }


        public async Task<Boolean> CheckIfJobExists(Int32 jobId)
        {
            Int32 amount = await Jobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfJobIsFinished(Int32 jobId)
        {
            Int32 amount = await FinishedJobs.CountAsync(x => x.JobId == jobId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfJobIsAcknowledged(Int32 jobId)
        {
            Int32 amount = await FinishedJobs.CountAsync(x => x.JobId == jobId && x.AccepterId.HasValue == true);
            return amount > 0;
        }

        public async Task<Int32> BindJobs(IEnumerable<Int32> jobIds, Int32 companyId, DateTimeOffset endDate, String name)
        {
            CollectionJobDataModel collectionJob = new CollectionJobDataModel
            {
                CompanyId = companyId,
                FinishedTill = endDate,
                Name = name,
            };

            CollectionJobs.Add(collectionJob);

            IEnumerable<JobDataModel> jobs = await Jobs.Where(x => jobIds.Contains(x.Id) == true).ToListAsync();

            foreach (JobDataModel item in jobs)
            {
                item.Collection = collectionJob;
            }

            await SaveChangesAsync();

            return collectionJob.Id;
        }

        public async Task<IEnumerable<PersonalJobOverviewModel>> GetJobOverviewForUser(String authId)
        {
            Int32 companyId = await Users.Where(x => x.AuthServiceId == authId).Select(x => x.CompanyId.Value).FirstAsync();

            IEnumerable<PersonalJobOverviewModel> result = await (from collectionJob in CollectionJobs
                                                                  where collectionJob.CompanyId == companyId
                                                                  orderby collectionJob.FinishedTill
                                                                  select new PersonalJobOverviewModel
                                                                  {
                                                                      CollectionJobId = collectionJob.Id,
                                                                      EndDate = collectionJob.FinishedTill,
                                                                      TaskAmount = collectionJob.Jobs.Count,
                                                                      Name = collectionJob.Name,
                                                                  }).ToListAsync();

            foreach (PersonalJobOverviewModel item in result)
            {
                if (await BranchOffJobs.CountAsync(x => x.CollectionId == item.CollectionJobId) > 0)
                {
                    item.JobType = JobTypes.BranchOff;
                }
            }

            return result;
        }

        public async Task<Boolean> CheckIfBranchOffJobExist(Int32 jobId)
        {
            Int32 amount = await BranchOffJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        private async Task<JobStates> GetJobState(Int32 jobId)
        {
            JobStates state = JobStates.Open;

            if (await CheckIfJobIsFinished(jobId) == true)
            {
                if (await CheckIfJobIsAcknowledged(jobId) == true)
                {
                    state = JobStates.Acknowledged;
                }
                else
                {
                    state = JobStates.Finished;
                }
            }

            return state;
        }

        public async Task<BranchOffJobDetailModel> GetBranchOffJobDetails(Int32 jobId)
        {
            BranchOffJobDetailModel result = await (from job in BranchOffJobs.Include(x => x.Building)
                                                    where job.Id == jobId
                                                    select new BranchOffJobDetailModel
                                                    {
                                                        Id = job.Id,
                                                        BuildingInfo = GetBuildingInfoForBranchOffModel(job.Building)
                                                    }).FirstAsync();

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(result.BuildingInfo.Id, jobId, JobTypes.BranchOff);

            return result;
        }

        public async Task<Boolean> CheckIfCollectionJobExists(Int32 collectionJobId)
        {
            Int32 amount = await CollectionJobs.CountAsync(x => x.Id == collectionJobId);
            return amount > 0;
        }

        private async Task<SimpleJobOverview> GetSimpleJobOverview(JobOverviewPreResult preResult)
        {
            String name = String.Empty;
            JobTypes type = JobTypes.BranchOff;

            if (preResult.Data is BranchOffJobDataModel)
            {
                Int32 buildingId = ((BranchOffJobDataModel)preResult.Data).BuildingId;
                name = await Buildings.Where(x => x.Id == buildingId).Select(x => x.StreetName).FirstAsync();
                type = JobTypes.BranchOff;
            }
            else if (preResult.Data is ConnectBuildingJobDataModel)
            {
                Int32 buildingId = ((ConnectBuildingJobDataModel)preResult.Data).BuildingId;
                name = await Buildings.Where(x => x.Id == buildingId).Select(x => x.StreetName).FirstAsync();
                type = JobTypes.HouseConnenction;
            }
            else if (preResult.Data is InjectJobDataModel)
            {
                Int32 buildingId = ((InjectJobDataModel)preResult.Data).BuildingId;
                name = await Buildings.Where(x => x.Id == buildingId).Select(x => x.StreetName).FirstAsync();
                type = JobTypes.Inject;
            }
            else if (preResult.Data is SpliceBranchableJobDataModel)
            {
                Int32 flatId = ((SpliceBranchableJobDataModel)preResult.Data).FlatId;
                name = await Flats.Where(x => x.Id == flatId).Select(x => $"{x.Building.StreetName} - {x.Number}").FirstAsync();
                type = JobTypes.SpliceInBranchable;
            }
            else if (preResult.Data is ActivationJobDataModel)
            {
                Int32 flatId = ((ActivationJobDataModel)preResult.Data).FlatId;
                name = await Flats.Where(x => x.Id == flatId).Select(x => $"{x.Building.StreetName} - {x.Number}").FirstAsync();
                type = JobTypes.ActivationJob;
            }
            else if (preResult.Data is PrepareBranchableJobDataModel)
            {
                Int32 branchableId = ((PrepareBranchableJobDataModel)preResult.Data).BranchableId;
                name = await Branchables.Where(x => x.Id == branchableId).Select(x => x.Name).FirstAsync();
                type = JobTypes.PrepareBranchableForSplice;
            }
            else if (preResult.Data is SpliceODFJobDataModel)
            {
                Int32 mainCableId = ((SpliceODFJobDataModel)preResult.Data).CableId;
                name = await FiberCables.Where(x => x.Id == mainCableId).Select(x => $"{x.Name} - {x.StartingAtPop.Name}").FirstAsync();
                type = JobTypes.SpliceInPoP;
            }

            SimpleJobOverview job = new SimpleJobOverview
            {
                Id = preResult.Id,
                Name = name,
                JobType = type,
                State = preResult.State,
            };

            return job;

        }

        private Task<SimpleJobOverview> GetSimpleJobOverview(JobDataModel jobData)
        {
            JobStates state = jobData.FinishedModel == null ? JobStates.Open : (jobData.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished);
            return GetSimpleJobOverview(new JobOverviewPreResult { Data = jobData, Id = jobData.Id, State = state });
        }

        public async Task<SimpleJobOverview> GetSimpleJobOverviewById(Int32 jobId)
        {
            JobDataModel jobData = await Jobs.Include(x => x.FinishedModel).FirstAsync(x => x.Id == jobId);
            SimpleJobOverview result = await GetSimpleJobOverview(jobData);
            return result;
        }

        public async Task<CollectionJobDetailsModel> GetCollectionJobDetails(Int32 collectionJobId)
        {
            var preResult = await (from collJob in CollectionJobs
                                   where collJob.Id == collectionJobId
                                   select new
                                   {
                                       Id = collJob.Id,
                                       End = collJob.FinishedTill,
                                       Name = collJob.Name,
                                       Jobs = collJob.Jobs.Select(x => new JobOverviewPreResult
                                       {
                                           Id = x.Id,
                                           Data = x,
                                           State = x.FinishedModel == null ? JobStates.Open : (x.FinishedModel.AccepterId.HasValue == true ? JobStates.Acknowledged : JobStates.Finished),
                                       })
                                   }).FirstAsync();

            CollectionJobDetailsModel result = new CollectionJobDetailsModel
            {
                Id = preResult.Id,
                Name = preResult.Name,
                End = preResult.End,
            };

            List<SimpleJobOverview> jobs = new List<SimpleJobOverview>();
            foreach (JobOverviewPreResult item in preResult.Jobs)
            {
                SimpleJobOverview job = await GetSimpleJobOverview(item);
                jobs.Add(job);
            }

            result.Jobs = jobs;

            Int32 jobAmount = result.Jobs.Count();
            Int32 acknowledtedAmount = result.Jobs.Count(x => x.State == JobStates.Acknowledged || x.State == JobStates.Finished);
            result.DoingPercentage = jobAmount == 0 ? 0.0 : 100.0 * acknowledtedAmount / (Double)jobAmount;
            result.State = acknowledtedAmount == jobAmount ? JobStates.Finished : JobStates.Open;

            return result;
        }

        public async Task<Boolean> CheckIfBuildingConnenctionIsFinished(Int32 buildingId)
        {
            Int32 amount = await ConnectBuildingJobs.Include(x => x.FinishedModel).CountAsync(x => x.BuildingId == buildingId && x.FinishedModel != null && x.FinishedModel.AcceptedAt.HasValue == true);
            return amount > 0;
        }
        public async Task<Boolean> CheckIfContactInfosExists(IEnumerable<Int32> personInfoIds)
        {
            Int32 result = await ContactInfos.Select(x => x.Id).Intersect(personInfoIds).CountAsync();
            return result == personInfoIds.Count();
        }

        public async Task<Int32> CreateCustomerConnenctionRequest(CustomerConnectRequestCreateModel request)
        {
            CustomerConnenctionRequestDataModel dataModel = new CustomerConnenctionRequestDataModel(request);
            CustomerRequests.Add(dataModel);

            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<IEnumerable<PersonInfo>> SearchContacts(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.Trim().ToLower()}%";

            IEnumerable<PersonInfo> result = await (from contact in ContactInfos
                                                    where
                                                    EF.Functions.Like(contact.Surname, parsedQuery) ||
                                                    EF.Functions.Like(contact.Lastname, parsedQuery) ||
                                                    EF.Functions.Like(contact.CompanyName, parsedQuery)
                                                    orderby contact.Lastname
                                                    select GetPersonInfo(contact)).Take(amount).ToListAsync();

            return result;
        }

        public async Task<Int32> CreateContact(PersonInfo contact)
        {
            ContactInfoDataModel contactInfoDataModel = new ContactInfoDataModel(contact);
            ContactInfos.Add(contactInfoDataModel);

            await SaveChangesAsync();

            return contactInfoDataModel.Id;
        }

        private async Task AddFilesToFinishedJob(FinishedJobDataModel dataModel, FinishedJobModel model, Boolean save)
        {
            foreach (Int32 fileId in model.FileIds)
            {
                FileDataModel file = await Files.FirstAsync(x => x.Id == fileId);
                file.RelatedJob = dataModel;
                dataModel.Files.Add(file);
            }

            if (save == true)
            {
                await SaveChangesAsync();
            }
        }

        private async Task<Boolean> FinishJob(FinishedJobModel model, FinishedJobDataModel dataModel)
        {
            dataModel.JobberId = await GetUserIdByAuthServiceId(model.UserAuthId);

            await AddFilesToFinishedJob(dataModel, model, false);

            FinishedJobs.Add(dataModel);

            Int32 changeAmout = await SaveChangesAsync();
            return changeAmout > 0;
        }

        public async Task<Boolean> FinishBranchOffJob(FinishBranchOffJobModel model)
        {
            BranchOffJobFinishedDataModel dataModel = new BranchOffJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> FinishBuildingConnenctionJob(FinishBuildingConnenctionJobModel model)
        {
            ConnectBuildingFinishedDataModel dataModel = new ConnectBuildingFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> AcknowledgeJob(AcknowledgeJobModel model)
        {
            FinishedJobDataModel finishedJob = await FinishedJobs.FirstAsync(x => x.JobId == model.JobId);

            finishedJob.AcceptedAt = model.Timestamp;
            finishedJob.AccepterId = await GetUserIdByAuthServiceId(model.UserAuthId);

            Int32 changeAmout = await SaveChangesAsync();
            return changeAmout > 0;
        }

        private async Task<TOutput> GetFinishedJobDetails<TOutput, TDataModel>(Int32 jobId, Action<TDataModel, TOutput> transformer) where TOutput : FinishJobDetailModel, new()
            where TDataModel : FinishedJobDataModel
        {
            TDataModel dataModel = await FinishedJobs.Include(x => x.Accepter).ThenInclude(x => x.Company).Include(x => x.DoneBy).ThenInclude(x => x.Company).Include(x => x.Files).FirstAsync(x => x.JobId == jobId) as TDataModel;
            TOutput result = new TOutput
            {
                JobId = jobId,
                Comment = dataModel.Comment,
                FinishedAt = dataModel.FinishedAt,
                FinishedBy = GetUserOverviewModel(dataModel.DoneBy),
                ProblemHappend = new ExplainedBooleanModel { Value = dataModel.ProblemHappend, Description = dataModel.ProblemHappendDescription }
            };

            transformer?.Invoke(dataModel, result);

            if (dataModel.AccepterId.HasValue == true)
            {
                result.AcceptedBy = GetUserOverviewModel(dataModel.Accepter);
                result.AcceptedAt = dataModel.AcceptedAt;
            }

            result.Files = GetFileFileOverviewModel(dataModel.Files);

            return result;
        }

        public async Task<FinishBranchOffDetailModel> GetBranchOffFinishedJobDetails(Int32 jobId)
        {
            FinishBranchOffDetailModel result = await GetFinishedJobDetails<FinishBranchOffDetailModel, BranchOffJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
                output.DuctChanged = new ExplainedBooleanModel { Value = dataModel.DuctChanged, Description = dataModel.DuctChangedDescription };
            });

            return result;
        }

        public async Task<FinishBuildingConnenctionDetailModel> GetConnectionBuildingFinishedJobDetails(Int32 jobId)
        {
            FinishBuildingConnenctionDetailModel result = await GetFinishedJobDetails<FinishBuildingConnenctionDetailModel, ConnectBuildingFinishedDataModel>(jobId, (dataModel, output) =>
            {
                output.DuctChanged = new ExplainedBooleanModel { Value = dataModel.DuctChanged, Description = dataModel.DuctChangedDescription };
            });

            return result;
        }

        public async Task<IEnumerable<SimpleJobOverview>> GetJobsToAcknowledge()
        {
            ICollection<JobOverviewPreResult> preResults = await (from finishedJob in FinishedJobs.Include(x => x.Job)
                                                                  where finishedJob.AccepterId.HasValue == false
                                                                  select new JobOverviewPreResult
                                                                  {
                                                                      Id = finishedJob.JobId,
                                                                      Data = finishedJob.Job
                                                                  }).ToListAsync();


            List<SimpleJobOverview> results = new List<SimpleJobOverview>(preResults.Count);

            foreach (JobOverviewPreResult preResult in preResults)
            {
                SimpleJobOverview result = await GetSimpleJobOverview(preResult);
                results.Add(result);
            }

            return results;
        }

        public async Task<Boolean> DiscardJob(Int32 jobId)
        {
            FinishedJobDataModel dataModel = await FinishedJobs.Include(x => x.Files).FirstAsync(x => x.JobId == jobId);

            Files.RemoveRange(dataModel.Files);
            FinishedJobs.Remove(dataModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<RequestOverviewModel>> GetOpenRequests()
        {
            IEnumerable<RequestOverviewModel> result = await (from request in CustomerRequests.Include(x => x.Flat).ThenInclude(x => x.Building).Include(x => x.Customer).Include(x => x.PropertyOwner)
                                                              where request.IsClosed == false
                                                              select new RequestOverviewModel
                                                              {
                                                                  Id = request.Id,
                                                                  Building = request.OnlyHouseConnection == true ? GetSimpleBuildingOverviewModel(request.Building) : GetSimpleBuildingOverviewModel(request.Flat.Building),
                                                                  OnlyHouseConnection = request.OnlyHouseConnection,
                                                                  PrimaryPerson = request.OnlyHouseConnection == true ? GetPersonInfo(request.PropertyOwner) : GetPersonInfo(request.Customer)
                                                              }).ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfCustomerRequestExists(Int32 requestId)
        {
            Int32 amount = await CustomerRequests.CountAsync(x => x.Id == requestId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfCustomerRequestIsClosed(Int32 requestId)
        {
            Boolean result = await CustomerRequests.Where(x => x.Id == requestId).Select(x => x.IsClosed).FirstAsync();
            return result;
        }

        public async Task<Boolean> DeleteCustomerRequest(Int32 requestId)
        {
            CustomerConnenctionRequestDataModel requestDataModel = await CustomerRequests.FirstAsync(x => x.Id == requestId);
            requestDataModel.IsClosed = true;

            Int32 changeAmount = await SaveChangesAsync();
            return changeAmount > 0;
        }

        public async Task<SimpleJobOverview> GetMostRecentJobByFlatId(Int32 flatId, Int32? buildingId)
        {
            SimpleJobOverview jobOverview = null;
            ActivationJobDataModel activationJob = await ActivationJobs.Include(x => x.FinishedModel).Where(x => x.FlatId == flatId).FirstOrDefaultAsync();
            if (activationJob != null)
            {
                jobOverview = await GetSimpleJobOverview(activationJob);
            }
            else
            {
                SpliceBranchableJobDataModel spliceJob = await SpliceBranchableJobs.Include(x => x.FinishedModel).Where(x => x.FlatId == flatId).FirstOrDefaultAsync();
                if (spliceJob != null)
                {
                    jobOverview = await GetSimpleJobOverview(spliceJob);
                }
                else if (buildingId.HasValue == true)
                {
                    jobOverview = await GetMostRecentJobByBuildingId(buildingId.Value);
                }
            }

            return jobOverview;
        }

        public async Task<SimpleJobOverview> GetMostRecentJobByBuildingId(Int32 buildingId)
        {
            SimpleJobOverview jobOverview = null;

            InjectJobDataModel injectJob = await InjectJobs.Include(x => x.FinishedModel).Where(x => x.BuildingId == buildingId).FirstOrDefaultAsync();
            if (injectJob != null)
            {
                jobOverview = await GetSimpleJobOverview(injectJob);
            }
            else
            {
                ConnectBuildingJobDataModel connectBuildingJob = await ConnectBuildingJobs.Include(x => x.FinishedModel).Where(x => x.BuildingId == buildingId).FirstOrDefaultAsync();
                if (connectBuildingJob != null)
                {
                    jobOverview = await GetSimpleJobOverview(connectBuildingJob);
                }
                else
                {
                    BranchOffJobDataModel branchOff = await BranchOffJobs.Include(x => x.FinishedModel).Where(x => x.BuildingId == buildingId).FirstOrDefaultAsync();
                    if (branchOff != null)
                    {
                        jobOverview = await GetSimpleJobOverview(branchOff);
                    }
                }
            }

            return jobOverview;
        }

        public async Task<RequestDetailModel> GetCustomerRequestDetails(Int32 requestId)
        {
            RequestDetailModel result = await CustomerRequests
                .Include(x => x.Architect)
                .Include(x => x.Workman)
                .Include(x => x.PropertyOwner)
                .Include(x => x.Customer)
                .Include(x => x.Flat).ThenInclude(x => x.Building)
                .Include(x => x.Building)
                .Where(x => x.Id == requestId).Select(x => new RequestDetailModel
                {
                    ActivePointNearSubscriberEndpoint = new ExplainedBooleanModel { Value = x.ActivePointNearSubscriberEndpointValue, Description = x.ActivePointNearSubscriberEndpointDescription },
                    Architect = x.ArchitectPersonId.HasValue == true ? GetPersonInfo(x.Architect) : null,
                    Workman = x.WorkmanPersonId.HasValue == true ? GetPersonInfo(x.Workman) : null,
                    Owner = x.PropertyOwnerPersonId.HasValue == true ? GetPersonInfo(x.PropertyOwner) : null,
                    Customer = x.CustomerPersonId.HasValue == true ? GetPersonInfo(x.Customer) : null,

                    Building = x.OnlyHouseConnection == true ? GetSimpleBuildingOverviewModel(x.Building) : GetSimpleBuildingOverviewModel(x.Flat.Building),
                    CivilWorkIsDoneByCustomer = x.CivilWorkIsDoneByCustomer,
                    ConnectionAppointment = x.ConnectionAppointment,
                    ConnectionOfOtherMediaRequired = x.ConnectioOfOtherMediaRequired,
                    DescriptionForHouseConnection = x.DescriptionForHouseConnection,
                    DuctAmount = x.DuctAmount,
                    OnlyHouseConnection = x.OnlyHouseConnection,
                    PowerForActiveEquipment = new ExplainedBooleanModel { Value = x.PowerForActiveEquipmentValue, Description = x.PowerForActiveEquipmentDescription },
                    SubscriberEndpointLength = x.SubscriberEndpointLength,
                    SubscriberEndpointNearConnectionPoint = new ExplainedBooleanModel { Value = x.SubscriberEndpointNearConnectionPointValue, Description = x.SubscriberEndpointNearConnectionPointDescription },
                    Id = x.Id,
                    Flat = x.FlatId.HasValue == true ? GetSimpleFlatModelOverviewModel(x.Flat) : null,
                }).FirstAsync();

            if (result.OnlyHouseConnection == true)
            {
                result.IsInProgress = await BuildingConnectionProcedures.CountAsync(x => x.BuildingId == result.Building.Id && x.IsFinished == false && x.Timeline.Count(y => y.RelatedRequestId == result.Id) == 0) > 0;
            }
            else
            {
                result.IsInProgress = await CustomerConnectionProcedures.CountAsync(x => x.FlatId == result.Flat.Id && x.IsFinished == false && x.Timeline.Count(y => y.RelatedRequestId == result.Id) == 0) > 0;
            }

            return result;
        }

        public async Task<Int32> CreateHouseConnenctionJob(HouseConnenctionJobCreateModel jobCreateModel)
        {
            ConnectBuildingJobDataModel dataModel = new ConnectBuildingJobDataModel(jobCreateModel);
            ConnectBuildingJobs.Add(dataModel);
            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<Boolean> CloseRequest(Int32 requestId)
        {
            CustomerConnenctionRequestDataModel dataModel = await CustomerRequests.FirstAsync(x => x.Id == requestId);
            dataModel.IsClosed = true;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> GetBranchOffJobIdByBuildingId(Int32 id)
        {
            Int32 result = await BranchOffJobs.Where(x => x.BuildingId == id).Select(x => x.Id).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfBranchoffJobIsBoundByBuildingId(Int32 id)
        {
            Int32 result = await CollectionJobs.SumAsync(x => x.Jobs.OfType<BranchOffJobDataModel>().Count(y => y.BuildingId == id));
            return result > 0;
        }

        public async Task<Int32> GetCollectionJobId(Int32 jobId)
        {
            Int32 result = await Jobs.Where(x => x.CollectionId.HasValue == true && x.Id == jobId).Select(x => x.CollectionId.Value).FirstAsync();
            return result;
        }

        public async Task<Boolean> AddJobToCollectionJob(Int32 collectionJobId, Int32 jobId)
        {
            CollectionJobDataModel collectionJob = await CollectionJobs.FirstAsync(x => x.Id == collectionJobId);
            JobDataModel job = await Jobs.FirstAsync(x => x.Id == jobId);

            job.Collection = collectionJob;
            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Dictionary<Int32, String>> SearchBuildingsAndGetBranchOffJobIds(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.ToLower()}%";

            Dictionary<Int32, String> result = await BranchOffJobs.Include(x => x.Building)
                .Where(x => EF.Functions.Like(x.Building.StreetName, parsedQuery) == true)
                .OrderBy(x => x.Building.StreetName)
                .Take(amount)
                .ToDictionaryAsync(x => x.Id, x => x.Building.StreetName);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchBuildingsAndGetConnenctionJobIds(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.ToLower()}%";

            Dictionary<Int32, String> result = await ConnectBuildingJobs.Include(x => x.Building)
                .Where(x => EF.Functions.Like(x.Building.StreetName, parsedQuery) == true)
                .OrderBy(x => x.Building.StreetName)
                .Take(amount)
                .ToDictionaryAsync(x => x.Id, x => x.Building.StreetName);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchBuildingsAndGetInjectJobIds(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.ToLower()}%";

            Dictionary<Int32, String> result = await InjectJobs.Include(x => x.Building)
                .Where(x => EF.Functions.Like(x.Building.StreetName, parsedQuery) == true)
                .OrderBy(x => x.Building.StreetName)
                .Take(amount)
                .ToDictionaryAsync(x => x.Id, x => x.Building.StreetName);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchBranchablesAndGetPrepareJobId(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.Trim().ToLower()}%";

            Dictionary<Int32, String> result = await (from branchable in Branchables.Include(x => x.PrepareJob)
                                                      orderby branchable.Name
                                                      where branchable.PrepareJob != null && EF.Functions.Like(branchable.Name, parsedQuery)
                                                      select new
                                                      {
                                                          Id = branchable.PrepareJob.Id,
                                                          Name = branchable.Name,
                                                      }
                                                      ).Take(amount).ToDictionaryAsync(x => x.Id, x => x.Name);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchBuildingsAndGetSplicesJobsIds(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.Trim().ToLower()}%";

            Dictionary<Int32, String> result = await (from flat in Flats.Include(x => x.Building).Include(x => x.BranchableSpliceJob)
                                                      orderby flat.Building.StreetName
                                                      where EF.Functions.Like(flat.Building.StreetName, parsedQuery)
                                                      select new
                                                      {
                                                          Id = flat.BranchableSpliceJob.Id,
                                                          Name = flat.Building.StreetName + " - " + flat.Number,
                                                      }
                                                 ).Take(amount).ToDictionaryAsync(x => x.Id, x => x.Name);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchBuildingsAndGetActivationJobId(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.Trim().ToLower()}%";

            Dictionary<Int32, String> result = await (from flat in Flats
                                                      orderby flat.Building.StreetName
                                                      where EF.Functions.Like(flat.Building.StreetName, parsedQuery)
                                                      select new
                                                      {
                                                          Id = flat.ActivationJob.Id,
                                                          Name = flat.Building.StreetName + " - " + flat.Number,
                                                      }
                                                 ).Take(amount).ToDictionaryAsync(x => x.Id, x => x.Name);

            return result;
        }

        public async Task<Dictionary<Int32, String>> SearchMainCablesAndGetODFSpliceJobIds(String query, Int32 amount)
        {
            String parsedQuery = $"%{query.Trim().ToLower()}%";

            Dictionary<Int32, String> result = await (from job in SpliceODFJobs.Include(x => x.Cable)
                                                      orderby job.Cable.Name
                                                      where EF.Functions.Like(job.Cable.Name, parsedQuery)
                                                      select new
                                                      {
                                                          Id = job.Id,
                                                          Name = job.Cable.Name,
                                                      }
                                                 ).Take(amount).ToDictionaryAsync(x => x.Id, x => x.Name);

            return result;
        }

        public async Task<Boolean> CheckIfBuildingConnenctionJobExist(Int32 jobId)
        {
            Int32 result = await ConnectBuildingJobs.CountAsync(x => x.Id == jobId);
            return result > 0;
        }

        public async Task<HouseConnenctionJobDetailModel> GetBuildingConnenctionJobDetails(Int32 jobId)
        {
            HouseConnenctionJobDetailModel result = await (from job in ConnectBuildingJobs.Include(x => x.Architect).Include(x => x.Workman).Include(x => x.Owner).Include(x => x.Customer).Include(x => x.Building)
                                                           where job.Id == jobId
                                                           select new HouseConnenctionJobDetailModel
                                                           {
                                                               Architect = job.ArchitectContactId.HasValue == true ? GetPersonInfo(job.Architect) : null,
                                                               Workman = job.WorkmanContactId.HasValue == true ? GetPersonInfo(job.Workman) : null,
                                                               Owner = GetPersonInfo(job.Owner),
                                                               Customer = job.CustomerContactId.HasValue == true ? GetPersonInfo(job.Customer) : null,

                                                               Id = job.Id,
                                                               CivilWorkIsDoneByCustomer = job.CivilWorkIsDoneByCustomer,
                                                               ConnectionAppointment = job.ConnectionAppointment,
                                                               Description = job.Description,
                                                               ConnectionOfOtherMediaRequired = job.ConnectionOfOtherMediaRequired,
                                                               OnlyHouseConnection = job.OnlyHouseConnection,
                                                               BuildingInfo = GetBuildingInfoForBranchOffModel(job.Building),
                                                           }
                                                           ).FirstAsync();

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(result.BuildingInfo.Id, jobId, JobTypes.HouseConnenction);

            return result;
        }

        public async Task<Int32> CreateInjectJob(InjectJobCreateModel model)
        {
            InjectJobDataModel dataModel = new InjectJobDataModel(model);
            InjectJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<JobTypes> GetJobType(Int32 jobId)
        {
            if (_jobTypeDictionary.ContainsKey(jobId) == false)
            {
                JobDataModel job = await Jobs.FirstAsync(x => x.Id == jobId);
                JobTypes type = JobTypes.BranchOff;
                if (job is BranchOffJobDataModel)
                {
                    type = JobTypes.BranchOff;
                }
                else if (job is ConnectBuildingJobDataModel)
                {
                    type = JobTypes.HouseConnenction;
                }
                else if (job is InjectJobDataModel)
                {
                    type = JobTypes.Inject;
                }
                else if (job is SpliceBranchableJobDataModel)
                {
                    type = JobTypes.SpliceInBranchable;
                }
                else if (job is ActivationJobDataModel)
                {
                    type = JobTypes.ActivationJob;
                }
                else if (job is PrepareBranchableJobDataModel)
                {
                    type = JobTypes.PrepareBranchableForSplice;
                }
                else if (job is SpliceODFJobDataModel)
                {
                    type = JobTypes.SpliceInPoP;
                }
                else if (job is InventoryJobDataModel)
                {
                    type = JobTypes.Inventory;
                }

                _jobTypeDictionary.Add(jobId, type);
            }

            return _jobTypeDictionary[jobId];
        }

        public async Task<Boolean> CheckIfOnlyHouseConnenctionIsNeeded(Int32 connenctBuildingJobId)
        {
            Boolean result = await ConnectBuildingJobs.Where(x => x.Id == connenctBuildingJobId).Select(x => x.OnlyHouseConnection).FirstAsync();
            return result;
        }

        public async Task<Int32> GetBuildingIdByConnectionJobId(Int32 connenctBuildingJobId)
        {
            Int32 result = await ConnectBuildingJobs.Where(x => x.Id == connenctBuildingJobId).Select(x => x.BuildingId).FirstAsync();
            return result;
        }

        public async Task<Int32> GetCustomerContactIdByConnenctionJobId(Int32 connenctBuildingJobId)
        {
            Int32 result = await ConnectBuildingJobs.Where(x => x.Id == connenctBuildingJobId).Select(x => x.CustomerContactId.Value).FirstAsync();
            return result;
        }

        public async Task<Boolean> FinishInjectJob(InjectJobFinishedModel model)
        {
            InjectJobFinishedDataModel dataModel = new InjectJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> CheckIfInjectJobExist(Int32 jobId)
        {
            Int32 result = await InjectJobs.CountAsync(x => x.Id == jobId);
            return result > 0;
        }

        public async Task<InjectJobDetailModel> GetInjectJobDetails(Int32 jobId)
        {
            InjectJobDetailModel result = await InjectJobs
                .Include(x => x.Building).ThenInclude(x => x.Branchable)
                .Include(x => x.Building).ThenInclude(x => x.FiberCableType)
                .Include(x => x.CustomerContact)
                .Where(x => x.Id == jobId).Select(x => new InjectJobDetailModel
                {
                    Branchable = GetSimpleBranchableWithGPSModel(x.Building.Branchable),
                    Customer = GetPersonInfo(x.CustomerContact),
                    ExpectedLength = x.Building.ConnenctionLength,
                    BuildingInfo = GetSimpleBuildingOverviewWithGPSModel(x.Building),
                    InjectDuctColor = new ProjectAdapterColor { Name = x.Building.MicroDuctColorName, RGBHexValue = x.Building.MicroDuctHexValue },
                    Id = x.Id,
                    Cable = GetProjectAdapterCableModel(x.Building.FiberCableType)
                }).FirstAsync();

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(result.BuildingInfo.Id, jobId, JobTypes.Inject);
            return result;
        }

        public async Task<FinishInjectJobDetailModel> GetInjectFinishedJobDetails(Int32 jobId)
        {
            FinishInjectJobDetailModel result = await GetFinishedJobDetails<FinishInjectJobDetailModel, InjectJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
                output.Lenght = dataModel.ActualLength;
            });

            return result;
        }

        public async Task<Int32> FinishAndAcknowledgeJobAdministrative(FinishJobAdministrativeModel model)
        {
            FinishedJobDataModel dataModel = null;
            JobTypes type = await GetJobType(model.JobId);
            switch (type)
            {
                case JobTypes.BranchOff:
                    dataModel = new BranchOffJobFinishedDataModel();
                    break;
                case JobTypes.HouseConnenction:
                    dataModel = new ConnectBuildingFinishedDataModel();
                    break;
                case JobTypes.Inject:
                    dataModel = new InjectJobFinishedDataModel();
                    break;
                case JobTypes.SpliceInBranchable:
                    dataModel = new SpliceBranchableJobFinishedDataModel();
                    break;
                case JobTypes.ActivationJob:
                    dataModel = new ActivationJobFinishedDataModel();
                    break;
                case JobTypes.SpliceInPoP:
                    dataModel = new SpliceODFJobFinishedDataModel();
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    dataModel = new PrepareBranchableJobFinishedDataModel();
                    break;
                default:
                    break;
            }

            UserDataModel user = await Users.FirstAsync(x => x.AuthServiceId == model.UserAuthId);

            DateTimeOffset now = DateTimeOffset.Now;
            DateTimeOffset acceptedAt = now;
            DateTimeOffset finishedAt = now;
            if (model.FinishedAt.HasValue == true)
            {
                acceptedAt = finishedAt = model.FinishedAt.Value;
            }

            dataModel.JobId = model.JobId;
            dataModel.AcceptedAt = acceptedAt;
            dataModel.FinishedAt = finishedAt;
            dataModel.DoneBy = dataModel.Accepter = user;
            dataModel.WasAdministrativeFinished = true;

            FinishedJobs.Add(dataModel);
            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task DeleteJobFinished(IEnumerable<Int32> jobFinisehdIds)
        {
            IEnumerable<ICollection<Int32>> parts = SplitElements(jobFinisehdIds, 20);

            foreach (ICollection<Int32> ids in parts)
            {
                List<FinishedJobDataModel> dataModels = await FinishedJobs.Where(x => ids.Contains(x.Id)).ToListAsync();
                FinishedJobs.RemoveRange(dataModels);
                await SaveChangesAsync();
            }
        }

        public async Task DeleteJobs(IEnumerable<Int32> jobIds)
        {
            IEnumerable<ICollection<Int32>> parts = SplitElements(jobIds, 20);

            foreach (ICollection<Int32> ids in parts)
            {
                List<JobDataModel> dataModels = await Jobs.Where(x => ids.Contains(x.Id)).ToListAsync();
                Jobs.RemoveRange(dataModels);
                await SaveChangesAsync();
            }
        }

        public async Task<Int32> CreateInventoryJob(InventoryJobCreateModel model)
        {
            InventoryJobDataModel dataModel = new InventoryJobDataModel(model);
            InventoryJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Int32> GetBuildingIdByBranchOffJobId(Int32 branchOffJobId)
        {
            Int32 result = await BranchOffJobs.Where(x => x.Id == branchOffJobId).Select(x => x.BuildingId).FirstAsync();
            return result;
        }

        public async Task<Boolean> DeleteCollectionJob(Int32 collectionJobId)
        {
            CollectionJobDataModel collectionJobData = await CollectionJobs.Include(x => x.Jobs).FirstAsync(x => x.Id == collectionJobId);

            foreach (JobDataModel item in collectionJobData.Jobs)
            {
                item.CollectionId = null;
            }

            CollectionJobs.Remove(collectionJobData);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }


        #region Activation job

        public async Task<Int32> CreateActivationJob(ActivationJobCreateModel jobCreateModel)
        {
            ActivationJobDataModel dataModel = new ActivationJobDataModel(jobCreateModel);
            ActivationJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> FinishActivationJob(ActivationJobFinishModel model)
        {
            ActivationJobFinishedDataModel dataModel = new ActivationJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> CheckIfActivationJobExists(Int32 jobId)
        {
            Int32 amount = await ActivationJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<ActivationJobDetailModel> GetActivationJobDetails(Int32 jobId)
        {
            ActivationJobDetailModel result = await ActivationJobs
              .Where(x => x.Id == jobId).Select(x => new ActivationJobDetailModel
              {
                  Customer = GetPersonInfo(x.Customer),
                  Building = GetSimpleBuildingOverviewWithGPSModel(x.Flat.Building),
                  Flat = GetSimpleFlatModelOverviewModel(x.Flat),
                  Endpoints = x.Flat.Connections.Select(y => GetFiberEndpointModel(y)),
                  Pop = GetSimplePopOverviewWithGPSModel(x.Flat.Building.Branchable.PoP),
                  Id = x.Id,
              }).FirstAsync();

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(result.Building.Id, jobId, JobTypes.ActivationJob);
            return result;
        }

        public async Task<FinishActivationJobDetailModel> GetActivationFinishedJobDetails(Int32 jobId)
        {
            FinishActivationJobDetailModel result = await GetFinishedJobDetails<FinishActivationJobDetailModel, ActivationJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
                // output.Lenght = dataModel.ActualLength;
            });

            return result;
        }

        #endregion

        #region Splice related jobs

        public async Task<Boolean> CheckIfBuildingConnenctionIsFinishedByFlatId(Int32 flatId)
        {
            Int32 buildingId = await Flats.Where(x => x.Id == flatId).Select(x => x.BuildingId).FirstAsync();
            Boolean result = await CheckIfBuildingConnenctionIsFinished(buildingId);
            return result;
        }

        public async Task<Boolean> CheckIfSpliceIsFinishedByFlatId(Int32 flatId)
        {
            Int32 amount = await SpliceBranchableJobs.Include(x => x.FinishedModel).CountAsync(x => x.FlatId == flatId && x.FinishedModel != null);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfBranchableIsPreparedForSplice(Int32 branchableId)
        {
            Int32 amount = await PrepareBranchableForSpliceJobs.Include(x => x.FinishedModel).CountAsync(x => x.BranchableId == branchableId && x.FinishedModel != null);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfCableExists(Int32 cableId)
        {
            Int32 amount = await FiberCables.CountAsync(x => x.Id == cableId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfODFSpliceIsFinished(Int32 cableId)
        {
            Int32 amount = await SpliceODFJobs.Include(x => x.FinishedModel).CountAsync(x => x.CableId == cableId && x.FinishedModel != null);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfPrepareBranchableJobExists(Int32 jobId)
        {
            Int32 amount = await PrepareBranchableForSpliceJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfSpliceODFJobExists(Int32 jobId)
        {
            Int32 amount = await SpliceODFJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<Boolean> FinishSpliceBranchable(SpliceBranchableJobFinishModel model)
        {
            SpliceBranchableJobFinishedDataModel dataModel = new SpliceBranchableJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> FinishPrepareBranchableForSplice(PrepareBranchableForSpliceJobFinishModel model)
        {
            PrepareBranchableJobFinishedDataModel dataModel = new PrepareBranchableJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Boolean> FinishSpliceODF(SpliceODFJobFinishModel model)
        {
            SpliceODFJobFinishedDataModel dataModel = new SpliceODFJobFinishedDataModel(model);
            Boolean result = await FinishJob(model, dataModel);
            return result;
        }

        public async Task<Int32> CreateSpliceBranchableJob(SpliceBranchableJobCreateModel model)
        {
            SpliceBranchableJobDataModel dataModel = new SpliceBranchableJobDataModel(model);
            SpliceBranchableJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Int32> CreatePrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobCreateModel model)
        {
            PrepareBranchableJobDataModel dataModel = new PrepareBranchableJobDataModel(model);
            PrepareBranchableForSpliceJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }



        public async Task<Int32> CreateSpliceODFJob(SpliceODFJobCreateModel model)
        {
            SpliceODFJobDataModel dataModel = new SpliceODFJobDataModel(model);
            SpliceODFJobs.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> CheckIfSpliceBranchableJobExist(Int32 jobId)
        {
            Int32 amount = await SpliceBranchableJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<SpliceBranchableJobDetailModel> GetSpliceBranchableJobDetails(Int32 jobId)
        {
            SpliceBranchableJobDetailModel result = await SpliceBranchableJobs
               .Where(x => x.Id == jobId).Select(x => new SpliceBranchableJobDetailModel
               {
                   Id = x.Id,
                   Branchable = GetSimpleBranchableWithGPSModel(x.Flat.Building.Branchable),
                   Customer = GetPersonInfo(x.CustomerContact),
                   BranchableType = x.Flat.Building.Branchable is CabinetDataModel ? BranchableTypes.Cabinet : BranchableTypes.Sleeve,
                   Caption = "",
                   DuctColor = new ProjectAdapterColor { Name = x.Flat.Building.MicroDuctColorName, RGBHexValue = x.Flat.Building.MicroDuctHexValue },
                   TrayNumberForFiberBuffer = -1,
                   BuildingCable = GetSpliceCableInfoModel(x.Flat.Building.FiberCable),
                   MainCable = GetSpliceCableInfoModel(x.Flat.Building.Branchable.MainCable),
                   BuildingInfo = GetSimpleBuildingOverviewWithGPSModel(x.Flat.Building),
               }).FirstAsync();

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(result.BuildingInfo.Id, jobId, JobTypes.SpliceInBranchable);

            if (result.MainCable != null && result.MainCable.FiberAmount <= 0)
            {
                result.MainCable.FiberAmount = await CableTypes.Where(x => x.Id == (-result.MainCable.FiberAmount)).Select(x => x.FiberAmount).FirstAsync();
            }

            if (result.BuildingCable != null && result.BuildingCable.FiberAmount <= 0)
            {
                result.BuildingCable.FiberAmount = await CableTypes.Where(x => x.Id == (-result.BuildingCable.FiberAmount)).Select(x => x.FiberAmount).FirstAsync();
            }

            if (result.MainCable != null && result.BuildingCable != null)
            {
                result.Entries = await Splices.Include(x => x.FirstFiber).Include(x => x.SecondFiber).Where(x => (x.FirstFiberId.HasValue == true && x.SecondFiberId.HasValue == true) &&
                (
                (x.FirstFiber.CableId == result.MainCable.Id && x.SecondFiber.CableId == result.BuildingCable.Id) ||
                (x.FirstFiber.CableId == result.BuildingCable.Id && x.SecondFiber.CableId == result.MainCable.Id)
                )).OrderBy(x => x.TrayNumber).ThenBy(x => x.NumberInTray).Select(x => GetSpliceEntryModel(x)).ToListAsync();
            }

            return result;
        }

        private FiberBundleModel GetFiberBundleModel(FiberOverviewModel fiberModel)
        {
            FiberBundleModel bundleModel = new FiberBundleModel { Number = fiberModel.BundleNumber, Color = fiberModel.Color };
            Int32 splitIndex = bundleModel.Color.LastIndexOf('/');
            if (splitIndex >= 0)
            {
                bundleModel.Color = bundleModel.Color.Substring(splitIndex + 1);
            }

            return bundleModel;
        }

        public async Task<PrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceJobDetails(Int32 jobId)
        {
            PrepareBranchableForSpliceJobDetailModel result = await PrepareBranchableForSpliceJobs
                .Include(x => x.Branchable)
                .Where(x => x.Id == jobId).Select(x => new PrepareBranchableForSpliceJobDetailModel
                {
                    Id = x.Id,
                    Branchable = GetSimpleBranchableWithGPSModel(x.Branchable),
                    MainCable = GetSpliceCableInfoModel(x.Branchable.MainCable),
                }).FirstAsync();

            if (result.MainCable != null)
            {
                if (result.MainCable.FiberAmount <= 0)
                {
                    result.MainCable.FiberAmount = await CableTypes.Where(x => x.Id == (-result.MainCable.FiberAmount)).Select(x => x.FiberAmount).FirstAsync();
                }

                result.Splices = await Splices.Include(x => x.FirstFiber).Include(x => x.SecondFiber).Where(x =>
                   x.FirstFiber.Cable.StartingAtBranchableleId == result.Branchable.Id ||
                   x.SecondFiber.Cable.StartingAtBranchableleId == result.Branchable.Id
                  ).OrderBy(x => x.Type).ThenBy(x => x.TrayNumber).ThenBy(x => x.NumberInTray).ThenBy(x => x.FirstFiber.Name).ThenBy(x => x.FirstFiber.FiberNumber)
                  .Select(x => GetSpliceEntryModel(x)).ToListAsync();

                SortedDictionary<Int32, FiberBundleModel> loopUntouched = new SortedDictionary<int, FiberBundleModel>();
                SortedDictionary<Int32, FiberBundleModel> bundlesForCustomer = new SortedDictionary<int, FiberBundleModel>();

                List<SpliceEntryModel> splicesForLoop = new List<SpliceEntryModel>();
                foreach (var item in result.Splices)
                {
                    FiberOverviewModel mainCableFiber = item.FirstFiber;
                    if (item.FirstFiber == null)
                    {
                        mainCableFiber = item.SecondFiber;
                    }
                    else if (item.FirstFiber.CableId != result.MainCable.Id && item.SecondFiber != null)
                    {
                        mainCableFiber = item.SecondFiber;
                    }

                    if (mainCableFiber == null) { continue; }

                    if (item.Type == SpliceTypes.LoopUntouched)
                    {
                        if (loopUntouched.ContainsKey(mainCableFiber.BundleNumber) == false)
                        {
                            FiberBundleModel bundleModel = GetFiberBundleModel(mainCableFiber);
                            loopUntouched.Add(mainCableFiber.BundleNumber, bundleModel);
                        }
                    }
                    else if (item.Type == SpliceTypes.LoopTouched)
                    {
                        if (result.BundleForLoop == null)
                        {
                            FiberBundleModel bundleModel = GetFiberBundleModel(mainCableFiber);
                            result.BundleForLoop = bundleModel;
                        }

                        splicesForLoop.Add(item);
                    }
                    else if (item.Type == SpliceTypes.Normal)
                    {
                        if (bundlesForCustomer.ContainsKey(mainCableFiber.BundleNumber) == false)
                        {
                            FiberBundleModel bundleModel = GetFiberBundleModel(mainCableFiber);
                            bundlesForCustomer.Add(mainCableFiber.BundleNumber, bundleModel);
                        }
                    }
                }

                result.SplicesForLoop = splicesForLoop;
                result.UntouchedBundles = loopUntouched.Values;
                result.BundlesForCustomer = bundlesForCustomer.Values;
            }

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(null, jobId, JobTypes.PrepareBranchableForSplice);
            return result;
        }

        public async Task<SpliceODFJobDetailModel> GetSpliceODFJobDetails(Int32 jobId)
        {
            SpliceODFJobDetailModel result = await SpliceODFJobs
                .Where(x => x.Id == jobId).Select(x => new SpliceODFJobDetailModel
                {
                    Id = x.Id,
                    Pop = GetSimplePopOverviewWithGPSModel(x.Cable.StartingAtPop),
                    MainCable = GetSpliceCableInfoModel(x.Cable),

                }).FirstAsync();

            if (result.MainCable != null && result.MainCable.FiberAmount <= 0)
            {
                result.MainCable.FiberAmount = await CableTypes.Where(x => x.Id == (-result.MainCable.FiberAmount)).Select(x => x.FiberAmount).FirstAsync();
            }

            result.State = await GetJobState(jobId);
            result.Files = await GetFilesAllowedForJob(null, jobId, JobTypes.SpliceInPoP);
            return result;
        }

        public async Task<FinishSpliceBranchableJobDetailModel> GetSpliceBranchableFinishedJobDetails(Int32 jobId)
        {
            FinishSpliceBranchableJobDetailModel result = await GetFinishedJobDetails<FinishSpliceBranchableJobDetailModel, SpliceBranchableJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
                output.CableMetric = dataModel.CableMetric;
            });

            return result;
        }

        public async Task<FinishPrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceFinishedJobDetails(Int32 jobId)
        {
            FinishPrepareBranchableForSpliceJobDetailModel result = await GetFinishedJobDetails<FinishPrepareBranchableForSpliceJobDetailModel, PrepareBranchableJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
            });

            return result;
        }

        public async Task<FinishSpliceODFJobDetailModel> GetSpliceODFFinishedJobDetails(Int32 jobId)
        {
            FinishSpliceODFJobDetailModel result = await GetFinishedJobDetails<FinishSpliceODFJobDetailModel, SpliceODFJobFinishedDataModel>(jobId, (dataModel, output) =>
            {
                output.CableMetric = dataModel.CableMetric;
            });

            return result;
        }

        public async Task<IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel>> GetOpenPrepareBranchableForSpliceJobsOverview()
        {
            IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel> result = await PrepareBranchableForSpliceJobs.Where(x => x.CollectionId.HasValue == false && x.FinishedModel == null).OrderBy(x => x.Branchable.Name)
                .Select(x => new SimplePrepareBranchableForSpliceJobOverviewModel
                {
                    JobId = x.Id,
                    Branchable = GetSimpleBranchableWithGPSModel(x.Branchable),
                }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<SimpleSpliceODFJobOverviewModel>> GetOpenSpliceODFJobsOverview()
        {
            IEnumerable<SimpleSpliceODFJobOverviewModel> result = await SpliceODFJobs.Where(x => x.CollectionId.HasValue == false && x.FinishedModel == null).OrderBy(x => x.Cable.Name)
                .Select(x => new SimpleSpliceODFJobOverviewModel
                {
                    JobId = x.Id,
                    CableName = x.Cable.Name,
                }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<SimpleActivationJobOverviewModel>> GetOpenActivationJobsOverview()
        {
            IEnumerable<SimpleActivationJobOverviewModel> result = await ActivationJobs.Where(x => x.CollectionId.HasValue == false && x.FinishedModel == null).OrderBy(x => x.Customer.Lastname)
                .Select(x => new SimpleActivationJobOverviewModel
                {
                    JobId = x.Id,
                    Flat = GetSimpleFlatModelOverviewModel(x.Flat),
                    Building = GetSimpleBuildingOverviewModel(x.Flat.Building),
                    Customer = GetPersonInfo(x.Customer),
                }).ToListAsync();

            return result;
        }

        public async Task<Int32?> GetConnectBuildingJobIdByBuildingId(Int32 buildingId)
        {
            Int32 id = await ConnectBuildingJobs.Where(x => x.BuildingId == buildingId).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<Int32?> GetConnectBuildingJobIdByFlatId(Int32 flatId)
        {
            Int32 buildingId = await GetBuildingIdByFlatId(flatId);
            Int32? result = await GetConnectBuildingJobIdByBuildingId(buildingId);
            return result;
        }

        public async Task<Int32?> GetRequestIdByFilterValues(RequestFilterModel filterModel)
        {
            Int32 id = await CustomerRequests
                .Where(x => x.BuildingId == filterModel.BuildingId && filterModel.OnlyConnection == x.OnlyHouseConnection && filterModel.OwnerContactId == x.PropertyOwnerPersonId)
                .OrderByDescending(x => x.Id)
                .Select(x => x.Id).FirstOrDefaultAsync();

            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<IEnumerable<SpliceODFJobOverviewModel>> GetOpenSpliceODFJobs()
        {
            IEnumerable<SpliceODFJobOverviewModel> result = await (from job in SpliceODFJobs
                                                                   where job.FinishedModel == null
                                                                   select new SpliceODFJobOverviewModel
                                                                   {
                                                                       BoundedCompanyName = job.CollectionId.HasValue == true ? job.Collection.Company.Name : null,
                                                                       JobId = job.Id,
                                                                       State = JobStates.Open,
                                                                       MainCable = GetCableModel(job.Cable),
                                                                       Location = new GPSCoordinate { },
                                                                       PopName = "",
                                                                   }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<ActivationJobOverviewModel>> GetOpenActivationJobsAsOverview()
        {
            IEnumerable<ActivationJobOverviewModel> result = await (from job in ActivationJobs
                                                                    where job.FinishedModel == null
                                                                    select new ActivationJobOverviewModel
                                                                    {
                                                                        BoundedCompanyName = job.CollectionId.HasValue == true ? job.Collection.Company.Name : null,
                                                                        JobId = job.Id,
                                                                        State = JobStates.Open,
                                                                        Building = GetSimpleBuildingOverviewModel(job.Flat.Building),
                                                                        Customer = GetPersonInfo(job.Customer),
                                                                        PoPName = "",
                                                                    }).ToListAsync();

            return result;
        }

        #endregion

        public async Task<Boolean> CheckIfActivationJobExitsByFlatId(Int32 flatId)
        {
            Int32 amount = await Flats.CountAsync(x => x.Id == flatId && x.ActivationJob != null);
            return amount > 0;
        }

        public async Task<Int32?> GetInjectJobIdByBuildingId(Int32 buildingId)
        {
            Int32 id = await InjectJobs.Where(x => x.BuildingId == buildingId).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<Int32?> GetInjectJobIdByFlatId(Int32 flatId)
        {
            Int32 buildingId = await GetBuildingIdByFlatId(flatId);
            Int32? result = await GetInjectJobIdByBuildingId(buildingId);
            return result;
        }

        public async Task<Int32?> GetSpliceBranachableJobIdByFlatId(Int32 flatId)
        {
            Int32 id = await SpliceBranchableJobs.Where(x => x.FlatId == flatId).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<Int32?> GetActivationJobIdByFlatId(Int32 flatId)
        {
            Int32 id = await ActivationJobs.Where(x => x.FlatId == flatId).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<IEnumerable<SimpleJobOverview>> SearchBranchOffJobByBuildingNameQuery(String query, JobStates? expectedState, Int32 amount)
        {
            String pattern = $"%{query.Trim().ToLower()}%";
            IQueryable<BranchOffJobDataModel> preQuery = BranchOffJobs.Include(x => x.FinishedModel).Include(x => x.Building).Where(x => EF.Functions.Like(x.Building.StreetName, pattern) == true);
            if (expectedState.HasValue == true)
            {
                switch (expectedState.Value)
                {
                    case JobStates.Open:
                        preQuery = preQuery.Where(x => x.FinishedModel == null);
                        break;
                    case JobStates.Finished:
                        preQuery = preQuery.Where(x => x.FinishedModel != null && x.FinishedModel.AccepterId.HasValue == false);
                        break;
                    case JobStates.Acknowledged:
                        preQuery = preQuery.Where(x => x.FinishedModel != null && x.FinishedModel.AccepterId.HasValue == true);
                        break;
                    default:
                        break;
                }
            }

            IEnumerable<SimpleJobOverview> result = await (from job in preQuery
                                                           let state = job.FinishedModel == null ? JobStates.Open : (job.FinishedModel.AccepterId.HasValue == false ? JobStates.Finished : JobStates.Acknowledged)
                                                           orderby job.Building.StreetName
                                                           select new SimpleJobOverview
                                                           {
                                                               Id = job.Id,
                                                               JobType = JobTypes.BranchOff,
                                                               Name = job.Building.StreetName,
                                                               State = state,
                                                           }).Take(amount).ToListAsync();

            return result;
        }

        #region Job-Collections

        public async Task<IEnumerable<JobCollectionOverviewModel>> GetJobCollectionOverview(JobCollectionFilterModel filterModel)
        {
            IQueryable<CollectionJobDataModel> query = CollectionJobs;
            if (String.IsNullOrEmpty(filterModel.Query) == false)
            {
                String pattern = $"%{filterModel.Query.ToLower().Trim()}%";

                query = query.Where(x => EF.Functions.Like(x.Name, pattern) == true ||
                EF.Functions.Like(x.Company.Name, pattern) == true);
            }

            var parsedQuery = from element in query
                              let jobAmount = element.Jobs.Count
                              let ackJobs = element.Jobs.Count(x => x.FinishedModel != null && x.FinishedModel.AccepterId.HasValue == true)
                              let percentage = jobAmount == 0 ? 0.0 : 1.0 * (Double)ackJobs / jobAmount
                              select new
                              {
                                  JobAmount = (Double)jobAmount,
                                  Percentage = percentage,
                                  Model = element,
                                  AckJobs = (Double)ackJobs,
                              };

            switch (filterModel.SortProperty)
            {
                case JobCollectionSortProperties.FinishedTill:
                    if (filterModel.SortDirection == SortDirections.Ascending)
                    {
                        parsedQuery = parsedQuery.OrderBy(x => x.Model.FinishedTill);
                    }
                    else
                    {
                        parsedQuery = parsedQuery.OrderByDescending(x => x.Model.FinishedTill);
                    }
                    break;
                case JobCollectionSortProperties.Name:
                    if (filterModel.SortDirection == SortDirections.Ascending)
                    {
                        parsedQuery = parsedQuery.OrderBy(x => x.Model.Name);
                    }
                    else
                    {
                        parsedQuery = parsedQuery.OrderByDescending(x => x.Model.Name);
                    }
                    break;
                case JobCollectionSortProperties.CompanyName:
                    if (filterModel.SortDirection == SortDirections.Ascending)
                    {
                        parsedQuery = parsedQuery.OrderBy(x => x.Model.Company.Name);
                    }
                    else
                    {
                        parsedQuery = parsedQuery.OrderByDescending(x => x.Model.Company.Name);
                    }
                    break;
                case JobCollectionSortProperties.DoingPercentage:
                    if (filterModel.SortDirection == SortDirections.Ascending)
                    {
                        parsedQuery = parsedQuery.OrderBy(x => x.Percentage);
                    }
                    else
                    {
                        parsedQuery = parsedQuery.OrderByDescending(x => x.Percentage);
                    }
                    break;
                default:
                    break;
            }

            IEnumerable<JobCollectionOverviewModel> result = await (from item in parsedQuery
                                                                    select new JobCollectionOverviewModel
                                                                    {
                                                                        CollectionJobId = item.Model.Id,
                                                                        BoundedTo = GetSimpleCompanyOverview(item.Model.Company),
                                                                        FinishedTill = item.Model.FinishedTill,
                                                                        Percentage = item.JobAmount == 0 ? 0.0 : 1.0 * (Double)item.AckJobs / (Double)item.JobAmount,
                                                                        Name = item.Model.Name,
                                                                        Jobs = item.Model.Jobs.OrderBy(x => x.Id).Select(x => new SimpleJobOverview { Id = x.Id }),
                                                                    }
                                                                    ).Skip(filterModel.Start).Take(filterModel.Amount).ToListAsync();
            foreach (JobCollectionOverviewModel item in result)
            {
                List<SimpleJobOverview> jobs = new List<SimpleJobOverview>();
                foreach (SimpleJobOverview job in item.Jobs)
                {
                    try
                    {
                        SimpleJobOverview realJob = await GetSimpleJobOverviewById(job.Id);
                        jobs.Add(realJob);
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                }

                item.Jobs = jobs;
            }

            return result;
        }
        
        public async Task<IEnumerable<Int32>> GetAllJobIdsFromCollectionJobId(Int32 collectionJobId)
        {
            IEnumerable<Int32> ids = await Jobs.Where(x => x.CollectionId == collectionJobId).Select(x => x.Id).ToListAsync();
            return ids;
        }

        public async Task<IEnumerable<Int32>> GetCompanyUserIdsByCollectionJobId(Int32 collectionJobId)
        {
            throw new NotImplementedException();
        }

        public async Task<Int32> GetJobFinisherUserIdByJobId(Int32 jobId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Inventory Jobs

        public async Task<IEnumerable<InventoryJobOverviewModel>> GetOpenIventoryJobs()
        {
            IEnumerable<InventoryJobOverviewModel> result = await (from job in InventoryJobs
                                                                   where job.FinishedModel == null
                                                                   orderby job.PickupAt descending
                                                                   select new InventoryJobOverviewModel
                                                                   {
                                                                       Id = job.Id,
                                                                       Issuer = GetPersonInfo(job.IssuerContact),
                                                                       Positions = job.Items.Select(x =>
                                                                       new InventoryJobPositionModel
                                                                       {
                                                                           Amount = x.Amount,
                                                                           ArticleId = x.ArticleId,
                                                                           ArticleName = x.Article.Name,
                                                                       }
                                                                       )
                                                                   }
                                                                   ).ToListAsync();
            return result;

        }

        public async Task<Boolean> CheckIfInventoryJobExists(Int32 jobId)
        {
            Int32 amount = await InventoryJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<InventoryJobDetailModel> GetInventoryobDetails(Int32 jobId)
        {
            InventoryJobDetailModel result = await InventoryJobs.Where(x => x.Id == jobId).Select(x =>
            new InventoryJobDetailModel
            {
                Id = x.Id,
                Issuer = GetPersonInfo(x.IssuerContact),
                Positions = x.Items.Select(y =>
                new InventoryJobPositionModel
                {
                    Amount = y.Amount,
                    ArticleId = y.ArticleId,
                    ArticleName = y.Article.Name,
                })
            }).FirstAsync();

            return result;

        }

        public async Task<Int32> FinishInventoryJob(InventoryFinishedJobModel model)
        {
            InventoryJobFinishedDataModel finishedDataModel = new InventoryJobFinishedDataModel(model);
            FinishedInventoryJobs.Add(finishedDataModel);

            await SaveChangesAsync();
            return finishedDataModel.Id;
        }

        #endregion

        #region Sales Jobs

        public async Task<Int32> CreateContactAfterFinishedJob(CreateContactAfterFinishedJobModel model)
        {
            if(await ContactAfterFinishedJobs.CountAsync(x => x.BuildingId == model.BuildingId && x.RelatedProcedureId == model.RelatedProcedureId && x.FlatId == model.FlatId) > 0)
            {
                return -1;
            }

            ContactAfterFinishedJobDataModel dataModel = new ContactAfterFinishedJobDataModel(model);
            Int32 contactId = await BuildingConnectionProcedures.Where(x => x.Id == model.RelatedProcedureId).Select(x => x.OwnerContactId).FirstAsync();
            dataModel.CustomerContactId = contactId;

            ContactAfterFinishedJobs.Add(dataModel);

            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<Boolean> CheckIfCustomerContactJobExists(Int32 jobId)
        {
            Int32 amount = await ContactAfterFinishedJobs.CountAsync(x => x.Id == jobId);
            return amount > 0;
        }

        public async Task<ContactAfterFinishedJobModel> GetContactAfterFinishedJobModelById(Int32 jobId)
        {
            ContactAfterFinishedJobModel result = await ContactAfterFinishedJobs.Where(x => x.Id == jobId).Select(x => new ContactAfterFinishedJobModel
            {
                Id = jobId,
                BuildingId = x.BuildingId,
                FlatId = x.FlatId,
                ContactId = x.CustomerContactId,
            }).FirstAsync();

            return result;
        }

        public async Task<Int32> FinishContactAfterFinishedJob(ContactAfterFinishedFinishModel finishedModel)
        {
            ContactAfterFinishedJobFinishedDataModel finishedJobDataModel = new ContactAfterFinishedJobFinishedDataModel(finishedModel);
            Boolean result = await FinishJob(finishedModel, finishedJobDataModel);

            return finishedJobDataModel.Id;
        }

        public async Task<IEnumerable<ContactAfterSalesJobOverview>> GetContactAfterSalesJobs(ContactAfterFinishedFilterModel filter)
        {
            var preResult = ContactAfterFinishedJobs.Select(x => new
            {
                Job = x,
                Name = (x.Building.NormalizedStreetName + x.CustomerContact.Lastname + x.CustomerContact.Surname).ToLower(),
                IsOpen = x.FinishedModel == null,
            });

            if(filter.OpenState.HasValue == true)
            {
                preResult = preResult.Where(x => x.IsOpen == filter.OpenState.Value);
            }
            if(String.IsNullOrEmpty(filter.Query) == false)
            {
                String searchPattern = $"%{filter.Query.ToLower()}%";
                preResult = preResult.Where(x => EF.Functions.Like(x.Name, searchPattern) == true);
            }

            IEnumerable<ContactAfterSalesJobOverview> result = await preResult.Select(x => new ContactAfterSalesJobOverview
            {
                Id = x.Job.Id,
                Building = GetSimpleBuildingOverviewModel(x.Job.Building),
                Contact = GetPersonInfo(x.Job.CustomerContact),
                IsFinished = !x.IsOpen
            }).OrderBy(x => x.Contact.Lastname).Skip(filter.Start).Take(filter.Amount).ToListAsync();

            return result;
        }

        public async Task<ContactAfterSalesJobDetails> GetContactAfterSalesJobDetail(Int32 jobId)
        {
            ContactAfterSalesJobDetails result = await ContactAfterFinishedJobs.Where(x => x.Id == jobId).Select(x => new ContactAfterSalesJobDetails
            {
                Building = GetSimpleBuildingOverviewWithGPSModel(x.Building),
                Flat = GetSimpleFlatModelOverviewModel(x.Flat),
                RelatedProcedureId = x.RelatedProcedureId,
                Closed = x.FinishedModel != null,
                FinishedAt = x.FinishedModel != null ? x.FinishedModel.FinishedAt : new DateTimeOffset?(),
                CloseComment = x.FinishedModel.ProblemHappendDescription,
                Contact = GetPersonInfo(x.CustomerContact),
                Id = x.Id,
            }).FirstAsync();

            return result;
        }

        #endregion

        #region Get-Overview-models

        private PersonInfo GetPersonInfo(ContactInfoDataModel contactInfoData)
        {
            return new PersonInfo
            {
                Id = contactInfoData.Id,
                Address = new AddressModel
                {
                    City = contactInfoData.City,
                    PostalCode = contactInfoData.PostalCode,
                    Street = contactInfoData.Street,
                    StreetNumber = contactInfoData.StreetNumber,
                },
                CompanyName = contactInfoData.CompanyName,
                EmailAddress = contactInfoData.EmailAddress,
                Lastname = contactInfoData.Lastname,
                Surname = contactInfoData.Surname,
                Phone = contactInfoData.Phone,
                Type = contactInfoData.Type,
            };
        }

        private BuildingInfoForBranchOffModel GetBuildingInfoForBranchOffModel(BuildingDataModel model)
        {
            return new BuildingInfoForBranchOffModel
            {
                MainColor = new ProjectAdapterColor
                {
                    Name = model.MicroDuctColorName,
                    RGBHexValue = model.MicroDuctHexValue,
                },
                BranchColor = new ProjectAdapterColor
                {
                    Name = model.HouseConnenctionColorName,
                    RGBHexValue = model.HouseConnenctionHexValue,
                },
                Coordinate = new GPSCoordinate
                {
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                },
                Id = model.Id,
                StreetName = model.StreetName,
            };
        }

        private SpliceCableInfoModel GetCableModel(FiberCableDataModel fiberCable)
        {
            return fiberCable != null ? new SpliceCableInfoModel
            {
                Name = fiberCable.Name,
                FiberAmount = fiberCable.CableType != null ? fiberCable.CableType.FiberAmount : 0
            } : new SpliceCableInfoModel();
        }

        #endregion
    }
}
