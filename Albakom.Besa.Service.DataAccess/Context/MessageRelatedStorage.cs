﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Int32> CreateMessage(MessageRawCreateModel createModel)
        {
            MessageDataModel dataModel = new MessageDataModel(createModel);
            Messages.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<MessageRawModel> GetMessageModelById(Int32 messageId)
        {
            MessageRawModel messageModel = await Messages.Where(x => x.Id == messageId).Select(x => GetMessageModel(x)).FirstAsync();
            return messageModel;
        }

        private MessageRawModel GetMessageModel(MessageDataModel dataModel)
        {
            return new MessageRawModel
            {
                Action = dataModel.Action,
                RelatedObjectType = dataModel.RelatedObjectType,
                Id = dataModel.Id,
                MarkedAsRead = dataModel.MarkedAsRead,
                TimeStamp = dataModel.Timestamp,
                SourceUser = GetUserOverviewModel(dataModel.Sender),
            };
        }

        public async Task<String> GetMessageObjectName(Int32 objectId, MessageRelatedObjectTypes relatedType)
        {
            String result = string.Empty;
            switch (relatedType)
            {
                case MessageRelatedObjectTypes.User:
                    result = await Users.Where(x => x.ID == objectId).Select(x => x.Surname + " " + x.Lastname).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Article:
                    result = await Articles.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Branchable:
                    result = await Branchables.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Company:
                    result = await Companies.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.ConstructionStage:
                    result = await ConstructionStages.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.ContactInfo:
                    result = await ContactInfos.Where(x => x.Id == objectId).Select(x => x.Surname + " " + x.Lastname).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.FiberCable:
                    result = await FiberCables.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.FiberCableType:
                    result = await CableTypes.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.FiberConnection:
                    result = await FiberConnenctions.Where(x => x.Id == objectId).Select(x => x.Caption).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Fiber:
                    result = await Fibers.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Splice:
                    result = await Splices.Where(x => x.Id == objectId).Select(x => x.ProjectAdapterId).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.File:
                    result = await Files.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Flat:
                    result = await Flats.Where(x => x.Id == objectId).Select(x => x.Number).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.PoP:
                    result = await Pops.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.BuildingConnenctionProcedure:
                    result = await BuildingConnectionProcedures.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.CustomerConnenctionProcedure:
                    result = await CustomerConnectionProcedures.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.CollectionJob:
                    result = await CollectionJobs.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.JobGenerell:
                    // result = await Jobs.Where(x => x.Id == objectId).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.ActivationJob:
                    result = await ActivationJobs.Include(x => x.Flat).Where(x => x.Id == objectId).Select(x => x.Flat.Number).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.BranchOffJob:
                    result = await BranchOffJobs.Include(x => x.Building).Where(x => x.Id == objectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.ConnectBuildingJob:
                    result = await ConnectBuildingJobs.Include(x => x.Building).Where(x => x.Id == objectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.InjectJob:
                    result = await InjectJobs.Include(x => x.Building).Where(x => x.Id == objectId).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.InventoryJob:
                    // result = await InventoryJobs.Where(x => x.Id == objectId).Select(x => x.).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.PrepareBranchableJob:
                    result = await PrepareBranchableForSpliceJobs.Include(x => x.Branchable).Where(x => x.Id == objectId).Select(x => x.Branchable.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.SpliceInBranchableJob:
                    result = await SpliceBranchableJobs.Include(x => x.Flat).ThenInclude(x => x.Building).Where(x => x.Id == objectId).Select(x => x.Flat.Building.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.SpliceMainCableInPoPJob:
                    result = await SpliceODFJobs.Include(x => x.Cable).Where(x => x.Id == objectId).Select(x => x.Cable.Name).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.Buildings:
                    result = await Buildings.Where(x => x.Id == objectId).Select(x => x.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.CustomerRequest:
                    result = await CustomerRequests.Include(x => x.Building).Where(x => x.Id == objectId && x.BuildingId.HasValue == true).Select(x => x.Building.StreetName).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.UserRequests:
                    result = await UserAccessRequests.Where(x => x.Id == objectId).Select(x => x.Surname + " " + x.Lastname).FirstOrDefaultAsync();
                    break;
                case MessageRelatedObjectTypes.ContactAfterFinishedJob:
                    result = await ContactAfterFinishedJobs.Include(x => x.CustomerContact).Where(x => x.Id == objectId).Select(x => x.CustomerContact.Surname + " " + x.CustomerContact.Lastname).FirstOrDefaultAsync();
                    break;
                default:
                    break;
            }

            return result;
        }

        public async Task<MessageActionJobBoundDetailsModel> GetMessageActionJobBoundDetails(Int32 jobId)
        {
            MessageActionJobBoundDetailsModel result = await Jobs.Where(x => x.Id == jobId && x.CollectionId.HasValue == true).Select(x => new MessageActionJobBoundDetailsModel
            {
                CollectionJob = new JobCollectionOverviewModel
                {
                    CollectionJobId = x.Collection.Id,
                    BoundedTo = GetSimpleCompanyOverview(x.Collection.Company),
                    FinishedTill = x.Collection.FinishedTill,
                    Name = x.Collection.Name,
                    Jobs = x.Collection.Jobs.OrderBy(y => x.Id).Select(y => new SimpleJobOverview { Id = y.Id }),
                },
                Id = jobId,
            }).FirstOrDefaultAsync();

            if (result != null)
            {
                await SetJobOverviewToJobBasedDetailsModel(jobId, result);

                List<SimpleJobOverview> collectionJobs = new List<SimpleJobOverview>();
                foreach (Int32 item in result.CollectionJob.Jobs.Select(x => x.Id))
                {
                    collectionJobs.Add(await GetSimpleJobOverviewById(item));
                }

                result.CollectionJob.Jobs = collectionJobs;
            }

            return result;
        }

        public async Task<MessageActionEmployesBoundDetailsModel> GetMessageActionEmployesBoundDetails(Int32 protocolEntryId)
        {
            ProtocolEntryDataModel protocolEntryDataModel = await ProtocolEntries.FirstAsync(x => x.Id == protocolEntryId);

            MessageActionEmployesBoundDetailsModel result = await Companies.Where(x => x.Id == protocolEntryDataModel.RelatedObjectId).Select(x => new MessageActionEmployesBoundDetailsModel
            {
                Company = GetCompanyOverviewByDataModel(x),
                Id = x.Id,
                Name = x.Name,
            }).FirstOrDefaultAsync();

            if (result != null)
            {
                result.Removed = protocolEntryDataModel.Action == MessageActions.AddEmployeeToCompany ? false : true;
            }

            return result;
        }

        private async Task SetJobOverviewToJobBasedDetailsModel(Int32 jobId, MessageActionJobBasedDetailsModel result)
        {
            SimpleJobOverview jobOverview = await GetSimpleJobOverviewById(jobId);
            result.Name = jobOverview.Name;
            result.JobOverview = jobOverview;
        }

        public async Task<MessageActionJobFinishedDetailsModel> GetMessageActionJobFinishedDetails(Int32 jobId)
        {
            MessageActionJobFinishedDetailsModel result = await FinishedJobs.Include(x => x.DoneBy).Where(x => x.JobId == jobId).Select(x => new MessageActionJobFinishedDetailsModel
            {
                FinishedAt = x.FinishedAt,
                FinishedBy = GetUserOverviewModel(x.DoneBy),
                Id = jobId,
                ProblemHappend = new ExplainedBooleanModel { Value = x.ProblemHappend, Description = x.ProblemHappendDescription },

            }).FirstOrDefaultAsync();

            if (result != null)
            {
                await SetJobOverviewToJobBasedDetailsModel(jobId, result);
            }

            return result;
        }

        public async Task<MessageActionJobDiscardDetailsModel> GetMessageActionJobDiscardedDetails(Int32 jobId)
        {
            MessageActionJobDiscardDetailsModel result = await FinishedJobs.Where(x => x.JobId == jobId).Select(x => new MessageActionJobDiscardDetailsModel
            {
                //FinishedAt = x.FinishedAt,
                //FinishedBy = GetUserOverviewModel(x.DoneBy),
                Id = jobId,
                //ProblemHappend = new ExplainedBooleanModel { Value = x.ProblemHappend, Description = x.ProblemHappendDescription },

            }).FirstOrDefaultAsync();

            if (result != null)
            {
                await SetJobOverviewToJobBasedDetailsModel(jobId, result);
            }

            return result;
        }

        public async Task<MessageActionJobAcknowledgedDetailsModel> GetMessageActionJobAcknowledgedDetails(Int32 jobId)
        {
            MessageActionJobAcknowledgedDetailsModel result = await FinishedJobs.Include(x => x.Accepter).Where(x => x.JobId == jobId && x.AcceptedAt.HasValue == true).Select(x => new MessageActionJobAcknowledgedDetailsModel
            {
                AcceptedAt = x.AcceptedAt.Value,
                AcepptedBy = GetUserOverviewModel(x.Accepter),
                Id = jobId,
            }).FirstOrDefaultAsync();

            if (result != null)
            {
                await SetJobOverviewToJobBasedDetailsModel(jobId, result);
            }

            return result;
        }

        public async Task<MessageActionJobFinishJobAdministrativeDetailModel> GetMessageActionJobFinishJobAdministrativeDetail(Int32 jobId)
        {
            MessageActionJobFinishJobAdministrativeDetailModel result = await FinishedJobs.Where(x => x.JobId == jobId).Select(x => new MessageActionJobFinishJobAdministrativeDetailModel
            {
                FinishedAt = x.FinishedAt,
                FinishedBy = GetUserOverviewModel(x.DoneBy),
                Id = jobId,
            }).FirstOrDefaultAsync();

            if (result != null)
            {
                await SetJobOverviewToJobBasedDetailsModel(jobId, result);
            }

            return result;
        }

        public async Task<MessageActionRemoveMemberDetailsModel> GetMessageActionRemoveMemberDetails(Int32 userId)
        {
            MessageActionRemoveMemberDetailsModel result = await Users.Where(x => x.ID == userId).Select(x => new MessageActionRemoveMemberDetailsModel
            {
                Member = GetUserOverviewModel(x),
                Id = userId,
                Name = x.Surname + " " + x.Lastname
            }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<MessageActionDetailsModel> GetMessageActionProcedureFinishedDetails(Int32 procedureid)
        {
            MessageActionDetailsModel result = await Procedures.Where(x => x.Id == procedureid).Select(x => new MessageActionDetailsModel
            {
                Id = procedureid,
                Name = x.Name,
            }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<MessageActionCollectionJobDetailsModel> GetMessageActionCollectionJobDetails(Int32 collectionJobId)
        {
            MessageActionCollectionJobDetailsModel result = await CollectionJobs.Where(x => x.Id == collectionJobId).Select(x => new MessageActionCollectionJobDetailsModel
            {
                CollectionJob = new JobCollectionOverviewModel
                {
                    CollectionJobId = x.Id,
                    BoundedTo = GetSimpleCompanyOverview(x.Company),
                    FinishedTill = x.FinishedTill,
                    Name = x.Name,
                    Jobs = x.Jobs.OrderBy(y => x.Id).Select(y => new SimpleJobOverview { Id = y.Id }),
                },
                Id = collectionJobId,
                Name = x.Name,
            }).FirstOrDefaultAsync();

            if (result != null)
            {
                List<SimpleJobOverview> collectionJobs = new List<SimpleJobOverview>();
                foreach (Int32 item in result.CollectionJob.Jobs.Select(x => x.Id))
                {
                    collectionJobs.Add(await GetSimpleJobOverviewById(item));
                }

                result.CollectionJob.Jobs = collectionJobs;
            }

            return result;
        }

        public async Task<IEnumerable<MessageRawModel>> GetMessages(Boolean onlyUnreaded, String userAuthId)
        {
            Int32 userId = await GetUserIdByAuthId(userAuthId);

            IEnumerable<MessageRawModel> result = await Messages
                .Where(x => x.ReceiverId == userId && x.MarkedAsRead == !onlyUnreaded)
                .OrderByDescending(x => x.Timestamp)
                .Select(x => GetMessageRawModel(x)).ToListAsync();

            return result;

        }

        public async Task<IEnumerable<MessageRawModel>> GetMessages(MessageFilterModel filterModel, String userAuthId)
        {
            Int32 userId = await GetUserIdByAuthId(userAuthId);

            IQueryable<MessageDataModel> preResult = Messages.Where(x => x.ReceiverId == userId);
            if (filterModel.Actions.Count > 0)
            {
                preResult = preResult.Where(x => filterModel.Actions.Contains(x.Action) == true);
            }
            if (filterModel.Types.Count > 0)
            {
                preResult = preResult.Where(x => filterModel.Types.Contains(x.RelatedObjectType) == true);
            }
            if (filterModel.StartTime.HasValue == true)
            {
                preResult = preResult.Where(x => x.Timestamp >= filterModel.StartTime.Value);
            }
            if (filterModel.EndTime.HasValue == true)
            {
                preResult = preResult.Where(x => x.Timestamp <= filterModel.EndTime.Value);
            }
            if (String.IsNullOrEmpty(filterModel.Query) == false)
            {
                String normalizedQuery = filterModel.Query.Trim().ToLower();
                preResult = preResult.Where(x => EF.Functions.Like(x.Title, $"%{normalizedQuery}%") == true);
            }

            IEnumerable<MessageRawModel> result = await preResult
    .OrderByDescending(x => x.Timestamp)
    .Select(x => GetMessageRawModel(x)).Skip(filterModel.Start).Take(filterModel.Amount).ToListAsync();

            return result;
        }

        private MessageRawModel GetMessageRawModel(MessageDataModel dataModel)
        {
            return new MessageRawModel
            {
                Action = dataModel.Action,
                Details = dataModel.Details,
                Id = dataModel.Id,
                MarkedAsRead = dataModel.MarkedAsRead,
                RelatedObjectType = dataModel.RelatedObjectType,
                SourceUser = GetUserOverviewModel(dataModel.Sender),
                TimeStamp = dataModel.Timestamp,
            };
        }

        public async Task<Boolean> CheckIfMessageBelongsToUser(Int32 messageId, String userAuthId)
        {
            Int32 userId = await GetUserIdByAuthId(userAuthId);
            Int32 amount = await Messages.CountAsync(x => x.Id == messageId && x.ReceiverId == userId);
            return amount > 0;
        }

        public async Task<Boolean> SetReadStateToMessage(Boolean readState, Int32 messageId)
        {
            MessageDataModel dataModel = await Messages.FirstAsync(x => x.Id == messageId);
            dataModel.MarkedAsRead = readState;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfMessageExists(Int32 messageId)
        {
            Int32 amount = await Messages.CountAsync(x => x.Id == messageId);
            return amount > 0;
        }

    }
}
