﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        private IEnumerable<ICollection<T>> SplitElements<T>(IEnumerable<T> input, Int32 amountPerPart)
        {
            List<ICollection<T>> result = new List<ICollection<T>>();

            List<T> current = new List<T>();
            result.Add(current);
            foreach (T item in input)
            {
                if (current.Count == amountPerPart)
                {
                    current = new List<T>();
                    result.Add(current);
                }

                current.Add(item);
            }

            return result;
        }

        public async Task<Int32> CreateBuildingConnectionProcedure(BuildingConnectionProcedureCreateModel model)
        {
            BuildingConnectionProcedureDataModel dataModel = new BuildingConnectionProcedureDataModel(model);
            BuildingConnectionProcedures.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Int32> CreateCustomerConnectionProcedure(CustomerConnectionProcedureCreateModel model)
        {
            CustomerConnectionProcedureDataModel dataModel = new CustomerConnectionProcedureDataModel(model);
            CustomerConnectionProcedures.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Int32?> GetProcecureIdByRequestId(Int32 requestId)
        {
            Int32 id = await ProcedureTimeLineElements.Where(x => x.RelatedRequestId == requestId).Select(x => x.ProcedureId).FirstOrDefaultAsync();
            //  Int32 id = await Procedures.Where(x => x.Timeline.Count(y => y.RelatedRequestId == requestId) > 0 && x.IsFinished == false).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<Int32?> GetProcecureIdByJobId(Int32 jobId)
        {
            Int32 id = await ProcedureTimeLineElements.Where(x => x.RelatedJobId == jobId && x.Procedure.IsFinished == false).Select(x => x.ProcedureId).FirstOrDefaultAsync();
            //  Int32 id = await Procedures.Where(x => x.Timeline.Count(y => y.RelatedJobId == requestId) > 0 && x.IsFinished == false).Select(x => x.Id).FirstOrDefaultAsync();
            if (id <= 0) { return null; }
            else { return id; }
        }

        public async Task<Int32> AddProcedureTimelineElementIfNessesary(ProcedureTimelineCreateModel procedureTimeline)
        {
            if(await ProcedureTimeLineElements.CountAsync(x => 
                x.ProcedureId == procedureTimeline.ProcedureId && 
                x.RelatedJobId == procedureTimeline.RelatedJobId &&
                x.State == procedureTimeline.State) > 0)
            {
                return -1;
            }

            ProcedureTimeLineElementDataModel dataModel = new ProcedureTimeLineElementDataModel(procedureTimeline);
            ProcedureTimeLineElements.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<Boolean> MarkProcedureAsFinished(Int32 value)
        {
            ProcedureDataModel dataModel = await Procedures.FirstAsync(x => x.Id == value);
            dataModel.IsFinished = true;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckCheckIfProcedureIsFinished(Int32 procedureId)
        {
            //ProcedureStates finished = await Procedures.Where(x => x.Id == procedureId).Select(x => x.ExpectedEnd).FirstAsync();
            //ProcedureStates lastState = (await Procedures.Include(x => x.Timeline).Where(x => x.Id == procedureId)
            //    .Select(x => x.Timeline.OrderByDescending(y => y.TimeStamp).ThenByDescending(y => y.Id).Select(y => y.State).First()).ToListAsync())[0];

            Int32 result = await Procedures.Where(x => x.Id == procedureId && x.Timeline.Count(y => y.State == x.ExpectedEnd) > 0).CountAsync();
            return result > 0;
        }

        public async Task DeleteProcedureTimelineElements(IEnumerable<Int32> proceduresTimelineElements)
        {
            IEnumerable<ICollection<Int32>> splitedResult = SplitElements(proceduresTimelineElements, 20);

            foreach (ICollection<Int32> ids in splitedResult)
            {
                List<ProcedureTimeLineElementDataModel> elements = await ProcedureTimeLineElements.Where(x => ids.Contains(x.Id)).ToListAsync();
                ProcedureTimeLineElements.RemoveRange(elements);

                await SaveChangesAsync();
            }
        }

        public async Task DeleteProcedure(Int32 procedureId)
        {
            ProcedureDataModel procedureData = await Procedures.FirstAsync(x => x.Id == procedureId);
            Procedures.Remove(procedureData);

            await SaveChangesAsync();
        }


        public async Task DeleteProcedures(IEnumerable<Int32> procedureIds)
        {
            IEnumerable<ICollection<Int32>> parts = SplitElements(procedureIds, 20);

            foreach (ICollection<Int32> ids in parts)
            {
                List<ProcedureDataModel> dataModels = await Procedures.Where(x => ids.Contains(x.Id)).ToListAsync();
                Procedures.RemoveRange(dataModels);
                await SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<ProcedureOverviewModel>> GetProcedures(ProcedureFilterModel filter, ProcedureSortProperties sortProperty, SortDirections direction)
        {
            IQueryable<ProcedureDataModel> preQuery = Procedures;

            var dbQuery = preQuery.Select(x => new
            {
                AsSoonAsPossible = x is CustomerConnectionProcedureDataModel ? ((CustomerConnectionProcedureDataModel)x).ActivationAsSoonAsPossible : new Boolean?(),
                InformSalesAfterFinishedState = x is BuildingConnectionProcedureDataModel ? ((BuildingConnectionProcedureDataModel)x).InformSalesAfterFinish : new Boolean?(),
                LatestState = x.Timeline.Select(y => new { y.TimeStamp, y.State }).OrderByDescending(y => y.TimeStamp).First(),
                Name = x.Name,
                IsOpen = !x.IsFinished,
                Procedure = x,
                Appointment = x is BuildingConnectionProcedureDataModel == true ? ((BuildingConnectionProcedureDataModel)x).Appointment : x is CustomerConnectionProcedureDataModel == true ? ((CustomerConnectionProcedureDataModel)x).Appointment : new DateTimeOffset?(),
                Type = x is BuildingConnectionProcedureDataModel == true ? ProcedureTypes.HouseConnection : ProcedureTypes.CustomerConnection,
                ContactId = x is BuildingConnectionProcedureDataModel == true ? ((BuildingConnectionProcedureDataModel)x).OwnerContactId : x is CustomerConnectionProcedureDataModel == true ? ((CustomerConnectionProcedureDataModel)x).CustomerContactId : -1,
                BuildingOrFlatId = x is BuildingConnectionProcedureDataModel == true ? ((BuildingConnectionProcedureDataModel)x).BuildingId : x is CustomerConnectionProcedureDataModel == true ? ((CustomerConnectionProcedureDataModel)x).FlatId : -1,
            });

            if(filter.AsSoonAsPossibleState.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.AsSoonAsPossible.HasValue == true && x.AsSoonAsPossible.Value == filter.AsSoonAsPossibleState.Value);
            } 
            
            if(filter.InformSalesAfterFinishedState.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.InformSalesAfterFinishedState.HasValue == true && x.InformSalesAfterFinishedState.Value == filter.InformSalesAfterFinishedState.Value);
            }

            if (filter.Type.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.Type == filter.Type.Value);
            }

            if (filter.OpenState.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.IsOpen == filter.OpenState.Value);
            }
            if (filter.State.HasValue == true)
            {
                switch (filter.StateOperator.Value)
                {
                    case FilterOperations.Equal:
                        dbQuery = dbQuery.Where(x => x.LatestState.State == filter.State.Value);
                        break;
                    case FilterOperations.Unqual:
                        dbQuery = dbQuery.Where(x => x.LatestState.State != filter.State.Value);
                        break;
                    case FilterOperations.Greater:
                        dbQuery = dbQuery.Where(x => x.LatestState.State > filter.State.Value);
                        break;
                    case FilterOperations.Smaller:
                        dbQuery = dbQuery.Where(x => x.LatestState.State < filter.State.Value);
                        break;
                    default:
                        break;
                }
            }

            if (filter.StartDate.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.Appointment.HasValue == true && x.Appointment.Value >= filter.StartDate.Value);
            }

            if (filter.EndDate.HasValue == true)
            {
                dbQuery = dbQuery.Where(x => x.Appointment.HasValue == true && x.Appointment.Value <= filter.EndDate.Value);
            }

            if (String.IsNullOrEmpty(filter.Query) == false)
            {
                String parsedQuery = $"%{filter.Query.ToLower()}%";

                dbQuery = dbQuery.Where(x => EF.Functions.Like(x.Procedure.Name, parsedQuery) == true);
            }

            if (direction == SortDirections.Ascending)
            {
                switch (sortProperty)
                {
                    case ProcedureSortProperties.State:
                        dbQuery = dbQuery.OrderBy(x => x.LatestState.State);
                        break;
                    case ProcedureSortProperties.Appointment:
                        dbQuery = dbQuery.OrderBy(x => x.Appointment);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (sortProperty)
                {
                    case ProcedureSortProperties.State:
                        dbQuery = dbQuery.OrderByDescending(x => x.LatestState.State);
                        break;
                    case ProcedureSortProperties.Appointment:
                        dbQuery = dbQuery.OrderByDescending(x => x.Appointment);
                        break;
                    default:
                        break;
                }
            }

            IEnumerable<ProcedureOverviewModel> result = await dbQuery
                .Select(x => new ProcedureOverviewModel
                {
                    Id = x.Procedure.Id,
                    Current = x.LatestState.State,
                    Contact = new PersonInfo { Id = x.ContactId },
                    Start = x.Procedure.Start,
                    End = x.Procedure.ExpectedEnd,
                    IsOpen = !x.Procedure.IsFinished,
                    Name = x.Procedure.Name,
                    Type = x.Type,
                    Building = new SimpleBuildingOverviewModel { Id = x.BuildingOrFlatId },
                }).ToListAsync();

            HashSet<Int32> contactIds = new HashSet<Int32>(result.Select(x => x.Contact.Id));
            //HashSet<Int32> procedureIds = new HashSet<int>(result.Select(x => x.Id));

            Dictionary<Int32, PersonInfo> contactInfos = await ContactInfos.Where(x => contactIds.Contains(x.Id) == true).Select(x => GetPersonInfo(x)).ToDictionaryAsync(x => x.Id, x => x);
            contactInfos.Add(-1, null);

            Dictionary<Int32, SimpleBuildingOverviewModel> cachedBuildings = new Dictionary<int, SimpleBuildingOverviewModel>();
            Dictionary<Int32, SimpleBuildingOverviewModel> cachedBuildingsByFlat = new Dictionary<int, SimpleBuildingOverviewModel>();

            foreach (ProcedureOverviewModel item in result)
            {
                item.Contact = contactInfos[item.Contact.Id];

                if (item.Type == ProcedureTypes.HouseConnection)
                {
                    if (cachedBuildings.ContainsKey(item.Building.Id) == false)
                    {
                        item.Building = await Buildings.Where(x => x.Id == item.Building.Id).Select(x => GetSimpleBuildingOverviewModel(x)).FirstAsync();
                        cachedBuildings.Add(item.Building.Id, item.Building);
                    }
                    else
                    {
                        item.Building = cachedBuildings[item.Building.Id];
                    }
                }
                else
                {
                    if (cachedBuildingsByFlat.ContainsKey(item.Building.Id) == false)
                    {
                        item.Building = await Flats.Where(x => x.Id == item.Building.Id).Include(x => x.Building).Select(x => GetSimpleBuildingOverviewModel(x.Building)).FirstAsync();
                        cachedBuildingsByFlat.Add(item.Building.Id, item.Building);
                    }
                    else
                    {
                        item.Building = cachedBuildingsByFlat[item.Building.Id];
                    }
                }
            }

            if (filter.BuildingUnitAmount.HasValue == true)
            {
                switch (filter.BuildingUnitAmountOperation.Value)
                {
                    case FilterOperations.Equal:
                        result = result.Where(x => x.Building.CommercialUnits + x.Building.HouseholdUnits == filter.BuildingUnitAmount.Value);
                        break;
                    case FilterOperations.Unqual:
                        result = result.Where(x => x.Building.CommercialUnits + x.Building.HouseholdUnits != filter.BuildingUnitAmount.Value);
                        break;
                    case FilterOperations.Greater:
                        result = result.Where(x => x.Building.CommercialUnits + x.Building.HouseholdUnits > filter.BuildingUnitAmount.Value);
                        break;
                    case FilterOperations.Smaller:
                        result = result.Where(x => x.Building.CommercialUnits + x.Building.HouseholdUnits < filter.BuildingUnitAmount.Value);
                        break;
                    default:
                        break;
                }
            }

            if (sortProperty == ProcedureSortProperties.BuildingName)
            {
                if (direction == SortDirections.Ascending)
                {
                    result = result.OrderBy(x => x.Building.StreetName);
                }
                else
                {
                    result = result.OrderByDescending(x => x.Building.StreetName);
                }
            }

            return result.Skip(filter.Start).Take(filter.Amount).ToList();
        }

        public async Task<Boolean> CheckIfBuildingConnectionProcedureExists(Int32 procedureId)
        {
            Int32 result = await BuildingConnectionProcedures.CountAsync(x => x.Id == procedureId);
            return result > 0;
        }

        public async Task<Boolean> CheckIfCustomerConnectionProcedureExists(Int32 procedureId)
        {
            Int32 result = await CustomerConnectionProcedures.CountAsync(x => x.Id == procedureId);
            return result > 0;
        }
        
        private async Task<TOutput> GetProcedureDetails<TOutput, TDataModel>(IQueryable<ProcedureDataModel> preQuery, Int32 procedureId, Action<TDataModel, TOutput> transformer) where TOutput : ProcedureDetailModel, new()
           where TDataModel : ProcedureDataModel
        {
            TDataModel dataModel = await preQuery.Include(x => x.Timeline).FirstAsync(x => x.Id == procedureId) as TDataModel;
            TOutput result = new TOutput
            {
                Id = dataModel.Id,
                Timeline = dataModel.Timeline.OrderByDescending(x => x.TimeStamp).Select(x => GetProcedureTimelineElementDetailModel(x)).ToList(),
                IsClosed = dataModel.IsFinished,
                Start = dataModel.Start,
                ExpectedEnd = dataModel.ExpectedEnd,
                Name = dataModel.Name,
            };

            result.Current = result.Timeline.First().State;

            transformer?.Invoke(dataModel, result);

            foreach (var item in result.Timeline)
            {
                if (item.RelatedJobId.HasValue == false) { continue; }

                item.RelatedJobType = await GetJobType(item.RelatedJobId.Value);
            }

            return result;
        }

        public async Task<BuildingConnectionProcedureDetailModel> GetConnectBuildingDetails(Int32 procedureId)
        {
            IQueryable<ProcedureDataModel> preQuery = BuildingConnectionProcedures.Include(x => x.Building).Include(x => x.Owner);

            BuildingConnectionProcedureDetailModel result = await GetProcedureDetails<BuildingConnectionProcedureDetailModel, BuildingConnectionProcedureDataModel>(preQuery,
                procedureId, (dataModel, model) =>
                {
                    model.Building = GetSimpleBuildingOverviewWithGPSModel(dataModel.Building);
                    model.Contact = GetPersonInfo(dataModel.Owner);
                    model.Appointment = dataModel.Appointment;
                    model.DeclarationOfAggrementInStock = dataModel.DeclarationOfAggrementInStock;
                    model.InformSalesAfterFinish = dataModel.InformSalesAfterFinish;
                }
                );

            return result;
        }

        public async Task<CustomerConnectionProcedureDetailModel> GetConnectCustomerDetails(Int32 procedureId)
        {
            IQueryable<ProcedureDataModel> preQuery = CustomerConnectionProcedures.Include(x => x.Flat).ThenInclude(x => x.Building).Include(x => x.Customer);

            CustomerConnectionProcedureDetailModel result = await GetProcedureDetails<CustomerConnectionProcedureDetailModel, CustomerConnectionProcedureDataModel>(preQuery,
                procedureId, (dataModel, model) =>
                {
                    model.Flat = GetSimpleFlatModelOverviewModel(dataModel.Flat);
                    model.Building = GetSimpleBuildingOverviewWithGPSModel(dataModel.Flat.Building);
                    model.Contact = GetPersonInfo(dataModel.Customer);
                    model.Appointment = dataModel.Appointment;
                    model.ActivationAsSoonAsPossible = dataModel.ActivationAsSoonAsPossible;
                    model.DeclarationOfAggrementInStock = dataModel.DeclarationOfAggrementInStock;
                    model.ContractInStock = dataModel.ContractInStock;
                    model.ActivationAsSoonAsPossible = dataModel.ActivationAsSoonAsPossible;
                }
                );

            return result;
        }

        public async Task<SpliceBranchableJobCreateModel> GetCreateSpliceJobInfoByCustomerConnectionProcedureId(Int32 procedureId)
        {
            SpliceBranchableJobCreateModel result = await CustomerConnectionProcedures.Where(x => x.Id == procedureId).Select(x => new SpliceBranchableJobCreateModel
            {
                CustomerContactId = x.CustomerContactId,
                FlatId = x.FlatId,
            }).FirstAsync();

            return result;
        }

        public async Task<ActivationJobCreateModel> GetCreateActivationJobInfoByCustomerConnectionProcedureId(Int32 procedureId)
        {
            ActivationJobCreateModel result = await CustomerConnectionProcedures.Where(x => x.Id == procedureId).Select(x => new ActivationJobCreateModel
            {
                CustomerContactId = x.CustomerContactId,
                FlatId = x.FlatId,
            }).FirstAsync();

            return result;
        }

        public async Task<Boolean> CheckIfCustomerConnectionProcedureExistsByFlatId(Int32 flatId)
        {
            Int32 amount = await CustomerConnectionProcedures.CountAsync(x => x.FlatId == flatId);
            return amount > 0;
        }

        public async Task<Int32> GetCustomerConnectionProcedureByFlatId(Int32 flatId)
        {
            Int32 id = await CustomerConnectionProcedures.Where(x => x.FlatId == flatId).Select(x => x.Id).FirstAsync();
            return id;
        }

        public async Task<Boolean> CheckIfBuildingConnectionProcedureExistsByBuildingId(Int32 buildingId)
        {
            Int32 amount = await BuildingConnectionProcedures.CountAsync(x => x.BuildingId == buildingId);
            return amount > 0;
        }

        public async Task<Int32> GetBuildingConnectionProcedureByBuildingId(Int32 buildingId)
        {
            Int32 id = await BuildingConnectionProcedures.Where(x => x.BuildingId == buildingId).Select(x => x.Id).FirstAsync();
            return id;
        }

        public async Task<IEnumerable<ProcedureWithCurrentTimelineModel>> GetOpenProceduresWithCurrentTimelineelement()
        {
            IEnumerable<ProcedureWithCurrentTimelineModel> result = await Procedures.Where(x => x.IsFinished == false)
                .Select(x => new ProcedureWithCurrentTimelineModel
                {
                    Id = x.Id,
                    ExpectedEnd = x.ExpectedEnd,
                    CurrentTimelineElement = x.Timeline.OrderByDescending(y => y.TimeStamp).Select(y => GetProcedureTimelineElementDetailModel(y)).First()
                }).ToListAsync();

            return result;

        }

        public async Task<Boolean> CheckIfContactAfterFinishedJobShouldCreated(Int32 procedureId)
        {
            Boolean result = false;
            if(await BuildingConnectionProcedures.CountAsync(x => x.Id == procedureId) > 0)
            {
                result = await BuildingConnectionProcedures.Where(x => x.Id == procedureId).Select(x => x.InformSalesAfterFinish).FirstAsync();
            }

            return result;
        }

        public async Task<ProcedureTypes> GetProcedureTypeById(Int32 procedureId)
        {
            ProcedureTypes result = ProcedureTypes.CustomerConnection;
            ProcedureDataModel dataModel = await Procedures.FirstAsync(x => x.Id == procedureId);
            if(dataModel is BuildingConnectionProcedureDataModel)
            {
                result = ProcedureTypes.HouseConnection;
            }

            return result;
        }

        #region Helper

        private ProcedureTimelineElementDetailModel GetProcedureTimelineElementDetailModel(ProcedureTimeLineElementDataModel dataModel)
        {
            return new ProcedureTimelineElementDetailModel
            {
                Id = dataModel.Id,
                RelatedJobId = dataModel.RelatedJobId,
                RelatedRequestId = dataModel.RelatedRequestId,
                State = dataModel.State,
                Timestamp = dataModel.TimeStamp,
                Comment = dataModel.Comment,
            };
        }

        #endregion
    }
}
