﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model)
        {
            ProtocolEntryDataModel dataModel = new ProtocolEntryDataModel(model);
            ProtocolEntries.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        public async Task<IDictionary<Int32, ProtocolNotificationTypes>> GetUsersToInform(MessageRelatedObjectTypes relatedType, MessageActions action)
        {
            IDictionary<Int32, ProtocolNotificationTypes> result = await
            ProtocolInformEntries.Where(x => x.RelatedObject == relatedType && x.Action == action).ToDictionaryAsync(x => x.UserId, x => x.NotifationTypes);

            return result;
        }

        public async Task<IDictionary<Int32, ProtocolNotificationTypes>> GetUsersToInform(MessageRelatedObjectTypes relatedType, MessageActions action, IEnumerable<Int32> userIds)
        {
            IDictionary<Int32, ProtocolNotificationTypes> result = await
         ProtocolInformEntries.Where(x => x.RelatedObject == relatedType && x.Action == action && userIds.Contains(x.UserId) == true)
         .ToDictionaryAsync(x => x.UserId, x => x.NotifationTypes);

            return result;
        }

        public async Task<ProtocolNotificationTypes> GetProtocolNofitcation(MessageRelatedObjectTypes type, MessageActions action, Int32 userId)
        {
            ProtocolNotificationTypes result = await ProtocolInformEntries
                .Where(x => x.UserId == userId && x.Action == action && x.RelatedObject == type)
                .Select(x => x.NotifationTypes)
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<IEnumerable<ProtocolEntryModel>> GetProtocolEntries(ProtocolFilterModel filterModel)
        {
            IQueryable<ProtocolEntryDataModel> preResult = ProtocolEntries;
            if (filterModel.StartTime.HasValue == true)
            {
                preResult = preResult.Where(x => x.Timestamp >= filterModel.StartTime.Value);
            }

            if (filterModel.EndTime.HasValue == true)
            {
                preResult = preResult.Where(x => x.Timestamp <= filterModel.EndTime.Value);
            }

            if (filterModel.Actions.Count > 0)
            {
                preResult = preResult.Where(x => filterModel.Actions.Contains(x.Action));
            }

            if (filterModel.Types.Count > 0)
            {
                preResult = preResult.Where(x => filterModel.Types.Contains(x.RelatedType));
            }

            IEnumerable<ProtocolEntryModel> result = await preResult.Select(x => new ProtocolEntryModel
            {
                Action = x.Action,
                Id = x.Id,
                RelatedObjectId = x.RelatedObjectId,
                SourcedBy = GetUserOverviewModel(x.TriggeredUser),
                Timestamp = x.Timestamp,
                Type = x.RelatedType,
            }).OrderBy(x => x.Id).Skip(filterModel.Start).Take(filterModel.Amount).ToListAsync();

            foreach (ProtocolEntryModel item in result)
            {
                if (item.RelatedObjectId > 0)
                {
                    item.Name = await GetMessageObjectName(item.Id, item.Type);
                }
            }

            return result;
        }

        public async Task<IEnumerable<ProtocolUserInformModel>> GetUserProtocolInformation(String userAuthId)
        {
            Int32 userId = await GetUserIdByAuthId(userAuthId);
            IEnumerable<ProtocolUserInformModel> result = await GetUserProtocolInformation(userId);
            return result;
        }

        public async Task<ICollection<ProtocolUserInformModel>> GetUserProtocolInformation(Int32 userId)
        {
            ICollection<ProtocolUserInformModel> result = await ProtocolInformEntries.Where(x => x.UserId == userId)
                .OrderBy(x => x.RelatedObject).ThenBy(x => x.Action).Select(x => new ProtocolUserInformModel
                {
                    Action = x.Action,
                    NotifcationType = x.NotifationTypes,
                    RelatedType = x.RelatedObject
                }).ToListAsync();

            return result;
        }

        public async Task<Boolean> UpdateProtocolNotification(String userAuthId, ProtocolUserInformModel item)
        {
            Int32 userId = await GetUserIdByAuthId(userAuthId);

            ProtocolUserInformDataModel informModel = await ProtocolInformEntries.Where(x =>
            x.UserId == userId && x.Action == item.Action && x.RelatedObject == item.RelatedType).FirstAsync();

            informModel.NotifationTypes = item.NotifcationType;

            Int32 changes = await SaveChangesAsync();

            return changes > 0;
        }

        public async Task<Boolean> CreateProtocolNotificaiton(Int32 userId, ProtocolUserInformModel item)
        {
            ProtocolUserInformDataModel informModel = new ProtocolUserInformDataModel(userId, item);

            ProtocolInformEntries.Add(informModel);
            Int32 changes = await SaveChangesAsync();

            return changes > 0;
        }

        public async Task<Boolean> DeleteProtocolNotification(Int32 userId, ProtocolUserInformModel item)
        {
            ProtocolUserInformDataModel informModel = await ProtocolInformEntries.Where(x =>
            x.UserId == userId && x.Action == item.Action && x.RelatedObject == item.RelatedType).FirstAsync();

            ProtocolInformEntries.Remove(informModel);
            Int32 changes = await SaveChangesAsync();

            return changes > 0;
        }
    }
}
