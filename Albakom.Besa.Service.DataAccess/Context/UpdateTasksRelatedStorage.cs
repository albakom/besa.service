﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Boolean> CheckIfOpenUpdateTasksExists(UpdateTasks taskType)
        {
            Int32 amount = await UpdateTasks.CountAsync(x => x.Task == taskType && x.Ended.HasValue == false);
            return amount > 0;
        }

        public async Task<Int32> CreateUpdateTask(UpdateTaskCreateModel createModel)
        {
            Int32 userId = await GetUserIdByAuthId(createModel.UserAuthId);

            UpdateTaskDataModel dataModel = new UpdateTaskDataModel(createModel, userId);
            UpdateTasks.Add(dataModel);

            await SaveChangesAsync();

            return dataModel.Id;
        }

        public async Task<IEnumerable<UpdateTaskElementModel>> GetOpenUpdateTaskElement(UpdateTasks taskType)
        {
            IEnumerable<UpdateTaskElementModel> result = await UpdateTaskElements.Where(
                x => x.Task.Ended.HasValue == false && x.Task.Task == taskType && x.State == UpdateTaskElementStates.NotStarted)
                .Select(x => new UpdateTaskElementModel
                {
                    Id = x.Id,
                    AdapterId = x.AdapterId,
                    ObjectId = x.ObjectId,
                }).ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfUpdateTaskElementExists(Int32 elementId)
        {
            Int32 amount = await UpdateTaskElements.CountAsync(x => x.Id == elementId);
            return amount > 0;
        }

        public async Task<Boolean> SetUpdateTaskElementAsProcessing(Int32 elementId)
        {
            UpdateTaskElementDataModel dataModel = await UpdateTaskElements.Include(x => x.Task).FirstAsync(x => x.Id == elementId);
            dataModel.State = UpdateTaskElementStates.InProgress;

            DateTimeOffset now = DateTimeOffset.Now;
            dataModel.Started = now;

            dataModel.Task.Heartbeat = now;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> EditUpdateTaskElement(UpdateTaskElementUpdateModel updateModel)
        {
            UpdateTaskElementDataModel dataModel = await UpdateTaskElements.Include(x => x.Task).FirstAsync(x => x.Id == updateModel.ElementId);
            dataModel.Update(updateModel);

            dataModel.Task.Heartbeat = updateModel.Timestamp;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> GetUpdateTaskIdByTaskTye(UpdateTasks taskType)
        {
            Int32 id = await UpdateTasks.Where(x => x.Ended.HasValue == false && x.Task == taskType).
                Select(x => x.Id).FirstAsync();

            return id;
        }

        public async Task<Boolean> CheckIfTaskHasOpenElements(UpdateTasks taskType)
        {
            Int32 result = await UpdateTaskElements.CountAsync(x => x.Task.Ended.HasValue == false && x.Task.Task == taskType && x.Ended.HasValue == false);
            return result > 0;
        }

        public async Task<Boolean> EditUpdateTask(UpdateTaskFinishModel finishModel)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.FirstAsync(x => x.Id == finishModel.TaskId);
            dataModel.Update(finishModel);

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfTaskIsCanceledByElementId(Int32 elementId)
        {
            Boolean result = await UpdateTaskElements.Where(x => x.Id == elementId).Select(x => x.Task.Canceled).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfTaskElementIsAlreadyFinished(Int32 elementId)
        {
            Boolean result = await UpdateTaskElements
                .Where(x => x.Id == elementId)
                .Select(x => x.State == UpdateTaskElementStates.Success || x.State == UpdateTaskElementStates.Waring || x.State == UpdateTaskElementStates.Error)
               .FirstAsync();

            return result;
        }

        public async Task<Boolean> ResetUpdateTaskElement(Int32 elementId)
        {
            UpdateTaskElementDataModel elementDataModel = await UpdateTaskElements.FirstAsync(x => x.Id == elementId);

            elementDataModel.Started = null;
            elementDataModel.Ended = null;
            elementDataModel.State = UpdateTaskElementStates.NotStarted;
            elementDataModel.Result = String.Empty;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<Int32>> GetUpdateTaskElementsInProgressSince(DateTimeOffset timestamp)
        {
            IEnumerable<Int32> result = await UpdateTaskElements
                .Where(x => x.State == UpdateTaskElementStates.InProgress && x.Started <= timestamp && x.Task.Heartbeat <= timestamp)
                .Select(X => X.Id)
                .ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfTaskHeartBeatIsOlderThen(DateTimeOffset timestamp, UpdateTasks taskType)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.Where(x => x.Ended.HasValue == false && x.Task == taskType).FirstOrDefaultAsync();
            if (dataModel == null) { return false; }

            return dataModel.Heartbeat <= timestamp;
        }

        public async Task<Boolean> UpdateTaskHeartBeatByElementId(Int32 elementId, DateTimeOffset value)
        {
            UpdateTaskDataModel dataModel = await UpdateTaskElements.Where(x => x.Id == elementId).Select(x => x.Task).FirstAsync();
            dataModel.Heartbeat = value;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Boolean> CheckIfTaskIsLocked(UpdateTasks taskType)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.Where(x => x.Ended.HasValue == false && x.Task == taskType).AsNoTracking().FirstOrDefaultAsync();
            if (dataModel == null) { return false; }

            return dataModel.IsLocked;
        }

        public async Task<Boolean> SetLockStateOfTask(Int32 taskId, Boolean lockedState)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.FirstAsync(x => x.Id == taskId);
            dataModel.IsLocked = lockedState;
            dataModel.Heartbeat = DateTimeOffset.Now;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<Int32> GetUpdateTaskIdByElementId(Int32 taskElementId)
        {
            Int32 id = await UpdateTaskElements.Where(x => x.Id == taskElementId).Select(x => x.TaskId).FirstAsync();
            return id;
        }

        public async Task<Boolean> UpdateHeartbeatByTaskId(Int32 taskId, DateTimeOffset hearbeat)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.FirstAsync(x => x.Id == taskId);
            dataModel.Heartbeat = hearbeat;

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllBranchablesAsUpdateTaskObjectOverviewModel()
        {
            IEnumerable<UpdateTaskObjectOverviewModel> result = await Branchables.OrderBy(x => x.NormalizedName).Select(
               x => new UpdateTaskObjectOverviewModel
               {
                   Id = x.Id,
                   Name = x.Name,
                   ProjectAdapterId = x.ProjectId,
                   Type = MessageRelatedObjectTypes.Branchable,
               }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllPoPsAsUpdateTaskObjectOverviewModel()
        {
            IEnumerable<UpdateTaskObjectOverviewModel> result = await Pops.OrderBy(x => x.Name).Select(
                   x => new UpdateTaskObjectOverviewModel
                   {
                       Id = x.Id,
                       Name = x.Name,
                       ProjectAdapterId = x.ProjectAdapterId,
                       Type = MessageRelatedObjectTypes.PoP,
                   }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetAllFlatCablesAsUpdateTaskObjectOverviewModel()
        {
            IEnumerable<UpdateTaskObjectOverviewModel> result = await FiberCables.Where(x => x.FlatId.HasValue == true || x.BuildingId.HasValue == true).OrderBy(x => x.Name).Select(
                   x => new UpdateTaskObjectOverviewModel
                   {
                       Id = x.Id,
                       Name = x.FlatId.HasValue == true ? x.StartingFlat.Building.StreetName + " - " + x.StartingFlat.Number : (x.BuildingId.HasValue == true ? x.ForBuilding.StreetName : x.Name),
                       ProjectAdapterId = x.ProjectAdapterId,
                       Type = MessageRelatedObjectTypes.FiberCable,
                   }).ToListAsync();

            return result;
        }

        public async Task<Boolean> CheckIfUpdateTasksExists(Int32 taskId)
        {
            Int32 amount = await UpdateTasks.CountAsync(x => x.Id == taskId);
            return amount > 0;
        }

        public async Task<Boolean> CheckIfUpdateTaskIsCanceledByTaskId(Int32 taskId)
        {
            Boolean result = await UpdateTasks.Where(x => x.Id == taskId).Select(x => x.Canceled).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfUpdateTaskIsFinished(Int32 taskId)
        {
            DateTimeOffset? value = await UpdateTasks.Where(x => x.Id == taskId).Select(x => x.Ended).FirstAsync();
            return value.HasValue == true;
        }

        public async Task<Boolean> CancelUpdateTask(Int32 taskId)
        {
            UpdateTaskDataModel dataModel = await UpdateTasks.Include(x => x.Elements).FirstAsync(x => x.Id == taskId);
            dataModel.Ended = DateTimeOffset.Now;
            dataModel.Canceled = true;

            foreach (UpdateTaskElementDataModel item in dataModel.Elements.Where(x => x.State == UpdateTaskElementStates.NotStarted))
            {
                item.State = UpdateTaskElementStates.Canceled;
            }

            Int32 changes = await SaveChangesAsync();
            return changes > 0;
        }

        public async Task<IEnumerable<UpdateTaskOverviewModel>> GetUpdateTasks(UpdateTasks? taskType)
        {
            IQueryable<UpdateTaskDataModel> preResult = UpdateTasks.Include(x => x.StartedBy);
            if (taskType.HasValue == true)
            {
                preResult = preResult.Where(x => x.Task == taskType.Value);
            }

            IEnumerable<UpdateTaskOverviewModel> result = await preResult.Select(x => GetUpdateTaskOverviewModel(x)).ToListAsync();
            return result;
        }

        private async Task GetUpdateTaskElementObjectName(UpdateTaskElementRawOverviewModel model, UpdateTasks taskType)
        {
            String name = String.Empty;
            switch (taskType)
            {
                case Contracts.Models.UpdateTasks.MainCableForPops:
                    name = await Pops.Where(x => x.Id == model.RelatedObject.Id).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case Contracts.Models.UpdateTasks.PatchConnections:
                    name = await FiberCables.Where(x => x.Id == model.RelatedObject.Id).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                case Contracts.Models.UpdateTasks.Splices:
                case Contracts.Models.UpdateTasks.MainCableForBranchable:
                case Contracts.Models.UpdateTasks.Cables:
                    name = await Branchables.Where(x => x.Id == model.RelatedObject.Id).Select(x => x.Name).FirstOrDefaultAsync();
                    break;
                default:
                    break;
            }

            model.RelatedObject.Name = name;
        }

        public async Task<UpdateTaskRawDetailModel> GetUpdateTaskDetails(Int32 taskId)
        {
            UpdateTaskRawDetailModel result = await UpdateTasks.Include(x => x.StartedBy).Where(x => x.Id == taskId).Select(x => new UpdateTaskRawDetailModel
            {
                Overview = GetUpdateTaskOverviewModel(x),
                Elements = x.Elements.Select(y => GetUpdateTaskElementRawOverviewModel(y))
            }).FirstAsync();

            result.Elements = result.Elements.ToList();

            foreach (UpdateTaskElementRawOverviewModel item in result.Elements)
            {
                await GetUpdateTaskElementObjectName(item, result.Overview.TaskType);
            }

            return result;
        }

        public async Task<UpdateTaskElementRawOverviewModel> GetUpdateTaskElementOverviewById(Int32 elementId)
        {
            UpdateTaskElementRawOverviewModel updateTaskElement = await UpdateTaskElements.Where(x => x.Id == elementId).Select(x => GetUpdateTaskElementRawOverviewModel(x)).FirstAsync();
            UpdateTasks task = await UpdateTaskElements.Where(x => x.Id == elementId).Select(x => x.Task.Task).FirstAsync();

            await GetUpdateTaskElementObjectName(updateTaskElement, task);
            return updateTaskElement;
        }

        public async Task<UpdateTaskOverviewModel> GetUpdateTaskOverviewById(Int32 id)
        {
            UpdateTaskOverviewModel result = await UpdateTasks.Include(x => x.StartedBy).Where(x => x.Id == id).Select(x => GetUpdateTaskOverviewModel(x)).FirstAsync();
            return result;
        }

        private UpdateTaskElementRawOverviewModel GetUpdateTaskElementRawOverviewModel(UpdateTaskElementDataModel dataModel)
        {
            return new UpdateTaskElementRawOverviewModel
            {
                Id = dataModel.Id,
                Ended = dataModel.Ended,
                State = dataModel.State,
                Started = dataModel.Started,
                RelatedObject = new UpdateTaskObjectOverviewModel
                {
                    Id = dataModel.ObjectId,
                    ProjectAdapterId = dataModel.AdapterId,
                },
                RawResult = dataModel.Result,
            };
        }

        private UpdateTaskOverviewModel GetUpdateTaskOverviewModel(UpdateTaskDataModel dataModel)
        {
            return new UpdateTaskOverviewModel
            {
                CreatedBy = new SimpleUserOverviewModel
                {
                    Id = dataModel.StartedByUserId,
                    Name = dataModel.StartedBy.Lastname,
                },
                Started = dataModel.Started,
                Endend = dataModel.Ended,
                Id = dataModel.Id,
                LastHearbeat = dataModel.Heartbeat,
                TaskType = dataModel.Task,
                WasCanceled = dataModel.Canceled,
            };
        }

    }
}
