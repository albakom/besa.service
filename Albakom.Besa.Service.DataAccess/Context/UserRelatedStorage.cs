﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Albakom.Besa.Service.DataAccess.Context
{
    public partial class BesaDataStorage : IBesaDataStorage
    {
        public async Task<Boolean> CheckIfUserExists(String authID)
        {
            Int32 result = await Users.CountAsync(x => x.AuthServiceId == authID);
            return result > 0;
        }

        public async Task<Boolean> CheckIfUserExists(Int32 id)
        {
            Int32 result = await Users.CountAsync(x => x.ID == id);
            return result > 0;
        }

        public async Task<Boolean> CheckIfUsersExists(ICollection<Int32> userIdsToCheck)
        {
            Int32 amount = await Users.Select(x => x.ID).Intersect(userIdsToCheck).CountAsync();
            return amount == userIdsToCheck.Count;
        }

        public async Task<Boolean> CheckIfUserHasRole(String authID, BesaRoles role)
        {
            Int32 result = await Users.CountAsync(x => x.AuthServiceId == authID && (x.Role & role) == role);
            return result > 0;
        }

        public async Task<Boolean> CheckIfUserAccessRequestExists(String authServiceId)
        {
            Int32 result = await UserAccessRequests.CountAsync(x => x.AuthServiceId == authServiceId);
            return result > 0;
        }

        public async Task<Boolean> CheckIfUserAccessRequestExists(Int32 authRequestId)
        {
            Int32 result = await UserAccessRequests.CountAsync(x => x.Id == authRequestId);
            return result > 0;
        }

        public async Task<Boolean> CheckIfUserHasCompany(String authId)
        {
            Int32 result = await Users.CountAsync(x => x.AuthServiceId == authId && x.CompanyId.HasValue == true);
            return result > 0;
        }

        public async Task<Int32> CreateUserAccessRequest(UserAccessRequestCreateModel model)
        {
            UserAccessRequestDataModel dataModel = new UserAccessRequestDataModel(model);
            UserAccessRequests.Add(dataModel);

            await SaveChangesAsync();
            return dataModel.Id;
        }

        private UserAccessRequestOverviewModel GetUserAccessRequestOverviewModel(UserAccessRequestDataModel dataModel)
        {
            return new UserAccessRequestOverviewModel
            {
                Id = dataModel.Id,
                EMailAddress = dataModel.Email,
                Lastname = dataModel.Lastname,
                Surname = dataModel.Surname,
                Phone = dataModel.Phone,
                GeneratedAt = dataModel.CreatedAt,
                CompanyInfo = GetSimpleCompanyOverview(dataModel.Company)
            };
        }

        public async Task<IEnumerable<UserAccessRequestOverviewModel>> GetAllUserAccessRequest()
        {
            IEnumerable<UserAccessRequestOverviewModel> result = await (from request in UserAccessRequests.Include(x => x.Company)
                                                                        select GetUserAccessRequestOverviewModel(request)
                                                                   ).ToListAsync();

            return result;
        }

        public async Task<UserAccessRequestOverviewModel> GetUserRequestOverviewById(Int32 requestId)
        {
            UserAccessRequestOverviewModel result = await UserAccessRequests.Include(x => x.Company)
                .Where(x => x.Id == requestId).Select(x => GetUserAccessRequestOverviewModel(x)).FirstAsync();

            return result;
        }

        public async Task<String> GetAuthServiceIdByAccessRequestId(Int32 authRequestId)
        {
            String result = await UserAccessRequests.Where(x => x.Id == authRequestId).Select(x => x.AuthServiceId).FirstAsync();
            return result;
        }

        public async Task DeleteAuthRequest(Int32 authRequestId)
        {
            UserAccessRequestDataModel dataModel = await UserAccessRequests.FirstAsync(x => x.Id == authRequestId);
            UserAccessRequests.Remove(dataModel);

            await SaveChangesAsync();
        }

        public async Task<Int32> CreateUser(UserCreateModel createModel)
        {
            UserDataModel dataModel = new UserDataModel(createModel);

            Users.Add(dataModel);
            await SaveChangesAsync();

            return dataModel.ID;
        }

        public async Task<BesaRoles> GetUserRoles(String authServiceId)
        {
            BesaRoles result = await Users.Where(x => x.AuthServiceId == authServiceId).Select(x => x.Role).FirstAsync();
            return result;
        }

        public async Task<Boolean> CheckIfUserIsManagement(String authServiceId)
        {
            Boolean result = await Users.Where(x => x.AuthServiceId == authServiceId).Select(x => x.IsManagement).FirstAsync();
            return result;
        }

        public async Task<Boolean> RemoveAuthIdFromUser(Int32 userID)
        {
            UserDataModel dataModel = await Users.FirstAsync(x => x.ID == userID);
            dataModel.Role = BesaRoles.Unknown;
            dataModel.AuthServiceId = String.Empty;

            Int32 result = await SaveChangesAsync();
            return result > 0;
        }

        public async Task<Boolean> CheckIfHasAuthId(Int32 userID)
        {
            Int32 result = await Users.CountAsync(x => x.ID == userID && String.IsNullOrEmpty(x.AuthServiceId) == false);
            return result > 0;
        }

        public async Task<IEnumerable<UserOverviewModel>> GetAllUsers()
        {
            IEnumerable<UserOverviewModel> result = await Users.Include(x => x.Company)
                .Where(x => String.IsNullOrEmpty(x.AuthServiceId) == false)
                .OrderBy(x => x.Lastname).ThenBy(x => x.Surname)
                .Select(x => GetUserOverviewModel(x))
                .ToListAsync();

            return result;
        }

        public async Task<String> GetAuthServiceIdByUserId(Int32 userId)
        {
            String result = await Users.Where(x => x.ID == userId).Select(x => x.AuthServiceId).FirstAsync();
            return result;
        }

        public async Task<BesaRoles> GetUserRoleById(Int32 userId)
        {
            BesaRoles result = await Users.Where(x => x.ID == userId).Select(x => x.Role).FirstAsync();
            return result;
        }

        public async Task<UserOverviewModel> GetUserOverviewById(Int32 userId)
        {
            UserOverviewModel result = await Users.Include(x => x.Company).Where(x => x.ID == userId).Select(x => GetUserOverviewModel(x)).FirstAsync();
            return result;
        }

        public async Task EditUser(UserEditModel model)
        {
            UserDataModel user = await Users.FirstAsync(x => x.ID == model.Id);
            user.Update(model);

            await SaveChangesAsync();
        }

        public async Task<UserEditModel> GetUserDataForEdit(Int32 userId)
        {
            UserEditModel result = await (from user in Users
                                          where user.ID == userId
                                          select new UserEditModel
                                          {
                                              Id = user.ID,
                                              CompanyId = user.CompanyId,
                                              EMailAddress = user.Email,
                                              Lastname = user.Lastname,
                                              Phone = user.Phone,
                                              Role = user.Role,
                                              Surname = user.Surname,
                                              IsManagement = user.IsManagement,
                                          }).FirstAsync();

            return result;
        }

        private async Task<Int32> GetUserIdByAuthServiceId(String authId)
        {
            Int32 result = await Users.Where(x => x.AuthServiceId == authId).Select(x => x.ID).FirstAsync();
            return result;
        }

        public async Task<UserInfoModel> GetUserInfoModel(String authServiceId)
        {
            UserInfoModel result = await Users.Where(x => x.AuthServiceId == authServiceId).Select(x => new UserInfoModel
            {
                IsManagement = x.IsManagement,
                Roles = x.Role,
                Surname = x.Surname,
                Lastname = x.Lastname,
                Company = x.CompanyId.HasValue == true ? GetSimpleCompanyOverview(x.Company) : null
            }).FirstAsync();

            return result;
        }

        public async Task<Int32> GetUserIdByAuthId(String authId)
        {
            Int32 id = await Users.Where(x => x.AuthServiceId == authId).Select(x => x.ID).FirstAsync();
            return id;
        }

        public async Task<IDictionary<Int32, BesaRoles>> GetUserRoleRelations()
        {
            IDictionary<Int32, BesaRoles> result = await Users.ToDictionaryAsync(x => x.ID, x => x.Role);
            return result;
        }

        #region Overview-Helper


        private UserOverviewModel GetUserOverviewModel(UserDataModel dataModel)
        {
            return dataModel != null ? new UserOverviewModel
            {
                Id = dataModel.ID,
                EMailAddress = dataModel.Email,
                Lastname = dataModel.Lastname,
                Surname = dataModel.Surname,
                Phone = dataModel.Phone,
                CompanyInfo = dataModel.Company == null ? null : this.GetSimpleCompanyOverview(dataModel.Company),
                IsMember = !String.IsNullOrEmpty(dataModel.AuthServiceId)
            } : null;
        }

        #endregion


    }
}
