﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Article")]
    public class ArticleDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean ProductSoldByMeter { get; set; }

        public virtual ICollection<ActivationJobDataModel> UsedByActivations { get; set; }

        #endregion

        #region Constructor

        public ArticleDataModel()
        {
            UsedByActivations = new List<ActivationJobDataModel>();
        }

        public ArticleDataModel(ArticleCreateModel model) : this()
        {
            Update(model);
        }

        #endregion

        #region Methods

        public void Update(IArticle article)
        {
            Name = article.Name;
            ProductSoldByMeter = article.ProductSoldByMeter;
        }

        #endregion

    }

}
