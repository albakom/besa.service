﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public abstract class BranchableDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        public String Name { get; set; }
        public String NormalizedName { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        [ForeignKey("ConstructionStage")]
        public Int32? ConstructionStageId { get; set; }
        public virtual ConstructionStageDataModel ConstructionStage { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxLengthOfProjectMembersInternal)]
        public String ProjectId { get; set; }

        public virtual ICollection<BuildingDataModel> Buildings { get; set; }

        public virtual PrepareBranchableJobDataModel PrepareJob { get; set; }

        [InverseProperty("StartingAtBranchable")]
        public virtual ICollection<FiberCableDataModel> StartingCables { get; set; }

        [InverseProperty("EndingAtBranchable")]
        public virtual ICollection<FiberCableDataModel> EndingCables { get; set; }

        [ForeignKey("PoP")]
        public Int32? PoPId { get; set; }
        public virtual PoPDataModel PoP { get; set; }

        public FiberCableDataModel MainCable { get;  set; }

        public ICollection<FiberSpliceDataModel> Splices { get; set; }

        #endregion

        #region Constructor

        public BranchableDataModel()
        {
            Buildings = new List<BuildingDataModel>();
            StartingCables = new List<FiberCableDataModel>();
            Splices = new List<FiberSpliceDataModel>();
        }

        public void Update(ProjectAdapterBranchableModel adapterModel)
        {
            Name = adapterModel.Name;
            NormalizedName = BesaDataStorage.NormalizeName(adapterModel.Name);
            Longitude = adapterModel.Coordinate.Longitude;
            Latitude = adapterModel.Coordinate.Latitude;
        }

        #endregion
    }
}
