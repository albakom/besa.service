﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Buldings")]
    public class BuildingDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [Required]
        public String StreetName { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }

        public Int32 HouseholdUnits { get; set; }
        public Int32 CommercialUnits { get; set; }

        [Required]
        public String NormalizedStreetName { get;  set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxLengthOfProjectMembersInternal)]
        public String ProjectId { get; set; }

        [ForeignKey("Branchable")]
        public Int32 BranchableId { get; set; }
        public BranchableDataModel Branchable { get; set; }

        public String HouseConnenctionColorName { get; set; }
        public String HouseConnenctionHexValue { get; set; }
        public String MicroDuctColorName { get; set; }
        public String MicroDuctHexValue { get; set; }

        public virtual BranchOffJobDataModel BranchOffJob { get; set; }
        public virtual ConnectBuildingJobDataModel ConnectionJob { get; set; }
        public virtual InjectJobDataModel InjectJob { get; set; }

        public virtual ICollection<FlatDataModel> Flats { get; set; }

        [ForeignKey("FiberCableType")]
        public Int32? CableTypeId { get; set; }
        public virtual FiberCableTypeDataModel FiberCableType { get; set; }

        public Double ConnenctionLength { get;  set; }

        public virtual FiberCableDataModel FiberCable { get; set; }

        public virtual ICollection<BuildingConnectionProcedureDataModel> ConnectionProcedures { get; set; }

        #endregion

        #region Constructor

        public BuildingDataModel()
        {
            Flats = new List<FlatDataModel>();
        }

        public BuildingDataModel(ProjectAdapterBuildingModel model, BranchableDataModel branchable): this()
        {
            Update(model);
            Branchable = branchable;
        }

        #endregion

        #region Methods

        public void UpdateCableInfo(ProjectAdapterUpdateBuldingCableModel updateData, FiberCableTypeDataModel type)
        {
            FiberCableType = type;
            ConnenctionLength = updateData.Length;
        }

        public void UpdateName(ProjectAdapterUpdateBuildingNameModel updateData)
        {
            StreetName = updateData.Name;
            NormalizedStreetName = BesaDataStorage.NormalizeName(updateData.Name);
        }

        public void Update(ProjectAdapterBuildingModel model)
        {
            StreetName = model.Street;
            Longitude = model.Coordinate.Longitude;
            Latitude = model.Coordinate.Latitude;
            HouseholdUnits = model.HouseholdUnits;
            CommercialUnits = model.CommercialUnits;
            NormalizedStreetName = BesaDataStorage.NormalizeName(model.Street);
            ProjectId = model.AdapterId;

            if (model.MicroductColor != null)
            {
                MicroDuctColorName = model.MicroductColor.Name;
                MicroDuctHexValue = model.MicroductColor.RGBHexValue;
            }

            if (model.HouseConnectionColor != null)
            {
                HouseConnenctionColorName = model.HouseConnectionColor.Name;
                HouseConnenctionHexValue = model.HouseConnectionColor.RGBHexValue;
            }
        }

        #endregion
    }
}
