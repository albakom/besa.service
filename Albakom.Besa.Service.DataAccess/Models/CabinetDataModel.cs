﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("StreetCabintes")]
    public class CabinetDataModel : BranchableDataModel
    {
        #region Properties

        #endregion

        #region Constructor

        public CabinetDataModel()
        {

        }

        public CabinetDataModel(ProjectAdapterCabinetModel model) : this()
        {
            Name = model.Name;
            NormalizedName = BesaDataStorage.NormalizeName(model.Name);
            Latitude = model.Coordinate.Latitude;
            Longitude = model.Coordinate.Longitude;
            ProjectId = model.AdapterId;

            Buildings = model.Buildings.Select(x => new BuildingDataModel(x, this)).ToList();
        }

        #endregion
    }
}
