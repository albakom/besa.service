﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Companies")]
    public class CompanyDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxCompanyNameCharsInternal)]
        public String Name { get; set; }

        [StringLength(BesaDataStorageContraints.MaxCompanyPhoneCharsInternal)]
        public String Phone { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressStreetCharsInternal)]
        public String Street { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressStreetNumberCharsInternal)]
        public String StreetNumber { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressCityCharsInternal)]
        public String City { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressZipCodeCharsInternal,
            MinimumLength = BesaDataStorageContraints.MinAddressZipCodeCharsInternal)]
        public String PostalCode { get; set; }

        public virtual ICollection<UserDataModel> Employes { get; set; }
        public virtual ICollection<CollectionJobDataModel> CollectionJobs { get; set; }

        #endregion

        #region Constructor

        public CompanyDataModel()
        {
            Employes = new List<UserDataModel>();
        }

        public CompanyDataModel(CompanyCreateModel model) : this()
        {
            this.Update(model);
        }

        #endregion

        #region Methods

        public void Update(ICompanyModel model)
        {
            Name = model.Name;
            Phone = model.Phone;
            Street = model.Address.Street;
            StreetNumber = model.Address.StreetNumber;
            City = model.Address.City;
            PostalCode = model.Address.PostalCode;
        }

        #endregion
    }
}
