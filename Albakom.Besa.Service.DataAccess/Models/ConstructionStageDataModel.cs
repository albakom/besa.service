﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ConstructionStages")]
    public class ConstructionStageDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        [Required]
        [StringLength(BesaDataStorageContraints.MaxLengthOfConstructionStageNameInternal)]
        public String Name { get; set; }

        public virtual ICollection<BranchableDataModel> Branchables { get; set; }

        #endregion

        #region Constructor

        public ConstructionStageDataModel()
        {
            Branchables = new List<BranchableDataModel>();
        }

        public ConstructionStageDataModel(ConstructionStageCreateModel model) : this()
        {
            Name = model.Name;
        }

        #endregion
    }
}
