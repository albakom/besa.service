﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ContactInfos")]
    public class ContactInfoDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public PersonTypes Type { get; set; }

        [StringLength(BesaDataStorageContraints.MaxUserSurnameInternal)]
        public String Surname { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserLastnameInternal)]
        public String Lastname { get; set; }

        [StringLength(BesaDataStorageContraints.MaxCompanyNameCharsInternal)]
        public String CompanyName { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressStreetCharsInternal)]
        public String Street { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressStreetNumberCharsInternal)]
        public String StreetNumber { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressCityCharsInternal)]
        public String City { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxAddressZipCodeCharsInternal, MinimumLength = BesaDataStorageContraints.MinAddressZipCodeCharsInternal)]
        public String PostalCode { get; set; }

        [EmailAddress]
        [StringLength(BesaDataStorageContraints.MaxUserMailCharsInternal)]
        public String EmailAddress { get; set; }

        [StringLength(BesaDataStorageContraints.MaxCompanyPhoneCharsInternal)]
        public String Phone { get; set; }

        #endregion

        #region Constructor

        public ContactInfoDataModel()
        {

        }

        public ContactInfoDataModel(PersonInfo model) : this()
        {
            Update(model);
        }

        #endregion

        #region Methods

        public void Update(PersonInfo model)
        {
            Type = model.Type;
            Surname = model.Surname;
            Lastname = model.Lastname;
            CompanyName = model.CompanyName;
            Street = model.Address.Street;
            StreetNumber = model.Address.StreetNumber;
            City = model.Address.City;
            PostalCode = model.Address.PostalCode;

            EmailAddress = model.EmailAddress;
            Phone = model.Phone;
        }

        #endregion
    }
}
