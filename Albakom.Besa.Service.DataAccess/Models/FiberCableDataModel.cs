﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FiberCable")]
    public class FiberCableDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public String ProjectAdapterId { get; set; }

        public String Name { get; set; }

        [ForeignKey("CableType")]
        public Int32 CableTypeId { get; set; }
        public FiberCableTypeDataModel CableType { get; set; }

        public String DuctColorName { get; set; }
        public String DuctHexValue { get; set; }

        public Double Length { get; set; }

        [ForeignKey("MainCableForBranchable")]
        public Int32? MainCableForBranchableId { get; set; }
        public virtual BranchableDataModel MainCableForBranchable { get; set; }

        [ForeignKey("StartingAtBranchable")]
        public Int32? StartingAtBranchableleId { get; set; }
        public virtual BranchableDataModel StartingAtBranchable { get; set; }

        [ForeignKey("EndingAtBranchable")]
        public Int32? EndingAtBranchableId { get; set; }
        public virtual BranchableDataModel EndingAtBranchable { get; set; }

        [ForeignKey("ForBuilding")]
        public Int32? BuildingId { get; set; }
        public virtual BuildingDataModel ForBuilding { get; set; }

        [ForeignKey("StartingAtPop")]
        public Int32? StartingAdPopId { get; internal set; }
        public virtual PoPDataModel StartingAtPop { get; set; }

        [ForeignKey("StartingFlat")]
        public Int32? FlatId { get; internal set; }
        public virtual FlatDataModel StartingFlat { get; set; }

        public virtual SpliceODFJobDataModel SpliceODFJob { get; set; }

        #endregion

        #region Constructor

        public FiberCableDataModel()
        {

        }

        public FiberCableDataModel(ProjectAdapterCableModel cableItem) : this()
        {
            ProjectAdapterId = cableItem.ProjectAdapterId;
            Update(cableItem);
        }

        #endregion

        #region Methods

        public void Update(ProjectAdapterCableModel cableItem)
        {
            Name = cableItem.Name;
            Length = cableItem.Lenght;
            if(cableItem.DuctColor != null)
            {
                DuctColorName = cableItem.DuctColor.Name;
                DuctHexValue = cableItem.DuctColor.RGBHexValue;
            }

        }

        #endregion
    }
}
