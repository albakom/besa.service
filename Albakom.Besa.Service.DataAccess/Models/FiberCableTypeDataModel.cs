﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FiberCableTypes")]
    public class FiberCableTypeDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        public String AdapterId { get; set; }
        public String Name { get; set; }
        public Int32 FiberAmount { get; set; }
        public Int32 FiberPerBundle { get; set; }

        public virtual ICollection<BuildingDataModel> ConnenctedBuildings { get; set; }
        public virtual ICollection<FiberCableDataModel> Cables { get; set; }

        #endregion

        #region Constructor

        public FiberCableTypeDataModel()
        {
            ConnenctedBuildings = new List<BuildingDataModel>();
        }

        public FiberCableTypeDataModel(ProjectAdapterCableTypeModel model) : this()
        {
            AdapterId = model.AdapterId;
            Update(model);
        }

        #endregion

        #region Methods

        public void Update(ProjectAdapterCableTypeModel model)
        {
            Name = model.Name;
            FiberAmount = model.FiberAmount;
            FiberPerBundle = model.FiberPerBundle;
        }

        #endregion
    }
}
