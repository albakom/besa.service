﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FiberConnenctions")]
    public class FiberConnenctionDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Flat")]
        public Int32 FlatId { get; set; }
        public virtual FlatDataModel Flat { get; set; }

        public String ModuleName { get; set; }
        public String RowNumber { get; set; }
        public String ColumnNumber { get; set; }

        public String Caption { get; set; }

        #endregion

        #region Constructor

        public FiberConnenctionDataModel()
        {

        }

        public FiberConnenctionDataModel(ProjectAdapterPatchModel adapterModel, Int32 flatId) : this()
        {
            Caption = adapterModel.Caption;
            ModuleName = adapterModel.ModuleName;
            RowNumber = adapterModel.RowNumber;
            ColumnNumber = adapterModel.ColumnNumber;
            FlatId = flatId;
        }

        #endregion
    }
}
