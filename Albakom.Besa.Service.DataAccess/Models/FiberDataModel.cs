﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Fibers")]
    public class FiberDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        public String ProjectAdapterId { get; set; }

        [ForeignKey("Cable")]
        public Int32 CableId { get; set; }
        public virtual FiberCableDataModel Cable { get; set; }

        public String Name { get; set; }
        public Int32 BundleNumber { get; set; }
        public Int32 NumberinBundle { get; set; }
        public String Color { get; set; }
        public Int32 FiberNumber { get; set; }

        [InverseProperty("FirstFiber")]
        public virtual ICollection<FiberSpliceDataModel> RelatedSplicesAsFirstFiber { get; set; }
        [InverseProperty("SecondFiber")]
        public virtual ICollection<FiberSpliceDataModel> RelatedSplicesAsLastFiber { get; set; }

        #endregion

        #region Constructor

        public FiberDataModel()
        {

        }

        public FiberDataModel(ProjectAdapterFiberModel fiberProjectModel, Int32 cableId) : this()
        {
            CableId = cableId;
            ProjectAdapterId = fiberProjectModel.ProjectAdapterId;
            Update(fiberProjectModel);
        }

        #endregion

        #region Methods

        public void Update(ProjectAdapterFiberModel fiberProjectModel)
        {
            Name = fiberProjectModel.Name;
            NumberinBundle = fiberProjectModel.NumberinBundle;
            FiberNumber = fiberProjectModel.FiberNumber;
            Color = fiberProjectModel.Color;
            BundleNumber = fiberProjectModel.BundleNumber;
        }

        #endregion
    }
}
