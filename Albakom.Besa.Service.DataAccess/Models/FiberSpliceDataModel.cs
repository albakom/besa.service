﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FiberSplices")]
    public class FiberSpliceDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public String ProjectAdapterId { get; set; }

        public Int32 TrayNumber { get; set; }
        public Int32 NumberInTray { get; set; }
        public SpliceTypes Type { get; set; }

        [ForeignKey("FirstFiber")]
        public Int32? FirstFiberId { get; set; }
        public virtual FiberDataModel FirstFiber { get; set; }

        [ForeignKey("SecondFiber")]
        public Int32? SecondFiberId { get; set; }
        public virtual FiberDataModel SecondFiber { get; set; }
        public Int32 SecondFiberNumber { get; set; }

        [ForeignKey("Branchable")]
        public Int32 BranchableId { get; set; }

        public virtual BranchableDataModel Branchable { get; set; }

        #endregion

        #region Constructor

        public FiberSpliceDataModel()
        {

        }

        public FiberSpliceDataModel(FiberSpliceCreateModel model) : this(model.SpliceModel, model.FirstFiberId, model.SecondFiberId, model.BranchableId)
        {

        }

        public FiberSpliceDataModel(ProjectAdapterSpliceModel model, Int32? firstFiberId, Int32? secondFiberId, Int32 branchableId) : this()
        {
            ProjectAdapterId = model.ProjectAdapterId;
            Update(model, branchableId);
            FirstFiberId = firstFiberId;
            SecondFiberId = secondFiberId;
            BranchableId = branchableId;
        }

        #endregion

        #region Methods

        public void Update(ProjectAdapterSpliceModel spliceItem, Int32 branchableId)
        {
            TrayNumber = spliceItem.TrayNumber;
            NumberInTray = spliceItem.NumberInTray;
            Type = spliceItem.Type;
            BranchableId = branchableId;
        }

        #endregion
    }
}
