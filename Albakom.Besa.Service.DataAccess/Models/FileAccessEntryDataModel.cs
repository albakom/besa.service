﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FileAccessEntries")]
    public class FileAccessEntryDataModel 
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        public Int32 ObjectId { get; set; }
        public FileAccessObjects ObjectType { get; set; }

        [ForeignKey("FileDataModel")]
        public Int32 FileId { get; set; }
        public virtual FileDataModel File { get; set; }

        #endregion

        #region Constructor

        public FileAccessEntryDataModel()
        {

        }

        #endregion
    }
}
