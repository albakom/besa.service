﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Files")]
    public class FileDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxFileNameCharsInternal)]
        public String Name { get; set; }

        [Required]
        public String Extention { get; set; }

        [Required]
        public String MimeType { get; set; }
        public Int32 Size { get; set; }
        public DateTimeOffset CreatedAt { get; set; }

        [Required]
        public String StorageUrl { get; set; }
        public FileTypes Type { get; set; }

        [ForeignKey("RelatedJob")]
        public Int32? JobId { get; set; }
        public virtual FinishedJobDataModel RelatedJob { get; set; }

        public virtual ICollection<FileAccessEntryDataModel> AccessList { get; set; }

        #endregion

        #region Constructor

        public FileDataModel()
        {
            AccessList = new List<FileAccessEntryDataModel>();
        }

        public FileDataModel(FileCreateModel model) : this()
        {
            Name = model.Name;
            Extention = model.Extention;
            MimeType = model.MimeType;
            Size = model.Size;
            CreatedAt = model.CreatedAt;
            StorageUrl = model.StorageUrl;
            Type = model.Type;
        }

        #endregion
    }
}
