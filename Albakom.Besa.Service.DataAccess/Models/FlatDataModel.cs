﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Flats")]
    public class FlatDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxFlatNumberCharsInternal)]
        public String Number { get; set; }

        [StringLength(BesaDataStorageContraints.MaxFlatDescriptionCharsInternal)]
        public String Description { get; set; }

        [StringLength(BesaDataStorageContraints.MaxFlatFloorCharsInternal)]
        public String Floor { get; set; }

        public String ProjectAdapterCableId { get; set; }

        [ForeignKey("Building")]
        public Int32 BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        public virtual SpliceBranchableJobDataModel BranchableSpliceJob { get; set; }
        public virtual ActivationJobDataModel ActivationJob { get; set; }

        public virtual ICollection<CustomerConnectionProcedureDataModel> ConnectionProcedures { get; set; }

        public virtual FiberCableDataModel Cable { get; set; }

        public virtual ICollection<FiberConnenctionDataModel> Connections { get; set; }

        #endregion

        #region Constructor

        public FlatDataModel()
        {
            Connections = new List<FiberConnenctionDataModel>();
        }

        public FlatDataModel(FlatCreateModel model): this()
        {
            BuildingId = model.BuildingId;
            Number = model.Number;
            Description = model.Description;
            Floor = model.Floor;
        }

        #endregion
    }
}
