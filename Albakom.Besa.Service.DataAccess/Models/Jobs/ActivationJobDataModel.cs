﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class ActivationJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("Flat")]
        public Int32 FlatId { get; set; }
        public virtual FlatDataModel Flat { get; set; }

        [ForeignKey("Customer")]
        public Int32 CustomerId { get; set; }
        public virtual ContactInfoDataModel Customer { get; set; }

        [ForeignKey("Device")]
        public Int32? DeviceId { get; set; }
        public virtual ArticleDataModel Device { get; set; }

        public Boolean UnchagedIpAdress { get; set; }
        public ActivationJobIpAddressRequirements AddressRequirement { get; set; }

        #endregion

        #region Constructor

        public ActivationJobDataModel()
        {

        }

        public ActivationJobDataModel(ActivationJobCreateModel model) : this()
        {
            FlatId = model.FlatId;
            CustomerId = model.CustomerContactId;
            DeviceId = model.DeviceId;
            AddressRequirement = model.AddressRequirement;
            UnchagedIpAdress = model.UnchagedIpAdress;
        }

        #endregion
    }
}
