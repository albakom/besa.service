﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class ActivationJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Double? ActualSpeed { get; set; }

        #endregion

        #region Constructor

        public ActivationJobFinishedDataModel()
        {

        }

        public ActivationJobFinishedDataModel(ActivationJobFinishModel model) : base(model)
        {
        }

        #endregion
    }
}
