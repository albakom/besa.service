﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("BranchOffJobs")]
    public class BranchOffJobDataModel : JobDataModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        #endregion

        #region Constructor

        public BranchOffJobDataModel()
        {

        }

        public BranchOffJobDataModel(BranchOffJobCreateModel model): this()
        {
            BuildingId = model.BuildingId;
        }

        #endregion
    }
}
