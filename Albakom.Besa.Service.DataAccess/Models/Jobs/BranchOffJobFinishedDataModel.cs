﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("BranchOffJobFinisheds")]
    public class BranchOffJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Boolean DuctChanged { get; set; }
        public String DuctChangedDescription { get; set; }

        #endregion

        #region Constructor

        public BranchOffJobFinishedDataModel()
        {

        }

        public BranchOffJobFinishedDataModel(FinishBranchOffJobModel model) : base(model)
        {
            DuctChanged = model.DuctChanged.Value;
            DuctChangedDescription = model.DuctChanged.Description;
        }

        #endregion


    }
}
