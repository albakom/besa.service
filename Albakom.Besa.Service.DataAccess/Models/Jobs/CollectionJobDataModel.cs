﻿using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("CollectionJobs")]
    public class CollectionJobDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Company")]
        public Int32 CompanyId { get; set; }
        public virtual CompanyDataModel Company { get; set; }

        public DateTimeOffset FinishedTill { get; set; }

        public virtual ICollection<JobDataModel> Jobs { get; set; }
        [Required]
        [StringLength(BesaDataStorageContraints.MaxLengthCollectionJobNameInternal)]
        public string Name { get;  set; }

        #endregion

        #region Constructor

        public CollectionJobDataModel()
        {
            Jobs = new List<JobDataModel>();
        }

        #endregion
    }
}
