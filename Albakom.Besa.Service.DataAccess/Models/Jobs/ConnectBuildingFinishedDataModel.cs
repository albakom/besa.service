﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class ConnectBuildingFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Boolean DuctChanged { get; set; }
        public String DuctChangedDescription { get; set; }

        #endregion

        #region Constructor

        public ConnectBuildingFinishedDataModel()
        {

        }

        public ConnectBuildingFinishedDataModel(FinishBuildingConnenctionJobModel model) : base(model)
        {
            DuctChanged = model.DuctChanged.Value;
            DuctChangedDescription = model.DuctChanged.Description;
        }

        #endregion
    }
}
