﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ConnectBuildingJobs")]
    public class ConnectBuildingJobDataModel : JobDataModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        [ForeignKey("Customer")]
        public Int32? CustomerContactId { get; set; }
        public virtual ContactInfoDataModel Customer { get; set; }

        [ForeignKey("Owner")]
        public Int32 OwnerContactId { get; set; }
        public virtual ContactInfoDataModel Owner { get; set; }

        [ForeignKey("Workman")]
        public Int32? WorkmanContactId { get; set; }
        public virtual ContactInfoDataModel Workman { get; set; }

        [ForeignKey("Architect")]
        public Int32? ArchitectContactId { get; set; }
        public virtual ContactInfoDataModel Architect { get; set; }

        public Boolean OnlyHouseConnection { get; set; }

        [StringLength(BesaDataStorageContraints.MaxHouseConnenctionDescriptionCharsInternal)]
        public String Description { get; set; }

        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectionOfOtherMediaRequired { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }

        #endregion

        #region Constructor

        public ConnectBuildingJobDataModel()
        {

        }

        public ConnectBuildingJobDataModel(HouseConnenctionJobCreateModel model)
        {
            BuildingId = model.BuildingId;
            CustomerContactId = model.CustomerContactId;
            OwnerContactId = model.OwnerContactId;
            WorkmanContactId = model.WorkmanContactId;
            ArchitectContactId = model.ArchitectContactId;
            OnlyHouseConnection = model.OnlyHouseConnection;
            Description = model.Description;
            CivilWorkIsDoneByCustomer = model.CivilWorkIsDoneByCustomer;
            ConnectionOfOtherMediaRequired = model.ConnectionOfOtherMediaRequired;
            ConnectionAppointment = model.ConnectionAppointment;
        }

        #endregion
    }
}
