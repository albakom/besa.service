﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("CustomerConnenctionRequests")]
    public class CustomerConnenctionRequestDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Flat")]
        public Int32? FlatId { get; set; }
        public virtual FlatDataModel Flat { get; set; }

        [ForeignKey("Building")]
        public Int32? BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        [ForeignKey("Customer")]
        public Int32? CustomerPersonId { get; set; }
        public virtual ContactInfoDataModel Customer { get; set; }

        [ForeignKey("PropertyOwner")]
        public Int32? PropertyOwnerPersonId { get; set; }
        public ContactInfoDataModel PropertyOwner { get; set; }

        [ForeignKey("Architect")]
        public Int32? ArchitectPersonId { get; set; }
        public ContactInfoDataModel Architect { get; set; }

        [ForeignKey("Workman")]
        public Int32? WorkmanPersonId { get; set; }
        public ContactInfoDataModel Workman { get; set; }

        public String DescriptionForHouseConnection { get; set; }
        public Boolean OnlyHouseConnection { get; set; }

        public Boolean SubscriberEndpointNearConnectionPointValue { get; set; }
        public String SubscriberEndpointNearConnectionPointDescription { get; set; }

        public Boolean ActivePointNearSubscriberEndpointValue { get; set; }
        public String ActivePointNearSubscriberEndpointDescription { get; set; }

        public Boolean PowerForActiveEquipmentValue { get; set; }
        public String PowerForActiveEquipmentDescription { get; set; }

        public Boolean CivilWorkIsDoneByCustomer { get; set; }
        public Boolean ConnectioOfOtherMediaRequired { get; set; }

        public Double? DuctAmount { get; set; }
        public Double? SubscriberEndpointLength { get; set; }

        public DateTimeOffset? ConnectionAppointment { get; set; }
        public Boolean IsClosed { get;  set; }

        public Boolean InformSalesAfterFinish { get; set; }
        public Boolean ActivationAsSoonAsPossible { get; set; }


        #endregion

        #region Constructor

        public CustomerConnenctionRequestDataModel()
        {

        }

        public CustomerConnenctionRequestDataModel(CustomerConnectRequestCreateModel model) : this()
        {
            CustomerPersonId = model.CustomerPersonId;
            PropertyOwnerPersonId = model.PropertyOwnerPersonId;
            ArchitectPersonId = model.ArchitectPersonId;
            WorkmanPersonId = model.WorkmanPersonId;

            FlatId = model.FlatId;
            BuildingId = model.BuildingId;

            OnlyHouseConnection = model.OnlyHouseConnection;
            DescriptionForHouseConnection = model.DescriptionForHouseConnection;

            SubscriberEndpointNearConnectionPointValue = model.SubscriberEndpointNearConnectionPoint.Value;
            SubscriberEndpointNearConnectionPointDescription = model.SubscriberEndpointNearConnectionPoint.Description;

            ActivePointNearSubscriberEndpointValue = model.ActivePointNearSubscriberEndpoint.Value;
            ActivePointNearSubscriberEndpointDescription = model.ActivePointNearSubscriberEndpoint.Description;

            PowerForActiveEquipmentValue = model.PowerForActiveEquipment.Value;
            PowerForActiveEquipmentDescription = model.PowerForActiveEquipment.Description;

            CivilWorkIsDoneByCustomer = model.CivilWorkIsDoneByCustomer;
            ConnectioOfOtherMediaRequired = model.ConnectioOfOtherMediaRequired;

            DuctAmount = model.DuctAmount;
            SubscriberEndpointLength = model.SubscriberEndpointLength;

            ConnectionAppointment = model.ConnectionAppointment;

            InformSalesAfterFinish = model.InformSalesAfterFinish;
            ActivationAsSoonAsPossible = model.ActivationAsSoonAsPossible;
        }

        #endregion
    }
}
