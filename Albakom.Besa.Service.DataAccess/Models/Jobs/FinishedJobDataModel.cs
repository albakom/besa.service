﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("FinishedJobs")]
    public abstract class FinishedJobDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Job")]
        public Int32 JobId { get; set; }
        public virtual JobDataModel Job { get; set; }

        [ForeignKey("DoneBy")]
        public Int32 JobberId { get; set; }
        public virtual UserDataModel DoneBy { get; set; }

        [StringLength(BesaDataStorageContraints.MaxJobFinishedCharsInternal)]
        public String Comment { get; set; }

        public DateTimeOffset? AcceptedAt { get; set; }

        [ForeignKey("Accepter")]
        public Int32? AccepterId { get; set; }
        public virtual UserDataModel Accepter { get; set; }

        public DateTimeOffset FinishedAt { get; set; }

        public Boolean ProblemHappend { get; set; }
        public String ProblemHappendDescription { get; set; }

        public virtual ICollection<FileDataModel> Files { get; set; }
        public Boolean WasAdministrativeFinished { get; internal set; }

        #endregion

        #region Constructor

        public FinishedJobDataModel()
        {
            Files = new List<FileDataModel>();
        }
         
        public FinishedJobDataModel(FinishedJobModel model) : this()
        {
            JobId = model.JobId;
            Comment = model.Comment;
            FinishedAt = model.Timestamp;
            ProblemHappend = model.ProblemHappend.Value;
            ProblemHappendDescription = model.ProblemHappend.Description;
        }

        #endregion
    }
}
