﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class InjectJobDataModel : JobDataModel
    {
        #region Properties

        public Int32 BuildingId { get; set; }
        public BuildingDataModel Building { get; set; }

        [ForeignKey("CustomerContact")]
        public Int32 CustomerContactId { get; set; }
        public ContactInfoDataModel CustomerContact { get; set; }

        #endregion

        #region Constructor

        public InjectJobDataModel() : base()
        {

        }

        public InjectJobDataModel(InjectJobCreateModel model) : this()
        {
            BuildingId = model.BuildingId;
            CustomerContactId = model.CustomerContactId;
        }

        #endregion

    }
}
