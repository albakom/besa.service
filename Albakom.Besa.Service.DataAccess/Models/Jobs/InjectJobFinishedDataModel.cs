﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class InjectJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Double ActualLength { get; set; }

        #endregion

        #region Constructor

        public InjectJobFinishedDataModel()
        {

        }

        public InjectJobFinishedDataModel(InjectJobFinishedModel model) : base(model)
        {
            ActualLength = model.Length;
        }

        #endregion
    }
}
