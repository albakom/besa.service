﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class InventoryJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("IssuerContact")]
        public Int32 IssuerContactId { get; set; }
        public virtual ContactInfoDataModel IssuerContact { get; set; }

        public DateTimeOffset PickupAt { get; set; }

        public virtual ICollection<IventoryJobArticleRelationDataModel> Items { get; set; }


        #endregion

        #region Constructor

        public InventoryJobDataModel()
        {
            Items = new List<IventoryJobArticleRelationDataModel>();
        }

        public InventoryJobDataModel(InventoryJobCreateModel model) :  this()
        {
            IssuerContactId = model.IssuanceToId;
            PickupAt = model.PickupAt;

            foreach (KeyValuePair<Int32,Double> item in model.Articles)
            {
                Items.Add(new IventoryJobArticleRelationDataModel(this, item.Key, item.Value));
            }

        }

        #endregion
    }
}
