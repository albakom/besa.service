﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class InventoryJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        #endregion

        #region Constructor

        public InventoryJobFinishedDataModel()
        {

        }

        public InventoryJobFinishedDataModel(InventoryFinishedJobModel model) : base(model)
        {

        }

        #endregion
    }

}
