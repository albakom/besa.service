﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("IventoryJobArticleRelations")]
    public class IventoryJobArticleRelationDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Article")]
        public Int32 ArticleId { get; set; }
        public virtual ArticleDataModel Article { get; set; }

        [ForeignKey("Job")]
        public Int32 JobId { get; set; }
        public virtual InventoryJobDataModel Job { get; set; }

        public Double Amount { get; set; }

        #endregion

        #region Constructor

        public IventoryJobArticleRelationDataModel()
        {

        }

        public IventoryJobArticleRelationDataModel(InventoryJobDataModel job, Int32 articleId, Double amount)
        {
            Job = job;
            ArticleId = articleId;
            Amount = amount;
        }

        #endregion
    }
}
