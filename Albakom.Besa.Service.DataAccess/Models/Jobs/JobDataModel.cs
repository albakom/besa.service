﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public abstract class JobDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Collection")]
        public Int32? CollectionId { get; set; }
        public virtual CollectionJobDataModel Collection { get; set; }

        public virtual FinishedJobDataModel FinishedModel { get; set; }

        #endregion

        #region Constructor


        #endregion

    }
}
