﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class ContactAfterFinishedJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("Building")]
        public Int32? BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        [ForeignKey("Flat")]
        public Int32? FlatId { get; set; }
        public virtual FlatDataModel Flat { get; set; }

        [ForeignKey("CustomerContact")]
        public Int32 CustomerContactId { get; set; }
        public virtual ContactInfoDataModel CustomerContact { get; set; }

        [ForeignKey("RelatedProcedure")]
        public Int32 RelatedProcedureId { get; set; }
        public virtual BuildingConnectionProcedureDataModel RelatedProcedure { get; set; }

        public String Notes { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedJobDataModel()
        {

        }

        public ContactAfterFinishedJobDataModel(CreateContactAfterFinishedJobModel model) : this()
        {
            BuildingId = model.BuildingId;
            FlatId = model.FlatId;
            RelatedProcedureId = model.RelatedProcedureId;
        }

        #endregion
    }
}
