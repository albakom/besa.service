﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class ContactAfterFinishedJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        [ForeignKey("Request")]
        public Int32? CreatedRequestId { get; set; }
        public virtual CustomerConnenctionRequestDataModel Request { get; set; }

        #endregion

        #region Constructor

        public ContactAfterFinishedJobFinishedDataModel()
        {

        }

        public ContactAfterFinishedJobFinishedDataModel(ContactAfterFinishedFinishModel model) : base(model)
        {
            CreatedRequestId = model.RequestId;
        }

        #endregion
    }
}
