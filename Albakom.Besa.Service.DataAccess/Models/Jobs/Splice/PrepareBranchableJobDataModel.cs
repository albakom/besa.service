﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class PrepareBranchableJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("Branchable")]
        public Int32 BranchableId { get; set; }
        public virtual BranchableDataModel Branchable { get; set; }

        #endregion

        #region Constructor

        public PrepareBranchableJobDataModel()
        {

        }

        public PrepareBranchableJobDataModel(PrepareBranchableForSpliceJobCreateModel model) : this()
        {
            BranchableId = model.BranchableId;
        }

        #endregion
    }
}
