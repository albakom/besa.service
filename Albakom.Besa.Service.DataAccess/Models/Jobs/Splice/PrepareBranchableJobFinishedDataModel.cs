﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class PrepareBranchableJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties


        #endregion

        #region Constructor

        public PrepareBranchableJobFinishedDataModel()
        {

        }

        public PrepareBranchableJobFinishedDataModel(PrepareBranchableForSpliceJobFinishModel model) : base(model)
        {

        }

        #endregion
    }
}
