﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class SpliceBranchableJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("Flat")]
        public Int32 FlatId { get; set;}
        public virtual FlatDataModel Flat { get; set; }

        [ForeignKey("CustomerContact")]
        public Int32 CustomerContactId { get; set; }
        public virtual ContactInfoDataModel CustomerContact { get; set; }

        #endregion

        #region Constructor

        public SpliceBranchableJobDataModel()
        {

        }

        public SpliceBranchableJobDataModel(SpliceBranchableJobCreateModel model): this()
        {
            FlatId = model.FlatId;
            CustomerContactId = model.CustomerContactId;
        }

        #endregion
    }
}
