﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class SpliceBranchableJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Double CableMetric { get; set; }

        #endregion

        #region Constructor

        public SpliceBranchableJobFinishedDataModel()
        {

        }

        public SpliceBranchableJobFinishedDataModel(SpliceBranchableJobFinishModel model) : base(model)
        {
            CableMetric = model.CableMetric;
        }

        #endregion
    }
}
