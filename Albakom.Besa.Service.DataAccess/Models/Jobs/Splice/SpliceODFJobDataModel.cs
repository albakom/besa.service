﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class SpliceODFJobDataModel : JobDataModel
    {
        #region Properties

        [ForeignKey("Cable")]
        public Int32 CableId { get; set; }
        public FiberCableDataModel Cable {get; set; }

        #endregion

        #region Constructor

        public SpliceODFJobDataModel()
        {

        }

        public SpliceODFJobDataModel(SpliceODFJobCreateModel model) : this()
        {
            CableId = model.MainCableId;
        }

        #endregion
    }
}
