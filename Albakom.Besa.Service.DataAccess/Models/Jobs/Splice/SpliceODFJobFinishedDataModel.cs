﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class SpliceODFJobFinishedDataModel : FinishedJobDataModel
    {
        #region Properties

        public Double CableMetric { get; set; }

        #endregion

        #region Constructor

        public SpliceODFJobFinishedDataModel()
        {

        }

        public SpliceODFJobFinishedDataModel(SpliceODFJobFinishModel model) : base(model)
        {
            CableMetric = model.CableMetric;
        }

        #endregion
    }
}
