﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Messages")]
    public class MessageDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("ProtocolEntry")]
        public Int32 ProtocolEntryId { get; set; }
        public virtual ProtocolEntryDataModel ProtocolEntry { get; set; }

        [ForeignKey("Sender")]
        public Int32? SenderId { get; set; }
        public virtual UserDataModel Sender { get; set; }

        [ForeignKey("Receiver")]
        public Int32 ReceiverId { get; set; }
        public virtual UserDataModel Receiver { get; set; }

        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedObjectType { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public Boolean MarkedAsRead { get; set; }
        public String Details { get; set; }
        public String Title { get;  set; }

        #endregion

        #region Constructor

        public MessageDataModel()
        {

        }

        public MessageDataModel(MessageRawCreateModel model) : this()
        {
            ProtocolEntryId = model.ProtocolEntryId;
            ReceiverId = model.ReceiverId;
            SenderId = model.SenderId;
            Timestamp = model.Timestamp;
            Action = model.Action;
            RelatedObjectType = model.RelatedObjectType;
            Details = model.Details;
            Title = model.Title;
        }

        #endregion
    }
}
