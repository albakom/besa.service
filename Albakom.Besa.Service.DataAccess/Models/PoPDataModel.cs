﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Albakom.Besa.Service.Contracts.Models;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("PoPs")]
    public class PoPDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        public virtual ICollection<BranchableDataModel> Branchables { get; set; }
        public virtual ICollection<FiberCableDataModel> StartingCables { get; set; }

        #endregion

        #region Constructor

        public PoPDataModel()
        {
            Branchables = new List<BranchableDataModel>();
            StartingCables = new List<FiberCableDataModel>();
        }

        public PoPDataModel(ProjectAdapterPoPModel adapterModel) : this()
        {
            ProjectAdapterId = adapterModel.ProjectAdapterId;

            Update(adapterModel);
        }

        #endregion

        #region Methods

        public void Update(ProjectAdapterPoPModel adapterModel)
        {
            Name = adapterModel.Name;
            Latitude = adapterModel.Location.Latitude;
            Longitude = adapterModel.Location.Longitude;
        }

        #endregion
    }
}
