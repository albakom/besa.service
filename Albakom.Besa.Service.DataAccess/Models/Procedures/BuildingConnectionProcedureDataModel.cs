﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class BuildingConnectionProcedureDataModel : ProcedureDataModel
    {
        #region Properties

        [ForeignKey("Owner")]
        public Int32 OwnerContactId { get; set; }
        public ContactInfoDataModel Owner { get; set; }

        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }

        [ForeignKey("Building")]
        public Int32 BuildingId { get; set; }
        public virtual BuildingDataModel Building { get; set; }

        public Boolean InformSalesAfterFinish { get; set; }


        #endregion

        #region Constructor

        public BuildingConnectionProcedureDataModel()
        {

        }

        public BuildingConnectionProcedureDataModel(BuildingConnectionProcedureCreateModel model) : base(model)
        {
            OwnerContactId = model.OwnerContactId;
            Appointment = model.Appointment;
            DeclarationOfAggrementInStock = model.DeclarationOfAggrementInStock;
            BuildingId = model.BuildingId;
            InformSalesAfterFinish = model.InformSalesAfterFinish;
        }

        #endregion
    }
}
