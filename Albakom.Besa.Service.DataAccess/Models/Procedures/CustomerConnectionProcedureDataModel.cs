﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    public class CustomerConnectionProcedureDataModel : ProcedureDataModel
    {
        #region Properties

        [ForeignKey("Customer")]
        public Int32 CustomerContactId { get; set; }
        public ContactInfoDataModel Customer { get; set; }

        public DateTimeOffset? Appointment { get; set; }

        public Boolean DeclarationOfAggrementInStock { get; set; }
        public Boolean ContractInStock { get; set; }

        public Boolean ActivationAsSoonAsPossible { get; set; }

        [ForeignKey("Flat")]
        public Int32 FlatId { get; set; }
        public virtual FlatDataModel Flat { get; set; }



        #endregion

        #region Constructor

        public CustomerConnectionProcedureDataModel()
        {

        }

        public CustomerConnectionProcedureDataModel(CustomerConnectionProcedureCreateModel model) : base(model)
        {
            CustomerContactId = model.CustomerContactId;
            Appointment = model.Appointment;
            DeclarationOfAggrementInStock = model.DeclarationOfAggrementInStock;
            ContractInStock = model.ContractInStock;
            FlatId = model.FlatId;
            ActivationAsSoonAsPossible = model.AsSoonAsPossible;
         
        }

        #endregion
    }
}
