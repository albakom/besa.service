﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Procedures")]
    public abstract class ProcedureDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public ProcedureStates Start { get; set; }
        public ProcedureStates ExpectedEnd { get; set; }
        public Boolean IsFinished { get; set; }

        public virtual ICollection<ProcedureTimeLineElementDataModel> Timeline { get; set; }

        #endregion

        #region Constructor

        public ProcedureDataModel()
        {
            Timeline = new List<ProcedureTimeLineElementDataModel>();
        }

        public ProcedureDataModel(ProcedureCreateModel model) : this()
        {
            Name = model.Name;
            Start = model.Start;
            ExpectedEnd = model.ExpectedEnd;

            if(model.FirstElement != null)
            {
                ProcedureTimeLineElementDataModel element = new ProcedureTimeLineElementDataModel
                {
                    Comment = model.FirstElement.Comment,
                    Procedure = this,
                    State = model.FirstElement.State,
                    TimeStamp = DateTimeOffset.Now,
                    RelatedRequestId = model.FirstElement.RelatedRequestId,
                    RelatedJobId = model.FirstElement.RelatedJobId,
                };

                Timeline.Add(element);
            }
        }

        #endregion
    }
}
