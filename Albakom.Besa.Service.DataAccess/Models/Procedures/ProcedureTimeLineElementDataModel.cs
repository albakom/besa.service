﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ProcedureTimeLineElements")]
    public class ProcedureTimeLineElementDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }
        public ProcedureStates State { get; set; }
        public String Comment { get; set; }

        public DateTimeOffset TimeStamp { get; set; }

        [ForeignKey("Procedure")]
        public Int32 ProcedureId { get; set; }
        public virtual  ProcedureDataModel Procedure { get; set; }

        [ForeignKey("RelatedJob")]
        public Int32? RelatedJobId { get; set; }
        public virtual JobDataModel RelatedJob { get; set; }

        [ForeignKey("RelatedRequest")]
        public Int32? RelatedRequestId { get; set; }
        public virtual CustomerConnenctionRequestDataModel RelatedRequest { get; set; }

        #endregion

        #region Constructor

        public ProcedureTimeLineElementDataModel()
        {

        }

        public ProcedureTimeLineElementDataModel(ProcedureTimelineCreateModel model) : this()
        {
            State = model.State;
            Comment = model.Comment;
            if(model.Timestamp.HasValue == false)
            {
                TimeStamp = DateTimeOffset.Now;
            }
            else
            {
                TimeStamp = model.Timestamp.Value;
            }
            RelatedJobId = model.RelatedJobId;
            RelatedRequestId = model.RelatedRequestId;
            ProcedureId = model.ProcedureId;
        }

        #endregion
    }
}
