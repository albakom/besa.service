﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ProtocolEntries")]
    public class ProtocolEntryDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        public DateTimeOffset Timestamp { get; set; }
        public MessageActions Action { get; set; }
        public MessageRelatedObjectTypes RelatedType { get; set; }
        public ProtocolLevels Level { get; set; }
        public Int32 RelatedObjectId { get; set; }

        [ForeignKey("TriggeredUser")]
        public Int32? TriggeredByUserId { get; set; }
        public virtual UserDataModel TriggeredUser { get; set; }

        public String CustomMessage { get; set; }

        #endregion

        #region Constructor

        public ProtocolEntryDataModel()
        {

        }

        public ProtocolEntryDataModel(ProtocolEntyCreateModel model) : this()
        {
            Timestamp = model.Timestamp;
            Action = model.Action;
            RelatedType = model.RelatedType;
            RelatedObjectId = model.RelatedObjectId.HasValue == true ? model.RelatedObjectId.Value : -1;
            TriggeredByUserId = model.TriggeredByUserId;
            Level = model.Level;
        }

        #endregion
    }
}
