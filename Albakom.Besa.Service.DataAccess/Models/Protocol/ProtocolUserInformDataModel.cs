﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("ProtcolUserInforms")]
    public class ProtocolUserInformDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("User")]
        public Int32 UserId { get; set; }
        public virtual UserDataModel User { get; set; }

        public ProtocolNotificationTypes NotifationTypes { get; set; }
        public MessageRelatedObjectTypes RelatedObject { get; set; }
        public MessageActions Action { get; set; }

        #endregion

        #region Constructor

        public ProtocolUserInformDataModel()
        {

        }

        public ProtocolUserInformDataModel(Int32 userId, ProtocolUserInformModel item) : this()
        {
            UserId = userId;
            NotifationTypes = item.NotifcationType;
            RelatedObject = item.RelatedType;
            Action = item.Action;
        }

        #endregion
    }
}
