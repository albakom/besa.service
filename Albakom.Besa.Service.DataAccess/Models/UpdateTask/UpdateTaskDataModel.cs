﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("UpdateTasks")]
    public class UpdateTaskDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("StartedBy")]
        public Int32 StartedByUserId { get; set; }
        public virtual UserDataModel StartedBy { get; set; }

        public UpdateTasks Task { get; set; }
        public DateTimeOffset Started{ get; set; }
        public DateTimeOffset? Ended { get; set; }

        public Boolean Canceled { get; set; }

        public virtual ICollection<UpdateTaskElementDataModel> Elements { get; set; }
        public DateTimeOffset Heartbeat { get;  set; }

        public Boolean IsLocked { get; set; }

        #endregion

        #region Constructor

        public UpdateTaskDataModel()
        {
            Elements = new List<UpdateTaskElementDataModel>();
        }

        public UpdateTaskDataModel(UpdateTaskCreateModel model, Int32 userId) : this()
        {
            StartedByUserId = userId;
            Task = model.Task;
            Started = model.Timestamp;
            Heartbeat = model.Timestamp;

            foreach (KeyValuePair<String,Int32> item in model.Elements)
            {
                UpdateTaskElementDataModel elementDataModel = new UpdateTaskElementDataModel
                {
                    Task = this,
                    AdapterId = item.Key,
                    ObjectId = item.Value,
                    State = UpdateTaskElementStates.NotStarted,
                };

                this.Elements.Add(elementDataModel);
            }
        }

        public void Update(UpdateTaskFinishModel finishModel)
        {
            Ended = finishModel.Timestamp;
            Canceled = finishModel.Cancel;
            IsLocked = false;
        }

        #endregion
    }
}
