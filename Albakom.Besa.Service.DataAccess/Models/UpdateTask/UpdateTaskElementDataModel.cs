﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("UpdateTaskElements")]
    public class UpdateTaskElementDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Task")]
        public Int32 TaskId { get; set; }
        public virtual UpdateTaskDataModel Task { get; set; }

        public Int32 ObjectId { get; set; }
        public String AdapterId { get; set; }

        public DateTimeOffset? Started { get; set; }
        public DateTimeOffset? Ended { get; set; }
        public UpdateTaskElementStates State { get; set; }

        public String Result { get; set; }


        #endregion

        #region Constructor

        public UpdateTaskElementDataModel()
        {
        }

        public void Update(UpdateTaskElementUpdateModel updateModel)
        {
            State = updateModel.State;
            Ended = updateModel.Timestamp;
            Result = updateModel.Result;
        }

        #endregion
    }
}
