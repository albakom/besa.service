﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("UserAccessRequests")]
    public class UserAccessRequestDataModel
    {
        #region Properties

        [Key]
        public Int32 Id { get; set; }

        [ForeignKey("Company")]
        public Int32? CompanyId { get; set; }

        public virtual CompanyDataModel Company { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserSurnameInternal)]
        public String Surname { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserLastnameInternal)]
        public String Lastname { get; set; }

        [EmailAddress]
        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserMailCharsInternal)]
        public String Email { get; set; }

        [Phone]
        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserPhoneCharsInternal)]
        public String Phone { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserAuthServiceIdInternal)]
        public String AuthServiceId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        #endregion

        #region Constructor

        public UserAccessRequestDataModel()
        {

        }

        public UserAccessRequestDataModel(UserAccessRequestCreateModel model)
        {
            Surname = model.Surname;
            Lastname = model.Lastname;
            Email = model.EMailAddress;
            Phone = model.Phone;
            AuthServiceId = model.AuthServiceId;
            CompanyId = model.CompanyId;
            CreatedAt = model.GeneratedAt;
        }

        #endregion
    }
}
