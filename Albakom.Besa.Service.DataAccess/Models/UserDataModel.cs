﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.DataAccess.Models
{
    [Table("Users")]
    public class UserDataModel
    {
        #region Properties

        [Key]
        public Int32 ID { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserSurnameInternal)]
        public String Surname { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserLastnameInternal)]
        public String Lastname { get; set; }

        [EmailAddress]
        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserMailCharsInternal)]
        public String Email { get; set; }

        [Phone]
        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserPhoneCharsInternal)]
        public String Phone { get; set; }

        [Required]
        [StringLength(BesaDataStorageContraints.MaxUserAuthServiceIdInternal)]
        public String AuthServiceId { get;  set; }

        public BesaRoles Role { get;  set; }

        public Boolean IsManagement { get; set; }

        [ForeignKey("Company")]
        public Int32? CompanyId { get; set; }
        public virtual CompanyDataModel Company { get; set; }

        [InverseProperty("DoneBy")]
        public virtual ICollection<FinishedJobDataModel> FinishedJobs { get; set; }

        [InverseProperty("Accepter")]
        public virtual ICollection<FinishedJobDataModel> AcceptedJobs { get; set; }

        public virtual ICollection<ProtocolEntryDataModel> TriggeredEntries { get; set; }

        public virtual ICollection<UpdateTaskDataModel> StartedUpdateTasks { get; set; }

        #endregion

        #region Constructor

        public UserDataModel()
        {
            FinishedJobs = new List<FinishedJobDataModel>();
            AcceptedJobs = new List<FinishedJobDataModel>();
            TriggeredEntries = new List<ProtocolEntryDataModel>();
        }

        public UserDataModel(UserCreateModel data) : this()
        {
            Update(data);
            Role = data.Role;
            AuthServiceId = data.AuthServiceId;
        }

        #endregion

        #region Methods

        private void Update(IUserData data)
        {
            Surname = data.Surname;
            Lastname = data.Lastname;
            Email = data.EMailAddress;
            Phone = data.Phone;
            CompanyId = data.CompanyId;
        }

        public void Update(UserEditModel user)
        {
            Update(user as IUserData);
            Role = user.Role;
            IsManagement = user.IsManagement;
        }

        #endregion
    }
}
