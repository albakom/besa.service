﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Startup.BesaAdminRoleAccessPolicyName)]
    public class ArticleController : Controller
    {
        #region Fields

        private readonly IArticleManagementService service;
        private readonly IClaimsExtractor extractor;

        #endregion

        #region Constructor

        public ArticleController(
            IArticleManagementService service,
            IClaimsExtractor extractor
            )

        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> Overview()
        {
            IEnumerable<ArticleOverviewModel> result = await service.GetAllArticles();
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Details([FromRoute(Name = "id")]Int32 articleID)
        {
            ArticleDetailModel result = await service.GetDetails(articleID);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit([FromBody]ArticleEditModel model)
        {
            String userAuthId = extractor.ExtractTokenValue(base.HttpContext);

            ArticleOverviewModel result = await service.EditArticle(model, userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ArticleCreateModel model)
        {
            String userAuthId = extractor.ExtractTokenValue(base.HttpContext);

            ArticleOverviewModel result = await service.CreateArticle(model, userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> IsInUse([FromRoute(Name = "id")]Int32 articleID)
        {
            Boolean result = await service.CheckIfArticleIsInUse(articleID);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Remove([FromRoute(Name = "id")]Int32 articleID, [FromQuery]Boolean forceDeleted = false)
        {
            String userAuthId = extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await service.DeleteArticle(articleID, forceDeleted, userAuthId);
            return base.Ok(result);
        }
    }
}