﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    public class BuildingController : Controller
    {
        [HttpPost]
        public async Task<IActionResult> CreateBuildingArea([FromBody]BuildingAreaCreateModel coordinates)
        {
            Random rnd = new Random();
            Int32 result = rnd.Next(1000000);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetBuildingAreaOverviews()
        {
            List<BuildingAreaOverviewModel> result = new List<BuildingAreaOverviewModel>()
            {
                new BuildingAreaOverviewModel()
                {
                    Id = 1,
                    Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.09186474737769,
                            Longitude = 12.129444826886811
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.085471250771754,
                            Longitude = 12.125754107282319
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.08849192312048,
                            Longitude = 12.103781451032319
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.0949352892233,
                            Longitude = 12.109703768537202
                        }
                    },
                    AreaName = "KTV"
                },
                new BuildingAreaOverviewModel()
                {
                    Id = 2,
                    Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.104799754317206,
                            Longitude = 12.066273440168061
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09820692797026,
                            Longitude = 12.090992678449311
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09337487832541,
                            Longitude = 12.095885027692475
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09075728307953,
                            Longitude = 12.088074435041108
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.092972182114174,
                            Longitude = 12.066101778791108
                        }

                    },
                    AreaName = "Reutershagen"
                },
                new BuildingAreaOverviewModel()
                {
                    Id = 3,
                    Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.10306177600314 ,
                            Longitude = 12.073826540753998
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09883425822184,
                            Longitude = 12.092537630841889
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.10230689371798,
                            Longitude = 12.096571673200287
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.110358263031905,
                            Longitude = 12.091679323957123
                        }
                    },
                    AreaName = "Bramow"
                }
            };
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetBuildingAreaDetails([FromRoute(Name = "id")] Int32 buildingAreaId)
        {
            Random rnd = new Random();
            BuildingAreaDetailModel result = new BuildingAreaDetailModel()
            {
                Id = buildingAreaId,
                Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = rnd.Next(999999),
                            Longitude = rnd.Next(999999)
                        },
                        new GPSCoordinate()
                        {
                            Latitude = rnd.Next(999999),
                            Longitude = rnd.Next(999999)
                        }
                    },
                AreaName = "Bramow",
                Buildings = new List<BuildingOverviewModel>
                {
                    new BuildingOverviewModel()
                    {
                        Id = 1,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.09171146428057,
                            Longitude = 12.134075367207288
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091536851199216,
                            Longitude = 12.134130352492093
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091557301417936,
                            Longitude = 12.13432615375018
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09172719515271,
                            Longitude = 12.13426848625636
                        }
                    }
                },
                    new BuildingOverviewModel() {
                        Id = 2,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.091741352932544,
                            Longitude = 12.134302013869046
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091614719285765,
                            Longitude = 12.134340905899762
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09163516946605,
                            Longitude = 12.13452597832179
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09175865687912,
                            Longitude = 12.134488427395581
                        }
                    }
                   },
                    new BuildingOverviewModel() {
                        Id = 3,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.091768095392375,
                            Longitude = 12.134539389366864
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091638315646755,
                            Longitude = 12.13457291697955
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091654046546644,
                            Longitude = 12.134749942774533
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09178382624307,
                            Longitude = 12.134717756266355
                        }
                    }
                   },
                    new BuildingOverviewModel() {
                        Id = 4,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.09177989353094,
                            Longitude = 12.134866618866681
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09165168691203,
                            Longitude = 12.134906852001905
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09167685634087,
                            Longitude = 12.13513483976817
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091633596375644,
                            Longitude = 12.135154956335782
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09165168691203,
                            Longitude = 12.135309183354138
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09182472642689,
                            Longitude = 12.135251515860318
                        }
                    }
                   }
            },
                Streets = new List<StreetOverviewModel>()
                {
                    new StreetOverviewModel()
                    {
                        Id = 1,
                        StreetName = "Drosselweg"
                    },
                    new StreetOverviewModel()
                    {
                        Id = 2,
                        StreetName = "Fasanenweg"
                    },
                    new StreetOverviewModel()
                    {
                        Id = 3,
                        StreetName = "Tessiner Str"
                    }
                },
                EmptySpaces = new List<EmptySpaceOverviewModel>()
                {
                    new EmptySpaceOverviewModel()
                    {
                        Id = 1,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = rnd.Next(999999),
                            Longitude = rnd.Next(999999)
                        },
                        new GPSCoordinate()
                        {
                            Latitude = rnd.Next(999999),
                            Longitude = rnd.Next(999999)
                        }
                    }
                    },
                    new EmptySpaceOverviewModel()
                    {
                        Id = 2,
                        Coordinates = new List<GPSCoordinate>()
                    {
                        new GPSCoordinate()
                        {
                            Latitude = 54.091865036664764,
                            Longitude = 12.134019040817975
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09175806697198,
                            Longitude = 12.134035134072064
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.091899644447494,
                            Longitude = 12.135220670456647
                        },
                        new GPSCoordinate()
                        {
                            Latitude = 54.09200818685155,
                            Longitude = 12.135177755112409
                        }
                    }
                    }
                }
            };
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> EditBuildingArea([FromBody]BuildingAreaOverviewModel buildingArea)
        {
            Boolean result = true;
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBuilding([FromBody]BuildingCreateModel building)
        {
            Int32 result = 1;
            return base.Ok(result);
        }

        //[HttpGet]
        //public async Task<IActionResult> GetBuildings([FromRoute(Name = "id")] Int32 buildingAreaId)
        //{
        //    List<BuildingOverviewModel> result = new List<BuildingOverviewModel>();
        //    return base.Ok(result);
        //}

        [HttpPost]
        public async Task<IActionResult> EditBuilding([FromBody]BuildingDetailModel building)
        {
            Boolean result = true;
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteBuilding([FromBody]BuildingDeleteModel building)
        {
            Boolean result = true;
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> BuildingDetails([FromRoute(Name = "id")] Int32 id)
        {
            BuildingDetailModel result = new BuildingDetailModel()
            {
                Id = id,
                StreetName = "Teststraße",
                StreetNumber = "1",
                HouseholdUnits = 3,
                CommercialUnits = 1,
                Coordinates = new List<GPSCoordinate>()
                {
                    new GPSCoordinate()
                    {
                        Latitude = 123456,
                        Longitude = 654321
                    },
                    new GPSCoordinate()
                    {
                        Latitude = 234567,
                        Longitude = 765432
                    }
                },
                FileIds = new List<Int32>()
                {
                    1,2,3,4,5
                },
                Comment = "Dieses Gebäude ist wunderbar."
            };
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> SplitBuilding([FromBody]BuildingSplitModel building)
        {
            Boolean result = true;
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateStreet([FromBody]CreateStreetModel steetName)
        {
            Int32 result = 1;
            return base.Ok(result);
        }

        //[HttpGet]
        //public async Task<IActionResult> GetStreets([FromRoute(Name = "id")] Int32 buildingAreaId)
        //{
        //    List<StreetOverviewModel> result = new List<StreetOverviewModel>();
        //    return base.Ok(result);
        //}

        [HttpPost]
        public async Task<IActionResult> EditStreet([FromBody]StreetEditModel street)
        {
            Boolean result = true;
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteStreet([FromRoute(Name = "id")]Int32 id)
        {
            return base.Ok(true);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmptySpace([FromBody]EmptySpaceCreateModel emptySpace)
        {
            Int32 result = 1;
            return base.Ok(result);
        }

        //[HttpGet]
        //public async Task<IActionResult> GetEmptySpaces([FromRoute(Name = "id")] Int32 buildingAreaId)
        //{
        //    List<EmptySpaceOverviewModel> result = new List<EmptySpaceOverviewModel>();
        //    return base.Ok(result);
        //}

        [HttpGet]
        public async Task<IActionResult> EmptySpaceDetails([FromRoute(Name = "id")] Int32 id)
        {
            EmptySpaceDetailModel result = new EmptySpaceDetailModel()
            {
                StreetName = "Teststraße",
                Id = id,
                Comment = "Tolle freie Fläche",
                Coordinates = new List<GPSCoordinate>()
                {
                    new GPSCoordinate()
                    {
                        Latitude = 123456,
                        Longitude = 654321
                    },
                    new GPSCoordinate()
                    {
                        Latitude = 234567,
                        Longitude = 765432
                    }
                },
                FileIds = new List<Int32>()
                {
                    1,2,3,4
                }
            };
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmptySpace([FromRoute(Name = "id")] Int32 id)
        {
            return base.Ok(true);
        }

        [HttpPost]
        public async Task<IActionResult> EditEmptySpace([FromBody]EmptySpaceEditModel emptySpace)
        {
            Boolean result = true;
            return base.Ok(result);
        }
    } 
}