﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Policy = Startup.BesaAdminRoleAccessPolicyName)]
    public class CompaniesController : Controller
    {
        private readonly IUserService userService;
        private readonly IClaimsExtractor _claimsExtractor;
        private readonly ICompanyService _companyService;

        #region Constructor

        public CompaniesController(
            IUserService userService,
            IClaimsExtractor claimsExtractor,
            ICompanyService companyService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this._claimsExtractor = claimsExtractor ?? throw new ArgumentNullException(nameof(claimsExtractor));
            this._companyService = companyService ?? throw new ArgumentNullException(nameof(companyService));
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> Overview()
        {
            IEnumerable<CompanyOverviewModel> result = await _companyService.GetAllCompanies();
            return base.Ok(result);
        }

        [HttpGet]
        public async  Task<IActionResult> Details([FromRoute(Name = "id")]Int32 companyId)
        {
            CompanyDetailModel result = await _companyService.GetCompanyDetails(companyId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit([FromBody]CompanyEditModel model)
        {
            String userAuthId = _claimsExtractor.ExtractTokenValue(base.HttpContext);
            CompanyOverviewModel result = await _companyService.EditCompany(model,userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody]CompanyCreateModel model)
        {
            String userAuthId = _claimsExtractor.ExtractTokenValue(base.HttpContext);
            CompanyOverviewModel result = await _companyService.CreateCompany(model, userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee([FromRoute(Name = "id")]Int32 companyId, [FromQuery]Int32 employeeId)
        {
            String userAuthId = _claimsExtractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _companyService.AddEmployee(companyId, employeeId, userAuthId);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveEmployee([FromRoute(Name = "id")]Int32 companyId, [FromQuery]Int32 employeeId)
        {
            String userAuthId = _claimsExtractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _companyService.RemoveEmployee(companyId, employeeId, userAuthId);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute(Name ="id")]Int32 companyId)
        {
            String userAuthId = _claimsExtractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _companyService.DeleteCompany(companyId,userAuthId);
            return base.Ok(result);
        }
    }
}