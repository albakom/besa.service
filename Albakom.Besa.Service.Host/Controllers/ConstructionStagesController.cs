﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize]
    public class ConstructionStagesController : Controller
    {
        private readonly IConstructionStageService _service;
        private readonly IExcelFileGeneratorService excelFileGenerator;
        private readonly IClaimsExtractor _extractor;

        #region Constructor

        public ConstructionStagesController(
            IConstructionStageService service,
            IExcelFileGeneratorService excelFileGenerator,
            IClaimsExtractor extractor)
        {
            this._service = service ?? throw new ArgumentNullException(nameof(service));
            this.excelFileGenerator = excelFileGenerator ?? throw new ArgumentNullException(nameof(excelFileGenerator));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Overview()
        {
            IEnumerable<ConstructionStageOverviewModel> result = await _service.GetAllConstructionStages();
            return base.Ok(result);
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DeleteConstructionStage([FromRoute(Name = "id")]Int32 constructionStageId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _service.DeleteConstructionStage(constructionStageId,userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> UpdateInfos([FromRoute(Name = "id")]Int32 constructionStageId)
        {
            ConstructionStageDetailsForEditModel result = await _service.GetDetailsForEdit(constructionStageId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Update([FromBody]ConstructionStageUpdateModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            ConstructionStageDetailsForEditModel result = await _service.UpdateConstructionStage(model,userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OverviewForCivilWork([FromQuery]Boolean onlyUnboundedJobs = false)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await _service.GetOverviewForCilivWork(onlyUnboundedJobs);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DetailsForCivilWork([FromRoute(Name = "id")]Int32 constructionStageId, [FromQuery]Boolean onlyUnboundedJob = false)
        {
            ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview> result = await _service.GetCivilWorkDetail(constructionStageId, onlyUnboundedJob);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.InjectOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OverviewForInject([FromQuery]Boolean onlyUnboundedJobs = false)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await _service.GetOverviewForInject(onlyUnboundedJobs);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.InjectOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DetailsForInject([FromRoute(Name = "id")]Int32 constructionStageId, [FromQuery]Boolean onlyUnboundedJob = false)
        {
            ConstructionStageDetailModel<InjectJobOverviewModel> result = await _service.GetInjectDetails(constructionStageId, onlyUnboundedJob);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OverviewForSplice([FromQuery]Boolean onlyUnboundedJobs = false)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await _service.GetOverviewForSplice(onlyUnboundedJobs);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DetailsForSplice([FromRoute(Name = "id")]Int32 constructionStageId, [FromQuery]Boolean onlyUnboundedJob = false)
        {
            ConstructionStageDetailModel<SpliceBranchableJobOverviewModel> result = await _service.GetSpliceDetails(constructionStageId, onlyUnboundedJob);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OverviewForPrepareBranchable([FromQuery]Boolean onlyUnboundedJobs = false)
        {
            IEnumerable<ConstructionStageOverviewForJobs> result = await _service.GetOverviewForPrepareBranchable(onlyUnboundedJobs);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DetailsForPrepareBranchable([FromRoute(Name = "id")]Int32 constructionStageId, [FromQuery]Boolean onlyUnboundedJob = false)
        {
            ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel> result = await _service.GetPrepareBranchableDetails(constructionStageId, onlyUnboundedJob);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OverviewForProjectManangement()
        {
            IEnumerable<ConstructionStageForProjectManagementOverviewModel> result = await _service.GetOverviewForProjectManagement();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SearchCabinets([FromQuery]String query)
        {
            IEnumerable<SimpleCabinetOverviewModel> result = await _service.SearchCabinets(query, 10);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SearchSleeves([FromQuery]String query)
        {
            IEnumerable<SimpleSleeveOverviewModel> result = await _service.SearchSleeves(query, 10);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SearchBuildings([FromQuery]String query)
        {
            IEnumerable<SimpleBuildingOverviewModel> result = await _service.SearchStreet(query, 10);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> FlatsByBuilding([FromRoute(Name = "id")]Int32 buildingId)
        {
            IEnumerable<SimpleFlatOverviewModel> result = await _service.GetFlatsByBuildingId(buildingId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> BuldingsByBrachnable([FromRoute(Name = "id")]Int32 branchableId)
        {
            IEnumerable<SimpleBuildingOverviewModel> result = await _service.GetBuildingsByBranchable(branchableId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Create([FromBody]ConstructionStageCreateModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            ConstructionStageOverviewModel result = await _service.CreateConstructionStage(model, userAuthId);
            return base.Ok(result);
        }

        //[HttpPost]
        //public async Task<IActionResult> Edit([FromBody]CompanyEditModel model)
        //{
        //    CompanyOverviewModel result = await _service.EditCompany(model);
        //    return base.Ok(result);
        //}

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateBranchJobs([FromRoute(Name = "id")]Int32 contructionStageId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            await _service.CreateBranchOffJob(contructionStageId, userAuthId);
            return base.Ok(true);
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Delete([FromRoute(Name = "id")]Int32 constructionStageId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _service.DeleteConstructionStage(constructionStageId, userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateFlat([FromBody]FlatCreateModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            SimpleFlatOverviewModel result = await _service.AddFlatToBuilding(model, userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CheckIfBuildingIsConnected([FromRoute(Name = "id")]Int32 buildingId)
        {
            Boolean result = await _service.CheckIfBuildingIsConnected(buildingId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateCableTypesFromAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateAllCableTypesFromAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateBuildingCableInfoFromAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateBuildingCableInfoFromAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateBuildingNameFromAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateBuildingNameFromAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateBuildigsFromAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateBuildigsFromAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateCablesFromAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateCablesFromAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdatePopInformation()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdatePopInformation(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateMainCables()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateMainCables(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdatePatchConnections()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdatePatchConnections(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateSplices()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateSplices(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateMainCableForBranchable()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateMainCableForBranchable(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public IActionResult UpdateAllDataFromProjectAdapter()
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            String jobId = BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateAllDataFromProjectAdapter(userAuthId));
            return base.Ok(new JobResultModel() { JobId = jobId });
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> MainCables()
        {
            IEnumerable<MainCableOverviewModel> result = await _service.GetMainCables();
            return base.Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> SpliceOverviewAsExcel([FromRoute(Name = "id")]Int32 branchableId)
        {
            NamedExportFile result = await excelFileGenerator.GenerateSpliceOverview(branchableId);
            return base.File(result.Data,result.MimeType, result.Name);
        }
    }
}