﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
    public class CustomerController : Controller
    {
        private readonly IClaimsExtractor _extractor;
        private readonly ICustomerService customerService;

        #region Constructor

        public CustomerController(
            IClaimsExtractor extractor,
            ICustomerService customerService)
        {
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
            this.customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
        }

        #endregion

        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody]CustomerConnectRequestCreateModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Int32 result = await customerService.CreateCustomerConnectRequest(model,userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateContact([FromBody]PersonInfo model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Int32 result = await customerService.CreateContact(model, userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetContacts([FromQuery]Int32 start = 0, [FromQuery]Int32 amount = 10)
        {
            IEnumerable<PersonInfo> result = await customerService.GetContacts(start, amount);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> SearchContacts([FromQuery]String query, [FromQuery]Int32 amount = 10)
        {
            IEnumerable<PersonInfo> result = await customerService.SearchContacts(query, amount);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Contact([FromRoute(Name = "id")]Int32 id)
        {
            PersonInfo result = await customerService.GetContactById(id);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> CheckIfContactIsInUse([FromRoute(Name = "id")]Int32 contactId)
        {
            Boolean result = await customerService.CheckIfContactIsInUse(contactId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> EditContact([FromBody]PersonInfo model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);

            PersonInfo result = await customerService.EditContact(model, userAuthId);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteContact([FromRoute(Name = "id")]Int32 contactId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await customerService.DeleteContact(contactId, userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> ImportContacts([FromBody]CSVContactImportInfoModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);

            IEnumerable<Int32> result = await customerService.ImportContactsFromCSV(model, userAuthId);
            return base.Ok(result);
        }
    }
}