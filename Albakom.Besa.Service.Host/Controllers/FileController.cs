﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize]
    public class FileController : Controller
    {
        #region Fields

        private readonly IFileService _fileService;
        private readonly IClaimsExtractor _extractor;

        #endregion

        #region Constructor

        public FileController(
            IFileService fileService,
            IClaimsExtractor extractor)
        {
            this._fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Overview([FromQuery]Int32 start = 0, [FromQuery]Int32 amount = 30)
        {
            IEnumerable<FileOverviewModel> result = await _fileService.GetFiles(start, amount);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Search([FromQuery]String query = "", [FromQuery]Int32 start = 0, [FromQuery]Int32 amount = 30)
        {
            IEnumerable<FileOverviewModel> result = await _fileService.SearchFiles(query, start, amount);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> AccessObjects([FromQuery]FileAccessObjects type, [FromQuery]String query, [FromQuery]Int32 amount = 10)
        {
            IEnumerable<FileObjectOverviewModel> result = await _fileService.SearchObjectForAccess(type, query, amount);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Details([FromRoute(Name = "id")]Int32 fileId)
        {
            FileDetailModel result = await _fileService.GetDetails(fileId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Edit([FromBody]FileEditModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            FileDetailModel result = await _fileService.EditFile(model, userAuthId);
            return base.Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromRoute(Name ="id")]Int32 fileId)
        {
            FileWithStreamModel file = await _fileService.GetFileWithStream(fileId);
            return base.File(file.Data, file.MimeType, $"{file.Name}.{file.Extention}");
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> Delete([FromRoute(Name = "id")]Int32 fileId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _fileService.DeleteFile(fileId, userAuthId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            String rawFileName = file.FileName;
            Int32 dotIndex = rawFileName.IndexOf('.');

            String fileName = rawFileName.Substring(0, dotIndex);
            String extention = rawFileName.Substring(dotIndex + 1);

            FileCreateModel model = new FileCreateModel
            {
                Name = fileName,
                Extention = extention,
                MimeType = file.ContentType,
                Size = file.Length > Int32.MaxValue ? Int32.MaxValue : (Int32)file.Length,
            };

            using (Stream data = file.OpenReadStream())
            {
                String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
                Int32 fileId = await _fileService.CreateFile(model, data, userAuthId);
                return base.Ok(fileId);
            }
        }
    }
}