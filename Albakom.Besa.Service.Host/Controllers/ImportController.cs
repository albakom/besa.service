﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Startup.SalesOrProjectManagerRoleAccessPolicyName)]
    public class ImportController : Controller
    {
        #region Fields

        private readonly IImportService service;
        private readonly IClaimsExtractor _extractor;

        #endregion

        #region Constructor

        public ImportController(IImportService service, IClaimsExtractor extractor)
        {
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
            this.service = service ?? throw new ArgumentNullException(nameof(service));
        }

        #endregion

        #region Methods

        [HttpPost]
        public async Task<IActionResult> Procedure([FromBody]CSVProcedureImportInfoModel model)
        {
            String userAuth = _extractor.ExtractTokenValue(base.HttpContext);
            CSVImportProcedureResultModel result = await service.ImportProcedures(model, userAuth);
            return base.Ok(result);
        }

        #endregion

    }
}
