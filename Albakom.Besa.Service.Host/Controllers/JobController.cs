﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize]
    public class JobController : Controller
    {
        private readonly IClaimsExtractor _extractor;
        private readonly IJobService _jobService;

        #region Constructor

        public JobController(
            IClaimsExtractor extractor,
            IJobService jobService)
        {
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
            this._jobService = jobService ?? throw new ArgumentNullException(nameof(jobService));
        }

        #endregion

        [HttpGet]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> BranchOffDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            BranchOffJobDetailModel result = await _jobService.GetBranchOffJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> ConnectBuildingDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            HouseConnenctionJobDetailModel result = await _jobService.GetBuildingConnenctionJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.InjectOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> InjectDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            InjectJobDetailModel result = await _jobService.GetInjectJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.CrafterOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CollectionDetails([FromRoute(Name = "id")]Int32 collectionJobId)
        {
            CollectionJobDetailsModel result = await _jobService.GetCollectionJobDetails(collectionJobId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> JobsToAcknowledge()
        {
            IEnumerable<SimpleJobOverview> result = await _jobService.GetJobsToAcknowledge();
            return base.Ok(result);
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DiscardJob([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await _jobService.DiscardJob(jobId, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> AcknowledgeJob([FromBody]AcknowledgeJobModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.AcknowledgeJob(model);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenRequests()
        {
            IEnumerable<RequestOverviewModel> result = await _jobService.GetOpenRequests();
            return base.Ok(result);
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> DeleteRequest([FromRoute(Name = "id")]Int32 requestId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await _jobService.DeleteRequest(requestId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> RequestDetails([FromRoute(Name = "id")] Int32 requestId)
        {
            RequestDetailModel result = await _jobService.GetRequestDetails(requestId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateHouseConnenction([FromRoute(Name = "id")] Int32 requestId, [FromQuery]Boolean boundLikeBranchOff = true)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateHouseConnenctionJobByRequest(requestId, boundLikeBranchOff, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateConnectBuildingJob([FromBody]HouseConnenctionJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateHouseConnenctionJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateInjectJob([FromBody]InjectJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateInjectJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> BindJobs([FromBody]BindJobModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await _jobService.BindJobs(model, authId);
            return base.Ok(result);
        }

        [HttpDelete]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> UnboundJobs([FromRoute(Name = "id")]Int32 collectionJobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Boolean result = await _jobService.DeleteCollectionJob(collectionJobId, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateSpliceBranchableJob([FromBody]SpliceBranchableJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateSpliceBranchableJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreatePrepareBranchableForSpliceJob([FromBody]PrepareBranchableForSpliceJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreatePrepareBranchableForSpliceJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateSpliceODFJob([FromBody]SpliceODFJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateSpliceODFJob(model, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SpliceBranchableDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            SpliceBranchableJobDetailModel result = await _jobService.GetSpliceBranchableJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> PrepareBranchableForSpliceDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            PrepareBranchableForSpliceJobDetailModel result = await _jobService.GetPrepareBranchbaleForSpliceJobDetails(jobId, authId);

            HttpRequest request = base.Request;
            String spliceTableUri = $"{request.Scheme}://{request.Host}/api/ConstructionStages/SpliceOverviewAsExcel/{result.Branchable.Id}";

            result.SpliceOverviewTableUrl = spliceTableUri;

            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SplicODFDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            SpliceODFJobDetailModel result = await _jobService.GetSpliceODFJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CheckIfBuildingHasCable([FromRoute(Name = "id")]Int32 buildingId)
        {
            Boolean result = await _jobService.CheckIfBuildingHasCable(buildingId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateInventoryJob([FromBody]InventoryJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateInventoryJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> FinishJobAdministrative([FromBody]FinishJobAdministrativeModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = userAuthId;

            Boolean result = await _jobService.FinishJobAdministrative(model);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateActivationJob([FromBody]ActivationJobCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            Int32 result = await _jobService.CreateActivationJob(model, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenPrepareBranchableForSpliceJobs()
        {
            IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel> result = await _jobService.GetOpenPrepareBranchableForSpliceJobsOverview();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenSpliceODFJobs()
        {
            IEnumerable<SimpleSpliceODFJobOverviewModel> result = await _jobService.GetOpenSpliceODFJobsOverview();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenActivationJobs()
        {
            IEnumerable<SimpleActivationJobOverviewModel> result = await _jobService.GetOpenActivationJobsOverview();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenSpliceODFJobsAsOverview()
        {
            IEnumerable<SpliceODFJobOverviewModel> result = await _jobService.GetOpenSpliceODFJobs();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ActiveNetworkOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> OpenAcitvationJobsAsOverview()
        {
            IEnumerable<ActivationJobOverviewModel> result = await _jobService.GetOpenActivationJobsAsOverview();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ActiveNetworkOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> ActivationJobDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            ActivationJobDetailModel result = await _jobService.GetActivationJobDetails(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.InventoryRoleAccessPolicyName)]
        public async Task<IActionResult> GetInventoryJobOverviews()
        {

            IEnumerable<InventoryJobOverviewModel> result = await _jobService.GetOpenIventoryJobs();
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.InventoryRoleAccessPolicyName)]
        public async Task<IActionResult> InventoryJobDetails([FromRoute(Name = "id")]Int32 jobId)
        {
            InventoryJobDetailModel result = await _jobService.GetInventoryobDetails(jobId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> SearchBranchOffJobByBuildingName([FromQuery]String query, [FromQuery]JobStates? state = null, [FromQuery]Int32 amount = 30)
        {
            IEnumerable<SimpleJobOverview> result = await _jobService.SearchBranchOffJobByBuildingNameQuery(query, state, amount);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CopyFinishBranchOffToConnectBuilding([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            Int32 result = await _jobService.CopyFinishFromBranchOffToConnectBuildingJob(jobId, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateJobByRequestId([FromRoute(Name = "id")]Int32 jobId)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);

            SimpleJobOverview result = await _jobService.CreateJobByRequestId(jobId, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CollectionJobOverview([FromQuery]JobCollectionFilterModel filterModel)
        {
            IEnumerable<JobCollectionOverviewModel> result = await _jobService.GetJobCollectionOverview(filterModel);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> CreateRequestFromContactCustomerJob([FromBody]RequestCustomerConnectionByContactModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            Int32 result = await _jobService.CreateRequestFromContactCustomerJob(model, authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> FinishContactAfterFinished([FromBody]ContactAfterFinishedUnsucessfullyModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _jobService.FinishContactAfterFinished(model, authId);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> ContactAfterSalesJobs([FromQuery] ContactAfterFinishedFilterModel filter)
        {
            IEnumerable<ContactAfterSalesJobOverview> result = await _jobService.GetContactAfterSalesJobs(filter);
            return base.Ok(result);
        }

        [HttpGet]
        [Authorize(Policy = Startup.SalesOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> ContactAfterSalesJobDetail([FromRoute(Name = "id")]Int32 jobId)
        {
            ContactAfterSalesJobDetails result = await _jobService.GetContactAfterSalesJobDetail(jobId);
            return base.Ok(result);
        }
    }
}