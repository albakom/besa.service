﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize]
    public class PersonalController : Controller
    {
        private readonly IClaimsExtractor _extractor;
        private readonly IUserService _userService;
        private readonly IJobService _jobService;
        private readonly IMessageService _msgService;
        private readonly ICompanyService _companyService;

        #region Constructor

        public PersonalController(
            IClaimsExtractor extractor,
            IUserService userService,
            IJobService jobService,
            IMessageService msgService,
            ICompanyService companyService)
        {
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
            this._userService = userService;
            this._jobService = jobService ?? throw new ArgumentNullException(nameof(jobService));
            this._msgService = msgService ?? throw new ArgumentNullException(nameof(msgService));
            this._companyService = companyService;
        }

        #endregion

        [Obsolete("Replaced by UserInfo", false)]
        [HttpGet]
        public async Task<IActionResult> Roles()
        {
            String authServiceId = _extractor.ExtractTokenValue(base.HttpContext);
            BesaRoles result = await _userService.GetRolesByAuthServiceId(authServiceId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> UserInfo()
        {
            String authServiceId = _extractor.ExtractTokenValue(base.HttpContext);
            UserInfoModel result = await _userService.GetUserInfoModel(authServiceId);

            result.UnreadMessage = await _msgService.GetMessages(true, authServiceId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> MemberStatus()
        {
            String authServiceId = _extractor.ExtractTokenValue(base.HttpContext);
            MemberStatus result = await _userService.GetMemberStatus(authServiceId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateAccessRequest([FromBody]UserAccessRequestCreateModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.AuthServiceId = authId;
            await _userService.CreateAccessRequest(model);
            return base.Ok(true);
        }

        [HttpGet]
        public async Task<IActionResult> OpenJobs()
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            IEnumerable<PersonalJobOverviewModel> result = await _userService.GetJobsForMember(authId);
            return base.Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> FinishBranchOffJob([FromBody]FinishBranchOffJobModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishBranchOffJobModel(model);
            return base.Ok();
        }

        [HttpPost]
        [Authorize(Policy = Startup.CivilWorkerOrProjectManagerRoleAccessPolicyName)]
        public async Task<IActionResult> FinishBuildingConnenctionJob([FromBody]FinishBuildingConnenctionJobModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishBuildingConnenction(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.InjectOrProjectManagerRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> FinishInjectJob([FromBody]InjectJobFinishedModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishInjectJob(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> SpliceBranchableJob([FromBody]SpliceBranchableJobFinishModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishSpliceBranchableJob(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> PrepareBranchableForSpliceJob([FromBody]PrepareBranchableForSpliceJobFinishModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishPrepareBranchableForSpliceJob(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.SplicerOrProjectManagerRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> SpliceODFJob([FromBody]SpliceODFJobFinishModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishSpliceODFJob(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.ActiveNetworkOrProjectManagerRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> FinishActivationJob([FromBody]ActivationJobFinishModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishActivationJob(model);
            return base.Ok();
        }

        [Authorize(Policy = Startup.InventoryRoleAccessPolicyName)]
        [HttpPost]
        public async Task<IActionResult> FinishInventoryJob([FromBody]InventoryFinishedJobModel model)
        {
            String authId = _extractor.ExtractTokenValue(base.HttpContext);
            model.UserAuthId = authId;

            Boolean result = await _jobService.FinishInventoryJob(model);
            return base.Ok(result);
        }

        private HashSet<TEnum> ParseEnums<TEnum>(String input, Char delimiter = '-') where TEnum : struct
        {
            HashSet<TEnum> result = new HashSet<TEnum>();
            if(String.IsNullOrEmpty(input) == true)
            {
                return result;
            }

            String[] parts = input.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
           

            foreach (String part in parts)
            {
                TEnum value = default(TEnum);
                if (Enum.TryParse<TEnum>(part, out value) == true)
                {
                    result.Add(value);
                }
            }

            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetMessages([FromQuery]MessageFilterModel filterModel, [FromQuery] String actionList, [FromQuery] String typeList)
        {
            if(filterModel == null)
            {
                return base.Conflict();
            }

            filterModel.Actions = ParseEnums<MessageActions>(actionList);
            filterModel.Types = ParseEnums<MessageRelatedObjectTypes>(actionList);

            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            IEnumerable<MessageModel> messages = await _msgService.GetMessages(filterModel, userAuthId);
            return base.Ok(messages);
        }

        [HttpPost]
        public async Task<IActionResult> ReadMessage([FromBody]Int32 id)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean markedAsUnread = await _msgService.MarkMessageAsRead(id, userAuthId);

            return base.Ok(markedAsUnread);
        }

    }
}