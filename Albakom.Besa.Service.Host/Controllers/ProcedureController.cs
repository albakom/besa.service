﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Startup.SalesOrProjectManagerRoleAccessPolicyName)]
    public class ProcedureController : Controller
    {
        #region Fields

        private readonly IProcedureService service;

        #endregion

        #region Constructor

        public ProcedureController(IProcedureService service)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
        }

        #endregion

        #region Methods

        [HttpGet]
        public async Task<IActionResult> Overview([FromQuery]ProcedureFilterModel filter, ProcedureSortProperties property = ProcedureSortProperties.BuildingName, SortDirections direction = SortDirections.Ascending )
        {
            IEnumerable<ProcedureOverviewModel> result = await service.GetProcedures(filter,property,direction);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> ConnectBuildingDetails([FromRoute(Name = "id")]Int32 procedueId)
        {
            BuildingConnectionProcedureDetailModel result = await service.GetConnectBuildingDetails(procedueId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> ConnectCustomerDetails([FromRoute(Name = "id")]Int32 procedueId)
        {
            CustomerConnectionProcedureDetailModel result = await service.GetConnectCustomerDetails(procedueId);
            return base.Ok(result);
        }

        #endregion

    }
}
