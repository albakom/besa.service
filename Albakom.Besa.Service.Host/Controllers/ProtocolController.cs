﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
    public class ProtocolController : Controller
    {
        private readonly IProtocolService _service;
        private static HashSet<Int32> _validProtocolActions;
        private static HashSet<Int32> _validProtocolObjectTypes;

        #region Constructor

        static ProtocolController()
        {
            _validProtocolActions = new HashSet<int>();
            foreach (var item in Enum.GetValues(typeof(MessageActions)))
            {
                _validProtocolActions.Add((Int32)item);
            }

            _validProtocolObjectTypes = new HashSet<int>();
            foreach (var item in Enum.GetValues(typeof(MessageRelatedObjectTypes)))
            {
                _validProtocolObjectTypes.Add((Int32)item);
            }
        }

        public ProtocolController(IProtocolService service)
        {
            this._service = service ?? throw new ArgumentNullException(nameof(service));
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> GetProtocol([FromQuery]ProtocolFilterModel filterModel, [FromQuery]String actionList, [FromQuery]String typeList)
        {
            filterModel.Actions = new List<MessageActions>();
            filterModel.Types = new List<MessageRelatedObjectTypes>();

            if (String.IsNullOrEmpty(actionList) == false)
            {
                String[] parts = actionList.Split('-', StringSplitOptions.RemoveEmptyEntries);
                foreach (String part in parts)
                {
                    Int32 enumRawValue = 0;
                    if(Int32.TryParse(part,out enumRawValue) == true)
                    {
                        if(_validProtocolActions.Contains(enumRawValue) == true)
                        {
                            filterModel.Actions.Add((MessageActions)enumRawValue);
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(typeList) == false)
            {
                String[] parts = typeList.Split('-', StringSplitOptions.RemoveEmptyEntries);
                foreach (String part in parts)
                {
                    Int32 enumRawValue = 0;
                    if (Int32.TryParse(part, out enumRawValue) == true)
                    {
                        if (_validProtocolObjectTypes.Contains(enumRawValue) == true)
                        {
                            filterModel.Types.Add((MessageRelatedObjectTypes)enumRawValue);
                        }
                    }
                }
            }

            IEnumerable<ProtocolEntryModel> result = await _service.GetProtocolEntries(filterModel);
            return base.Ok(result);
        }
    }
}