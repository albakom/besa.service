﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Hubs;
using Albakom.Besa.Service.Host.Infrastructure;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Policy = Startup.ProjectManagerRoleAccessPolicyName)]
    public class UpdateTaskController : Controller
    {
        private readonly IUpdateTaskService _updateTaskService;
        private readonly IClaimsExtractor _extractor;

        #region Constructor

        public UpdateTaskController(
            IUpdateTaskService taskService,
            IClaimsExtractor extractor)
        {
            this._updateTaskService = taskService ?? throw new ArgumentNullException(nameof(taskService));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> Overview([FromQuery]UpdateTasks? taskType)
        {
            IEnumerable<UpdateTaskOverviewModel> result = await _updateTaskService.GetUpdateTasks(taskType);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Details([FromRoute(Name = "id")]Int32 taskId)
        {
            UpdateTaskDetailModel result = await _updateTaskService.GetUpdateTaskDetails(taskId);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> CancelTask([FromRoute(Name = "id")]Int32 taskId)
        {
            Boolean result = await _updateTaskService.CancelUpdateTask(taskId);
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> ObjectForTask([FromQuery]UpdateTasks taskType)
        {
            IEnumerable<UpdateTaskObjectOverviewModel> result = await _updateTaskService.GetPossibleObjectForTaskType(taskType);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> StartTask([FromBody]StartUpdateTaskModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);

            if (model != null)
            {
                model.UserAuthId = userAuthId;
            }

            Int32 result = await _updateTaskService.CreateUpdateTask(model);

            switch (model.TaskType)
            {
                case UpdateTasks.UpdateBuildings:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateBuildigsFromAdapter(userAuthId));
                    break;
                case UpdateTasks.CableTypes:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateAllCableTypesFromAdapter(userAuthId));
                    break;
                case UpdateTasks.Cables:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateCablesFromAdapter(userAuthId));
                    break;
                case UpdateTasks.PoPs:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdatePopInformation(userAuthId));
                    break;
                case UpdateTasks.MainCableForPops:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateMainCables(userAuthId));
                    break;
                case UpdateTasks.PatchConnections:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdatePatchConnections(userAuthId));
                    break;
                case UpdateTasks.Splices:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateSplices(userAuthId));
                    break;
                case UpdateTasks.MainCableForBranchable:
                    BackgroundJob.Enqueue<IConstructionStageService>((service) => service.UpdateMainCableForBranchable(userAuthId));
                    break;
                default:
                    break;
            }

            return base.Ok(result);
        }
    }
}