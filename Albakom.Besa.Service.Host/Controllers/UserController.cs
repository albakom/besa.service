﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Hubs;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Albakom.Besa.Service.Host.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [Authorize(Policy = Startup.BesaAdminRoleAccessPolicyName)]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IClaimsExtractor _extractor;

        #region Constructor

        public UserController(
            IUserService userService,
            IClaimsExtractor extractor)
        {
            this._userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> RequestOverview()
        {
            IEnumerable<UserAccessRequestOverviewModel> result = await _userService.GetAllAccessRequests();
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GrantAccess([FromBody]GrantUserAccessModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Int32 userId = await _userService.GrantAccess(model, userAuthId);
            return base.Ok(userId);
        }

        [HttpGet]
        public async Task<IActionResult> All()
        {
            IEnumerable<UserOverviewModel> result = await _userService.GetAllUsers();
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> AllEmployees()
        {
            IEnumerable<UserOverviewModel> resultUsers = await _userService.GetAllUsers();
            ICollection<SimpleEmployeeModel> result = new List<SimpleEmployeeModel>();
            foreach (var item in resultUsers)
            {
                result.Add(new SimpleEmployeeModel() { Id = item.Id, Surname = item.Surname, Lastname = item.Lastname });
            }
            return base.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> DataForEdit([FromRoute(Name = "id")]Int32 userId)
        {
            UserEditModel result = await _userService.GetUserDataForEdit(userId);
            return base.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit([FromBody]UserEditModel model)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            UserOverviewModel result = await _userService.EditUser(model, userAuthId);
            return base.Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveMemberStatus([FromRoute(Name = "id")]Int32 userId)
        {
            String userAuthId = _extractor.ExtractTokenValue(base.HttpContext);
            Boolean result = await _userService.RemoveMemberStatus(userId, userAuthId);
            return base.Ok(result);
        }
    }
}