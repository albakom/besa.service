﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    public interface IAccessRequestHubClient
    {
        Task NewAccessRequest(UserAccessRequestOverviewModel overview);
        Task AccessGranted();
        Task RemoveAccess();
        Task RoleChanged(BesaRoles newRole);
    }
}
