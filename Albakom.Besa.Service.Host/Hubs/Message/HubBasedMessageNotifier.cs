﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    public class HubBasedMessageNotifier : IMessageNotifier
    {
        #region Fields

        private readonly ILogger<HubBasedMessageNotifier> _logger;
        private readonly IHubContext<MessageHub, IMessageHubClient> _hubContex;

        #endregion

        #region Constructor

        public HubBasedMessageNotifier(ILogger<HubBasedMessageNotifier> logger,
            IHubContext<MessageHub, IMessageHubClient> hubContex)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._hubContex = hubContex ?? throw new ArgumentNullException(nameof(hubContex));
        }

        #endregion

        public async Task NewMessageArrivied(string userAuthId, MessageModel message)
        {
            _logger.LogTrace("NewMessageArrivied for user with auth id {0}, Message id is : {1}", userAuthId, message.Id);

            await _hubContex.Clients.Group(userAuthId).NewMessageArrived(message);
            _logger.LogTrace("user with auth id {0} was notifiied ", userAuthId);
        }
    }
}
