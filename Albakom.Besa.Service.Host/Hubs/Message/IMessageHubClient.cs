﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    public interface IMessageHubClient
    {
        Task NewMessageArrived(MessageModel message);
    }
}
