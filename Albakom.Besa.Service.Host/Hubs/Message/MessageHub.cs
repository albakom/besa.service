﻿using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    [Authorize]
    public class MessageHub : Hub<IMessageHubClient>
    {
        #region Fields and consts

        private readonly ILogger<MessageHub> _logger;
        private readonly IClaimsExtractor _extractor;

        #endregion

        #region Constructor

        public MessageHub(ILogger<MessageHub> logger, IClaimsExtractor extractor)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
        }

        #endregion

        #region Methods

        public override async Task OnConnectedAsync()
        {
            _logger.LogTrace("Client connected to message hub");

            String authId = _extractor.ExtractTokenValue(base.Context.User);
            _logger.LogTrace("user hast auth id {0}", authId);

            String groupName = authId;
            await Groups.AddToGroupAsync(base.Context.ConnectionId, groupName);
            _logger.LogDebug("add user {0} to group {1}", authId, groupName);

            await base.OnConnectedAsync();
        }

        #endregion
    }
}

