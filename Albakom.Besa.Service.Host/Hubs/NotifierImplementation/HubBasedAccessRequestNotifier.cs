﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    public class HubBasedAccessRequestNotifier : IAccessRequestNotifier
    {
        #region Fields

        private readonly IHubContext<RequestAccessHub, IAccessRequestHubClient> _hubContext;
        private readonly ILogger<HubBasedAccessRequestNotifier> logger;

        #endregion

        #region Constuctor

        public HubBasedAccessRequestNotifier(ILogger<HubBasedAccessRequestNotifier> logger,
            IHubContext<RequestAccessHub, IAccessRequestHubClient> hubContext)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
        }

        #endregion

        #region Methods

        public async Task AccessGranted(String authUserid)
        {
            logger.LogDebug("AccessGranted for user with id {0}", authUserid);
            await this._hubContext.Clients.Group(authUserid).AccessGranted();
            logger.LogInformation("notifications send");
        }

        public async Task NewRequestAdded(UserAccessRequestOverviewModel model)
        {
            logger.LogDebug("NewRequestAdded with id {0}", model.Id);
            await this._hubContext.Clients.Group(RequestAccessHub.AdminGroupName).NewAccessRequest(model);
            logger.LogInformation("notifications send");
        }

        public async Task RemoveAccess(String authUserid)
        {
            logger.LogDebug("RemoveAccess with id {0}", authUserid);
            await this._hubContext.Clients.Group(authUserid).AccessGranted();
            logger.LogInformation("notifications send");
        }

        public async Task RoleChanged(String authUserid, BesaRoles newRole)
        {
            logger.LogDebug("RoleChanged for user with id {0}. New role is {1}", authUserid, newRole);
            await this._hubContext.Clients.Group(authUserid).RoleChanged(newRole);
            logger.LogInformation("notifications send");
        }

        #endregion
    }
}
