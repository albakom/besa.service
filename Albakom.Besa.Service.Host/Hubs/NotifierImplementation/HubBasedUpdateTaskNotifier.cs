﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    public class HubBasedUpdateTaskNotifier : IUpdateTaskNotifier
    {
        #region Fields

        private readonly ILogger<HubBasedUpdateTaskNotifier> _logger;
        private readonly IHubContext<UpdateTaskHub, IUpdateTaskHubClient> _hubContext;
        private readonly IBesaDataStorage _context;

        #endregion

        #region Constructor

        public HubBasedUpdateTaskNotifier(
            ILogger<HubBasedUpdateTaskNotifier> logger,
            IHubContext<UpdateTaskHub, IUpdateTaskHubClient> hubContext,
            IBesaDataStorage context)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
            this._context = context ?? throw new ArgumentNullException(nameof(context));
        }

        #endregion

        #region Methods

        public async Task HeartbeatChanged(UpdateTaskHeartbeatChangedModel model)
        {
            _logger.LogTrace("send notification about heatbeat changed to group with name {0}", model.TaskId);
            await _hubContext.Clients.Group(model.TaskId.ToString()).HeartbeatChanged(model);
        }

        public async Task TaskElementChanged(UpdateTaskElementOverviewModel model)
        {
            _logger.LogTrace("resolve update task element id {0} to task id ", model.Id);
            Int32 taskId = await _context.GetUpdateTaskIdByElementId(model.Id);
            _logger.LogTrace("update task element id {0} to resolved to task id ", model.Id, taskId);

            _logger.LogTrace("send notification about changed task element changed to group with name {0}", taskId);
            await _hubContext.Clients.Group(taskId.ToString()).TaskElementChanged(model);
        }

        public async Task TaskLockDetected(Int32 taskId)
        {
            _logger.LogTrace("send notification about task deadlock to group with name {0}", taskId);
            await _hubContext.Clients.Group(taskId.ToString()).TaskLockDetected(taskId);
        }

        public async Task UpdateTaskAdded(UpdateTaskOverviewModel model)
        {
            _logger.LogTrace("send notitication about new added task to all clients");
            await _hubContext.Clients.All.UpdateTaskAdded(model);
        }

        public async Task UpdateTaskCanceled(Int32 taskId)
        {
            _logger.LogTrace("send notitication about canceld task to all clients");
            await _hubContext.Clients.All.UpdateTaskCanceled(taskId);
        }

        public async Task UpdateTaskFinished(UpdateTasFinishedStateChangedModel model)
        {
            _logger.LogTrace("send notitication about canceld task to all clients");
            await _hubContext.Clients.All.UpdateTaskFinished(model);
        }

        #endregion


    }
}
