﻿using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Host.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    [Authorize]
    public class RequestAccessHub : Hub<IAccessRequestHubClient>
    {
        #region Fields and consts

        public const String AdminGroupName = "admin";
        private readonly ILogger<RequestAccessHub> _logger;
        private readonly IClaimsExtractor _extractor;
        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public RequestAccessHub(ILogger<RequestAccessHub> logger, IClaimsExtractor extractor, IUserService userService)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._extractor = extractor ?? throw new ArgumentNullException(nameof(extractor));
            this._userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        #endregion

        #region Methods

        public override async Task OnConnectedAsync()
        {
            _logger.LogDebug("Client connected");

            String authId = _extractor.ExtractTokenValue(base.Context.User);
            _logger.LogDebug("user as id {0}", authId);

            String groupName = authId;

            Boolean isMember = await _userService.CheckIfUserExists(authId);
            _logger.LogDebug("user is already member: {0}", isMember);

            if (isMember == true)
            {
                Boolean isAdmin = await _userService.CheckIfUserHasRole(authId, Contracts.Models.BesaRoles.Admin);
                _logger.LogDebug("user is admin: {0}", isAdmin);
                if (isAdmin == true)
                {
                    groupName = AdminGroupName;
                }
            }

            await Groups.AddToGroupAsync(base.Context.ConnectionId, groupName);
            _logger.LogDebug("add user {0} to group {1}", authId, groupName);

            await base.OnConnectedAsync();
        }

        #endregion
    }
}
