﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Hubs
{
    [Authorize]
    public class UpdateTaskHub : Hub<IUpdateTaskHubClient>
    {
        #region Fields

        private readonly ILogger<UpdateTaskHub> _logger;

        #endregion

        #region Constructor

        public UpdateTaskHub(ILogger<UpdateTaskHub> logger)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        public override async Task OnConnectedAsync()
        {
            _logger.LogTrace("Client connected to updatetask hub");
            await base.OnConnectedAsync();
        }

        public async Task JoinTask(Int32 taskId)
        {
            _logger.LogTrace("{0} joins task group with id {1}", base.Context.ConnectionId, taskId);
            await Groups.AddToGroupAsync(base.Context.ConnectionId, taskId.ToString());
        }

        public async Task LeaveTask(Int32 taskId)
        {
            _logger.LogTrace("{0} leaves task group with id {1}", base.Context.ConnectionId, taskId);
            await Groups.RemoveFromGroupAsync(base.Context.ConnectionId, taskId.ToString());
        }

        #endregion
    }
}
