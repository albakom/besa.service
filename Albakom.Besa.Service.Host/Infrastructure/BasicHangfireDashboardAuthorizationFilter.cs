﻿using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using ZNetCS.AspNetCore.Authentication.Basic;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class BasicHangfireDashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();

            if (httpContext.User.Identity.IsAuthenticated == false)
            {
                Task<AuthenticateResult> authTask = httpContext.AuthenticateAsync(BasicAuthenticationDefaults.AuthenticationScheme);
                authTask.Wait();

                if (authTask.Result.Succeeded == false)
                {
                    Task challangeTask = httpContext.ChallengeAsync(BasicAuthenticationDefaults.AuthenticationScheme);
                    challangeTask.Wait();
                    return false;
                }

                if (authTask.Result.Principal.Identity.IsAuthenticated == true)
                {
                    return authTask.Result.Principal.IsInRole("hangfire-admin");
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return httpContext.User.IsInRole("hangfire-admin");
            }

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            // return httpContext.User.Identity.IsAuthenticated;
        }
    }
}
