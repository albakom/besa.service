﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class BesaRoleHandler: AuthorizationHandler<BesaRoleRequirement>
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly ILogger<BesaRoleHandler> _logger;
        private readonly IClaimsExtractor _claimsExtractor;

        #endregion

        #region Constructor

        public BesaRoleHandler(IUserService userService, ILogger<BesaRoleHandler> logger, IClaimsExtractor claimsExtractor)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _claimsExtractor = claimsExtractor ?? throw new ArgumentNullException(nameof(claimsExtractor));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        #endregion

        #region HandleRequirement

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, BesaRoleRequirement requirement)
        {
            _logger.LogDebug("Begin handle role requierement");

            if (context == null)
            {
                _logger.LogInformation("not context");
                return;
            }

            String authServiceId = _claimsExtractor.ExtractTokenValue(context.User);
            if (await _userService.CheckIfUserExists(authServiceId) == false)
            {
                _logger.LogInformation("user with auth id {0} not exists", authServiceId);
                return;
            }

            _logger.LogTrace("getting roles for user with auth id {0}", authServiceId);
            BesaRoles userRoles = await _userService.GetRolesByAuthServiceId(authServiceId);
            _logger.LogTrace("user with auth id {0} has roles: {1}", authServiceId, userRoles);
            Boolean hasRole = false;
            foreach (BesaRoles requiredRole in requirement.RequiredRoles)
            {
                if( (requiredRole & userRoles) == requiredRole)
                {
                    hasRole = true;
                    break;
                }
            }

            if(hasRole == false)
            {
                _logger.LogInformation("user with auth id {0} has not the required role", authServiceId);
                return;
            }
            else
            {
                context.Succeed(requirement);
            }
        }

        #endregion

    }
}
