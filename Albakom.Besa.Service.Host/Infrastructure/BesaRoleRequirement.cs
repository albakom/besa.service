﻿using Albakom.Besa.Service.Contracts.Models;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class BesaRoleRequirement : IAuthorizationRequirement
    {
        public ICollection<BesaRoles> RequiredRoles { get; set; }

        public BesaRoleRequirement(BesaRoles role)
        {
            this.RequiredRoles = new List<BesaRoles> { role };
        }

        public BesaRoleRequirement(params BesaRoles[] roles)
        {
            this.RequiredRoles = new List<BesaRoles>(roles);
        }

    }
}
