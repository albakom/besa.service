﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class ClaimsExtractor : IClaimsExtractor
    {
        #region Properties

        private String _claimType;
        private ILogger<ClaimsExtractor> _logger;

        #endregion

        #region Constructor

        public ClaimsExtractor(String claimType, ILogger<ClaimsExtractor> logger)
        {
            _claimType = claimType ?? throw new ArgumentNullException(nameof(claimType));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        public String ExtractTokenValue(HttpContext context)
        {
            _logger.LogDebug("ExtractTokenValue from Context");

            String result = String.Empty;
            if (context == null)
            {
                _logger.LogInformation("context is null");
                return String.Empty;
            }
            result = ExtractTokenValue(context.User);
            return result;
        }

        public string ExtractTokenValue(ClaimsPrincipal principal)
        {
            _logger.LogDebug("ExtractTokenValue form principal");
            String result = String.Empty;

             if (principal == null)
            {
                _logger.LogInformation("user is null. Maybe not authenticated?");
            }
            else if (principal.HasClaim(x => x.Type == _claimType) == false)
            {
                _logger.LogInformation("claim {0} not found on user {1}", _claimType, principal.ToString());

            }
            else
            {
                Claim suplierClaim = principal.FindFirst(_claimType);
                result = suplierClaim.Value;
                _logger.LogInformation("claim {0} found on user {1}. Value {2}", _claimType, principal.ToString(), result);
            }

            return result;
        }

        #endregion

    }
}
