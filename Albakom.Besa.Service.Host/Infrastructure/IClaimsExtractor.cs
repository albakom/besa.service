﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public interface IClaimsExtractor
    {
        String ExtractTokenValue(HttpContext context);
        String ExtractTokenValue(ClaimsPrincipal principal);
    }
}
