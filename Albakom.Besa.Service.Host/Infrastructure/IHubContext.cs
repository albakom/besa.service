﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public interface ITypedHubContext<THub,THubClient> : IHubContext<THub> where THub : Hub
    {
        IHubClients<THubClient> TypedClients { get; }
    }
}
