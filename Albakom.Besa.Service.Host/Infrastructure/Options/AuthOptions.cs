﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class AuthOptions
    {
        #region Properites

        public String AuthUri { get; set; }

        #endregion

        #region Constructor

        public AuthOptions()
        {

        }

        #endregion
    }
}
