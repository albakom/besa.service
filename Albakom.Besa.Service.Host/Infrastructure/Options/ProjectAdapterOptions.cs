﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class ProjectAdapterOptions
    {
        #region Properties

        public String ServiceUrl { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterOptions()
        {

        }

        #endregion
    }
}
