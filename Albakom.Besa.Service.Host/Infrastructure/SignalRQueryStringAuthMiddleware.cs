﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class SignalRQueryStringAuthMiddleware
    {
        private readonly SignalRQueryStringAuthOptions options;
        private readonly RequestDelegate _next;

        public SignalRQueryStringAuthMiddleware(RequestDelegate next, SignalRQueryStringAuthOptions options)
        {
            this.options = options ?? throw new ArgumentNullException(nameof(options));
            this._next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey("Connection") == true)
            {
                if (context.Request.Headers["Connection"].ToString().ToLower().Contains("upgrade") &&
context.Request.Query.TryGetValue(options.QueryKey, out var token))
                {
                    context.Request.Headers.Add("Authorization", $"{options.HeaderAuthtype} {token.First()}");
                }
            }

            return _next.Invoke(context);

        }
    }

    public static class SignalRQueryStringAuthExtensions
    {
        public static IApplicationBuilder UseSignalRQueryStringAuth(this IApplicationBuilder builder)
        {
            return UseSignalRQueryStringAuth(builder, new SignalRQueryStringAuthOptions());
        }

        public static IApplicationBuilder UseSignalRQueryStringAuth(this IApplicationBuilder builder, SignalRQueryStringAuthOptions options)
        {
            return builder.UseMiddleware<SignalRQueryStringAuthMiddleware>(options);
        }
    }
}
