﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Host.Infrastructure
{
    public class SignalRQueryStringAuthOptions
    {
        #region Properties

        public String QueryKey { get; set; } = "access_token";
        public String HeaderAuthtype { get; set; } = "Bearer";

        #endregion

        #region Constructor

        public SignalRQueryStringAuthOptions()
        {

        }

        #endregion

    }
}
