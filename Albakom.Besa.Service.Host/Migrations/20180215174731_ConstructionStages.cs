﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class ConstructionStages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConstructionStages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConstructionStages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Branchables",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConstructionStageId = table.Column<int>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true),
                    ProjectId = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branchables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Branchables_ConstructionStages_ConstructionStageId",
                        column: x => x.ConstructionStageId,
                        principalTable: "ConstructionStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Buldings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BranchableId = table.Column<int>(nullable: false),
                    CommercialUnits = table.Column<int>(nullable: false),
                    ConstructionStageId = table.Column<int>(nullable: true),
                    HouseConnenctionColorName = table.Column<string>(nullable: true),
                    HouseConnenctionHexValue = table.Column<string>(nullable: true),
                    HouseholdUnits = table.Column<int>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    MicroDuctColorName = table.Column<string>(nullable: true),
                    MicroDuctHexValue = table.Column<string>(nullable: true),
                    NormalizedStreetName = table.Column<string>(nullable: false),
                    ProjectId = table.Column<string>(maxLength: 128, nullable: false),
                    StreetName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buldings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buldings_Branchables_BranchableId",
                        column: x => x.BranchableId,
                        principalTable: "Branchables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Buldings_ConstructionStages_ConstructionStageId",
                        column: x => x.ConstructionStageId,
                        principalTable: "ConstructionStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Branchables_ConstructionStageId",
                table: "Branchables",
                column: "ConstructionStageId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_BranchableId",
                table: "Buldings",
                column: "BranchableId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_ConstructionStageId",
                table: "Buldings",
                column: "ConstructionStageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Buldings");

            migrationBuilder.DropTable(
                name: "Branchables");

            migrationBuilder.DropTable(
                name: "ConstructionStages");
        }
    }
}
