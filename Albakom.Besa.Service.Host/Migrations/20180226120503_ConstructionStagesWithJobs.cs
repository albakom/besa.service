﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class ConstructionStagesWithJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BranchOffJobId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CollectionJobs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    FinishedTill = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionJobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CollectionJobs_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    BuildingId = table.Column<int>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CollectionId = table.Column<int>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jobs_Buldings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buldings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Jobs_CollectionJobs_CollectionId",
                        column: x => x.CollectionId,
                        principalTable: "CollectionJobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_BranchOffJobId",
                table: "Buldings",
                column: "BranchOffJobId");

            migrationBuilder.CreateIndex(
                name: "IX_CollectionJobs_CompanyId",
                table: "CollectionJobs",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CollectionId",
                table: "Jobs",
                column: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_Jobs_BranchOffJobId",
                table: "Buldings",
                column: "BranchOffJobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_Jobs_BranchOffJobId",
                table: "Buldings");

            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "CollectionJobs");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_BranchOffJobId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "BranchOffJobId",
                table: "Buldings");
        }
    }
}
