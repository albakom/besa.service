﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class BuildingWithoutConstructionStages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_ConstructionStages_ConstructionStageId",
                table: "Buldings");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_ConstructionStageId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "ConstructionStageId",
                table: "Buldings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConstructionStageId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_ConstructionStageId",
                table: "Buldings",
                column: "ConstructionStageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_ConstructionStages_ConstructionStageId",
                table: "Buldings",
                column: "ConstructionStageId",
                principalTable: "ConstructionStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
