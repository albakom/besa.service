﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class CustomerContactsAndFlat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Jobs",
                newName: "ConnectBuildingJobDataModel_BuildingId");

            migrationBuilder.AddColumn<int>(
                name: "BuildingId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActivePointNearSubscriberEndpointDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ActivePointNearSubscriberEndpointValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ArchitectPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ConnectioOfOtherMediaRequired",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ConnectionAppointment",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionForHouseConnection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DuctAmount",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OnlyHouseConnection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PowerForActiveEquipmentDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PowerForActiveEquipmentValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PropertyOwnerPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SubscriberEndpointLength",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubscriberEndpointNearConnectionPointDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SubscriberEndpointNearConnectionPointValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkmanPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FinishedAt",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConnenctionJobId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ContactInfos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(maxLength: 64, nullable: false),
                    CompanyName = table.Column<string>(maxLength: 255, nullable: true),
                    EmailAddress = table.Column<string>(maxLength: 255, nullable: true),
                    Lastname = table.Column<string>(maxLength: 64, nullable: false),
                    Phone = table.Column<string>(maxLength: 32, nullable: false),
                    PostalCode = table.Column<string>(maxLength: 5, nullable: false),
                    Street = table.Column<string>(maxLength: 255, nullable: false),
                    StreetNumber = table.Column<string>(maxLength: 8, nullable: false),
                    Surname = table.Column<string>(maxLength: 64, nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Flats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    Floor = table.Column<string>(maxLength: 128, nullable: true),
                    Number = table.Column<string>(maxLength: 128, nullable: false),
                    ProjectAdapterCableId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Flats_Buldings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buldings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnenctionJobId = table.Column<int>(nullable: true),
                    ContactId = table.Column<int>(nullable: false),
                    FlatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Jobs_ConnenctionJobId",
                        column: x => x.ConnenctionJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_ContactInfos_ContactId",
                        column: x => x.ContactId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Customers_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                unique: true,
                filter: "[ConnectBuildingJobDataModel_BuildingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ArchitectPersonId",
                table: "Jobs",
                column: "ArchitectPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_PropertyOwnerPersonId",
                table: "Jobs",
                column: "PropertyOwnerPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_WorkmanPersonId",
                table: "Jobs",
                column: "WorkmanPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_ConnenctionJobId",
                table: "Buldings",
                column: "ConnenctionJobId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ConnenctionJobId",
                table: "Customers",
                column: "ConnenctionJobId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ContactId",
                table: "Customers",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_FlatId",
                table: "Customers",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_Flats_BuildingId",
                table: "Flats",
                column: "BuildingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_Jobs_ConnenctionJobId",
                table: "Buldings",
                column: "ConnenctionJobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectPersonId",
                table: "Jobs",
                column: "ArchitectPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Customers_CustomerId",
                table: "Jobs",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_PropertyOwnerPersonId",
                table: "Jobs",
                column: "PropertyOwnerPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanPersonId",
                table: "Jobs",
                column: "WorkmanPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_Jobs_ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Customers_CustomerId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "ContactInfos");

            migrationBuilder.DropTable(
                name: "Flats");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ActivePointNearSubscriberEndpointDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ActivePointNearSubscriberEndpointValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectioOfOtherMediaRequired",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectionAppointment",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DescriptionForHouseConnection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DuctAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "OnlyHouseConnection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PowerForActiveEquipmentDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PowerForActiveEquipmentValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointLength",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointNearConnectionPointDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointNearConnectionPointValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinishedAt",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.RenameColumn(
                name: "ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                newName: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
