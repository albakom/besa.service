﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class FinishJobsAndFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FinishedJobs",
                columns: table => new
                {
                    DuctChanged = table.Column<bool>(nullable: true),
                    DuctChangedDescription = table.Column<string>(nullable: true),
                    Blub = table.Column<string>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcceptedAt = table.Column<DateTimeOffset>(nullable: true),
                    AccepterId = table.Column<int>(nullable: true),
                    Comment = table.Column<string>(maxLength: 1000, nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    FinishedAt = table.Column<DateTimeOffset>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    JobberId = table.Column<int>(nullable: false),
                    ProblemHappend = table.Column<bool>(nullable: false),
                    ProblemHappendDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinishedJobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinishedJobs_Users_AccepterId",
                        column: x => x.AccepterId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FinishedJobs_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FinishedJobs_Users_JobberId",
                        column: x => x.JobberId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Files",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    Extention = table.Column<string>(nullable: false),
                    JobId = table.Column<int>(nullable: true),
                    MimeType = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Size = table.Column<long>(nullable: false),
                    StorageUrl = table.Column<string>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Files", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Files_FinishedJobs_JobId",
                        column: x => x.JobId,
                        principalTable: "FinishedJobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Files_JobId",
                table: "Files",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_FinishedJobs_AccepterId",
                table: "FinishedJobs",
                column: "AccepterId");

            migrationBuilder.CreateIndex(
                name: "IX_FinishedJobs_JobId",
                table: "FinishedJobs",
                column: "JobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FinishedJobs_JobberId",
                table: "FinishedJobs",
                column: "JobberId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Files");

            migrationBuilder.DropTable(
                name: "FinishedJobs");
        }
    }
}
