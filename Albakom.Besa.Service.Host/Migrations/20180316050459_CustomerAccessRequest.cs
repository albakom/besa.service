﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class CustomerAccessRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Customers_CustomerId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ActivePointNearSubscriberEndpointDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ActivePointNearSubscriberEndpointValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ArchitectPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectioOfOtherMediaRequired",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectionAppointment",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DescriptionForHouseConnection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DuctAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "OnlyHouseConnection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PowerForActiveEquipmentDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PowerForActiveEquipmentValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PropertyOwnerPersonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointLength",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointNearConnectionPointDescription",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubscriberEndpointNearConnectionPointValue",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WorkmanPersonId",
                table: "Jobs");

            migrationBuilder.CreateTable(
                name: "CustomerConnenctionRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivePointNearSubscriberEndpointDescription = table.Column<string>(nullable: true),
                    ActivePointNearSubscriberEndpointValue = table.Column<bool>(nullable: false),
                    ArchitectPersonId = table.Column<int>(nullable: true),
                    BuildingId = table.Column<int>(nullable: true),
                    CivilWorkIsDoneByCustomer = table.Column<bool>(nullable: false),
                    ConnectioOfOtherMediaRequired = table.Column<bool>(nullable: false),
                    ConnectionAppointment = table.Column<DateTimeOffset>(nullable: false),
                    CustomerPersonId = table.Column<int>(nullable: true),
                    DescriptionForHouseConnection = table.Column<string>(nullable: true),
                    DuctAmount = table.Column<double>(nullable: true),
                    FlatId = table.Column<int>(nullable: true),
                    IsClosed = table.Column<bool>(nullable: false),
                    OnlyHouseConnection = table.Column<bool>(nullable: false),
                    PowerForActiveEquipmentDescription = table.Column<string>(nullable: true),
                    PowerForActiveEquipmentValue = table.Column<bool>(nullable: false),
                    PropertyOwnerPersonId = table.Column<int>(nullable: true),
                    SubscriberEndpointLength = table.Column<double>(nullable: true),
                    SubscriberEndpointNearConnectionPointDescription = table.Column<string>(nullable: true),
                    SubscriberEndpointNearConnectionPointValue = table.Column<bool>(nullable: false),
                    WorkmanPersonId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerConnenctionRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_ContactInfos_ArchitectPersonId",
                        column: x => x.ArchitectPersonId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_Buldings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buldings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_ContactInfos_CustomerPersonId",
                        column: x => x.CustomerPersonId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_ContactInfos_PropertyOwnerPersonId",
                        column: x => x.PropertyOwnerPersonId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerConnenctionRequests_ContactInfos_WorkmanPersonId",
                        column: x => x.WorkmanPersonId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_ArchitectPersonId",
                table: "CustomerConnenctionRequests",
                column: "ArchitectPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_BuildingId",
                table: "CustomerConnenctionRequests",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_CustomerPersonId",
                table: "CustomerConnenctionRequests",
                column: "CustomerPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_FlatId",
                table: "CustomerConnenctionRequests",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_PropertyOwnerPersonId",
                table: "CustomerConnenctionRequests",
                column: "PropertyOwnerPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerConnenctionRequests_WorkmanPersonId",
                table: "CustomerConnenctionRequests",
                column: "WorkmanPersonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerConnenctionRequests");

            migrationBuilder.AddColumn<string>(
                name: "ActivePointNearSubscriberEndpointDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ActivePointNearSubscriberEndpointValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ArchitectPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ConnectioOfOtherMediaRequired",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ConnectionAppointment",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionForHouseConnection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DuctAmount",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OnlyHouseConnection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PowerForActiveEquipmentDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PowerForActiveEquipmentValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PropertyOwnerPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SubscriberEndpointLength",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubscriberEndpointNearConnectionPointDescription",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SubscriberEndpointNearConnectionPointValue",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkmanPersonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnenctionJobId = table.Column<int>(nullable: true),
                    ContactId = table.Column<int>(nullable: false),
                    FlatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Jobs_ConnenctionJobId",
                        column: x => x.ConnenctionJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_ContactInfos_ContactId",
                        column: x => x.ContactId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Customers_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ArchitectPersonId",
                table: "Jobs",
                column: "ArchitectPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_PropertyOwnerPersonId",
                table: "Jobs",
                column: "PropertyOwnerPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_WorkmanPersonId",
                table: "Jobs",
                column: "WorkmanPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ConnenctionJobId",
                table: "Customers",
                column: "ConnenctionJobId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ContactId",
                table: "Customers",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_FlatId",
                table: "Customers",
                column: "FlatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectPersonId",
                table: "Jobs",
                column: "ArchitectPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Customers_CustomerId",
                table: "Jobs",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_PropertyOwnerPersonId",
                table: "Jobs",
                column: "PropertyOwnerPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanPersonId",
                table: "Jobs",
                column: "WorkmanPersonId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
