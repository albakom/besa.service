﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class FinishBuildingConnenction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DuctChangedDescription",
                table: "FinishedJobs",
                newName: "ConnectBuildingFinishedDataModel_DuctChangedDescription");

            migrationBuilder.RenameColumn(
                name: "DuctChanged",
                table: "FinishedJobs",
                newName: "ConnectBuildingFinishedDataModel_DuctChanged");

            migrationBuilder.RenameColumn(
                name: "Blub",
                table: "FinishedJobs",
                newName: "DuctChangedDescription");

            migrationBuilder.AddColumn<int>(
                name: "ArchitectContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ConnectionAppointment",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ConnectionOfOtherMediaRequired",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Jobs",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OnlyHouseConnection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OwnerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkmanContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DuctChanged",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ArchitectContactId",
                table: "Jobs",
                column: "ArchitectContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_OwnerContactId",
                table: "Jobs",
                column: "OwnerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_WorkmanContactId",
                table: "Jobs",
                column: "WorkmanContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectContactId",
                table: "Jobs",
                column: "ArchitectContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_OwnerContactId",
                table: "Jobs",
                column: "OwnerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanContactId",
                table: "Jobs",
                column: "WorkmanContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_ArchitectContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_OwnerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_WorkmanContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ArchitectContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_OwnerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_WorkmanContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ArchitectContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CivilWorkIsDoneByCustomer",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectionAppointment",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ConnectionOfOtherMediaRequired",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "OnlyHouseConnection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "OwnerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WorkmanContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DuctChanged",
                table: "FinishedJobs");

            migrationBuilder.RenameColumn(
                name: "ConnectBuildingFinishedDataModel_DuctChangedDescription",
                table: "FinishedJobs",
                newName: "DuctChangedDescription");

            migrationBuilder.RenameColumn(
                name: "ConnectBuildingFinishedDataModel_DuctChanged",
                table: "FinishedJobs",
                newName: "DuctChanged");

            migrationBuilder.RenameColumn(
                name: "DuctChangedDescription",
                table: "FinishedJobs",
                newName: "Blub");
        }
    }
}
