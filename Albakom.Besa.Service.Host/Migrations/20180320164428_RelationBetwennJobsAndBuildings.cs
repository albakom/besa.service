﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class RelationBetwennJobsAndBuildings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_Jobs_BranchOffJobId",
                table: "Buldings");

            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_Jobs_ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_BranchOffJobId",
                table: "Buldings");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                unique: true,
                filter: "[BuildingId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_BranchOffJobId",
                table: "Buldings",
                column: "BranchOffJobId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_ConnenctionJobId",
                table: "Buldings",
                column: "ConnenctionJobId");

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_Jobs_BranchOffJobId",
                table: "Buldings",
                column: "BranchOffJobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_Jobs_ConnenctionJobId",
                table: "Buldings",
                column: "ConnenctionJobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
