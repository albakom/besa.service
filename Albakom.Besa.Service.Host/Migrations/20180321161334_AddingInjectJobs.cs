﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class AddingInjectJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BranchOffJobId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "ConnenctionJobId",
                table: "Buldings");

            migrationBuilder.RenameColumn(
                name: "ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                newName: "InjectJobDataModel_BuildingId");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Jobs",
                newName: "ConnectBuildingJobDataModel_BuildingId");

            migrationBuilder.AddColumn<int>(
                name: "BuildingId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                unique: true,
                filter: "[BuildingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                unique: false,
                filter: "[ConnectBuildingJobDataModel_BuildingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_InjectJobDataModel_BuildingId",
                table: "Jobs",
                column: "InjectJobDataModel_BuildingId",
                unique: false,
                filter: "[InjectJobDataModel_BuildingId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs",
                column: "InjectJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_InjectJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "InjectJobDataModel_BuildingId",
                table: "Jobs",
                newName: "ConnectBuildingJobDataModel_BuildingId");

            migrationBuilder.RenameColumn(
                name: "ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                newName: "BuildingId");

            migrationBuilder.AddColumn<int>(
                name: "BranchOffJobId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConnenctionJobId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                unique: true,
                filter: "[BuildingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                unique: true,
                filter: "[ConnectBuildingJobDataModel_BuildingId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
