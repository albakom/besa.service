﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class TestRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs",
                column: "InjectJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_BuildingId",
                table: "Jobs",
                column: "BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ConnectBuildingJobDataModel_BuildingId",
                table: "Jobs",
                column: "ConnectBuildingJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_InjectJobDataModel_BuildingId",
                table: "Jobs",
                column: "InjectJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
