﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class UpdateBuildingCableInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "CustomerContactId",
                table: "Jobs",
                newName: "InjectJobDataModel_CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_InjectJobDataModel_CustomerContactId");

            migrationBuilder.AddColumn<int>(
                name: "CustomerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "ActualLength",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CableTypeId",
                table: "Buldings",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "ConnenctionLength",
                table: "Buldings",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "FiberCableTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdapterId = table.Column<string>(nullable: true),
                    FiberAmount = table.Column<int>(nullable: false),
                    FiberPerBundle = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FiberCableTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_CableTypeId",
                table: "Buldings",
                column: "CableTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Buldings_FiberCableTypes_CableTypeId",
                table: "Buldings",
                column: "CableTypeId",
                principalTable: "FiberCableTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "InjectJobDataModel_CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Buldings_FiberCableTypes_CableTypeId",
                table: "Buldings");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "FiberCableTypes");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_CableTypeId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ActualLength",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "CableTypeId",
                table: "Buldings");

            migrationBuilder.DropColumn(
                name: "ConnenctionLength",
                table: "Buldings");

            migrationBuilder.RenameColumn(
                name: "InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_CustomerContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
