﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class SpliceJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinishedAt",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "SpliceBranchableJobDataModel_CustomerContactId");

            migrationBuilder.RenameColumn(
                name: "CustomerContactId",
                table: "Jobs",
                newName: "InjectJobDataModel_CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_SpliceBranchableJobDataModel_CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_InjectJobDataModel_CustomerContactId");

            migrationBuilder.AddColumn<int>(
                name: "CustomerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchableId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FlatId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CableId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "CableMetric",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SpliceODFJobFinishedDataModel_CableMetric",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FiberCable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BranchableId = table.Column<int>(nullable: true),
                    BuildingId = table.Column<int>(nullable: true),
                    CableTypeId = table.Column<int>(nullable: false),
                    DuctColorName = table.Column<string>(nullable: true),
                    DuctHexValue = table.Column<string>(nullable: true),
                    Length = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ProjectAdapterId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FiberCable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FiberCable_Branchables_BranchableId",
                        column: x => x.BranchableId,
                        principalTable: "Branchables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FiberCable_Buldings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buldings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FiberCable_FiberCableTypes_CableTypeId",
                        column: x => x.CableTypeId,
                        principalTable: "FiberCableTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FiberSplices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstCableId = table.Column<int>(nullable: false),
                    FirstFiberNumber = table.Column<int>(nullable: false),
                    SecondCableId = table.Column<int>(nullable: false),
                    SecondFiberNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FiberSplices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FiberSplices_FiberCable_FirstCableId",
                        column: x => x.FirstCableId,
                        principalTable: "FiberCable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FiberSplices_FiberCable_SecondCableId",
                        column: x => x.SecondCableId,
                        principalTable: "FiberCable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_BranchableId",
                table: "Jobs",
                column: "BranchableId",
                unique: true,
                filter: "[BranchableId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs",
                column: "CableId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_BranchableId",
                table: "FiberCable",
                column: "BranchableId",
                unique: true,
                filter: "[BranchableId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_BuildingId",
                table: "FiberCable",
                column: "BuildingId",
                unique: true,
                filter: "[BuildingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_CableTypeId",
                table: "FiberCable",
                column: "CableTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_FirstCableId",
                table: "FiberSplices",
                column: "FirstCableId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_SecondCableId",
                table: "FiberSplices",
                column: "SecondCableId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "InjectJobDataModel_CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Branchables_BranchableId",
                table: "Jobs",
                column: "BranchableId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_SpliceBranchableJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "SpliceBranchableJobDataModel_CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs",
                column: "FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs",
                column: "CableId",
                principalTable: "FiberCable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Branchables_BranchableId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_SpliceBranchableJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "FiberSplices");

            migrationBuilder.DropTable(
                name: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_BranchableId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BranchableId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FlatId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CableId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CableMetric",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "SpliceODFJobFinishedDataModel_CableMetric",
                table: "FinishedJobs");

            migrationBuilder.RenameColumn(
                name: "SpliceBranchableJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "InjectJobDataModel_CustomerContactId");

            migrationBuilder.RenameColumn(
                name: "InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_SpliceBranchableJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_InjectJobDataModel_CustomerContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Jobs_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                newName: "IX_Jobs_CustomerContactId");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FinishedAt",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerContactId",
                table: "Jobs",
                column: "CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_InjectJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "InjectJobDataModel_CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
