﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class Procedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Procedures",
                columns: table => new
                {
                    Appointment = table.Column<DateTimeOffset>(nullable: true),
                    BuildingId = table.Column<int>(nullable: true),
                    DeclarationOfAggrementInStock = table.Column<bool>(nullable: true),
                    OwnerContactId = table.Column<int>(nullable: true),
                    CustomerConnectionProcedureDataModel_Appointment = table.Column<DateTimeOffset>(nullable: true),
                    ContractInStock = table.Column<bool>(nullable: true),
                    CustomerContactId = table.Column<int>(nullable: true),
                    CustomerConnectionProcedureDataModel_DeclarationOfAggrementInStock = table.Column<bool>(nullable: true),
                    FlatId = table.Column<int>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    ExpectedEnd = table.Column<int>(nullable: false),
                    IsFinished = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Start = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procedures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Procedures_Buldings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buldings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procedures_ContactInfos_OwnerContactId",
                        column: x => x.OwnerContactId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procedures_ContactInfos_CustomerContactId",
                        column: x => x.CustomerContactId,
                        principalTable: "ContactInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procedures_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ProcedureTimeLineElements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comment = table.Column<string>(nullable: true),
                    ProcedureId = table.Column<int>(nullable: false),
                    RelatedJobId = table.Column<int>(nullable: true),
                    RelatedRequestId = table.Column<int>(nullable: true),
                    State = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcedureTimeLineElements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcedureTimeLineElements_Procedures_ProcedureId",
                        column: x => x.ProcedureId,
                        principalTable: "Procedures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProcedureTimeLineElements_Jobs_RelatedJobId",
                        column: x => x.RelatedJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProcedureTimeLineElements_CustomerConnenctionRequests_RelatedRequestId",
                        column: x => x.RelatedRequestId,
                        principalTable: "CustomerConnenctionRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_BuildingId",
                table: "Procedures",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_OwnerContactId",
                table: "Procedures",
                column: "OwnerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_CustomerContactId",
                table: "Procedures",
                column: "CustomerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_FlatId",
                table: "Procedures",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedureTimeLineElements_ProcedureId",
                table: "ProcedureTimeLineElements",
                column: "ProcedureId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedureTimeLineElements_RelatedJobId",
                table: "ProcedureTimeLineElements",
                column: "RelatedJobId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedureTimeLineElements_RelatedRequestId",
                table: "ProcedureTimeLineElements",
                column: "RelatedRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProcedureTimeLineElements");

            migrationBuilder.DropTable(
                name: "Procedures");
        }
    }
}
