﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class ChangeRelationchipBetwennProceduresAndFlats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Procedures_FlatId",
                table: "Procedures");

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_FlatId",
                table: "Procedures",
                column: "FlatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Procedures_FlatId",
                table: "Procedures");

            migrationBuilder.CreateIndex(
                name: "IX_Procedures_FlatId",
                table: "Procedures",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");
        }
    }
}
