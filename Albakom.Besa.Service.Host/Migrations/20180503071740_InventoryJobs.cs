﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class InventoryJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Procedures_ContactInfos_OwnerContactId",
                table: "Procedures");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "FlatId",
                table: "Jobs",
                newName: "SpliceBranchableJobDataModel_FlatId");

            migrationBuilder.AddColumn<int>(
                name: "AddressRequirement",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeviceId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FlatId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "UnchagedIpAdress",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IssuerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "PickupAt",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "IventoryJobArticleRelations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<double>(nullable: false),
                    ArticleId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IventoryJobArticleRelations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IventoryJobArticleRelations_Article_ArticleId",
                        column: x => x.ArticleId,
                        principalTable: "Article",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IventoryJobArticleRelations_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_DeviceId",
                table: "Jobs",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_IssuerContactId",
                table: "Jobs",
                column: "IssuerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_SpliceBranchableJobDataModel_FlatId",
                table: "Jobs",
                column: "SpliceBranchableJobDataModel_FlatId",
                unique: true,
                filter: "[SpliceBranchableJobDataModel_FlatId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_IventoryJobArticleRelations_ArticleId",
                table: "IventoryJobArticleRelations",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_IventoryJobArticleRelations_JobId",
                table: "IventoryJobArticleRelations",
                column: "JobId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerId",
                table: "Jobs",
                column: "CustomerId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Article_DeviceId",
                table: "Jobs",
                column: "DeviceId",
                principalTable: "Article",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs",
                column: "FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_IssuerContactId",
                table: "Jobs",
                column: "IssuerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Flats_SpliceBranchableJobDataModel_FlatId",
                table: "Jobs",
                column: "SpliceBranchableJobDataModel_FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Procedures_ContactInfos_OwnerContactId",
                table: "Procedures",
                column: "OwnerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_CustomerId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Article_DeviceId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_IssuerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Flats_SpliceBranchableJobDataModel_FlatId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Procedures_ContactInfos_OwnerContactId",
                table: "Procedures");

            migrationBuilder.DropTable(
                name: "IventoryJobArticleRelations");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CustomerId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_DeviceId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_IssuerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_SpliceBranchableJobDataModel_FlatId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "AddressRequirement",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FlatId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "UnchagedIpAdress",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "IssuerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PickupAt",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "SpliceBranchableJobDataModel_FlatId",
                table: "Jobs",
                newName: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_FlatId",
                table: "Jobs",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Flats_FlatId",
                table: "Jobs",
                column: "FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Procedures_ContactInfos_OwnerContactId",
                table: "Procedures",
                column: "OwnerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
