﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class RelationODFSpliceJobCable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs",
                column: "CableId",
                unique: true,
                filter: "[CableId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs",
                column: "CableId",
                principalTable: "FiberCable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_CableId",
                table: "Jobs",
                column: "CableId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_FiberCable_CableId",
                table: "Jobs",
                column: "CableId",
                principalTable: "FiberCable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
