﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class DataFromAdapter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberSplices_FiberCable_FirstCableId",
                table: "FiberSplices");

            migrationBuilder.DropForeignKey(
                name: "FK_FiberSplices_FiberCable_SecondCableId",
                table: "FiberSplices");

            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_FirstCableId",
                table: "FiberSplices");

            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_SecondCableId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "FirstCableId",
                table: "FiberSplices");

            migrationBuilder.RenameColumn(
                name: "SecondCableId",
                table: "FiberSplices",
                newName: "TrayNumber");

            migrationBuilder.RenameColumn(
                name: "FirstFiberNumber",
                table: "FiberSplices",
                newName: "NumberInTray");

            migrationBuilder.AddColumn<int>(
                name: "FlatId",
                table: "FiberCable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StartingAdPopId",
                table: "FiberCable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PoPId",
                table: "Branchables",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FirstFiberId",
                table: "FiberSplices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectAdapterId",
                table: "FiberSplices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SecondFiberId",
                table: "FiberSplices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "FiberSplices",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "FiberConnenctions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Caption = table.Column<string>(nullable: true),
                    ColumnNumber = table.Column<string>(nullable: true),
                    FlatId = table.Column<int>(nullable: false),
                    ModuleName = table.Column<string>(nullable: true),
                    RowNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FiberConnenctions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FiberConnenctions_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Fibers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BundleNumber = table.Column<int>(nullable: false),
                    CableId = table.Column<int>(nullable: false),
                    Color = table.Column<string>(nullable: true),
                    FiberNumber = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NumberinBundle = table.Column<int>(nullable: false),
                    ProjectAdapterId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fibers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fibers_FiberCable_CableId",
                        column: x => x.CableId,
                        principalTable: "FiberCable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PoPs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ProjectAdapterId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoPs", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_FlatId",
                table: "FiberCable",
                column: "FlatId",
                unique: true,
                filter: "[FlatId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_StartingAdPopId",
                table: "FiberCable",
                column: "StartingAdPopId");

            migrationBuilder.CreateIndex(
                name: "IX_Branchables_PoPId",
                table: "Branchables",
                column: "PoPId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_FirstFiberId",
                table: "FiberSplices",
                column: "FirstFiberId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_SecondFiberId",
                table: "FiberSplices",
                column: "SecondFiberId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberConnenctions_FlatId",
                table: "FiberConnenctions",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_Fibers_CableId",
                table: "Fibers",
                column: "CableId");

            migrationBuilder.AddForeignKey(
                name: "FK_Branchables_PoPs_PoPId",
                table: "Branchables",
                column: "PoPId",
                principalTable: "PoPs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_Flats_FlatId",
                table: "FiberCable",
                column: "FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_PoPs_StartingAdPopId",
                table: "FiberCable",
                column: "StartingAdPopId",
                principalTable: "PoPs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FiberSplices_Fibers_FirstFiberId",
                table: "FiberSplices",
                column: "FirstFiberId",
                principalTable: "Fibers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FiberSplices_Fibers_SecondFiberId",
                table: "FiberSplices",
                column: "SecondFiberId",
                principalTable: "Fibers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branchables_PoPs_PoPId",
                table: "Branchables");

            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_Flats_FlatId",
                table: "FiberCable");

            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_PoPs_StartingAdPopId",
                table: "FiberCable");

            migrationBuilder.DropForeignKey(
                name: "FK_FiberSplices_Fibers_FirstFiberId",
                table: "FiberSplices");

            migrationBuilder.DropForeignKey(
                name: "FK_FiberSplices_Fibers_SecondFiberId",
                table: "FiberSplices");

            migrationBuilder.DropTable(
                name: "FiberConnenctions");

            migrationBuilder.DropTable(
                name: "Fibers");

            migrationBuilder.DropTable(
                name: "PoPs");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_FlatId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_StartingAdPopId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_Branchables_PoPId",
                table: "Branchables");

            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_FirstFiberId",
                table: "FiberSplices");

            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_SecondFiberId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "FlatId",
                table: "FiberCable");

            migrationBuilder.DropColumn(
                name: "StartingAdPopId",
                table: "FiberCable");

            migrationBuilder.DropColumn(
                name: "PoPId",
                table: "Branchables");

            migrationBuilder.DropColumn(
                name: "FirstFiberId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "ProjectAdapterId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "SecondFiberId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "FiberSplices");

            migrationBuilder.RenameColumn(
                name: "TrayNumber",
                table: "FiberSplices",
                newName: "SecondCableId");

            migrationBuilder.RenameColumn(
                name: "NumberInTray",
                table: "FiberSplices",
                newName: "FirstFiberNumber");

            migrationBuilder.AddColumn<int>(
                name: "FirstCableId",
                table: "FiberSplices",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_FirstCableId",
                table: "FiberSplices",
                column: "FirstCableId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_SecondCableId",
                table: "FiberSplices",
                column: "SecondCableId");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberSplices_FiberCable_FirstCableId",
                table: "FiberSplices",
                column: "FirstCableId",
                principalTable: "FiberCable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FiberSplices_FiberCable_SecondCableId",
                table: "FiberSplices",
                column: "SecondCableId",
                principalTable: "FiberCable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
