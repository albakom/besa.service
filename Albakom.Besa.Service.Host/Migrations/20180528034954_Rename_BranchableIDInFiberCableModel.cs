﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class Rename_BranchableIDInFiberCableModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_Branchables_BranchableId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_BranchableId",
                table: "FiberCable");

            migrationBuilder.RenameColumn(
                name: "BranchableId",
                table: "FiberCable",
                newName: "MainCableForBranchableId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_MainCableForBranchableId",
                table: "FiberCable",
                column: "MainCableForBranchableId",
                unique: true,
                filter: "[MainCableForBranchableId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_Branchables_MainCableForBranchableId",
                table: "FiberCable",
                column: "MainCableForBranchableId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_Branchables_MainCableForBranchableId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_MainCableForBranchableId",
                table: "FiberCable");

            migrationBuilder.RenameColumn(
                name: "MainCableForBranchableId",
                table: "FiberCable",
                newName: "BranchableId");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_BranchableId",
                table: "FiberCable",
                column: "BranchableId",
                unique: true,
                filter: "[BranchableId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_Branchables_BranchableId",
                table: "FiberCable",
                column: "BranchableId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
