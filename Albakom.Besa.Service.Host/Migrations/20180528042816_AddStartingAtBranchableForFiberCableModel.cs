﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class AddStartingAtBranchableForFiberCableModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StartingAtBranchableleId",
                table: "FiberCable",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_StartingAtBranchableleId",
                table: "FiberCable",
                column: "StartingAtBranchableleId");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_Branchables_StartingAtBranchableleId",
                table: "FiberCable",
                column: "StartingAtBranchableleId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_Branchables_StartingAtBranchableleId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_StartingAtBranchableleId",
                table: "FiberCable");

            migrationBuilder.DropColumn(
                name: "StartingAtBranchableleId",
                table: "FiberCable");
        }
    }
}
