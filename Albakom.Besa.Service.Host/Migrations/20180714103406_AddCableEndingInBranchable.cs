﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class AddCableEndingInBranchable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EndingAtBranchableId",
                table: "FiberCable",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_EndingAtBranchableId",
                table: "FiberCable",
                column: "EndingAtBranchableId");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberCable_Branchables_EndingAtBranchableId",
                table: "FiberCable",
                column: "EndingAtBranchableId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberCable_Branchables_EndingAtBranchableId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_EndingAtBranchableId",
                table: "FiberCable");

            migrationBuilder.DropColumn(
                name: "EndingAtBranchableId",
                table: "FiberCable");
        }
    }
}
