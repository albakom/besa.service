﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class AddBranchableIdToSpice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BranchableId",
                table: "FiberSplices",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_BranchableId",
                table: "FiberSplices",
                column: "BranchableId");

            migrationBuilder.AddForeignKey(
                name: "FK_FiberSplices_Branchables_BranchableId",
                table: "FiberSplices",
                column: "BranchableId",
                principalTable: "Branchables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FiberSplices_Branchables_BranchableId",
                table: "FiberSplices");

            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_BranchableId",
                table: "FiberSplices");

            migrationBuilder.DropColumn(
                name: "BranchableId",
                table: "FiberSplices");
        }
    }
}
