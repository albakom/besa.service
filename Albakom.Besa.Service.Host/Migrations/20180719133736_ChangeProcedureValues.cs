﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class ChangeProcedureValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "InformSalesAfterFinish",
                table: "Procedures",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ActivationAsSoonAsPossible",
                table: "Procedures",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RelatedProcedureId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CreatedProcedureId",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NoInterest",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ActivationAsSoonAsPossible",
                table: "CustomerConnenctionRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "InformSalesAfterFinish",
                table: "CustomerConnenctionRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_CustomerContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_RelatedProcedureId",
                table: "Jobs",
                column: "RelatedProcedureId");

            migrationBuilder.CreateIndex(
                name: "IX_FinishedJobs_CreatedProcedureId",
                table: "FinishedJobs",
                column: "CreatedProcedureId");

            migrationBuilder.AddForeignKey(
                name: "FK_FinishedJobs_Procedures_CreatedProcedureId",
                table: "FinishedJobs",
                column: "CreatedProcedureId",
                principalTable: "Procedures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Buldings_ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_BuildingId",
                principalTable: "Buldings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ContactInfos_ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_CustomerContactId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Flats_ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs",
                column: "ContactAfterFinishedJobDataModel_FlatId",
                principalTable: "Flats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Procedures_RelatedProcedureId",
                table: "Jobs",
                column: "RelatedProcedureId",
                principalTable: "Procedures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinishedJobs_Procedures_CreatedProcedureId",
                table: "FinishedJobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Buldings_ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ContactInfos_ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Flats_ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Procedures_RelatedProcedureId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_RelatedProcedureId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_FinishedJobs_CreatedProcedureId",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "InformSalesAfterFinish",
                table: "Procedures");

            migrationBuilder.DropColumn(
                name: "ActivationAsSoonAsPossible",
                table: "Procedures");

            migrationBuilder.DropColumn(
                name: "ContactAfterFinishedJobDataModel_BuildingId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ContactAfterFinishedJobDataModel_CustomerContactId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ContactAfterFinishedJobDataModel_FlatId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Notes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RelatedProcedureId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CreatedProcedureId",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "NoInterest",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "ActivationAsSoonAsPossible",
                table: "CustomerConnenctionRequests");

            migrationBuilder.DropColumn(
                name: "InformSalesAfterFinish",
                table: "CustomerConnenctionRequests");
        }
    }
}
