﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class UpdateTasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinishedJobs_Procedures_CreatedProcedureId",
                table: "FinishedJobs");

            migrationBuilder.DropColumn(
                name: "NoInterest",
                table: "FinishedJobs");

            migrationBuilder.RenameColumn(
                name: "CreatedProcedureId",
                table: "FinishedJobs",
                newName: "CreatedRequestId");

            migrationBuilder.RenameIndex(
                name: "IX_FinishedJobs_CreatedProcedureId",
                table: "FinishedJobs",
                newName: "IX_FinishedJobs_CreatedRequestId");

            migrationBuilder.CreateTable(
                name: "UpdateTasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartedByUserId = table.Column<int>(nullable: false),
                    Task = table.Column<int>(nullable: false),
                    Started = table.Column<DateTimeOffset>(nullable: false),
                    Ended = table.Column<DateTimeOffset>(nullable: true),
                    Canceled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpdateTasks_Users_StartedByUserId",
                        column: x => x.StartedByUserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpdateTaskElements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TaskId = table.Column<int>(nullable: false),
                    ObjectId = table.Column<int>(nullable: false),
                    AdapterId = table.Column<string>(nullable: true),
                    Started = table.Column<DateTimeOffset>(nullable: true),
                    Ended = table.Column<DateTimeOffset>(nullable: true),
                    State = table.Column<int>(nullable: false),
                    Result = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateTaskElements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpdateTaskElements_UpdateTasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "UpdateTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UpdateTaskElements_TaskId",
                table: "UpdateTaskElements",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_UpdateTasks_StartedByUserId",
                table: "UpdateTasks",
                column: "StartedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FinishedJobs_CustomerConnenctionRequests_CreatedRequestId",
                table: "FinishedJobs",
                column: "CreatedRequestId",
                principalTable: "CustomerConnenctionRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinishedJobs_CustomerConnenctionRequests_CreatedRequestId",
                table: "FinishedJobs");

            migrationBuilder.DropTable(
                name: "UpdateTaskElements");

            migrationBuilder.DropTable(
                name: "UpdateTasks");

            migrationBuilder.RenameColumn(
                name: "CreatedRequestId",
                table: "FinishedJobs",
                newName: "CreatedProcedureId");

            migrationBuilder.RenameIndex(
                name: "IX_FinishedJobs_CreatedRequestId",
                table: "FinishedJobs",
                newName: "IX_FinishedJobs_CreatedProcedureId");

            migrationBuilder.AddColumn<bool>(
                name: "NoInterest",
                table: "FinishedJobs",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FinishedJobs_Procedures_CreatedProcedureId",
                table: "FinishedJobs",
                column: "CreatedProcedureId",
                principalTable: "Procedures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
