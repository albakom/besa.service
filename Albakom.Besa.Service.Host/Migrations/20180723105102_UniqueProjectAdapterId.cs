﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class UniqueProjectAdapterId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "FiberSplices",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "Fibers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AdapterId",
                table: "FiberCableTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "FiberCable",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FiberSplices_ProjectAdapterId",
                table: "FiberSplices",
                column: "ProjectAdapterId",
                unique: true,
                filter: "[ProjectAdapterId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Fibers_ProjectAdapterId",
                table: "Fibers",
                column: "ProjectAdapterId",
                unique: true,
                filter: "[ProjectAdapterId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCableTypes_AdapterId",
                table: "FiberCableTypes",
                column: "AdapterId",
                unique: true,
                filter: "[AdapterId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FiberCable_ProjectAdapterId",
                table: "FiberCable",
                column: "ProjectAdapterId",
                unique: true,
                filter: "[ProjectAdapterId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Buldings_ProjectId",
                table: "Buldings",
                column: "ProjectId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Branchables_ProjectId",
                table: "Branchables",
                column: "ProjectId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FiberSplices_ProjectAdapterId",
                table: "FiberSplices");

            migrationBuilder.DropIndex(
                name: "IX_Fibers_ProjectAdapterId",
                table: "Fibers");

            migrationBuilder.DropIndex(
                name: "IX_FiberCableTypes_AdapterId",
                table: "FiberCableTypes");

            migrationBuilder.DropIndex(
                name: "IX_FiberCable_ProjectAdapterId",
                table: "FiberCable");

            migrationBuilder.DropIndex(
                name: "IX_Buldings_ProjectId",
                table: "Buldings");

            migrationBuilder.DropIndex(
                name: "IX_Branchables_ProjectId",
                table: "Branchables");

            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "FiberSplices",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "Fibers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AdapterId",
                table: "FiberCableTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProjectAdapterId",
                table: "FiberCable",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
