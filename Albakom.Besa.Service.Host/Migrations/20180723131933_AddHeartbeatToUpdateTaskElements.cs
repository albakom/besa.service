﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Albakom.Besa.Service.Host.Migrations
{
    public partial class AddHeartbeatToUpdateTaskElements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Heartbeat",
                table: "UpdateTasks",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Heartbeat",
                table: "UpdateTasks");
        }
    }
}
