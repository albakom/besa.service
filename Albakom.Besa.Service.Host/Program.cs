﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Albakom.Besa.Service.Host
{
    public class Program
    {
        static async Task RunAsync(IServiceProvider services)
        {
            ICompanyService companyService = services.GetService<ICompanyService>();
            ISeedService seedSvc = services.GetService<ISeedService>();

            if ( (await companyService.GetAllCompanies()).Count() == 0)
            {
               await seedSvc.SeedCompanies(30);
            }

            //await seedSvc.SeedContacts(1000);
            //await seedSvc.SeedContactAfterFinsiehdJobs(1000,/*HierAuthIdEinfügen*/"b8472e76-8ae9-4a7a-b9e3-a4cdc2b9e39e");
        }

        public static void Main(string[] args)
        {
            IWebHost host = BuildWebHost(args);
            using (IServiceScope scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;

                try
                {
                    RunAsync(services).Wait();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
