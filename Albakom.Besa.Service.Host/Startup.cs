﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Host.Hubs;
using Albakom.Besa.Service.Host.Infrastructure;
using Albakom.Besa.Service.Implementation.Services;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;

namespace Albakom.Besa.Service.Host
{
    public class Startup
    {
        #region Consts

        public const String BesaAdminRoleAccessPolicyName = "BesaAdminRole";
        public const String ProjectManagerRoleAccessPolicyName = "ProjectManagerRole";
        public const String CivilWorkerOrProjectManagerRoleAccessPolicyName = "CivilWorkerOrProjectManagerRole";
        public const String SalesOrProjectManagerRoleAccessPolicyName = "SalesOrProjectManagerRole";
        public const String InjectOrProjectManagerRoleAccessPolicyName = "InjectOrProjectManagerRole";
        public const String SplicerOrProjectManagerRoleAccessPolicyName = "SplicerOrProjectManagerRoleAccessRole";
        public const String ActiveNetworkOrProjectManagerRoleAccessPolicyName = "ActiveNetworkOrProjectManagerRole";
        public const String SalesRoleAccessPolicyName = "SalesRole";
        public const String InventoryRoleAccessPolicyName = "InventoryRole";
        public const String CrafterOrProjectManagerRoleAccessPolicyName = "CrafterOrProjectManagerRole";


        #endregion

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            TokenClientConfig tokenClientOptions = Configuration.GetSection("TokenClient").Get<TokenClientConfig>();
            ProjectAdapterOptions projectAdapterOptions = Configuration.GetSection("ProjectAdapter").Get<ProjectAdapterOptions>();

            services.AddDbContext<BesaDataStorage>(options => options.UseSqlServer(Configuration.GetConnectionString("ServiceContext"), (b) => b.MigrationsAssembly(typeof(Startup).Assembly.GetName().Name)));

            services.AddScoped<IBesaDataStorage, BesaDataStorage>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddScoped<IAuthorizationHandler, BesaRoleHandler>();
            services.AddTransient<IClaimsExtractor, ClaimsExtractor>((activator) => new ClaimsExtractor("sub", activator.GetService<ILogger<ClaimsExtractor>>()));
            services.AddTransient<ISeedService, SeedService>();
            services.AddTransient<IJobService, JobService>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IProcedureService, ProcedureService>();
            services.AddTransient<IImportService, ImportService>();
            services.AddTransient<IArticleManagementService, ArticleManagementService>();
            services.AddTransient<IAccessRequestNotifier, HubBasedAccessRequestNotifier>();
            services.AddTransient<IConstructionStageService, ConstructionStageService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IProtocolService, ProtocolService>();
            services.AddTransient<IExcelFileGeneratorService, ExcelFileGeneratorService>();
            services.AddTransient<IUpdateTaskService, UpdateTaskService>();
            services.AddTransient<IUpdateTaskNotifier, HubBasedUpdateTaskNotifier>();
            services.AddTransient<IMessageNotifier, HubBasedMessageNotifier>();
            services.AddTransient<IMessageSendEngine, MessageSendEngine>();
            services.AddTransient<IEmailSendEngine, EMailSendEngine>();
            services.AddTransient<ISMSSendEngine, SMSSendEngine>();
            services.AddTransient<IPushSendEngine, PushSendEngine>();
            services.AddTransient<IMessageService, MessageService>();

            services.AddTransient<IFileManager, AzureFileManager>((activator) => new AzureFileManager(activator.GetService<ILogger<AzureFileManager>>(), Configuration.GetConnectionString("AzureStorage")));

            services.AddTransient<IHttpHandler, HttpClientBasedHttpHandler>();

            services.AddTransient<IProjectAdapter, HttpBasedProjectAdapter>(activator =>
               new HttpBasedProjectAdapter(projectAdapterOptions.ServiceUrl,
                   activator.GetService<IHttpHandler>(),
                   activator.GetService<ITokenClient>(),
                   activator.GetService<ILogger<HttpBasedProjectAdapter>>()
                   ));
            services.AddTransient<ITokenClient, SimpleTokenClient>(activator =>
                new SimpleTokenClient(tokenClientOptions)
                 );

            services.AddAuthorization(options =>
            {
                options.AddPolicy(BesaAdminRoleAccessPolicyName, policy =>
                    policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.Admin)));

                options.AddPolicy(ProjectManagerRoleAccessPolicyName, policy =>
    policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.ProjectManager)));

                options.AddPolicy(SalesRoleAccessPolicyName, policy =>
policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.Sales)));

                options.AddPolicy(InjectOrProjectManagerRoleAccessPolicyName, policy =>
                     policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.AirInjector, BesaRoles.ProjectManager)));

                options.AddPolicy(CivilWorkerOrProjectManagerRoleAccessPolicyName, policy =>
                     policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.CivilWorker, BesaRoles.ProjectManager)));

                options.AddPolicy(SalesOrProjectManagerRoleAccessPolicyName, policy =>
     policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.Sales, BesaRoles.ProjectManager)));

                options.AddPolicy(SplicerOrProjectManagerRoleAccessPolicyName, policy =>
     policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.Splice, BesaRoles.ProjectManager)));

                options.AddPolicy(ActiveNetworkOrProjectManagerRoleAccessPolicyName, policy =>
    policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.ActiveNetwork, BesaRoles.ProjectManager)));

                options.AddPolicy(CrafterOrProjectManagerRoleAccessPolicyName, policy =>
policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.CivilWorker, BesaRoles.AirInjector, BesaRoles.Splice, BesaRoles.ActiveNetwork, BesaRoles.ProjectManager)));

                options.AddPolicy(InventoryRoleAccessPolicyName, policy =>
                policy.Requirements.Add(new BesaRoleRequirement(BesaRoles.Inventory)));

            });

            services.AddCors();

            services.AddMvc();

            services.AddSignalR((configure) =>
            {
            });

            AuthOptions authOptions = Configuration.GetSection("AuthOptons").Get<AuthOptions>();

            services.AddAuthentication("Bearer")
            .AddIdentityServerAuthentication(options =>
            {
                options.Authority = authOptions.AuthUri;
                options.RequireHttpsMetadata = false;

                options.ApiName = "api1";

            });
            services.AddAuthentication().AddBasicAuthentication(
            options =>
            {
                options.Realm = "My Application";
                options.Events = new BasicAuthenticationEvents
                {
                    OnValidatePrincipal = context =>
                    {
                        if ((context.UserName == "admin") && (context.Password == "Albakom2019!"))
                        {
                            var claims = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer),
                                new Claim(ClaimTypes.Role, "hangfire-admin", context.Options.ClaimsIssuer),

                            };

                            var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, BasicAuthenticationDefaults.AuthenticationScheme));
                            context.Principal = principal;
                        }
                        else
                        {
                            // optional with following default.
                            // context.AuthenticationFailMessage = "Authentication failed."; 
                        }

                        return Task.CompletedTask;
                    }
                };
            });

            services.AddHangfire(x =>
#if DEBUG
            //x.UseSqlServerStorage(Configuration.GetConnectionString("HangfireContext"))
            x.UseMemoryStorage()
#else
            x.UseSqlServerStorage(Configuration.GetConnectionString("HangfireContext"))
#endif

            );

            // services.AddTransient<SignalRQueryStringAuthMiddleware>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
    IHostingEnvironment env,
    ILoggerFactory loggerFactory,
    IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            GlobalConfiguration.Configuration
    .UseActivator(new ContainerJobActivator(serviceProvider));

            app.UseCors((option) =>
            {
                option.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials().WithExposedHeaders("Content-Disposition");
            });

            app.UseSignalRQueryStringAuth();

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<RequestAccessHub>("/access");
                routes.MapHub<UpdateTaskHub>("/updateTasks");
                routes.MapHub<MessageHub>("/messages");


            });

            app.UseMvc();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                AppPath = null,
                Authorization = new List<IDashboardAuthorizationFilter> { new BasicHangfireDashboardAuthorizationFilter() },
            });

            app.UseHangfireServer();

            StartJobs(app);
        }

        private void StartJobs(IApplicationBuilder app)
        {
            BackgroundJob.Enqueue<IConstructionStageService>((service) => service.CleanupCables());

            BackgroundJob.Enqueue<IConstructionStageService>((service) => service.SetCableToFlatsWithOneBuildingUnit());
            BackgroundJob.Enqueue<IConstructionStageService>((service) => service.AddFlatsToBuildingWithOneUnit());

            BackgroundJob.Enqueue<IProcedureService>((service) => service.FinishProceduresThatShouldAlreadyFinished());
            BackgroundJob.Enqueue<IProtocolService>((service) => service.RestoreProtocolNotifications());

            RecurringJob.AddOrUpdate<IFileService>((service) => service.RemoveOrphansAccessEntries(), Cron.HourInterval(1));
            RecurringJob.AddOrUpdate<IUpdateTaskService>((service) => service.RestoreLockedUpdateTaskElements(), Cron.MinuteInterval(4));
            RecurringJob.AddOrUpdate<IUpdateTaskService>((service) => service.UnlockedTaskWithDeadlock(), Cron.MinuteInterval(5));
            
        }
    }
}
