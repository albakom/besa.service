﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Implementation.Helper
{
    public class MimeTypeRelationModel
    {
        #region Properties

        public String Extention { get; set; }
        public String Name { get; set; }
        public String MimeType { get; set; }
        public FileTypes Type { get; set; }

        #endregion

        #region Constructor

        public MimeTypeRelationModel()
        {

        }

        #endregion

    }
}
