﻿using Albakom.Besa.Service.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Implementation.Helper
{
    class ProtocolUserInformModelComparer : IEqualityComparer<ProtocolUserInformModel>
    {
        public bool Equals(ProtocolUserInformModel x, ProtocolUserInformModel y)
        {
            if(x == null || y == null) { return false; }

            return x.Action == y.Action && x.RelatedType == y.RelatedType;
        }

        public int GetHashCode(ProtocolUserInformModel obj)
        {
            if(obj == null) { return -1; }
            Int64 result = obj.Action.GetHashCode() + obj.RelatedType.GetHashCode();
            return result.GetHashCode();
        }
    }
}
