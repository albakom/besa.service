﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ArticleManagementService : IArticleManagementService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly ILogger<ArticleManagementService> _logger;
        private readonly IProtocolService _protocolService;

        #endregion

        #region Constructor

        public ArticleManagementService(IBesaDataStorage storage, ILogger<ArticleManagementService> logger, IProtocolService protocolService)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        #endregion

        #region Methods

        private async Task CheckIfArticleExists(Int32 articleId)
        {
            _logger.LogTrace("check if article with id {0} exists", articleId);
            if (await _storage.CheckIfArticleExists(articleId) == false)
            {
                _logger.LogInformation("article with id {0} not exists", articleId);
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.NotFound);
            }
        }

        private async Task ValidateArticle(IArticle article, Int32? existingId,String userAuthId)
        {
            _logger.LogTrace("ValidateArticle");

            if (article == null)
            {
                _logger.LogInformation("no model was given to validate");
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.NoData);
            }

            if (String.IsNullOrEmpty(article.Name) == true || article.Name.Length > _storage.Constraints.MaxArticleNameChars)
            {
                _logger.LogInformation("model is invalid");
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.InvalidData);
            }

            if(existingId.HasValue == true)
            {
                await CheckIfArticleExists(existingId.Value);
            }

            _logger.LogTrace("check if name exists");
            
            if (await _storage.CheckIfArticleNameExists(article.Name, existingId) == true)
            {
                _logger.LogInformation("article with name {0} already exists", article.Name);
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.InvalidData);
            }

            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if(await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.UserNotFound);
            }
        }

        public async Task<ArticleOverviewModel> CreateArticle(ArticleCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateArticle");

            await ValidateArticle(model, null,userAuthId);

            Int32 id = await _storage.CreateArticle(model);
            ArticleOverviewModel result = await _storage.GetArticleOverviewById(id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create,MessageRelatedObjectTypes.Article,id), userAuthId);

            return result;
        }


        public async Task<Boolean> DeleteArticle(Int32 articleId, Boolean force, String userAuthId)
        {
            _logger.LogTrace("DeleteArticle. articleId: {0}. force {1}");

            await CheckIfArticleExists(articleId);

            _logger.LogTrace("check if article with id {0} is in use", articleId);
            Boolean articleIsInUse = await _storage.CheckIfArticleIsInUse(articleId);
            _logger.LogTrace("articel with id {0} is in use: {1}", articleId, articleIsInUse);

            if (articleIsInUse == true && force == false)
            {
                _logger.LogInformation("trying to delete a article in use without force delete. article id: {0}", articleId);
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.DeleteNeedToBeForced);
            }

            Boolean result = await _storage.DeleteArticle(articleId);

            if(result == true)
            {
                _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
                if (await _storage.CheckIfUserExists(userAuthId) == false)
                {
                    _logger.LogInformation("user with auth id {0} not found", userAuthId);
                    throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.UserNotFound);
                }

                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Delete, MessageRelatedObjectTypes.Article, articleId), userAuthId);
            }

            return result;
        }

        public async Task<ArticleOverviewModel> EditArticle(ArticleEditModel model, String userAuthId)
        {
            _logger.LogTrace("EditArticle");

            if(model == null)
            {
                _logger.LogInformation("no model was given to validate");
                throw new ArticleManagementServicException(ArticleManagementServicExceptionReasons.NoData);
            }

            await ValidateArticle(model, model.Id,userAuthId);

            await _storage.EditArticle(model);
            ArticleOverviewModel result = await _storage.GetArticleOverviewById(model.Id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Update, MessageRelatedObjectTypes.Article, model.Id), userAuthId);

            return result;
        }

        public async Task<IEnumerable<ArticleOverviewModel>> GetAllArticles()
        {
            _logger.LogTrace("GetAllArticles");

            IEnumerable<ArticleOverviewModel> result = await _storage.GetAllArticles();
            return result;
        }

        public async Task<ArticleDetailModel> GetDetails(Int32 articleId)
        {
            _logger.LogTrace("GetDetails. articleId: {0}", articleId);
            await CheckIfArticleExists(articleId);

            ArticleDetailModel result = await _storage.GetArticleDetails(articleId);
            return result;
        }

        public async  Task<Boolean> CheckIfArticleIsInUse(Int32 articleId)
        {
            _logger.LogTrace("CheckIfArticleIsInUse. articleId: {0}", articleId);
            await CheckIfArticleExists(articleId);

            Boolean result = await _storage.CheckIfArticleIsInUse(articleId);
            _logger.LogTrace("result: {0}", result);
            return result;
        }

        #endregion
    }
}
