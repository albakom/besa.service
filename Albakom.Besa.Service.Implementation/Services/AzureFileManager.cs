﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class AzureFileManager : IFileManager
    {
        #region Fields and consts

        private readonly ILogger<AzureFileManager> _logger;
        private readonly string storageConnectionString;

        private const String _generellContainerName = "generell";

        #endregion

        #region Constructor

        public AzureFileManager(ILogger<AzureFileManager> logger, String storageConnectionString)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.storageConnectionString = storageConnectionString ?? throw new ArgumentNullException(nameof(storageConnectionString));
        }

        #endregion

        #region Methods

        public async Task<String> Upload(FileCreateModel model, Stream data, String containerName)
        {
            if (String.IsNullOrEmpty(containerName) == true)
            {
                containerName = _generellContainerName;
            }

            CloudStorageAccount account = CloudStorageAccount.Parse(storageConnectionString);
            CloudBlobClient serviceClient = account.CreateCloudBlobClient();

            CloudBlobContainer container = serviceClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync();

            String fileName = $"{model.Name}.{model.Extention}";

            CloudBlockBlob blobData = container.GetBlockBlobReference(fileName);
            Int32 fileAppendingNumber = 1;
            while(await blobData.ExistsAsync() == true)
            {
                fileAppendingNumber += 1;
                fileName = $"{model.Name}-{fileAppendingNumber}.{model.Extention}";
                blobData = container.GetBlockBlobReference(fileName);
            }

            await blobData.UploadFromStreamAsync(data);

            String url = blobData.Uri.ToString();
            return url;
        }

        public async Task<Stream> Download(String url, Int32 size)
        {
            CloudStorageAccount account = CloudStorageAccount.Parse(storageConnectionString);
            CloudBlobClient serviceClient = account.CreateCloudBlobClient();

            Uri uri = new Uri(url);
            try
            {
                ICloudBlob blob = await serviceClient.GetBlobReferenceFromServerAsync(uri);
                CloudBlockBlob blobData = (CloudBlockBlob)blob;

                MemoryStream stream = new MemoryStream(size);
                await blobData.DownloadToStreamAsync(stream);
                return stream;
            }
            catch (Exception)
            {
                return Stream.Null;
            }
        }

        public async Task Delete(IEnumerable<String> urls)
        {
            CloudStorageAccount account = CloudStorageAccount.Parse(storageConnectionString);
            CloudBlobClient serviceClient = account.CreateCloudBlobClient();

            foreach (String url in urls)
            {
                Uri uri = new Uri(url);
                try
                {
                    ICloudBlob blob = await serviceClient.GetBlobReferenceFromServerAsync(uri);
                    await blob.DeleteIfExistsAsync();
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        #endregion
    }
}
