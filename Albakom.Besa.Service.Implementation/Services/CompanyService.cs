﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class CompanyService : ICompanyService
    {
        #region Fields

        private IBesaDataStorage _storage;
        private ILogger<CompanyService> _logger;
        private readonly IProtocolService _protocolService;

        #endregion

        #region Constructor

        public CompanyService(
            IBesaDataStorage storage,
            ILogger<CompanyService> logger,
            IProtocolService protocolService)
        {
            _storage = storage;
            _logger = logger;
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        #endregion

        #region Methods

        private async Task CheckUserAuthId(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.UserNotFound);
            }
        }

        private async Task ValidateCompany(ICompanyModel model, ComponyServiceExceptionReasons nullReason, ComponyServiceExceptionReasons invalidReason, Int32? companyID, String userAuthId)
        {
            _logger.LogDebug("ValidateCompany");

            if (model == null)
            {
                _logger.LogInformation("model is null");
                throw new CompanyServiceException(nullReason);
            }

            if (model.Address == null)
            {
                _logger.LogInformation("no address specified");
                throw new CompanyServiceException(invalidReason);
            }

            if (
                String.IsNullOrEmpty(model.Name) == true || model.Name.Length > _storage.Constraints.MaxCompanyNameChars ||
                String.IsNullOrEmpty(model.Phone) == true || model.Name.Length > _storage.Constraints.MaxCompanyPhoneChars ||
                String.IsNullOrEmpty(model.Address.City) == true || model.Address.City.Length > _storage.Constraints.MaxAddressCityChars ||
                String.IsNullOrEmpty(model.Address.Street) == true || model.Address.Street.Length > _storage.Constraints.MaxAddressStreetChars ||
                String.IsNullOrEmpty(model.Address.StreetNumber) == true || model.Address.StreetNumber.Length > _storage.Constraints.MaxAddressStreetNumberChars ||
                String.IsNullOrEmpty(model.Address.PostalCode) == true || model.Address.PostalCode.Length > _storage.Constraints.MaxAddressZipCodeChars || model.Address.PostalCode.Length < _storage.Constraints.MinAddressZipCodeChars
                )
            {
                _logger.LogInformation("CompanyCreateModel is invalid");
                throw new CompanyServiceException(invalidReason);
            }

            _logger.LogDebug("check if company name {0} is in use ", model.Name);
            if (companyID.HasValue == false)
            {
                if (await _storage.CheckIfCompanyNameExists(model.Name) == true)
                {
                    _logger.LogInformation("company name {0} already exists found", model.Name);
                    throw new CompanyServiceException(invalidReason);
                }
            }
            else
            {
                if (await _storage.CheckIfCompanyNameExists(model.Name, companyID.Value) == true)
                {
                    _logger.LogInformation("company name {0} already exists found", model.Name);
                    throw new CompanyServiceException(invalidReason);
                }
            }

            await CheckUserAuthId(userAuthId);
        }

        public async Task<CompanyOverviewModel> CreateCompany(CompanyCreateModel company, String userAuthId)
        {
            _logger.LogDebug("CreateCompany");
            await ValidateCompany(company, ComponyServiceExceptionReasons.CreateModelIsNull, ComponyServiceExceptionReasons.InvalidEditModel, null, userAuthId);

            Int32 id = await _storage.CreateCompany(company);
            _logger.LogDebug("company created with id {0}", id);
            CompanyOverviewModel result = await _storage.GetCompanyOverviewById(id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.Company, id), userAuthId);

            return result;
        }

        public async Task<IEnumerable<CompanyOverviewModel>> GetAllCompanies()
        {
            _logger.LogDebug("GetAllCompanies");
            IEnumerable<CompanyOverviewModel> result = await _storage.GetAllCompanies();
            _logger.LogInformation("company amount: {0}", result.Count());
            return result;
        }

        public async Task<Boolean> RemoveEmployee(Int32 companyId, Int32 employeeId, String userAuthId)
        {
            _logger.LogDebug("RemoveEmployee. companyId: {0}, employeeId {1}", companyId, employeeId);

            _logger.LogTrace("checking if company with id {0} exists", companyId);
            if (await _storage.CheckIfCompanyExists(companyId) == false)
            {
                _logger.LogInformation("company with id {0} not found", companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            };

            _logger.LogTrace("checking if user with id {0} exists", employeeId);
            if (await _storage.CheckIfUserExists(employeeId) == false)
            {
                _logger.LogInformation("user with id {0} not found", employeeId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            };

            _logger.LogTrace("checking if user {0} belongs to company with id {1} exists", employeeId, companyId);
            if (await _storage.CheckIfUserBelongsToCompany(companyId, employeeId) == false)
            {
                _logger.LogInformation("user with id {0} is not a member of companie with id {1}", employeeId, companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.UserNotCompanyMember);
            }

            await CheckUserAuthId(userAuthId);

            Boolean result = await _storage.RemoveEmployeeFromCompany(employeeId);
            _logger.LogInformation("remove result is: {0}", result);

            if (result == true)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.RemoveEmployeeToCompany, MessageRelatedObjectTypes.Company, companyId), userAuthId);
            }

            return result;
        }

        public async Task<Boolean> AddEmployee(Int32 companyId, Int32 employeeId, String userAuthId)
        {
            _logger.LogDebug("AddEmployee. companyId: {0}, employeeId {1}", companyId, employeeId);

            _logger.LogTrace("checking if company with id {0} exists", companyId);
            if (await _storage.CheckIfCompanyExists(companyId) == false)
            {
                _logger.LogInformation("company with id {0} not found", companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            }

            _logger.LogTrace("checking if user with id {0} exists", employeeId);
            if (await _storage.CheckIfUserExists(employeeId) == false)
            {
                _logger.LogInformation("user with id {0} not found", employeeId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            }

            _logger.LogTrace("checking if user {0} belongs to company with id {1} exists", employeeId, companyId);
            if (await _storage.CheckIfUserBelongsToCompany(companyId, employeeId) == true)
            {
                _logger.LogInformation("user with id {0} is already a member of companie with id {1}", employeeId, companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.UserAlreadyMember);
            }

            await CheckUserAuthId(userAuthId);

            Boolean result = await _storage.AddEmployeeToCompany(companyId, employeeId);
            _logger.LogInformation("adding result is: {0}", result);

            if (result == true)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Company, companyId), userAuthId);
            }

            return result;
        }

        public async Task<CompanyOverviewModel> EditCompany(CompanyEditModel company, String userAuthId)
        {
            _logger.LogDebug("EditCompany");
            await ValidateCompany(company, ComponyServiceExceptionReasons.EditModelIsNull, ComponyServiceExceptionReasons.InvalidEditModel, company.Id, userAuthId);

            _logger.LogInformation("check if company with id {0} exists", company.Id);
            if (await _storage.CheckIfCompanyExists(company.Id) == false) { throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound); }

            _logger.LogInformation("check if ids for user are valid");
            if (company.AddedEmployees == null) { company.AddedEmployees = new List<Int32>(); }
            if (company.RemovedEmployees == null) { company.RemovedEmployees = new List<Int32>(); }

            Int32 expectedAmount = company.AddedEmployees.Count() + company.RemovedEmployees.Count();
            List<Int32> userIdsToCheck = company.AddedEmployees.Union(company.RemovedEmployees).ToList();

            if (expectedAmount != userIdsToCheck.Count) { throw new CompanyServiceException(ComponyServiceExceptionReasons.InvalidEditModel); }
            if (userIdsToCheck.Count != 0)
            {
                _logger.LogTrace("check if user ids exists");
                if (await _storage.CheckIfUsersExists(userIdsToCheck) == false)
                {
                    _logger.LogInformation("user not existing");
                    throw new CompanyServiceException(ComponyServiceExceptionReasons.InvalidEditModel);
                }

                if (company.AddedEmployees.Count() > 0)
                {
                    _logger.LogInformation("added {0} employees to company with id {1}", company.AddedEmployees.Count(), company.Id);
                    await _storage.AddEmployeesToCompany(company.AddedEmployees, company.Id);
                }
                if (company.RemovedEmployees.Count() > 0)
                {
                    _logger.LogTrace("checking if user does't belong to company");
                    if (await _storage.CheckIfUsersBelongsToCompany(company.Id, company.RemovedEmployees) == false)
                    {
                        _logger.LogInformation("not all users belongs to company");
                        throw new CompanyServiceException(ComponyServiceExceptionReasons.UserNotCompanyMember);
                    };

                    _logger.LogInformation("removed {0} employees to company with id {1}", company.AddedEmployees.Count(), company.Id);
                    await _storage.RemoveEmployeesFromCompany(company.RemovedEmployees);
                }
            }

            await _storage.EditCompany(company);
            CompanyOverviewModel result = await _storage.GetCompanyOverviewById(company.Id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Update, MessageRelatedObjectTypes.Company, company.Id), userAuthId);

            return result;
        }

        public async Task<Boolean> DeleteCompany(Int32 companyId, String userAuthId)
        {
            _logger.LogDebug("DeleteCompany. companyId {0}", companyId);

            _logger.LogTrace("check if company with id {0} exists");
            if (await _storage.CheckIfCompanyExists(companyId) == false)
            {
                _logger.LogInformation("company with id {0} not found", companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            }

            await CheckUserAuthId(userAuthId);

            Boolean result = await _storage.DeleteCompany(companyId);
            _logger.LogInformation("delete result is: {0}", result);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId), userAuthId);

            return result;
        }

        public async Task<CompanyDetailModel> GetCompanyDetails(Int32 companyId)
        {
            _logger.LogTrace("GetCompanyDetails. companyId {0}", companyId);

            _logger.LogTrace("check if company with id {0}, exists", companyId);
            if (await _storage.CheckIfCompanyExists(companyId) == false)
            {
                _logger.LogInformation("company with id {0} not found", companyId);
                throw new CompanyServiceException(ComponyServiceExceptionReasons.NotFound);
            }

            CompanyDetailModel result = await _storage.GetCompanyDetails(companyId);
            return result;
        }


        #endregion
    }
}
