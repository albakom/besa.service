﻿using Albakom.Besa.Service.Contracts;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ConstructionStageService : IConstructionStageService
    {
        #region Fields

        private readonly ILogger<ConstructionStageService> _logger;
        private readonly IProtocolService _protocolService;
        private readonly IUpdateTaskService _updateService;
        private readonly IBesaDataStorage _storage;
        private readonly IProjectAdapter _projectAdapter;

        #endregion

        #region Constructor

        public ConstructionStageService(
    IBesaDataStorage storage,
    IProjectAdapter projectAdapter,
    ILogger<ConstructionStageService> logger,
    IProtocolService protocolService,
    IUpdateTaskService updateService
    )
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._projectAdapter = projectAdapter ?? throw new ArgumentNullException(nameof(projectAdapter));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
            this._updateService = updateService ?? throw new ArgumentNullException(nameof(updateService));
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<ConstructionStageOverviewModel>> GetAllConstructionStages()
        {
            _logger.LogTrace("GetAllConstructionStages()");

            IEnumerable<ConstructionStageOverviewModel> result = await _storage.GetAllConstructionStages();
            _logger.LogTrace("{0} construction stages found", result.Count());
            return result;
        }

        private async Task ValidateConstructionStageName(String name, Int32? currentId, ConstructionStageServicExceptionReasons reason)
        {
            if (String.IsNullOrEmpty(name) == true || name.Length > _storage.Constraints.MaxLengthOfConstructionStageName)
            {
                _logger.LogInformation("model is invalid");
                throw new ConstructionStageServicException(reason);
            }

            if (await _storage.CheckIfConstructionStageNameExists(name, currentId) == true)
            {
                _logger.LogInformation("a construction stage with name {0} already exists", name);
                throw new ConstructionStageServicException(reason);
            }
        }

        private async Task CheckUserAuthId(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.UserNotFound);
            }
        }

        private async Task ValidateConstructionStage(ConstructionStageCreateModel model, String userAuthId)
        {
            _logger.LogTrace("ValidateConstructionStage");
            if (model == null)
            {
                _logger.LogInformation("no model was given");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.ModelIsNull);
            }

            await ValidateConstructionStageName(model.Name, null, ConstructionStageServicExceptionReasons.InvalidCreateModel);
            await CheckUserAuthId(userAuthId);
        }

        public async Task<ConstructionStageOverviewModel> CreateConstructionStage(ConstructionStageCreateModel model, String userAuthId)
        {
            await ValidateConstructionStage(model, userAuthId);

            model.Cabinets = model.Cabinets ?? new List<Int32>();
            model.Sleeves = model.Sleeves ?? new List<Int32>();

            if (await _storage.CheckIfCabinetsExists(model.Cabinets) == false)
            {
                _logger.LogInformation("cabinets ids not found");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidCreateModel);
            }

            if (await _storage.CheckIfSleevesExists(model.Sleeves) == false)
            {
                _logger.LogInformation("sleeves ids not found");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidCreateModel);
            }

            Int32 id = await _storage.CreateConstructionStage(model);
            _logger.LogTrace("construction side created with id {0}", id);

            ConstructionStageOverviewModel result = await _storage.GetConstructionStageById(id);

            if (model.CreateJobs == true)
            {
                await CreateBranchOffJob(result.Id, userAuthId);
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.ConstructionStage, id), userAuthId);

            return result;
        }

        public async Task<Boolean> DeleteConstructionStage(Int32 constructionStageId, String userAuthId)
        {
            _logger.LogTrace("DeleteConstructionStage. constructionStageId: {0}", constructionStageId);
            if (await _storage.CheckIfConstructionStageExists(constructionStageId) == false)
            {
                _logger.LogInformation("construction stage with id {0} not found", constructionStageId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            Boolean result = await _storage.DeleteConstructionStage(constructionStageId);
            _logger.LogTrace("Delete construction stage result is {0}", result);

            await CheckUserAuthId(userAuthId);
            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Delete, MessageRelatedObjectTypes.ConstructionStage, constructionStageId), userAuthId);

            return result;
        }

        public async Task<GetDataFromAdapterResult> GetDataFromAdapter(Boolean forceUpdate, String userAuthId)
        {
            _logger.LogTrace("GetDataFromAdapter. ProjectAdapter: {0}. ForceUpdate {1}", _projectAdapter.GetType().ToString(), forceUpdate);

            await CheckUserAuthId(userAuthId);

            if (forceUpdate == false)
            {
                Int32 currentAmount = await _storage.GetBuildingAmount();
                if (currentAmount > 0)
                {
                    await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateFromAdapter, MessageRelatedObjectTypes.NotSpecified, new Int32?(), ProtocolLevels.Error), userAuthId);

                    _logger.LogInformation("add building already exists");
                    throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.DataUpdateInvalid);
                }

                ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> cabinetResult = await _projectAdapter.GetAllCabinets();
                ServiceResult<IEnumerable<ProjectAdapterSleeveModel>> sleeveResult = await _projectAdapter.GetAllSleeves();

                Boolean errorOccured = cabinetResult.HasError || sleeveResult.HasError;
                if (errorOccured == true)
                {
                    await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateFromAdapter, ProtocolLevels.Error), userAuthId);

                    _logger.LogInformation("error gettting data from adapter");
                    throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidDataFromAdapter);
                }

                Int32 cabinetAmount = await _storage.CreateCabinets(cabinetResult.Data);
                Int32 sleeveAmount = await _storage.CreateSleeves(sleeveResult.Data);

                Int32 buildingAmount = cabinetResult.Data.Sum(x => x.Buildings.Count());

                GetDataFromAdapterResult result = new GetDataFromAdapterResult
                {
                    BuildingAmount = buildingAmount,
                    CabinentAmount = cabinetAmount,
                    SleevesAmount = sleeveAmount,
                };

                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateFromAdapter, MessageRelatedObjectTypes.NotSpecified, new Int32?(), ProtocolLevels.Sucesss), userAuthId);

                return result;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<SimpleCabinetOverviewModel>> SearchCabinets(String query, Int32? amount)
        {
            _logger.LogTrace("SearchCabinets. Query: {0}. amount: {1}", query, amount);
            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogTrace("no query given");
                return new List<SimpleCabinetOverviewModel>();
            }

            IEnumerable<SimpleCabinetOverviewModel> result = await _storage.SearchCabinets(query, amount);
            _logger.LogTrace("{0} results found", result.Count());
            return result;
        }

        public async Task<IEnumerable<SimpleSleeveOverviewModel>> SearchSleeves(String query, Int32? amount)
        {
            _logger.LogTrace("SearchCabinets. Query: {0}. amount: {1}", query, amount);
            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogTrace("no query given");
                return new List<SimpleSleeveOverviewModel>();
            }

            IEnumerable<SimpleSleeveOverviewModel> result = await _storage.SearchSleeves(query, amount);
            _logger.LogTrace("{0} results found", result.Count());
            return result;
        }

        public async Task<IEnumerable<SimpleBuildingOverviewModel>> SearchStreet(String query, Int32? amount)
        {
            _logger.LogTrace("SearchCabinets. Query: {0}. amount: {1}", query, amount);
            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogTrace("no query given");
                return new List<SimpleBuildingOverviewModel>();
            }

            IEnumerable<SimpleBuildingOverviewModel> result = await _storage.SearchStreets(query, amount);
            _logger.LogTrace("{0} results found", result.Count());
            return result;
        }


        public async Task<IEnumerable<SimpleBuildingOverviewModel>> GetBuildingsByBranchable(Int32 branchableId)
        {
            _logger.LogTrace("GetBuildingsByBranchable. branchableId: {0}. amount: {1}", branchableId);

            _logger.LogTrace("Check if branchbable exits with id {0}", branchableId);
            if (await _storage.CheckIfBranchableExists(branchableId) == false)
            {
                _logger.LogInformation("branchbable with id {0} not found", branchableId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            IEnumerable<SimpleBuildingOverviewModel> result = await _storage.GetBuildingsByBranchable(branchableId);
            return result;
        }

        public async Task CreateBranchOffJob(Int32 constructionStageId, String userAuthId)
        {
            _logger.LogTrace("CreateBranchOffJob. constructionStageId: {0}", constructionStageId);
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("check if construction stage with id {0} exists", constructionStageId);
            if (await _storage.CheckIfConstructionStageExists(constructionStageId) == false)
            {
                _logger.LogInformation("construction stage with id {0} not found", constructionStageId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("get buildings without branch off job");
            IEnumerable<Int32> buildingIdsWithoutBranchOffJob = await _storage.GetBuildingIdsWithoutBranchOffJob(constructionStageId);

            _logger.LogTrace("creating job models");
            List<BranchOffJobCreateModel> jobsToCreate = new List<BranchOffJobCreateModel>();
            foreach (Int32 buildingId in buildingIdsWithoutBranchOffJob)
            {
                BranchOffJobCreateModel job = new BranchOffJobCreateModel { BuildingId = buildingId, };
                jobsToCreate.Add(job);
            }

            _logger.LogTrace("saving jobs");
            await _storage.CreateBranchOffJobs(jobsToCreate);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.CreateBranchOffJobs, MessageRelatedObjectTypes.BranchOffJob, new Int32?(), ProtocolLevels.Sucesss), userAuthId);
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForCilivWork(Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetOverviewForCilivWork.onlyUnboundedJob: {0}", onlyUnboundedJob);

            IEnumerable<ConstructionStageOverviewForJobs> result = await _storage.GetConstructionStageOverviewForCilivWork(onlyUnboundedJob);
            return result;
        }

        private async Task CheckIfConstructionStageExists(Int32 constructionStageId)
        {
            _logger.LogTrace("check if construction stage with id {0} exists", constructionStageId);
            if (await _storage.CheckIfConstructionStageExists(constructionStageId) == false)
            {
                _logger.LogInformation("construction stage with id {0} not found", constructionStageId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }
        }

        public async Task<ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview>> GetCivilWorkDetail(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetCivilWorkDetail. constructionStageId: {0}", constructionStageId);
            await CheckIfConstructionStageExists(constructionStageId);

            ConstructionStageDetailModel<SimpleBuildingConnenctionJobOverview> result = await _storage.GetConstructionStageDetailsForCivilWork(constructionStageId, onlyUnboundedJob);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageForProjectManagementOverviewModel>> GetOverviewForProjectManagement()
        {
            _logger.LogTrace("GetOverviewForProjectManagement()");

            IEnumerable<ConstructionStageForProjectManagementOverviewModel> result = await _storage.GetConstructionStageOverviewForProjectManagement();
            return result;
        }


        public async Task<IEnumerable<Int32>> AddFlatsToBuildingWithOneUnit()
        {
            _logger.LogTrace("AddFlatsToBuildingWithOneUnit");

            _logger.LogTrace("Get building with one unit and without flats");
            IEnumerable<SimpleBuildingOverviewModel> buildingIds = await _storage.GetBuildingsWithOneUnitAndWithoutFlats();

            if (buildingIds.Count() == 0)
            {
                _logger.LogInformation("no building was found");
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.AddFlatsToBuildingWithOneUnit, ProtocolLevels.Warning));
                return new List<Int32>();
            }

            _logger.LogTrace("generetating flat models");
            List<FlatCreateModel> flatsToCreate = buildingIds.Select(x => new FlatCreateModel
            {
                BuildingId = x.Id,
                Description = "Standard-Wohnung",
                Floor = "Erdgeschoss",
                Number = x.StreetName,
            }).ToList();

            _logger.LogTrace("creating flats");
            IEnumerable<Int32> flatIds = await _storage.CreateFlats(flatsToCreate);
            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.AddFlatsToBuildingWithOneUnit, ProtocolLevels.Sucesss));
            return flatIds;
        }

        public async Task<SimpleFlatOverviewModel> AddFlatToBuilding(FlatCreateModel flat, String userAuthId)
        {
            _logger.LogTrace("AddFlatToBuilding");

            if (flat == null)
            {
                _logger.LogInformation("no flat model was given");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.ModelIsNull);
            }

            _logger.LogTrace("check if flat model ist valid");
            if (String.IsNullOrEmpty(flat.Number) == true || flat.Number.Length > _storage.Constraints.MaxFlatNumberChars
                || (String.IsNullOrEmpty(flat.Description) == false && flat.Description.Length > _storage.Constraints.MaxFlatDescriptionChars)
                || (String.IsNullOrEmpty(flat.Floor) == false && flat.Floor.Length > _storage.Constraints.MaxFlatFloorChars)
                )
            {
                _logger.LogInformation("flat model is invalid");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidCreateModel);
            }

            _logger.LogTrace("checking if building with id {0} exists", flat.BuildingId);
            if (await _storage.CheckIfBuildingExists(flat.BuildingId) == false)
            {
                _logger.LogInformation("building with id {0} not found", flat.BuildingId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidCreateModel);
            }

            if (await _storage.CheckIfBuildingHasSpaceForAFlat(flat.BuildingId) == false)
            {
                _logger.LogInformation("unable to add a new flat to bulding with id {0}. units are full");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidCreateModel);
            }

            await CheckUserAuthId(userAuthId);

            Int32 flatId = await _storage.CreateFlat(flat);
            SimpleFlatOverviewModel result = await _storage.GetFlatOverviewById(flatId);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId, ProtocolLevels.Sucesss), userAuthId);

            return result;
        }

        public async Task<IEnumerable<SimpleFlatOverviewModel>> GetFlatsByBuildingId(Int32 buildingId)
        {
            _logger.LogTrace("GetFlatsByBuildingId. buildingId: {0}", buildingId);

            _logger.LogTrace("check if building with id {0} exists", buildingId);
            if (await _storage.CheckIfBuildingExists(buildingId) == false)
            {
                _logger.LogInformation("building with id {0} not found");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            IEnumerable<SimpleFlatOverviewModel> result = await _storage.GetFlatsByBuildingId(buildingId);
            return result;
        }

        public async Task<Boolean> CheckIfBuildingIsConnected(Int32 buildingId)
        {
            _logger.LogTrace("CheckIfBuildingIsConnected. buildingId: {0}", buildingId);

            _logger.LogTrace("check if building exists");
            if (await _storage.CheckIfBuildingExists(buildingId) == false)
            {
                _logger.LogInformation("building with id {0} not found");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            Boolean result = await _storage.CheckIfBuildingConnenctionIsFinished(buildingId);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForInject(Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetOverviewForInject. onlyUnboundedJob: {0}", onlyUnboundedJob);

            IEnumerable<ConstructionStageOverviewForJobs> result = await _storage.GetConstructionStageOverviewForInject(onlyUnboundedJob);
            return result;
        }

        public async Task<ConstructionStageDetailModel<InjectJobOverviewModel>> GetInjectDetails(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetInjectDetails. constructionStageId: {0}", constructionStageId);
            await CheckIfConstructionStageExists(constructionStageId);

            ConstructionStageDetailModel<InjectJobOverviewModel> result = await _storage.GetConstructionStageDetailsForInject(constructionStageId, onlyUnboundedJob);
            return result;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForPrepareBranchable(Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetOverviewForPrepareBranchable. onlyUnboundedJob: {0}", onlyUnboundedJob);

            IEnumerable<ConstructionStageOverviewForJobs> result = await _storage.GetConstructionStageOverviewForPrepareBranchable(onlyUnboundedJob);
            return result;
        }

        public async Task<ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel>> GetPrepareBranchableDetails(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetPrepareBranchableDetails. constructionStageId: {0}", constructionStageId);
            await CheckIfConstructionStageExists(constructionStageId);

            ConstructionStageDetailModel<PrepareBranchableForSpliceJobOverviewModel> result = await _storage.GetConstructionStageDetailsForPrepareBranchable(constructionStageId, onlyUnboundedJob);
            return result;
        }

        public async Task<IEnumerable<ProjectAdapterCableTypeModel>> UpdateAllCableTypesFromAdapter(String userAuthId)
        {
            _logger.LogTrace("UpdateAllCableTypesFromAdapter");

            await CheckUserAuthId(userAuthId);

            ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>> cableTypeResult = await _projectAdapter.GetAllCableTypes();
            if (cableTypeResult.HasError == true)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateAllCableTypesFromAdapter, MessageRelatedObjectTypes.FiberCableType, new Int32?(), ProtocolLevels.Error), userAuthId);

                _logger.LogInformation("error getting data from adapter. err {0}", cableTypeResult.Error);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidDataFromAdapter);
            }

            IEnumerable<ProjectAdapterCableTypeModel> result = cableTypeResult.Data;

            ICollection<String> cableTypeProjectIds = await _storage.GetAllCableProjectAdapterIds();
            foreach (ProjectAdapterCableTypeModel item in result)
            {
                if (cableTypeProjectIds.Contains(item.AdapterId) == true)
                {
                    await _storage.UpdateCableType(item);
                    cableTypeProjectIds.Remove(item.AdapterId);
                }
                else
                {
                    await _storage.AddCableType(item);
                }
            }

            if (cableTypeProjectIds.Count > 0)
            {
                await _storage.DeleteCableTypes(cableTypeProjectIds);
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateAllCableTypesFromAdapter, MessageRelatedObjectTypes.FiberCableType, new Int32?(), ProtocolLevels.Sucesss), userAuthId);

            return result;
        }

        public async Task<Boolean> UpdateBuildingCableInfoFromAdapter(String userAuthId)
        {
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("UpdateBuildingCableInfoFromAdapter");

            _logger.LogTrace("getting project adapter building ids");
            IEnumerable<String> buildingProjectAdapterIds = await _storage.GetBuildingProjectAdapterIds();

            Boolean errorOccured = false;
            foreach (String id in buildingProjectAdapterIds)
            {
                ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await _projectAdapter.GetBuildingCableInfo(id);
                if (result.HasError == true || result.Data.Length <= 0)
                {
                    errorOccured = true;
                    continue;
                }

                await _storage.UpdateBuildingWithCableInfo(result.Data);
            }

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (errorOccured == true)
            {
                level = ProtocolLevels.Error;
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateBuildingCableInfoFromAdapter, MessageRelatedObjectTypes.Buildings, new Int32?(), level), userAuthId);

            return errorOccured;
        }

        public async Task<Boolean> UpdateBuildingNameFromAdapter(String userAuthId)
        {
            _logger.LogTrace("UpdateBuildingNameFromAdapter");

            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("getting project adapter building ids");
            IEnumerable<String> buildingProjectAdapterIds = await _storage.GetBuildingProjectAdapterIds();

            Boolean errorOccured = false;
            foreach (String id in buildingProjectAdapterIds)
            {
                ServiceResult<ProjectAdapterUpdateBuildingNameModel> result = await _projectAdapter.GetBuildingNameUpdate(id);
                if (result.HasError == true || String.IsNullOrEmpty(result.Data.Name) == true)
                {
                    errorOccured = true;
                    continue;
                }

                await _storage.UpdateBuildingName(result.Data);
            }

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (errorOccured == true)
            {
                level = ProtocolLevels.Error;
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateBuildingNameFromAdapter, MessageRelatedObjectTypes.Buildings, new Int32?(), level), userAuthId);

            return errorOccured;
        }

        public async Task<IEnumerable<ConstructionStageOverviewForJobs>> GetOverviewForSplice(Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetOverviewForInject. onlyUnboundedJob: {0}", onlyUnboundedJob);

            IEnumerable<ConstructionStageOverviewForJobs> result = await _storage.GetConstructionStageOverviewForSplice(onlyUnboundedJob);
            return result;
        }

        public async Task<ConstructionStageDetailModel<SpliceBranchableJobOverviewModel>> GetSpliceDetails(Int32 constructionStageId, Boolean onlyUnboundedJob)
        {
            _logger.LogTrace("GetInjectDetails. constructionStageId: {0}", constructionStageId);
            await CheckIfConstructionStageExists(constructionStageId);

            ConstructionStageDetailModel<SpliceBranchableJobOverviewModel> result = await _storage.GetConstructionStageDetailsForSplice(constructionStageId, onlyUnboundedJob);
            return result;
        }

        public async Task UpdateBuildigsFromAdapter(String userAuthId)
        {
            _logger.LogInformation("UpdateBuildigsFromAdapter");
            await CheckUserAuthId(userAuthId);

            _logger.LogInformation("get branchables from adapter");

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> cabinetResult = await _projectAdapter.GetAllCabinets();
            ServiceResult<IEnumerable<ProjectAdapterSleeveModel>> sleeveResult = await _projectAdapter.GetAllSleeves();

            Boolean errorOccured = cabinetResult.HasError || sleeveResult.HasError;
            if (errorOccured == true)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateBuildigsFromAdapter, MessageRelatedObjectTypes.Buildings, new Int32?(), ProtocolLevels.Error), userAuthId);

                _logger.LogInformation("error gettting data from adapter");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidDataFromAdapter);
            }

            _logger.LogInformation("get existing buildings");
            IDictionary<String, Int32> buldingsInDb = await _storage.GetBuildingIdsAndProjectAdapterIds();
            _logger.LogInformation("{0} buildings in db", buldingsInDb.Count);

            _logger.LogInformation("get existing branchables");
            IDictionary<String, Int32> branchableIds = await _storage.GetBranchablesIdsAndProjectAdapterIds();

            List<ProjectAdapterBranchableModel> projectBranchables = new List<ProjectAdapterBranchableModel>(cabinetResult.Data);
            projectBranchables.AddRange(sleeveResult.Data);

            Dictionary<String, Int32> buildingIdsToDelete = new Dictionary<String, Int32>(buldingsInDb);

            List<String> errorMessages = new List<string>();

            foreach (ProjectAdapterBranchableModel item in projectBranchables)
            {
                if (item.Name == null) { item.Name = String.Empty; }

                _logger.LogInformation("check if branchable exits");
                if (branchableIds.ContainsKey(item.AdapterId) == false)
                {
                    Int32 createdId = 0;
                    if (item is ProjectAdapterCabinetModel)
                    {
                        createdId = await _storage.CreateCabinet(item as ProjectAdapterCabinetModel);
                    }
                    else if (item is ProjectAdapterSleeveModel)
                    {
                        createdId = await _storage.CreateSleeve(item as ProjectAdapterSleeveModel);
                    }
                    else
                    {
                        throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.InvalidOperation);
                    }

                    branchableIds.Add(item.AdapterId, createdId);
                }
                else
                {
                    await _storage.UpdateBranchableFromAdapter(branchableIds[item.AdapterId], item);
                }

                Int32 branchableId = branchableIds[item.AdapterId];

                _logger.LogInformation("start updating building");
                List<ProjectAdapterBuildingModel> newBuildings = new List<ProjectAdapterBuildingModel>();
                foreach (ProjectAdapterBuildingModel building in item.Buildings)
                {
                    _logger.LogInformation("check if building with adapter id {0} is in database", building.AdapterId);
                    Int32 buildingId = 1;
                    if (buldingsInDb.ContainsKey(building.AdapterId) == true)
                    {
                        buildingId = buldingsInDb[building.AdapterId];

                        _logger.LogInformation("building with adaper id {0} found in database. Updating values", building.AdapterId);
                        Boolean branchableChanged = await _storage.UpdateBuildingFromAdapter(buildingId, building, branchableId);

                        buildingIdsToDelete.Remove(building.AdapterId);
                    }
                    else
                    {
                        buildingId = await _storage.CreateBuilding(building, branchableId);

                        buldingsInDb.Add(building.AdapterId, buildingId);
                        buildingIdsToDelete.Remove(building.AdapterId);
                    }

                    ServiceResult<ProjectAdapterUpdateBuldingCableModel> cableInfoResult = await _projectAdapter.GetBuildingCableInfo(building.AdapterId);
                    if (cableInfoResult.HasError == true)
                    {
                        _logger.LogError("unable to get cable info for building with id {0} from adapter. Reason: {1}", buildingId, cableInfoResult.Error);
                    }
                    else
                    {
                        try
                        {
                            await _storage.UpdateBuildingWithCableInfo(cableInfoResult.Data);
                        }
                        catch (Exception ex)
                        {
                            _storage.Restore();
                            _logger.LogError("unable to update cable info for building with id {0}. Exception: {1}", buildingId, ex.ToString());
                            errorMessages.Add(ex.ToString());
                        }
                    }
                }

                branchableIds.Remove(item.AdapterId);
            }

            foreach (KeyValuePair<String, Int32> item in buildingIdsToDelete)
            {
                ///TODO optional erst die Jobs laden diese dann einzeln löschen (Benachrichtigung) 
                try
                {
                    await _storage.DeleteBuilding(item.Value);
                }
                catch (Exception ex)
                {
                    _storage.Restore();
                    _logger.LogError("unable to delete building with id {0}. Exception: {1}", item.Value, ex.ToString());
                    errorMessages.Add(ex.ToString());

                }
            }

            foreach (KeyValuePair<String, Int32> item in branchableIds)
            {
                try
                {
                    await _storage.DeleteBranchable(item.Value);
                }
                catch (Exception ex)
                {
                    _storage.Restore();
                    _logger.LogError("unable to delete branchable with id {0}. Exception: {1}", item.Value, ex.ToString());
                    errorMessages.Add(ex.ToString());

                }
            }

            if (errorMessages.Count > 0)
            {
                String protocolMessage = String.Empty;
                foreach (String item in errorMessages)
                {
                    protocolMessage += item + "\n\r";
                }
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateBuildigsFromAdapter, MessageRelatedObjectTypes.Buildings, new Int32?(), ProtocolLevels.Warning), userAuthId);
            }
            else
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateBuildigsFromAdapter, MessageRelatedObjectTypes.Buildings, new Int32?(), ProtocolLevels.Sucesss), userAuthId);
            }
        }

        public async Task<IEnumerable<MainCableOverviewModel>> GetMainCables()
        {
            _logger.LogTrace("GetMainCables");
            IEnumerable<MainCableOverviewModel> result = await _storage.GetMainCableWithJobAssocatiation();
            return result;
        }

        public async Task<Boolean> UpdatePopInformation(String userAuthId)
        {
            _logger.LogTrace("UpdatePopInformation");
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("getting pops in database");
            IDictionary<String, Int32> popsInDatabase = await _storage.GetPopsWithProjectAdapterId();
            await _updateService.GetOpenElements(UpdateTasks.PoPs, userAuthId);

            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> popsResult = await _projectAdapter.GetAllPoPs();
            if (popsResult.HasError == true || popsResult.Data.Count() <= 0)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdatePopInformation, MessageRelatedObjectTypes.PoP, new Int32?(), ProtocolLevels.Error), userAuthId);

                _logger.LogInformation("unable to get informationen from adapter");
                return false;
            }

            foreach (ProjectAdapterPoPModel popFromAdapter in popsResult.Data)
            {
                _logger.LogTrace("check if pop with project adapter id {0} exist in databse", popFromAdapter.ProjectAdapterId);
                Int32 popId = 0;
                if (popsInDatabase.ContainsKey(popFromAdapter.ProjectAdapterId) == true)
                {
                    popId = popsInDatabase[popFromAdapter.ProjectAdapterId];
                    _logger.LogTrace("pop with project adapter id {0} found in database. id is {1}", popFromAdapter.ProjectAdapterId, popId);
                    _logger.LogTrace("updating pop");
                    await _storage.UpdatePoPFromAdapter(popId, popFromAdapter);

                    popsInDatabase.Remove(popFromAdapter.ProjectAdapterId);
                }
                else
                {
                    _logger.LogTrace("new pop from adapter. AdapterID {0}. Creating databse entry", popFromAdapter.ProjectAdapterId);
                    popId = await _storage.AddPoPFromAdapter(popFromAdapter);
                    _logger.LogTrace("pop created with id {0}", popId);
                }

                if (popFromAdapter.Branchables != null)
                {
                    foreach (String branchableAdapterId in popFromAdapter.Branchables)
                    {
                        _logger.LogTrace("check if branchable with adapter Id {0} exists", branchableAdapterId);
                        if (await _storage.CheckIfBranchableExistsByAdapterId(branchableAdapterId) == false)
                        {
                            _logger.LogInformation("branchable with adapter id {0} not found", branchableAdapterId);
                            continue;
                        }

                        _logger.LogTrace("branchable with adapter Id {0} exists.Updating Pop", branchableAdapterId);
                        await _storage.UpdatePoPIdFromBranchable(branchableAdapterId, popId);
                    }
                }
            }

            foreach (Int32 popId in popsInDatabase.Values)
            {
                await _storage.DeletePopById(popId);
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdatePopInformation, MessageRelatedObjectTypes.PoP, new Int32?(), ProtocolLevels.Sucesss), userAuthId);
            await _updateService.FinishTask(UpdateTasks.PoPs);

            return true;
        }

        public async Task<Boolean> UpdateCablesFromAdapter(String userAuthId)
        {
            _logger.LogInformation("UpdateCablesFromAdapter");

            Boolean accessGranted = await WaitForUpdateTaskAccess(UpdateTasks.Cables, userAuthId);
            if (accessGranted == false) { return false; }

            _logger.LogTrace("get stored branchables");
            IEnumerable<UpdateTaskElementModel> branchables = await _updateService.GetOpenElements(UpdateTasks.Cables, userAuthId);
            Int32 changes = 0;

            ICollection<String> storedCables = await _storage.GetAllCableProjectAdapterIds();
            Dictionary<String, Int32> cableCount = storedCables.ToDictionary(x => x, x => 0);

            foreach (UpdateTaskElementModel item in branchables)
            {
                UpdateTaskResultModel taskResultModel = new UpdateTaskResultModel();
                try
                {
                    await _updateService.StartUpdateTaskElement(item.Id);

                    ServiceResult<IEnumerable<ProjectAdapterCableModel>> cableResult = await _projectAdapter.GetCableByBranchableId(item.AdapterId);
                    if (cableResult.HasError == true || cableResult.Data.Count() == 0)
                    {
                        String logText = $"unable to load cables from adapter with branchable id {item.ObjectId}, Reason: {cableResult.Error}";
                        _logger.LogInformation(logText);

                        taskResultModel.Warning = logText;
                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Waring });

                        continue;
                    }

                    _logger.LogTrace("get stored cable from branchable with id {0}", item.ObjectId);
                    IDictionary<String, Int32> cableFromBranchable = await _storage.GetCableFromBranchbaleWithProjectAdapterId(item.ObjectId);

                    foreach (ProjectAdapterCableModel cableItem in cableResult.Data)
                    {
                        _logger.LogTrace("check if cable type with project adapter id {0} exists", cableItem.ProjectAdapterId);
                        if (await _storage.CheckIfCableTypeExistsByProjectAdapterId(cableItem.ProjectAdapterCableTypeId) == false)
                        {
                            _logger.LogInformation("cable type with project adater id {0} not found", cableItem.ProjectAdapterId);
                        }

                        Int32 cableTypeId = await _storage.GetCableTypeIdByProjectAdapterId(cableItem.ProjectAdapterCableTypeId);

                        _logger.LogTrace("check if cable with project adapter id {0} exist in database", cableItem.ProjectAdapterId);
                        Int32 cableId = 0;
                        if (cableFromBranchable.ContainsKey(cableItem.ProjectAdapterId) == true)
                        {
                            cableId = cableFromBranchable[cableItem.ProjectAdapterId];
                            _logger.LogTrace("cable with project adapter id {0} found in database. id is {1}", cableItem.ProjectAdapterId, cableId);
                            _logger.LogTrace("updating pop");
                            await _storage.UpdateCableFromAdapter(cableId, cableItem, item.ObjectId, cableTypeId);

                            cableCount[cableItem.ProjectAdapterId] += 1;

                            taskResultModel.UpdatedIds.Add(cableId);
                        }
                        else
                        {
                            if (storedCables.Contains(cableItem.ProjectAdapterId) == true)
                            {
                                await _storage.SetBranachableToCable(cableItem.ProjectAdapterId, item.ObjectId);
                                _logger.LogTrace("cable already added to database");
                                continue;
                            }

                            _logger.LogTrace("new cable from adapter. Adapter id {0}. Creating databse entry", cableItem.ProjectAdapterId);
                            cableId = await _storage.AddCableFromAdapter(cableItem, item.ObjectId, cableTypeId);
                            _logger.LogTrace("cable created with id {0}", cableId);

                            cableFromBranchable.Add(cableItem.ProjectAdapterId, cableId);
                            storedCables.Add(cableItem.ProjectAdapterId);

                            taskResultModel.AddedIds.Add(cableId);
                        }

                        if (String.IsNullOrEmpty(cableItem.ConnectedBuildingAdapterId) == false)
                        {
                            _logger.LogTrace("check if building with project adapter id {0} exists", cableItem.ConnectedBuildingAdapterId);
                            if (await _storage.CheckIfBuildingExistsByProjectAdapterId(cableItem.ConnectedBuildingAdapterId) == false)
                            {
                                _logger.LogTrace("building with project adapter id {0} not found in database", cableItem.ConnectedBuildingAdapterId);
                                continue;
                            }

                            Int32 buildingId = await _storage.GetBuildingIdByProjectAdapterId(cableItem.ConnectedBuildingAdapterId);
                            await _storage.UpdateBuildingCableRelation(cableId, buildingId);
                        }
                    }

                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Success });
                    changes += 1;
                }
                catch (Exception ex)
                {
                    _storage.Restore();

                    String errorText = $"an error happend during update patch connection for cable with id {item.ObjectId}. Reason: {ex.ToString()}";
                    _logger.LogError(errorText);

                    taskResultModel.Error = errorText;
                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Error });
                    continue;
                }
            }

            foreach (String cableProjectAdapterId in cableCount.Where(x => x.Value == 0).Select(x => x.Key))
            {
                try
                {
                    await _storage.DeleteCable(cableProjectAdapterId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "unable to delete cable with adapter id {0}", cableProjectAdapterId);
                    _storage.Restore();

                    continue;
                }
            }

            await CleanupCables();

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (changes > 0)
            {
                level = ProtocolLevels.Warning;
                await SetCableToFlatsWithOneBuildingUnit();
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateCablesFromAdapter, MessageRelatedObjectTypes.FiberCable, new Int32?(), level), userAuthId);
            await _updateService.FinishTask(UpdateTasks.Cables);

            return changes > 0;
        }

        public async Task<Boolean> UpdateMainCables(String userAuthId)
        {
            _logger.LogInformation("UpdateMainCables");
            await CheckUserAuthId(userAuthId);

            Boolean accessGranted = await WaitForUpdateTaskAccess(UpdateTasks.MainCableForPops, userAuthId);
            if (accessGranted == false) { return false; }

            _logger.LogTrace("get stored pops");

            IEnumerable<UpdateTaskElementModel> storedPops = await _updateService.GetOpenElements(UpdateTasks.MainCableForPops, userAuthId);

            Int32 changes = 0;

            foreach (UpdateTaskElementModel item in storedPops)
            {
                UpdateTaskResultModel resultModel = new UpdateTaskResultModel();

                try
                {
                    await _updateService.StartUpdateTaskElement(item.Id);

                    ServiceResult<IEnumerable<String>> cableResult = await _projectAdapter.GetMainCableStartingFromPop(item.AdapterId);

                    if (cableResult.HasError == true || cableResult.Data.Count() == 0)
                    {
                        String logText = $"unable to get main cables from pop with id {item.ObjectId}. Reason: {cableResult.Error}";
                        _logger.LogInformation(logText);

                        resultModel.Warning = logText;

                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = resultModel, State = UpdateTaskElementStates.Waring });
                        continue;
                    }

                    IEnumerable<Int32> mappedResult = await _storage.GetCableIdsByProjectIds(cableResult.Data);
                    foreach (Int32 id in mappedResult)
                    {
                        resultModel.UpdatedIds.Add(id);
                    }

                    await _storage.UpdatePoPCableRelation(item.ObjectId, mappedResult);

                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = resultModel, State = UpdateTaskElementStates.Success });

                    changes += 1;
                }
                catch (Exception ex)
                {
                    String errorText = $"an error happend during update main cable for pop. pop id is {item.ObjectId}. Reason: {ex.ToString()}";
                    _logger.LogError(errorText);

                    resultModel.Error = errorText;
                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = resultModel, State = UpdateTaskElementStates.Error });

                    _storage.Restore();

                    continue;
                }
            }

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (changes == 0)
            {
                level = ProtocolLevels.Warning;
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateMainCables, MessageRelatedObjectTypes.MainCables, new Int32?(), level), userAuthId);
            await _updateService.FinishTask(UpdateTasks.MainCableForPops);

            return changes > 0;
        }

        public async Task<Boolean> UpdatePatchConnections(String userAuthId)
        {
            _logger.LogInformation("UpdatePatchConnections");
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("get stored pops");

            Boolean accessGranted = await WaitForUpdateTaskAccess(UpdateTasks.PatchConnections, userAuthId);
            if (accessGranted == false) { return false; }

            IEnumerable<UpdateTaskElementModel> storedCables = await _updateService.GetOpenElements(UpdateTasks.PatchConnections, userAuthId);

            Int32 changes = 0;

            foreach (UpdateTaskElementModel item in storedCables)
            {
                UpdateTaskResultModel taskResultModel = new UpdateTaskResultModel();

                try
                {
                    await _updateService.StartUpdateTaskElement(item.Id);

                    ServiceResult<IEnumerable<ProjectAdapterPatchModel>> connectionResult = await _projectAdapter.GetConnectionByCableId(item.AdapterId);

                    if (connectionResult.HasError == true || connectionResult.Data.Count() == 0)
                    {
                        String logText = $"unable to get connection from cable with id {item.Id}. Reason: {connectionResult.Error}";
                        _logger.LogInformation(logText);

                        taskResultModel.Warning = logText;
                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Waring });
                        continue;
                    }

                    await _storage.DeletePatchConnectionByFlatId(item.ObjectId);
                    Int32? flatId = await _storage.GetFlatIdByCableId(item.ObjectId);
                    if (flatId.HasValue == false)
                    {
                        String logText = $"unable to get flat from cable with id {item.Id}";
                        _logger.LogInformation(logText);

                        taskResultModel.Warning = logText;
                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Waring });
                        continue;
                    }
                    else
                    {
                        IEnumerable<Int32> addedIds = await _storage.AddPatchConnectionByFlatId(flatId.Value, connectionResult.Data);
                        foreach (Int32 id in addedIds)
                        {
                            taskResultModel.AddedIds.Add(id);
                        }

                        if (taskResultModel.AddedIds.Count == 0 && taskResultModel.DeletedIds.Count == 0)
                        {
                            taskResultModel.AddedIds.Add(-1);
                        }

                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Success });
                        changes += 1;
                    }
                }
                catch (Exception ex)
                {
                    String errorText = $"an error happend during update patch connection for cable with id {item.ObjectId}. Reason: {ex.ToString()}";
                    _logger.LogError(errorText);

                    _storage.Restore();
                    taskResultModel.Error = errorText;
                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, Result = taskResultModel, State = UpdateTaskElementStates.Error });
                    continue;
                }

            }

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (changes == 0)
            {
                level = ProtocolLevels.Warning;
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdatePatchConnections, MessageRelatedObjectTypes.FiberConnection, new Int32?(), level), userAuthId);
            await _updateService.FinishTask(UpdateTasks.PatchConnections);

            return changes > 0;
        }

        public async Task<Boolean> UpdateSplices(UpdateTaskElementModel elementModel, String userAuthId)
        {
            _logger.LogTrace("UpdateSplices");
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("check if branchable with id {0} exists", elementModel.ObjectId);
            if (await _storage.CheckIfBranchableExists(elementModel.ObjectId) == false)
            {
                _logger.LogError("unable to update splice. branchable with id {0} not found", elementModel.ObjectId);
                return false;
            }

            UpdateSpliceTaskResultModel taskResult = new UpdateSpliceTaskResultModel();
            try
            {
                await _updateService.StartUpdateTaskElement(elementModel.Id);

                ServiceResult<IEnumerable<ProjectAdapterSpliceModel>> splicesResult = await _projectAdapter.GetSplicesForBranchable(elementModel.AdapterId);
                if (splicesResult.HasError == true || splicesResult.Data.Count() == 0)
                {
                    String waringText = $"error while getting splice data from adapter. branchable project adapter id: {elementModel.AdapterId}. Error {splicesResult.Error}";
                    _logger.LogInformation(waringText);
                    taskResult.Warning = waringText;

                    _logger.LogTrace("update task element model with id", elementModel.Id);
                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = elementModel.Id, State = UpdateTaskElementStates.Waring, Result = taskResult });
                    return false;
                }

                Dictionary<String, Boolean> checkedCables = new Dictionary<string, bool>();
                Dictionary<String, Int32?> cableIdMapper = new Dictionary<string, Int32?>();

                _logger.LogTrace("get stored cable from branchable with id {0}", elementModel.ObjectId);
                IDictionary<String, Int32> spicesFromBranchable = await _storage.GetSplicesFromBranchbaleWithProjectAdapterId(elementModel.ObjectId);
                IDictionary<String, Int32> splicesToRemove = new Dictionary<String, Int32>(spicesFromBranchable);

                Boolean allItemsAreMapped = true;

                DateTimeOffset lastHearbeat = DateTime.Now;

                foreach (ProjectAdapterSpliceModel spliceItem in splicesResult.Data)
                {
                    try
                    {
                        if ((DateTimeOffset.Now - lastHearbeat).TotalMinutes > 3)
                        {
                            lastHearbeat = await _updateService.UpdateHeartbeatByTaskElementId(elementModel.Id);
                        }

                        //   _logger.LogTrace("check if cable with id {0} and {1] exists", spliceItem.FirstFiber.CableProjectAdapterId, spliceItem.SecondFiber.CableProjectAdapterId);
                        Boolean checkCableFirstFiberResult = true;

                        if (spliceItem.FirstFiber != null)
                        {
                            if (checkedCables.ContainsKey(spliceItem.FirstFiber.CableProjectAdapterId) == false)
                            {
                                checkedCables.Add(spliceItem.FirstFiber.CableProjectAdapterId, await _storage.CheckIfCableExistsByProjectId(spliceItem.FirstFiber.CableProjectAdapterId));
                            }

                            checkCableFirstFiberResult = checkedCables[spliceItem.FirstFiber.CableProjectAdapterId];
                        }

                        Boolean checkCableSecondFiberResult = true;

                        if (spliceItem.SecondFiber != null)
                        {
                            if (checkedCables.ContainsKey(spliceItem.SecondFiber.CableProjectAdapterId) == false)
                            {
                                checkedCables.Add(spliceItem.SecondFiber.CableProjectAdapterId, await _storage.CheckIfCableExistsByProjectId(spliceItem.SecondFiber.CableProjectAdapterId));
                            }

                            checkCableSecondFiberResult = checkedCables[spliceItem.SecondFiber.CableProjectAdapterId];
                        }

                        if (checkCableFirstFiberResult == false || checkCableSecondFiberResult == false)
                        {
                            _logger.LogWarning("a cable from splice doesn't exists");
                            allItemsAreMapped = false;
                            continue;
                        }

                        Int32? firstCableId = null;
                        if (spliceItem.FirstFiber != null)
                        {
                            if (cableIdMapper.ContainsKey(spliceItem.FirstFiber.CableProjectAdapterId) == false)
                            {
                                cableIdMapper.Add(spliceItem.FirstFiber.CableProjectAdapterId, await _storage.GetCableIdByProjectId(spliceItem.FirstFiber.CableProjectAdapterId));
                            }

                            firstCableId = cableIdMapper[spliceItem.FirstFiber.CableProjectAdapterId];
                        }

                        Int32? seconddCableId = null;

                        if (spliceItem.SecondFiber != null)
                        {
                            if (cableIdMapper.ContainsKey(spliceItem.SecondFiber.CableProjectAdapterId) == false)
                            {
                                cableIdMapper.Add(spliceItem.SecondFiber.CableProjectAdapterId, await _storage.GetCableIdByProjectId(spliceItem.SecondFiber.CableProjectAdapterId));
                            }

                            seconddCableId = cableIdMapper[spliceItem.SecondFiber.CableProjectAdapterId];
                        }

                        Int32? firstFiberId = null;
                        Int32? secondFiberId = null;

                        if (spliceItem.FirstFiber != null)
                        {
                            if (await _storage.CheckIfFiberExistsByProjectAdapterId(spliceItem.FirstFiber.ProjectAdapterId, firstCableId.Value) == true)
                            {
                                await _storage.UpdateFiber(spliceItem.FirstFiber);
                                //taskResult.UpdatedFiberIds.Add(firstFiberId.Value);
                            }
                            else
                            {
                                firstFiberId = await _storage.AddFiberFromAdapter(spliceItem.FirstFiber, firstCableId.Value);

                                taskResult.AddedFiberIds.Add(firstFiberId.Value);
                            }
                        }

                        if (spliceItem.SecondFiber != null)
                        {
                            if (await _storage.CheckIfFiberExistsByProjectAdapterId(spliceItem.SecondFiber.ProjectAdapterId, seconddCableId.Value) == true)
                            {
                                await _storage.UpdateFiber(spliceItem.SecondFiber);
                                //taskResult.UpdatedFiberIds.Add(secondFiberId.Value);
                            }
                            else
                            {
                                secondFiberId = await _storage.AddFiberFromAdapter(spliceItem.SecondFiber, firstCableId.Value);
                                taskResult.AddedFiberIds.Add(secondFiberId.Value);
                            }
                        }

                        Int32 spliceId = 0;
                        if (spicesFromBranchable.ContainsKey(spliceItem.ProjectAdapterId) == true)
                        {
                            _logger.LogTrace("slice with project adapter id {0} found in database. id is {1}", spliceItem.ProjectAdapterId, spliceId);

                            spliceId = spicesFromBranchable[spliceItem.ProjectAdapterId];
                            splicesToRemove.Remove(spliceItem.ProjectAdapterId);

                            await _storage.UpdateSpliceFromAdapter(spliceId, spliceItem, elementModel.ObjectId);

                            taskResult.UpdatedIds.Add(spliceId);
                            _logger.LogTrace("updating splice");
                        }
                        else
                        {
                            spliceId = await _storage.AddSpliceFromAdapter(new FiberSpliceCreateModel
                            {
                                SpliceModel = spliceItem,
                                FirstFiberId = firstFiberId,
                                SecondFiberId = secondFiberId,
                                BranchableId = elementModel.ObjectId
                            });
                            spicesFromBranchable.Add(spliceItem.ProjectAdapterId, spliceId);

                            taskResult.AddedIds.Add(spliceId);
                        }
                    }
                    catch (Exception ex)
                    {
                        allItemsAreMapped = false;
                        taskResult.Warning += $"Failed to update splice with project adapter id {spliceItem.ProjectAdapterId}. Error {ex.ToString()} \n\r";

                        _storage.Restore();
                        _logger.LogError("unable to update splice. Error: {0}", ex.ToString());
                        continue;
                    }
                }

                foreach (Int32 spliceId in splicesToRemove.Values)
                {
                    try
                    {
                        await _storage.DeleteSpliceAndFibers(spliceId);
                        taskResult.DeletedIds.Add(spliceId);
                    }
                    catch (Exception ex)
                    {
                        allItemsAreMapped = false;
                        taskResult.Warning += $"Failed to remove splice with id {spliceId}. Error {ex.ToString()} \n\r";
                        _logger.LogError("unable to update splice. Error: {0}", ex.ToString());

                        _storage.Restore();
                        continue;
                    }
                }

                ProtocolLevels level = ProtocolLevels.Sucesss;
                if (allItemsAreMapped == false)
                {
                    level = ProtocolLevels.Warning;
                }

                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateSplices, MessageRelatedObjectTypes.Branchable, elementModel.ObjectId, level), userAuthId);

                UpdateTaskElementStates taskState = UpdateTaskElementStates.Success;
                if (String.IsNullOrEmpty(taskResult.Warning) == false)
                {
                    taskState = UpdateTaskElementStates.Waring;
                }

                await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = elementModel.Id, State = taskState, Result = taskResult });

                return true;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "unable to update splicesfrom branchable with id {0}", elementModel.ObjectId);
                _storage.Restore();

                taskResult.Error = ex.ToString();
                await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = elementModel.Id, State = UpdateTaskElementStates.Error, Result = taskResult });

                return false;
            }
        }

        private static SemaphoreSlim _startTaskSemaphore = new SemaphoreSlim(1);

        private async Task<Boolean> WaitForUpdateTaskAccess(UpdateTasks taskType, String userAuthId)
        {
            _logger.LogTrace("wait for access to update task with type: {0}", taskType);

            Int32 minutesToWait = 1;
            Int32 triesLeft = 11;

            while (triesLeft > 0)
            {
                _logger.LogTrace("tries left for tasktype {0} are: {1}", taskType, triesLeft);

                try
                {
                    _logger.LogTrace("try access to semaphore for taskType {0}", taskType);
                    await _startTaskSemaphore.WaitAsync();
                    _logger.LogTrace("access to semaphore for tasktype {0} granted", taskType);

                    Boolean isLocked = await _updateService.CheckIfUpdateTaskIsLocked(taskType);
                    _logger.LogTrace("tasktype {0} is currently lock state is: {1}", taskType, isLocked);
                    if (isLocked == false)
                    {
                        _logger.LogTrace("locking tasktype {0} is in progress", taskType);
                        await _updateService.GetOpenElements(taskType, userAuthId);
                        await _updateService.LockUpdateTask(taskType);
                        _logger.LogTrace("tasktype {0} is now locked", taskType);
                        return true;
                    }
                }
                finally
                {
                    _logger.LogTrace("realease semaphore");
                    _startTaskSemaphore.Release();
                }

                _logger.LogTrace("waiting {0} minute for next try", minutesToWait);
                await Task.Delay(TimeSpan.FromMinutes(minutesToWait));
                triesLeft -= 1;
            }

            if (triesLeft == 0)
            {
                _logger.LogWarning("no tries left. unable to get access to task with type {0}", taskType);
                return false;
            }

            return false;
        }

        public async Task<Boolean> UpdateSplices(String userAuthId)
        {
            _logger.LogTrace("UpdateSplices");
            await CheckUserAuthId(userAuthId);

            Boolean accessGranted = await WaitForUpdateTaskAccess(UpdateTasks.Splices, userAuthId);
            if (accessGranted == false) { return false; }

            _logger.LogTrace("get stored branchables");
            IEnumerable<UpdateTaskElementModel> branchables = await _updateService.GetOpenElements(UpdateTasks.Splices, userAuthId);

            Int32 changes = 0;
            foreach (UpdateTaskElementModel item in branchables)
            {
                if (await _updateService.CheckIfTaskIsCanceledByElementId(item.Id) == true)
                {
                    _logger.LogInformation("task should be canceld");
                    break;
                }

                Boolean result = await UpdateSplices(item, userAuthId);
                if (result == true)
                {
                    changes += 1;
                }
            }

            await _updateService.FinishTask(UpdateTasks.Splices);

            return changes > 0;
        }

        public async Task<Boolean> UpdateMainCableForBranchable(String userAuthId)
        {
            _logger.LogTrace("UpdateMainCableForBranchable");
            await CheckUserAuthId(userAuthId);

            Boolean accessGranted = await WaitForUpdateTaskAccess(UpdateTasks.MainCableForBranchable, userAuthId);
            if (accessGranted == false) { return false; }

            _logger.LogTrace("get open elements from task");
            IEnumerable<UpdateTaskElementModel> openElements = await _updateService.GetOpenElements(UpdateTasks.MainCableForBranchable, userAuthId);

            Int32 changes = 0;

            foreach (UpdateTaskElementModel item in openElements)
            {
                _logger.LogTrace("check if task should be canceld");
                if (await _updateService.CheckIfTaskIsCanceledByElementId(item.Id) == true)
                {
                    _logger.LogInformation("task should be canceld");
                    break;
                }

                _logger.LogTrace("set task element as processing");
                await _updateService.StartUpdateTaskElement(item.Id);

                UpdateTaskResultModel taskResult = new UpdateTaskResultModel();

                try
                {
                    ServiceResult<ProjectAdapterMainCableForBranchableResult> mainCableResult = await _projectAdapter.GetMainCableForBranchable(item.AdapterId);

                    if (mainCableResult.HasError == true || mainCableResult.Data == null)
                    {
                        String waringText = $"unable to get main cable from adapter with id {item.ObjectId}";
                        _logger.LogInformation(waringText);
                        taskResult.Warning = waringText;

                        _logger.LogTrace("update task element model with id", item.Id);
                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, State = UpdateTaskElementStates.Waring, Result = taskResult });
                        continue;
                    }

                    _logger.LogTrace("check if cable with project adapter id {0} exists", mainCableResult.Data.ProjectAdapterCableId);
                    if (await _storage.CheckIfCableExistsByProjectId(mainCableResult.Data.ProjectAdapterCableId) == false)
                    {
                        String warningText = $"cable with prject adapter id {mainCableResult.Data.ProjectAdapterCableId} not found in database";
                        _logger.LogInformation(warningText);

                        _logger.LogTrace("update task element model with id", item.Id);
                        await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, State = UpdateTaskElementStates.Waring, Result = taskResult });
                        continue;
                    }

                    Int32 cableId = await _storage.GetCableIdByProjectId(mainCableResult.Data.ProjectAdapterCableId);
                    await _storage.UpdateBranchableCableRelation(item.ObjectId, cableId);

                    taskResult.UpdatedIds.Add(cableId);

                    _logger.LogTrace("check if ending of main cable with id {0} exists", cableId);
                    if (String.IsNullOrEmpty(mainCableResult.Data.BranchableEndCableProejctAdapterId) == false)
                    {
                        _logger.LogTrace("check if branchable with adapter id {0} exists", mainCableResult.Data.BranchableEndCableProejctAdapterId);
                        if (await _storage.CheckIfBranchableExistsByAdapterId(mainCableResult.Data.BranchableEndCableProejctAdapterId) == true)
                        {
                            _logger.LogTrace("get branchable id by project adapter id {0}", mainCableResult.Data.BranchableEndCableProejctAdapterId);
                            Int32 branchableId = await _storage.GetBranchableIdByProjectAdapterId(mainCableResult.Data.BranchableEndCableProejctAdapterId);
                            await _storage.UpdateEndingForMainCable(cableId, branchableId);
                        }
                        else
                        {
                            _logger.LogInformation("branchable with project adapter id {0} not found", mainCableResult.Data.BranchableEndCableProejctAdapterId);
                        }
                    }

                    changes += 1;

                    _logger.LogTrace("update task element model with id", item.Id);

                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, State = UpdateTaskElementStates.Success, Result = taskResult });
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "unable to update main cable from branchable with id {0}", item.ObjectId);
                    _storage.Restore();

                    taskResult.Error = ex.ToString();
                    await _updateService.FinishUpdateElement(new UpdateTaskElementFinishModel { ElementId = item.Id, State = UpdateTaskElementStates.Error, Result = taskResult });
                }
            }

            await _updateService.FinishTask(UpdateTasks.MainCableForBranchable);

            ProtocolLevels level = ProtocolLevels.Sucesss;
            if (changes == 0)
            {
                level = ProtocolLevels.Warning;
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.UpdateMainCableForBranchable, MessageRelatedObjectTypes.MainCables, new Int32?(), level), userAuthId);

            return changes > 0;
        }

        public async Task SetCableToFlatsWithOneBuildingUnit()
        {
            _logger.LogTrace("SetCalbeToFlatsWithOneBuildingUnit");

            _logger.LogTrace("Get building with one unit");
            IEnumerable<Int32> buildingIds = await _storage.GetBuildingsWithOneUnit();

            if (buildingIds.Count() == 0)
            {
                _logger.LogInformation("no building was found");
                return;
            }

            foreach (Int32 buildingID in buildingIds)
            {
                await _storage.SetCableToFlatByBuildingId(buildingID);
            }
        }

        public async Task<Boolean> UpdateAllDataFromProjectAdapter(String userAuthId)
        {
            try
            {
                await UpdateAllCableTypesFromAdapter(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdateBuildigsFromAdapter(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdatePopInformation(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdateCablesFromAdapter(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdateMainCables(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdateMainCableForBranchable(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdatePatchConnections(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            try
            {
                await UpdateSplices(userAuthId);
            }
            catch (Exception)
            {
                _storage.Restore();
            }

            return true;
        }


        public async Task<ConstructionStageDetailsForEditModel> GetDetailsForEdit(Int32 constructionStageId)
        {
            _logger.LogTrace("GetDetailsForEdit. constructionStageId: {0}", constructionStageId);

            await CheckIfConstructionStageExists(constructionStageId);

            ConstructionStageDetailsForEditModel result = await _storage.GetDetailsForEdit(constructionStageId);
            return result;
        }

        public async Task<ConstructionStageDetailsForEditModel> UpdateConstructionStage(ConstructionStageUpdateModel model, String userAuthId)
        {
            _logger.LogTrace("UpdateConstructionStage");
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("Validating model");

            if (model == null)
            {
                _logger.LogInformation("model is null. unable to update construction stage");
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.ModelIsNull);
            }

            await CheckIfConstructionStageExists(model.Id);
            await ValidateConstructionStageName(model.Name, model.Id, ConstructionStageServicExceptionReasons.NameInUse);

            if (model.AddedBranchables == null)
            {
                model.AddedBranchables = new List<Int32>();
            }
            if (model.RemovedBranchables == null)
            {
                model.RemovedBranchables = new List<Int32>();
            }

            List<Int32> branchablesToCheck = new List<Int32>();
            branchablesToCheck.AddRange(model.AddedBranchables);
            branchablesToCheck.AddRange(model.RemovedBranchables);

            if (branchablesToCheck.Count > 0)
            {
                _logger.LogTrace("check if branchable exists");
                if (await _storage.CheckIfCabinetsExists(branchablesToCheck) == false)
                {
                    _logger.LogInformation("not all branchables found");
                    throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
                }

                _logger.LogTrace("get current branchable ids");
                IEnumerable<Int32> currentBranchableIds = await _storage.GetBranchableIdsByConstructionStageId(model.Id);

                List<Int32> branchablesToRemove = new List<int>();
                List<Int32> branchablesToAdd = new List<int>();

                foreach (Int32 currentId in currentBranchableIds)
                {
                    _logger.LogTrace("check if branchable with id {0} should removed", currentId);
                    if (model.RemovedBranchables.Contains(currentId) == true)
                    {
                        _logger.LogTrace("branchable with id {0} should be removed", currentId);
                        branchablesToRemove.Add(currentId);
                    }
                }

                foreach (Int32 branchableIdToAdd in model.AddedBranchables)
                {
                    _logger.LogTrace("check if branchable with id {0} should be added", branchableIdToAdd);
                    if (currentBranchableIds.Contains(branchableIdToAdd) == false)
                    {
                        _logger.LogTrace("branchable with id {0} should be added", branchableIdToAdd);
                        branchablesToAdd.Add(branchableIdToAdd);
                    }
                }

                model.AddedBranchables = branchablesToAdd;
                model.RemovedBranchables = branchablesToRemove;
            }

            await _storage.UpdateConstructionStage(model);

            ConstructionStageDetailsForEditModel result = await _storage.GetDetailsForEdit(model.Id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, model.Id), userAuthId);

            return result;
        }

        public async Task<IEnumerable<Int32>> CleanupCables()
        {
            IDictionary<Int32, String> cableAdapterOverview = await _storage.GetAllCablesWithProjectAdapterId();

            HashSet<String> usedAdapterIds = new HashSet<string>();
            List<Int32> deletedCableIds = new List<Int32>();

            foreach (KeyValuePair<Int32, String> item in cableAdapterOverview)
            {
                if (usedAdapterIds.Contains(item.Value) == true)
                {
                    await _storage.DeleteCable(item.Key);
                    deletedCableIds.Add(item.Key);
                }
                else
                {
                    usedAdapterIds.Add(item.Value);
                }
            }

            return deletedCableIds;
        }

        public async Task<BranchableSpliceOverviewModel> GetBranchableSpliceOverview(Int32 branchableId)
        {
            _logger.LogTrace("GetBranchableSpliceOverview. branchableId: {0}", branchableId);

            _logger.LogTrace("check if branchable with id {0} exists", branchableId);
            if (await _storage.CheckIfBranchableExists(branchableId) == false)
            {
                _logger.LogWarning("branchable with id {0} not found", branchableId);
                throw new ConstructionStageServicException(ConstructionStageServicExceptionReasons.NotFound);
            }

            BranchableSpliceOverviewModel result = await _storage.GetBranchableSpliceOverview(branchableId);
            return result;
        }

        public async Task<IDictionary<String, Int32>> GetAllBranchableWithProjectAdapterIds()
        {
            _logger.LogTrace("GetAllBranchableWithProjectAdapterIds");

            IDictionary<String, Int32> result = await _storage.GetBranchablesWithProjectAdapterId();
            return result;
        }

        #endregion
    }
}
