﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class CustomerService : ICustomerService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly ILogger<CustomerService> _logger;
        private readonly IProtocolService _protocolService;

        #endregion

        #region Constructor

        public CustomerService(
            IBesaDataStorage storage, 
            ILogger<CustomerService> logger,
            IProtocolService protocolService)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        #endregion

        #region Methods

        public static String GetNameForProcedure(String type, String buldingStreetName, PersonInfo info)
        {
            String contactDisplay = GetContactDisplay(info);
            String name = $"{type} {buldingStreetName} - {contactDisplay}";
            return name;
        }

        public static String GetNameForConnectBuildingProcedure(String buldingStreetName, PersonInfo info)
        {
            return GetNameForProcedure("Hausanschluss", buldingStreetName, info);
        }

        public static String GetNameForConnectCustomerProcedure(String buldingStreetName, PersonInfo info)
        {
            return GetNameForProcedure("Aktiver Anschluss", buldingStreetName, info);
        }

        public static async Task<String> GenerateNameForProcedure(IBesaDataStorage storage, Int32 buildingId, Int32 contactId)
        {
            String buldingStreetName = await storage.GetBuildingStreetNameById(buildingId);
            PersonInfo ownerInfo = await storage.GetContactById(contactId);

            return GetNameForConnectBuildingProcedure(buldingStreetName, ownerInfo);
        }

        private async Task CheckUserAuthId(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new CustomerServicException(CustomerServicExceptionReasons.UserNotFound);
            }
        }

        public async Task<Int32> CreateCustomerConnectRequest(CustomerConnectRequestCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateCustomer");

            await CheckUserAuthId(userAuthId);

            if (model == null)
            {
                _logger.LogInformation("model not set");
                throw new CustomerServicException(CustomerServicExceptionReasons.NoData);
            }
            if (model.CustomerPersonId.HasValue == false && model.PropertyOwnerPersonId.HasValue == false)
            {
                _logger.LogInformation("either a customer or a owner found");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }
            if (model.FlatId.HasValue == false && model.BuildingId.HasValue == false)
            {
                _logger.LogInformation("neither a flat or a building found");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }
            if (model.FlatId.HasValue == true && model.CustomerPersonId.HasValue == false)
            {
                _logger.LogInformation("if a customer is choosen, a flat has also be choosen");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }
            if (model.BuildingId.HasValue == true && model.PropertyOwnerPersonId.HasValue == false)
            {
                _logger.LogInformation("if a building is choosen, a owner has also be choosen");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }

            if (model.FlatId.HasValue == true)
            {
                if (model.ConnectionAppointment.HasValue == false && model.ActivationAsSoonAsPossible == false)
                {
                    throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
                }

                model.InformSalesAfterFinish = false;

                _logger.LogTrace("check if flat with id {0} exists", model.FlatId);
                if (await _storage.CheckIfFlatExists(model.FlatId.Value) == false)
                {
                    _logger.LogInformation("flat with id {0} not found", model.FlatId);
                    throw new CustomerServicException(CustomerServicExceptionReasons.NotFound);
                }
            }

            Int32 buildingId = 0;
            if (model.FlatId.HasValue == false)
            {
                _logger.LogTrace("check if building with id {0} exists", model.BuildingId.Value);
                if (await _storage.CheckIfBuildingExists(model.BuildingId.Value) == false)
                {
                    _logger.LogInformation("bulding with id {0} not found", model.FlatId);
                    throw new CustomerServicException(CustomerServicExceptionReasons.NotFound);
                }

                buildingId = model.BuildingId.Value;
            }
            else
            {
                _logger.LogTrace("get building id by flat id. flat id {0}", model.FlatId);
                buildingId = await _storage.GetBuildingIdByFlatId(model.FlatId.Value);
                _logger.LogTrace("flat id {0} has building id {1}", model.FlatId, buildingId);
                
            }

            _logger.LogTrace("check if house connenction with buildingId {0} already finsiehd", buildingId);
            if (await _storage.CheckIfBuildingConnenctionIsFinished(buildingId) == true)
            {
                _logger.LogTrace("building with id {0} has already been connected");
                if (model.OnlyHouseConnection == true)
                {
                    _logger.LogInformation("buildig with id {0} has already been connected. only a house connection is not allowed");
                    throw new CustomerServicException(CustomerServicExceptionReasons.InvalidOperation);
                }

                _logger.LogTrace("no building connection is needed, so contact person set to null");
            }
            else
            {

            }

            HashSet<Int32> personInfoIds = new HashSet<Int32>();
            List<Int32?> idsToAdd = new List<Int32?> { model.CustomerPersonId, model.PropertyOwnerPersonId, model.ArchitectPersonId, model.WorkmanPersonId };
            foreach (Int32? id in idsToAdd)
            {
                if (id.HasValue == false) { continue; }
                personInfoIds.Add(id.Value);
            }

            _logger.LogTrace("check if contact infos exists");
            if (await _storage.CheckIfContactInfosExists(personInfoIds) == false)
            {
                _logger.LogInformation("not all contacts ids was found");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }

            Int32 requestId = await _storage.CreateCustomerConnenctionRequest(model);
            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.CustomerRequest, requestId), userAuthId);

            if (model.OnlyHouseConnection == true)
            {
                String buldingStreetName = await _storage.GetBuildingStreetNameById(model.BuildingId.Value);
                PersonInfo ownerInfo = await _storage.GetContactById(model.PropertyOwnerPersonId.Value);
                String contactDisplay = GetContactDisplay(ownerInfo);

                BuildingConnectionProcedureCreateModel procedureCreateModel = new BuildingConnectionProcedureCreateModel
                {
                    Appointment = model.ConnectionAppointment,
                    BuildingId = model.BuildingId.Value,
                    OwnerContactId = model.PropertyOwnerPersonId.Value,
                    Name = $"Hausanschluss {buldingStreetName} - {contactDisplay}",
                    FirstElement = new ProcedureTimelineCreateModel
                    {
                        RelatedRequestId = requestId,
                        State = ProcedureStates.RequestCreated,
                    },
                    InformSalesAfterFinish = model.InformSalesAfterFinish,
                };

                Int32 procedureId = await _storage.CreateBuildingConnectionProcedure(procedureCreateModel);
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.BuildingConnenctionProcedure, procedureId), userAuthId);

            }
            else
            {
                PersonInfo ownerInfo = await _storage.GetContactById(model.CustomerPersonId.Value);
                String buldingStreetName = await _storage.GetBuildingStreetNameByFlatId(model.FlatId.Value);

                CustomerConnectionProcedureCreateModel procedureCreateModel = new CustomerConnectionProcedureCreateModel
                {
                    Appointment = model.ConnectionAppointment,
                    AsSoonAsPossible = model.ActivationAsSoonAsPossible,
                    CustomerContactId = model.CustomerPersonId.Value,
                    Name = GetNameForConnectCustomerProcedure(buldingStreetName,ownerInfo),
                    FlatId = model.FlatId.Value,
                    FirstElement = new ProcedureTimelineCreateModel
                    {
                        RelatedRequestId = requestId,
                        State = ProcedureStates.RequestCreated,
                    },
                };

                Int32 procedureId = await _storage.CreateCustomerConnectionProcedure(procedureCreateModel);
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.CustomerConnenctionProcedure, procedureId), userAuthId);
            }

            return requestId;
        }

        private static string GetContactDisplay(PersonInfo ownerInfo)
        {
            String result = String.Empty;
            if (ownerInfo.Type == PersonTypes.Male)
            {
                result += "Herr ";
            }
            else
            {
                result += "Frau ";
            }

            if (String.IsNullOrEmpty(ownerInfo.Surname) == false)
            {
                result += ownerInfo.Surname + ' ';
            }
            if (String.IsNullOrEmpty(ownerInfo.Lastname) == false)
            {
                result += ownerInfo.Lastname + ' ';
            }
            if (String.IsNullOrEmpty(ownerInfo.CompanyName) == false)
            {
                result += '(' + ownerInfo.CompanyName + ')';
            }

            return result;
        }

        public async Task<IEnumerable<PersonInfo>> SearchContacts(String query, Int32 amount)
        {
            _logger.LogTrace("SearchContacts");
            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogTrace("SearchContact with no query. Empty list returns");

                return new List<PersonInfo>();
            }
            else
            {
                _logger.LogTrace("SearchContact with query: {0}", query);

                _logger.LogTrace("check amount");
                if (amount > 100 || amount <= 0)
                {
                    _logger.LogInformation("only 100 results are allowed per query. {0} requested", amount);
                    throw new CustomerServicException(CustomerServicExceptionReasons.InvalidOperation);
                }

                IEnumerable<PersonInfo> result = await _storage.SearchContacts(query, amount);
                return result;
            }
        }

        private async Task ValidateContact(PersonInfo contact, String userAuthId)
        {
            _logger.LogTrace("validating contact model");
            await CheckUserAuthId(userAuthId);

            if (contact == null)
            {
                _logger.LogInformation("no contact data was given");
                throw new CustomerServicException(CustomerServicExceptionReasons.NoData);
            }

            if (
                contact.Address == null ||
                String.IsNullOrEmpty(contact.Address.Street) == true || contact.Address.Street.Length > _storage.Constraints.MaxAddressStreetChars ||
                String.IsNullOrEmpty(contact.Address.StreetNumber) == true || contact.Address.StreetNumber.Length > _storage.Constraints.MaxAddressStreetNumberChars ||
                String.IsNullOrEmpty(contact.Address.City) == true || contact.Address.City.Length > _storage.Constraints.MaxAddressCityChars ||
                String.IsNullOrEmpty(contact.Address.PostalCode) == true || contact.Address.PostalCode.Length > _storage.Constraints.MaxAddressZipCodeChars || contact.Address.PostalCode.Length < _storage.Constraints.MinAddressZipCodeChars ||
                String.IsNullOrEmpty(contact.Lastname) == true || contact.Lastname.Length > _storage.Constraints.MaxUserLastnameChars ||
                (String.IsNullOrEmpty(contact.EmailAddress) == false && (contact.EmailAddress.Length > _storage.Constraints.MaxUserMailChars)) ||
                String.IsNullOrEmpty(contact.Phone) == true || contact.Phone.Length > _storage.Constraints.MaxUserPhoneChars ||
                (String.IsNullOrEmpty(contact.CompanyName) == false && contact.CompanyName.Length > _storage.Constraints.MaxCompanyNameChars) ||
                (String.IsNullOrEmpty(contact.Surname) == false && contact.Surname.Length > _storage.Constraints.MaxUserSurnameChars)
                )
            {
                _logger.LogInformation("invalid contact data");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidData);
            }
        }

        public async Task<Int32> CreateContact(PersonInfo contact, String userAuthId)
        {
            _logger.LogTrace("CreateContact");

            await ValidateContact(contact,userAuthId);

            Int32 contactId = await _storage.CreateContact(contact);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.ContactInfo, contactId), userAuthId);

            return contactId;
        }

        private async Task CheckIfContactExists(Int32 contactId)
        {
            _logger.LogTrace("check if contact with id {0} exists", contactId);

            if (await _storage.CheckIfContactExists(contactId) == false)
            {
                _logger.LogInformation("contact with id {0} not found", contactId);
                throw new CustomerServicException(CustomerServicExceptionReasons.NotFound);
            }
        }

        public async Task<PersonInfo> EditContact(PersonInfo model, String userAuthId)
        {
            _logger.LogTrace("EditContact");

            await ValidateContact(model,userAuthId);
            await CheckIfContactExists(model.Id);

            await _storage.EditContact(model);
            PersonInfo result = await _storage.GetContactById(model.Id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Update, MessageRelatedObjectTypes.ContactInfo, model.Id), userAuthId);

            return result;
        }

        public async Task<Boolean> CheckIfContactIsInUse(Int32 contactId)
        {
            _logger.LogTrace("CheckIfContactIsInUse. Contact id {0}", contactId);
            await CheckIfContactExists(contactId);

            Boolean result = await _storage.CheckIfContactIsInUse(contactId);
            return result;
        }

        public async Task<PersonInfo> GetContactById(int contactId)
        {
            _logger.LogTrace("GetContactById. Contact id {0}", contactId);
            await CheckIfContactExists(contactId);

            PersonInfo result = await _storage.GetContactById(contactId);
            return result;
        }

        public async Task<Boolean> DeleteContact(Int32 contactId, String userAuthId)
        {
            _logger.LogTrace("DeleteContact. Contact id {0}", contactId);
            await CheckIfContactExists(contactId);
            await CheckUserAuthId(userAuthId);

            if (await _storage.CheckIfContactIsInUse(contactId) == true)
            {
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidOperation);
            }

            Boolean result = await _storage.DeleteContact(contactId);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Delete, MessageRelatedObjectTypes.ContactInfo, contactId), userAuthId);

            return result;
        }

        public async Task<IEnumerable<PersonInfo>> GetContacts(Int32 start, Int32 amount)
        {
            _logger.LogTrace("GetContacts. start: {0}, amount: {1}", start, amount);

            _logger.LogTrace("check amount");
            if (amount > 100 || amount <= 0)
            {
                _logger.LogInformation("only 100 results are allowed per query. {0} requested", amount);
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidOperation);
            }
            _logger.LogTrace("check start");
            if (start < 0)
            {
                _logger.LogInformation("start should be positive. given: {0}", start);
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidOperation);
            }

            IEnumerable<PersonInfo> result = await _storage.GetContactsSortByLastname(start, amount);
            return result;
        }

        public async Task<IEnumerable<Int32>> ImportContactsFromCSV(CSVContactImportInfoModel infoModel, String userAuthId)
        {
            _logger.LogTrace("ImportContactsFromCSV");

            _logger.LogTrace("check if model is valid");
            if (infoModel == null)
            {
                _logger.LogInformation("no model was given");
                throw new CustomerServicException(CustomerServicExceptionReasons.NoData);
            }

            if (infoModel.Mappings == null || infoModel.Mappings.Count == 0)
            {
                _logger.LogInformation("no mapping was given");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidImportData);
            }

            if (infoModel.Lines == null || infoModel.Lines.Count == 0)
            {
                _logger.LogInformation("no import data");
                throw new CustomerServicException(CustomerServicExceptionReasons.InvalidImportData);
            }

            List<PersonProperty> propertiesNeeded = new List<PersonProperty> { PersonProperty.Lastname, PersonProperty.Phone, PersonProperty.Street, PersonProperty.StreetNumer };

            if (String.IsNullOrEmpty(infoModel.DefaultCity) == true)
            {
                propertiesNeeded.Add(PersonProperty.City);
            }

            if (String.IsNullOrEmpty(infoModel.DefaultPostalCode) == true)
            {
                propertiesNeeded.Add(PersonProperty.ZipCode);
            }

            foreach (PersonProperty neededProperty in propertiesNeeded)
            {
                _logger.LogTrace("check if mapping for property {0} is found", neededProperty);
                if (infoModel.Mappings.ContainsKey(neededProperty) == false)
                {
                    _logger.LogInformation("property {0} not found in mapping", neededProperty);
                    throw new CustomerServicException(CustomerServicExceptionReasons.InvalidImportData);
                }
            }

            await CheckUserAuthId(userAuthId);

            List<PersonInfo> contactsToCreate = new List<PersonInfo>(infoModel.Lines.Count);

            for (int i = 0; i < infoModel.Lines.Count; i++)
            {
                String[] parts = infoModel.Lines[i];
                if (parts.Length <= 1) { continue; }

                PersonInfo contact = new PersonInfo();
                foreach (KeyValuePair<PersonProperty, Int32> mapping in infoModel.Mappings)
                {
                    if (mapping.Value >= parts.Length) { continue; }
                    String value = parts[mapping.Value];
                    if (String.IsNullOrEmpty(value) == true) { continue; }

                    switch (mapping.Key)
                    {
                        case PersonProperty.Surname:
                            contact.Surname = value;
                            break;
                        case PersonProperty.Lastname:
                            contact.Lastname = value;
                            break;
                        case PersonProperty.CompanyName:
                            contact.CompanyName = value;
                            break;
                        case PersonProperty.Street:
                            contact.Address.Street = value;
                            break;
                        case PersonProperty.StreetNumer:
                            contact.Address.StreetNumber = value;
                            break;
                        case PersonProperty.City:
                            if (String.IsNullOrEmpty(value) == true)
                            {
                                contact.Address.City = infoModel.DefaultCity;
                            }
                            else
                            {
                            }
                            contact.Address.City = value;
                            break;
                        case PersonProperty.ZipCode:


                            contact.Address.PostalCode = value;

                            break;
                        case PersonProperty.Email:
                            contact.EmailAddress = value;
                            break;
                        case PersonProperty.Phone:
                            contact.Phone = value;
                            break;
                        case PersonProperty.Type:
                            contact.Type = ConvertToPersonType(value);
                            break;
                        default:
                            break;
                    }
                }

                if (String.IsNullOrEmpty(contact.Address.PostalCode) == true)
                {
                    contact.Address.PostalCode = infoModel.DefaultPostalCode;
                }

                if (String.IsNullOrEmpty(contact.Address.City) == true)
                {
                    contact.Address.City = infoModel.DefaultCity;
                }

                if (String.IsNullOrEmpty(contact.Address.StreetNumber) == true)
                {
                    contact.Address.StreetNumber = "0";
                }

                contactsToCreate.Add(contact);
            }

            List<Int32> contactIds = new List<int>();

            foreach (PersonInfo contact in contactsToCreate)
            {
                try
                {
                    if(String.IsNullOrEmpty(contact.Surname) == true || String.IsNullOrEmpty(contact.Lastname) == true)
                    {
                        _logger.LogInformation("no lastname or surname was givven. unable to create contact");
                        continue;
                    }

                    if(infoModel.UniqueSurnameAndLastname == true && await _storage.CheckIfContactExists(contact.Surname,contact.Lastname) == true)
                    {
                        _logger.LogInformation("contact {0} {1} found in database, contact not created");
                    }
                    else
                    {
                        Int32 id = await _storage.CreateContact(contact);
                        await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, id), userAuthId);

                        contactIds.Add(id);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("Error while importing contact: {0}", ex.ToString());
                }
            }

            return contactIds;
        }

        private PersonTypes ConvertToPersonType(String value)
        {
            return PersonTypes.Male;
        }

        #endregion
    }
}
