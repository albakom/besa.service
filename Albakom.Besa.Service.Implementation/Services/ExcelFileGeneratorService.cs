﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ExcelFileGeneratorService : IExcelFileGeneratorService
    {
        #region  Fields

        private readonly IConstructionStageService _constructionStageService;
        private readonly ILogger<ExcelFileGeneratorService> _logger;

        #endregion

        #region Constructor

        public ExcelFileGeneratorService(IConstructionStageService constructionStageService, ILogger<ExcelFileGeneratorService> logger)
        {
            this._constructionStageService = constructionStageService ?? throw new ArgumentNullException(nameof(constructionStageService));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        public async Task<NamedExportFile> GenerateSpliceOverview(Int32 branchableId)
        {
            _logger.LogTrace("GenerateSpiceOverview. branchableId: {0}", branchableId);

            _logger.LogTrace("try to get splice result from service");
            BranchableSpliceOverviewModel serviceResult = null;
            try
            {
                serviceResult = await _constructionStageService.GetBranchableSpliceOverview(branchableId);
                _logger.LogTrace("for branchable {0} ({1}) {3} splices were found", serviceResult.Branchable.Name,serviceResult.Branchable.Id, serviceResult.Splices.Count());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "unable to load splices from service");
                throw new ExcelFileGeneratorServiceException(ExcelFileGeneratorServicExceptionReasons.UnableToGetSpliceResultFromService);
            }

            ///TODO export to excel logic here
            ///

            var splicesResult = await _constructionStageService.GetBranchableSpliceOverview(branchableId);
            IEnumerable<SpliceEntryModel> splices = splicesResult.Splices;
            List< SpliceEntryModel> splicesToPrint = splices.Where(x => x.Type != SpliceTypes.LoopUntouched).ToList();
            List<string> headings = new List<string> { "Kabel 1 Alias", "F 1", "Nr 1", "F/BA", "Kabel 2 Alias", "F 2", "Nr 2", "F/BA", "S-Platz", "Kas", "Bem." };

            MemoryStream memoryStream = new MemoryStream(4 * 1024);


            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet workSheet = pck.Workbook.Worksheets.Add("Spleißübersicht");

                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = splicesResult.Branchable.Name;
                workSheet.Cells["A1:K1"].Merge = true;

                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(2).Style.Font.Bold = true;

                for (int i = 1; i < headings.Count + 1; i++)
                {
                    workSheet.Cells[2, i].Value = headings[i - 1];
                }

                fillWorkSheetWithData(workSheet, splicesToPrint);
                using (ExcelRange range = workSheet.Cells[2, 1, workSheet.Dimension.End.Row, workSheet.Dimension.End.Column])
                {
                    var table = workSheet.Cells[range.ToString()];
                    table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    foreach (var cell in range)
                    {
                        if (cell.Style.Border.Top.Style == ExcelBorderStyle.None)
                        {
                            cell.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        }
                    }
                }
                
                // Druckeinstellungen
                workSheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$2");
                workSheet.PrinterSettings.PaperSize = ePaperSize.A4;
                workSheet.PrinterSettings.Orientation = eOrientation.Portrait;
                workSheet.PrinterSettings.LeftMargin = .05M;
                workSheet.PrinterSettings.RightMargin = .05M;
                workSheet.PrinterSettings.TopMargin = .05M;
                workSheet.PrinterSettings.FooterMargin = .05M;
                workSheet.PrinterSettings.Scale = 80;

                pck.SaveAs(memoryStream);
                memoryStream.Position = 0;
            }

            Byte[] result = memoryStream.ToArray(); // TODO: oder war das eine Vorgabe?: new Byte[1000];
            memoryStream.Dispose();
            String fileName = $"{serviceResult.Branchable.Name}.xlsx";
            String mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            return new NamedExportFile
            {
                Data = result,
                Name = fileName,
                MimeType = mimeType
            };
        }


        private void fillWorkSheetWithData(ExcelWorksheet workSheet, List<SpliceEntryModel> splicesToPrint)
        {
            // Test: http://localhost:63133/api/ConstructionStages/SpliveOverviewAsExcel/122
            int recordIndex = 3;

            IOrderedEnumerable<SpliceEntryModel> splicesToPrintOrdered = splicesToPrint.OrderBy(x => x.Tray); //TODO: ehem. TrayNumber?

            string tempKvzName1 = string.Empty;
            string currentAlias1 = string.Empty;
            int currentTrayNumber = 0;
            foreach (SpliceEntryModel item in splicesToPrintOrdered)
            {
                currentAlias1 = item.FirstFiber.Name;
                if (currentAlias1.Trim().ToLowerInvariant().StartsWith("kvz"))
                {
                    if (item.FirstFiber.Name == tempKvzName1)
                    {
                        //Kabel 1 Alias
                        workSheet.Cells[recordIndex, 1].Value = string.Empty;
                    }
                    else
                    {
                        //Kabel 1 Alias
                        workSheet.Cells[recordIndex, 1].Value = getKabelAliasNameFiltered(item.FirstFiber.Name);
                        tempKvzName1 = item.FirstFiber.Name;
                    }

                    // Nr. 1
                    workSheet.Cells[recordIndex, 3].Value = item.FirstFiber.FiberNumber.ToString();
                    string faserBaColor = getFaserBaColor(item.FirstFiber.Color);
                    if (!faserBaColor.Contains('/'))
                    {
                        // Faser 1
                        workSheet.Cells[recordIndex, 2].Value = item.FirstFiber.NumberinBundle.ToString();
                        // Faser / BA
                        workSheet.Cells[recordIndex, 4].Value = faserBaColor;
                    }
                    else
                    {
                        // Faser 1
                        workSheet.Cells[recordIndex, 2].Value = String.Format("{0}-{1}", item.FirstFiber.BundleNumber, item.FirstFiber.NumberinBundle);
                        // Faser/BA
                        workSheet.Cells[recordIndex, 4].Value = faserBaColor;
                    }
                }
                else
                {
                    // Kabel 1 Alias
                    workSheet.Cells[recordIndex, 1].Value = getKabelAliasNameFiltered(item.FirstFiber.Name);
                    // Faser 1
                    workSheet.Cells[recordIndex, 2].Value = String.Format("{0}-{1}", item.FirstFiber.BundleNumber, item.FirstFiber.NumberinBundle);
                    // Nr 1
                    workSheet.Cells[recordIndex, 3].Value = item.FirstFiber.FiberNumber;
                    // Faser / BA
                    workSheet.Cells[recordIndex, 4].Value = item.FirstFiber.Color;

                    tempKvzName1 = item.FirstFiber.Name;
                }

                if (item.SecondFiber != null)
                {
                    // Kabel 2 Alias
                    workSheet.Cells[recordIndex, 5].Value = getKabelAliasNameFiltered(item.SecondFiber.Name);

                    if (item.SecondFiber.Name.Trim().ToLowerInvariant().StartsWith("kvz"))
                    {
                        string tmpFaserBaColor = getFaserBaColor(item.SecondFiber.Color);
                        if (!tmpFaserBaColor.Contains('/'))
                        {
                            // Faser 2
                            workSheet.Cells[recordIndex, 6].Value = String.Format("{0}", item.SecondFiber.NumberinBundle);
                            // Faser/BA
                            workSheet.Cells[recordIndex, 8].Value = tmpFaserBaColor;
                        }
                        else
                        {
                            // Faser 2
                            workSheet.Cells[recordIndex, 6].Value = String.Format("{0}-{1}", item.SecondFiber.BundleNumber, item.SecondFiber.NumberinBundle);
                            // Faser/BA
                            workSheet.Cells[recordIndex, 8].Value = tmpFaserBaColor;
                        }
                    }
                    else
                    {
                        // Faser 2
                        workSheet.Cells[recordIndex, 6].Value = String.Format("{0}-{1}", item.SecondFiber.BundleNumber, item.SecondFiber.NumberinBundle);
                        // Faser/BA
                        workSheet.Cells[recordIndex, 8].Value = item.SecondFiber.Color;
                    }
                    // Nr. 2
                    workSheet.Cells[recordIndex, 7].Value = item.SecondFiber.FiberNumber;
                }

                // Spleißplatz
                workSheet.Cells[recordIndex, 9].Value = (item.Number != 0 ? item.Number.ToString() : string.Empty); //TODO: ehem. NumberInTray jetzt Number?
                // Kassette
                workSheet.Cells[recordIndex, 10].Value = item.Tray;

                if (currentTrayNumber != item.Tray)
                {
                    currentTrayNumber = item.Tray;
                    if (recordIndex > 3)
                    {
                        // Linie zum Abgrenzen
                        for (int i = 1; i < 12; i++)
                        {
                            workSheet.Cells[recordIndex, i].Style.Border.Top.Style = ExcelBorderStyle.Thick;

                            
                        }
                    }
                }
                workSheet.Cells[recordIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                workSheet.Cells[recordIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                workSheet.Cells[recordIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                recordIndex++;
            }
            
            workSheet.Column(1).Width = 20;
            workSheet.Column(5).Width = 20;
        }

        private string getKabelAliasNameFiltered(string name)
        {
            if (name.Contains('.'))
            {
                name = name.Split('.').LastOrDefault().ToString();
            }

            return name;
        }

        private string getFaserBaColor(string colorString)
        {
            if (!colorString.ToLowerInvariant().Contains("faser/ba:"))
            {
                return colorString;
            }
            string color = string.Empty;
            color = colorString.Split(':')[1];
            color = color.Split('/')[0];
            return color.Trim();
        }
        #endregion
    }
}
