﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Helper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class FileService : IFileService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly IFileManager _fileUploader;
        private readonly ILogger<FileService> _logger;
        private readonly IProtocolService _protocolService;
        private static Dictionary<String, FileTypes> _mimeTypeMapper;

        public const Int32 MaxQueryAmount = 250;

        #endregion

        #region Constructor

        public FileService(
            IBesaDataStorage storage,
            IFileManager fileUploader,
            ILogger<FileService> logger,
            IProtocolService protocolService)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._fileUploader = fileUploader ?? throw new ArgumentNullException(nameof(fileUploader));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        static FileService()
        {
            _mimeTypeMapper = new Dictionary<string, FileTypes>();

            var assembly = typeof(FileService).GetTypeInfo().Assembly;
            using (Stream resource = assembly.GetManifestResourceStream("Albakom.Besa.Service.Implementation.Helper.mimeTypes.json"))
            {
                using (StreamReader reader = new StreamReader(resource))
                {
                    String jsonInput = reader.ReadToEnd();
                    List<MimeTypeRelationModel> list = JsonConvert.DeserializeObject<List<MimeTypeRelationModel>>(jsonInput);

                    foreach (MimeTypeRelationModel item in list)
                    {
                        if (_mimeTypeMapper.ContainsKey(item.MimeType) == true) continue;

                        _mimeTypeMapper.Add(item.MimeType, item.Type);
                    }
                }
            }
        }

        #endregion

        #region Methods

        private void ValidateAndParseFileModel(FileCreateModel model)
        {
            _logger.LogTrace("validate file model");
            if (model == null)
            {
                _logger.LogInformation("no model was given");
                throw new FileServicException(FileServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check file name");
            if (String.IsNullOrEmpty(model.Name) == true || model.Name.Length > _storage.Constraints.MaxFileNameChars)
            {
                _logger.LogTrace("invalid file name");
                throw new FileServicException(FileServicExceptionReasons.InvalidModel);
            }

            _logger.LogTrace("check mime type");
            if (String.IsNullOrEmpty(model.MimeType) == true)
            {
                _logger.LogInformation("no mime type was set");
                throw new FileServicException(FileServicExceptionReasons.InvalidModel);
            }

            _logger.LogTrace("check extention");
            if (String.IsNullOrEmpty(model.Extention) == true)
            {
                _logger.LogInformation("no file extention was given");
                throw new FileServicException(FileServicExceptionReasons.InvalidModel);
            }

            if (model.Extention.StartsWith(".") == true)
            {
                model.Extention = model.Extention.Substring(1);
            }
        }

        private FileTypes GetFileTypeByMimeType(String mimeType)
        {
            String parsedType = mimeType.ToLower();
            if (_mimeTypeMapper.ContainsKey(parsedType) == false)
            {
                return FileTypes.Unknown;
            }
            else
            {
                return _mimeTypeMapper[parsedType];
            }
        }

        private async Task CheckUserAuthId(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new FileServicException(FileServicExceptionReasons.UserNotFound);
            }
        }

        public async Task<Int32> CreateFile(FileCreateModel model, Stream fileData, String userAuthId)
        {
            _logger.LogTrace("CreateFile");
            ValidateAndParseFileModel(model);
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("check file stream");
            if (fileData == null || fileData.CanRead == false)
            {
                _logger.LogInformation("invalid file stream");
                throw new FileServicException(FileServicExceptionReasons.InvalidFileData);
            }

            if (fileData.CanSeek == true && fileData.Position > 0)
            {
                fileData.Seek(0, SeekOrigin.Begin);
            }

            model.Type = GetFileTypeByMimeType(model.MimeType);

            model.StorageUrl = await _fileUploader.Upload(model, fileData, null);
            model.CreatedAt = DateTimeOffset.Now;

            Int32 id = await _storage.CreateFile(model);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Create, MessageRelatedObjectTypes.File, id), userAuthId);

            return id;
        }

        public async Task<FileWithStreamModel> GetFileWithStream(Int32 fileId)
        {
            _logger.LogTrace("GetFileWithStream. fileId: {0}", fileId);

            _logger.LogTrace("check if file with id {0} exits", fileId);
            if (await _storage.CheckIfFileExists(fileId) == false)
            {
                _logger.LogInformation("file with id {0} not found", fileId);
                throw new FileServicException(FileServicExceptionReasons.NotFound);
            }

            FileModel fileModel = await _storage.GetFileInfo(fileId);
            Stream data = await _fileUploader.Download(fileModel.StorageUrl, fileModel.Size);
            if (data.CanRead == false)
            {
                throw new FileServicException(FileServicExceptionReasons.FileNotSaved);
            }

            if (data.CanSeek == true && data.Position != 0)
            {
                data.Seek(0, SeekOrigin.Begin);
            }

            FileWithStreamModel result = new FileWithStreamModel
            {
                Id = fileModel.Id,
                Data = data,
                Extention = fileModel.Extention,
                MimeType = fileModel.MimeType,
                Name = fileModel.Name
            };

            return result;
        }

        private void ValidateStartAndAmount(Int32 start, Int32 amount)
        {

            if (start < 0)
            {
                _logger.LogInformation("start should be greater or equal to zero");
                throw new FileServicException(FileServicExceptionReasons.InvalidParameter);
            }

            if (amount <= 0 || amount > MaxQueryAmount)
            {
                _logger.LogInformation("amount is greater then MaxQueryAmount ({0})", MaxQueryAmount);
                throw new FileServicException(FileServicExceptionReasons.InvalidParameter);
            }
        }

        public async Task<IEnumerable<FileOverviewModel>> GetFiles(Int32 start, Int32 amount)
        {
            _logger.LogTrace("Get files. start {0}; amount {1}", start, amount);
            ValidateStartAndAmount(start, amount);

            IEnumerable<FileOverviewModel> result = await _storage.GetFilesSortByName(start, amount);
            return result;
        }

        public async Task<IEnumerable<FileOverviewModel>> SearchFiles(String query, Int32 start, Int32 amount)
        {
            _logger.LogTrace("Search files. query: {0}; start {1}; amount {2}", query, start, amount);

            ValidateStartAndAmount(start, amount);

            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogWarning("search files without query. empty result returned");
                return new List<FileOverviewModel>();
            }

            IEnumerable<FileOverviewModel> result = await _storage.SearchFiles(query, start, amount);
            return result;
        }

        public async Task<IEnumerable<FileObjectOverviewModel>> SearchObjectForAccess(FileAccessObjects type, String query, Int32 amount)
        {
            _logger.LogTrace("SearchObjectForAccess. query: {0};  amount {1}; type {2}", query, amount, type);

            if (amount <= 0 || amount > MaxQueryAmount)
            {
                _logger.LogInformation("amount is greater then MaxQueryAmount ({0})", MaxQueryAmount);
                throw new FileServicException(FileServicExceptionReasons.InvalidParameter);
            }

            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogWarning("search files without query. empty result returned");
                return new List<FileObjectOverviewModel>();
            }

            if (type == FileAccessObjects.Building)
            {
                IEnumerable<SimpleBuildingOverviewModel> preResult = await _storage.SearchStreets(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Id,
                    Name = x.StreetName,
                });
            }
            else if (type == FileAccessObjects.BranchOffJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBuildingsAndGetBranchOffJobIds(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.HouseConnectionJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBuildingsAndGetConnenctionJobIds(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.InjectJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBuildingsAndGetInjectJobIds(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.PrepareBranchableJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBranchablesAndGetPrepareJobId(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.SpliceInBranchableJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBuildingsAndGetSplicesJobsIds(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.SpliceInPopJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchMainCablesAndGetODFSpliceJobIds(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
            }
            else if (type == FileAccessObjects.ActivationJob)
            {
                Dictionary<Int32, String> preResult = await _storage.SearchBuildingsAndGetActivationJobId(query, amount);
                return preResult.Select(x => new FileObjectOverviewModel
                {
                    Id = x.Key,
                    Name = x.Value,
                });
                throw new FileServicException(FileServicExceptionReasons.NotImplemented);
            }
            else
            {
                throw new FileServicException(FileServicExceptionReasons.NotImplemented);
            }
        }

        private async Task CheckIfFileExists(Int32 fileId)
        {

            _logger.LogTrace("check if file with id {0} exits", fileId);
            if (await _storage.CheckIfFileExists(fileId) == false)
            {
                _logger.LogInformation("file with id {0} not found", fileId);
                throw new FileServicException(FileServicExceptionReasons.NotFound);
            }
        }

        public async Task<FileDetailModel> GetDetails(Int32 fileId)
        {
            _logger.LogTrace("GetDetails. fileId {0}", fileId);
            await CheckIfFileExists(fileId);

            FileDetailModel result = await _storage.GetFileDetails(fileId);
            return result;
        }

        private async Task<Boolean> CheckFileAccessRelatedObjectExists(FileAccessObjects type, Int32 id)
        {
            Boolean exist = false;
            if (type == FileAccessObjects.Building)
            {
                exist = await _storage.CheckIfBuildingExists(id);
            }
            else if (type == FileAccessObjects.BranchOffJob)
            {
                exist = await _storage.CheckIfBranchOffJobExist(id);
            }
            else if (type == FileAccessObjects.HouseConnectionJob)
            {
                exist = await _storage.CheckIfBuildingConnenctionJobExist(id);
            }
            else if (type == FileAccessObjects.InjectJob)
            {
                exist = await _storage.CheckIfInjectJobExist(id);
            }

            return exist;
        }

        public async Task<FileDetailModel> EditFile(FileEditModel model, String userAuthId)
        {
            _logger.LogTrace("EditFile");

            _logger.LogTrace("check if model is null");
            if (model == null)
            {
                _logger.LogInformation("no model was given");
                throw new FileServicException(FileServicExceptionReasons.NoModel);
            }

            if (String.IsNullOrEmpty(model.Name) || model.Name.Length > _storage.Constraints.MaxFileNameChars)
            {
                _logger.LogInformation("invalid model");
                throw new FileServicException(FileServicExceptionReasons.InvalidModel);
            }

            await CheckIfFileExists(model.Id);
            await CheckUserAuthId(userAuthId);

            _logger.LogTrace("rename file to {0}", model.Name);
            await _storage.RenameFile(model.Id, model.Name);

            _logger.LogTrace("check if access list should be cleared");
            if (model.AccessList == null || model.AccessList.Count() == 0)
            {
                await _storage.DeleteFileAccess(model.Id);
                _logger.LogTrace("access list cleared");
            }
            else
            {
                _logger.LogTrace("getting current access list");
                ICollection<FileAccessWithIdModel> accessListInDb = await _storage.GetFileAccessList(model.Id);

                foreach (FileAccessModel item in model.AccessList)
                {
                    FileAccessWithIdModel existingModel = accessListInDb.FirstOrDefault(x => x.ObjectId == item.ObjectId && x.ObjectType == item.ObjectType);
                    if (existingModel == null)
                    {
                        if (await CheckFileAccessRelatedObjectExists(item.ObjectType, item.ObjectId) == false)
                        {
                            _logger.LogInformation("object type {0} with id {1} not found", item.ObjectType, item.ObjectId);
                            throw new FileServicException(FileServicExceptionReasons.NotFound);
                        }

                        _logger.LogTrace("adding access entry. type: {0}, id {1}", item.ObjectType, item.ObjectId);
                        await _storage.AddFileAccessEntry(model.Id, item);
                    }
                    else
                    {
                        _logger.LogTrace("exisiting element with type {0} and id {1} found", existingModel.ObjectType, existingModel.ObjectId);
                        accessListInDb.Remove(existingModel);
                    }
                }

                if (accessListInDb.Count > 0)
                {
                    _logger.LogTrace("remove access entries");
                    await _storage.DeleteFileAcessEntries(accessListInDb.Select(x => x.Id));
                }
            }

            FileDetailModel result = await _storage.GetFileDetails(model.Id);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Update, MessageRelatedObjectTypes.File, model.Id), userAuthId);

            return result;
        }

        public async Task RemoveOrphansAccessEntries()
        {
            _logger.LogTrace("RemoveOrphansAccessEntries");

            IEnumerable<FileAccessWithIdModel> accessEntries = await _storage.GetAllAccessEntries();

            foreach (FileAccessWithIdModel entry in accessEntries)
            {
                Boolean exists = await CheckFileAccessRelatedObjectExists(entry.ObjectType, entry.ObjectId);
                if (exists == false)
                {
                    await _storage.DeleteFileAcessEntry(entry.Id);
                }
            }
        }

        public async Task<Boolean> DeleteFile(Int32 fileId, String userAuthId)
        {
            _logger.LogTrace("DeleteFile. fileId: {0}", fileId);

            await CheckIfFileExists(fileId);
            await CheckUserAuthId(userAuthId);

            String fileUrl = await _storage.GetFileUrlById(fileId);
            await _fileUploader.Delete(new String[] { fileUrl });

            Boolean result = await _storage.DeleteFile(fileId);

            if (result == true)
            {
                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel(MessageActions.Delete, MessageRelatedObjectTypes.File, fileId), userAuthId);
            }

            return result;
        }


        #endregion
    }
}
