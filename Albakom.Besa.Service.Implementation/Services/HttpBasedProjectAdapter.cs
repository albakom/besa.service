﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class HttpBasedProjectAdapter : IProjectAdapter
    {
        #region Fields

        private enum ProjectServiceEndpoints
        {
            AllBuildings,
            AllCabintes,
            AllSleeves,
            AllCableTypes,
            UpdateBuildingWithCableInfo,
            UpdateBuildingName,
            AllPoPs,
            CableByBranchable,
            MainCableByPoPId,
            ConnectionByCableId,
            SplicesByBranchable,
            MainCableByBranchable,
        }

        private readonly IHttpHandler handler;
        private readonly ITokenClient tokenClient;
        private readonly ILogger<HttpBasedProjectAdapter> logger;

        private Dictionary<ProjectServiceEndpoints, String> uris;

        #endregion

        #region Constructor

        public HttpBasedProjectAdapter(
            String serviceUrl,
            IHttpHandler handler,
            ITokenClient tokenClient,
            ILogger<HttpBasedProjectAdapter> logger)
        {
            if (String.IsNullOrEmpty(serviceUrl) == true) { throw new ArgumentNullException(nameof(handler)); }
            this.handler = handler ?? throw new ArgumentNullException(nameof(handler));
            this.tokenClient = tokenClient ?? throw new ArgumentNullException(nameof(tokenClient));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));

            this.SetUris(serviceUrl);
        }

        #endregion

        #region Methods

        private void SetUris(String baseUri)
        {
            if (baseUri.EndsWith("/") == true)
            {
                baseUri = baseUri.Substring(0, baseUri.Length - 1);
            }

            uris = new Dictionary<ProjectServiceEndpoints, string>();
            uris.Add(ProjectServiceEndpoints.AllBuildings, $"{baseUri}/api/v1/Buildings/All");
            uris.Add(ProjectServiceEndpoints.AllCabintes, $"{baseUri}/api/v1/Cabinents/All");
            uris.Add(ProjectServiceEndpoints.AllSleeves, $"{baseUri}/api/v1/Sleeves/All");
            uris.Add(ProjectServiceEndpoints.AllCableTypes, $"{baseUri}/api/v1/Cables/AllTypes");
            uris.Add(ProjectServiceEndpoints.UpdateBuildingWithCableInfo, $"{baseUri}/api/v1/Buildings/UpdateWithCable");
            uris.Add(ProjectServiceEndpoints.UpdateBuildingName, $"{baseUri}/api/v1/Buildings/UpdateName");
            uris.Add(ProjectServiceEndpoints.AllPoPs, $"{baseUri}/api/v1/PoP/All");
            uris.Add(ProjectServiceEndpoints.CableByBranchable, $"{baseUri}/api/v1/Cables/CableByBranchableId");
            uris.Add(ProjectServiceEndpoints.MainCableByPoPId, $"{baseUri}/api/v1/Cables/MainCableStartingFromPop");
            uris.Add(ProjectServiceEndpoints.ConnectionByCableId, $"{baseUri}/api/v1/Connenction/ConnectionByCableId");
            uris.Add(ProjectServiceEndpoints.SplicesByBranchable, $"{baseUri}/api/v1/Splices/ByBranchableId");
            uris.Add(ProjectServiceEndpoints.MainCableByBranchable, $"{baseUri}/api/v1/Cables/MainCableForBranchable");
        }

        private ServiceErrros GetErrorFromResponse(HttpResponseMessage responseMessage)
        {
            ServiceErrros error = ServiceErrros.NotSpecified;
            switch (responseMessage.StatusCode)
            {
                case System.Net.HttpStatusCode.Forbidden:
                case System.Net.HttpStatusCode.Unauthorized:
                    error = ServiceErrros.NotAuthorize;
                    break;
                case System.Net.HttpStatusCode.InternalServerError:
                    error = ServiceErrros.UnknowError;
                    break;
                case System.Net.HttpStatusCode.NotFound:
                    error = ServiceErrros.NotFound;
                    break;
                default:
                    break;
            }
            logger.LogTrace("status code {0} was translate to error {1}", responseMessage.StatusCode, error);
            return error;
        }

        private async Task<Boolean> PrepareToken()
        {
            logger.LogTrace("checking for token");

            if (tokenClient.IsReady() == false)
            {
                logger.LogTrace("token service is not ready. Trying to prepare");
                Boolean result = await tokenClient.Prepare();
                if (result == false)
                {
                    logger.LogInformation("unable to get token response");
                    return false;
                }
                else
                {
                    logger.LogTrace("token service responds");
                }
            }
            else
            {
                logger.LogTrace("token service is ready");
            }

            logger.LogTrace("check if token is saved");
            if (tokenClient.HasToken() == false)
            {
                logger.LogTrace("no token found. Trying to get one");
                Boolean tokenResult = await tokenClient.RequestToken();
                if (tokenResult == false)
                {
                    logger.LogTrace("unable to get token");
                    return false;
                }
            }

            logger.LogTrace("get token. {0}", tokenClient.GetToken());
            return true;
        }

        private async Task<Boolean> PrepareClientForToken()
        {
            Boolean tokenResult = await PrepareToken();
            if (tokenResult == false)
            {
                return false;
            }
            else
            {
                String tokenValue = tokenClient.GetToken();
                handler.AddHeader("Authorization", $"Bearer {tokenValue}");
                return true;
            }
        }

        private async Task<ServiceResult<T>> GetResult<T>(ProjectServiceEndpoints endpoint, String uriPathValue, String queryValues)
        {
            Boolean tokenPreperationSuccess = await PrepareClientForToken();
            if (tokenPreperationSuccess == false)
            {
                return new ServiceResult<T>(ServiceErrros.TokenUnvaible);
            }

            try
            {
                String url = uris[endpoint];
                if (String.IsNullOrEmpty(uriPathValue) == false)
                {
                    url += "/" + uriPathValue;
                }
                if (String.IsNullOrEmpty(queryValues) == false)
                {
                    url += "?" + queryValues;
                }

                logger.LogTrace("sending http get request to {0}", url);
                HttpResponseMessage responseMessage = await handler.GetAsync(url);

                if (responseMessage.IsSuccessStatusCode == true)
                {
                    logger.LogTrace("response is valid");
                    T content = await responseMessage.Content.ReadAsAsync<T>();
                    return new ServiceResult<T>(content);
                }
                else
                {
                    //  String temp = await responseMessage.Content.ReadAsStringAsync();

                    logger.LogInformation("http request failed with status code {0}", responseMessage.StatusCode);
                    ServiceErrros error = GetErrorFromResponse(responseMessage);
                    return new ServiceResult<T>(error);
                }
            }
            catch (Exception)
            {
                return new ServiceResult<T>(ServiceErrros.UnknowError);
            }
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterBuildingModel>>> GetAllBuildings()
        {
            logger.LogTrace("GetAllBuildings()");

            ServiceResult<IEnumerable<ProjectAdapterBuildingModel>> result =
                await GetResult<IEnumerable<ProjectAdapterBuildingModel>>(ProjectServiceEndpoints.AllBuildings, null, null);

            return result;
        }

        private async Task<ServiceResult<IEnumerable<T>>> GetParts<T>(ProjectServiceEndpoints endpoint, Int32 amount)
        {
            List<T> accumulatedResult = new List<T>();

            Int32 currentStart = 0;

            while (true)
            {
                String query = $"start={currentStart}&amount={amount}";

                ServiceResult<IEnumerable<T>> result =
    await GetResult<IEnumerable<T>>(endpoint, null, query);
                if (result.HasError == true)
                {
                    logger.LogInformation("result has error");
                    return result;
                }

                accumulatedResult.AddRange(result.Data);

                Int32 dataCount = result.Data.Count();
                if (dataCount < amount)
                {
                    logger.LogTrace("less elements then requested. End reached");
                    break;
                }
                else if (dataCount > amount)
                {
                    logger.LogTrace("more items then required. Request ends");
                    break;
                }

                logger.LogTrace("getting next items");
                currentStart += amount;
            }

            return new ServiceResult<IEnumerable<T>>(accumulatedResult);

        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>> GetAllCabinets()
        {
            logger.LogTrace("GetAllCabinets()");

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await GetParts<ProjectAdapterCabinetModel>(ProjectServiceEndpoints.AllCabintes, 5);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>> GetAllSleeves()
        {
            logger.LogTrace("GetAllSleeves()");

            ServiceResult<IEnumerable<ProjectAdapterSleeveModel>> result = await GetParts<ProjectAdapterSleeveModel>(ProjectServiceEndpoints.AllSleeves, 5);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>>> GetAllCableTypes()
        {
            logger.LogTrace("GetAllCables()");

            ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>> result = await GetResult<IEnumerable<ProjectAdapterCableTypeModel>>(ProjectServiceEndpoints.AllCableTypes, null, null);
            return result;
        }

        public async Task<ServiceResult<ProjectAdapterUpdateBuldingCableModel>> GetBuildingCableInfo(String projectAdapterId)
        {
            logger.LogTrace("UpdateBuildingWithCableInfo. projectAdapterId: {0}", projectAdapterId);

            ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await GetResult<ProjectAdapterUpdateBuldingCableModel>(ProjectServiceEndpoints.UpdateBuildingWithCableInfo, projectAdapterId, null);
            return result;
        }

        public async Task<ServiceResult<ProjectAdapterUpdateBuildingNameModel>> GetBuildingNameUpdate(String projectAdapterId)
        {
            logger.LogTrace("GetBuildingCableInfo. projectAdapterId: {0}", projectAdapterId);

            ServiceResult<ProjectAdapterUpdateBuildingNameModel> result = await GetResult<ProjectAdapterUpdateBuildingNameModel>(ProjectServiceEndpoints.UpdateBuildingName, projectAdapterId, null);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterPoPModel>>> GetAllPoPs()
        {
            logger.LogTrace("GetAllPoPs()");
            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await GetResult<IEnumerable<ProjectAdapterPoPModel>>(ProjectServiceEndpoints.AllPoPs, null, null);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterCableModel>>> GetCableByBranchableId(String projectAdapterId)
        {
            logger.LogTrace("GetCableByBranchableId(projectAdapterId: {0})", projectAdapterId);
            ServiceResult<IEnumerable<ProjectAdapterCableModel>> result = await GetResult<IEnumerable<ProjectAdapterCableModel>>(ProjectServiceEndpoints.CableByBranchable, projectAdapterId, null);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<String>>> GetMainCableStartingFromPop(String projectAdapterId)
        {
            logger.LogTrace("GetMainCableStartingFromPop(projectAdapterId: {0})", projectAdapterId);

            ServiceResult<IEnumerable<String>> result = await GetResult<IEnumerable<String>>(ProjectServiceEndpoints.MainCableByPoPId, projectAdapterId, null);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterPatchModel>>> GetConnectionByCableId(String cableProjectAdatperID)
        {
            logger.LogTrace("GetConnectionByCableId(cableProjectAdatperID: {0})", cableProjectAdatperID);

            ServiceResult<IEnumerable<ProjectAdapterPatchModel>> result = await GetResult<IEnumerable<ProjectAdapterPatchModel>>(ProjectServiceEndpoints.ConnectionByCableId, cableProjectAdatperID, null);
            return result;
        }

        public async Task<ServiceResult<IEnumerable<ProjectAdapterSpliceModel>>> GetSplicesForBranchable(String branchableProjectAdapterId)
        {
            logger.LogTrace("GetConnectionByCableId(branchableProjectAdapterId: {0})", branchableProjectAdapterId);

            ServiceResult<IEnumerable<ProjectAdapterSpliceModel>> result = await GetResult<IEnumerable<ProjectAdapterSpliceModel>>(ProjectServiceEndpoints.SplicesByBranchable, branchableProjectAdapterId, null);
            return result;
        }

        public async Task<ServiceResult<ProjectAdapterMainCableForBranchableResult>> GetMainCableForBranchable(String branchableProjectAdapterId)
        {
            logger.LogTrace("GetMainCableForBranchable(branchableProjectAdapterId: {0})", branchableProjectAdapterId);

            ServiceResult< ProjectAdapterMainCableForBranchableResult > result = await GetResult< ProjectAdapterMainCableForBranchableResult > (ProjectServiceEndpoints.MainCableByBranchable, branchableProjectAdapterId, null);
            return result;
        }


        #endregion

    }
}
