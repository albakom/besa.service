﻿using Albakom.Besa.Service.Contracts.Service;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class HttpClientBasedHttpHandler : HttpClient, IHttpHandler
    {
        public void AddHeader(String header, String value)
        {
            if(this.DefaultRequestHeaders.Contains(header) == true)
            {
                this.DefaultRequestHeaders.Remove(header);
            }

            this.DefaultRequestHeaders.Add(header, value);
        }
    }
}
