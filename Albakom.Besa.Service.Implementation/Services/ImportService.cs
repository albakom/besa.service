﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ImportService : IImportService
    {
        #region Fields

        private IBesaDataStorage _storage;
        private ILogger<ImportService> _logger;

        private const String _defaultImportProcedureTimelineNote = "Durch Import erstellt";

        #endregion

        #region Constructor

        public ImportService(
            IBesaDataStorage storage,
            ILogger<ImportService> logger)
        {
            _storage = storage;
            _logger = logger;
        }

        #endregion

        #region Methods

        private void CheckImportProcecureModel(CSVProcedureImportInfoModel importInfo)
        {
            _logger.LogTrace("ImportProcedures");
            if (importInfo.Lines == null || importInfo.Lines.Count == 0)
            {
                _logger.LogInformation("no import data was given");
                throw new ImportServicException(ImportExceptionReasons.NoData);
            }

            _logger.LogTrace("check import info model");
            if (importInfo == null || importInfo.Mappings == null)
            {
                _logger.LogInformation("no import informationen was given");
                throw new ImportServicException(ImportExceptionReasons.NoImportInfo);
            }

            _logger.LogTrace("check if all needed properties are mappend");
            List<ProcedureImportProperties> neededProperties = new List<ProcedureImportProperties> { ProcedureImportProperties.Street, ProcedureImportProperties.StreetNumber };
            foreach (ProcedureImportProperties item in neededProperties)
            {
                if (importInfo.Mappings.ContainsKey(item) == false)
                {
                    _logger.LogInformation("property {0} not found in mapping", item);
                    throw new ImportServicException(ImportExceptionReasons.InvalidImportModel);
                }
            }

            if ((importInfo.Mappings.ContainsKey(ProcedureImportProperties.ContactId) == false || (importInfo.Mappings.ContainsKey(ProcedureImportProperties.ContactLastname) && importInfo.Mappings.ContainsKey(ProcedureImportProperties.ContactLastname))) == false)
            {
                _logger.LogInformation("no property for contact found");
                throw new ImportServicException(ImportExceptionReasons.InvalidImportModel);
            }
        }

        private String GetValue(String[] line, ProcedureImportProperties properties, CSVProcedureImportInfoModel importInfoModel)
        {
            return GetValue(line, properties, importInfoModel, String.Empty);
        }

        private String GetValue(String[] line, ProcedureImportProperties properties, CSVProcedureImportInfoModel importInfoModel, String defaultValue)
        {
            if (importInfoModel.Mappings.ContainsKey(properties) == false) { return defaultValue; }

            Int32 index = importInfoModel.Mappings[properties];
            if (line.Length <= index) { return defaultValue; }

            String value = line[index];
            return value;
        }

        private async Task<PersonInfo> GetContactFromImport(Boolean contactIdMappend, String[] currentLine, List<CSVProcedureImportError> errors, Int32 itemIndex, CSVProcedureImportInfoModel importInfo)
        {
            PersonInfo contact = null;
            if (contactIdMappend == true)
            {
                Int32 contactId = 0;
                Int32 contactIdIndex = importInfo.Mappings[ProcedureImportProperties.ContactId];
                if (currentLine.Length <= contactIdIndex)
                {
                    errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.ContactNotReadable, Description = "IndexOutOfRange" });

                }
                else
                {
                    if (Int32.TryParse(currentLine[contactIdIndex], out contactId) == false)
                    {
                        errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.ContactNotReadable, Description = "IndexOutOfRange" });
                    }
                    else if (await _storage.CheckIfContactExists(contactId) == false)
                    {
                        errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.ContactNotFound, Description = "contact not found in database" });
                    }
                    else
                    {
                        contact = await _storage.GetContactById(contactId);
                    }
                }
            }
            else
            {
                Int32 surnameIndex = importInfo.Mappings[ProcedureImportProperties.ContactSurname];
                Int32 lastnameIndex = importInfo.Mappings[ProcedureImportProperties.ContactLastname];

                if (currentLine.Length <= surnameIndex || currentLine.Length <= lastnameIndex)
                {
                    errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.ContactNotReadable, Description = "IndexOutOfRange" });
                }
                else
                {
                    String surname = currentLine[surnameIndex];
                    String lastname = currentLine[lastnameIndex];

                    if (await _storage.CheckIfContactExists(surname, lastname) == false)
                    {
                        errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.ContactNotFound, Description = "contact not found in database" });
                    }
                    else
                    {
                        contact = await _storage.GetContactByName(surname, lastname);
                    }
                }
            }

            return contact;
        }

        private async Task<Boolean> GetBuildingByQuery(String streetName, String streetNumber)
        {
            String streetquery = $"{streetName.Trim()} {streetNumber.Trim()}";

            Boolean result = await _storage.CheckIfBuildingExists(streetquery);
            return result;
        }

        private async Task<SimpleBuildingOverviewModel> GetBuildingId(String[] currentLine, List<CSVProcedureImportError> errors, Int32 itemIndex, CSVProcedureImportInfoModel importInfo)
        {
            String streetName = GetValue(currentLine, ProcedureImportProperties.Street, importInfo);
            if (String.IsNullOrEmpty(streetName) == true)
            {
                errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.StreetNotReadable, Description = "street not found in import" });
                return null;
            }
            else
            {
                streetName = streetName.Trim();
            }

            String streetNumber = GetValue(currentLine, ProcedureImportProperties.StreetNumber, importInfo);
            if (String.IsNullOrEmpty(streetNumber) == true)
            {
                errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.StreetNumberNotReadable, Description = "number not found in import" });
                return null;
            }
            else
            {
                streetNumber = streetNumber.Trim();
            }

            while (streetNumber[0] == '0')
            {
                streetNumber = streetNumber.Substring(1);
            }

            Int32 streetNumberCharCount = streetNumber.Length;
            Char lastStreetNumberChar = streetNumber[streetNumberCharCount - 1];
            Boolean hasAdressSuffix = Char.IsLetter(lastStreetNumberChar) == true;
            if (hasAdressSuffix == true && streetNumber[streetNumberCharCount - 2] != ' ')
            {
                streetNumber = streetNumber.Substring(0, streetNumberCharCount - 1) + ' ' + lastStreetNumberChar;
            }

            Boolean buildingExists = await GetBuildingByQuery(streetName, streetNumber);
            if (buildingExists == false)
            {
                if (streetName.ToLower().EndsWith("str.") == true)
                {
                    streetName = streetName.Substring(0, streetName.Length - 4) + "Straße";
                    buildingExists = await GetBuildingByQuery(streetName, streetNumber);
                }

                if (buildingExists == false && hasAdressSuffix == true)
                {
                    streetNumber = streetNumber.Substring(0, streetNumber.Length - 2) + lastStreetNumberChar;
                    buildingExists = await GetBuildingByQuery(streetName, streetNumber);
                }
            }

            if (buildingExists == false)
            {
                errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.BuildingNotFound, Description = "building not found" });
                return null;
            }
            else
            {
                String streetquery = $"{streetName.Trim()} {streetNumber.Trim()}";
                SimpleBuildingOverviewModel buildingId = await _storage.GetBuildingByStreetName(streetquery);
                return buildingId;
            }
        }

        private async Task<Int32> GetFlatId(Int32 buildingId)
        {
            IEnumerable<SimpleFlatOverviewModel> flats = await _storage.GetFlatsByBuildingId(buildingId);
            Int32 flatId = 0;
            if (flats.Count() == 0)
            {
                FlatCreateModel flatCreateModel = new FlatCreateModel { BuildingId = buildingId, Number = "Wohnung für Import", Description = "Erstellt durch Import" };
                flatId = await _storage.CreateFlat(flatCreateModel);
            }
            else
            {
                flatId = flats.First().Id;
            }

            return flatId;
        }

        private DateTimeOffset? GetAppointment(String[] line, CSVProcedureImportInfoModel importInfo)
        {
            CultureInfo info = CultureInfo.GetCultureInfo("de-DE");

            DateTimeOffset? appointment = null;
            String appointRawValue = GetValue(line, ProcedureImportProperties.Appointment, importInfo);
            if (String.IsNullOrEmpty(appointRawValue) == false)
            {
                DateTimeOffset appointmentParsed = new DateTimeOffset();
                if (DateTimeOffset.TryParse(appointRawValue, info.DateTimeFormat, DateTimeStyles.None, out appointmentParsed) == true)
                {
                    appointment = appointmentParsed;
                }
            }

            return appointment;
        }

        private CustomerConnectionProcedureCreateModel GetCustomerConnectionProcedureCreateModel(DateTimeOffset? appointment, Boolean activationAsSoonAsPossible, Int32 flatId, PersonInfo contact, SimpleBuildingOverviewModel building, Boolean hasAggrement, Int32 itemIndex, List<CSVProcedureImportError> errors)
        {
            if (appointment.HasValue == false && activationAsSoonAsPossible == false)
            {
                errors.Add(new CSVProcedureImportError { ItemIndex = itemIndex, Type = ErrorTypes.AppointmentNotReadable, Description = "appointment is not readable" });
                return null;
            }

            CustomerConnectionProcedureCreateModel procedureCreate = new CustomerConnectionProcedureCreateModel
            {
                Appointment = appointment,
                AsSoonAsPossible = activationAsSoonAsPossible,
                ContractInStock = true,
                CustomerContactId = contact.Id,
                DeclarationOfAggrementInStock = hasAggrement,
                FlatId = flatId,
                Name = CustomerService.GetNameForConnectCustomerProcedure(building.StreetName, contact)
            };

            return procedureCreate;
        }

        private async Task CreateContactAfterFinishedJob(Int32 flatId, Int32 buildingId, Int32 procedureId)
        {
            CreateContactAfterFinishedJobModel createContactAfterFinishedJobModel = new CreateContactAfterFinishedJobModel
            {
                FlatId = flatId,
                BuildingId = buildingId,
                RelatedProcedureId = procedureId,
            };

            await _storage.CreateContactAfterFinishedJob(createContactAfterFinishedJobModel);
        }

        public async Task<CSVImportProcedureResultModel> ImportProcedures(CSVProcedureImportInfoModel importInfo, String userAuthId)
        {
            CheckImportProcecureModel(importInfo);

            List<CSVProcedureImportError> errors = new List<CSVProcedureImportError>();
            List<Int32> jobIds = new List<int>();
            List<Int32> jobFinisehdIds = new List<int>();
            List<Int32> procedureIds = new List<int>();
            List<Int32> proceduresTimelineElements = new List<int>();

            Boolean importErrorHappend = false;
            try
            {
                Boolean contactIdMappend = importInfo.Mappings.ContainsKey(ProcedureImportProperties.ContactId);

                for (int i = 0; i < importInfo.Lines.Count; i++)
                {
                    String[] currentLine = importInfo.Lines[i];
                    if (currentLine.Length < importInfo.Mappings.Count) { continue; }

                    _logger.LogTrace("try to get contact");
                    PersonInfo contact = await GetContactFromImport(contactIdMappend, currentLine, errors, i, importInfo);
                    if (contact == null)
                    {
                        _logger.LogInformation("contact not found");
                        if (importInfo.SkipError == true)
                        {
                            _logger.LogTrace("skipping error. go to next line");
                            continue;
                        }
                        else
                        {
                            _logger.LogInformation("error found, import canceld");
                            await CleanImport(proceduresTimelineElements, procedureIds, jobFinisehdIds, jobFinisehdIds);
                            return new CSVImportProcedureResultModel { Errors = errors };
                        }
                    }

                    Boolean isActiveCustomer = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.ActiveCustomer, importInfo)) == false;
                    Boolean hasAggrement = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.Aggrement, importInfo)) == false;
                    _logger.LogTrace("only building connection needed: {0}", isActiveCustomer == false);
                    _logger.LogTrace("try to get the building");
                    SimpleBuildingOverviewModel building = await GetBuildingId(currentLine, errors, i, importInfo);
                    if (building == null)
                    {
                        _logger.LogInformation("no building found");
                        if (importInfo.SkipError == true)
                        {
                            _logger.LogTrace("skipping error. go to next line");
                            continue;
                        }
                        else
                        {
                            _logger.LogInformation("error found, import canceld");
                            await CleanImport(proceduresTimelineElements, procedureIds, jobFinisehdIds, jobFinisehdIds);
                            return new CSVImportProcedureResultModel { Errors = errors };
                        }
                    }

                    _logger.LogTrace("get flat for building");
                    Int32 flatId = await GetFlatId(building.Id);
                    DateTimeOffset? appointment = GetAppointment(currentLine, importInfo);
                    Boolean activationAsSoonAsPossible =
                        String.IsNullOrEmpty(importInfo.AsSoonAsPossibleString) == true ? false :
                        GetValue(currentLine, ProcedureImportProperties.Appointment, importInfo).ToLower() == importInfo.AsSoonAsPossibleString.ToLower();
                    Int32 procedureId = 0;
                    Boolean addImportTimeline = false;
                    Boolean informSalesAfterFinished = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.InformSalesAfterFinished, importInfo)) == false;

                    _logger.LogInformation("generating procedures models");
                    if (isActiveCustomer == true)
                    {
                        CustomerConnectionProcedureCreateModel procedureCreate = GetCustomerConnectionProcedureCreateModel(appointment, activationAsSoonAsPossible, flatId, contact, building, hasAggrement, i, errors);
                        if (procedureCreate == null)
                        {
                            _logger.LogInformation("unable to created procedure");
                            if (importInfo.SkipError == true)
                            {
                                _logger.LogTrace("skipping error. go to next line");
                                continue;
                            }
                            else
                            {
                                _logger.LogInformation("error found, import canceld");
                                await CleanImport(proceduresTimelineElements, procedureIds, jobFinisehdIds, jobFinisehdIds);
                                return new CSVImportProcedureResultModel { Errors = errors };
                            }
                        }

                        if (await _storage.CheckIfCustomerConnectionProcedureExistsByFlatId(flatId) == true)
                        {
                            procedureId = await _storage.GetCustomerConnectionProcedureByFlatId(flatId);
                        }
                        else
                        {
                            procedureId = await _storage.CreateCustomerConnectionProcedure(procedureCreate);
                            procedureIds.Add(procedureId);
                            addImportTimeline = true;
                        }
                    }
                    else
                    {
                        if (await _storage.CheckIfBuildingConnectionProcedureExistsByBuildingId(building.Id) == true)
                        {
                            procedureId = await _storage.GetBuildingConnectionProcedureByBuildingId(building.Id);
                        }
                        else
                        {
                            BuildingConnectionProcedureCreateModel procedureCreate = new BuildingConnectionProcedureCreateModel
                            {
                                Appointment = appointment,
                                OwnerContactId = contact.Id,
                                BuildingId = building.Id,
                                DeclarationOfAggrementInStock = hasAggrement,
                                Name = CustomerService.GetNameForConnectBuildingProcedure(building.StreetName, contact),
                                InformSalesAfterFinish = informSalesAfterFinished
                            };

                            procedureId = await _storage.CreateBuildingConnectionProcedure(procedureCreate);
                            procedureIds.Add(procedureId);
                            addImportTimeline = true;
                        }

                    }

                    if (addImportTimeline == true)
                    {
                        Int32 importTimelineId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.Imported, Comment = _defaultImportProcedureTimelineNote });
                        proceduresTimelineElements.Add(importTimelineId);
                    }

                    _logger.LogTrace("check if building with id {0} has a connection job", building.Id);
                    Int32? connectBuildingJobId = await _storage.GetConnectBuildingJobIdByBuildingId(building.Id);
                    Boolean connectBuildingShouldFinished = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.ConnectBuildingFinished, importInfo)) == false;
                    Boolean connenctionJobIsFinished = false;
                    if (connectBuildingJobId.HasValue == false)
                    {
                        _logger.LogTrace("no connection job found for building with id {0}", building.Id);
                        HouseConnenctionJobCreateModel houseConnenctionJobCreateModel = new HouseConnenctionJobCreateModel
                        {
                            BuildingId = building.Id,
                            OwnerContactId = contact.Id,
                            CustomerContactId = contact.Id,
                            FlatId = flatId,
                            OnlyHouseConnection = isActiveCustomer == false,
                        };

                        connectBuildingJobId = await _storage.CreateHouseConnenctionJob(houseConnenctionJobCreateModel);
                        jobIds.Add(connectBuildingJobId.Value);
                        Int32 timelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobCreated, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                        proceduresTimelineElements.Add(timelineElementId);
                        _logger.LogTrace("connect bulding job with id {0} created", connectBuildingJobId);
                    }
                    else
                    {
                        _logger.LogTrace("connection job found for building with id {0}", building.Id);

                        Int32 timelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobCreated, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                        proceduresTimelineElements.Add(timelineElementId);

                        if (connectBuildingShouldFinished == true)
                        {
                            _logger.LogTrace("job already finished");
                            if (await _storage.CheckIfJobIsFinished(connectBuildingJobId.Value) == true)
                            {
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                                proceduresTimelineElements.Add(finishedTimelineElementId);

                                if (await _storage.CheckIfJobIsAcknowledged(connectBuildingJobId.Value) == true)
                                {
                                    _logger.LogTrace("job already acknowledge. Nothing to be done");
                                }
                                else
                                {
                                    await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = connectBuildingJobId.Value, Timestamp = DateTimeOffset.Now, UserAuthId = userAuthId });
                                    _logger.LogTrace("job now acknowledged");
                                }

                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                                proceduresTimelineElements.Add(timelineElementId);
                                connenctionJobIsFinished = true;

                                if (informSalesAfterFinished == true)
                                {
                                    await CreateContactAfterFinishedJob(flatId, building.Id, procedureId);
                                }
                            }
                            else
                            {
                                _logger.LogTrace("building with id {0} has a connection job with id {1} but the job is not finished", building.Id, connectBuildingJobId.Value);

                            }
                        }
                    }

                    if (connectBuildingShouldFinished == true && connenctionJobIsFinished == false)
                    {
                        Int32 finishedId = await _storage.FinishAndAcknowledgeJobAdministrative(new FinishJobAdministrativeModel { JobId = connectBuildingJobId.Value, UserAuthId = userAuthId });
                        Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                        Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ConnectBuildingJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = connectBuildingJobId.Value });
                        jobFinisehdIds.Add(finishedId);
                        proceduresTimelineElements.Add(finishedTimelineElementId);
                        proceduresTimelineElements.Add(ackTimelineElementId);

                        if (informSalesAfterFinished == true)
                        {
                            await CreateContactAfterFinishedJob(flatId, building.Id, procedureId);
                        }
                    }

                    Boolean injectJobShouldFinished = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.InjectFinished, importInfo)) == false;

                    if (isActiveCustomer == true && connectBuildingShouldFinished == true)
                    {
                        Int32? injectJobId = await _storage.GetInjectJobIdByBuildingId(building.Id);

                        if (injectJobId.HasValue == false)
                        {
                            InjectJobCreateModel injectJobCreateModel = new InjectJobCreateModel
                            {
                                BuildingId = building.Id,
                                CustomerContactId = contact.Id,
                            };

                            injectJobId = await _storage.CreateInjectJob(injectJobCreateModel);
                            jobIds.Add(injectJobId.Value);
                        }

                        Int32 createTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.InjectJobCreated, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = injectJobId });
                        proceduresTimelineElements.Add(createTimelineElementId);

                        if (injectJobShouldFinished == true)
                        {
                            if (await _storage.CheckIfJobIsFinished(injectJobId.Value) == false)
                            {
                                Int32 finishedId = await _storage.FinishAndAcknowledgeJobAdministrative(new FinishJobAdministrativeModel { JobId = injectJobId.Value, UserAuthId = userAuthId });
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.InjectJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = injectJobId.Value });
                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.InjectJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = injectJobId.Value });
                                jobFinisehdIds.Add(finishedId);
                                proceduresTimelineElements.Add(finishedTimelineElementId);
                                proceduresTimelineElements.Add(ackTimelineElementId);
                            }
                            else
                            {
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.InjectJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = injectJobId.Value });
                                proceduresTimelineElements.Add(finishedTimelineElementId);

                                if (await _storage.CheckIfJobIsAcknowledged(injectJobId.Value) == false)
                                {
                                    await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = injectJobId.Value, Timestamp = DateTimeOffset.Now, UserAuthId = userAuthId });
                                }

                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.InjectJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = injectJobId.Value });
                                proceduresTimelineElements.Add(ackTimelineElementId);

                            }
                        }
                    }

                    Boolean spliceJobShouldFinished = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.SpliceFinished, importInfo)) == false;

                    if (isActiveCustomer == true && connectBuildingShouldFinished == true && injectJobShouldFinished == true)
                    {
                        Int32? spliceBranchableJobId = await _storage.GetSpliceBranachableJobIdByFlatId(flatId);

                        if (spliceBranchableJobId.HasValue == false)
                        {
                            SpliceBranchableJobCreateModel jobCreateModel = new SpliceBranchableJobCreateModel { CustomerContactId = contact.Id, FlatId = flatId };
                            spliceBranchableJobId = await _storage.CreateSpliceBranchableJob(jobCreateModel);
                            jobIds.Add(spliceBranchableJobId.Value);
                        }

                        Int32 createTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.SpliceBranchableJobCreated, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = spliceBranchableJobId.Value });
                        proceduresTimelineElements.Add(createTimelineElementId);

                        if (spliceJobShouldFinished == true)
                        {
                            if (await _storage.CheckIfJobIsFinished(spliceBranchableJobId.Value) == false)
                            {
                                Int32 finishedId = await _storage.FinishAndAcknowledgeJobAdministrative(new FinishJobAdministrativeModel { JobId = spliceBranchableJobId.Value, UserAuthId = userAuthId });
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.SpliceBranchableJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = spliceBranchableJobId.Value });
                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.SpliceBranchableJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = spliceBranchableJobId.Value });
                                jobFinisehdIds.Add(finishedId);
                                proceduresTimelineElements.Add(finishedTimelineElementId);
                                proceduresTimelineElements.Add(ackTimelineElementId);
                            }
                            else
                            {
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.SpliceBranchableJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = spliceBranchableJobId.Value });
                                proceduresTimelineElements.Add(finishedTimelineElementId);

                                if (await _storage.CheckIfJobIsAcknowledged(spliceBranchableJobId.Value) == false)
                                {
                                    await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = spliceBranchableJobId.Value, Timestamp = DateTimeOffset.Now, UserAuthId = userAuthId });
                                }

                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.SpliceBranchableJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = spliceBranchableJobId.Value });
                                proceduresTimelineElements.Add(ackTimelineElementId);
                            }
                        }
                    }

                    Boolean activationJobShouldFinished = String.IsNullOrEmpty(GetValue(currentLine, ProcedureImportProperties.ActivationFinished, importInfo)) == false;
                    if (isActiveCustomer == true && connectBuildingShouldFinished == true && injectJobShouldFinished == true && spliceJobShouldFinished == true && spliceJobShouldFinished == true)
                    {
                        Int32? activationJobId = await _storage.GetActivationJobIdByFlatId(flatId);

                        if (activationJobId.HasValue == false)
                        {
                            ActivationJobCreateModel jobCreateModel = new ActivationJobCreateModel { CustomerContactId = contact.Id, FlatId = flatId };
                            activationJobId = await _storage.CreateActivationJob(jobCreateModel);
                            jobIds.Add(activationJobId.Value);
                        }

                        Int32 createTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ActivatitionJobCreated, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = activationJobId.Value });
                        proceduresTimelineElements.Add(createTimelineElementId);

                        if (activationJobShouldFinished == true)
                        {
                            if (await _storage.CheckIfJobIsFinished(activationJobId.Value) == false)
                            {
                                Int32 finishedId = await _storage.FinishAndAcknowledgeJobAdministrative(new FinishJobAdministrativeModel { JobId = activationJobId.Value, UserAuthId = userAuthId });
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ActivatitionJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = activationJobId.Value });
                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ActivatitionJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = activationJobId.Value });
                                jobFinisehdIds.Add(finishedId);
                                proceduresTimelineElements.Add(finishedTimelineElementId);
                                proceduresTimelineElements.Add(ackTimelineElementId);
                            }
                            else
                            {
                                Int32 finishedTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ActivatitionJobFinished, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = activationJobId.Value });
                                proceduresTimelineElements.Add(finishedTimelineElementId);

                                if (await _storage.CheckIfJobIsAcknowledged(activationJobId.Value) == false)
                                {
                                    await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = activationJobId.Value, Timestamp = DateTimeOffset.Now, UserAuthId = userAuthId });
                                }

                                Int32 ackTimelineElementId = await _storage.AddProcedureTimelineElementIfNessesary(new ProcedureTimelineCreateModel { ProcedureId = procedureId, State = ProcedureStates.ActivatitionJobAcknlowedged, Comment = _defaultImportProcedureTimelineNote, RelatedJobId = activationJobId.Value });
                                proceduresTimelineElements.Add(ackTimelineElementId);
                            }
                        }
                    }

                    if (await _storage.CheckCheckIfProcedureIsFinished(procedureId) == true)
                    {
                        await _storage.MarkProcedureAsFinished(procedureId);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while importing procedures: {0}", ex.ToString());
                importErrorHappend = true;
            }

            if (importErrorHappend == true)
            {
                await CleanImport(proceduresTimelineElements, procedureIds, jobFinisehdIds, jobIds);
            }
            else
            {
                if (errors.Count > 0 && importInfo.RolebackIfErrorHappend == true)
                {
                    await CleanImport(proceduresTimelineElements, procedureIds, jobFinisehdIds, jobIds);
                }
            }

            return new CSVImportProcedureResultModel
            {
                Errors = errors,
                JobIds = jobIds,
                ProcedureIds = procedureIds,
            };
        }

        private async Task CleanImport(ICollection<Int32> proceduresTimelineElements, ICollection<Int32> procedureIds, ICollection<Int32> jobFinisehdIds, ICollection<Int32> jobIds)
        {
            _logger.LogInformation("clean up import data");
            await _storage.DeleteProcedureTimelineElements(proceduresTimelineElements.Where(x => x > 0).ToList());
            await _storage.DeleteProcedures(procedureIds);

            await _storage.DeleteJobFinished(jobFinisehdIds);
            await _storage.DeleteJobs(jobIds);

            proceduresTimelineElements.Clear();
            procedureIds.Clear();
            jobFinisehdIds.Clear();
            jobIds.Clear();
        }

        #endregion
    }
}
