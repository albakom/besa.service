﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class JobService : IJobService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly IFileManager _fileManager;
        private const Int32 _maxJobAmount = 50;
        private readonly ILogger<JobService> _logger;

        private const String _commentForAdministrativlyFinished = "Administrativ feritgestellt";

        public IProtocolService ProtocolService { get; }

        #endregion

        #region Constructor

        public JobService(IBesaDataStorage storage, IFileManager fileManager, IProtocolService protocolService, ILogger<JobService> logger)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._fileManager = fileManager ?? throw new ArgumentNullException(nameof(fileManager));
            ProtocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        private async Task<Boolean> CheckIfFinishInfoToJob<T>(JobDetailModel<T> model, String requesterAuthId) where T : FinishJobDetailModel
        {
            Boolean result = false;
            if (model.State == JobStates.Finished || model.State == JobStates.Acknowledged)
            {
                Boolean userIsManagement = await _storage.CheckIfUserIsManagement(requesterAuthId);
                if (userIsManagement == true)
                {
                    result = true;
                }
                else
                {
                    BesaRoles role = await _storage.GetUserRoles(requesterAuthId);
                    if ((role & BesaRoles.ProjectManager) == BesaRoles.ProjectManager)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        public async Task<BranchOffJobDetailModel> GetBranchOffJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetBranchOffJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if branchoff job with id {0} exists");
            if (await _storage.CheckIfBranchOffJobExist(jobId) == false)
            {
                _logger.LogInformation("branch off job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            BranchOffJobDetailModel result = await _storage.GetBranchOffJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetBranchOffFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<HouseConnenctionJobDetailModel> GetBuildingConnenctionJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetBuildingConnenctionJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if building connenction job with id {0} exists");
            if (await _storage.CheckIfBuildingConnenctionJobExist(jobId) == false)
            {
                _logger.LogInformation("building connenction job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            HouseConnenctionJobDetailModel result = await _storage.GetBuildingConnenctionJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetConnectionBuildingFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<CollectionJobDetailsModel> GetCollectionJobDetails(Int32 collectionJobId)
        {
            _logger.LogTrace("GetCollectionJobDetails. collectionJobId: {0}", collectionJobId);

            _logger.LogTrace("check if collection job with id {0} exists", collectionJobId);
            if (await _storage.CheckIfCollectionJobExists(collectionJobId) == false)
            {
                _logger.LogInformation("collection job with id {0} not found", collectionJobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            CollectionJobDetailsModel result = await _storage.GetCollectionJobDetails(collectionJobId);
            return result;
        }

        private async Task ValidateFinishedJobModel(FinishedJobModel model, Boolean fileNeeded, Action nonConxtextChecks)
        {
            _logger.LogTrace("validating FinishedJobModel");
            if (model == null)
            {
                _logger.LogInformation("no finished model given");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            if (model.ProblemHappend == null)
            {
                model.ProblemHappend = new ExplainedBooleanModel();
            }

            if (model.ProblemHappend.Value == true && String.IsNullOrEmpty(model.ProblemHappend.Description) == true)
            {
                _logger.LogInformation("if a problem happend, a description is needed");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            if (String.IsNullOrEmpty(model.Comment) == false && model.Comment.Length > _storage.Constraints.MaxJobFinishedChars)
            {
                _logger.LogInformation("description is to long");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            if (model.FileIds == null)
            {
                model.FileIds = new List<Int32>();
            }

            if (fileNeeded == true && model.FileIds.Count() == 0)
            {
                _logger.LogInformation("a file is needed");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            if (String.IsNullOrEmpty(model.UserAuthId) == true)
            {
                _logger.LogInformation("no user auth id was given");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            if (nonConxtextChecks != null)
            {
                nonConxtextChecks();
            }

            _logger.LogTrace("check if user with authid {0} exists", model.UserAuthId);
            if (await _storage.CheckIfUserExists(model.UserAuthId) == false)
            {
                _logger.LogTrace("user with authid {0} not found", model.UserAuthId);
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            _logger.LogTrace("check if job with id {0} exists", model.JobId);
            if (await _storage.CheckIfJobExists(model.JobId) == false)
            {
                _logger.LogInformation("job with id {0} not found", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }

            _logger.LogTrace("check if job with id {0} is already finished", model.JobId);
            if (await _storage.CheckIfJobIsFinished(model.JobId) == true)
            {
                _logger.LogInformation("job with id {0} has already finished", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if file exists");
            if (fileNeeded == true)
            {
                if (await _storage.CheckIfFileExists(model.FileIds) == false)
                {
                    _logger.LogInformation("not all files found");
                    throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
                }
            }
        }

        private void ValidateFinishBranchOffJobModel(FinishBranchOffJobModel model)
        {
            if (model.DuctChanged == null)
            {
                model.DuctChanged = new ExplainedBooleanModel();
            }

            if (model.DuctChanged.Value == true && String.IsNullOrEmpty(model.DuctChanged.Description) == true)
            {
                _logger.LogInformation("if the duct changed, a description is needed");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }
        }

        public async Task<Boolean> FinishBranchOffJobModel(FinishBranchOffJobModel model)
        {
            _logger.LogTrace("FinishBranchOffJobModel");

            await ValidateFinishedJobModel(model, true, () => ValidateFinishBranchOffJobModel(model));

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;
            Boolean created = await _storage.FinishBranchOffJob(model);

            await AddProcedureTimeline(model.JobId, ProcedureStates.BranchOffJobFinished);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinished,
                RelatedType = MessageRelatedObjectTypes.BranchOffJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = now,
                RelatedObjectId = model.JobId
            }, model.UserAuthId);

            return created;
        }

        private void ValidateFinishBuildingConnenction(FinishBuildingConnenctionJobModel model)
        {
            if (model.DuctChanged == null)
            {
                model.DuctChanged = new ExplainedBooleanModel();
            }

            if (model.DuctChanged.Value == true && String.IsNullOrEmpty(model.DuctChanged.Description) == true)
            {
                _logger.LogInformation("if the duct changed, a description is needed");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }
        }

        public async Task<Boolean> FinishBuildingConnenction(FinishBuildingConnenctionJobModel model)
        {
            _logger.LogTrace("FinishBuildingConnenction");

            await ValidateFinishedJobModel(model, true, () => ValidateFinishBuildingConnenction(model));

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishBuildingConnenctionJob(model);
            Int32? procedureId = await _storage.GetProcecureIdByJobId(model.JobId);
            await AddProcedureTimeline(model.JobId, ProcedureStates.ConnectBuildingJobFinished);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinished,
                RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = now,
                RelatedObjectId = model.JobId
            }, model.UserAuthId);

            return created;
        }

        private MessageRelatedObjectTypes GetMessageRelatedObjectTypeByJobType(JobTypes jobType)
        {
            MessageRelatedObjectTypes result = MessageRelatedObjectTypes.BranchOffJob;
            switch (jobType)
            {
                case JobTypes.BranchOff:
                    result = MessageRelatedObjectTypes.BranchOffJob;
                    break;
                case JobTypes.HouseConnenction:
                    result = MessageRelatedObjectTypes.ConnectBuildingJob;
                    break;
                case JobTypes.Inject:
                    result = MessageRelatedObjectTypes.InjectJob;
                    break;
                case JobTypes.SpliceInBranchable:
                    result = MessageRelatedObjectTypes.SpliceInBranchableJob;
                    break;
                case JobTypes.SpliceInPoP:
                    result = MessageRelatedObjectTypes.SpliceMainCableInPoPJob;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    result = MessageRelatedObjectTypes.PrepareBranchableJob;
                    break;
                case JobTypes.ActivationJob:
                    result = MessageRelatedObjectTypes.ActivationJob;
                    break;
                case JobTypes.Inventory:
                    result = MessageRelatedObjectTypes.InventoryJob;
                    break;
                case JobTypes.ContactAfterFinished:
                    result = MessageRelatedObjectTypes.ContactAfterFinishedJob;
                    break;
                default:
                    break;
            }

            return result;
        }

        private ProcedureStates? GetProcedureAcknowledgedStateByJobType(JobTypes jobType)
        {
            ProcedureStates? state = null;
            switch (jobType)
            {
                case JobTypes.BranchOff:
                    state = ProcedureStates.BranchOffJobAcknowledegd;
                    break;
                case JobTypes.HouseConnenction:
                    state = ProcedureStates.ConnectBuildingJobAcknlowedged;
                    break;
                case JobTypes.Inject:
                    state = ProcedureStates.InjectJobAcknlowedged;
                    break;
                case JobTypes.SpliceInBranchable:
                    state = ProcedureStates.SpliceBranchableJobAcknlowedged;
                    break;
                case JobTypes.ActivationJob:
                    state = ProcedureStates.ActivatitionJobAcknlowedged;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    state = ProcedureStates.PrepareBranchableForSpliceJobAcknlowedged;
                    break;
                case JobTypes.SpliceInPoP:
                    state = ProcedureStates.SpliceODFJobAcknlowedged;
                    break;
                default:
                    break;
            }

            return state;
        }

        private ProcedureStates? GetProcedureDiscardStateByJobType(JobTypes jobType)
        {
            ProcedureStates? state = null;
            switch (jobType)
            {
                case JobTypes.BranchOff:
                    state = ProcedureStates.BranchOffJobDiscarded;
                    break;
                case JobTypes.HouseConnenction:
                    state = ProcedureStates.ConnectBuildingJobDiscarded;
                    break;
                case JobTypes.Inject:
                    state = ProcedureStates.InjectJobDiscarded;
                    break;
                case JobTypes.SpliceInBranchable:
                    state = ProcedureStates.SpliceBranchableJobDiscarded;
                    break;
                case JobTypes.ActivationJob:
                    state = ProcedureStates.ActivatitionJobDiscarded;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    state = ProcedureStates.PrepareBranchableForSpliceJobDiscarded;
                    break;
                case JobTypes.SpliceInPoP:
                    state = ProcedureStates.SpliceODFJobDiscarded;
                    break;
                default:
                    break;
            }

            return state;
        }

        public async Task<Boolean> AcknowledgeJob(AcknowledgeJobModel model)
        {
            _logger.LogTrace("AcknowledgeJob");

            if (model == null)
            {
                _logger.LogInformation("no acknowledge model given");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            if (String.IsNullOrEmpty(model.UserAuthId) == true)
            {
                _logger.LogInformation("no uses was given");
                throw new JobServicException(JobServicExceptionReasons.InvalidAcknowlegdeModel);
            }

            _logger.LogTrace("check if user with authid {0} exists", model.UserAuthId);
            if (await _storage.CheckIfUserExists(model.UserAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", model.UserAuthId);
                throw new JobServicException(JobServicExceptionReasons.InvalidAcknowlegdeModel);
            }

            _logger.LogTrace("check if job with id {0} exists", model.JobId);
            if (await _storage.CheckIfJobExists(model.JobId) == false)
            {
                _logger.LogInformation("job with id {0} not found", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidAcknowlegdeModel);
            }

            _logger.LogTrace("check if job is finished");
            if (await _storage.CheckIfJobIsFinished(model.JobId) == false)
            {
                _logger.LogInformation("job with id {0} not finished, so it can't be acknowledged", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            if (await _storage.CheckIfJobIsAcknowledged(model.JobId) == true)
            {
                _logger.LogInformation("job with id {0} has alreay acknowledged", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean acknowledged = await _storage.AcknowledgeJob(model);

            if (acknowledged == true)
            {
                _logger.LogTrace("get jobtype from job with id {0}", model.JobId);
                JobTypes jobType = await _storage.GetJobType(model.JobId);
                _logger.LogTrace("job with id {0} resolved to jobtype: {1}", model.JobId, jobType);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobAcknowledged,
                    RelatedType = GetMessageRelatedObjectTypeByJobType(jobType),
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = now,
                    RelatedObjectId = model.JobId
                }, model.UserAuthId);

                Int32? procedureId = await _storage.GetProcecureIdByJobId(model.JobId);
                if (procedureId.HasValue == true)
                {
                    ProcedureStates? state = GetProcedureAcknowledgedStateByJobType(jobType);
                    if (state.HasValue == true)
                    {
                        await AddProcedureTimeline(procedureId.Value, model.JobId, state.Value);
                    }
                }

                if (jobType == JobTypes.HouseConnenction)
                {
                    await CreateInjectJobAfterAcknowledge(model.JobId, procedureId);

                    if (procedureId.HasValue == true)
                    {
                        Boolean shouldContactAfterFinishCreated = await _storage.CheckIfContactAfterFinishedJobShouldCreated(procedureId.Value);
                        if (shouldContactAfterFinishCreated == true)
                        {
                            Int32 buildingId = await _storage.GetBuildingIdByConnectionJobId(model.JobId);
                            Int32 contactAfterFinishedJobId = await _storage.CreateContactAfterFinishedJob(new CreateContactAfterFinishedJobModel
                            {
                                BuildingId = buildingId,
                                RelatedProcedureId = procedureId.Value,
                            });

                            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                            {
                                Action = MessageActions.Create,
                                RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob,
                                Level = ProtocolLevels.Sucesss,
                                Timestamp = DateTimeOffset.Now,
                                RelatedObjectId = contactAfterFinishedJobId,
                            });
                        }
                    }
                }
                else if (jobType == JobTypes.Inject && procedureId.HasValue == true)
                {
                    await CreateSpliceJobAfterAcknowledge(model.JobId, procedureId.Value);
                }
                else if (jobType == JobTypes.SpliceInBranchable && procedureId.HasValue == true)
                {
                    await CreateActivationJobAfterAcknlowedge(model.JobId, procedureId.Value);
                }

                if (procedureId.HasValue == true && await _storage.CheckCheckIfProcedureIsFinished(procedureId.Value))
                {
                    await _storage.MarkProcedureAsFinished(procedureId.Value);

                    await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                    {
                        Action = MessageActions.FinishedProcedures,
                        RelatedType = jobType == JobTypes.HouseConnenction ? MessageRelatedObjectTypes.BuildingConnenctionProcedure : MessageRelatedObjectTypes.CustomerConnenctionProcedure,
                        Level = ProtocolLevels.Sucesss,
                        Timestamp = DateTimeOffset.Now,
                        RelatedObjectId = procedureId.Value,
                    });
                }
            }

            return acknowledged;
        }

        private async Task CreateInjectJobAfterAcknowledge(Int32 jobId, Int32? procedureId)
        {
            _logger.LogTrace("job with id {0} is a house connenction job. Check if only house connenction is needed", jobId);
            Boolean onlyHouseConnenctionNeeded = await _storage.CheckIfOnlyHouseConnenctionIsNeeded(jobId);
            if (onlyHouseConnenctionNeeded == false)
            {
                _logger.LogTrace("creating inject job");
                Int32 buildingId = await _storage.GetBuildingIdByConnectionJobId(jobId);
                InjectJobCreateModel injectJob = new InjectJobCreateModel
                {
                    BuildingId = buildingId,
                    CustomerContactId = await _storage.GetCustomerContactIdByConnenctionJobId(jobId),
                };

                Int32 inejctJobId = await _storage.CreateInjectJob(injectJob);

                if (procedureId.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.InjectJobCreated);
                }

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Create,
                    RelatedType = MessageRelatedObjectTypes.InjectJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = inejctJobId,
                });
            }
        }

        private async Task CreateSpliceJobAfterAcknowledge(Int32 jobId, Int32 procedureId)
        {
            _logger.LogTrace("job with id {0} is a inject job. Check if request is active customer request", jobId);
            Boolean isActiveCustomer = await _storage.CheckIfCustomerConnectionProcedureExists(procedureId);

            if (isActiveCustomer == true)
            {
                _logger.LogTrace("create a splice job");
                SpliceBranchableJobCreateModel spliceJobInfo = await _storage.GetCreateSpliceJobInfoByCustomerConnectionProcedureId(procedureId);

                Int32 spliceJobId = await _storage.CreateSpliceBranchableJob(spliceJobInfo);
                await AddProcedureTimeline(procedureId, spliceJobId, ProcedureStates.SpliceBranchableJobCreated);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Create,
                    RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = spliceJobId,
                });
            }
        }

        private async Task CreateActivationJobAfterAcknlowedge(Int32 jobId, Int32 procedureId)
        {
            _logger.LogTrace("job with id {0} is a splice branchable job. Check if request is active customer request", jobId);
            Boolean isActiveCustomer = await _storage.CheckIfCustomerConnectionProcedureExists(procedureId);

            if (isActiveCustomer == true)
            {
                _logger.LogTrace("create a splice job");
                ActivationJobCreateModel activationJobInfo = await _storage.GetCreateActivationJobInfoByCustomerConnectionProcedureId(procedureId);

                Int32 activationJobId = await _storage.CreateActivationJob(activationJobInfo);
                await AddProcedureTimeline(procedureId, activationJobId, ProcedureStates.ActivatitionJobCreated);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Create,
                    RelatedType = MessageRelatedObjectTypes.ActivationJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = activationJobId,
                });
            }
        }

        public async Task<IEnumerable<SimpleJobOverview>> GetJobsToAcknowledge()
        {
            _logger.LogTrace("GetJobsToAcknowledge");

            _logger.LogTrace("get jobs with are waiting for an acknowledge from database");
            IEnumerable<SimpleJobOverview> result = await _storage.GetJobsToAcknowledge();
            return result;
        }

        public async Task<Boolean> DiscardJob(Int32 jobId, String userAuthId)
        {
            _logger.LogTrace("DiscardJob. jobId: {0}", jobId);

            _logger.LogTrace("check if job with id {0} exists", jobId);
            if (await _storage.CheckIfJobExists(jobId) == false)
            {
                _logger.LogInformation("job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if job with id {0} is finished", jobId);
            if (await _storage.CheckIfJobIsFinished(jobId) == false)
            {
                _logger.LogInformation("discarding a non finished job is not allowed");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if job with id {0} is acknowled", jobId);
            if (await _storage.CheckIfJobIsAcknowledged(jobId) == true)
            {
                _logger.LogInformation("discarding a job with is acknowledge is not allowed");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if user with auth id: {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("getting file url realted with job id {0}", jobId);
            IEnumerable<String> fileUrls = await _storage.GetFileUrlsByJobId(jobId);

            _logger.LogTrace("deleting files");
            await _fileManager.Delete(fileUrls);

            _logger.LogTrace("remove finish attributes in database");
            Boolean result = await _storage.DiscardJob(jobId);
            if (result == true)
            {
                JobTypes jobType = await _storage.GetJobType(jobId);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobDiscared,
                    RelatedType = GetMessageRelatedObjectTypeByJobType(jobType),
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = jobId
                }, userAuthId);

                Int32? procedure = await _storage.GetProcecureIdByJobId(jobId);
                if (procedure.HasValue == true)
                {
                    ProcedureStates? state = GetProcedureDiscardStateByJobType(jobType);
                    if (state.HasValue == true)
                    {
                        await AddProcedureTimeline(procedure.Value, jobId, state.Value);
                    }
                }
            }

            return result;
        }

        public async Task<IEnumerable<RequestOverviewModel>> GetOpenRequests()
        {
            _logger.LogTrace("GetOpenRequests");
            IEnumerable<RequestOverviewModel> result = await _storage.GetOpenRequests();
            return result;
        }

        private async Task CheckIfRequestExistsAndIsNotClosed(Int32 requestId)
        {
            _logger.LogTrace("check if customer request with id {0} exists", requestId);
            if (await _storage.CheckIfCustomerRequestExists(requestId) == false)
            {
                _logger.LogTrace("customer request with id {0} not found", requestId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if customer request with id {0} is closed", requestId);
            if (await _storage.CheckIfCustomerRequestIsClosed(requestId) == true)
            {
                _logger.LogTrace("customer request with id {0} ist closed. So it can not be deleted", requestId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }
        }

        private MessageRelatedObjectTypes GetMessageRelatedObjectTypeByProcedureType(ProcedureTypes procedureType)
        {
            MessageRelatedObjectTypes result = MessageRelatedObjectTypes.CustomerConnenctionProcedure;
            if (procedureType == ProcedureTypes.HouseConnection)
            {
                result = MessageRelatedObjectTypes.BuildingConnenctionProcedure;
            }

            return result;
        }

        private async Task CheckIfUserExists(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id: {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }
        }

        public async Task<Boolean> DeleteRequest(Int32 requestId, String userAuthId)
        {
            _logger.LogTrace("DeleteRequest. CustomerAccessRequestId: {0}", requestId);

            await CheckIfRequestExistsAndIsNotClosed(requestId);
            await CheckIfUserExists(userAuthId);

            Boolean result = await _storage.DeleteCustomerRequest(requestId);
            if (result == true)
            {
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Delete,
                    RelatedType = MessageRelatedObjectTypes.CustomerRequest,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = requestId
                }, userAuthId);

                Int32? procedureId = await _storage.GetProcecureIdByRequestId(requestId);

                if (procedureId.HasValue == true)
                {
                    ProcedureTimelineCreateModel procedureTimeline = new ProcedureTimelineCreateModel
                    {
                        ProcedureId = procedureId.Value,
                        State = ProcedureStates.RequestDeleted,
                        RelatedRequestId = requestId,
                    };

                    await _storage.AddProcedureTimelineElementIfNessesary(procedureTimeline);
                    await _storage.MarkProcedureAsFinished(procedureId.Value);

                    ProcedureTypes procedureType = await _storage.GetProcedureTypeById(procedureId.Value);

                    await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                    {
                        Action = MessageActions.FinishedProcedures,
                        RelatedType = GetMessageRelatedObjectTypeByProcedureType(procedureType),
                        Level = ProtocolLevels.Sucesss,
                        Timestamp = DateTimeOffset.Now,
                        RelatedObjectId = procedureId.Value
                    }, userAuthId);
                }
            }

            return result;
        }

        public async Task<RequestDetailModel> GetRequestDetails(Int32 requestId)
        {
            _logger.LogTrace("GetRequestDetails. CustomerAccessRequestId: {0}", requestId);

            await CheckIfRequestExistsAndIsNotClosed(requestId);

            RequestDetailModel request = await _storage.GetCustomerRequestDetails(requestId);
            if (request.OnlyHouseConnection == true)
            {
                request.MostRecentJob = await _storage.GetMostRecentJobByBuildingId(request.Building.Id);
            }
            else
            {
                request.MostRecentJob = await _storage.GetMostRecentJobByFlatId(request.Flat.Id, request.Building.Id);
            }

            return request;
        }

        public async Task<Int32> CreateHouseConnenctionJobByRequest(Int32 requestId, Boolean boundLikeBranchOff, String userAuthId)
        {
            _logger.LogTrace("CreateHouseConnenctionJobByRequest. CustomerAccessRequestId: {0}", requestId);

            await CheckIfRequestExistsAndIsNotClosed(requestId);
            await CheckIfUserExists(userAuthId);

            RequestDetailModel request = await _storage.GetCustomerRequestDetails(requestId);

            Int32 buildingId = request.Building.Id;
            _logger.LogTrace("check if building with id {0} already connected", buildingId);
            if (await _storage.CheckIfBuildingConnenctionIsFinished(buildingId) == true)
            {
                _logger.LogInformation("building with id {0} is already connected, so a connection job couldn't be established");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            HouseConnenctionJobCreateModel jobCreateModel = new HouseConnenctionJobCreateModel
            {
                BuildingId = request.Building.Id,
                CustomerContactId = request.Customer == null ? new Nullable<Int32>() : request.Customer.Id,
                OwnerContactId = request.Owner.Id,
                ArchitectContactId = request.Architect == null ? new Nullable<Int32>() : request.Architect.Id,
                WorkmanContactId = request.Workman == null ? new Nullable<Int32>() : request.Workman.Id,
                CivilWorkIsDoneByCustomer = request.CivilWorkIsDoneByCustomer,
                ConnectionOfOtherMediaRequired = request.ConnectionOfOtherMediaRequired,
                Description = request.DescriptionForHouseConnection,
                OnlyHouseConnection = request.OnlyHouseConnection,
                ConnectionAppointment = request.ConnectionAppointment
            };

            Int32 jobId = await _storage.CreateHouseConnenctionJob(jobCreateModel);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId
            }, userAuthId);

            Int32? procedureId = await _storage.GetProcecureIdByRequestId(requestId);

            if (procedureId.HasValue == true)
            {
                ProcedureTimelineCreateModel ackProcedureTimeline = new ProcedureTimelineCreateModel
                {
                    ProcedureId = procedureId.Value,
                    State = ProcedureStates.RequestAcknkowledged,
                    RelatedRequestId = requestId,
                };

                await _storage.AddProcedureTimelineElementIfNessesary(ackProcedureTimeline);
                await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.ConnectBuildingJobCreated);
            }

            if (boundLikeBranchOff == true)
            {
                _logger.LogTrace("Try to bound job link branch off job with same building");

                _logger.LogTrace("check if building with id {0} has a branch off job", request.Building.Id);
                if (await _storage.CheckIfBranchoffJobIsBoundByBuildingId(request.Building.Id) == true)
                {
                    _logger.LogTrace("Getting branch off job id from building with id {0}", request.Building.Id);
                    Int32 branchOffJobId = await _storage.GetBranchOffJobIdByBuildingId(request.Building.Id);
                    _logger.LogTrace("building with id {0} has branch off job with id {1}", request.Building.Id, branchOffJobId);

                    _logger.LogTrace("getting collection job id form job with id {0}", branchOffJobId);
                    Int32 collectionJobId = await _storage.GetCollectionJobId(branchOffJobId);
                    _logger.LogTrace("job  with id {0} has collectionJob with id {1}", branchOffJobId, collectionJobId);

                    _logger.LogTrace("connection job with id {0} was added to collection job with id {1}", jobId, collectionJobId);
                    await _storage.AddJobToCollectionJob(collectionJobId, jobId);

                    if (procedureId.HasValue == true)
                    {
                        await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.ConnectBuildingJobBound);
                    }

                    await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                    {
                        Action = MessageActions.JobBound,
                        RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                        Level = ProtocolLevels.Sucesss,
                        Timestamp = DateTimeOffset.Now,
                        RelatedObjectId = jobId
                    }, userAuthId);
                }
            }

            await _storage.CloseRequest(requestId);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Delete,
                RelatedType = MessageRelatedObjectTypes.CustomerRequest,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = requestId
            }, userAuthId);

            return jobId;
        }

        private async Task<SimpleJobOverview> GetCurrentJobByRequest(RequestDetailModel request)
        {
            SimpleJobOverview currentJobState = null;
            if (request.OnlyHouseConnection == true)
            {
                _logger.LogTrace("only house connection is needed. Get job state by building");
                currentJobState = await _storage.GetMostRecentJobByBuildingId(request.Building.Id);
            }
            else
            {
                _logger.LogTrace("active customer is needed. Get job state by flat and building id");
                currentJobState = await _storage.GetMostRecentJobByFlatId(request.Flat.Id, request.Building.Id);
            }

            return currentJobState;
        }

        public async Task<SimpleJobOverview> CreateJobByRequestId(Int32 requestId, String userAuthId)
        {
            _logger.LogTrace("CreateHouseConnenctionJobByRequest. CustomerAccessRequestId: {0}", requestId);

            await CheckIfRequestExistsAndIsNotClosed(requestId);
            await CheckIfUserExists(userAuthId);

            RequestDetailModel request = await _storage.GetCustomerRequestDetails(requestId);

            _logger.LogTrace("check if request is currently processing");
            if (request.IsInProgress == true)
            {
                _logger.LogInformation("there is already a procedure. No job creation is needed");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("get current job state");
            SimpleJobOverview currentJobState = await GetCurrentJobByRequest(request);

            if (currentJobState == null)
            {
                _logger.LogInformation("unable to get current job");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if current job is acknowledged");
            if (currentJobState.State != JobStates.Acknowledged)
            {
                _logger.LogInformation("the current job is not acknowledged. unable to create the next one");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            Int32? createdJobId = null;
            ProcedureStates? state = null;
            JobTypes createType = JobTypes.ActivationJob;
            switch (currentJobState.JobType)
            {
                case JobTypes.HouseConnenction:
                    if (request.OnlyHouseConnection == false)
                    {
                        _logger.LogTrace("created inject job");
                        InjectJobCreateModel jobCreateModel = new InjectJobCreateModel
                        {
                            BuildingId = request.Building.Id,
                            CustomerContactId = request.Customer.Id,
                        };

                        createdJobId = await _storage.CreateInjectJob(jobCreateModel);
                        state = ProcedureStates.InjectJobCreated;
                        createType = JobTypes.Inject;
                    }
                    break;
                case JobTypes.Inject:
                    if (request.OnlyHouseConnection == false)
                    {
                        _logger.LogTrace("created splice in branchable job");
                        SpliceBranchableJobCreateModel jobCreateModel = new SpliceBranchableJobCreateModel
                        {
                            FlatId = request.Flat.Id,
                            CustomerContactId = request.Customer.Id,
                        };

                        createdJobId = await _storage.CreateSpliceBranchableJob(jobCreateModel);
                        state = ProcedureStates.SpliceBranchableJobCreated;
                        createType = JobTypes.SpliceInBranchable;
                    }
                    break;
                case JobTypes.SpliceInBranchable:
                    if (request.OnlyHouseConnection == false)
                    {
                        _logger.LogTrace("created activation job");
                        ActivationJobCreateModel jobCreateModel = new ActivationJobCreateModel
                        {
                            FlatId = request.Flat.Id,
                            CustomerContactId = request.Customer.Id,
                        };

                        createdJobId = await _storage.CreateActivationJob(jobCreateModel);
                        state = ProcedureStates.ActivatitionJobCreated;
                        createType = JobTypes.ActivationJob;
                    }
                    break;
                default:
                    break;
            }

            if (createdJobId.HasValue == false)
            {
                _logger.LogInformation("invalid job type");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }
            else
            {
                _logger.LogTrace("job created with id {0}", createdJobId.Value);
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Create,
                    RelatedType = GetMessageRelatedObjectTypeByJobType(createType),
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = createdJobId.Value
                }, userAuthId);

                _logger.LogTrace("close procedure with id {0}", request.Id);
                await _storage.CloseRequest(request.Id);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Delete,
                    RelatedType = MessageRelatedObjectTypes.CustomerRequest,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = requestId
                }, userAuthId);
            }

            Int32? procedureId = await _storage.GetProcecureIdByRequestId(requestId);
            if (procedureId.HasValue == true && state.HasValue == true)
            {
                ProcedureTimelineCreateModel ackProcedureTimeline = new ProcedureTimelineCreateModel
                {
                    ProcedureId = procedureId.Value,
                    State = ProcedureStates.RequestAcknkowledged,
                    RelatedRequestId = requestId,
                };

                await _storage.AddProcedureTimelineElementIfNessesary(ackProcedureTimeline);
                await AddProcedureTimeline(procedureId.Value, createdJobId.Value, state.Value);
            }

            SimpleJobOverview nextJob = await GetCurrentJobByRequest(request);
            return nextJob;
        }

        private async Task AddProcedureTimeline(Int32 procedureId, Int32 jobId, ProcedureStates state)
        {
            ProcedureTimelineCreateModel boundJobProcedureTimeline = new ProcedureTimelineCreateModel
            {
                ProcedureId = procedureId,
                State = state,
                RelatedJobId = jobId,
            };
            await _storage.AddProcedureTimelineElementIfNessesary(boundJobProcedureTimeline);
        }

        private async Task AddProcedureTimeline(Int32 procedureId, Int32 jobId, ProcedureStates state, String comment, DateTimeOffset? timestamp)
        {
            ProcedureTimelineCreateModel boundJobProcedureTimeline = new ProcedureTimelineCreateModel
            {
                ProcedureId = procedureId,
                State = state,
                RelatedJobId = jobId,
                Comment = comment,
                Timestamp = timestamp,
            };

            await _storage.AddProcedureTimelineElementIfNessesary(boundJobProcedureTimeline);
        }

        private async Task AddProcedureTimelineWithRequestId(Int32 procedureId, Int32 requestId, ProcedureStates state)
        {
            ProcedureTimelineCreateModel boundJobProcedureTimeline = new ProcedureTimelineCreateModel
            {
                ProcedureId = procedureId,
                State = state,
                RelatedRequestId = requestId,
            };
            await _storage.AddProcedureTimelineElementIfNessesary(boundJobProcedureTimeline);
        }

        public async Task<Int32> CreateInjectJob(InjectJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateInjectJob");

            if (model == null)
            {
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if building with id {0} exists", model.BuildingId);
            if (await _storage.CheckIfBuildingExists(model.BuildingId) == false)
            {
                _logger.LogInformation("building with id {0} not found", model.BuildingId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if building with id {0} is connected", model.BuildingId);
            if (await _storage.CheckIfBuildingConnenctionIsFinished(model.BuildingId) == false)
            {
                _logger.LogInformation("building with id {0} is not connected", model.BuildingId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if contact with id {0} exists", model.CustomerContactId);
            if (await _storage.CheckIfContactExists(model.CustomerContactId) == false)
            {
                _logger.LogInformation("contact with id {0} not found", model.CustomerContactId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreateInjectJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.InjectJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId
            }, userAuthId);

            Int32? connectBuildingJobId = await _storage.GetConnectBuildingJobIdByBuildingId(model.BuildingId);
            if (connectBuildingJobId.HasValue == true)
            {
                Int32? procedureId = await _storage.GetProcecureIdByJobId(connectBuildingJobId.Value);
                if (procedureId.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.InjectJobCreated);
                }
            }

            return jobId;
        }

        public async Task<InjectJobDetailModel> GetInjectJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetInjctJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if inject job with id {0} exists");
            if (await _storage.CheckIfInjectJobExist(jobId) == false)
            {
                _logger.LogInformation("inject job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            InjectJobDetailModel result = await _storage.GetInjectJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetInjectFinishedJobDetails(result.Id);
            };

            return result;
        }

        private void ValidateFinishInjectJob(InjectJobFinishedModel model)
        {
            _logger.LogInformation("check if cable length {0} is valid", model.Length);
            if (model.Length <= 0)
            {
                _logger.LogInformation("cable length should be greater then zero");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }
        }

        public async Task<Boolean> FinishInjectJob(InjectJobFinishedModel model)
        {
            _logger.LogTrace("FinishInjectJob");

            await ValidateFinishedJobModel(model, true, () => ValidateFinishInjectJob(model));

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishInjectJob(model);
            Int32? procedureId = await _storage.GetProcecureIdByJobId(model.JobId);
            if (procedureId.HasValue == true)
            {
                await AddProcedureTimeline(procedureId.Value, model.JobId, ProcedureStates.InjectJobFinished);
            }

            if (created == true)
            {
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.InjectJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = now,
                    RelatedObjectId = model.JobId
                }, model.UserAuthId);
            }

            return created;
        }

        public async Task<Int32> CreateHouseConnenctionJob(HouseConnenctionJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateHouseConnenctionJob");
            if (model == null)
            {
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            if (model.OnlyHouseConnection == false && model.CustomerContactId.HasValue == false)
            {
                throw new JobServicException(JobServicExceptionReasons.InvalidConnectBuildingJobCreateModel);
            }

            _logger.LogTrace("check if building with id {0} exists", model.BuildingId);
            if (await _storage.CheckIfBuildingExists(model.BuildingId) == false)
            {
                _logger.LogInformation("building with id {0} not found", model.BuildingId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if building with id {0} already connected", model.BuildingId);
            if (await _storage.CheckIfBuildingConnenctionIsFinished(model.BuildingId) == true)
            {
                _logger.LogInformation("building with id {0} is already connected, so a connection job couldn't be established");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            Int32?[] tempContacs = new Int32?[] { model.CustomerContactId, model.WorkmanContactId, model.ArchitectContactId };

            HashSet<Int32> contacts = new HashSet<Int32> { model.OwnerContactId };
            foreach (Int32? item in tempContacs)
            {
                if (item.HasValue == true)
                {
                    contacts.Add(item.Value);
                }
            }

            _logger.LogTrace("check if contacts exists");
            if (await _storage.CheckIfContactInfosExists(contacts) == false)
            {
                _logger.LogInformation("not all contacs was found");
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }
            await CheckIfUserExists(userAuthId);

            Int32 id = await _storage.CreateHouseConnenctionJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = id,
            }, userAuthId);

            Boolean procedureFound = false;

            Int32? requestId = await _storage.GetRequestIdByFilterValues(new RequestFilterModel
            {
                BuildingId = model.BuildingId,
                OwnerContactId = model.OwnerContactId,
                OnlyConnection = model.OnlyHouseConnection,
            });

            if (requestId.HasValue == true)
            {
                Boolean requestClosed = false;
                if (await _storage.CheckIfCustomerRequestIsClosed(requestId.Value) == false)
                {
                    requestClosed = await _storage.CloseRequest(requestId.Value);
                    if (requestClosed == true)
                    {
                        await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                        {
                            Action = MessageActions.Delete,
                            RelatedType = MessageRelatedObjectTypes.CustomerRequest,
                            Level = ProtocolLevels.Sucesss,
                            Timestamp = DateTimeOffset.Now,
                            RelatedObjectId = requestId.Value,
                        });
                    }
                }

                Int32? procedureId = await _storage.GetProcecureIdByRequestId(requestId.Value);
                if (procedureId.HasValue == true)
                {
                    procedureFound = true;
                    if (requestClosed == true)
                    {
                        await AddProcedureTimelineWithRequestId(procedureId.Value, requestId.Value, ProcedureStates.RequestAcknkowledged);
                    }

                    await AddProcedureTimeline(procedureId.Value, id, ProcedureStates.ConnectBuildingJobCreated);
                }
            }

            if (procedureFound == false && model.CreateProcedureIfNotAlreadyExists == true)
            {
                Int32 procedureId = 0;
                MessageRelatedObjectTypes messageType = MessageRelatedObjectTypes.BuildingConnenctionProcedure;
                if (model.OnlyHouseConnection == true)
                {
                    BuildingConnectionProcedureCreateModel procedureCreateModel = new BuildingConnectionProcedureCreateModel
                    {
                        BuildingId = model.BuildingId,
                        Start = ProcedureStates.ConnectBuildingJobCreated,
                        Name = await CustomerService.GenerateNameForProcedure(_storage, model.BuildingId, model.OwnerContactId),
                        OwnerContactId = model.OwnerContactId,
                        FirstElement = new ProcedureTimelineCreateModel
                        {
                            RelatedJobId = id,
                            State = ProcedureStates.ConnectBuildingJobCreated,
                        }
                    };

                    procedureId = await _storage.CreateBuildingConnectionProcedure(procedureCreateModel);
                    messageType = MessageRelatedObjectTypes.BuildingConnenctionProcedure;
                }
                else
                {
                    CustomerConnectionProcedureCreateModel procedureCreateModel = new CustomerConnectionProcedureCreateModel
                    {
                        FlatId = model.FlatId,
                        Start = ProcedureStates.ConnectBuildingJobCreated,
                        Name = await CustomerService.GenerateNameForProcedure(_storage, model.BuildingId, model.CustomerContactId.Value),
                        CustomerContactId = model.CustomerContactId.Value,
                        FirstElement = new ProcedureTimelineCreateModel
                        {
                            RelatedJobId = id,
                            State = ProcedureStates.ConnectBuildingJobCreated,
                        }
                    };

                    procedureId = await _storage.CreateCustomerConnectionProcedure(procedureCreateModel);
                    messageType = MessageRelatedObjectTypes.CustomerConnenctionProcedure;
                }

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Create,
                    RelatedType = messageType,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = procedureId,
                });
            }

            return id;
        }

        public async Task<Boolean> BindJobs(BindJobModel model, String userAuhtId)
        {
            _logger.LogTrace("BindBranchOffJobs");
            if (model == null)
            {
                _logger.LogInformation("no BindBranchOffJobs model was given");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            if (String.IsNullOrEmpty(model.Name) == true)
            {
                _logger.LogInformation("not task name was given");
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            _logger.LogTrace("validating model");
            if (model.JobIds == null || model.JobIds.Count() == 0)
            {
                _logger.LogInformation("no jobs was given");
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            if (model.EndDate <= DateTimeOffset.Now)
            {
                _logger.LogInformation("jobs end in pasts");
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            _logger.LogTrace("check if company with id {0} exists", model.CompanyId);
            if (await _storage.CheckIfCompanyExists(model.CompanyId) == false)
            {
                _logger.LogInformation("company with id {0} not found", model.CompanyId);
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            _logger.LogTrace("check if jobs exits");
            if (await _storage.CheckIfJobsExists(model.JobIds) == false)
            {
                _logger.LogInformation("jobs not found");
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            _logger.LogTrace("check if jobs are unbounded");
            if (await _storage.CheckIfJobsAreUnbound(model.JobIds) == false)
            {
                _logger.LogInformation("jobs are not unbounded");
                throw new JobServicException(JobServicExceptionReasons.InvalidBindJobModel);
            }

            await CheckIfUserExists(userAuhtId);

            Int32 collectionJobId = await _storage.BindJobs(model.JobIds, model.CompanyId, model.EndDate, model.Name);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.CollectionJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = collectionJobId,
            }, userAuhtId);

            foreach (Int32 jobId in model.JobIds)
            {
                Int32? procedure = await _storage.GetProcecureIdByJobId(jobId);
                if (procedure.HasValue == false) { continue; }

                JobTypes type = await _storage.GetJobType(jobId);
                ProcedureStates? state = ProcedureStates.ConnectBuildingJobBound;
                switch (type)
                {
                    case JobTypes.HouseConnenction:
                        state = ProcedureStates.ConnectBuildingJobBound;
                        break;
                    case JobTypes.Inject:
                        state = ProcedureStates.InjectJobBound;
                        break;
                    case JobTypes.SpliceInBranchable:
                        state = ProcedureStates.SpliceBranchableJobBound;
                        break;
                    case JobTypes.ActivationJob:
                        state = ProcedureStates.ActivatitionJobBound;
                        break;
                    case JobTypes.PrepareBranchableForSplice:
                        state = ProcedureStates.PrepareBranchableForSpliceJobBound;
                        break;
                    case JobTypes.SpliceInPoP:
                        state = ProcedureStates.SpliceODFJobBound;
                        break;
                    //case JobTypes.BranchOff:
                    //    state = ProcedureStates.BranchOffJobBound;
                    //    break;
                    default:
                        break;
                }

                if (state.HasValue == true)
                {
                    await AddProcedureTimeline(procedure.Value, jobId, state.Value);
                }
            }

            return true;
        }


        private void ValidateSpliceMetric(Double length)
        {
            _logger.LogInformation("check if cable metric {0} is valid", length);
            if (length <= 0)
            {
                _logger.LogInformation("cable metric should be greater then zero");
                throw new JobServicException(JobServicExceptionReasons.InvalidFinishedModel);
            }
        }

        public async Task<Boolean> FinishSpliceBranchableJob(SpliceBranchableJobFinishModel model)
        {
            _logger.LogTrace("FinishSpliceBranchableJob");

            await ValidateFinishedJobModel(model, true, () => ValidateSpliceMetric(model.CableMetric));

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishSpliceBranchable(model);
            if (created == true)
            {
                await AddProcedureTimeline(model.JobId, ProcedureStates.SpliceBranchableJobFinished);
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = now,
                    RelatedObjectId = model.JobId,
                }, model.UserAuthId);
            }

            return created;
        }

        public async Task<Boolean> FinishPrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobFinishModel model)
        {
            _logger.LogTrace("FinishPrepareBranchableForSpliceJob");

            await ValidateFinishedJobModel(model, true, null);

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishPrepareBranchableForSplice(model);
            if (created == true)
            {
                // await AddProcedureTimeline(model.JobId, ProcedureStates.PrepareBranchableForSpliceJobFinished);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = now,
                    RelatedObjectId = model.JobId,
                }, model.UserAuthId);
            }

            return created;
        }

        public async Task<Boolean> FinishSpliceODFJob(SpliceODFJobFinishModel model)
        {
            _logger.LogTrace("SpliceODFJobFinishModel");

            await ValidateFinishedJobModel(model, true, () => ValidateSpliceMetric(model.CableMetric));

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishSpliceODF(model);
            if (created == true)
            {
                //await AddProcedureTimeline(model.JobId, ProcedureStates.SpliceODFJobFinished);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = now,
                    RelatedObjectId = model.JobId,
                }, model.UserAuthId);
            }

            return created;
        }

        public async Task<Int32> CreateSpliceBranchableJob(SpliceBranchableJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateSpliceBranchableJob");

            if (model == null)
            {
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if flat with id {0} exists", model.FlatId);
            if (await _storage.CheckIfFlatExists(model.FlatId) == false)
            {
                _logger.LogInformation("flat with id {0} not found", model.FlatId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if contact with id {0} exists", model.CustomerContactId);
            if (await _storage.CheckIfContactExists(model.CustomerContactId) == false)
            {
                _logger.LogInformation("contact with id {0} not found", model.CustomerContactId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if flat with id {0} is connected", model.FlatId);
            if (await _storage.CheckIfBuildingConnenctionIsFinishedByFlatId(model.FlatId) == false)
            {
                _logger.LogInformation("building with flat  {0} is not connected", model.FlatId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if building with id {0} has a cable injected");
            if (await _storage.CheckIfCableIsInjectedByFlatId(model.FlatId) == false)
            {
                _logger.LogInformation("building with id {0} has no cable injected");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if flat with id {0} is already spliced", model.FlatId);
            if (await _storage.CheckIfSpliceIsFinishedByFlatId(model.FlatId) == true)
            {
                _logger.LogInformation("flat with  id {0} is already splice", model.FlatId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreateSpliceBranchableJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId,
            }, userAuthId);

            Int32? connectBuildingJobId = await _storage.GetConnectBuildingJobIdByFlatId(model.FlatId);
            if (connectBuildingJobId.HasValue == true)
            {
                Int32? procedureId = await _storage.GetProcecureIdByJobId(connectBuildingJobId.Value);
                if (procedureId.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.SpliceBranchableJobCreated);
                }
            }

            return jobId;
        }

        public async Task<Int32> CreatePrepareBranchableForSpliceJob(PrepareBranchableForSpliceJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreatePrepareBranchableForSpliceJob");

            if (model == null)
            {
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if branchable with id {0} exists", model.BranchableId);
            if (await _storage.CheckIfBranchableExists(model.BranchableId) == false)
            {
                _logger.LogInformation("branchable with id {0} not found", model.BranchableId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if branchable with id {0} is already prepared for splice jobs", model.BranchableId);
            if (await _storage.CheckIfBranchableIsPreparedForSplice(model.BranchableId) == true)
            {
                _logger.LogInformation("branchable with id {0} is already prepared for splice", model.BranchableId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreatePrepareBranchableForSpliceJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId,
            }, userAuthId);

            return jobId;
        }

        public async Task<Int32> CreateSpliceODFJob(SpliceODFJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateSpliceODFJob");

            if (model == null)
            {
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if cable with id {0} exists", model.MainCableId);
            if (await _storage.CheckIfCableExists(model.MainCableId) == false)
            {
                _logger.LogInformation("cable with id {0} not found", model.MainCableId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if main cable  with id {0} is already spliced in odf", model.MainCableId);
            if (await _storage.CheckIfODFSpliceIsFinished(model.MainCableId) == true)
            {
                _logger.LogInformation("main cable with id {0} is already spliced in ODF", model.MainCableId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreateSpliceODFJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId,
            }, userAuthId);

            return jobId;
        }

        public async Task<SpliceBranchableJobDetailModel> GetSpliceBranchableJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetSpliceBranchableJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if splice branchable job with id {0} exists");
            if (await _storage.CheckIfSpliceBranchableJobExist(jobId) == false)
            {
                _logger.LogInformation("splice branchable job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            SpliceBranchableJobDetailModel result = await _storage.GetSpliceBranchableJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetSpliceBranchableFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<PrepareBranchableForSpliceJobDetailModel> GetPrepareBranchbaleForSpliceJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetPrepareBranchbaleForSpliceJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if preapre branchable job with id {0} exists");
            if (await _storage.CheckIfPrepareBranchableJobExists(jobId) == false)
            {
                _logger.LogInformation("preapre branchablejob with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            PrepareBranchableForSpliceJobDetailModel result = await _storage.GetPrepareBranchbaleForSpliceJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetPrepareBranchbaleForSpliceFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<SpliceODFJobDetailModel> GetSpliceODFJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetSpliceODFJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if splice ODF job with id {0} exists");
            if (await _storage.CheckIfSpliceODFJobExists(jobId) == false)
            {
                _logger.LogInformation("splice ODF with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            SpliceODFJobDetailModel result = await _storage.GetSpliceODFJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetSpliceODFFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<Boolean> CheckIfBuildingHasCable(Int32 buildingId)
        {
            _logger.LogTrace("CheckIfBuildingHasCable. Buildingid {0}", buildingId);

            _logger.LogTrace("check if building with id {0} exists", buildingId);
            if (await _storage.CheckIfBuildingExists(buildingId) == false)
            {
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            Boolean result = await _storage.CheckIfCableIsInjected(buildingId);
            return result;
        }

        public async Task<Boolean> FinishJobAdministrative(FinishJobAdministrativeModel model)
        {
            _logger.LogTrace("FinishJobAdministrative");
            if (model == null)
            {
                _logger.LogInformation("no model was given");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if user with auth id {0} exists", model.UserAuthId);
            if (await _storage.CheckIfUserExists(model.UserAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", model.UserAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if user with auth id {0} has project manager role", model.UserAuthId);
            if (await _storage.CheckIfUserHasRole(model.UserAuthId, BesaRoles.ProjectManager) == false)
            {
                _logger.LogInformation("user with auth id {0} has no project manager role", model.UserAuthId);
                throw new JobServicException(JobServicExceptionReasons.WrongRole);
            }

            _logger.LogTrace("check if job with id {0} exists", model.JobId);
            if (await _storage.CheckIfJobExists(model.JobId) == false)
            {
                _logger.LogInformation("job with id {0} not found", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if job with id {0} is already finished", model.JobId);
            if (await _storage.CheckIfJobIsFinished(model.JobId) == true)
            {
                _logger.LogInformation("job with id {0} is already finished", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            Int32 finishId = await _storage.FinishAndAcknowledgeJobAdministrative(model);
            JobTypes type = await _storage.GetJobType(model.JobId);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinishJobAdministrative,
                RelatedType = GetMessageRelatedObjectTypeByJobType(type),
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = model.JobId,
            }, model.UserAuthId);

            Int32? procedureId = await _storage.GetProcecureIdByJobId(model.JobId);
            if (procedureId.HasValue == true)
            {
                ProcedureStates? finishStatus = null;
                ProcedureStates? ackStatus = null;
                switch (type)
                {
                    case JobTypes.BranchOff:
                        finishStatus = ProcedureStates.BranchOffJobFinished;
                        ackStatus = ProcedureStates.BranchOffJobAcknowledegd;
                        break;
                    case JobTypes.HouseConnenction:
                        finishStatus = ProcedureStates.ConnectBuildingJobFinished;
                        ackStatus = ProcedureStates.ConnectBuildingJobAcknlowedged;
                        break;
                    case JobTypes.Inject:
                        finishStatus = ProcedureStates.InjectJobFinished;
                        ackStatus = ProcedureStates.InjectJobAcknlowedged;
                        break;
                    case JobTypes.SpliceInBranchable:
                        finishStatus = ProcedureStates.SpliceBranchableJobFinished;
                        ackStatus = ProcedureStates.SpliceBranchableJobAcknlowedged;
                        break;
                    case JobTypes.ActivationJob:
                        finishStatus = ProcedureStates.ActivatitionJobFinished;
                        ackStatus = ProcedureStates.ActivatitionJobAcknlowedged;
                        break;
                    case JobTypes.PrepareBranchableForSplice:
                        finishStatus = ProcedureStates.PrepareBranchableForSpliceJobFinished;
                        ackStatus = ProcedureStates.PrepareBranchableForSpliceJobAcknlowedged;
                        break;
                    case JobTypes.SpliceInPoP:
                        finishStatus = ProcedureStates.SpliceODFJobFinished;
                        ackStatus = ProcedureStates.SpliceODFJobAcknlowedged;
                        break;
                    default:
                        break;
                }

                if (finishStatus.HasValue == true && ackStatus.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, model.JobId, finishStatus.Value, _commentForAdministrativlyFinished, model.FinishedAt);
                }

                if (ackStatus.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, model.JobId, ackStatus.Value, _commentForAdministrativlyFinished, model.FinishedAt);

                    if (ackStatus.Value == ProcedureStates.ConnectBuildingJobAcknlowedged)
                    {
                        Boolean informSales = await _storage.CheckIfContactAfterFinishedJobShouldCreated(procedureId.Value);
                        if (informSales == true)
                        {
                            Int32 buildingId = await _storage.GetBuildingIdByConnectionJobId(model.JobId);
                            Int32 concatAfterFinishedJobId = await _storage.CreateContactAfterFinishedJob(new CreateContactAfterFinishedJobModel
                            {
                                BuildingId = buildingId,
                                RelatedProcedureId = procedureId.Value,
                            });

                            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                            {
                                Action = MessageActions.Create,
                                RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob,
                                Level = ProtocolLevels.Sucesss,
                                Timestamp = DateTimeOffset.Now,
                                RelatedObjectId = concatAfterFinishedJobId,
                            });

                        }
                    }
                }

                if (await _storage.CheckCheckIfProcedureIsFinished(procedureId.Value) == true)
                {
                    await _storage.MarkProcedureAsFinished(procedureId.Value);

                    await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                    {
                        Action = MessageActions.FinishedProcedures,
                        RelatedType = type == JobTypes.HouseConnenction ? MessageRelatedObjectTypes.BuildingConnenctionProcedure : MessageRelatedObjectTypes.CustomerConnenctionProcedure,
                        Level = ProtocolLevels.Sucesss,
                        Timestamp = DateTimeOffset.Now,
                        RelatedObjectId = procedureId.Value,
                    });
                }
            }

            if (model.CreateNextJobInPipeLine == true)
            {
                JobTypes jobType = await _storage.GetJobType(model.JobId);
                if (jobType == JobTypes.HouseConnenction)
                {
                    await CreateInjectJobAfterAcknowledge(model.JobId, procedureId);
                }
                else if (jobType == JobTypes.Inject && procedureId.HasValue == true)
                {
                    await CreateSpliceJobAfterAcknowledge(model.JobId, procedureId.Value);
                }
                else if (jobType == JobTypes.SpliceInBranchable && procedureId.HasValue == true)
                {
                    await CreateActivationJobAfterAcknlowedge(model.JobId, procedureId.Value);
                }
            }

            return true;
        }

        public async Task<Int32> CreateInventoryJob(InventoryJobCreateModel model, String userAuthId)
        {
            if (model == null)
            {
                _logger.LogInformation("no model");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            if (model.Articles == null || model.Articles.Count == 0)
            {
                _logger.LogInformation("no items was given");
                throw new JobServicException(JobServicExceptionReasons.InvalidData);
            }

            if (model.Articles.Values.Min() <= 0)
            {
                _logger.LogInformation("items should have a positive amount");
                throw new JobServicException(JobServicExceptionReasons.InvalidData);
            }

            _logger.LogTrace("check if used articels exists");
            if (await _storage.CheckIfArticlesExists(model.Articles.Keys) == false)
            {
                _logger.LogInformation("not all articles was found in db");
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if contact with id {0} exists", model.IssuanceToId);
            if (await _storage.CheckIfContactExists(model.IssuanceToId) == false)
            {
                _logger.LogInformation("contact with id {0} not found", model.IssuanceToId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreateInventoryJob(model);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.InventoryJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId,
            }, userAuthId);

            return jobId;
        }

        public async Task<Int32> CreateActivationJob(ActivationJobCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateActivationJob");

            if (model == null)
            {
                _logger.LogInformation("no model was given");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if flat with id {0} exists", model.FlatId);
            if (await _storage.CheckIfFlatExists(model.FlatId) == false)
            {
                _logger.LogTrace("flat with id {0} not found", model.FlatId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if contact with id {0} exists", model.CustomerContactId);
            if (await _storage.CheckIfContactExists(model.CustomerContactId) == false)
            {
                _logger.LogTrace("contact with id {0} not found", model.CustomerContactId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            if (model.DeviceId.HasValue == true)
            {
                _logger.LogTrace("check if device with id {0} exists", model.DeviceId);
                if (await _storage.CheckIfArticleExists(model.DeviceId.Value) == false)
                {
                    _logger.LogInformation("device with id {0} not found", model.DeviceId);
                    throw new JobServicException(JobServicExceptionReasons.NotFound);
                }
            }

            _logger.LogTrace("check if flat has already activation job");
            if (await _storage.CheckIfActivationJobExitsByFlatId(model.FlatId) == true)
            {
                _logger.LogInformation("flat with id {0} has already a activation job");
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            await CheckIfUserExists(userAuthId);

            Int32 jobId = await _storage.CreateActivationJob(model);

            Int32? connectBuildingJobId = await _storage.GetInjectJobIdByFlatId(model.FlatId);
            if (connectBuildingJobId.HasValue == true)
            {
                Int32? procedureId = await _storage.GetProcecureIdByJobId(connectBuildingJobId.Value);
                if (procedureId.HasValue == true)
                {
                    await AddProcedureTimeline(procedureId.Value, jobId, ProcedureStates.ActivatitionJobCreated);
                }
            }

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.ActivationJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = jobId,
            }, userAuthId);

            return jobId;
        }

        public async Task<IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel>> GetOpenPrepareBranchableForSpliceJobsOverview()
        {
            _logger.LogTrace("GetOpenPrepareBranchableForSpliceJobsOverview");
            IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel> result = await _storage.GetOpenPrepareBranchableForSpliceJobsOverview();
            return result;
        }

        public async Task<IEnumerable<SimpleSpliceODFJobOverviewModel>> GetOpenSpliceODFJobsOverview()
        {
            _logger.LogTrace("GetOpenSpliceODFJobsOverview");
            IEnumerable<SimpleSpliceODFJobOverviewModel> result = await _storage.GetOpenSpliceODFJobsOverview();
            return result;
        }

        public async Task<IEnumerable<SimpleActivationJobOverviewModel>> GetOpenActivationJobsOverview()
        {
            _logger.LogTrace("GetOpenActivationJobsOverview");
            IEnumerable<SimpleActivationJobOverviewModel> result = await _storage.GetOpenActivationJobsOverview();
            return result;
        }

        private async Task AddProcedureTimeline(Int32 jobId, ProcedureStates state)
        {
            Int32? procedureId = await _storage.GetProcecureIdByJobId(jobId);
            if (procedureId.HasValue == true)
            {
                await AddProcedureTimeline(procedureId.Value, jobId, state);
            }
        }

        public async Task<Boolean> FinishActivationJob(ActivationJobFinishModel model)
        {
            _logger.LogTrace("ActivationJobFinishModel");
            await ValidateFinishedJobModel(model, false, () => { });

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Boolean created = await _storage.FinishActivationJob(model);
            if (created == true)
            {
                await AddProcedureTimeline(model.JobId, ProcedureStates.ActivatitionJobFinished);

                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.ActivationJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = model.JobId,
                }, model.UserAuthId);
            }

            return created;
        }

        public async Task<IEnumerable<SpliceODFJobOverviewModel>> GetOpenSpliceODFJobs()
        {
            _logger.LogTrace("GetOpenSpliceODFJobs");
            IEnumerable<SpliceODFJobOverviewModel> result = await _storage.GetOpenSpliceODFJobs();
            return result;
        }

        public async Task<IEnumerable<ActivationJobOverviewModel>> GetOpenActivationJobsAsOverview()
        {
            _logger.LogTrace("GetOpenActivationJobsAsOverview");
            IEnumerable<ActivationJobOverviewModel> result = await _storage.GetOpenActivationJobsAsOverview();
            return result;
        }

        public async Task<ActivationJobDetailModel> GetActivationJobDetails(Int32 jobId, String requesterAuthId)
        {
            _logger.LogTrace("GetActivationJobDetails. jobId {0}", jobId);

            _logger.LogTrace("check if activation job with id {0} exists");
            if (await _storage.CheckIfActivationJobExists(jobId) == false)
            {
                _logger.LogInformation("activation job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            ActivationJobDetailModel result = await _storage.GetActivationJobDetails(jobId);
            if (await CheckIfFinishInfoToJob(result, requesterAuthId) == true)
            {
                result.FinishInfo = await _storage.GetActivationFinishedJobDetails(result.Id);
            };

            return result;
        }

        public async Task<IEnumerable<InventoryJobOverviewModel>> GetOpenIventoryJobs()
        {
            _logger.LogTrace("GetOpenIventoryJobs");
            IEnumerable<InventoryJobOverviewModel> result = await _storage.GetOpenIventoryJobs();
            return result;
        }

        public async Task<InventoryJobDetailModel> GetInventoryobDetails(Int32 jobId)
        {
            _logger.LogTrace("GetInventoryobDetails. jobId: {0}", jobId);

            _logger.LogTrace("check if inventory job with id {0} exists", jobId);
            if (await _storage.CheckIfInventoryJobExists(jobId) == false)
            {
                _logger.LogInformation("inventory job with id {0} not found", jobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            InventoryJobDetailModel details = await _storage.GetInventoryobDetails(jobId);
            return details;
        }

        public async Task<Boolean> FinishInventoryJob(InventoryFinishedJobModel model)
        {
            _logger.LogTrace("FinishInventoryJob");

            await ValidateFinishedJobModel(model, false, () => { });

            DateTimeOffset now = DateTimeOffset.Now;
            model.Timestamp = now;

            Int32 id = await _storage.FinishInventoryJob(model);

            if (id > 0)
            {
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobFinished,
                    RelatedType = MessageRelatedObjectTypes.InventoryJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = model.JobId,
                }, model.UserAuthId);
            }

            return id > 0;
        }

        public async Task<Int32> CopyFinishFromBranchOffToConnectBuildingJob(Int32 branchOffJobId, String userAuthId)
        {
            _logger.LogTrace("CopyFinishFromBranchOffToConnectBuildingJob. branchOffJobId: {0}; userAuthId: {1} ", branchOffJobId, userAuthId);

            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if branch off job with id {0} exists", branchOffJobId);
            if (await _storage.CheckIfBranchOffJobExist(branchOffJobId) == false)
            {
                _logger.LogInformation("branch off job with id {0} not found in database", branchOffJobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if branch off job with id {0} is finished", branchOffJobId);
            if (await _storage.CheckIfJobIsFinished(branchOffJobId) == false)
            {
                _logger.LogInformation("branch off job with id {0} is not finished", branchOffJobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("get building id by branch off job id");
            Int32 buildingId = await _storage.GetBuildingIdByBranchOffJobId(branchOffJobId);
            _logger.LogTrace("branch off job with id {0} resolved to building with id {1}", branchOffJobId, buildingId);

            _logger.LogTrace("get connect building job by building id {0}", buildingId);
            Int32? connectBuildingJob = await _storage.GetConnectBuildingJobIdByBuildingId(buildingId);
            _logger.LogTrace("building with id {0} resolved to job with id {1}", buildingId, connectBuildingJob.HasValue == true ? connectBuildingJob.Value.ToString() : "null");

            if (connectBuildingJob.HasValue == false)
            {
                _logger.LogInformation("no connect building job found with building id {0}", buildingId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if connect building job with id {0} is finished", connectBuildingJob.Value);
            if (await _storage.CheckIfJobIsFinished(connectBuildingJob.Value) == true)
            {
                _logger.LogInformation("connect building job with id {0} is finished is already finished", connectBuildingJob.Value);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("get branch off finished model with id {0}", branchOffJobId);
            FinishBranchOffDetailModel existingFinishModel = await _storage.GetBranchOffFinishedJobDetails(branchOffJobId);

            FinishBuildingConnenctionJobModel finishBuildingConnenctionJob = new FinishBuildingConnenctionJobModel
            {
                Comment = existingFinishModel.Comment,
                DuctChanged = existingFinishModel.DuctChanged,
                FileIds = existingFinishModel.Files.Select(x => x.Id).ToList(),
                JobId = connectBuildingJob.Value,
                ProblemHappend = existingFinishModel.ProblemHappend,
                Timestamp = existingFinishModel.FinishedAt,
                UserAuthId = userAuthId,
            };

            _logger.LogTrace("finish connect building job");
            await _storage.FinishBuildingConnenctionJob(finishBuildingConnenctionJob);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinished,
                RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = connectBuildingJob.Value,
            }, userAuthId);

            _logger.LogTrace("check if connect building job with id {0} is associate with a procedure", connectBuildingJob.Value);
            Int32? procedureId = await _storage.GetProcecureIdByJobId(connectBuildingJob.Value);
            if (procedureId.HasValue == true)
            {
                _logger.LogTrace("connect building job with id {0} resolved to procedure id with id {1}", connectBuildingJob.Value, procedureId.Value);

                await AddProcedureTimeline(procedureId.Value, branchOffJobId, ProcedureStates.BranchOffFinsshedTransfromToConnectBuilding);
                await AddProcedureTimeline(procedureId.Value, connectBuildingJob.Value, ProcedureStates.ConnectBuildingJobFinished);
            }

            _logger.LogTrace("check if branch off job with id {0} is acknowledged", branchOffJobId);
            if (await _storage.CheckIfJobIsAcknowledged(branchOffJobId) == true)
            {
                _logger.LogTrace("acknowled connect building job with id {0}", connectBuildingJob.Value);
                AcknowledgeJobModel ackModel = new AcknowledgeJobModel { JobId = connectBuildingJob.Value, UserAuthId = userAuthId, Timestamp = DateTimeOffset.Now };

                Boolean acknowledged = await _storage.AcknowledgeJob(ackModel);
                if (acknowledged == true)
                {
                    await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                    {
                        Action = MessageActions.JobAcknowledged,
                        RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob,
                        Level = ProtocolLevels.Sucesss,
                        Timestamp = DateTimeOffset.Now,
                        RelatedObjectId = connectBuildingJob.Value,
                    }, userAuthId);

                    if (procedureId.HasValue == true)
                    {
                        await AddProcedureTimeline(procedureId.Value, connectBuildingJob.Value, ProcedureStates.ConnectBuildingJobAcknlowedged);

                        if (await _storage.CheckCheckIfProcedureIsFinished(procedureId.Value) == true)
                        {
                            await _storage.MarkProcedureAsFinished(procedureId.Value);

                            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                            {
                                Action = MessageActions.FinishedProcedures,
                                RelatedType = MessageRelatedObjectTypes.BuildingConnenctionProcedure,
                                Level = ProtocolLevels.Sucesss,
                                Timestamp = DateTimeOffset.Now,
                                RelatedObjectId = procedureId.Value,
                            });
                        }
                    }

                    await CreateInjectJobAfterAcknowledge(connectBuildingJob.Value, procedureId);
                }
            }

            return connectBuildingJob.Value;
        }

        public async Task<IEnumerable<SimpleJobOverview>> SearchBranchOffJobByBuildingNameQuery(String query, JobStates? expectedState, Int32 amount)
        {
            Int32 minAmount = 1;
            Int32 maxAmount = 50;

            _logger.LogTrace("GetBranchOffJobByBuildingNameQuery. Query: {0}, jobState: {1} amount: {2]", query, expectedState.HasValue == false ? "null" : expectedState.Value.ToString(), amount);
            if (String.IsNullOrEmpty(query) == true)
            {
                _logger.LogInformation("no query given. null result back");
                return new List<SimpleJobOverview>();
            }

            if (amount < minAmount || amount > maxAmount)
            {
                _logger.LogInformation("invalid amount. amount was {0}. expected range between {1} and {2}", amount, minAmount, maxAmount);
                throw new JobServicException(JobServicExceptionReasons.InvalidAmount);
            }

            IEnumerable<SimpleJobOverview> result = await _storage.SearchBranchOffJobByBuildingNameQuery(query, expectedState, amount);
            return result;
        }

        private ProcedureStates GetUnboundProcedureStateByJobType(JobTypes jobType)
        {
            ProcedureStates procedureState = ProcedureStates.BranchOffJobUnboud;
            switch (jobType)
            {
                case JobTypes.BranchOff:
                    procedureState = ProcedureStates.BranchOffJobUnboud;
                    break;
                case JobTypes.HouseConnenction:
                    procedureState = ProcedureStates.ConnectBuildingJobUnboud;
                    break;
                case JobTypes.Inject:
                    procedureState = ProcedureStates.InjectJobUnboud;
                    break;
                case JobTypes.SpliceInBranchable:
                    procedureState = ProcedureStates.SpliceBranchableJobUnboud;
                    break;
                case JobTypes.SpliceInPoP:
                    procedureState = ProcedureStates.SpliceODFJobUnboud;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    procedureState = ProcedureStates.PrepareBranchableForSpliceJobUnboud;
                    break;
                case JobTypes.ActivationJob:
                    procedureState = ProcedureStates.ActivatitionJobUnboud;
                    break;
                case JobTypes.Inventory:
                    break;
                case JobTypes.ContactAfterFinished:
                    break;
                default:
                    break;
            }

            return procedureState;
        }

        public async Task<Boolean> DeleteCollectionJob(Int32 collectionJobId, String userAuthId)
        {
            _logger.LogTrace("DeleteCollectionJob. collectionJobId: {0}", collectionJobId);

            _logger.LogTrace("check if collection job With Id {0} exists", collectionJobId);
            if (await _storage.CheckIfCollectionJobExists(collectionJobId) == false)
            {
                _logger.LogWarning("collection job With Id {0} not found", collectionJobId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            await CheckIfUserExists(userAuthId);

            IEnumerable<Int32> jobIds = await _storage.GetAllJobIdsFromCollectionJobId(collectionJobId);
            foreach (Int32 jobId in jobIds)
            {
                Int32? procedureId = await _storage.GetProcecureIdByJobId(jobId);
                if (procedureId.HasValue == false) continue;

                JobTypes type = await _storage.GetJobType(jobId);
                ProcedureStates state = GetUnboundProcedureStateByJobType(type);
                await AddProcedureTimeline(jobId, state);
            }

            Boolean result = await _storage.DeleteCollectionJob(collectionJobId);
            if (result == true)
            {
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.Delete,
                    RelatedType = MessageRelatedObjectTypes.CollectionJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = collectionJobId,
                }, userAuthId);
            }
            return result;
        }

        public async Task<IEnumerable<JobCollectionOverviewModel>> GetJobCollectionOverview(JobCollectionFilterModel filterModel)
        {
            _logger.LogTrace("GetJobCollectionOverview");
            if (filterModel == null)
            {
                _logger.LogInformation("filer model is null");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check filter start: {0}", filterModel.Start);
            if (filterModel.Start < 0)
            {
                _logger.LogInformation("filter model with invalid start. start should be equal or greater zero. start has value: {0}", filterModel.Start);
                throw new JobServicException(JobServicExceptionReasons.InvalidFilterModel);
            }

            _logger.LogTrace("check filter amount: {0}", filterModel.Amount);
            if (filterModel.Amount <= 0 || filterModel.Amount > _maxJobAmount)
            {
                _logger.LogInformation("filter amount: {0} is invalid. Amount should be greater zero and less {1}", filterModel.Amount, _maxJobAmount);
                throw new JobServicException(JobServicExceptionReasons.InvalidFilterModel);
            }

            IEnumerable<JobCollectionOverviewModel> result = await _storage.GetJobCollectionOverview(filterModel);
            return result;
        }

        public async Task<Int32> CreateRequestFromContactCustomerJob(RequestCustomerConnectionByContactModel model, String userAuthId)
        {
            _logger.LogTrace("RequestCustomerConnectionByContactModel");
            _logger.LogTrace("check if model is null");

            if (model == null)
            {
                _logger.LogInformation("no model was given for CreateRequestFromContactCustomerJob");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if either appointment set or connect as soon as possible");
            if (model.ConnectionAppointment.HasValue == false && model.ActivationAsSoonAsPossible == false)
            {
                _logger.LogInformation("either appoint or connect as soon as possible should set");
                throw new JobServicException(JobServicExceptionReasons.InvalidData);
            }

            _logger.LogTrace("check if contact customer job with id {0} exists");
            if (await _storage.CheckIfCustomerContactJobExists(model.JobId) == false)
            {
                _logger.LogInformation("customer contact job with id {0} not exists");
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if customer contact job with id {0} is already finsihed");
            if (await _storage.CheckIfJobIsFinished(model.JobId) == true)
            {
                _logger.LogInformation("the customer contact job with id {0} is already finished. Unable to create a request", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("check if user with auth id {0} exits", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            _logger.LogTrace("get contact after finished job model with id {0}", model.JobId);
            ContactAfterFinishedJobModel job = await _storage.GetContactAfterFinishedJobModelById(model.JobId);

            _logger.LogTrace("complete flat id or building from job model");
            if (job.BuildingId.HasValue == true && job.FlatId.HasValue == false)
            {
                IEnumerable<SimpleFlatOverviewModel> flats = await _storage.GetFlatsByBuildingId(job.BuildingId.Value);
                job.FlatId = flats.First().Id;
            }
            else if (job.FlatId.HasValue == true && job.BuildingId.HasValue == false)
            {
                Int32 buildingId = await _storage.GetBuildingIdByFlatId(job.FlatId.Value);
                job.BuildingId = buildingId;
            }

            CustomerConnectRequestCreateModel createModel = new CustomerConnectRequestCreateModel
            {
                ActivationAsSoonAsPossible = model.ActivationAsSoonAsPossible,
                ActivePointNearSubscriberEndpoint = model.ActivePointNearSubscriberEndpoint,
                BuildingId = job.BuildingId.Value,
                FlatId = job.FlatId.Value,
                ConnectionAppointment = model.ConnectionAppointment,
                DuctAmount = model.DuctAmount,
                OnlyHouseConnection = false,
                PowerForActiveEquipment = model.PowerForActiveEquipment,
                SubscriberEndpointLength = model.SubscriberEndpointLength,
                SubscriberEndpointNearConnectionPoint = model.SubscriberEndpointNearConnectionPoint,
                CustomerPersonId = job.ContactId,
            };

            Int32 requestId = await _storage.CreateCustomerConnenctionRequest(createModel);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.CustomerRequest,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = requestId,
            });

            PersonInfo customer = await _storage.GetContactById(job.ContactId);
            String buldingStreetName = await _storage.GetBuildingStreetNameByFlatId(job.FlatId.Value);

            CustomerConnectionProcedureCreateModel procedureCreateModel = new CustomerConnectionProcedureCreateModel
            {
                Appointment = model.ConnectionAppointment,
                AsSoonAsPossible = model.ActivationAsSoonAsPossible,
                CustomerContactId = job.ContactId,
                Name = CustomerService.GetNameForConnectCustomerProcedure(buldingStreetName, customer),
                FlatId = job.FlatId.Value,
                FirstElement = new ProcedureTimelineCreateModel
                {
                    RelatedRequestId = requestId,
                    State = ProcedureStates.RequestCreated,
                },
            };

            Int32 procedureId = await _storage.CreateCustomerConnectionProcedure(procedureCreateModel);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                RelatedType = MessageRelatedObjectTypes.CustomerConnenctionProcedure,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = procedureId,
            });

            DateTimeOffset now = DateTimeOffset.Now;
            ContactAfterFinishedFinishModel finishedModel = new ContactAfterFinishedFinishModel { JobId = model.JobId, RequestId = requestId, Timestamp = now, UserAuthId = userAuthId };

            await _storage.FinishContactAfterFinishedJob(finishedModel);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinished,
                RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob ,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = model.JobId,
            }, userAuthId);

            await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = model.JobId, UserAuthId = userAuthId, Timestamp = now });

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobAcknowledged,
                RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = model.JobId,
            });

            return requestId;
        }

        private async Task CheckIfCustomerContactJobExists(Int32 jobId)
        {
            _logger.LogTrace("check if contact customer job with id {0} exists");
            if (await _storage.CheckIfCustomerContactJobExists(jobId) == false)
            {
                _logger.LogInformation("customer contact job with id {0} not exists");
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }
        }

        public async Task<Boolean> FinishContactAfterFinished(ContactAfterFinishedUnsucessfullyModel model, String userAuthId)
        {
            _logger.LogTrace("FinishContactCAfterFinished");

            _logger.LogTrace("check if model is null");
            if (model == null)
            {
                _logger.LogInformation("no model was given for FinishContactCAfterFinished");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }

            _logger.LogTrace("check if user with auth id {0} exits", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new JobServicException(JobServicExceptionReasons.NotFound);
            }

            await CheckIfCustomerContactJobExists(model.JobId);

            _logger.LogTrace("check if customer contact job with id {0} is already finsihed");
            if (await _storage.CheckIfJobIsFinished(model.JobId) == true)
            {
                _logger.LogInformation("the customer contact job with id {0} is already finished. Unable to create a request", model.JobId);
                throw new JobServicException(JobServicExceptionReasons.InvalidOperation);
            }

            DateTimeOffset now = DateTimeOffset.Now;
            ContactAfterFinishedFinishModel finishedModel = new ContactAfterFinishedFinishModel
            {
                JobId = model.JobId,
                RequestId = null,
                Timestamp = now,
                UserAuthId = userAuthId,
                ProblemHappend = new ExplainedBooleanModel
                {
                    Value = true,
                    Description = model.Comment,
                }
            };

            await _storage.FinishContactAfterFinishedJob(finishedModel);

            await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobFinished,
                RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob,
                Level = ProtocolLevels.Sucesss,
                Timestamp = DateTimeOffset.Now,
                RelatedObjectId = finishedModel.JobId,
            }, userAuthId);

            Boolean result = await _storage.AcknowledgeJob(new AcknowledgeJobModel { JobId = model.JobId, UserAuthId = userAuthId, Timestamp = now });
            if (result == true)
            {
                await ProtocolService.CreateProtocolEntry(new ProtocolEntyCreateModel
                {
                    Action = MessageActions.JobAcknowledged,
                    RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob,
                    Level = ProtocolLevels.Sucesss,
                    Timestamp = DateTimeOffset.Now,
                    RelatedObjectId = finishedModel.JobId,
                });
            }
            return result;
        }

        public async Task<IEnumerable<ContactAfterSalesJobOverview>> GetContactAfterSalesJobs(ContactAfterFinishedFilterModel filter)
        {
            _logger.LogTrace("GetContactAfterSalesJobs");

            _logger.LogTrace("check if filter is valid");
            if (filter == null)
            {
                _logger.LogInformation("filter for  GetContactAfterSalesJobs not set");
                throw new JobServicException(JobServicExceptionReasons.NoModel);
            }
            if (filter.Amount < 0 || filter.Amount > _maxJobAmount)
            {
                _logger.LogInformation("filter amount for GetContactAfterSalesJobs should be greater zero and less then {0}", _maxJobAmount);
                throw new JobServicException(JobServicExceptionReasons.InvalidFilterModel);
            }
            if (filter.Start < 0)
            {
                _logger.LogInformation("filter start for GetContactAfterSalesJobs should be greater zero ");
                throw new JobServicException(JobServicExceptionReasons.InvalidFilterModel);
            }

            IEnumerable<ContactAfterSalesJobOverview> result = await _storage.GetContactAfterSalesJobs(filter);
            return result;
        }

        public async Task<ContactAfterSalesJobDetails> GetContactAfterSalesJobDetail(Int32 jobId)
        {
            _logger.LogTrace("GetContactAfterSalesJobDetail. jobId: {0}", jobId);

            await CheckIfCustomerContactJobExists(jobId);

            ContactAfterSalesJobDetails details = await _storage.GetContactAfterSalesJobDetail(jobId);
            return details;
        }



        #endregion
    }
}
