﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class MessageService : IMessageService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly ILogger<MessageService> _logger;
        private const Int32 _maxMessageAmount = 200;

        #endregion

        #region Constructor

        public MessageService(
            IBesaDataStorage storage,
            ILogger<MessageService> logger)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        private async Task CheckUser(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new MessageServiceException(MessageServiceExceptionReasons.UserNotFound);
            }
        }

        private MessageModel DeserializeMessageDetails(MessageRawModel rawModel)
        {
            MessageModel result = new MessageModel
            {
                Action = rawModel.Action,
                Id = rawModel.Id,
                MarkedAsRead = rawModel.MarkedAsRead,
                RelatedObjectType = rawModel.RelatedObjectType,
                SourceUser = rawModel.SourceUser,
                TimeStamp = rawModel.TimeStamp,
            };
            if (String.IsNullOrEmpty(rawModel.Details) == false)
            {
                try
                {
                    MessageActionDetailsModel detail = null;

                    switch (rawModel.Action)
                    {
                        case MessageActions.Create:
                        case MessageActions.Update:
                        case MessageActions.Delete:
                        case MessageActions.CreateViaImport:
                            detail = JsonConvert.DeserializeObject<MessageActionDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.JobFinished:
                            detail = JsonConvert.DeserializeObject<MessageActionJobFinishedDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.JobDiscared:
                        case MessageActions.InformJobFinisherAboutDiscard:
                            detail = JsonConvert.DeserializeObject<MessageActionJobDiscardDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.JobAcknowledged:
                        case MessageActions.InformJobFinisherAboutAcknlowedged:
                            detail = JsonConvert.DeserializeObject<MessageActionJobAcknowledgedDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.JobBound:
                            detail = JsonConvert.DeserializeObject<MessageActionJobBoundDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.RemoveEmployeeToCompany:
                        case MessageActions.AddEmployeeToCompany:
                            detail = JsonConvert.DeserializeObject<MessageActionEmployesBoundDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.UpdateFromAdapter:
                        case MessageActions.AddFlatsToBuildingWithOneUnit:
                        case MessageActions.AddFlatToBuilding:
                        case MessageActions.UpdateAllCableTypesFromAdapter:
                        case MessageActions.UpdateBuildingCableInfoFromAdapter:
                        case MessageActions.UpdateBuildingNameFromAdapter:
                        case MessageActions.UpdateBuildigsFromAdapter:
                        case MessageActions.UpdatePopInformation:
                        case MessageActions.UpdateMainCables:
                        case MessageActions.UpdateSplices:
                        case MessageActions.UpdateMainCableForBranchable:
                        case MessageActions.UpdatePatchConnections:
                        case MessageActions.UpdateCablesFromAdapter:
                            detail = JsonConvert.DeserializeObject<MessageActionUpdateFromAdapterDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.CreateBranchOffJobs:
                            break;
                        case MessageActions.RemoveMemberState:
                            detail = JsonConvert.DeserializeObject<MessageActionRemoveMemberDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.FinishedProcedures:
                            detail = JsonConvert.DeserializeObject<MessageActionProcedureFinishedDetailsModel>(rawModel.Details);
                            break;
                        case MessageActions.JobFinishJobAdministrative:
                            detail = JsonConvert.DeserializeObject<MessageActionJobFinishJobAdministrativeDetailModel>(rawModel.Details);
                            break;
                        case MessageActions.CollectionJobBoundToCompany:
                            detail = JsonConvert.DeserializeObject<MessageActionCollectionJobDetailsModel>(rawModel.Details);
                            break;
                        default:
                            break;
                    }

                    result.ActionDetails = detail;
                }
                catch (Exception)
                {
                    _logger.LogInformation("unable to deserialize message details model");
                }
            }

            return result;
        }

        public async Task<IEnumerable<MessageModel>> GetMessages(Boolean onlyUnreaded, String userAuthId)
        {
            _logger.LogTrace("GetMessages. onlyUnreaded: {0}, userAuthId: {1}", onlyUnreaded, userAuthId);
            await CheckUser(userAuthId);
            IEnumerable<MessageRawModel> result = await _storage.GetMessages(onlyUnreaded, userAuthId);
            return result.Select(x => DeserializeMessageDetails(x)).ToList();
        }

        public async Task<IEnumerable<MessageModel>> GetMessages(MessageFilterModel filterModel, String userAuthId)
        {
            if (filterModel.Start < 0)
            {
                _logger.LogInformation("invalid start amount for message filter. start value: {0}", filterModel.Start);
                throw new MessageServiceException(MessageServiceExceptionReasons.InvalidFilter);
            }
            if (filterModel.Amount <= 0 || filterModel.Amount > _maxMessageAmount)
            {
                _logger.LogInformation("invalid filter amount for message filter. amount should be between {0} and {1}. Actual: {2}"
                    , 0, _maxMessageAmount, filterModel.Amount);
                throw new MessageServiceException(MessageServiceExceptionReasons.InvalidFilter);
            }

            if (filterModel.StartTime.HasValue == true && filterModel.EndTime.HasValue == true && filterModel.StartTime.Value >= filterModel.EndTime.Value)
            {
                _logger.LogInformation("invalid message filter model. Start should be earlier as end");
                throw new MessageServiceException(MessageServiceExceptionReasons.InvalidFilter);
            }

            if (filterModel.Actions == null)
            {
                filterModel.Actions = new HashSet<MessageActions>();
            }

            if (filterModel.Types == null)
            {
                filterModel.Types = new HashSet<MessageRelatedObjectTypes>();
            }

            await CheckUser(userAuthId);
            IEnumerable<MessageRawModel> result = await _storage.GetMessages(filterModel, userAuthId);
            return result.Select(x => DeserializeMessageDetails(x)).ToList();
        }

        public async Task<Boolean> MarkMessageAsRead(Int32 messageId, String userAuthId)
        {
            await CheckUser(userAuthId);

            _logger.LogTrace("check if message with id {0} exists", messageId);
            if (await _storage.CheckIfMessageExists(messageId) == false)
            {
                _logger.LogInformation("message with id {0} not exists", messageId);
                throw new MessageServiceException(MessageServiceExceptionReasons.NotFound);
            }

            _logger.LogTrace("check if message with id {0} belongs to user with auth id {1}", messageId, userAuthId);
            if (await _storage.CheckIfMessageBelongsToUser(messageId, userAuthId) == false)
            {
                _logger.LogInformation("message with id {0} not belongs to user with id {1}. unable to change read state", messageId, userAuthId);
                throw new MessageServiceException(MessageServiceExceptionReasons.InvalidOperation);
            }

            Boolean result = await _storage.SetReadStateToMessage(true, messageId);
            return result;
        }

        #endregion
    }
}
