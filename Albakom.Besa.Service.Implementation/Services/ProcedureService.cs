﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ProcedureService : IProcedureService
    {
        #region Fields

        private IBesaDataStorage _storage;
        private ILogger<ProcedureService> _logger;
        private readonly IProtocolService _protocolService;
        private const Int32 _maxAmountPerRequest = 100;

        #endregion

        #region Constructor

        public ProcedureService(
            IBesaDataStorage storage,
            ILogger<ProcedureService> logger,
            IProtocolService protocolService)
        {
            _storage = storage;
            _logger = logger;
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        #endregion

        #region Methods

        public async Task<BuildingConnectionProcedureDetailModel> GetConnectBuildingDetails(Int32 procedureId)
        {
            _logger.LogTrace("GetConnectBuildingDetails. procedureId {0}", procedureId);

            _logger.LogTrace("chek if connect building  procedure with id {0} exists", procedureId);
            if (await _storage.CheckIfBuildingConnectionProcedureExists(procedureId) == false)
            {
                _logger.LogInformation("connect building procedure with id {0} not found", procedureId);
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.NotFound);
            }

            BuildingConnectionProcedureDetailModel result = await _storage.GetConnectBuildingDetails(procedureId);
            return result;
        }

        public async Task<CustomerConnectionProcedureDetailModel> GetConnectCustomerDetails(Int32 procedureId)
        {
            _logger.LogTrace("GetConnectCustomerDetails. procedureId {0}", procedureId);

            _logger.LogTrace("chek if connect building  procedure with id {0} exists", procedureId);
            if (await _storage.CheckIfCustomerConnectionProcedureExists(procedureId) == false)
            {
                _logger.LogInformation("connect building procedure with id {0} not found", procedureId);
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.NotFound);
            }

            CustomerConnectionProcedureDetailModel result = await _storage.GetConnectCustomerDetails(procedureId);
            return result;
        }

        public async Task<IEnumerable<ProcedureOverviewModel>> GetProcedures(ProcedureFilterModel filter, ProcedureSortProperties property, SortDirections direction)
        {
            _logger.LogTrace("GetProcedures");
            if (filter == null)
            {
                _logger.LogInformation("no filter model was given");
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.NoFilter);
            }
            if (filter.Amount <= 0 || filter.Start < 0)
            {
                _logger.LogInformation("invalid filter model. amount and start have to be greater zero");
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.InvalidFilterModel);
            }

            if (filter.Amount > _maxAmountPerRequest)
            {
                _logger.LogInformation("amount greater then max amount");
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.InvalidFilterModel);
            }

            if (
                 (filter.BuildingUnitAmount.HasValue == true && filter.BuildingUnitAmountOperation.HasValue == false) ||
                 (filter.State.HasValue == true && filter.StateOperator.HasValue == false)
                 )
            {
                _logger.LogInformation("value was set, but operator is null");
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.InvalidFilterModel);

            }

            if (filter.StartDate.HasValue == true && filter.EndDate.HasValue == true && filter.StartDate.Value > filter.EndDate.Value)
            {
                _logger.LogInformation("invalid date range");
                throw new ProcedureServiceException(ProcedureServicExceptionReasons.InvalidFilterModel);
            }

            IEnumerable<ProcedureOverviewModel> result = await _storage.GetProcedures(filter, property, direction);
            return result;
        }

        public async Task<IEnumerable<Int32>> FinishProceduresThatShouldAlreadyFinished()
        {
            _logger.LogTrace("");
            IEnumerable<ProcedureWithCurrentTimelineModel> openProcedures = await _storage.GetOpenProceduresWithCurrentTimelineelement();
            _logger.LogTrace("{0} unfinished procedures found", openProcedures.Count());

            List<Int32> finishedProcedureIds = new List<int>();

            foreach (ProcedureWithCurrentTimelineModel item in openProcedures)
            {
                if (item.CurrentTimelineElement == null)
                {
                    _logger.LogInformation("procuedure with id {0} has no current timeline", item.Id);
                    continue;
                }
                if (item.CurrentTimelineElement.RelatedJobId.HasValue == false)
                {
                    _logger.LogInformation("procuedure with id {0} and current timeline element with id {1} has no related job", item.Id, item.CurrentTimelineElement.Id);
                    continue;
                }

                if (await _storage.CheckIfJobIsAcknowledged(item.CurrentTimelineElement.RelatedJobId.Value) == false)
                {
                    _logger.LogTrace("procuedure with id {0} and current timeline element with id {1} has a job with id {2} that is not acknlowdged. nothing to do", item.Id, item.CurrentTimelineElement.Id, item.CurrentTimelineElement.RelatedJobId.Value);
                    continue;
                }

                _logger.LogTrace("get job overview form job with id {0}", item.CurrentTimelineElement.RelatedJobId.Value);

                SimpleJobOverview jobOverview = await _storage.GetSimpleJobOverviewById(item.CurrentTimelineElement.RelatedJobId.Value);

                ProcedureStates current = ProcedureStates.RequestCreated;
                switch (jobOverview.JobType)
                {
                    case JobTypes.BranchOff:
                        current = ProcedureStates.BranchOffJobAcknowledegd;
                        break;
                    case JobTypes.HouseConnenction:
                        current = ProcedureStates.ConnectBuildingJobAcknlowedged;
                        break;
                    case JobTypes.Inject:
                        current = ProcedureStates.InjectJobAcknlowedged;
                        break;
                    case JobTypes.SpliceInBranchable:
                        current = ProcedureStates.SpliceBranchableJobAcknlowedged;
                        break;
                    case JobTypes.ActivationJob:
                        current = ProcedureStates.ActivatitionJobAcknlowedged;
                        break;
                    default:
                        _logger.LogWarning("uanble to resolve job tyepe to procedure step. job type is {0}", jobOverview.JobType);
                        continue;
                }

                if(current != item.ExpectedEnd)
                {
                    _logger.LogInformation("procudre with id {0} is not finished yet", item.Id);
                    continue;
                }

                ProcedureTimelineCreateModel finishedTimeline = new ProcedureTimelineCreateModel
                {
                    ProcedureId = item.Id,
                    Timestamp = DateTimeOffset.Now,
                    State = ProcedureStates.AdministrativFinished,
                };

                _logger.LogTrace("add AdministrativFinished timeline element to procedure with id {0}", item.Id);
                await _storage.AddProcedureTimelineElementIfNessesary(finishedTimeline);
                _logger.LogTrace("mark procedure with id {0} as finished", item.Id);
                await _storage.MarkProcedureAsFinished(item.Id);

                ProtocolEntyCreateModel entry = new ProtocolEntyCreateModel
                {
                    Action = MessageActions.FinishedProcedures,
                    Level = ProtocolLevels.Sucesss,
                    RelatedObjectId = item.Id,
                    RelatedType = jobOverview.JobType == JobTypes.HouseConnenction ? MessageRelatedObjectTypes.BuildingConnenctionProcedure : MessageRelatedObjectTypes.CustomerConnenctionProcedure,
                    Timestamp = DateTimeOffset.Now,
                    TriggeredByUserId = null,
                };

                await _protocolService.CreateProtocolEntry(entry);
                finishedProcedureIds.Add(item.Id);
            }

            return finishedProcedureIds;
        }

        #endregion
    }
}

