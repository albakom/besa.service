﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Helper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class ProtocolService : IProtocolService
    {
        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly IMessageSendEngine _messageEngine;
        private readonly IEmailSendEngine _emailEngine;
        private readonly ISMSSendEngine _smsEngine;
        private readonly IPushSendEngine _pushEngine;
        private readonly ILogger<ProtocolService> _logger;

        private const Int32 _maxEntriesPerRequest = 100;

        private static Dictionary<BesaRoles, IEnumerable<ProtocolUserInformModel>> _defaultRoleNotificationRelation;

        #endregion

        #region Constructor


        static ProtocolService()
        {
            FillDefaultNotificationRelations();
            List<BesaRoles> singleRoles = new List<BesaRoles>
            {
                BesaRoles.ActiveNetwork, BesaRoles.Admin, BesaRoles.AirInjector, BesaRoles.CivilWorker, BesaRoles.Inventory,
                BesaRoles.ProjectManager, BesaRoles.Sales, BesaRoles.Splice,
            };

            _possibleBesaRoles = singleRoles;
        }

        public ProtocolService(IBesaDataStorage storage,
            IMessageSendEngine messageEngine,
            IEmailSendEngine emailEngine,
            ISMSSendEngine smsEngine,
            IPushSendEngine pushEngine,
            ILogger<ProtocolService> logger)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._messageEngine = messageEngine ?? throw new ArgumentNullException(nameof(messageEngine));
            this._emailEngine = emailEngine ?? throw new ArgumentNullException(nameof(emailEngine));
            this._smsEngine = smsEngine ?? throw new ArgumentNullException(nameof(smsEngine));
            this._pushEngine = pushEngine ?? throw new ArgumentNullException(nameof(pushEngine));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        private async Task CheckIfUserExists(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists");
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new ProtocolServiceException(ProtocolServicExceptionReasons.UserNotFound);
            }
        }

        public async Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model, String userAuthId)
        {
            _logger.LogTrace("CreateProtocolEntry. userAuthId: {0}", userAuthId);

            await CheckIfUserExists(userAuthId);

            Int32 userId = await _storage.GetUserIdByAuthId(userAuthId);
            _logger.LogTrace("user with auth id {0} resolved to id {1}", userAuthId, userId);
            model.TriggeredByUserId = userId;

            Int32 id = await CreateProtocolEntry(model);
            return id;
        }

        public async Task<Int32> CreateProtocolEntry(ProtocolEntyCreateModel model)
        {
            _logger.LogTrace("CreateProtocolEntry");
            Int32 id = await _storage.CreateProtocolEntry(model);
            _logger.LogTrace("protocol entry created. id: {0}", id);

            await CreateMessagesAfterEntry(model, id);

            return id;
        }

        private async Task<MessageActionDetailsModel> GetMessageActionDetailsModel(ProtocolEntyCreateModel protocolModel, Int32 protocolEntryId)
        {
            MessageActionDetailsModel result = null;
            switch (protocolModel.Action)
            {
                case MessageActions.Create:
                case MessageActions.Update:
                case MessageActions.Delete:
                case MessageActions.CreateViaImport:
                    String name = String.Empty;
                    if (protocolModel.RelatedObjectId.HasValue == true)
                    {
                        try
                        {
                            name = await _storage.GetMessageObjectName(protocolModel.RelatedObjectId.Value, protocolModel.RelatedType);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "unable to resolve message name. Action: {0} Type {1}: ObjectId {2}",
                                protocolModel.Action,
                                protocolModel.RelatedType,
                                protocolModel.RelatedObjectId);
                        }
                    }
                    result = new MessageActionDetailsModel
                    {
                        Id = protocolModel.RelatedObjectId.Value,
                        Name = name
                    };
                    break;
                case MessageActions.JobFinished:
                    result = await _storage.GetMessageActionJobFinishedDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.JobDiscared:
                    result = await _storage.GetMessageActionJobDiscardedDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.JobAcknowledged:
                    result = await _storage.GetMessageActionJobAcknowledgedDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.JobBound:
                    result = await _storage.GetMessageActionJobBoundDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.RemoveEmployeeToCompany:
                case MessageActions.AddEmployeeToCompany:
                    result = await _storage.GetMessageActionEmployesBoundDetails(protocolEntryId);
                    break;
                case MessageActions.UpdateFromAdapter:
                case MessageActions.AddFlatsToBuildingWithOneUnit:
                case MessageActions.AddFlatToBuilding:
                case MessageActions.UpdateAllCableTypesFromAdapter:
                case MessageActions.UpdateBuildingCableInfoFromAdapter:
                case MessageActions.UpdateBuildingNameFromAdapter:
                case MessageActions.UpdateBuildigsFromAdapter:
                case MessageActions.UpdatePopInformation:
                case MessageActions.UpdateMainCables:
                case MessageActions.UpdateSplices:
                case MessageActions.UpdateMainCableForBranchable:
                case MessageActions.UpdatePatchConnections:
                case MessageActions.UpdateCablesFromAdapter:
                    result = new MessageActionUpdateFromAdapterDetailsModel { Action = protocolModel.Action, Level = protocolModel.Level, Id = -1, Name = String.Empty };
                    break;
                case MessageActions.CreateBranchOffJobs:
                    break;
                case MessageActions.RemoveMemberState:
                    result = await _storage.GetMessageActionRemoveMemberDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.FinishedProcedures:
                    result = await _storage.GetMessageActionProcedureFinishedDetails(protocolModel.RelatedObjectId.Value);
                    break;
                case MessageActions.JobFinishJobAdministrative:
                    result = await _storage.GetMessageActionJobFinishJobAdministrativeDetail(protocolModel.RelatedObjectId.Value);
                    break;
                default:
                    break;
            }

            return result;
        }

        private async Task CreateMessagesAfterEntry(ProtocolEntyCreateModel protocolModel, Int32 protocolEntryId)
        {
            MessageRelatedObjectTypes realtedObjectTyoe = protocolModel.RelatedType;

            IDictionary<Int32, ProtocolNotificationTypes> usersToInform =
                await _storage.GetUsersToInform(realtedObjectTyoe, protocolModel.Action);

            if (usersToInform.Count > 0)
            {
                MessageActionDetailsModel messageDetails = await GetMessageActionDetailsModel(protocolModel, protocolEntryId);

                foreach (KeyValuePair<Int32, ProtocolNotificationTypes> inform in usersToInform)
                {
                    MessageCreateModel messageModel = new MessageCreateModel
                    {
                        Action = protocolModel.Action,
                        RelatedObjectType = protocolModel.RelatedType,
                        ReceiverId = inform.Key,
                        SenderId = protocolModel.TriggeredByUserId,
                        Timestamp = DateTimeOffset.Now,
                        ProtocolEntryId = protocolEntryId,
                    };

                    messageModel.Details = messageDetails;
                    await SendNotifications(messageModel, inform);
                }
            }

            if (protocolModel.Action == MessageActions.Create && protocolModel.RelatedType == MessageRelatedObjectTypes.CollectionJob)
            {
                IEnumerable<Int32> companieUserIds = await _storage.GetCompanyUserIdsByCollectionJobId(protocolModel.RelatedObjectId.Value);
                IDictionary<Int32, ProtocolNotificationTypes> companiesusersToInform = await _storage.GetUsersToInform(MessageRelatedObjectTypes.NotSpecified, MessageActions.CollectionJobBoundToCompany, companieUserIds);

                MessageActionCollectionJobDetailsModel collectionJobDetails = await _storage.GetMessageActionCollectionJobDetails(protocolModel.RelatedObjectId.Value);

                foreach (KeyValuePair<Int32, ProtocolNotificationTypes> inform in usersToInform)
                {
                    MessageCreateModel messageModel = new MessageCreateModel
                    {
                        Action = MessageActions.JobBound,
                        RelatedObjectType = MessageRelatedObjectTypes.JobGenerell,
                        ReceiverId = inform.Key,
                        SenderId = protocolModel.TriggeredByUserId,
                        Timestamp = DateTimeOffset.Now,
                        ProtocolEntryId = protocolEntryId,
                    };

                    messageModel.Details = collectionJobDetails;
                    await SendNotifications(messageModel, inform);
                }
            }
            else if (protocolModel.Action == MessageActions.JobAcknowledged)
            {
                await SendJobNotification(protocolModel, protocolEntryId, MessageActions.JobAcknowledged);
            }
            else if (protocolModel.Action == MessageActions.JobDiscared)
            {
                await SendJobNotification(protocolModel, protocolEntryId, MessageActions.JobDiscared);
            }
        }

        private async Task SendJobNotification(ProtocolEntyCreateModel model, int protocolEntryId, MessageActions jobAction)
        {
            Int32 finisherId = await _storage.GetJobFinisherUserIdByJobId(model.RelatedObjectId.Value);

            MessageCreateModel messageModel = new MessageCreateModel
            {
                RelatedObjectType = MessageRelatedObjectTypes.NotSpecified,
                ReceiverId = finisherId,
                Timestamp = DateTimeOffset.Now,
                ProtocolEntryId = protocolEntryId,
            };

            MessageActions mappedAction = MessageActions.NotSpecified;

            if (jobAction == MessageActions.JobAcknowledged)
            {
                mappedAction = MessageActions.InformJobFinisherAboutAcknlowedged;
                messageModel.Details = await _storage.GetMessageActionJobAcknowledgedDetails(model.RelatedObjectId.Value);

            }
            else if (jobAction == MessageActions.JobDiscared)
            {
                messageModel.Details = await _storage.GetMessageActionJobDiscardedDetails(model.RelatedObjectId.Value);
                mappedAction = MessageActions.InformJobFinisherAboutDiscard;
            }
            else
            {
                throw new InvalidOperationException("");
            }

            messageModel.Action = mappedAction;

            ProtocolNotificationTypes type = await _storage.GetProtocolNofitcation(MessageRelatedObjectTypes.NotSpecified, mappedAction, finisherId);
            await SendNotifications(messageModel, new KeyValuePair<int, ProtocolNotificationTypes>(finisherId, type));
        }

        private async Task SendNotifications(MessageCreateModel model, KeyValuePair<int, ProtocolNotificationTypes> inform)
        {
            if ((inform.Value & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message)
            {
                await _messageEngine.SendMessage(inform.Key, model);
            }
            if ((inform.Value & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email)
            {
                await _emailEngine.SendMessage(inform.Key, model);
            }
            if ((inform.Value & ProtocolNotificationTypes.SMS) == ProtocolNotificationTypes.SMS)
            {
                await _smsEngine.SendMessage(inform.Key, model);
            }
            if ((inform.Value & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push)
            {
                await _pushEngine.SendMessage(inform.Key, model);
            }
        }

        public async Task<IEnumerable<ProtocolEntryModel>> GetProtocolEntries(ProtocolFilterModel filterModel)
        {
            _logger.LogTrace("GetProtocolEntries");
            if (filterModel == null)
            {
                _logger.LogWarning("no protocol filter was given");
                throw new ProtocolServiceException(ProtocolServicExceptionReasons.NoFilterModel);
            }

            if (filterModel.Amount <= 0 || filterModel.Amount > _maxEntriesPerRequest)
            {
                _logger.LogInformation("invalid protocol filter amount. Amount betwenn {0} and {1}", 1, _maxEntriesPerRequest);
                throw new ProtocolServiceException(ProtocolServicExceptionReasons.InvalidFilterModel);
            }

            if (filterModel.Start < 0)
            {
                _logger.LogInformation("invalid protocol filter start. Start has to be greater or equal zero");
                throw new ProtocolServiceException(ProtocolServicExceptionReasons.InvalidFilterModel);
            }

            if (filterModel.StartTime.HasValue == true && filterModel.EndTime.HasValue == true && filterModel.EndTime.Value < filterModel.StartTime.Value)
            {
                _logger.LogInformation("start time have to be before end time");
                throw new ProtocolServiceException(ProtocolServicExceptionReasons.InvalidFilterModel);
            }

            if (filterModel.Types == null)
            {
                filterModel.Types = new List<MessageRelatedObjectTypes>();
            }
            if (filterModel.Actions == null)
            {
                filterModel.Actions = new List<MessageActions>();
            }

            IEnumerable<ProtocolEntryModel> result = await _storage.GetProtocolEntries(filterModel);
            return result;
        }

        public async Task<IEnumerable<ProtocolUserInformModel>> GetUserProtocolInformations(String userAuthId)
        {
            _logger.LogTrace("GetUserProtocolInformations. user auth id: {0}", userAuthId);

            await CheckIfUserExists(userAuthId);

            IEnumerable<ProtocolUserInformModel> result = await _storage.GetUserProtocolInformation(userAuthId);
            return result;
        }

        public async Task<IEnumerable<ProtocolUserInformModel>> UpdateUserProtocolInformations(String userAuthId, IEnumerable<ProtocolUserInformModel> models)
        {
            _logger.LogTrace("UpdateUserProtocolInformations. user auth id: {0}", userAuthId);
            await CheckIfUserExists(userAuthId);

            IEnumerable<ProtocolUserInformModel> exisitingInformations = await _storage.GetUserProtocolInformation(userAuthId);

            if (models != null && models.Count() > 0)
            {
                foreach (ProtocolUserInformModel item in models)
                {
                    _logger.LogTrace("check if protocol information exists with action: {0} and related type {1} for user with auth id {2}"
                        , item.Action, item.RelatedType, userAuthId);

                    ProtocolUserInformModel existingInformation = exisitingInformations.FirstOrDefault(x => x.Action == item.Action && x.RelatedType == item.RelatedType);
                    if (existingInformation == null)
                    {
                        _logger.LogInformation("notification with action: {0} and type {1} for user with auth id {2} not found. Unable to update"
                             , item.Action, item.RelatedType, userAuthId);

                        continue;
                    }

                    if (item.NotifcationType == existingInformation.NotifcationType)
                    {
                        _logger.LogTrace("nothing to change");
                        continue;
                    }

                    Boolean updateResult = await _storage.UpdateProtocolNotification(userAuthId, item);
                    if (updateResult == false)
                    {
                        _logger.LogInformation("unable to update notification with action: {0} and type {1} for user with auth id {2} not found. Unable to update"
      , item.Action, item.RelatedType, userAuthId);
                    }
                    else
                    {
                        existingInformation.NotifcationType = item.NotifcationType;
                    }
                }

                return exisitingInformations;
            }
            else
            {
                return exisitingInformations;
            }
        }


        private static void FillDefaultNotificationRelations()
        {
            _defaultRoleNotificationRelation = new Dictionary<BesaRoles, IEnumerable<ProtocolUserInformModel>>();

            List<ProtocolUserInformModel> adminNotifications = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.Company, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.Company, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.Company, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.AddEmployeeToCompany, RelatedType = MessageRelatedObjectTypes.Company, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.RemoveEmployeeToCompany, RelatedType = MessageRelatedObjectTypes.Company, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.User, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.User, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.User, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.RemoveMemberState, RelatedType = MessageRelatedObjectTypes.User, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.UserRequests, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.UserRequests, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.Article, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.Article, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.Article, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            List<ProtocolUserInformModel> projectManagerNotifications = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.ConstructionStage, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.ConstructionStage, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.ConstructionStage, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.CustomerRequest, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.CustomerRequest, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateAllCableTypesFromAdapter, RelatedType = MessageRelatedObjectTypes.FiberCableType, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateBuildigsFromAdapter, RelatedType = MessageRelatedObjectTypes.Buildings, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateBuildingCableInfoFromAdapter, RelatedType = MessageRelatedObjectTypes.Buildings, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateBuildingNameFromAdapter, RelatedType = MessageRelatedObjectTypes.Buildings, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateCablesFromAdapter, RelatedType = MessageRelatedObjectTypes.FiberCable, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateMainCableForBranchable, RelatedType = MessageRelatedObjectTypes.MainCables, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdatePatchConnections, RelatedType = MessageRelatedObjectTypes.FiberConnection, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdatePopInformation, RelatedType = MessageRelatedObjectTypes.PoP, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.UpdateSplices, RelatedType = MessageRelatedObjectTypes.Splice, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            List<ProtocolUserInformModel> injectorNotificaitons = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.InjectJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            projectManagerNotifications.AddRange(injectorNotificaitons);

            List<ProtocolUserInformModel> civilWorkerNotifcations = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.BranchOffJob, NotifcationType = ProtocolNotificationTypes.NoAction },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            projectManagerNotifications.AddRange(civilWorkerNotifcations);

            List<ProtocolUserInformModel> salesNotifcations = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.ContactInfo, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.CreateViaImport, RelatedType = MessageRelatedObjectTypes.ContactInfo, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.ContactInfo, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.ContactInfo, NotifcationType = ProtocolNotificationTypes.NoAction },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.BuildingConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.BuildingConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.BuildingConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.FinishedProcedures, RelatedType = MessageRelatedObjectTypes.BuildingConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.CustomerConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.CustomerConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.CustomerConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.FinishedProcedures, RelatedType = MessageRelatedObjectTypes.CustomerConnenctionProcedure, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.ContactAfterFinishedJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            List<ProtocolUserInformModel> inventoryNotificaitons = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.InventoryJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            projectManagerNotifications.AddRange(inventoryNotificaitons);

            List<ProtocolUserInformModel> networkOperationsNotificaitons = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.ConnectBuildingJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            projectManagerNotifications.AddRange(networkOperationsNotificaitons);

            List<ProtocolUserInformModel> spliceJobNotificaitons = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.PrepareBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.SpliceInBranchableJob, NotifcationType = ProtocolNotificationTypes.NoAction },

                new ProtocolUserInformModel{ Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.JobBound, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel{ Action = MessageActions.Delete, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobDiscared, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinished, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobFinishJobAdministrative, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.NoAction },
                new ProtocolUserInformModel{ Action = MessageActions.JobAcknowledged, RelatedType = MessageRelatedObjectTypes.SpliceMainCableInPoPJob, NotifcationType = ProtocolNotificationTypes.NoAction },
            };

            projectManagerNotifications.AddRange(spliceJobNotificaitons);

            Dictionary<BesaRoles, IEnumerable<ProtocolUserInformModel>> result = new Dictionary<BesaRoles, IEnumerable<ProtocolUserInformModel>>
            {
                { BesaRoles.Admin, adminNotifications  },
                { BesaRoles.ActiveNetwork, networkOperationsNotificaitons  },
                { BesaRoles.AirInjector, injectorNotificaitons  },
                { BesaRoles.CivilWorker, civilWorkerNotifcations  },
                { BesaRoles.Inventory, inventoryNotificaitons },
                { BesaRoles.ProjectManager, projectManagerNotifications },
                { BesaRoles.Sales, salesNotifcations },
                { BesaRoles.Splice, spliceJobNotificaitons },
            };

            _defaultRoleNotificationRelation = result;
        }

        private static IEnumerable<ProtocolUserInformModel> GetDefaultNotifications()
        {
            IEnumerable<ProtocolUserInformModel> protocolUserInformModel = new List<ProtocolUserInformModel>
            {
                new ProtocolUserInformModel{ Action = MessageActions.CollectionJobBoundToCompany, RelatedType = MessageRelatedObjectTypes.NotSpecified, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel { Action = MessageActions.InformJobFinisherAboutAcknlowedged, RelatedType = MessageRelatedObjectTypes.NotSpecified, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },
                new ProtocolUserInformModel { Action = MessageActions.InformJobFinisherAboutDiscard, RelatedType = MessageRelatedObjectTypes.NotSpecified, NotifcationType = ProtocolNotificationTypes.AllWithoutSMS },

            };

            return protocolUserInformModel;
        }

        private static List<BesaRoles> _possibleBesaRoles;

        private List<ProtocolUserInformModel> GetExpectedNotifications(BesaRoles roles)
        {
            List<ProtocolUserInformModel> result = new List<ProtocolUserInformModel>(GetDefaultNotifications());

            foreach (BesaRoles possibleRole in _possibleBesaRoles)
            {
                if ((roles & possibleRole) == possibleRole)
                {
                    result.AddRange(_defaultRoleNotificationRelation[possibleRole]);
                }
            }

            result = result.Distinct(new ProtocolUserInformModelComparer()).ToList();

            return result;
        }

        public async Task<IEnumerable<ProtocolUserInformModel>> RestoreProtocolNotifications(Int32 userId, BesaRoles role)
        {
            _logger.LogTrace("get existing notifications options for user with id {0}", userId);

            ICollection<ProtocolUserInformModel> exitingInformations = await _storage.GetUserProtocolInformation(userId);
            List<ProtocolUserInformModel> notificationsToDelete = new List<ProtocolUserInformModel>(exitingInformations);

            _logger.LogTrace("get default notifications and notifications based on role {0} for user with id {1}", role, userId);
            List<ProtocolUserInformModel> expectedNotifications = GetExpectedNotifications(role);

            foreach (ProtocolUserInformModel expectedNotification in expectedNotifications)
            {
                ProtocolUserInformModel existingNotification = exitingInformations.
                    FirstOrDefault(x => x.Action == expectedNotification.Action && x.RelatedType == expectedNotification.RelatedType);

                if (existingNotification == null)
                {
                    Boolean result = await _storage.CreateProtocolNotificaiton(userId, expectedNotification);
                    if (result == true)
                    {
                        _logger.LogTrace("add protocol notification with action: {0} type {1} and related user id {2}",
                       expectedNotification.Action, expectedNotification.RelatedType, userId);
                        exitingInformations.Add(expectedNotification);
                    }
                    else
                    {
                        _logger.LogInformation("unable to add protocol notification with action: {0} type {1} and related user id {2}",
                        expectedNotification.Action, expectedNotification.RelatedType, userId);
                    }
                }
                else
                {
                    _logger.LogTrace("no notification needs to changed");
                    expectedNotifications.Remove(existingNotification);
                }
            }

            foreach (ProtocolUserInformModel notificationToDelete in notificationsToDelete)
            {
                Boolean result = await _storage.DeleteProtocolNotification(userId, notificationToDelete);
                if (result == true)
                {
                    exitingInformations.Remove(notificationToDelete);
                }
                else
                {
                    _logger.LogInformation("unable to delete protocol notification with action: {0} type {1} and related user id {2}",
                       notificationToDelete.Action, notificationToDelete.RelatedType, userId);
                }
            }

            return exitingInformations;

        }

        public async Task<IDictionary<Int32, IEnumerable<ProtocolUserInformModel>>> RestoreProtocolNotifications()
        {
            _logger.LogTrace("RestoreProtocolInformations");

            _logger.LogTrace("get all users with roles");

            IDictionary<Int32, BesaRoles> userRoleRelations = await _storage.GetUserRoleRelations();

            Dictionary<Int32, IEnumerable<ProtocolUserInformModel>> restoreResult = new Dictionary<int, IEnumerable<ProtocolUserInformModel>>();

            foreach (KeyValuePair<Int32, BesaRoles> userRoleRelation in userRoleRelations)
            {
                IEnumerable<ProtocolUserInformModel> restoredItems = await RestoreProtocolNotifications(userRoleRelation.Key, userRoleRelation.Value);
                restoreResult.Add(userRoleRelation.Key, restoredItems);
            }

            return restoreResult;
        }

        #endregion
    }
}
