﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class SeedService : ISeedService
    {
        #region Fields

        private readonly ICompanyService companyService;
        private readonly ICustomerService customerService;
        private readonly ILogger<SeedService> logger;
        private readonly IBesaDataStorage _storage;

        #endregion

        #region Constructor

        public SeedService(ICompanyService companyService, ICustomerService customerService, ILogger<SeedService> logger, IBesaDataStorage storage)
        {
            this.companyService = companyService ?? throw new ArgumentNullException(nameof(companyService));
            this.customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        #endregion

        #region Methods

        public async Task SeedCompanies(Int32 amount)
        {
            for (int i = 0; i < amount; i++)
            {
                CompanyCreateModel model = new CompanyCreateModel
                {
                    Name = $"Firma {i}",
                    Address = new AddressModel
                    {
                        Street = "Musterstr.",
                        City = "Musterstadt",
                        PostalCode = "01452",
                        StreetNumber = $"{i + 1}a",
                    },
                    Phone = "01545234",
                };

                await companyService.CreateCompany(model, null);
            }
        }

        public async Task SeedContacts(Int32 amount)
        {
            Random random = new Random();

            for (int i = 0; i < amount; i++)
            {
                PersonInfo model = new PersonInfo
                {
                    Address = new AddressModel
                    {
                        City = $"Musterstadt {random.Next()}",
                        Street = $"Musterstr {random.Next()}",
                        StreetNumber = random.Next(10, 20).ToString(),
                        PostalCode = random.Next(10000, 99999).ToString(),
                    },
                    CompanyName = random.NextDouble() > 0.5 ? $"Musterfirma {random.Next()}" : "",
                    EmailAddress = $"test-{random.Next()}@test.de",
                    Lastname = $"Nachname {random.Next()}",
                    Surname = $"Vorname {random.Next()}",
                    Phone = $"0548754-{random.Next()}",
                    Type = random.NextDouble() > 0.5 ? Contracts.Models.PersonTypes.Male : Contracts.Models.PersonTypes.Female,
                };

                await customerService.CreateContact(model, String.Empty);
            }
        }

        public async Task SeedContactAfterFinsiehdJobs(Int32 amount, String userAuthId)
        {
            Random random = new Random();

            IDictionary<String, Int32> buildingIds = await _storage.GetBuildingIdsAndProjectAdapterIds();

            IEnumerable<UserOverviewModel> users = await _storage.GetAllUsers();

            IEnumerable<PersonInfo> infos = await _storage.GetContactsSortByLastname(0, 100);
            if (infos.Count() == 0)
            {
                await SeedContacts(100);
                infos = await _storage.GetContactsSortByLastname(0, 100);
            }

            for (int i = 0; i < amount; i++)
            {
                BuildingConnectionProcedureCreateModel createModel = new BuildingConnectionProcedureCreateModel
                {
                    BuildingId = buildingIds.ElementAt(random.Next(0, buildingIds.Count)).Value,
                    DeclarationOfAggrementInStock = true,
                    ExpectedEnd = ProcedureStates.ConnectBuildingJobAcknlowedged,
                    FirstElement = new ProcedureTimelineCreateModel
                    {
                        Comment = "Durch seeden von Daten enstanden",
                        State = ProcedureStates.RequestAcknkowledged,
                        Timestamp = DateTimeOffset.Now,
                    },
                    InformSalesAfterFinish = true,
                    Name = $"Testprocedure Nr {random.Next()}",
                    Start = ProcedureStates.RequestCreated,
                    OwnerContactId = infos.ElementAt(random.Next(0, infos.Count())).Id
                };

                Int32 procedureId = await _storage.CreateBuildingConnectionProcedure(createModel);

                CreateContactAfterFinishedJobModel jobModel = new CreateContactAfterFinishedJobModel
                {
                    BuildingId = createModel.BuildingId,
                    RelatedProcedureId = procedureId,
                };

                Int32 jobid = await _storage.CreateContactAfterFinishedJob(jobModel);

                if(random.NextDouble() > 0.7)
                {
                    await _storage.FinishContactAfterFinishedJob(new ContactAfterFinishedFinishModel
                    {
                        Timestamp =  DateTimeOffset.Now,
                        JobId = jobid,
                        UserAuthId = userAuthId,
                        ProblemHappend = new ExplainedBooleanModel
                        {
                            Value = true,
                            Description = "Durch seeden entstanden",
                        },
                    });
                }

            }
        }

        #endregion
    }
}
