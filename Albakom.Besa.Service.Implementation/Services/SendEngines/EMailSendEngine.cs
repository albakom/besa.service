﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class EMailSendEngine : IEmailSendEngine
    {
        #region Properties and Fields

        private readonly ILogger<EMailSendEngine> _logger;

        #endregion

        #region Constructor

        public EMailSendEngine(ILogger<EMailSendEngine> logger)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        public Task SendMessage(int userId, MessageCreateModel model)
        {
            return Task.FromResult(new Object());
        }
    }
}
