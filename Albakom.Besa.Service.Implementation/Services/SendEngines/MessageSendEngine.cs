﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class MessageSendEngine : IMessageSendEngine
    {

        #region Fields

        private readonly IBesaDataStorage _storage;
        private readonly IMessageNotifier _notifier;
        private readonly ILogger<MessageSendEngine> _logger;

        #endregion

        #region Constructor

        public MessageSendEngine(IBesaDataStorage storage, IMessageNotifier notifier, ILogger<MessageSendEngine> logger)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._notifier = notifier ?? throw new ArgumentNullException(nameof(notifier));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        #region Methods

        public async Task SendMessage(int userId, MessageCreateModel model)
        {
            MessageRawCreateModel createModel = new MessageRawCreateModel
            {
                Action = model.Action,
                RelatedObjectType = model.RelatedObjectType,
                ProtocolEntryId = model.ProtocolEntryId,
                ReceiverId = userId,
                SenderId = model.SenderId,
                Timestamp = model.Timestamp
            };

            if(model.Details != null)
            {
                createModel.Details = Newtonsoft.Json.JsonConvert.SerializeObject(model.Details);
                createModel.Title = model.Details.Name;
            }

            Int32 messageId = await _storage.CreateMessage(createModel);

            String authId = await _storage.GetAuthServiceIdByUserId(userId);
            MessageModel messageModel = new MessageModel
            {
                Action = model.Action,
                ActionDetails = model.Details,
                Id = messageId,
                MarkedAsRead = false,
                RelatedObjectType = model.RelatedObjectType,
                TimeStamp = createModel.Timestamp,
            };

            if(model.SenderId.HasValue == true)
            {
                messageModel.SourceUser = await _storage.GetUserOverviewById(model.SenderId.Value);
            }

            await _notifier.NewMessageArrivied(authId, messageModel);
        }

        #endregion


    }
}
