﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class PushSendEngine : IPushSendEngine
    {
        #region Properties and Fields

        private readonly ILogger<PushSendEngine> _logger;

        #endregion

        #region Constructor

        public PushSendEngine(ILogger<PushSendEngine> logger)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        public Task SendMessage(int userId, MessageCreateModel model)
        {
            return Task.FromResult(new Object());
        }
    }
}
