﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class SMSSendEngine : ISMSSendEngine
    {
        #region Properties and Fields

        private readonly ILogger<SMSSendEngine> _logger;

        #endregion

        #region Constructor

        public SMSSendEngine(ILogger<SMSSendEngine> logger)
        {
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion

        public Task SendMessage(int userId, MessageCreateModel model)
        {
            return Task.FromResult(new Object());
        }
    }
}
