﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class SimpleTokenClient : ITokenClient
    {
        #region Fields

        private DiscoveryResponse discoveryResponse;
        private String _accessToken;
        private DateTimeOffset _experationTimeForToken;

        private TokenClientConfig _tokenConfig;

        #endregion

        #region Constructor

        public SimpleTokenClient(TokenClientConfig tokenConfig)
        {
            _tokenConfig = tokenConfig ?? throw new ArgumentNullException(nameof(tokenConfig));
        }

        #endregion

        #region Methods

        public Boolean IsReady()
        {
            return discoveryResponse != null;
        }

        public String GetToken()
        {
            return _accessToken;
        }

        public Boolean HasToken()
        {
            if (String.IsNullOrEmpty(_accessToken) == true)
            {
                return false;
            }

            return DateTimeOffset.Now < _experationTimeForToken;
        }

        public async Task<Boolean> Prepare()
        {
            DiscoveryClient client = new DiscoveryClient(_tokenConfig.AuthUrl);

#if DEBUG
            client.Policy.RequireHttps = false;
#endif

            DiscoveryResponse response = await client.GetAsync();
            if (response.IsError == true)
            {
                return false;
            }

            discoveryResponse = response;
            return true;
        }

        public async Task<Boolean> RequestToken()
        {
            TokenClient client = new TokenClient(discoveryResponse.TokenEndpoint, _tokenConfig.ClientId, _tokenConfig.ClientSecret);
            TokenResponse response = await client.RequestClientCredentialsAsync(_tokenConfig.Scope);
            if (response.IsError == true)
            {
                return false;
            }

            _experationTimeForToken = DateTimeOffset.Now.AddSeconds(response.ExpiresIn - 60);
            _accessToken = response.AccessToken;
            return true;
        }

        #endregion
    }
}
