﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class UpdateTaskService : IUpdateTaskService
    {
        #region Fields

        private const Int32 _maxDurationPerUpdateTaskElement = 5;
        private const Int32 _maxKeepAliveTimeForTask = 8;
        private readonly IBesaDataStorage _storage;
        private readonly ILogger<UpdateTaskService> _logger;
        private readonly IUpdateTaskNotifier _notifier;

        #endregion

        #region Constructor

        public UpdateTaskService(IBesaDataStorage storage, ILogger<UpdateTaskService> logger, IUpdateTaskNotifier notifier)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._notifier = notifier ?? throw new ArgumentNullException(nameof(notifier));
        }

        #endregion

        #region Methods

        private async Task<Boolean> CheckIfOpenUpdateTasksExists(UpdateTasks taskType)
        {
            _logger.LogTrace("check if open task with type {0} exists", taskType);
            Boolean result = await _storage.CheckIfOpenUpdateTasksExists(taskType);
            return result;
        }

        public async Task<IEnumerable<UpdateTaskElementModel>> GetOpenElements(UpdateTasks taskType, String userAuthId)
        {
            _logger.LogTrace("GetOpenElements. taskType: {0}; userAuthId: {1}", taskType, userAuthId);

            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if (await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.UserNotFound);
            }

            _logger.LogTrace("check if a task already exists");
            IEnumerable<UpdateTaskElementModel> openElements = null;

            Boolean exists = await CheckIfOpenUpdateTasksExists(taskType);
            if (await _storage.CheckIfOpenUpdateTasksExists(taskType) == false)
            {
                _logger.LogTrace("get object ids from database related to task type {0}", taskType);
                IDictionary<String, Int32> storedBranchables = null;
                switch (taskType)
                {
                    case UpdateTasks.CableTypes:
                    case UpdateTasks.PoPs:
                        storedBranchables = new Dictionary<String, Int32>();
                        break;
                    case UpdateTasks.MainCableForPops:
                        storedBranchables = await _storage.GetPopsWithProjectAdapterId();
                        break;
                    case UpdateTasks.PatchConnections:
                        storedBranchables = await _storage.GetCablesWithFlatIDs();
                        break;
                    case UpdateTasks.MainCableForBranchable:
                    case UpdateTasks.UpdateBuildings:
                    case UpdateTasks.Cables:
                    case UpdateTasks.Splices:

                        storedBranchables = await _storage.GetBranchablesWithProjectAdapterId();
                        break;
                    default:
                        break;
                }

                if (storedBranchables == null)
                {
                    _logger.LogError("uanble to get data objects from type {0}", taskType);
                    throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
                }

                UpdateTaskCreateModel createModel = new UpdateTaskCreateModel
                {
                    Task = taskType,
                    UserAuthId = userAuthId,
                    Elements = storedBranchables,
                    Timestamp = DateTimeOffset.Now,
                };

                Int32 id = await _storage.CreateUpdateTask(createModel);
                openElements = await _storage.GetOpenUpdateTaskElement(taskType);
            }
            else
            {
                _logger.LogTrace("update task found in database");
                openElements = await _storage.GetOpenUpdateTaskElement(taskType);
            }

            return openElements;
        }

        private async Task CheckIfTaskElementExists(Int32 elementId)
        {
            _logger.LogTrace("check if update element with id {0} exists", elementId);
            if (await _storage.CheckIfUpdateTaskElementExists(elementId) == false)
            {
                _logger.LogError("uanble to get update task element with id {0}", elementId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.ElementNotFound);
            }
        }

        private UpdateTaskElementOverviewModel GetUpdateTaskElementOverviewModel(UpdateTaskElementRawOverviewModel rawModel)
        {
            UpdateTaskElementOverviewModel model = new UpdateTaskElementOverviewModel
            {
                Ended = rawModel.Ended,
                Id = rawModel.Id,
                RelatedObject = rawModel.RelatedObject,
                Started = rawModel.Started,
                State = rawModel.State,
                Result = String.IsNullOrEmpty(rawModel.RawResult) == true ? new UpdateTaskResultModel() :
                    JsonConvert.DeserializeObject<UpdateTaskResultModel>(rawModel.RawResult)
            };

            return model;
        }

        private async Task NotifyUpdateTaskElementChanged(Int32 elementId)
        {
            UpdateTaskElementRawOverviewModel rawResult = await _storage.GetUpdateTaskElementOverviewById(elementId);
            UpdateTaskElementOverviewModel result = GetUpdateTaskElementOverviewModel(rawResult);

            await _notifier.TaskElementChanged(result);
        }

        public async Task<Boolean> StartUpdateTaskElement(Int32 elementId)
        {
            _logger.LogTrace("StartUpdateTaskElement. elementId: {0}", elementId);
            await CheckIfTaskElementExists(elementId);

            _logger.LogTrace("set state to processing for update task element with id {0}", elementId);
            Boolean result = await _storage.SetUpdateTaskElementAsProcessing(elementId);

            if (result == true)
            {
                await NotifyUpdateTaskElementChanged(elementId);
            }

            return result;
        }

        public async Task<Boolean> FinishUpdateElement(UpdateTaskElementFinishModel model)
        {
            _logger.LogTrace("EndUpdateElement");
            if (model == null)
            {
                _logger.LogError("element for set end to update task element is null");
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.ModelIsNull);
            }

            if (model.State == UpdateTaskElementStates.NotStarted || model.State == UpdateTaskElementStates.InProgress)
            {
                _logger.LogError("the state for set end to update task element  shouldn't be neither 'InProgress' or 'NotStarted'");
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            if (model.State == UpdateTaskElementStates.Waring && String.IsNullOrEmpty(model.Result.Warning) == true)
            {
                _logger.LogError("if state says, that a warining is happend, the result should have a warning info");
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            if (model.State == UpdateTaskElementStates.Error && String.IsNullOrEmpty(model.Result.Error) == true)
            {
                _logger.LogError("if state says, that a error is happend, the result should have a error info");
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            //if (model.State == UpdateTaskElementStates.Success &&
            //    model.Result.AddedIds.Count + model.Result.UpdatedIds.Count + model.Result.DeletedIds.Count == 0)
            //{
            //    _logger.LogError("if says, that a task was successfull, the result should have corresponding ids");
            //    throw new UpdateTaskException(UpdateTaskExceptionReasons.InvalidOperation);
            //}

            await CheckIfTaskElementExists(model.ElementId);

            if (await _storage.CheckIfTaskElementIsAlreadyFinished(model.ElementId) == true)
            {
                _logger.LogError("unable to update a update task element, that alredy finished. Element id is {0}", model.ElementId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("serialze result object");
            String rawResultText = JsonConvert.SerializeObject(model.Result);

            UpdateTaskElementUpdateModel updateModel = new UpdateTaskElementUpdateModel
            {
                ElementId = model.ElementId,
                State = model.State,
                Timestamp = DateTimeOffset.Now,
                Result = rawResultText,
            };

            _logger.LogTrace("update database element");
            Boolean result = await _storage.EditUpdateTaskElement(updateModel);

            if (result == true)
            {
                await NotifyUpdateTaskElementChanged(model.ElementId);
            }

            return result;
        }

        public async Task<Boolean> FinishTask(UpdateTasks taskType)
        {
            _logger.LogTrace("FinishTask. taskType: {0}", taskType);

            Boolean exists = await CheckIfOpenUpdateTasksExists(taskType);
            if (exists == false)
            {
                _logger.LogError("no open task with type {0} exists. unable to finish", taskType);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("get task id from open task with type {0}", taskType);
            Int32 taskId = await _storage.GetUpdateTaskIdByTaskTye(taskType);
            _logger.LogTrace("open task with type {0} resolved to id {1}", taskType, taskId);

            _logger.LogTrace("check if task with type {0} has open elements", taskType);
            if (await _storage.CheckIfTaskHasOpenElements(taskType) == true)
            {
                _logger.LogError("task with type {0} has open sub task. unable to finish", taskType);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            _logger.LogTrace("set task with type {0} as finished", taskType);
            UpdateTaskFinishModel finishModel = new UpdateTaskFinishModel
            {
                Cancel = false,
                Timestamp = DateTimeOffset.Now,
                TaskId = taskId,
            };

            Boolean result = await _storage.EditUpdateTask(finishModel);
            if (result == true)
            {
                await _notifier.UpdateTaskFinished(new UpdateTasFinishedStateChangedModel { Id = taskId, FinishedAt = finishModel.Timestamp });
            }
            return result;
        }

        public async Task<Boolean> CheckIfTaskIsCanceledByElementId(Int32 elementId)
        {
            _logger.LogTrace("CheckIfTaskIsCanceledByElementId. eleelementId: {0}", elementId);
            await CheckIfTaskElementExists(elementId);

            Boolean result = await _storage.CheckIfTaskIsCanceledByElementId(elementId);
            return result;
        }

        public async Task<Boolean> RestoreLockedUpdateTaskElements()
        {
            _logger.LogTrace("RestoreHangingUpdateTaskElements");
            DateTimeOffset timestamp = DateTimeOffset.Now.AddMinutes(-_maxDurationPerUpdateTaskElement);

            _logger.LogTrace("get updated task, wich are in progress but older than {0}", timestamp);
            IEnumerable<Int32> elementIds = await _storage.GetUpdateTaskElementsInProgressSince(timestamp);

            _logger.LogTrace("{0} elements found", elementIds.Count());
            Boolean result = false;
            foreach (Int32 elementId in elementIds)
            {
                Boolean partialResult = await _storage.ResetUpdateTaskElement(elementId);
                _logger.LogTrace("update task element with id {0} was reseted", elementId);
                result |= partialResult;

                if (partialResult == true)
                {
                    await NotifyUpdateTaskElementChanged(elementId);
                }
            }

            return result;
        }

        public async Task<Boolean> CheckIfUpdateTaskIsLocked(UpdateTasks taskType)
        {
            _logger.LogTrace("CheckIfJobIsRunning. Tasktype: {0} ", taskType);

            _logger.LogTrace("check if an open task with type {0} exists", taskType);
            if (await _storage.CheckIfOpenUpdateTasksExists(taskType) == false)
            {
                _logger.LogTrace("no open task with type {0} exist. Task is unlocked");
                return false;
            }
            else
            {
                Boolean result = await _storage.CheckIfTaskIsLocked(taskType);
                _logger.LogTrace("a task with type is locked: {0}", taskType);

                return result;
            }
        }

        public async Task<Boolean> LockUpdateTask(UpdateTasks taskType)
        {
            _logger.LogTrace("LockUpdateTask. Tasktype: {0} ", taskType);

            _logger.LogTrace("check if an open task with type {0} exists", taskType);
            if (await _storage.CheckIfOpenUpdateTasksExists(taskType) == false)
            {
                _logger.LogTrace("no open task with type {0} exist. Task can't be locked");
                return false;
            }

            Int32 taskId = await _storage.GetUpdateTaskIdByTaskTye(taskType);

            Boolean result = await _storage.SetLockStateOfTask(taskId, true);
            return result;
        }

        public async Task<IEnumerable<Int32>> UnlockedTaskWithDeadlock()
        {
            _logger.LogTrace("UnlockedTaskWithDeadlock");

            UpdateTasks[] possibleTaskWithDeadlock = new UpdateTasks[] {
                UpdateTasks.Cables,
                UpdateTasks.MainCableForBranchable,
                UpdateTasks.MainCableForPops,
                UpdateTasks.PatchConnections,
                UpdateTasks.Splices
            };

            List<Int32> restoredTasks = new List<int>();

            foreach (UpdateTasks taskType in possibleTaskWithDeadlock)
            {
                DateTimeOffset timestamp = DateTimeOffset.Now.AddMinutes(-_maxKeepAliveTimeForTask);

                _logger.LogTrace("check if taskType: {0} has heartbeat older than {1}", taskType, timestamp);
                Boolean taskHasDeadlock = await _storage.CheckIfTaskHeartBeatIsOlderThen(timestamp, taskType);

                if (taskHasDeadlock == false)
                {
                    _logger.LogTrace("tasktype {0} has no deadlock. nothing to do", taskType);
                    continue;
                }

                _logger.LogWarning("tasktype {0} has a deadlock", taskType);

                Int32 taskId = await _storage.GetUpdateTaskIdByTaskTye(taskType);
                _logger.LogTrace("tasktype {0} resolved to id {1}", taskType, taskId);

                Boolean result = await _storage.SetLockStateOfTask(taskId, false);

                if(result == true)
                {
                    await _notifier.TaskLockDetected(taskId);
                }

                _logger.LogTrace("task with id {0} is now unlocked", taskId);
                restoredTasks.Add(taskId);
            }

            return restoredTasks;
        }

        public async Task<DateTimeOffset> UpdateHeartbeatByTaskElementId(Int32 taskElementId)
        {
            _logger.LogTrace("UpdateHeartbeatByTaskElementId. taskElementId: {0}", taskElementId);

            await CheckIfTaskElementExists(taskElementId);

            _logger.LogTrace("get task id by element with id {0}", taskElementId);
            Int32 taskId = await _storage.GetUpdateTaskIdByElementId(taskElementId);
            _logger.LogTrace("element with id {0} resolved to task with id {1}", taskElementId, taskId);

            DateTimeOffset hearbeat = DateTimeOffset.Now;
            _logger.LogTrace("set heartbeat {0} to task with id {1}", hearbeat, taskId);
            Boolean result = await _storage.UpdateHeartbeatByTaskId(taskId, hearbeat);

           if(result == true)
            {
                await _notifier.HeartbeatChanged(new UpdateTaskHeartbeatChangedModel { TaskId = taskId, Value = hearbeat });
            }

            return hearbeat;
        }

        public async Task<IEnumerable<UpdateTaskOverviewModel>> GetUpdateTasks(UpdateTasks? taskType)
        {
            _logger.LogTrace("GetUpdateTasks. tasktype: {0}", taskType.HasValue == true ? taskType.Value.ToString() : "not specifired");
            IEnumerable<UpdateTaskOverviewModel> result = await _storage.GetUpdateTasks(taskType);
            return result;
        }

        private async Task CheckIfTaskWithIdExsits(Int32 taskId)
        {
            _logger.LogTrace("check if task with id {0} exists", taskId);
            if (await _storage.CheckIfUpdateTasksExists(taskId) == false)
            {
                _logger.LogInformation("update task with id {0}", taskId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.TaskNotFound);
            }
        }

        public async Task<UpdateTaskDetailModel> GetUpdateTaskDetails(Int32 taskId)
        {
            _logger.LogTrace("GetUpdateTaskDetails. taskid: {0}", taskId);

            await CheckIfTaskWithIdExsits(taskId);

            UpdateTaskRawDetailModel rawResult = await _storage.GetUpdateTaskDetails(taskId);

            UpdateTaskDetailModel result = new UpdateTaskDetailModel
            {
                Overview = rawResult.Overview,
                Elements = rawResult.Elements.Select(x => GetUpdateTaskElementOverviewModel(x))
            };

            return result;
        }

        public async Task<Boolean> CancelUpdateTask(Int32 taskId)
        {
            _logger.LogTrace("CancelUpdateTask. taskId: {0}", taskId);

            await CheckIfTaskWithIdExsits(taskId);

            _logger.LogTrace("check if update task with id  {0} is not finished", taskId);
            if (await _storage.CheckIfUpdateTaskIsFinished(taskId) == true)
            {
                _logger.LogInformation("update task with id {0} is finished so it can't be canceled", taskId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidOperation);
            }

            Boolean result = await _storage.CancelUpdateTask(taskId);
            if(result == true)
            {
                await _notifier.UpdateTaskCanceled(taskId);
            }

            return result;
        }

        public async Task<IEnumerable<UpdateTaskObjectOverviewModel>> GetPossibleObjectForTaskType(UpdateTasks taskType)
        {
            IEnumerable<UpdateTaskObjectOverviewModel> result = null;
            switch (taskType)
            {
                case UpdateTasks.CableTypes:
                case UpdateTasks.UpdateBuildings:
                case UpdateTasks.PoPs:
                    result = new List<UpdateTaskObjectOverviewModel>();
                    break;
                case UpdateTasks.Cables:
                case UpdateTasks.MainCableForBranchable:
                case UpdateTasks.Splices:
                    result = await _storage.GetAllBranchablesAsUpdateTaskObjectOverviewModel();
                    break;
                case UpdateTasks.MainCableForPops:
                    result = await _storage.GetAllPoPsAsUpdateTaskObjectOverviewModel();
                    break;
                case UpdateTasks.PatchConnections:
                    result = await _storage.GetAllFlatCablesAsUpdateTaskObjectOverviewModel();
                    break;
                default:
                    throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.TaskTypeNotFound);
            }

            return result;
        }

        public async Task<Int32> CreateUpdateTask(StartUpdateTaskModel model)
        {
            _logger.LogTrace("StartUpdateTask");

            _logger.LogTrace("check if model is set");
            if (model == null)
            {
                _logger.LogInformation("no model was giben for create update task");
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.ModelIsNull);
            }

            _logger.LogTrace("check if user with auth service id {0} exists", model.UserAuthId);
            if (await _storage.CheckIfUserExists(model.UserAuthId) == false)
            {
                _logger.LogInformation("user with auth service id {0} not found", model.UserAuthId);
                throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.NotFound);
            }

            _logger.LogInformation("get object ids to check and if a check is nessesary");
            Boolean checkObjectIds = false;
            MessageRelatedObjectTypes objectTypeToCheck = MessageRelatedObjectTypes.Branchable;

            switch (model.TaskType)
            {
                case UpdateTasks.UpdateBuildings:
                    checkObjectIds = false;
                    break;
                case UpdateTasks.CableTypes:
                    checkObjectIds = false;
                    break;
                case UpdateTasks.Cables:
                    checkObjectIds = true;
                    objectTypeToCheck = MessageRelatedObjectTypes.Branchable;
                    break;
                case UpdateTasks.PoPs:
                    checkObjectIds = false;
                    break;
                case UpdateTasks.MainCableForPops:
                    checkObjectIds = true;
                    objectTypeToCheck = MessageRelatedObjectTypes.PoP;
                    break;
                case UpdateTasks.PatchConnections:
                    checkObjectIds = true;
                    objectTypeToCheck = MessageRelatedObjectTypes.FiberCable;
                    break;
                case UpdateTasks.Splices:
                    checkObjectIds = true;
                    objectTypeToCheck = MessageRelatedObjectTypes.Branchable;
                    break;
                case UpdateTasks.MainCableForBranchable:
                    checkObjectIds = true;
                    objectTypeToCheck = MessageRelatedObjectTypes.Branchable;
                    break;
                default:
                    throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.TaskTypeNotFound);
            }

            IDictionary<String, Int32> objectAdapterIds = null;

            if (checkObjectIds == true)
            {
                _logger.LogTrace("check if object ids was given");
                if (model.ObjectIds == null || model.ObjectIds.Count() == 0)
                {
                    _logger.LogInformation("no object ids was given for create update task with type {0}", model.TaskType);
                    throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.InvalidModel);
                }

                if (objectTypeToCheck == MessageRelatedObjectTypes.Branchable)
                {
                    _logger.LogTrace("check if branchables exists");
                    if (await _storage.CheckIfBranchablesExists(model.ObjectIds) == false)
                    {
                        _logger.LogInformation("not all branchables found for create update task with type {0}", model.TaskType);
                        throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.NotFound);
                    }

                    objectAdapterIds = await _storage.GetBranchablesWithProjectAdapterId(model.ObjectIds);
                }
                else if (objectTypeToCheck == MessageRelatedObjectTypes.PoP)
                {
                    _logger.LogTrace("check if pops exists");
                    if (await _storage.CheckIfPopsExists(model.ObjectIds) == false)
                    {
                        _logger.LogInformation("not all pops found for create update task with type {0}", model.TaskType);
                        throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.NotFound);
                    }

                    objectAdapterIds = await _storage.GetPopsWithProjectAdapterId(model.ObjectIds);

                }
                else if (objectTypeToCheck == MessageRelatedObjectTypes.FiberCable)
                {
                    _logger.LogTrace("check if cables exists");
                    if (await _storage.CheckIfCablesExists(model.ObjectIds) == false)
                    {
                        _logger.LogInformation("not all cables found for create update task with type {0}", model.TaskType);
                        throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.NotFound);
                    }

                    objectAdapterIds = await _storage.GetCableProjectAdapterIds(model.ObjectIds);
                }
                else
                {
                    throw new UpdateTaskServiceException(UpdateTaskServiceExceptionReasons.TaskTypeNotFound);
                }
            }
            else
            {
                objectAdapterIds = new Dictionary<String, Int32>();
            }

            UpdateTaskCreateModel createModel = new UpdateTaskCreateModel
            {
                Task = model.TaskType,
                UserAuthId = model.UserAuthId,
                Timestamp = DateTimeOffset.Now,
                Elements = objectAdapterIds,
            };

            Int32 id = await _storage.CreateUpdateTask(createModel);

            UpdateTaskOverviewModel result = await _storage.GetUpdateTaskOverviewById(id);
            await _notifier.UpdateTaskAdded(result);

            return id;
        }
       
        public async Task<Int32> GetTaskIdByElementId(Int32 elementId)
        {
            _logger.LogTrace("GetTaskIdByElementId. elementId: {0}",elementId);

            await CheckIfTaskElementExists(elementId);

            Int32 id = await _storage.GetUpdateTaskIdByElementId(elementId);
            return id;
        }

        #endregion
    }
}
