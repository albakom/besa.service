﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Service.Implementation.Services
{
    public class UserService : IUserService
    {
        #region Fields

        private IBesaDataStorage _storage;
        private readonly IAccessRequestNotifier _notifier;
        private ILogger<UserService> _logger;
        private readonly IProtocolService _protocolService;

        #endregion

        #region Constructor

        public UserService(
            IBesaDataStorage storage,
            IAccessRequestNotifier notifier,
            ILogger<UserService> logger,
            IProtocolService protocolService
            )
        {
            _storage = storage;
            this._notifier = notifier ?? throw new ArgumentNullException(nameof(notifier));
            _logger = logger;
            this._protocolService = protocolService ?? throw new ArgumentNullException(nameof(protocolService));
        }

        #endregion

        #region Methods

        public async Task<Boolean> CheckIfUserHasRole(String authID, BesaRoles role)
        {
            _logger.LogDebug("CheckIfUserHasRole. authID  {0},  role ", authID, role);

            if (role == BesaRoles.Unknown)
            {
                _logger.LogInformation("Unkown Role can not be found");
                return false;
            }

            if (await _storage.CheckIfUserExists(authID) == false)
            {
                _logger.LogInformation("Unable to find user with auth id {0}", authID);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            Boolean result = await _storage.CheckIfUserHasRole(authID, role);
            _logger.LogDebug("CheckIfUserHasRole result: {0}", result);
            return result;
        }

        public async Task<Boolean> CheckIfUserExists(String authServiceId)
        {
            _logger.LogDebug("CheckIfUserExists. authID  {0},  role ", authServiceId);

            Boolean result = await _storage.CheckIfUserExists(authServiceId);
            _logger.LogDebug("CheckIfUserExists result: {0}", result);

            return result;
        }

        private async Task ValidateUserData(IUserData model, UserServiceExceptionReasons nullException, UserServiceExceptionReasons invalidDataReason)
        {
            _logger.LogDebug("ValidateUserData");

            if (model == null)
            {
                _logger.LogInformation("model is null");
                throw new UserServiceException(nullException);
            }

            if (
              String.IsNullOrEmpty(model.EMailAddress) == true || model.EMailAddress.Length > _storage.Constraints.MaxUserMailChars ||
              String.IsNullOrEmpty(model.Phone) == true || model.Phone.Length > _storage.Constraints.MaxUserPhoneChars ||
              String.IsNullOrEmpty(model.Surname) == true || model.Surname.Length > _storage.Constraints.MaxUserSurnameChars ||
              String.IsNullOrEmpty(model.Lastname) == true || model.Lastname.Length > _storage.Constraints.MaxUserLastnameChars
              )
            {
                _logger.LogInformation("model is invalid");
                throw new UserServiceException(invalidDataReason);
            }

            if (model.CompanyId.HasValue == true)
            {
                _logger.LogDebug("check for compony with id {0}", model.CompanyId.Value);
                if (await _storage.CheckIfCompanyExists(model.CompanyId.Value) == false)
                {
                    _logger.LogInformation("company with id {0} not found", model.CompanyId.Value);
                    throw new UserServiceException(invalidDataReason);
                }
            }
        }

        private async Task ValidateAccessRequest(UserAccessRequestCreateModel model)
        {
            await ValidateUserData(model, UserServiceExceptionReasons.UserAcccessRequestIsNull, UserServiceExceptionReasons.InvalidAccessRequestModel);

            if (
               String.IsNullOrEmpty(model.AuthServiceId) == true || model.AuthServiceId.Length > _storage.Constraints.MaxAuthServiceIdChars
               )
            {
                _logger.LogInformation("UserAccessRequestCreateModel is invalid");
                throw new UserServiceException(UserServiceExceptionReasons.InvalidAccessRequestModel);
            }
        }

        public async Task CreateAccessRequest(UserAccessRequestCreateModel model)
        {
            _logger.LogDebug("CreateAccessRequest");
            await ValidateAccessRequest(model);

            if (await _storage.CheckIfUserAccessRequestExists(model.AuthServiceId) == true)
            {
                _logger.LogInformation("Request with auth service id {0} already exists", model.AuthServiceId);
                throw new UserServiceException(UserServiceExceptionReasons.AccessRequestAlreadyExists);
            }

            model.GeneratedAt = DateTimeOffset.Now;
            Int32 requestId = await _storage.CreateUserAccessRequest(model);

            UserAccessRequestOverviewModel overviewModel = await _storage.GetUserRequestOverviewById(requestId);
            await _notifier.NewRequestAdded(overviewModel);

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel { Action = MessageActions.Create, RelatedType = MessageRelatedObjectTypes.UserRequests, RelatedObjectId = requestId }, model.AuthServiceId);
        }

        public async Task<MemberStatus> GetMemberStatus(String authServiceId)
        {
            if (String.IsNullOrEmpty(authServiceId) == false)
            {
                _logger.LogDebug("GetMemberStatus with authId {0}", authServiceId);
            }
            else
            {
                _logger.LogDebug("GetMemberStatus");
                _logger.LogInformation("no auth id is given");
                throw new UserServiceException(UserServiceExceptionReasons.NoAuthId);
            }

            MemberStatus status = MemberStatus.NoMember;
            if (await _storage.CheckIfUserExists(authServiceId) == false)
            {
                _logger.LogInformation("user with auth service id {0} doesn't exists", authServiceId);

                if (await _storage.CheckIfUserAccessRequestExists(authServiceId) == true)
                {
                    _logger.LogInformation("access request form user with  auth service id {0} exists", authServiceId);
                    status = MemberStatus.RequestedOutstanding;
                }
                else
                {
                    _logger.LogInformation("Can't find access request with auth service id {0} exists", authServiceId);
                    status = MemberStatus.NoMember;
                }
            }
            else
            {
                status = MemberStatus.IsMember;
            }

            _logger.LogInformation("GetMemberStatus result: {0}", status);
            return status;
        }

        public async Task<IEnumerable<UserAccessRequestOverviewModel>> GetAllAccessRequests()
        {
            _logger.LogDebug("GetAllAccessRequests");
            IEnumerable<UserAccessRequestOverviewModel> result = await _storage.GetAllUserAccessRequest();
            _logger.LogInformation("company amount: {0}", result.Count());

            return result;
        }

        private async Task CheckIfUserExistsInternal(String userAuthId)
        {
            _logger.LogTrace("check if user with auth id {0} exists", userAuthId);
            if(await _storage.CheckIfUserExists(userAuthId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", userAuthId);

                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }
        }
            
        public async Task<Int32> GrantAccess(GrantUserAccessModel model, String userAuthId)
        {
            _logger.LogDebug("GrantAccess");

            await ValidateUserData(model, UserServiceExceptionReasons.GrantAccessIsNull, UserServiceExceptionReasons.InvalidGrantAccessModel);
            await CheckIfUserExistsInternal(userAuthId);

            if (model.Role == BesaRoles.Unknown)
            {
                _logger.LogInformation("no role was specified");
                throw new UserServiceException(UserServiceExceptionReasons.InvalidGrantAccessModel);
            }

            if (await _storage.CheckIfUserAccessRequestExists(model.AuthRequestId) == false)
            {
                _logger.LogInformation("userAccessRequest with id {0} doesn't exists", model.AuthRequestId);
                throw new UserServiceException(UserServiceExceptionReasons.AccessRequestNotFound);
            }

            String authServiceId = await _storage.GetAuthServiceIdByAccessRequestId(model.AuthRequestId);

            if (await _storage.CheckIfUserExists(authServiceId) == true)
            {
                _logger.LogInformation("user with id {0} has allready access", authServiceId);
                throw new UserServiceException(UserServiceExceptionReasons.UserAlreadyExists);
            }

            UserCreateModel createModel = new UserCreateModel
            {
                AuthServiceId = authServiceId,
                CompanyId = model.CompanyId,
                EMailAddress = model.EMailAddress,
                Lastname = model.Lastname,
                Phone = model.Phone,
                Surname = model.Surname,
                Role = model.Role,
            };

            Int32 userID = await _storage.CreateUser(createModel);
            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel { Action = MessageActions.Create, RelatedObjectId = userID, RelatedType = MessageRelatedObjectTypes.User }, userAuthId);

            await _storage.DeleteAuthRequest(model.AuthRequestId);
            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel { Action = MessageActions.Delete, RelatedObjectId = model.AuthRequestId, RelatedType = MessageRelatedObjectTypes.UserRequests }, userAuthId);


            await _notifier.AccessGranted(authServiceId);

            await _protocolService.RestoreProtocolNotifications(userID, model.Role);

            return userID;
        }

        public async Task<BesaRoles> GetRolesByAuthServiceId(String authServiceId)
        {
            _logger.LogDebug("GetRolesByAuthServiceId. authServiceId: {0}", authServiceId);

            if (await _storage.CheckIfUserExists(authServiceId) == false)
            {
                _logger.LogInformation("user with id {0} not found", authServiceId);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            BesaRoles roles = await _storage.GetUserRoles(authServiceId);
            _logger.LogInformation("GetRolesByAuthServiceId result {0}", roles);
            return roles;
        }

        public async Task<Boolean> RemoveMemberStatus(Int32 userID, String userAuthId)
        {
            _logger.LogDebug("RemoveMemberStatus. userid: {0}", userID);

            _logger.LogTrace("check if user with id {0} exists", userID);
            if (await _storage.CheckIfUserExists(userID) == false)
            {
                _logger.LogInformation("user with id {0} not found", userID);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            _logger.LogTrace("check if is user is already suspended");
            if (await _storage.CheckIfHasAuthId(userID) == false)
            {
                _logger.LogInformation("user with id {0} is already suspendend", userID);
                throw new UserServiceException(UserServiceExceptionReasons.UserAlreadySuspended);
            }

            await CheckIfUserExistsInternal(userAuthId);

            _logger.LogTrace("getting auth id for notficitation");
            String authID = await _storage.GetAuthServiceIdByUserId(userID);
            _logger.LogTrace("auth id is {0}", authID);

            _logger.LogTrace("remove auth id from user");
            Boolean result = await _storage.RemoveAuthIdFromUser(userID);
            _logger.LogTrace("remove result: {0}", result);

            if (result == true)
            {
                _logger.LogTrace("inform user");
                await _notifier.RemoveAccess(authID);

                await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel { Action = MessageActions.RemoveMemberState, RelatedType = MessageRelatedObjectTypes.User, RelatedObjectId = userID }, userAuthId);
            }

            return result;
        }

        public async Task<IEnumerable<UserOverviewModel>> GetAllUsers()
        {
            _logger.LogDebug("GetAllUsers");

            IEnumerable<UserOverviewModel> result = await _storage.GetAllUsers();
            _logger.LogTrace("result amount {0}", result.Count());

            return result;
        }

        public async Task<UserEditModel> GetUserDataForEdit(Int32 userId)
        {
            _logger.LogTrace("GetUserDataForEdit. userId :{0}", userId);

            _logger.LogTrace("check if user with id {0} exists", userId);
            if (await _storage.CheckIfUserExists(userId) == false)
            {
                _logger.LogInformation("user with id {0} not found", userId);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            _logger.LogTrace("geting data from database for user with id {0}", userId);
            UserEditModel data = await _storage.GetUserDataForEdit(userId);
            return data;
        }

        public async Task<UserOverviewModel> EditUser(UserEditModel model, String userAuthId)
        {
            _logger.LogTrace("Edit User");

            await ValidateUserData(model, UserServiceExceptionReasons.EditModelIsNull, UserServiceExceptionReasons.EditModelIsInvalid);

            if (await _storage.CheckIfUserExists(model.Id) == false)
            {
                _logger.LogInformation("user with id {0} not found", model.Id);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            if (model.Role == BesaRoles.Unknown)
            {
                _logger.LogInformation("no role was set");
                throw new UserServiceException(UserServiceExceptionReasons.EditModelIsInvalid);
            }

            await CheckIfUserExistsInternal(userAuthId);

            _logger.LogTrace("get role of user with id {0}", model.Id);
            BesaRoles currentRole = await _storage.GetUserRoleById(model.Id);
            _logger.LogTrace("saved user role is {0}", currentRole);
            Boolean roleChanged = currentRole != model.Role;
            _logger.LogTrace("role changed: {0}", roleChanged);

            await _storage.EditUser(model);
            _logger.LogTrace("user updated");

            UserOverviewModel result = await _storage.GetUserOverviewById(model.Id);

            if (roleChanged == true)
            {
                String authSerivceID = await _storage.GetAuthServiceIdByUserId(model.Id);
                _logger.LogTrace("authId for user notification: {0}", authSerivceID);

                await _notifier.RoleChanged(authSerivceID, model.Role);

                await _protocolService.RestoreProtocolNotifications(model.Id, model.Role);
            }

            await _protocolService.CreateProtocolEntry(new ProtocolEntyCreateModel { Action = MessageActions.Update, RelatedType = MessageRelatedObjectTypes.User, RelatedObjectId = model.Id }, userAuthId);

            return result;
        }

        public async Task<IEnumerable<PersonalJobOverviewModel>> GetJobsForMember(String authId)
        {
            _logger.LogTrace("GetJobsForMember. Member auth id: {0}", authId);

            _logger.LogTrace("check if user with auth id {0} exists", authId);
            if (await _storage.CheckIfUserExists(authId) == false)
            {
                _logger.LogInformation("user with auth id {0} not found", authId);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            _logger.LogTrace("check if user with auth id {0} has company", authId);
            if (await _storage.CheckIfUserHasCompany(authId) == false)
            {
                _logger.LogInformation("user with auth id {0} has no company", authId);
                throw new UserServiceException(UserServiceExceptionReasons.BelongsToNoCompany);
            }

            _logger.LogTrace("requesting outstanding jobs from database");

            IEnumerable<PersonalJobOverviewModel> result = await _storage.GetJobOverviewForUser(authId);
            return result;
        }

        public async Task<UserInfoModel> GetUserInfoModel(String authServiceId)
        {
            _logger.LogDebug("GetRolesByAuthServiceId. authServiceId: {0}", authServiceId);

            if (await _storage.CheckIfUserExists(authServiceId) == false)
            {
                _logger.LogInformation("user with id {0} not found", authServiceId);
                throw new UserServiceException(UserServiceExceptionReasons.UserNotFound);
            }

            UserInfoModel result = await _storage.GetUserInfoModel(authServiceId);
            return result;
        }

        #endregion
    }
}
