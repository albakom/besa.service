﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfArticleExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfArticleExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            Boolean existResult = await storage.CheckIfArticleExists(articleId);
            Boolean notExistResult = await storage.CheckIfArticleExists(random.Next(articleId));

            Assert.True(existResult);
            Assert.False(notExistResult);

        }
    }
}
