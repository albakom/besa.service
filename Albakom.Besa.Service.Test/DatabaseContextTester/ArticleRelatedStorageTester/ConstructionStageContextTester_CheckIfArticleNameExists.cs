﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfArticleNameExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfArticleNameExists_NoId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            String name = dataModel.Name;

            Dictionary<String, Boolean> expectedResults = new Dictionary<string, bool>();
            expectedResults.Add(name, true);
            expectedResults.Add(name.ToLower(), true);
            expectedResults.Add(name.ToUpper(), true);
            expectedResults.Add(name.Substring(0,random.Next(name.Length-2)), false);
            expectedResults.Add(name.ToUpper().PadLeft(name.Length*2), true);

            foreach (KeyValuePair<String,Boolean> item in expectedResults)
            {
                Boolean result = await storage.CheckIfArticleNameExists(item.Key,null);
                Assert.Equal(item.Value, result);
            }
        }

        [Fact]
        public async Task CheckIfArticleNameExists_WithId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            String name = dataModel.Name;

            Dictionary<String, Boolean> expectedResults = new Dictionary<string, bool>();
            expectedResults.Add(name, false);
            expectedResults.Add(name.ToLower(), false);
            expectedResults.Add(name.ToUpper(), false);
            expectedResults.Add(name.Substring(0, random.Next(name.Length - 2)), false);
            expectedResults.Add(name.ToUpper().PadLeft(name.Length * 2), false);

            foreach (KeyValuePair<String, Boolean> item in expectedResults)
            {
                Boolean result = await storage.CheckIfArticleNameExists(item.Key, articleId);
                Assert.Equal(item.Value, result);
            }
        }
    }
}
