﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ArticleRelatedStorageTester
{
    public class ConstructionStageContextTester_CheckIfArticlesExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfArticlesExists|ArticleRelatedStorageTester")]
        public async Task CheckIfArticleExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<ArticleDataModel> articles = new List<ArticleDataModel>();
            for (int i = 0; i < amount; i++)
            {
                ArticleDataModel articleDataModel = base.GenerateArticle(random);
                articles.Add(articleDataModel);
            }

            storage.Articles.AddRange(articles);
            storage.SaveChanges();

            List<Int32> allIds = articles.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExitingId = amount + 1;
            while(allIds.Contains(notExitingId) == true)
            {
                notExitingId = random.Next();
            }

            List<Int32> noSubset = new List<int>(subSet);
            noSubset.Add(notExitingId);

            Boolean result = await storage.CheckIfArticlesExists(subSet);
            Assert.True(result);

            Boolean resultFalse = await storage.CheckIfArticlesExists(noSubset);
            Assert.False(resultFalse);
        }
    }
}
