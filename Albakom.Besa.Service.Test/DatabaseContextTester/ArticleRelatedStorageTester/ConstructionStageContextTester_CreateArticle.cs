﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CreateArticle : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateArticle()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ArticleCreateModel createModel = new ArticleCreateModel
            {
                Name = $"Produktname {random.Next()}",
                ProductSoldByMeter = random.NextDouble() > 0.5,
            };

            Int32 id = await storage.CreateArticle(createModel);

            Assert.True(id > 0);

            ArticleDataModel dataModelAfterEdit = storage.Articles.FirstOrDefault(x => x.Id == id);

            Assert.NotNull(dataModelAfterEdit);

            Assert.Equal(id, dataModelAfterEdit.Id);
            Assert.Equal(createModel.Name, dataModelAfterEdit.Name);
            Assert.Equal(createModel.ProductSoldByMeter, dataModelAfterEdit.ProductSoldByMeter);
        }
    }
}
