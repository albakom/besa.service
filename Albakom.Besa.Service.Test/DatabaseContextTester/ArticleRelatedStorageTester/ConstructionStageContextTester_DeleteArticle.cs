﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_DeleteArticle : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteArticle()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            Boolean existResult = await storage.DeleteArticle(articleId);

            ArticleDataModel foundModel = storage.Articles.FirstOrDefault(x => x.Id == articleId);

            Assert.Null(foundModel);
        }
    }
}
