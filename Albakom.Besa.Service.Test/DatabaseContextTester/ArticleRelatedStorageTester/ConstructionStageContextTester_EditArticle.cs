﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_EditArticle : DatabaseTesterBase
    {
        [Fact]
        public async Task EditArticle()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            ArticleEditModel editModel = new ArticleEditModel
            {
                Id = articleId,
                Name = dataModel.Name.Substring(0, random.Next(1, dataModel.Name.Length - 2)),
                ProductSoldByMeter = !dataModel.ProductSoldByMeter,
            };

            await storage.EditArticle(editModel);

            ArticleDataModel dataModelAfterEdit = storage.Articles.FirstOrDefault(x => x.Id == articleId);

            Assert.NotNull(dataModelAfterEdit);

            Assert.Equal(articleId, dataModelAfterEdit.Id);
            Assert.Equal(editModel.Name, dataModelAfterEdit.Name);
            Assert.Equal(editModel.ProductSoldByMeter, dataModelAfterEdit.ProductSoldByMeter);
        }
    }
}
