﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetAllArticles : DatabaseTesterBase
    {
        [Fact]
        public async Task GetAllArticles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleAmount = random.Next(30, 100);

            List<ArticleDataModel> articles = new List<ArticleDataModel>(articleAmount);
            for (int i = 0; i < articleAmount; i++)
            {
                ArticleDataModel dataModel = base.GenerateArticle(random);
                articles.Add(dataModel);
            }

            storage.Articles.AddRange(articles);
            storage.SaveChanges();

            IEnumerable<ArticleOverviewModel> actual = await storage.GetAllArticles();
            Assert.NotNull(actual);

            Assert.Equal(articleAmount, actual.Count());

            foreach (ArticleOverviewModel actualItem in actual)
            {
                ArticleDataModel expctedItem = articles.FirstOrDefault(x => x.Id == actualItem.Id);

                Assert.NotNull(expctedItem);
                Assert.Equal(expctedItem.Id, actualItem.Id);
                Assert.Equal(expctedItem.Name, actualItem.Name);
                Assert.Equal(expctedItem.ProductSoldByMeter, actualItem.ProductSoldByMeter);
                Assert.False(actualItem.IsInUse);
            }
        }
    }
}
