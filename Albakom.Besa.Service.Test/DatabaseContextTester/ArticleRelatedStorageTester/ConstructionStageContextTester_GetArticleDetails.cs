﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetArticleDetails : DatabaseTesterBase
    {
        [Fact]
        public async Task GetArticleDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            ArticleDetailModel actual = await storage.GetArticleDetails(articleId);

            Assert.NotNull(actual);

            Assert.Equal(articleId,actual.Id);
            Assert.Equal(dataModel.Name, actual.Name);
            Assert.Equal(dataModel.ProductSoldByMeter, actual.ProductSoldByMeter);
        }
    }
}
