﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetArticleOverviewById : DatabaseTesterBase
    {
        [Fact]
        public async Task GetArticleOverviewById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 articleId = random.Next();

            ArticleDataModel dataModel = base.GenerateArticle(random);
            dataModel.Id = articleId;
            storage.Articles.Add(dataModel);
            storage.SaveChanges();

            ArticleOverviewModel actual = await storage.GetArticleOverviewById(articleId);
            Assert.NotNull(actual);

            Assert.Equal(articleId, actual.Id);
            Assert.Equal(dataModel.Name, actual.Name);
            Assert.Equal(dataModel.ProductSoldByMeter, actual.ProductSoldByMeter);
            Assert.False(actual.IsInUse);
        }
    }
}
