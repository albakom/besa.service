﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class CompanyDatabaseContextTester : DatabaseTesterBase
    {


        [Fact]
        public async Task CheckIfCompanyExists_True_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            CompanyDataModel model = GenerateCompanyDataModel();

            storage.Companies.Add(model);
            storage.SaveChanges();

            Int32 id = model.Id;

            Boolean result = await storage.CheckIfCompanyExists(id);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfCompanyExists_False_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            CompanyDataModel model = GenerateCompanyDataModel();

            storage.Companies.Add(model);
            storage.SaveChanges();

            Int32 id = model.Id;

            Boolean result = await storage.CheckIfCompanyExists(id + 42);
            Assert.False(result);
        }

        [Fact]
        public async Task CheckIfCompanyNameExists_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            String name = "Testfirma";
            Random random = new Random();

            CompanyDataModel model = new CompanyDataModel
            {
                Name = name,
                City = "Musterstadt",
                Phone = "00454",
                PostalCode = "01245",
                Street = "Musterstr. 4",
                StreetNumber = "20",
            };

            storage.Companies.Add(model);
            storage.SaveChanges();

            Dictionary<String, Boolean> testCases = new Dictionary<string, bool>();
            testCases[name] = true;
            testCases[name.ToLower()] = true;
            testCases[name.ToUpper()] = true;
            testCases[name.PadLeft(5)] = true;
            testCases[name.PadRight(5)] = true;
            testCases[name.PadRight(5).PadLeft(5)] = true;
            testCases[name.Substring(0, random.Next(1, name.Length - 2))] = false;

            foreach (KeyValuePair<String, Boolean> testCase in testCases)
            {
                Boolean result = await storage.CheckIfCompanyNameExists(testCase.Key);
                Assert.Equal(testCase.Value, result);
            }
        }

        [Fact]
        public async Task CheckIfCompanyNameExists_WithId_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            String firstName = "Testfirma";
            Random random = new Random();

            CompanyDataModel firstCompany = base.GenerateCompanyDataModel();
            firstCompany.Name = firstName;

            storage.Companies.Add(firstCompany);
            storage.SaveChanges();

            CompanyDataModel secondCompany = base.GenerateCompanyDataModel();
            String secondName = "anderer Name";
            secondCompany.Name = secondName;
            storage.Companies.Add(secondCompany);
            storage.SaveChanges();

            Boolean firstResult = await storage.CheckIfCompanyNameExists(firstName, firstCompany.Id);
            Assert.False(firstResult);

            Boolean secondResult = await storage.CheckIfCompanyNameExists(secondName, secondCompany.Id);
            Assert.False(secondResult);

            Boolean crossResult1 = await storage.CheckIfCompanyNameExists(secondName, firstCompany.Id);
            Assert.True(crossResult1);

            Boolean crossResult2 = await storage.CheckIfCompanyNameExists(firstName, secondCompany.Id);
            Assert.True(crossResult2);
        }

        [Fact]
        public async Task CheckCreateCompany_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            CompanyCreateModel createModel = new CompanyCreateModel
            {
                Name = "Testfirma",
                Phone = "00545121",
                Address = new AddressModel
                {
                    City = "Teststadt",
                    PostalCode = "01234",
                    Street = "Musterstr.",
                    StreetNumber = "1234a",
                }
            };

            await storage.CreateCompany(createModel);

            CompanyDataModel stored = storage.Companies.FirstOrDefault();

            Assert.NotNull(stored);
            Assert.Equal(createModel.Name, stored.Name);
            Assert.Equal(createModel.Phone, stored.Phone);
            Assert.Equal(createModel.Address.Street, stored.Street);
            Assert.Equal(createModel.Address.StreetNumber, stored.StreetNumber);
            Assert.Equal(createModel.Address.City, stored.City);
            Assert.Equal(createModel.Address.PostalCode, stored.PostalCode);
        }


        [Fact]
        public async Task GetCompanyOverviewById_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            String name = "Testfirma";

            CompanyDataModel model = new CompanyDataModel
            {
                Name = name,
                City = "Musterstadt",
                Phone = "00454",
                PostalCode = "01245",
                Street = "Musterstr. 4",
                StreetNumber = "20",
            };

            storage.Companies.Add(model);
            storage.SaveChanges();

            CompanyOverviewModel result = await storage.GetCompanyOverviewById(model.Id);

            Assert.NotNull(result);
            Assert.Equal(model.Name, result.Name);
            Assert.Equal(model.Id, result.Id);
            Assert.Equal(0, result.EmployeeCount);
        }

        [Fact]
        public async Task GetAllCompanies_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            Dictionary<Int32, CompanyDataModel> expectedResult = new Dictionary<int, CompanyDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                CompanyDataModel model = new CompanyDataModel
                {
                    Name = "Testfirma",
                    City = "Musterstadt",
                    Phone = "00454",
                    PostalCode = "01245",
                    Street = "Musterstr. 4",
                    StreetNumber = "20",
                };

                storage.Companies.Add(model);
                storage.SaveChanges();

                expectedResult.Add(model.Id, model);
            }

            IEnumerable<CompanyOverviewModel> results = await storage.GetAllCompanies();

            Assert.NotNull(results);
            Assert.Equal(expectedResult.Count, results.Count());

            foreach (CompanyOverviewModel result in results)
            {
                Assert.True(expectedResult.ContainsKey(result.Id));

                CompanyDataModel expected = expectedResult[result.Id];
                Assert.Equal(expected.Name, result.Name);
                Assert.Equal(expected.Id, result.Id);
                Assert.Equal(0, result.EmployeeCount);
            }
        }

        [Fact]
        public async Task CheckIfUserBelongsToCompany_Pass()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();
            UserDataModel user = base.GenerateUserDataModel(null);

            storage.Companies.Add(company);
            storage.Users.Add(user);
            storage.SaveChanges();

            user.CompanyId = company.Id;
            storage.SaveChanges();

            Boolean result = await storage.CheckIfUserBelongsToCompany(company.Id, user.ID);
            Assert.True(result);
        }


        [Fact]
        public async Task CheckIfUserBelongsToCompany_Fail()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();
            UserDataModel user = base.GenerateUserDataModel(null);

            storage.Companies.Add(company);
            storage.Users.Add(user);
            storage.SaveChanges();

            user.CompanyId = company.Id;
            storage.SaveChanges();

            Boolean result = await storage.CheckIfUserBelongsToCompany(company.Id - 2, user.ID);
            Assert.False(result);
        }

        [Fact]
        public async Task CheckIfUsersBelongsToCompany_Pass()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(10, 100);
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.CompanyId = company.Id;
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> ids = usersToAdd.Select(x => x.ID).ToList();

            Boolean result = await storage.CheckIfUsersBelongsToCompany(company.Id, ids);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfUsersBelongsToCompany_Fail()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(10, 100);
            Boolean notAdded = false;
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                if (random.NextDouble() > 0.5)
                {
                    user.CompanyId = company.Id;
                }
                else
                {
                    notAdded = true;
                }
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> ids = usersToAdd.Select(x => x.ID).ToList();

            if (notAdded == false)
            {
                ids.RemoveAt(0);
            }

            Boolean result = await storage.CheckIfUsersBelongsToCompany(company.Id, ids);
            Assert.False(result);
        }

        [Fact]
        public async Task AddEmployeesToCompany()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(50, 100);
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            List<UserDataModel> usersToJoinCompany = new List<UserDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.ID = 0;
                if (random.NextDouble() > 0.5)
                {
                    usersToJoinCompany.Add(user);
                }
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> userIds = usersToJoinCompany.Select(x => x.ID).ToList();

            await storage.AddEmployeesToCompany(userIds, company.Id);

            foreach (UserDataModel item in usersToAdd)
            {
                if (usersToJoinCompany.Contains(item) == true)
                {
                    Assert.NotNull(item.CompanyId);
                    Assert.Equal(company.Id, item.CompanyId);
                }
                else
                {
                    Assert.Null(item.CompanyId);
                }
            }
        }

        [Fact]
        public async Task RemoveEmployeesFromCompany()
        {
            BesaDataStorage storage = base.GetStorage();

            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(10, 100);
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            List<UserDataModel> companiesRemover = new List<UserDataModel>();
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                if (random.NextDouble() > 0.5)
                {
                    companiesRemover.Add(user);
                }

                user.CompanyId = company.Id;
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> userIds = companiesRemover.Select(x => x.ID).ToList();

            await storage.RemoveEmployeesFromCompany(userIds);

            foreach (UserDataModel item in usersToAdd)
            {
                if (companiesRemover.Contains(item) == true)
                {
                    Assert.Null(item.CompanyId);
                }
                else
                {
                    Assert.NotNull(item.CompanyId);
                    Assert.Equal(company.Id, item.CompanyId);
                }
            }
        }

        [Fact]
        public async Task AddEmployeeToCompany()
        {
            BesaDataStorage storage = base.GetStorage();

            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(50, 100);
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            Int32 employeeId = usersToAdd.Select(x => x.ID).Skip(random.Next(0, amount - 1)).Take(1).First();

            await storage.AddEmployeeToCompany(company.Id, employeeId);

            foreach (UserDataModel item in usersToAdd)
            {
                if (item.ID == employeeId)
                {
                    Assert.NotNull(item.CompanyId);
                    Assert.Equal(company.Id, item.CompanyId);
                }
                else
                {
                    Assert.Null(item.CompanyId);
                }
            }
        }

        [Fact]
        public async Task RemoveEmployeeToCompany()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random random = new Random();
            Int32 amount = random.Next(50, 100);
            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.CompanyId = company.Id;
                usersToAdd.Add(user);
            }

            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            Int32 employeeId = usersToAdd.Select(x => x.ID).Skip(random.Next(0, amount)).Take(1).First();

            await storage.RemoveEmployeeFromCompany(employeeId);

            foreach (UserDataModel item in usersToAdd)
            {
                if (item.ID == employeeId)
                {
                    Assert.Null(item.CompanyId);
                }
                else
                {
                    Assert.NotNull(item.CompanyId);
                    Assert.Equal(company.Id, item.CompanyId);
                }
            }
        }

        [Fact]
        public async Task EditCompany()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            CompanyEditModel editModel = new CompanyEditModel
            {
                Id = company.Id,
                Name = company.Name.Substring(company.Name.Length / 2),
                Phone = company.Phone.Substring(company.Phone.Length / 2),
                Address = new AddressModel
                {
                    City = company.City.Substring(company.City.Length / 2),
                    Street = company.Street.Substring(company.Street.Length / 2),
                    PostalCode = company.PostalCode.Substring(company.PostalCode.Length / 2),
                    StreetNumber = company.StreetNumber.Substring(company.StreetNumber.Length / 2),
                }
            };

            await storage.EditCompany(editModel);

            company = storage.Companies.FirstOrDefault(x => x.Id == company.Id);

            Assert.NotNull(company);
            Assert.Equal(editModel.Id, company.Id);
            Assert.Equal(editModel.Name, company.Name);
            Assert.Equal(editModel.Phone, company.Phone);
            Assert.Equal(editModel.Address.City, company.City);
            Assert.Equal(editModel.Address.PostalCode, company.PostalCode);
            Assert.Equal(editModel.Address.Street, company.Street);
            Assert.Equal(editModel.Address.StreetNumber, company.StreetNumber);
        }

        [Fact]
        public async Task DeleteCompany()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Int32 id = company.Id;

            await storage.DeleteCompany(id);

            CompanyDataModel afterDelete = storage.Companies.FirstOrDefault(x => x.Id == id);

            Assert.Null(afterDelete);
        }

        [Fact]
        public async Task GetCompanyDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            CompanyDataModel company = base.GenerateCompanyDataModel();

            storage.Companies.Add(company);
            storage.SaveChanges();

            Random rand = new Random();
            Int32 employeAmount = rand.Next(10, 30);

            List<UserDataModel> expectedEmployees = new List<UserDataModel>();

            for (int i = 0; i < employeAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                if(rand.Next() > 0.5)
                {
                    user.CompanyId = company.Id;
                    expectedEmployees.Add(user);
                }

                storage.Users.Add(user);
                storage.SaveChanges();
            }

            CompanyDetailModel model = await storage.GetCompanyDetails(company.Id);

            Assert.NotNull(model);
            Assert.Equal(company.Id, model.Id);
            Assert.Equal(company.Name, model.Name);
            Assert.Equal(company.Name, model.Name);
            Assert.Equal(company.Phone, model.Phone);
            Assert.Equal(company.City, model.Address.City);
            Assert.Equal(company.PostalCode, model.Address.PostalCode);
            Assert.Equal(company.StreetNumber, model.Address.StreetNumber);
            Assert.Equal(company.Street, model.Address.Street);

            Assert.NotNull(model.Employees);
            Assert.Equal(expectedEmployees.Count, model.Employees.Count());

            foreach (UserDataModel item in expectedEmployees)
            {
                SimpleEmployeeModel employee = model.Employees.FirstOrDefault(x => x.Id == item.ID);
                Assert.NotNull(employee);

                Assert.Equal(item.ID, employee.Id);
                Assert.Equal(item.Surname, employee.Surname);
                Assert.Equal(item.Lastname, employee.Lastname);
            }
        }

    }
}
