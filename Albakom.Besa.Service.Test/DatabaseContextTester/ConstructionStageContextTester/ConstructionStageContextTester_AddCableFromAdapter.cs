﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_AddCableFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddCableFromAdapter|ConstructionStageContextTester")]
        public async Task AddCableFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            BranchableDataModel branchable = base.GenerateCabinet(random);
            storage.Branchables.Add(branchable);
            storage.SaveChanges();


            Dictionary<Int32, ProjectAdapterCableModel> expected = new Dictionary<int, ProjectAdapterCableModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterCableModel adapterModel = base.GenerateProjectAdapterCableModel(random);

                Int32 id = await storage.AddCableFromAdapter(adapterModel, branchable.Id,fiberCableTypeDataModel.Id);
                Assert.True(id > 0);

                expected.Add(id, adapterModel);
            }

            foreach (KeyValuePair<Int32, ProjectAdapterCableModel> expectedItem in expected)
            {
                FiberCableDataModel dataModel = storage.FiberCables.FirstOrDefault(x => x.Id ==expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.NotNull(dataModel.StartingAtBranchableleId);
                Assert.Equal(branchable.Id, dataModel.StartingAtBranchableleId);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.Lenght, dataModel.Length);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
                Assert.Equal(expectedItem.Value.DuctColor.Name, dataModel.DuctColorName);
                Assert.Equal(expectedItem.Value.DuctColor.RGBHexValue, dataModel.DuctHexValue);
            }
        }
    }
}
