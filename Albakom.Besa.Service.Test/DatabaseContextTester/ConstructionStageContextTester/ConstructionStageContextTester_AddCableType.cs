﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_AddCableType : DatabaseTesterBase
    {
        [Fact]
        public async Task AddCableType()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ProjectAdapterCableTypeModel createItem = new ProjectAdapterCableTypeModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                FiberAmount = random.Next(30, 100),
                Name = $"Neues-Kabel-Name-Nr {random.Next()}",
                FiberPerBundle = random.Next(30, 100),
            };

            Int32 result = await storage.AddCableType(createItem);
            FiberCableTypeDataModel dataModel = storage.CableTypes.FirstOrDefault(x => x.Id == result);
            Assert.NotNull(dataModel);

            Assert.Equal(createItem.FiberAmount, dataModel.FiberAmount);
            Assert.Equal(createItem.Name, dataModel.Name);
            Assert.Equal(createItem.FiberPerBundle, dataModel.FiberPerBundle);
        }
    }
}
