﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_AddFiberFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddFiberFromAdapter|ConstructionStageContextTester")]
        public async Task AddFiberFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Dictionary<Int32, ProjectAdapterFiberModel> expected = new Dictionary<int, ProjectAdapterFiberModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                ProjectAdapterFiberModel adapterModel = base.GenerateProjectAdapterFiberModel(random);

                Int32 id = await storage.AddFiberFromAdapter(adapterModel, cableDataModel.Id);
                Assert.True(id > 0);

                expected.Add(id, adapterModel);
            }

            foreach (KeyValuePair<Int32, ProjectAdapterFiberModel> expectedItem in expected)
            {
                FiberDataModel dataModel = storage.Fibers.FirstOrDefault(x => x.Id ==expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.Color, dataModel.Color);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
                Assert.Equal(expectedItem.Value.BundleNumber, dataModel.BundleNumber);
                Assert.Equal(expectedItem.Value.NumberinBundle, dataModel.NumberinBundle);
                Assert.Equal(expectedItem.Value.FiberNumber, dataModel.FiberNumber);
            }
        }
    }
}
