﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_AddPatchConnectionByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddPatchConnectionByFlatId|ConstructionStageContextTester")]
        public async Task AddPatchConnectionByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);
            List<Int32> flatsToDelete = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);

                FlatDataModel flatData = base.GenerateFlatDataModel(random);
                flatData.Building = buildingData;
                storage.Flats.Add(flatData);
                storage.SaveChanges();

                Int32 connectionAmount = random.Next(3, 10);

                List<ProjectAdapterPatchModel> projectItems = new List<ProjectAdapterPatchModel>(connectionAmount);
                for (int j = 0; j < connectionAmount; j++)
                {
                    ProjectAdapterPatchModel projectItem = base.GenerateProjectAdapterPatchModel(random);
                    projectItems.Add(projectItem);
                }

                IEnumerable<Int32> result = await storage.AddPatchConnectionByFlatId(flatData.Id, projectItems);
                Assert.NotNull(result);

                Assert.Equal(projectItems.Count, result.Count());

                for (int j = 0; j < projectItems.Count; j++)
                {
                    ProjectAdapterPatchModel expectedItem = projectItems[j];

                    Int32 actualId = result.ElementAt(j);

                    FiberConnenctionDataModel fiberConnenctionDataModel = storage.FiberConnenctions.FirstOrDefault(x => x.Id == actualId);
                    Assert.NotNull(fiberConnenctionDataModel);

                    Assert.Equal(expectedItem.Caption, fiberConnenctionDataModel.Caption);
                    Assert.Equal(expectedItem.ModuleName, fiberConnenctionDataModel.ModuleName);
                    Assert.Equal(expectedItem.RowNumber, fiberConnenctionDataModel.RowNumber);
                    Assert.Equal(expectedItem.ColumnNumber, fiberConnenctionDataModel.ColumnNumber);

                    Assert.Equal(flatData.Id, fiberConnenctionDataModel.FlatId);
                }
            }
        }
    }
}
