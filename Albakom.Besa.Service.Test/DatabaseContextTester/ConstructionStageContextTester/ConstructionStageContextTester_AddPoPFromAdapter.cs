﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_AddPoPFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddPoPFromAdapter|ConstructionStageContextTester")]
        public async Task AddPoPFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            Dictionary<Int32, ProjectAdapterPoPModel> expected = new Dictionary<int, ProjectAdapterPoPModel>(popAmount);

            for (int i = 0; i < popAmount; i++)
            {
                ProjectAdapterPoPModel adapterModel = base.GenerateProjectAdapterPoPModel(random);

                Int32 id = await storage.AddPoPFromAdapter(adapterModel);
                Assert.True(id > 0);

                expected.Add(id, adapterModel);
            }

            foreach (KeyValuePair<Int32,ProjectAdapterPoPModel> expectedItem in expected)
            {
                PoPDataModel dataModel = storage.Pops.FirstOrDefault(x => x.Id ==expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.Location.Latitude, dataModel.Latitude);
                Assert.Equal(expectedItem.Value.Location.Longitude, dataModel.Longitude);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
            }
        }
    }
}
