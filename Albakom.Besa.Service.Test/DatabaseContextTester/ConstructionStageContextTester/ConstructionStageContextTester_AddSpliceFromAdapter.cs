﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_AddSpliceFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddSpliceFromAdapter|ConstructionStageContextTester")]
        public async Task AddSpliceFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Dictionary<Int32, ProjectAdapterSpliceModel> expected = new Dictionary<int, ProjectAdapterSpliceModel>(amount);

            Dictionary<Int32, Int32?[]> fiberMapper = new Dictionary<int, int?[]>();

            for (int i = 0; i < amount; i++)
            {
                Int32? firstFiberId = null;
                Int32? secondFiberId = null;

                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                FiberDataModel fiber = base.GenerateFiberDataModel(random, cableDataModel);
                storage.Fibers.Add(fiber);
                storage.SaveChanges();

                Int32 type = random.Next(0, 3);
                if (type == 0)
                {
                    FiberCableDataModel secondCableDataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                    storage.FiberCables.Add(secondCableDataModel);
                    storage.SaveChanges();

                    FiberDataModel secondFiber = base.GenerateFiberDataModel(random, secondCableDataModel);
                    storage.Fibers.Add(secondFiber);
                    storage.SaveChanges();

                    firstFiberId = fiber.Id;
                    secondFiberId = secondFiber.Id;
                }
                else if (type == 1)
                {
                    firstFiberId = fiber.Id;
                }
                else if (type == 2)
                {
                    secondFiberId = fiber.Id;
                }

                ProjectAdapterSpliceModel adapterModel = base.GenerateProjectAdapterSpliceModel(random, false);

                Int32 id = await storage.AddSpliceFromAdapter(new FiberSpliceCreateModel { SpliceModel = adapterModel, FirstFiberId = firstFiberId, SecondFiberId = secondFiberId });
                Assert.True(id > 0);

                expected.Add(id, adapterModel);
                fiberMapper.Add(id, new Int32?[] { firstFiberId, secondFiberId });
            }

            foreach (KeyValuePair<Int32, ProjectAdapterSpliceModel> expectedItem in expected)
            {
                FiberSpliceDataModel dataModel = storage.Splices.FirstOrDefault(x => x.Id == expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.NumberInTray, dataModel.NumberInTray);
                Assert.Equal(expectedItem.Value.TrayNumber, dataModel.TrayNumber);
                Assert.Equal(expectedItem.Value.Type, dataModel.Type);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);

                Int32?[] expectedFiberIds = fiberMapper[expectedItem.Key];

                Assert.Equal(expectedFiberIds[0], dataModel.FirstFiberId);
                Assert.Equal(expectedFiberIds[1], dataModel.SecondFiberId);
            }
        }
    }
}
