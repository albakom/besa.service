﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfBranchableExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBranchableExists_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinet = base.GenerateCabinet(random);
            SleeveDataModel sleeve = base.GenereteSleeve(random);

            storage.StreetCabintes.Add(cabinet);
            storage.Sleeves.Add(sleeve);
            storage.SaveChanges();

            Boolean resultCabinet = await storage.CheckIfBranchableExists(cabinet.Id);
            Assert.True(resultCabinet);

            Boolean resultSleeve = await storage.CheckIfBranchableExists(sleeve.Id);
            Assert.True(resultSleeve);
        }

        [Fact]
        public async Task CheckIfBranchableExists_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinet = base.GenerateCabinet(random);
            SleeveDataModel sleeve = base.GenereteSleeve(random);

            storage.StreetCabintes.Add(cabinet);
            storage.Sleeves.Add(sleeve);
            storage.SaveChanges();

            Boolean resultCabinet = await storage.CheckIfBranchableExists(random.Next());
            Assert.False(resultCabinet);

            Boolean resultSleeve = await storage.CheckIfBranchableExists(random.Next());
            Assert.False(resultSleeve);
        }
    }
}
