﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfBranchablesExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBranchablesExists|ConstructionStageContextTester")]
        public async Task CheckIfBranchablesExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> dataModels = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                dataModels.Add(dataModel);
            }

            storage.Branchables.AddRange(dataModels);
            storage.SaveChanges();

            List<Int32> allIds = dataModels.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExitingId = amount + 1;
            while(allIds.Contains(notExitingId) == true)
            {
                notExitingId = random.Next();
            }

            List<Int32> noSubset = new List<int>(subSet);
            noSubset.Add(notExitingId);

            Boolean result = await storage.CheckIfBranchablesExists(subSet);
            Assert.True(result);

            Boolean resultFalse = await storage.CheckIfBranchablesExists(noSubset);
            Assert.False(resultFalse);
        }
    }
}
