﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfBuildingExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> allIds = buildings.Select(x => x.Id).ToList();
            Int32 existingId = allIds[random.Next(0, allIds.Count)];
            Int32 notExisitngID = allIds.Count + 2;
            while(allIds.Contains(notExisitngID) == true)
            {
                notExisitngID = random.Next();
            }

            Boolean existingResult = await storage.CheckIfBuildingExists(existingId);
            Boolean notExistingResult = await storage.CheckIfBuildingExists(notExisitngID);

            Assert.True(existingResult);
            Assert.False(notExistingResult);
        }
    }
}
