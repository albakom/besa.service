﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfBuildingExistsByProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBuildingExistsByProjectAdapterId|ConstructionStageContextTester")]
        public async Task CheckIfBuildingExistsByProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            HashSet<String> existings = new HashSet<String>(popAmount);

            for (int i = 0; i < popAmount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(dataModel);
                storage.SaveChanges();

                existings.Add(dataModel.ProjectId);
            }

            foreach (String item in existings)
            {
                Boolean result = await storage.CheckIfBuildingExistsByProjectAdapterId(item);
                Assert.True(result);
            }

            HashSet<String> notExisting = new HashSet<string>();
            Int32 notExisitngAmount = random.Next(3, 10);

            for (int i = 0; i < notExisitngAmount; i++)
            {
                String guid = Guid.NewGuid().ToString();

                while (notExisting.Contains(guid) == true)
                {
                    guid = Guid.NewGuid().ToString();
                }

                notExisting.Add(guid);
            }

            foreach (String item in notExisting)
            {
                Boolean result = await storage.CheckIfBuildingExistsByProjectAdapterId(item);
                Assert.False(result);
            }

        }
    }
}
