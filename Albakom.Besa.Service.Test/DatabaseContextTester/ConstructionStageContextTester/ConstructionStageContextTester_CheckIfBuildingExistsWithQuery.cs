﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfBuildingExistsWithQuery : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBuildingExistsWithQuery|ConstructionStageContextTester")]
        public async Task CheckIfBuildingExistsWithQuery()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            Dictionary<String, Boolean> expectedResults = new Dictionary<string, bool>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingDataModel.StreetName, true);
                expectedResults.Add(buildingDataModel.StreetName.ToLower(), true);
                expectedResults.Add(buildingDataModel.StreetName.ToUpper(), true);
               // expectedResults.Add(buildingDataModel.StreetName.PadLeft(buildingDataModel.StreetName.Length + 4, ' '), true);
                expectedResults.Add(buildingDataModel.StreetName.Substring(3), false);
            }

            foreach (KeyValuePair<String, Boolean> item in expectedResults)
            {
                Boolean actual = await storage.CheckIfBuildingExists(item.Key);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
