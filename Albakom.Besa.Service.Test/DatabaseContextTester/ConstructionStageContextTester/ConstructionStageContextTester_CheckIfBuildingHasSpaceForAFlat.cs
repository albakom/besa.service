﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfBuildingHasSpaceForAFlat : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingHasSpaceForAFlat()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, Boolean> expectedResult = new Dictionary<Int32, Boolean>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 units = buildingDataModel.HouseholdUnits + buildingDataModel.CommercialUnits;

                Int32 flatAmount = random.Next(1, (units + 1) * 2);
                if(flatAmount > units)
                {
                    flatAmount = units;
                }

                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    buildingDataModel.Flats.Add(flatDataModel);
                    flatDataModel.Building = buildingDataModel;

                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                }

                expectedResult.Add(buildingDataModel.Id, units - flatAmount > 0);
            }

            foreach (KeyValuePair<Int32,Boolean> item in expectedResult)
            {
                Boolean result = await storage.CheckIfBuildingHasSpaceForAFlat(item.Key);
                Assert.Equal(item.Value, result);
            }
        }
    }
}
