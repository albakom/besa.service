﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfBuildingsExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingsExists_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> allIds = buildings.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();


            Boolean result = await storage.CheckIfBuildingsExists(subSet);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfBuildingsExists_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> allIds = buildings.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExistingId = amount;

            while (allIds.Contains(notExistingId) == true)
            {
                notExistingId = random.Next();
            }

            subSet.Add(notExistingId);

            Boolean result = await storage.CheckIfBuildingsExists(subSet);
            Assert.False(result);
        }
    }
}
