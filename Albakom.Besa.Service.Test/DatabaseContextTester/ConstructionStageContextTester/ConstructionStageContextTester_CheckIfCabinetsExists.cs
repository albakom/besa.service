﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfCabinetsExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfCabinetsExists_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> items = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                items.Add(dataModel);
            }

            storage.StreetCabintes.AddRange(items);
            storage.SaveChanges();

            List<Int32> allIds = items.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();


            Boolean result = await storage.CheckIfCabinetsExists(subSet);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfCabinetsExists_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> items = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                items.Add(dataModel);
            }

            storage.StreetCabintes.AddRange(items);
            storage.SaveChanges();

            List<Int32> allIds = items.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExistingId = amount;

            while (allIds.Contains(notExistingId) == true)
            {
                notExistingId = random.Next();
            }

            subSet.Add(notExistingId);
            Boolean result = await storage.CheckIfCabinetsExists(subSet);
            Assert.False(result);
        }
    }
}
