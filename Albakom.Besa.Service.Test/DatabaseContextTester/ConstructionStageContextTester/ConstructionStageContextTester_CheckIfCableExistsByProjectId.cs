﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfCableExistsByProjectId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCableExistsByProjectId|ConstructionStageContextTester")]
        public async Task CheckIfCableExistsByProjectId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            HashSet<String> existings = new HashSet<String>(popAmount);

            FiberCableTypeDataModel fiberCableTypeData = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeData);
            storage.SaveChanges();

            for (int i = 0; i < popAmount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeData);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                existings.Add(dataModel.ProjectAdapterId);
            }

            foreach (String item in existings)
            {
                Boolean result = await storage.CheckIfCableExistsByProjectId(item);
                Assert.True(result);
            }

            HashSet<String> notExisting = new HashSet<string>();
            Int32 notExisitngAmount = random.Next(3, 10);

            for (int i = 0; i < notExisitngAmount; i++)
            {
                String guid = Guid.NewGuid().ToString();

                while (notExisting.Contains(guid) == true)
                {
                    guid = Guid.NewGuid().ToString();
                }

                notExisting.Add(guid);
            }

            foreach (String item in notExisting)
            {
                Boolean result = await storage.CheckIfCableExistsByProjectId(item);
                Assert.False(result);
            }

        }
    }
}
