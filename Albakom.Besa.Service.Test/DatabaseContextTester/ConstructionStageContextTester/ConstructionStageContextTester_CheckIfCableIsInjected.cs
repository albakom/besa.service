﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfCableIsInjected : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCableIsInjected|ConstructionStageContextTester")]
        public async Task CheckIfCableIsInjected()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Boolean expectedResult = false;

                Double randomValue = random.NextDouble();
                if (randomValue < 0.7)
                {
                    ContactInfoDataModel customer = base.GenerateContactInfo(random);
                    storage.ContactInfos.Add(customer);
                    storage.SaveChanges();

                    InjectJobDataModel injectJob = new InjectJobDataModel { Building = buildingDataModel, CustomerContact = customer };
                    storage.InjectJobs.Add(injectJob);
                    storage.SaveChanges();

                    if (randomValue < 0.3)
                    {
                        UserDataModel jobber = base.GenerateUserDataModel(null);
                        storage.Users.Add(jobber);
                        storage.SaveChanges();

                        InjectJobFinishedDataModel finishedDataModel = new InjectJobFinishedDataModel { DoneBy = jobber, Job = injectJob, Accepter = jobber, AcceptedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)), };
                        storage.FinishedInjectJobs.Add(finishedDataModel);
                        storage.SaveChanges();

                        expectedResult = true;
                    }
                }

                expectedResults.Add(buildingDataModel.Id, expectedResult);
            }

            foreach (KeyValuePair<Int32, Boolean> expectedResult in expectedResults)
            {
                Boolean actual = await storage.CheckIfCableIsInjected(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
