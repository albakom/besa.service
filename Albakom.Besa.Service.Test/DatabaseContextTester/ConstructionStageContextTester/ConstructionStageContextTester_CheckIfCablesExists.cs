﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfCablesExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCablesExists|ConstructionStageContextTester")]
        public async Task CheckIfCablesExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            FiberCableTypeDataModel typeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            List<FiberCableDataModel> dataModels = new List<FiberCableDataModel>();
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                dataModels.Add(dataModel);
            }

            storage.FiberCables.AddRange(dataModels);
            storage.SaveChanges();

            List<Int32> allIds = dataModels.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExitingId = amount + 1;
            while(allIds.Contains(notExitingId) == true)
            {
                notExitingId = random.Next();
            }

            List<Int32> noSubset = new List<int>(subSet);
            noSubset.Add(notExitingId);

            Boolean result = await storage.CheckIfCablesExists(subSet);
            Assert.True(result);

            Boolean resultFalse = await storage.CheckIfCablesExists(noSubset);
            Assert.False(resultFalse);
        }
    }
}
