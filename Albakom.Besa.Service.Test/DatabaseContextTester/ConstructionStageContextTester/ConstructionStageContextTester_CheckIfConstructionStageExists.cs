﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfConstructionStageExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfConstructionStageExists_True()
        {
            BesaDataStorage storage = base.GetStorage();

            ConstructionStageDataModel constructionStageData = new ConstructionStageDataModel
            {
                Name = "Testgebiet",
            };

            storage.ConstructionStages.Add(constructionStageData);
            storage.SaveChanges();

            Int32 id = constructionStageData.Id;

            Boolean result = await storage.CheckIfConstructionStageExists(id);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfConstructionStageExists_False()
        {
            BesaDataStorage storage = base.GetStorage();

            ConstructionStageDataModel constructionStageData = new ConstructionStageDataModel
            {
                Name = "Testgebiet",
            };

            storage.ConstructionStages.Add(constructionStageData);
            storage.SaveChanges();

            Random random = new Random();
            Int32 id = constructionStageData.Id + random.Next(10,100);

            Boolean result = await storage.CheckIfConstructionStageExists(id);
            Assert.False(result);
        }
    }
}
