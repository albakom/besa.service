﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfConstructionStageNameExistss : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfConstructionStageNameExists()
        {
            BesaDataStorage storage = base.GetStorage();

            String name = "TestGebiet 1";

            ConstructionStageDataModel constructionStageData = new ConstructionStageDataModel();
            constructionStageData.Name = name;

            storage.ConstructionStages.Add(constructionStageData);
            storage.SaveChanges();

            Dictionary<String, Boolean> expected = new Dictionary<string, bool>();
            expected.Add(name, true);
            expected.Add(name.ToLower(), true);
            expected.Add(name.ToUpper(), true);
            expected.Add(name.PadLeft(name.Length*2), true);
            expected.Add(name.PadRight(name.Length * 2), true);
            expected.Add(name.PadRight(name.Length * 2).PadLeft(name.Length * 4), true);
            expected.Add(name.PadLeft(name.Length * 2).ToLower(), true);
            expected.Add(name.PadRight(name.Length * 2).ToLower(), true);
            expected.Add(name.PadRight(name.Length * 2).PadLeft(name.Length * 4).ToLower(), true);
            expected.Add(name.PadLeft(name.Length * 2).ToUpper(), true);
            expected.Add(name.PadRight(name.Length * 2).ToUpper(), true);
            expected.Add(name.PadRight(name.Length * 2).PadLeft(name.Length * 4).ToUpper(), true);
            expected.Add(name.Substring(name.Length/2),false);

            foreach (KeyValuePair<String,Boolean> item in expected)
            {
                Boolean actual = await storage.CheckIfConstructionStageNameExists(item.Key,null);
                Assert.Equal(item.Value, actual);
            }

            foreach (KeyValuePair<String, Boolean> item in expected)
            {
                Boolean actual = await storage.CheckIfConstructionStageNameExists(item.Key, constructionStageData.Id);
                Assert.False(actual);
            }
        }
    }
}
