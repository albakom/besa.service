﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfFiberExistsByProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfFiberExistsByProjectAdapterId|ConstructionStageContextTester")]
        public async Task CheckIfFiberExistsByProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            HashSet<String> existings = new HashSet<String>(popAmount);

            Dictionary<String, Int32> cableIdMapper = new Dictionary<string, int>();

            FiberCableTypeDataModel fiberCableTypeData = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeData);
            storage.SaveChanges();

            for (int i = 0; i < popAmount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeData);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                FiberDataModel fiber = base.GenerateFiberDataModel(random, dataModel);
                storage.Fibers.Add(fiber);
                storage.SaveChanges();
                    
                existings.Add(fiber.ProjectAdapterId);
                cableIdMapper.Add(fiber.ProjectAdapterId, dataModel.Id);
            }

            foreach (String item in existings)
            {
                Boolean result = await storage.CheckIfFiberExistsByProjectAdapterId(item,cableIdMapper[item]);
                Assert.True(result);
            }

            HashSet<String> notExisting = new HashSet<string>();
            Int32 notExisitngAmount = random.Next(3, 10);

            for (int i = 0; i < notExisitngAmount; i++)
            {
                String guid = Guid.NewGuid().ToString();

                while (notExisting.Contains(guid) == true)
                {
                    guid = Guid.NewGuid().ToString();
                }

                notExisting.Add(guid);
            }

            foreach (String item in notExisting)
            {
                Boolean result = await storage.CheckIfFiberExistsByProjectAdapterId(item,0);
                Assert.False(result);
            }

        }
    }
}
