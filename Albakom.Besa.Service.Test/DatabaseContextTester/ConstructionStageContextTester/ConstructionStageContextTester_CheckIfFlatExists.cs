﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfFlatExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfFlatExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(30, 100);

            List<FlatDataModel> flats = new List<FlatDataModel>();
            for (int i = 0; i < amount; i++)
            {
                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                flats.Add(flatDataModel);
            }

            storage.Flats.AddRange(flats);
            storage.SaveChanges();

            List<Int32> allIds = flats.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 existingId = allIds[random.Next(0, allIds.Count)];
            Int32 notExisitngID = allIds.Count + 2;
            while(allIds.Contains(notExisitngID) == true)
            {
                notExisitngID = random.Next();
            }

            Boolean existingResult = await storage.CheckIfFlatExists(existingId);
            Boolean notExistingResult = await storage.CheckIfFlatExists(notExisitngID);

            Assert.True(existingResult);
            Assert.False(notExistingResult);
        }
    }
}
