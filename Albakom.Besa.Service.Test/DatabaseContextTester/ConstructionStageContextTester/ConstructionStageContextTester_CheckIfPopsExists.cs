﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CheckIfPopsExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfPopsExists|ConstructionStageContextTester")]
        public async Task CheckIfPopsExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<PoPDataModel> dataModels = new List<PoPDataModel>();
            for (int i = 0; i < amount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                dataModels.Add(dataModel);
            }

            storage.Pops.AddRange(dataModels);
            storage.SaveChanges();

            List<Int32> allIds = dataModels.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExitingId = amount + 1;
            while(allIds.Contains(notExitingId) == true)
            {
                notExitingId = random.Next();
            }

            List<Int32> noSubset = new List<int>(subSet);
            noSubset.Add(notExitingId);

            Boolean result = await storage.CheckIfPopsExists(subSet);
            Assert.True(result);

            Boolean resultFalse = await storage.CheckIfPopsExists(noSubset);
            Assert.False(resultFalse);
        }
    }
}
