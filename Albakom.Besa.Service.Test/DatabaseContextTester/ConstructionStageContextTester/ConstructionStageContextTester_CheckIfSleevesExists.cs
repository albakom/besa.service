﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CheckIfSleevesExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfSleevesExists_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<SleeveDataModel> items = new List<SleeveDataModel>();
            for (int i = 0; i < amount; i++)
            {
                SleeveDataModel dataModel = base.GenereteSleeve(random);
                items.Add(dataModel);
            }

            storage.Sleeves.AddRange(items);
            storage.SaveChanges();

            List<Int32> allIds = items.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();


            Boolean result = await storage.CheckIfSleevesExists(subSet);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfSleevesExists_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<SleeveDataModel> items = new List<SleeveDataModel>();
            for (int i = 0; i < amount; i++)
            {
                SleeveDataModel dataModel = base.GenereteSleeve(random);
                items.Add(dataModel);
            }

            storage.Sleeves.AddRange(items);
            storage.SaveChanges();

            List<Int32> allIds = items.Select(x => x.Id).ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            Int32 notExistingId = amount;

            while (allIds.Contains(notExistingId) == true)
            {
                notExistingId = random.Next();
            }

            subSet.Add(notExistingId);

            Boolean result = await storage.CheckIfSleevesExists(subSet);
            Assert.False(result);
        }
    }
}
