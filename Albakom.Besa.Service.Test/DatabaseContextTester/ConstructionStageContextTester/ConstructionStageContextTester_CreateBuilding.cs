﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CreateBuilding : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateBuilding|ConstructionStageContextTester")]
        public async Task CreateBuilding()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            CabinetDataModel cabinetData = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetData);
            storage.SaveChanges();

            Dictionary<Int32, ProjectAdapterBuildingModel> expected = new Dictionary<Int32, ProjectAdapterBuildingModel>();
            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterBuildingModel model = base.GenerateProjectAdapterBuildingModel(random);
                Int32 id = await storage.CreateBuilding(model,cabinetData.Id);
                expected.Add(id, model);
            }

            foreach (KeyValuePair<Int32, ProjectAdapterBuildingModel> item in expected)
            {
                BuildingDataModel dataModel = storage.Buildings.FirstOrDefault(x => x.Id == item.Key);
                Assert.NotNull(dataModel);

                Assert.Equal(item.Key, dataModel.Id);
                Assert.Equal(item.Value.AdapterId, dataModel.ProjectId);
                Assert.Equal(item.Value.CommercialUnits, dataModel.CommercialUnits);
                Assert.Equal(item.Value.HouseholdUnits, dataModel.HouseholdUnits);
                Assert.Equal(item.Value.Street, dataModel.StreetName);

                Assert.False(String.IsNullOrEmpty(dataModel.NormalizedStreetName));
                Assert.Equal(item.Value.Coordinate.Latitude, dataModel.Latitude);
                Assert.Equal(item.Value.Coordinate.Longitude, dataModel.Longitude);

                Assert.Equal(item.Value.HouseConnectionColor.Name, dataModel.HouseConnenctionColorName);
                Assert.Equal(item.Value.HouseConnectionColor.RGBHexValue, dataModel.HouseConnenctionHexValue);

                Assert.Equal(item.Value.MicroductColor.Name, dataModel.MicroDuctColorName);
                Assert.Equal(item.Value.MicroductColor.RGBHexValue, dataModel.MicroDuctHexValue);

                Assert.Equal(item.Value.Coordinate.Longitude, dataModel.Longitude);

                Assert.Equal(item.Value.Street, dataModel.StreetName);
                Assert.NotNull(dataModel.NormalizedStreetName);

                Assert.Equal(item.Value.Street.Trim().ToLower(), dataModel.NormalizedStreetName);
            }
        }
    }
}
