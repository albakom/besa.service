﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CreateConstructionStage : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateConstructionStage()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 cabinetAmount = random.Next(30, 100);
            Int32 sleevesAmount = random.Next(30, 100);

            List<CabinetDataModel> cabinets = new List<CabinetDataModel>();
            for (int i = 0; i < cabinetAmount; i++)
            {
                CabinetDataModel model = base.GenerateCabinet(random);
                cabinets.Add(model);
            }

            List<SleeveDataModel> sleeves = new List<SleeveDataModel>();
            for (int i = 0; i < sleevesAmount; i++)
            {
                SleeveDataModel model = base.GenereteSleeve(random);
                sleeves.Add(model);
            }

            storage.StreetCabintes.AddRange(cabinets);
            storage.Sleeves.AddRange(sleeves);
            storage.SaveChanges();

            ConstructionStageCreateModel createModel = new ConstructionStageCreateModel
            {
                Name = "Tesgebiet",
                Cabinets = cabinets.Select(x => x.Id).Take(random.Next(1, cabinetAmount / 2)),
                Sleeves = sleeves.Select(x => x.Id).Take(random.Next(1, cabinetAmount / 2)),
            };

            HashSet<Int32> usedCabinets = new HashSet<int>(createModel.Cabinets);
            HashSet<Int32> usedSleeves = new HashSet<int>(createModel.Sleeves);

            Int32 id = await storage.CreateConstructionStage(createModel);

            ConstructionStageDataModel dataModel = storage.ConstructionStages.FirstOrDefault(x => x.Id == id);

            Assert.NotNull(dataModel);

            Assert.Equal(createModel.Name, dataModel.Name);

            foreach (CabinetDataModel item in cabinets)
            {
                if (usedCabinets.Contains(item.Id) == true)
                {
                    Assert.NotNull(item.ConstructionStageId);
                }
                else
                {
                    Assert.Null(item.ConstructionStageId);
                }
            }

            foreach (SleeveDataModel item in sleeves)
            {
                if (usedSleeves.Contains(item.Id) == true)
                {
                    Assert.NotNull(item.ConstructionStageId);
                }
                else
                {
                    Assert.Null(item.ConstructionStageId);
                }
            }
        }
    }
}
