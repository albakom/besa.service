﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CreateFlat : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateFlat()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Int32 expectedCreateAmount = amount;

            List<FlatCreateModel> list = new List<FlatCreateModel>(expectedCreateAmount);

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            FlatCreateModel flat = new FlatCreateModel
            {
                BuildingId = buildingDataModel.Id,
                Description = $"Tolle Wohnungsbeschreibung {random.Next()}",
                Floor = $"Geschoss Nr. {random.Next()}",
                Number = $"Wohung Nr. {random.Next()}",
            };


            Int32 result = await storage.CreateFlat(flat);

            FlatDataModel dataModel = storage.Flats.FirstOrDefault(x => x.Id == result);

            Assert.NotNull(dataModel);

            Assert.Equal(flat.BuildingId, dataModel.BuildingId);
            Assert.Equal(flat.Description, dataModel.Description);
            Assert.Equal(flat.Floor, dataModel.Floor);
            Assert.Equal(flat.Number, dataModel.Number);
        }
    }
}
