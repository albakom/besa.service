﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CreateFlats : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateFlats()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Int32 expectedCreateAmount = amount;

            List<FlatCreateModel> list = new List<FlatCreateModel>(expectedCreateAmount);

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            for (int i = 0; i < expectedCreateAmount; i++)
            {
                FlatCreateModel flat = new FlatCreateModel
                {
                    BuildingId = buildingDataModel.Id,
                    Description = $"Tolle Wohnungsbeschreibung {random.Next()}",
                    Floor = $"Geschoss Nr. {random.Next()}",
                    Number = $"Wohung Nr. {random.Next()}",
                };

                list.Add(flat);
            }

            List<Int32> result = (await storage.CreateFlats(list)).ToList();
            Assert.NotNull(result);

            Assert.Equal(list.Count, result.Count);

            for (int i = 0; i < list.Count; i++)
            {
                Int32 flatId = result[i];
                FlatCreateModel createModel = list[i];
                FlatDataModel dataModel = storage.Flats.FirstOrDefault(x => x.Id == flatId);

                Assert.NotNull(dataModel);

                Assert.Equal(createModel.BuildingId, dataModel.BuildingId);
                Assert.Equal(createModel.Description, dataModel.Description);
                Assert.Equal(createModel.Floor, dataModel.Floor);
                Assert.Equal(createModel.Number, dataModel.Number);
            }
        }
    }
}
