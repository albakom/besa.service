﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_CreateSleeve : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateSleeve|ConstructionStageContextTester")]
        public async Task CreateSleeve()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Dictionary<Int32, ProjectAdapterSleeveModel> expected = new Dictionary<Int32, ProjectAdapterSleeveModel>();
            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterSleeveModel model = base.GenerateProjectAdapterSleeveModel(random);
                Int32 id = await storage.CreateSleeve(model);
                expected.Add(id, model);
            }

            foreach (KeyValuePair<Int32, ProjectAdapterSleeveModel> item in expected)
            {
                SleeveDataModel dataModel = storage.Sleeves.FirstOrDefault(x => x.Id == item.Key);
                Assert.NotNull(dataModel);

                Assert.Equal(item.Key, dataModel.Id);
                Assert.Equal(item.Value.AdapterId, dataModel.ProjectId);
                Assert.Equal(item.Value.Name, dataModel.Name);
                Assert.NotNull(dataModel.NormalizedName);
                Assert.Equal(item.Value.Coordinate.Latitude, dataModel.Latitude);
                Assert.Equal(item.Value.Coordinate.Longitude, dataModel.Longitude);
            }
        }
    }
}
