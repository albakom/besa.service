﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_CreateSleeves : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateSleeves()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Int32 createAmount = amount;
            Dictionary<String, ProjectAdapterSleeveModel> items = new Dictionary<string, ProjectAdapterSleeveModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterSleeveModel dataModel = new ProjectAdapterSleeveModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = random.Next(30, 50) + random.NextDouble(),
                        Longitude = random.Next(30, 50) + random.NextDouble(),
                    },
                    Name = $"Muffer {random.Next()}"
                };

                Int32 buildingAmount = random.Next(3, 10);
                createAmount += buildingAmount;
                List<ProjectAdapterBuildingModel> buildings = new List<ProjectAdapterBuildingModel>();
                for (int j = 0; j < buildingAmount; j++)
                {
                    ProjectAdapterBuildingModel buildingItem = new ProjectAdapterBuildingModel
                    {
                        AdapterId = Guid.NewGuid().ToString(),
                        CommercialUnits = random.Next(3, 10),
                        HouseholdUnits = random.Next(3, 10),
                        Coordinate = new GPSCoordinate
                        {
                            Latitude = random.Next(30, 40) + random.NextDouble(),
                            Longitude = random.Next(30, 40) + random.NextDouble(),
                        },
                        Street = $"Tolle Straße {random.Next()}",
                        HouseConnectionColor = new ProjectAdapterColor { Name = random.Next().ToString(), RGBHexValue = "0x" + random.Next(999999) },
                        MicroductColor = new ProjectAdapterColor { Name = random.Next().ToString(), RGBHexValue = "0x" + random.Next(999999) },
                    };

                    buildings.Add(buildingItem);
                }

                dataModel.Buildings = buildings;

                items.Add(dataModel.AdapterId, dataModel);
            }

            Int32 result = await storage.CreateSleeves(items.Values);

            List<SleeveDataModel> datamodels = storage.Sleeves.ToList();

            Assert.Equal(createAmount, result);
            Assert.Equal(amount, datamodels.Count);

            foreach (SleeveDataModel item in datamodels)
            {
                Assert.False(String.IsNullOrEmpty(item.ProjectId));
                Assert.True(items.ContainsKey(item.ProjectId));

                ProjectAdapterSleeveModel projectItem = items[item.ProjectId];

                Assert.NotNull(projectItem);
                Assert.Equal(projectItem.AdapterId, item.ProjectId);
                Assert.Equal(projectItem.Name, item.Name);

                Assert.False(String.IsNullOrEmpty(item.NormalizedName));
                Assert.Equal(projectItem.Coordinate.Latitude, item.Latitude);
                Assert.Equal(projectItem.Coordinate.Longitude, item.Longitude);

                List<BuildingDataModel> buildings = storage.Buildings.Where(x => x.BranchableId == item.Id).ToList();

                Assert.Equal(projectItem.Buildings.Count(), buildings.Count());

                for (int i = 0; i < projectItem.Buildings.Count(); i++)
                {
                    ProjectAdapterBuildingModel projectionBuildingModel = projectItem.Buildings.ElementAt(i);
                    BuildingDataModel buildingData = buildings[i];

                    Assert.Equal(projectionBuildingModel.AdapterId, buildingData.ProjectId);
                    Assert.Equal(projectionBuildingModel.CommercialUnits, buildingData.CommercialUnits);
                    Assert.Equal(projectionBuildingModel.HouseholdUnits, buildingData.HouseholdUnits);
                    Assert.Equal(projectionBuildingModel.Street, buildingData.StreetName);

                    Assert.False(String.IsNullOrEmpty(buildingData.NormalizedStreetName));
                    Assert.Equal(projectionBuildingModel.Coordinate.Latitude, buildingData.Latitude);
                    Assert.Equal(projectionBuildingModel.Coordinate.Longitude, buildingData.Longitude);

                    Assert.Equal(projectionBuildingModel.HouseConnectionColor.Name, buildingData.HouseConnenctionColorName);
                    Assert.Equal(projectionBuildingModel.HouseConnectionColor.RGBHexValue, buildingData.HouseConnenctionHexValue);

                    Assert.Equal(projectionBuildingModel.MicroductColor.Name, buildingData.MicroDuctColorName);
                    Assert.Equal(projectionBuildingModel.MicroductColor.RGBHexValue, buildingData.MicroDuctHexValue);

                    Assert.Equal(projectionBuildingModel.Coordinate.Longitude, buildingData.Longitude);

                }
            }
        }
    }
}
