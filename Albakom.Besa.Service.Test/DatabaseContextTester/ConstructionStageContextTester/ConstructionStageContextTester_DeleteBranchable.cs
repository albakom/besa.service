﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeleteBranchable : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteBranchable|ConstructionStageContextTester")]
        public async Task DeleteBranchable()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinet = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinet);
            storage.SaveChanges();

            PrepareBranchableJobDataModel prepareBranchableJob = new PrepareBranchableJobDataModel { Branchable = cabinet };
            storage.PrepareBranchableForSpliceJobs.Add(prepareBranchableJob);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>();
            jobIds.Add(prepareBranchableJob.Id);

            Int32 branchableId = cabinet.Id;
            Boolean result = await storage.DeleteBranchable(branchableId);

            CabinetDataModel storedModel = storage.StreetCabintes.FirstOrDefault(x => x.Id == branchableId);
            Assert.Null(storedModel);

            foreach (Int32 jobId in jobIds)
            {
                JobDataModel storedFlatModel = storage.Jobs.FirstOrDefault(x => x.Id == jobId);
                Assert.Null(storedModel);
            }
        }
    }
}
