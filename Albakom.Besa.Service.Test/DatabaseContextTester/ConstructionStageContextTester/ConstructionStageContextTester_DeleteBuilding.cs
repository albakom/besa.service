﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeleteBuilding : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteBuilding|ConstructionStageContextTester")]
        public async Task DeleteBuilding()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinet = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinet);
            storage.SaveChanges();

            BuildingDataModel buildingData = base.GenerateBuilding(random);
            buildingData.Branchable = cabinet;
            storage.Buildings.Add(buildingData);
            storage.SaveChanges();

            ContactInfoDataModel owner = base.GenerateContactInfo(random);
            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.AddRange(owner, customer);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>();

            BranchOffJobDataModel branchOffJobDataModel = new BranchOffJobDataModel { Building = buildingData };
            ConnectBuildingJobDataModel connectBuildingJobModel = new ConnectBuildingJobDataModel { Building = buildingData, Owner = owner };
            InjectJobDataModel injectJobData = new InjectJobDataModel { Building = buildingData, CustomerContact = customer };
            storage.BranchOffJobs.Add(branchOffJobDataModel);
            storage.ConnectBuildingJobs.Add(connectBuildingJobModel);
            storage.InjectJobs.Add(injectJobData);
            storage.SaveChanges();

            jobIds.Add(branchOffJobDataModel.Id);
            jobIds.Add(connectBuildingJobModel.Id);
            jobIds.Add(injectJobData.Id);

            Int32 flatAmount = random.Next(3, 10);
            List<Int32> flatIds = new List<int>();
            for (int i = 0; i < flatAmount; i++)
            {
                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingData;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                flatIds.Add(flatDataModel.Id);

                SpliceBranchableJobDataModel spliceBranchableJobDataModel = new SpliceBranchableJobDataModel { Flat = flatDataModel };
                storage.SpliceBranchableJobs.Add(spliceBranchableJobDataModel);

                storage.SaveChanges();
                jobIds.Add(spliceBranchableJobDataModel.Id);
            }

            Int32 buildingId = buildingData.Id;
            Boolean result = await storage.DeleteBuilding(buildingId);

            BuildingDataModel storedModel = storage.Buildings.FirstOrDefault(x => x.Id == buildingId);
            Assert.Null(storedModel);

            foreach (Int32 flatID in flatIds)
            {
                FlatDataModel storedFlatModel = storage.Flats.FirstOrDefault(x => x.Id == flatID);
                Assert.Null(storedModel);
            }

            foreach (Int32 jobId in jobIds)
            {
                JobDataModel storedFlatModel = storage.Jobs.FirstOrDefault(x => x.Id == jobId);
                Assert.Null(storedModel);
            }
        }
    }
}
