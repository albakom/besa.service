﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeleteCable : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteCable|ConstructionStageContextTester")]
        public async Task DeleteCable()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            List<Int32> exitingItemIds = new List<int>();


            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);
            storage.SaveChanges();

            Dictionary<Int32, Boolean> expectedResult = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                exitingItemIds.Add(dataModel.Id);
            }

            foreach (Int32 itemId in exitingItemIds)
            {
                Boolean shouldDelete = random.NextDouble() > 0.5;
                if (shouldDelete == true)
                {
                    Boolean result = await storage.DeleteCable(itemId);
                    Assert.True(result);
                }

                expectedResult.Add(itemId, shouldDelete);
            }

            foreach (KeyValuePair<Int32, Boolean> expectedItem in expectedResult)
            {
                FiberCableDataModel dataModel = storage.FiberCables.FirstOrDefault(x => x.Id == expectedItem.Key);

                if (expectedItem.Value == true)
                {
                    Assert.Null(dataModel);
                }
                else
                {
                    Assert.NotNull(dataModel);
                }
            }
        }
    }
}
