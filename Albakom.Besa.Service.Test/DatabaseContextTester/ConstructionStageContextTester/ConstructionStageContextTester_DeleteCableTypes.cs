﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_DeleteCableTypes : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteCableTypes()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 cableTypeAmount = random.Next(30, 100);
            List<String> projectAdapterIds = new List<string>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                FiberCableTypeDataModel dataModel = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(dataModel);
                projectAdapterIds.Add(dataModel.AdapterId);
            }

            storage.SaveChanges();

            List<String> projectAdapterNotToDelete = new List<string>(projectAdapterIds);
            Int32 deleteAmount = random.Next(1, cableTypeAmount / 2);
            List<String> idsToDelte = new List<string>(deleteAmount);
            for (int i = 0; i < deleteAmount; i++)
            {
                String item = projectAdapterNotToDelete[random.Next(0, projectAdapterNotToDelete.Count)];

                projectAdapterNotToDelete.Remove(item);
                idsToDelte.Add(item);
            }

            Boolean result = await storage.DeleteCableTypes(idsToDelte);

            Assert.True(result);

            foreach (String adapterId in projectAdapterIds)
            {
                FiberCableTypeDataModel dataModel = storage.CableTypes.FirstOrDefault(x => x.AdapterId == adapterId);
                if(idsToDelte.Contains(adapterId) == true)
                {
                    Assert.Null(dataModel);
                }
                else
                {
                    Assert.NotNull(dataModel);
                }
            }
        }
    }
}
