﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeletePatchConnectionByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeletePatchConnectionByFlatId|ConstructionStageContextTester")]
        public async Task DeletePatchConnectionByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);
            List<Int32> flatsToDelete = new List<int>();
            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {

                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);

                FlatDataModel flatData = base.GenerateFlatDataModel(random);
                flatData.Building = buildingData;
                storage.Flats.Add(flatData);
                storage.SaveChanges();

                Boolean deleteConnections = random.NextDouble() > 0.5;
                if (deleteConnections == true)
                {
                    flatsToDelete.Add(flatData.Id);
                }

                Int32 connectionAmount = random.Next(3, 10);

                for (int j = 0; j < connectionAmount; j++)
                {
                    FiberConnenctionDataModel dataModel = base.GenerateFiberConnenctionDataModel(random, flatData);
                    storage.FiberConnenctions.Add(dataModel);
                    storage.SaveChanges();

                    expectedResults.Add(dataModel.Id, deleteConnections);
                }
            }

            foreach (Int32 flatId in flatsToDelete)
            {
                Boolean result = await storage.DeletePatchConnectionByFlatId(flatId);
                Assert.True(result);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedItem in expectedResults)
            {
                FiberConnenctionDataModel fiberConnenctionDataModel = storage.FiberConnenctions.FirstOrDefault(x => x.Id == expectedItem.Key);

                if(expectedItem.Value == true)
                {
                    Assert.Null(fiberConnenctionDataModel);
                }
                else
                {
                    Assert.NotNull(fiberConnenctionDataModel);
                }
            }
        }
    }
}
