﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeletePopById : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeletePopById|ConstructionStageContextTester")]
        public async Task DeletePopById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            List<Int32> existingPopIds = new List<int>();

            Dictionary<Int32, Boolean> expectedResult = new Dictionary<int, bool>();

            for (int i = 0; i < popAmount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                existingPopIds.Add(dataModel.Id);
            }

            foreach (Int32 popId in existingPopIds)
            {
                Boolean shouldDelete = random.NextDouble() > 0.5;
                if (shouldDelete == true)
                {
                    Boolean result = await storage.DeletePopById(popId);
                    Assert.True(result);
                }

                expectedResult.Add(popId, shouldDelete);
            }

            foreach (KeyValuePair<Int32, Boolean> expectedItem in expectedResult)
            {
                PoPDataModel dataModel = storage.Pops.FirstOrDefault(x => x.Id == expectedItem.Key);

                if (expectedItem.Value == true)
                {
                    Assert.Null(dataModel);
                }
                else
                {
                    Assert.NotNull(dataModel);
                }
            }
        }
    }
}