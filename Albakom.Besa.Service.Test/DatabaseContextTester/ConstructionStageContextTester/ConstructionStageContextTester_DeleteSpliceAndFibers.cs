﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_DeleteSpliceAndFibers : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteSpliceAndFibers|ConstructionStageContextTester")]
        public async Task DeleteSpliceAndFibers()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            List<Int32> exitingItemIds = new List<int>();

            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);
            storage.SaveChanges();

            Dictionary<Int32, Boolean> expectedResult = new Dictionary<int, bool>();

            Dictionary<Int32, List<Int32>> fiberMapper = new Dictionary<int, List<int>>();
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                FiberDataModel fiberDataModel = base.GenerateFiberDataModel(random, dataModel);
                storage.Fibers.Add(fiberDataModel);
                storage.SaveChanges();

                FiberSpliceDataModel spliceDataModel = new FiberSpliceDataModel
                {
                    NumberInTray = random.Next(3, 10),
                    TrayNumber = random.Next(1, 5),
                    Type = SpliceTypes.Buffer,
                };

                List<Int32> fiberIds = new List<int> { fiberDataModel.Id };


                Int32 type = random.Next(0, 3);
                if(type == 0)
                {
                    FiberCableDataModel secondCabledataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                    storage.FiberCables.Add(secondCabledataModel);
                    storage.SaveChanges();

                    FiberDataModel secondfiberDataModel = base.GenerateFiberDataModel(random, secondCabledataModel);
                    storage.Fibers.Add(secondfiberDataModel);
                    storage.SaveChanges();

                    spliceDataModel.FirstFiber = fiberDataModel;
                    spliceDataModel.SecondFiber = secondfiberDataModel;

                    fiberIds.Add(secondfiberDataModel.Id);
                }
                else if(type == 1)
                {
                    spliceDataModel.FirstFiber = fiberDataModel;
                }
                else if(type == 2)
                {
                    spliceDataModel.SecondFiber = fiberDataModel;
                }

                storage.Splices.Add(spliceDataModel);
                storage.SaveChanges();

                exitingItemIds.Add(spliceDataModel.Id);
                fiberMapper.Add(spliceDataModel.Id, fiberIds);
            }

            foreach (Int32 itemId in exitingItemIds)
            {
                Boolean shouldDelete = random.NextDouble() > 0.5;
                if (shouldDelete == true)
                {
                    Boolean result = await storage.DeleteSpliceAndFibers(itemId);
                    Assert.True(result);
                }

                expectedResult.Add(itemId, shouldDelete);
            }

            foreach (KeyValuePair<Int32, Boolean> expectedItem in expectedResult)
            {
                FiberSpliceDataModel dataModel = storage.Splices.FirstOrDefault(x => x.Id == expectedItem.Key);

                if (expectedItem.Value == true)
                {
                    Assert.Null(dataModel);

                    foreach (Int32 fiberId in fiberMapper[expectedItem.Key])
                    {
                        FiberDataModel fiber = storage.Fibers.FirstOrDefault(x => x.Id == fiberId);
                        Assert.Null(fiber);
                    }
                }
                else
                {
                    Assert.NotNull(dataModel);
                }
            }
        }
    }
}
