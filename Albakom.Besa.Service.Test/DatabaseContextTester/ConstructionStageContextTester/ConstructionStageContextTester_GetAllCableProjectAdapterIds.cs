﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetAllCableProjectAdapterIds : DatabaseTesterBase
    {
        [Fact]
        public async Task GetAllCableProjectAdapterIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();


            Int32 cableTypeAmount = random.Next(30, 100);
            HashSet<String> expected = new HashSet<string>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                FiberCableTypeDataModel dataModel = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(dataModel);
                expected.Add(dataModel.AdapterId);
            }
            storage.SaveChanges();

            ICollection<String> actual = await storage.GetAllCableProjectAdapterIds();

            Assert.NotNull(actual);
            Assert.Equal(expected.Count, actual.Count);

            foreach (String actualItem in actual)
            {
                Assert.Contains(actualItem, expected);
            }
        }
    }
}
