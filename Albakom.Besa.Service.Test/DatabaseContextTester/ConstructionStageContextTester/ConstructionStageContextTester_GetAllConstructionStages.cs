﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetAllConstructionStages : DatabaseTesterBase
    {
        private class ConstructionOverviewTestModel
        {
            public Int32 HouseholdUnits { get; set; }
            public Int32 CommercialUnits { get; set; }
            public Int32 StreetCabinetAmount { get; set; }
            public Int32 SleeveAmount { get; set; }
            public ConstructionStageDataModel DataModel { get; set; }
        }

        [Fact]
        public async Task GetAllConstructionStages()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 stageAmount = random.Next(3, 10);

            Dictionary<Int32, ConstructionOverviewTestModel> expected = new Dictionary<Int32, ConstructionOverviewTestModel>(stageAmount);
            Dictionary<Int32, Int32> householdAmountTester = new Dictionary<int, int>();
            Dictionary<Int32, Int32> commercialUnitAmountTester = new Dictionary<int, int>();

            for (int i = 0; i < stageAmount; i++)
            {
                ConstructionStageDataModel stageDataModel = new ConstructionStageDataModel()
                {
                    Name = $"Abschnitt {i + 1}",
                };

                storage.ConstructionStages.Add(stageDataModel);
                storage.SaveChanges();


                Int32 buildingAmount = random.Next(10, 100);
                Int32 cabinetAmount = random.Next(3, 10);
                Int32 sleeveAmount = random.Next(3, 10);

                Int32 householdUnits = 0;
                Int32 commercialUnits = 0;

                List<CabinetDataModel> cabinets = new List<CabinetDataModel>(buildingAmount);
                List<SleeveDataModel> sleeves = new List<SleeveDataModel>(buildingAmount);

                for (int j = 0; j < cabinetAmount; j++)
                {
                    CabinetDataModel cabinetData = base.GenerateCabinet(random);
                    cabinetData.ConstructionStageId = stageDataModel.Id;

                    cabinets.Add(cabinetData);
                }

                for (int j = 0; j < sleeveAmount; j++)
                {
                    SleeveDataModel sleeveData = base.GenereteSleeve(random);
                    sleeveData.ConstructionStageId = stageDataModel.Id;

                    sleeves.Add(sleeveData);
                }

                storage.StreetCabintes.AddRange(cabinets);
                storage.Sleeves.AddRange(sleeves);

                storage.SaveChanges();

                expected.Add(stageDataModel.Id, new ConstructionOverviewTestModel
                {
                    CommercialUnits = commercialUnits,
                    DataModel = stageDataModel,
                    HouseholdUnits = householdUnits,
                    SleeveAmount = sleeveAmount,
                    StreetCabinetAmount = cabinetAmount,
                });
            }


            IEnumerable<ConstructionStageOverviewModel> result = await storage.GetAllConstructionStages();

            Assert.NotNull(result);
            Assert.Equal(expected.Count, result.Count());

            foreach (ConstructionStageOverviewModel actual in result)
            {
                Assert.True(expected.ContainsKey(actual.Id));

                ConstructionOverviewTestModel expectedItem = expected[actual.Id];

                Assert.Equal(expectedItem.DataModel.Id, actual.Id);
                Assert.Equal(expectedItem.DataModel.Name, actual.Name);
                Assert.Equal(expectedItem.CommercialUnits, actual.CommercialUnits);
                Assert.Equal(expectedItem.HouseholdUnits, actual.HouseholdsUnits);
                Assert.Equal(expectedItem.SleeveAmount, actual.SleeveAmount);
                Assert.Equal(expectedItem.StreetCabinetAmount, actual.CabinetAmount);
            }

        }
    }
}
