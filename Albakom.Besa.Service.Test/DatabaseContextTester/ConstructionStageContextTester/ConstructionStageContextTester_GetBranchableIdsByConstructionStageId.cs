﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBranchableIdsByConstructionStageId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBranchableIdsByConstructionStageId|ConstructionStageContextTester")]
        public async Task GetBranchableIdsByConstructionStageId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageAmount = random.Next(3, 10);

            Dictionary<Int32, IEnumerable<Int32>> expectedResult = new Dictionary<int, IEnumerable<int>>();
            for (int i = 0; i < constructionStageAmount; i++)
            {
                Int32 cabinentAmount = random.Next(3, 10);
                ConstructionStageDataModel constructionStageDataModel = base.GenerateConstructionStage(random);
                storage.ConstructionStages.Add(constructionStageDataModel);
                storage.SaveChanges();

                List<Int32> branchableIds = new List<Int32>(cabinentAmount);
                for (int j = 0; j < cabinentAmount; j++)
                {
                    CabinetDataModel cabinet = base.GenerateCabinet(random);
                    cabinet.ConstructionStage = constructionStageDataModel;
                    storage.Branchables.Add(cabinet);
                    storage.SaveChanges();

                    Int32 buildindAmount = random.Next(10, 30);
                    List<SimpleBuildingOverviewModel> buildings = new List<SimpleBuildingOverviewModel>(buildindAmount);

                    branchableIds.Add(cabinet.Id);
                }

                expectedResult.Add(constructionStageDataModel.Id, branchableIds);
            }

            foreach (KeyValuePair<Int32,IEnumerable<Int32>> item in expectedResult)
            {
                IEnumerable<Int32> actual = await storage.GetBranchableIdsByConstructionStageId(item.Key);

                Assert.NotNull(actual);

                Assert.Equal(item.Value, actual);
            }
        }
    }
}
