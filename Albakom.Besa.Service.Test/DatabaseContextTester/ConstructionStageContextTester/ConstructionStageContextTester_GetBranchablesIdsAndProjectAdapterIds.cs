﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBranchablesIdsAndProjectAdapterIds : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBranchablesIdsAndProjectAdapterIds|ConstructionStageContextTester")]
        public async Task GetBranchablesIdsAndProjectAdapterIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Dictionary<String,Int32> expected = new Dictionary<String, Int32>(amount);
            for (int i = 0; i < amount; i++)
            {
                BranchableDataModel branchableDataModel = null;
                if(random.NextDouble() > 0.5)
                {
                    branchableDataModel = base.GenerateCabinet(random);
                }
                else
                {
                    branchableDataModel = base.GenereteSleeve(random);
                }
                
                storage.Branchables.Add(branchableDataModel);
                storage.SaveChanges();

                expected.Add(branchableDataModel.ProjectId,branchableDataModel.Id);
            }

            IDictionary<String,Int32> actual = await storage.GetBranchablesIdsAndProjectAdapterIds();

            Assert.NotNull(actual);
            Assert.Equal(expected.Count, actual.Count());

            foreach (KeyValuePair<String,Int32> actualItem in actual)
            {
                Assert.True(expected.ContainsKey(actualItem.Key));
                Assert.Equal(expected[actualItem.Key], actualItem.Value);
            }
        }


        [Fact(DisplayName = "GetBranchablesWithProjectAdapterId_Partial|ConstructionStageContextTester")]
        public async Task GetBranchablesWithProjectAdapterId_Partial()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Dictionary<String, Int32> adapterIdMapper = new Dictionary<string, int>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                storage.Branchables.Add(dataModel);
                storage.SaveChanges();

                adapterIdMapper.Add(dataModel.ProjectId, dataModel.Id);
            }

            List<Int32> allIds = adapterIdMapper.Values.ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            IDictionary<String, Int32> actual = await storage.GetBranchablesWithProjectAdapterId(subSet);
            Assert.NotNull(actual);
            Assert.Equal(subSet.Count, actual.Count);

            foreach (KeyValuePair<String, Int32> actualItem in actual)
            {
                Assert.True(adapterIdMapper.ContainsKey(actualItem.Key));
                Assert.Equal(adapterIdMapper[actualItem.Key], actualItem.Value);

                Assert.Contains(actualItem.Value, subSet);
            }
        }
    }
}
