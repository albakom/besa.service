﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBranchablesWithProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBranchablesWithProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetBranchablesWithProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 branchableAmount = random.Next(20, 30);

            Dictionary<String, Int32> expectedResults = new Dictionary<String, Int32>();

            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();


                expectedResults.Add(cabinetDataModel.ProjectId, cabinetDataModel.Id);
            }

            IDictionary<String, Int32> actualResult = await storage.GetBranchablesWithProjectAdapterId();

            Assert.NotNull(actualResult);
            Assert.Equal(expectedResults.Count, actualResult.Count);


            foreach (KeyValuePair<String, Int32> actualItem in actualResult)
            {
                Assert.True(expectedResults.ContainsKey(actualItem.Key));

                Assert.Equal(expectedResults[actualItem.Key], actualItem.Value);
            }
        }
    }
}
