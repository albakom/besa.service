﻿using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingAmount : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingAmount()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> items = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                items.Add(dataModel);
            }

            storage.Buildings.AddRange(items);
            storage.SaveChanges();

            Int32 result = await storage.GetBuildingAmount();
            Assert.Equal(amount, result);
        }
    }
}
