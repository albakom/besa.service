﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingByStreetName : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingByStreetName|ConstructionStageContextTester")]
        public async Task GetBuildingByStreetName()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            Dictionary<String, SimpleBuildingOverviewModel> expectedResults = new Dictionary<string, SimpleBuildingOverviewModel>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                SimpleBuildingOverviewModel result = new SimpleBuildingOverviewModel
                {
                    CommercialUnits = buildingDataModel.CommercialUnits,
                    HouseholdUnits = buildingDataModel.HouseholdUnits,
                    Id = buildingDataModel.Id,
                    StreetName = buildingDataModel.StreetName
                };

                expectedResults.Add(buildingDataModel.StreetName, result);
                expectedResults.Add(buildingDataModel.StreetName.ToLower(), result);
                expectedResults.Add(buildingDataModel.StreetName.ToUpper(), result);
            }

            foreach (KeyValuePair<String, SimpleBuildingOverviewModel> item in expectedResults)
            {
                SimpleBuildingOverviewModel actual = await storage.GetBuildingByStreetName(item.Key);
                Assert.NotNull(actual);

                Assert.Equal(item.Value.CommercialUnits, actual.CommercialUnits);
                Assert.Equal(item.Value.HouseholdUnits, actual.HouseholdUnits);
                Assert.Equal(item.Value.Id, actual.Id);
                Assert.Equal(item.Value.StreetName, actual.StreetName);
            }
        }
    }
}
