﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetBuildingIdByFlatId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingIdByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingId = random.Next();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            buildingDataModel.Id = buildingId;
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(30, 100);

            List<FlatDataModel> flats = new List<FlatDataModel>();
            for (int i = 0; i < amount; i++)
            {
                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                flats.Add(flatDataModel);
            }

            storage.Flats.AddRange(flats);
            storage.SaveChanges();

            List<Int32> allIds = flats.Select(x => x.Id).ToList();

            Int32 existingId = allIds[random.Next(0, allIds.Count)];

            Int32 actualbuildingId = await storage.GetBuildingIdByFlatId(existingId);

            Assert.Equal(buildingId, actualbuildingId);
        }
    }
}
