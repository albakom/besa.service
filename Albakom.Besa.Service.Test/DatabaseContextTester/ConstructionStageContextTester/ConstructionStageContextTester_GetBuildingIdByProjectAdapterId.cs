﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingIdByProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingIdByProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetBuildingIdByProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            Dictionary<String, Int32> expectedResults = new Dictionary<string, int>(amount);

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(dataModel);
                storage.SaveChanges();

                expectedResults.Add(dataModel.ProjectId,dataModel.Id);
            }

            foreach (KeyValuePair<String,Int32> item in expectedResults)
            {
                Int32 result = await storage.GetBuildingIdByProjectAdapterId(item.Key);
                Assert.Equal(item.Value,result);
            }

        }
    }
}
