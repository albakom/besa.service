﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingIdsAndProjectAdapterIds : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingIdsAndProjectAdapterIds|ConstructionStageContextTester")]
        public async Task GetBuildingIdsAndProjectAdapterIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Dictionary<String,Int32> expected = new Dictionary<String, Int32>(amount);
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                expected.Add(buildingDataModel.ProjectId,buildingDataModel.Id);
            }

            IDictionary<String,Int32> actual = await storage.GetBuildingIdsAndProjectAdapterIds();

            Assert.NotNull(actual);
            Assert.Equal(expected.Count, actual.Count());

            foreach (KeyValuePair<String,Int32> actualItem in actual)
            {
                Assert.True(expected.ContainsKey(actualItem.Key));
                Assert.Equal(expected[actualItem.Key], actualItem.Value);
            }
        }
    }
}
