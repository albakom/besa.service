﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetBuildingProjectAdapterIdss : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingProjectAdapterIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            HashSet<String> expected = new HashSet<String>(amount);
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                expected.Add(buildingDataModel.ProjectId);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            IEnumerable<String> actual = await storage.GetBuildingProjectAdapterIds();

            Assert.NotNull(actual);
            Assert.Equal(expected.Count, actual.Count());

            foreach (String actualItem in actual)
            {
                Assert.Contains(actualItem, expected);
            }
        }
    }
}
