﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingStreetNameByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingStreetNameByFlatId|ConstructionStageContextTester")]
        public async Task GetBuildingStreetNameByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, String> expectedResults = new Dictionary<int, String>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 flatAmount = random.Next(3, 10);
                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = buildingDataModel;
                    storage.Flats.Add(flat);

                    storage.SaveChanges();
                    expectedResults.Add(flat.Id, buildingDataModel.StreetName);
                }
            }

            foreach (KeyValuePair<Int32, String> expectedResult in expectedResults)
            {
                String actual = await storage.GetBuildingStreetNameByFlatId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
