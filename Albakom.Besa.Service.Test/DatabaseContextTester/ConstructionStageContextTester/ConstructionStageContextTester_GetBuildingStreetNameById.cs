﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingStreetNameById : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingStreetNameById|ConstructionStageContextTester")]
        public async Task GetBuildingStreetNameById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, String> expectedResults = new Dictionary<int, String>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingDataModel.Id, buildingDataModel.StreetName);
            }

            foreach (KeyValuePair<Int32, String> expectedResult in expectedResults)
            {
                String actual = await storage.GetBuildingStreetNameById(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
