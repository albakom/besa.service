﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingsByBranchablet : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingsByBranchable()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(100, 1000);

            CabinetDataModel cabinet = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinet);
            storage.SaveChanges();

            List<BuildingDataModel> addedModels = new List<BuildingDataModel>();

            List<BuildingDataModel> items = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                if (random.NextDouble() > 0.5)
                {
                    dataModel.BranchableId = cabinet.Id;
                    addedModels.Add(dataModel);
                }
                items.Add(dataModel);
            }

            storage.Buildings.AddRange(items);
            storage.SaveChanges();

            IEnumerable<SimpleBuildingOverviewModel> result = await storage.GetBuildingsByBranchable(cabinet.Id);

            Assert.NotNull(result);

            Assert.Equal(addedModels.Count, result.Count());

            foreach (SimpleBuildingOverviewModel item in result)
            {
                BuildingDataModel expected = addedModels.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(expected);

                Assert.Equal(expected.Id, item.Id);
                Assert.Equal(expected.StreetName, item.StreetName);
                Assert.Equal(expected.HouseholdUnits, item.HouseholdUnits);
                Assert.Equal(expected.CommercialUnits, item.CommercialUnits);
            }
        }
    }
}
