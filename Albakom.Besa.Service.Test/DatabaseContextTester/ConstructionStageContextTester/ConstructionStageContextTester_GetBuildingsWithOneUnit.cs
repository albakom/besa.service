﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetBuildingsWithOneUnit : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingsWithOneUnit|ConstructionStageContextTester")]
        public async Task GetBuildingsWithOneUnit()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            List<BuildingDataModel> expectedBuldings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);

                Boolean oneUnit = random.NextDouble() > 0.5;
                if (oneUnit == true)
                {
                    if (random.NextDouble() > 0.5)
                    {
                        buildingDataModel.HouseholdUnits = 0;
                        buildingDataModel.CommercialUnits = 1;
                    }
                    else
                    {
                        buildingDataModel.HouseholdUnits = 1;
                        buildingDataModel.CommercialUnits = 0;
                    }
                }
                else
                {
                    buildingDataModel.HouseholdUnits += 1;
                    buildingDataModel.CommercialUnits += 1;
                }

                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                if (oneUnit == true)
                {

                    expectedBuldings.Add(buildingDataModel);
                }
            }

            IEnumerable<Int32> result = await storage.GetBuildingsWithOneUnit();

            Assert.NotNull(result);
            Assert.Equal(expectedBuldings.Count, result.Count());

            foreach (Int32 item in result)
            {
                BuildingDataModel expectedModel = expectedBuldings.FirstOrDefault(x => x.Id == item);

                Assert.NotNull(expectedModel);
            }
        }
    }
}
