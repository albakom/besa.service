﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetCableFromBranchbaleWithProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCableFromBranchbaleWithProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetCableFromBranchbaleWithProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12415);

            Int32 branchableAmount = random.Next(20, 30);

            Dictionary<Int32, Dictionary<String, Int32>> overallResult = new Dictionary<int, Dictionary<string, int>>();


            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);
            storage.SaveChanges();

            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                Int32 cableAmount = random.Next(10, 20);

                Dictionary<String, Int32> expectedResults = new Dictionary<String, Int32>();

                for (int j = 0; j < cableAmount; j++)
                {
                    FiberCableDataModel fiberCableData = base.GenerateFiberCableDataModel(random, typeDataModel);
                    fiberCableData.StartingAtBranchable = cabinetDataModel;
                    storage.FiberCables.Add(fiberCableData);
                    storage.SaveChanges();

                    expectedResults.Add(fiberCableData.ProjectAdapterId, fiberCableData.Id);
                }

                overallResult.Add(cabinetDataModel.Id, expectedResults);
            }

            foreach (KeyValuePair<Int32,Dictionary<String, Int32>> item in overallResult)
            {
                Dictionary<String, Int32> expectedResults = item.Value;

                IDictionary<String, Int32> actualResult = await storage.GetCableFromBranchbaleWithProjectAdapterId(item.Key);

                Assert.NotNull(actualResult);
                Assert.Equal(expectedResults.Count, actualResult.Count);

                foreach (KeyValuePair<String, Int32> actualItem in actualResult)
                {
                    Assert.True(expectedResults.ContainsKey(actualItem.Key));

                    Assert.Equal(expectedResults[actualItem.Key], actualItem.Value);
                }

            }
        }
    }
}
