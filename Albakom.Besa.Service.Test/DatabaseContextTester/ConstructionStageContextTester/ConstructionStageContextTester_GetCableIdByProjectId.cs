﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetCableIdByProjectId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCableIdByProjectId|ConstructionStageContextTester")]
        public async Task GetCableIdByProjectId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            Dictionary<String, Int32> epectedResults = new Dictionary<string, int>(amount);

            FiberCableTypeDataModel fiberCableTypeData = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeData);
            storage.SaveChanges();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeData);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                epectedResults.Add(dataModel.ProjectAdapterId, dataModel.Id);
            }

            foreach (KeyValuePair<String,Int32> item in epectedResults)
            {
                Int32 result = await storage.GetCableIdByProjectId(item.Key);
                Assert.Equal(item.Value, result);
            }
        }
    }
}
