﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetCableIdsByProjectIds : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCableIdsByProjectIds|ConstructionStageContextTester")]
        public async Task GetCableIdsByProjectIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(100, 300);

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            List<Int32> expectedIds = new List<int>();
            List<String> input = new List<string>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                Boolean searched = random.NextDouble() > 0.5;
                if(searched == true)
                {
                    input.Add(dataModel.ProjectAdapterId);
                    expectedIds.Add(dataModel.Id);
                }
            }

            IEnumerable<Int32> actualResult = await storage.GetCableIdsByProjectIds(input);

            Assert.NotNull(actualResult);
            Assert.Equal(expectedIds, actualResult);
        }
    }
}
