﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetCableTypeIdByProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCableTypeIdByProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetCableTypeIdByProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            Dictionary<String, Int32> expectedResults = new Dictionary<string, int>(amount);

            for (int i = 0; i < amount; i++)
            {
                FiberCableTypeDataModel dataModel = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(dataModel);
                storage.SaveChanges();

                expectedResults.Add(dataModel.AdapterId,dataModel.Id);
            }

            foreach (KeyValuePair<String,Int32> item in expectedResults)
            {
                Int32 result = await storage.GetCableTypeIdByProjectAdapterId(item.Key);
                Assert.Equal(item.Value,result);
            }

        }
    }
}
