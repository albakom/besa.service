﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetCablesWithFlatIDs : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCablesWithFlatIDs|ConstructionStageContextTester")]
        public async Task GetCablesWithFlatIDs()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);
            storage.SaveChanges();


            Int32 cableAmount = random.Next(10, 20);

            Dictionary<String, Int32> expectedResults = new Dictionary<String, Int32>();

            for (int j = 0; j < cableAmount; j++)
            {
                FiberCableDataModel fiberCableData = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(fiberCableData);
                storage.SaveChanges();

                Boolean addFlat = random.NextDouble() > 0.5;
                if (addFlat == true)
                {
                    BuildingDataModel buildingData = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingData);
                    storage.SaveChanges();

                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingData;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    expectedResults.Add(fiberCableData.ProjectAdapterId, flatDataModel.Id);

                    fiberCableData.StartingFlat = flatDataModel;
                    storage.SaveChanges();
                }
            }

            IDictionary<String, Int32> actualResult = await storage.GetCablesWithFlatIDs();
            Assert.NotNull(actualResult);
            Assert.Equal(expectedResults.Count, actualResult.Count);

            foreach (KeyValuePair<String, Int32> actualItem in actualResult)
            {
                Assert.True(expectedResults.ContainsKey(actualItem.Key));
                Assert.Equal(expectedResults[actualItem.Key], actualItem.Value);
            }
        }
    }
}
