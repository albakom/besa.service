﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetConstructionStageOverviewForInject : DatabaseTesterBase
    {
        private class JobInfo
        {
            public Int32 TotalJobs { get; set; }
            public Int32 BoundJobs { get; set; }
            public Int32 FinishedJobs { get; set; }
        }

        [Fact]
        public async Task GetConstructionStageOverviewForInject()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 construstionstageAmount = random.Next(3, 10);

            Dictionary<Int32, ConstructionStageOverviewForJobs> expectResult = new Dictionary<int, ConstructionStageOverviewForJobs>();
            Dictionary<Int32, List<Int32>> cabinetIds = new Dictionary<int, List<int>>();
            for (int i = 0; i < construstionstageAmount; i++)
            {
                ConstructionStageDataModel constructionStageData = base.GenerateConstructionStage(random);
                storage.ConstructionStages.Add(constructionStageData);
                storage.SaveChanges();

                cabinetIds.Add(constructionStageData.Id, new List<int>());

                List<SimpleCabinetOverviewModel> expectedBranchables = new List<SimpleCabinetOverviewModel>();

                Int32 branchableAmount = random.Next(3, 6);
                for (int j = 0; j < branchableAmount; j++)
                {
                    CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                    cabinetDataModel.ConstructionStage = constructionStageData;

                    storage.StreetCabintes.Add(cabinetDataModel);
                    storage.SaveChanges();

                    cabinetIds[constructionStageData.Id].Add(cabinetDataModel.Id);

                    expectedBranchables.Add(new SimpleCabinetOverviewModel
                    {
                        Id = cabinetDataModel.Id,
                        Name = cabinetDataModel.Name
                    });
                }

                expectResult.Add(constructionStageData.Id, new ConstructionStageOverviewForJobs
                {
                    Id = constructionStageData.Id,
                    DoingPercentage = 0.0,
                    Name = constructionStageData.Name,
                    Branchables = expectedBranchables,
                });
            }

            Int32 buildingAmout = random.Next(30, 100);

            Dictionary<Int32, JobInfo> expectedJobAmount = new Dictionary<int, JobInfo>();
            for (int i = 0; i < buildingAmout; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                Int32 stageId = expectResult.ElementAt(random.Next(0, expectResult.Count)).Key;
                buildingDataModel.BranchableId = cabinetIds[stageId][random.Next(0, cabinetIds[stageId].Count)];

                if(expectedJobAmount.ContainsKey(stageId) == false)
                {
                    expectedJobAmount.Add(stageId, new JobInfo());
                }

                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                InjectJobDataModel injectJob = new InjectJobDataModel
                {
                    Building = buildingDataModel,
                };

                expectedJobAmount[stageId].TotalJobs += 1;

                if (random.NextDouble() > 0.5)
                {
                    //FinishJob hinzufügen
                }

                storage.InjectJobs.Add(injectJob);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    CollectionJobDataModel collectionJobDataModel = base.GenerateCollectionJobDataModel(random);
                    collectionJobDataModel.Jobs.Add(injectJob);
                    storage.CollectionJobs.Add(collectionJobDataModel);
                    storage.SaveChanges();

                    expectedJobAmount[stageId].FinishedJobs += 1;

                }
            }

            IEnumerable<ConstructionStageOverviewForJobs> actual = await storage.GetConstructionStageOverviewForInject(false);

            Assert.NotNull(actual);
            Assert.Equal(expectResult.Count,actual.Count());

            foreach (ConstructionStageOverviewForJobs actualItem in actual)
            {
                Assert.NotNull(actualItem);

                Assert.True(expectResult.ContainsKey(actualItem.Id));

                ConstructionStageOverviewForJobs expectedItem = expectResult[actualItem.Id];

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);
            }
        }
    }
}
