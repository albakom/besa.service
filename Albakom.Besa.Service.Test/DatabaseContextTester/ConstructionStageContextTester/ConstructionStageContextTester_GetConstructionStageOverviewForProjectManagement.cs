﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetConstructionStageOverviewForProjectManagement : DatabaseTesterBase
    {
        [Fact]
        public async Task GetConstructionStageOverviewForProjectManagement()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageAmount = random.Next(3, 10);

            List<ConstructionStageDataModel> constructionStages = new List<ConstructionStageDataModel>();
            Dictionary<Int32, Int32> jobFullStages = new Dictionary<int, int>();
            for (int i = 0; i < constructionStageAmount; i++)
            {
                ConstructionStageDataModel constructionStageDataModel = base.GenerateConstructionStage(random);
                constructionStages.Add(constructionStageDataModel);

                storage.ConstructionStages.Add(constructionStageDataModel);
                storage.SaveChanges();

                Int32 branchableAmount = random.Next(3, 10);

                Boolean noJobs = random.NextDouble() > 0.5;
                jobFullStages.Add(constructionStageDataModel.Id, 0);

                for (int j = 0; j < branchableAmount; j++)
                {
                    CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                    constructionStageDataModel.Branchables.Add(cabinetDataModel);
                    cabinetDataModel.ConstructionStage = constructionStageDataModel;

                    storage.StreetCabintes.Add(cabinetDataModel);
                    storage.SaveChanges();

                    Int32 buildingAmount = random.Next(5, 15);

                    for (int k = 0; k < buildingAmount; k++)
                    {
                        BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                        cabinetDataModel.Buildings.Add(buildingDataModel);
                        buildingDataModel.Branchable = cabinetDataModel;

                        storage.Buildings.Add(buildingDataModel);

                        storage.SaveChanges();

                        if (noJobs == true)
                        {
                            jobFullStages[constructionStageDataModel.Id] += 1;
                            continue;
                        }

                        if (random.NextDouble() > 0.5)
                        {
                            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                            {
                                Building = buildingDataModel,
                            };

                            buildingDataModel.BranchOffJob = jobDataModel;

                            storage.BranchOffJobs.Add(jobDataModel);
                            storage.SaveChanges();
                        }
                        else
                        {
                            jobFullStages[constructionStageDataModel.Id] += 1;
                        }
                    }
                }
            }

            IEnumerable<ConstructionStageForProjectManagementOverviewModel> result = await storage.GetConstructionStageOverviewForProjectManagement();

            Assert.NotNull(result);

            Assert.Equal(jobFullStages.Count, result.Count());

            foreach (ConstructionStageForProjectManagementOverviewModel item in result)
            {
                ConstructionStageDataModel expected = constructionStages.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(expected);

                Assert.Equal(expected.Id, item.Id);
                Assert.Equal(expected.Name, item.Name);

                Assert.True(jobFullStages.ContainsKey(item.Id));

                Assert.Equal(jobFullStages[item.Id], item.BuildingsWithoutBranchOffJobs);
            }
        }
    }
}
