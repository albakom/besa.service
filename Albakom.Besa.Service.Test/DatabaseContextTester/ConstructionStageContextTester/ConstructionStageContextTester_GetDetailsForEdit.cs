﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetDetailsForEdit : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetDetailsForEdit|ConstructionStageContextTester")]
        public async Task GetDetailsForEdit()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageAmount = random.Next(3, 10);

            Dictionary<Int32, ConstructionStageDetailsForEditModel> expectedResult = new Dictionary<int, ConstructionStageDetailsForEditModel>(constructionStageAmount);
            for (int i = 0; i < constructionStageAmount; i++)
            {
                Int32 cabinentAmount = random.Next(3, 10);
                ConstructionStageDataModel constructionStageDataModel = base.GenerateConstructionStage(random);
                storage.ConstructionStages.Add(constructionStageDataModel);
                storage.SaveChanges();

                ConstructionStageDetailsForEditModel expectedModel = new ConstructionStageDetailsForEditModel { Name = constructionStageDataModel.Name, Id = constructionStageDataModel.Id };

                List<SimpleCabinetOverviewModel> branchables = new List<SimpleCabinetOverviewModel>(cabinentAmount);
                for (int j = 0; j < cabinentAmount; j++)
                {
                    CabinetDataModel cabinet = base.GenerateCabinet(random);
                    cabinet.ConstructionStage = constructionStageDataModel;
                    storage.Branchables.Add(cabinet);
                    storage.SaveChanges();

                    Int32 buildindAmount = random.Next(10, 30);
                    List<SimpleBuildingOverviewModel> buildings = new List<SimpleBuildingOverviewModel>(buildindAmount);

                    for (int k = 0; k < buildindAmount; k++)
                    {
                        BuildingDataModel building = base.GenerateBuilding(random);
                        building.Branchable = cabinet;
                        storage.Buildings.Add(building);
                        storage.SaveChanges();

                        buildings.Add(new SimpleBuildingOverviewModel
                        {
                            Id = building.Id,
                            CommercialUnits = building.CommercialUnits,
                            HouseholdUnits = building.HouseholdUnits,
                            StreetName = building.StreetName
                        });
                    }

                    branchables.Add(new SimpleCabinetOverviewModel { Id = cabinet.Id, Name = cabinet.Name });
                    expectedModel.Buildings.Add(cabinet.Id, buildings);
                }

                expectedModel.Branchables = branchables;
                expectedResult.Add(constructionStageDataModel.Id, expectedModel);
            }

            foreach (KeyValuePair<Int32,ConstructionStageDetailsForEditModel> item in expectedResult)
            {
                ConstructionStageDetailsForEditModel actual = await storage.GetDetailsForEdit(item.Key);

                Assert.NotNull(actual);

                Assert.Equal(item.Value.Id, actual.Id);
                Assert.Equal(item.Value.Name, actual.Name);

                Assert.NotNull(actual.Branchables);
                Assert.Equal(item.Value.Branchables.Count(), actual.Branchables.Count());

                foreach (SimpleCabinetOverviewModel expectedBranchable in item.Value.Branchables)
                {
                    SimpleCabinetOverviewModel acutalItem = actual.Branchables.FirstOrDefault(x => x.Id == expectedBranchable.Id);
                    Assert.NotNull(acutalItem);

                    Assert.Equal(expectedBranchable.Id, acutalItem.Id);
                    Assert.Equal(expectedBranchable.Name, acutalItem.Name);
                }

                Assert.NotNull(actual.Buildings);
                Assert.Equal(item.Value.Buildings.Count, actual.Buildings.Count);

                foreach (KeyValuePair<Int32,IEnumerable<SimpleBuildingOverviewModel>> actualItem in actual.Buildings)
                {
                    Assert.True(item.Value.Buildings.ContainsKey(actualItem.Key));
                    IEnumerable<SimpleBuildingOverviewModel> expectedItems = item.Value.Buildings[actualItem.Key];

                    Assert.Equal(expectedItems.Count(), actualItem.Value.Count());

                    foreach (SimpleBuildingOverviewModel expectedBuilding in expectedItems)
                    {
                        SimpleBuildingOverviewModel actualBuilding = actualItem.Value.FirstOrDefault(x => x.Id == expectedBuilding.Id);
                        Assert.NotNull(actualBuilding);

                        Assert.Equal(expectedBuilding.Id, actualBuilding.Id);
                        Assert.Equal(expectedBuilding.StreetName, actualBuilding.StreetName);
                        Assert.Equal(expectedBuilding.HouseholdUnits, actualBuilding.HouseholdUnits);
                        Assert.Equal(expectedBuilding.CommercialUnits, actualBuilding.CommercialUnits);
                    }
                }
            }
        }
    }
}
