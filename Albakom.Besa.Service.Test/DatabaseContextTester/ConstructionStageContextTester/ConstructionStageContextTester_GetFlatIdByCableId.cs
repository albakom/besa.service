﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetFlatIdByCableId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetFlatIdByCableId|ConstructionStageContextTester")]
        public async Task GetFlatIdByCableId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 50);

            FiberCableTypeDataModel typeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            Dictionary<Int32, Int32?> flatMapper = new Dictionary<int, int?>();
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32? flatId = null;
                if(random.NextDouble() > 0.5)
                {
                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    dataModel.StartingFlat = flatDataModel;
                    storage.SaveChanges();

                    flatId = flatDataModel.Id;
                }

                flatMapper.Add(dataModel.Id, flatId);
            }

            foreach (KeyValuePair<Int32,Int32?> expectedItem in flatMapper)
            {
                Int32? actual = await storage.GetFlatIdByCableId(expectedItem.Key);
                Assert.Equal(expectedItem.Value, actual);
            }
        }
    }
}
