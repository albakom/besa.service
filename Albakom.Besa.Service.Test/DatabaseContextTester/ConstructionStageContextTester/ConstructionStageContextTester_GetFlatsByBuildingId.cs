﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetFlatsByBuildingId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFlatsByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingAmount = random.Next(3, 10);
            Dictionary<BuildingDataModel, List<FlatDataModel>> expected = new Dictionary<BuildingDataModel, List<FlatDataModel>>();
            for (int j = 0; j < buildingAmount; j++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 amount = random.Next(5, 30);

                List<FlatDataModel> list = new List<FlatDataModel>();
                for (int i = 0; i < amount; i++)
                {
                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    buildingDataModel.Flats.Add(flatDataModel);

                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();
                }
            }

            foreach (KeyValuePair<BuildingDataModel, List<FlatDataModel>> item in expected)
            {
                IEnumerable<SimpleFlatOverviewModel> result = await storage.GetFlatsByBuildingId(item.Key.Id);

                Assert.NotNull(result);
                Assert.Equal(item.Value.Count, result.Count());

                foreach (SimpleFlatOverviewModel actualItem in result)
                {
                    Assert.NotNull(actualItem);
                    FlatDataModel expetecItem = item.Value.FirstOrDefault(x => x.Id == actualItem.Id);
                    Assert.NotNull(expetecItem);

                    Assert.Equal(expetecItem.BuildingId, actualItem.BuildingId);
                    Assert.Equal(expetecItem.Description, actualItem.Description);
                    Assert.Equal(expetecItem.Floor, actualItem.Floor);
                    Assert.Equal(expetecItem.Number, actualItem.Number);
                }
            }
        }
    }
}
