﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetMainCableWithJobAssocatiation : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMainCableWithJobAssocatiation|ConstructionStageContextTester")]
        public async Task GetMainCableWithJobAssocatiation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);

            Dictionary<Int32, MainCableOverviewModel> expectedResults = new Dictionary<int, MainCableOverviewModel>();
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                Boolean jobExists = random.NextDouble() > 0.5;
                if(jobExists == true)
                {
                    SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel
                    {
                        Cable = dataModel,
                    };

                    storage.SpliceODFJobs.Add(jobDataModel);
                    storage.SaveChanges();
                }



                Boolean createPop = random.NextDouble() > 0.75;
                if(createPop == true)
                {
                    PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
                    storage.Pops.Add(poPDataModel);
                    storage.SaveChanges();

                    dataModel.StartingAtPop = poPDataModel;
                    storage.SaveChanges();

                    MainCableOverviewModel expected = new MainCableOverviewModel
                    {
                        Id = dataModel.Id,
                        Name = dataModel.Name,
                        JobExists = jobExists,
                    };

                    expectedResults.Add(dataModel.Id, expected);
                }
            }

            IEnumerable<MainCableOverviewModel> actual = await storage.GetMainCableWithJobAssocatiation();
            Assert.NotNull(actual);
            Assert.Equal(expectedResults.Count, actual.Count());

            foreach (MainCableOverviewModel actualItem in actual)
            {
                Assert.NotNull(actualItem);
                Assert.True(expectedResults.ContainsKey(actualItem.Id));

                MainCableOverviewModel expectedItem = expectedResults[actualItem.Id];
                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);
                Assert.Equal(expectedItem.JobExists, actualItem.JobExists);
            }
        }
    }
}
