﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_GetOverviewForCilivWork : DatabaseTesterBase
    {
        [Fact]
        public async Task GetOverviewForCilivWork()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageAmount = random.Next(3, 10);

            for (int i = 0; i < constructionStageAmount; i++)
            {
                Int32 cabinentAmount = random.Next(3, 10);
                ConstructionStageDataModel constructionStageDataModel = base.GenerateConstructionStage(random);

                for (int j = 0; j < cabinentAmount; j++)
                {
                    CabinetDataModel cabinet = base.GenerateCabinet(random);
                    constructionStageDataModel.Branchables.Add(cabinet);

                    Int32 buildindAmount = random.Next(10, 30);

                    for (int k = 0; k < buildindAmount; k++)
                    {
                        BuildingDataModel building = base.GenerateBuilding(random);
                        cabinet.Buildings.Add(building);
                    }

                }
            }

            storage.SaveChanges();

            Dictionary<Int32, ConstructionStageDataModel> expected = storage.ConstructionStages.Include(x => x.Branchables).ThenInclude(x => x.Buildings).ToDictionary(x => x.Id, x => x);
            IEnumerable<ConstructionStageOverviewForJobs> result = await storage.GetConstructionStageOverviewForCilivWork(false);

            Assert.NotNull(result);

            foreach (ConstructionStageOverviewForJobs actualItem in result)
            {
                Assert.NotNull(actualItem);

                Assert.True(expected.ContainsKey(actualItem.Id));

                ConstructionStageDataModel expectedItem = expected[actualItem.Id];

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);

                Assert.NotNull(actualItem.Branchables);

                Assert.Equal(expectedItem.Branchables.Count, actualItem.Branchables.Count());

                foreach (SimpleCabinetOverviewModel cabinet in actualItem.Branchables)
                {
                    CabinetDataModel dataModel = expectedItem.Branchables.OfType<CabinetDataModel>().FirstOrDefault(x => x.Id == cabinet.Id);

                    Assert.NotNull(dataModel);

                    Assert.Equal(dataModel.Id, cabinet.Id);
                    Assert.Equal(dataModel.Name, cabinet.Name);
                }
            }
        }
    }
}
