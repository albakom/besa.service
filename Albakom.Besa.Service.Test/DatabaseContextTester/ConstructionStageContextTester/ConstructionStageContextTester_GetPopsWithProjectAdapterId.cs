﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetPopsWithProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetPopsWithProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetPopsWithProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            Dictionary<String, Int32> expectedResults = new Dictionary<string, int>(popAmount);

            for (int i = 0; i < popAmount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);

                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                expectedResults.Add(dataModel.ProjectAdapterId, dataModel.Id);
            }

            IDictionary<String, Int32> actual = await storage.GetPopsWithProjectAdapterId();
            Assert.NotNull(actual);
            Assert.Equal(expectedResults.Count, actual.Count);

            foreach (KeyValuePair<String,Int32> actualItem in actual)
            {
                Assert.True(expectedResults.ContainsKey(actualItem.Key));

                Assert.Equal(expectedResults[actualItem.Key], actualItem.Value);
            }
        }

        [Fact(DisplayName = "GetPopsWithProjectAdapterId_Partial|ConstructionStageContextTester")]
        public async Task GetPopsWithProjectAdapterId_Partial()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Dictionary<String, Int32> adapterIdMapper = new Dictionary<string, int>();
            for (int i = 0; i < amount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                adapterIdMapper.Add(dataModel.ProjectAdapterId, dataModel.Id);
            }

            List<Int32> allIds = adapterIdMapper.Values.ToList();

            Int32 skipAmount = random.Next(0, amount - 1);
            Int32 takeAmount = amount - skipAmount;

            List<Int32> subSet = allIds.Skip(skipAmount).Take(takeAmount).ToList();

            IDictionary<String, Int32> actual = await storage.GetPopsWithProjectAdapterId(subSet);
            Assert.NotNull(actual);
            Assert.Equal(subSet.Count, actual.Count);

            foreach (KeyValuePair<String, Int32> actualItem in actual)
            {
                Assert.True(adapterIdMapper.ContainsKey(actualItem.Key));
                Assert.Equal(adapterIdMapper[actualItem.Key], actualItem.Value);

                Assert.Contains(actualItem.Value, subSet);
            }
        }
    }
}
