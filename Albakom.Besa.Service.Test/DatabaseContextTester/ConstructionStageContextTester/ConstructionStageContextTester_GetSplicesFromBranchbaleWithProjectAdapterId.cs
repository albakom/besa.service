﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_GetSplicesFromBranchbaleWithProjectAdapterId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetSplicesFromBranchbaleWithProjectAdapterId|ConstructionStageContextTester")]
        public async Task GetSplicesFromBranchbaleWithProjectAdapterId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12345);

            Int32 branchableAmount = random.Next(5, 10);

            Dictionary<Int32, Dictionary<String, Int32>> overallResult = new Dictionary<int, Dictionary<string, int>>();


            FiberCableTypeDataModel typeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(typeDataModel);
            storage.SaveChanges();

            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                Int32 spliceAmount = random.Next(5, 10);

                Dictionary<String, Int32> expectedResults = new Dictionary<String, Int32>();

                for (int j = 0; j < spliceAmount; j++)
                {
                    FiberCableDataModel firstFiberCableData = base.GenerateFiberCableDataModel(random, typeDataModel);
                    firstFiberCableData.StartingAtBranchable = cabinetDataModel;
                    storage.FiberCables.Add(firstFiberCableData);
                    storage.SaveChanges();

                    BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingDataModel);
                    storage.SaveChanges();

                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    FiberCableDataModel secondFiberCableData = base.GenerateFiberCableDataModel(random, typeDataModel);
                    secondFiberCableData.StartingFlat = flatDataModel;
                    secondFiberCableData.StartingAtBranchable = cabinetDataModel;
                    storage.FiberCables.Add(secondFiberCableData);
                    storage.SaveChanges();

                    FiberDataModel firstFiber = base.GenerateFiberDataModel(random, firstFiberCableData);
                    FiberDataModel secondFiber = base.GenerateFiberDataModel(random, secondFiberCableData);
                    storage.Fibers.AddRange(firstFiber, secondFiber);
                    storage.SaveChanges();

                    FiberSpliceDataModel spliceDataModel = base.GenerateFiberSpliceDataModel(random, firstFiber, secondFiber);
                    storage.Splices.Add(spliceDataModel);
                    storage.SaveChanges();
                    
                    expectedResults.Add(spliceDataModel.ProjectAdapterId, spliceDataModel.Id);

                }

                overallResult.Add(cabinetDataModel.Id, expectedResults);
            }

            foreach (KeyValuePair<Int32,Dictionary<String, Int32>> item in overallResult)
            {
                Dictionary<String, Int32> expectedResults = item.Value;

                IDictionary<String, Int32> actualResult = await storage.GetSplicesFromBranchbaleWithProjectAdapterId(item.Key);

                Assert.NotNull(actualResult);
                Assert.Equal(expectedResults.Count, actualResult.Count);

                foreach (KeyValuePair<String, Int32> actualItem in actualResult)
                {
                    Assert.True(expectedResults.ContainsKey(actualItem.Key));

                    Assert.Equal(expectedResults[actualItem.Key], actualItem.Value);
                }

            }
        }
    }
}
