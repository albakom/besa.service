﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_SearchCabinents : DatabaseTesterBase
    {
        [Fact]
        public async Task SearchCabinets_WithAmount()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> items = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                items.Add(dataModel);
            }

            storage.StreetCabintes.AddRange(items);
            storage.SaveChanges();

            Dictionary<Int32, CabinetDataModel> itemsAsDict = items.ToDictionary(x => x.Id, x => x);

            String searchTerm = items[0].Name.Substring(0, 2);
            Int32 searchAmount = random.Next(0, amount - 1);

            IEnumerable<SimpleCabinetOverviewModel> result = await storage.SearchCabinets(searchTerm, searchAmount);

            Assert.NotNull(result);
            Assert.False(result.Count() == 0);
            Assert.True(result.Count() <= searchAmount);

            String normalizedQury = searchTerm.ToLower();
            foreach (SimpleCabinetOverviewModel item in result)
            {
                Assert.True(itemsAsDict.ContainsKey(item.Id));

                CabinetDataModel dataModel = itemsAsDict[item.Id];

                Assert.Equal(dataModel.Name, item.Name);
                Assert.Equal(dataModel.Id, item.Id);
                String normalizedName = dataModel.Name.ToLower();
                Assert.True(normalizedName.IndexOf(normalizedQury) >= 0);
            }
        }

        [Fact]
        public async Task SearchCabinets_WithoutAmount()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> items = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                items.Add(dataModel);
            }

            storage.StreetCabintes.AddRange(items);
            storage.SaveChanges();

            Dictionary<Int32, CabinetDataModel> itemsAsDict = items.ToDictionary(x => x.Id, x => x);

            String searchTerm = items[0].Name.Substring(0, 2);
            Int32 searchAmount = random.Next(0, amount - 1);

            IEnumerable<SimpleCabinetOverviewModel> result = await storage.SearchCabinets(searchTerm, searchAmount);

            Assert.NotNull(result);
            Assert.False(result.Count() == 0);
            Assert.True(result.Count() == searchAmount);

            String normalizedQury = searchTerm.ToLower();
            foreach (SimpleCabinetOverviewModel item in result)
            {
                Assert.True(itemsAsDict.ContainsKey(item.Id));

                CabinetDataModel dataModel = itemsAsDict[item.Id];

                Assert.Equal(dataModel.Name, item.Name);
                Assert.Equal(dataModel.Id, item.Id);
                String normalizedName = dataModel.Name.ToLower();
                Assert.True(normalizedName.IndexOf(normalizedQury) >= 0);
            }
        }


        [Fact]
        public async Task SearchCabinets_WithBuildings()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            List<CabinetDataModel> items = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel dataModel = base.GenerateCabinet(random);
                items.Add(dataModel);

                Int32 buildingAmount = random.Next(3, 16);
                for (int j = 0; j < buildingAmount; j++)
                {
                    BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                    dataModel.Buildings.Add(buildingDataModel);
                }
            }

            storage.StreetCabintes.AddRange(items);
            storage.SaveChanges();

            Dictionary<Int32, CabinetDataModel> itemsAsDict = items.ToDictionary(x => x.Id, x => x);

            String searchTerm = items[0].Buildings.ElementAt(0).StreetName.Substring(1, 3);
            Int32 searchAmount = random.Next(0, amount - 1);

            IEnumerable<SimpleCabinetOverviewModel> result = await storage.SearchCabinets(searchTerm, searchAmount);

            Assert.NotNull(result);
            Assert.False(result.Count() == 0);
            Assert.True(result.Count() <= searchAmount);

            String normalizedQury = searchTerm.ToLower();
            foreach (SimpleCabinetOverviewModel item in result)
            {
                Assert.True(itemsAsDict.ContainsKey(item.Id));

                CabinetDataModel dataModel = itemsAsDict[item.Id];

                Assert.Equal(dataModel.Name, item.Name);
                Assert.Equal(dataModel.Id, item.Id);

                List<BuildingDataModel> buildings = storage.Buildings.Where(x => x.BranchableId == item.Id).ToList();

                Boolean found = false;
                foreach (BuildingDataModel building in buildings)
                {
                    String normalizedName = building.StreetName.ToLower();
                    if(normalizedName.IndexOf(normalizedQury) >= 0)
                    {
                        found = true;
                        break;
                    }
                }

                Assert.True(found);
            }
        }

    }
}
