﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_SearchStreets : DatabaseTesterBase
    {
        [Fact]
        public async Task SearchStreets_WithAmount()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> items = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                items.Add(dataModel);
            }

            storage.Buildings.AddRange(items);
            storage.SaveChanges();

            Dictionary<Int32, BuildingDataModel> itemsAsDict = items.ToDictionary(x => x.Id, x => x);

            String searchTerm = items[0].StreetName.Substring(2, 5);
            Int32 searchAmount = random.Next(0,amount-1);

            IEnumerable<SimpleBuildingOverviewModel> result = await storage.SearchStreets(searchTerm, searchAmount);

            Assert.NotNull(result);
            Assert.False(result.Count() == 0);
            Assert.True(result.Count() <= searchAmount);

            String normalizedQury = searchTerm.ToLower();
            foreach (SimpleBuildingOverviewModel item in result)
            {
                Assert.True(itemsAsDict.ContainsKey(item.Id));

                BuildingDataModel dataModel = itemsAsDict[item.Id];

                Assert.Equal(dataModel.StreetName, item.StreetName);
                Assert.Equal(dataModel.HouseholdUnits, item.HouseholdUnits);
                Assert.Equal(dataModel.CommercialUnits, item.CommercialUnits);

                String normalizedName = dataModel.StreetName.ToLower();
                Assert.True(normalizedName.IndexOf(normalizedQury) >= 0);
            }
        }

        [Fact]
        public async Task SearchStreets_WithoutAmount()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> items = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                items.Add(dataModel);
            }

            storage.Buildings.AddRange(items);
            storage.SaveChanges();

            Dictionary<Int32, BuildingDataModel> itemsAsDict = items.ToDictionary(x => x.Id, x => x);


            String searchTerm = items[0].StreetName.Substring(2, 5);

            IEnumerable<SimpleBuildingOverviewModel> result = await storage.SearchStreets(searchTerm, null);

            Assert.NotNull(result);
            Assert.True(result.Count() == amount);

            String normalizedQury = searchTerm.ToLower();
            foreach (SimpleBuildingOverviewModel item in result)
            {
                Assert.True(itemsAsDict.ContainsKey(item.Id));

                BuildingDataModel dataModel = itemsAsDict[item.Id];

                Assert.Equal(dataModel.StreetName, item.StreetName);
                Assert.Equal(dataModel.HouseholdUnits, item.HouseholdUnits);
                Assert.Equal(dataModel.CommercialUnits, item.CommercialUnits);

                String normalizedName = dataModel.StreetName.ToLower();
                Assert.True(normalizedName.IndexOf(normalizedQury) >= 0);
            }

        }

    }
}
