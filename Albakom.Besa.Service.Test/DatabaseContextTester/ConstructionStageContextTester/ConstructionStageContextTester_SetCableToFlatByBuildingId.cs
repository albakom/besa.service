﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_SetCableToFlatByBuildingId : DatabaseTesterBase
    {
        [Fact(DisplayName = "SetCableToFlatByBuildingId|ConstructionStageContextTester")]
        public async Task SetCableToFlatByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(cableTypeDataModel);
            storage.SaveChanges();

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();
            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                buildingData.HouseholdUnits = 0;
                buildingData.CommercialUnits = 0;
                if(random.NextDouble() > 0.5)
                {
                    buildingData.HouseholdUnits = 1;
                }
                else
                {
                    buildingData.CommercialUnits = 1;
                }

                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = buildingData;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, cableTypeDataModel);
                cableDataModel.ForBuilding = buildingData;
                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingData.Id, true);
            }

            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                buildingData.HouseholdUnits = 0;
                buildingData.CommercialUnits = 0;
                if (random.NextDouble() > 0.5)
                {
                    buildingData.HouseholdUnits = 1;
                }
                else
                {
                    buildingData.CommercialUnits = 1;
                }

                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                for (int i = 0; i < 2; i++)
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = buildingData;
                    storage.Flats.Add(flat);
                    storage.SaveChanges();
                }


                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, cableTypeDataModel);
                cableDataModel.ForBuilding = buildingData;
                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingData.Id, false);
            }

            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                buildingData.HouseholdUnits = 0;
                buildingData.CommercialUnits = 0;
                if (random.NextDouble() > 0.5)
                {
                    buildingData.HouseholdUnits = 1;
                }
                else
                {
                    buildingData.CommercialUnits = 1;
                }

                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = buildingData;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, cableTypeDataModel);
                cableDataModel.ForBuilding = buildingData;
                cableDataModel.StartingFlat = flat;
                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingData.Id, false);
            }

            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                buildingData.HouseholdUnits = 0;
                buildingData.CommercialUnits = 0;
                if (random.NextDouble() > 0.5)
                {
                    buildingData.HouseholdUnits = 1;
                }
                else
                {
                    buildingData.CommercialUnits = 1;
                }

                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = buildingData;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                expectedResults.Add(buildingData.Id, false);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedResult in expectedResults)
            {
                Boolean actual = await storage.SetCableToFlatByBuildingId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
