﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateBranchableCableRelation : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateBranchableCableRelation|ConstructionStageContextTester")]
        public async Task UpdateBranchableCableRelation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(100, 300);

            BranchableDataModel branchable = base.GenerateCabinet(random);
            storage.Branchables.Add(branchable);
            storage.SaveChanges();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            List<Int32> idsToAddToPoP = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                Boolean searched = random.NextDouble() > 0.5;
                if(searched == true)
                {
                    Boolean acutal = await storage.UpdateBranchableCableRelation(branchable.Id, dataModel.Id);
                    Assert.True(acutal);

                    FiberCableDataModel realDataModel = storage.FiberCables.FirstOrDefault(x => x.Id == dataModel.Id);
                    Assert.NotNull(realDataModel);

                    Assert.True(realDataModel.MainCableForBranchableId.HasValue);
                    Assert.Equal(realDataModel.MainCableForBranchableId.Value, branchable.Id);

                    BranchableDataModel realBranchableModel = storage.Branchables.FirstOrDefault(x => x.Id == branchable.Id);
                    Assert.NotNull(realBranchableModel);
                    Assert.NotNull(realBranchableModel.MainCable);
                    Assert.Equal(dataModel.Id, realBranchableModel.MainCable.Id);
                }
            }
        }
    }
}
