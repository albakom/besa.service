﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateBranchableFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateBranchableFromAdapter|ConstructionStageContextTester")]
        public async Task UpdateBranchableFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<CabinetDataModel> cabinets = new List<CabinetDataModel>();
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel buildingDataModel = base.GenerateCabinet(random);
                cabinets.Add(buildingDataModel);
            }

            storage.StreetCabintes.AddRange(cabinets);
            storage.SaveChanges();

            foreach (CabinetDataModel item in cabinets)
            {
                ProjectAdapterBranchableModel udateModel = base.ProjectAdapterBranchableModel(random);
                udateModel.AdapterId = item.ProjectId;

                Boolean result = await storage.UpdateBranchableFromAdapter(item.Id,udateModel);
                Assert.True(result);

                CabinetDataModel cabinetDataModel = storage.StreetCabintes.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(cabinetDataModel);

                Assert.Equal(item.ProjectId, cabinetDataModel.ProjectId);
                Assert.Equal(udateModel.Name, item.Name);
                Assert.Equal(udateModel.Name.Trim().ToLower(), cabinetDataModel.NormalizedName);
                Assert.Equal(udateModel.Coordinate.Latitude, item.Latitude);
                Assert.Equal(udateModel.Coordinate.Longitude, item.Longitude);
            }
        }
    }
}
