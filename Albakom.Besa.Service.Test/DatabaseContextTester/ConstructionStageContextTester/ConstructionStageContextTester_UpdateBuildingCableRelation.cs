﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateBuildingCableRelation : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateBuildingCableRelation|ConstructionStageContextTester")]
        public async Task UpdateBuildingCableRelation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(5, 10);
            Int32 buildingAmount = random.Next(50, 100);

            List<Int32> existingIds = new List<int>();
            List<Int32> buildingIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                existingIds.Add(dataModel.Id);
            }

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                buildingIds.Add(building.Id);
            }

            Dictionary<Int32, ProjectAdapterCableModel> expected = new Dictionary<int, ProjectAdapterCableModel>(amount);

            foreach (Int32 itemId in existingIds)
            {
                Int32 buildingId = buildingIds[random.Next(0, buildingIds.Count)];
                Boolean result = await storage.UpdateBuildingCableRelation(itemId, buildingId);
                Assert.True(result);

                FiberCableDataModel dataModel = storage.FiberCables.FirstOrDefault(x => x.Id == itemId);
                Assert.NotNull(dataModel);

                dataModel.BuildingId = buildingId;

                BuildingDataModel building = storage.Buildings.FirstOrDefault(x => x.Id == buildingId);
                Assert.NotNull(building);
                Assert.NotNull(building.FiberCable);
            }
        }
    }
}
