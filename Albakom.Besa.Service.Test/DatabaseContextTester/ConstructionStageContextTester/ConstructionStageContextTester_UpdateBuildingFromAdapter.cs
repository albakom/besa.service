﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateBuildingFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateBuildingFromAdapter|ConstructionStageContextTester")]
        public async Task UpdateBuildingFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            CabinetDataModel mainBranchable = base.GenerateCabinet(random);
            CabinetDataModel secondBranchable = base.GenerateCabinet(random);
            storage.StreetCabintes.AddRange(mainBranchable,secondBranchable);
            storage.SaveChanges();

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildingDataModel.Branchable = mainBranchable;
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            foreach (BuildingDataModel item in buildings)
            {
                ProjectAdapterBuildingModel udateModel = base.GenerateProjectAdapterBuildingModel(random);
                udateModel.AdapterId = item.ProjectId;

                Boolean branchableChanged = random.NextDouble() > 0.5;
                Int32 branchableId = mainBranchable.Id;
                if(branchableChanged == true)
                {
                    branchableId = secondBranchable.Id;
                }

                Boolean result = await storage.UpdateBuildingFromAdapter(item.Id,udateModel, branchableId);
                Assert.Equal(branchableChanged, result);

                BuildingDataModel buildingDataModel = storage.Buildings.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(buildingDataModel);

                Assert.Equal(udateModel.AdapterId, item.ProjectId);
                Assert.Equal(udateModel.CommercialUnits, item.CommercialUnits);
                Assert.Equal(udateModel.HouseholdUnits, item.HouseholdUnits);
                Assert.Equal(udateModel.Street, item.StreetName);

                Assert.False(String.IsNullOrEmpty(item.NormalizedStreetName));
                Assert.Equal(udateModel.Coordinate.Latitude, item.Latitude);
                Assert.Equal(udateModel.Coordinate.Longitude, item.Longitude);

                Assert.Equal(udateModel.HouseConnectionColor.Name, item.HouseConnenctionColorName);
                Assert.Equal(udateModel.HouseConnectionColor.RGBHexValue, item.HouseConnenctionHexValue);

                Assert.Equal(udateModel.MicroductColor.Name, item.MicroDuctColorName);
                Assert.Equal(udateModel.MicroductColor.RGBHexValue, item.MicroDuctHexValue);

                Assert.Equal(udateModel.Coordinate.Longitude, item.Longitude);

                Assert.Equal(udateModel.Street, buildingDataModel.StreetName);
                Assert.NotNull(buildingDataModel.NormalizedStreetName);

                Assert.Equal(udateModel.Street.Trim().ToLower(), buildingDataModel.NormalizedStreetName);
            }
        }
    }
}
