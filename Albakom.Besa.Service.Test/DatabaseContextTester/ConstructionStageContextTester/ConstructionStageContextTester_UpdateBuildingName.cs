﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_UpdateBuildingName : DatabaseTesterBase
    {
        [Fact]
        public async Task UpdateBuildingName()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            foreach (BuildingDataModel item in buildings)
            {
                ProjectAdapterUpdateBuildingNameModel updateBuldingModel = new ProjectAdapterUpdateBuildingNameModel
                {
                    AdapterId = item.ProjectId,
                    Name = $"Toller neuer Name mit der Nummer {random.Next()}",
                };

                Boolean result = await storage.UpdateBuildingName(updateBuldingModel);

                Assert.True(result);

                BuildingDataModel buildingDataModel = storage.Buildings.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(buildingDataModel);

                Assert.Equal(updateBuldingModel.Name, buildingDataModel.StreetName);
                Assert.Equal(updateBuldingModel.Name.Trim().ToLower(), buildingDataModel.NormalizedStreetName);
            }
        }
    }
}
