﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_UpdateBuildingWithCableInfo : DatabaseTesterBase
    {
        [Fact]
        public async Task UpdateBuildingWithCableInfo()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 1000);

            Int32 cableTypeAmount = random.Next(3, 10);
            List<FiberCableTypeDataModel> cableTypes = new List<FiberCableTypeDataModel>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                FiberCableTypeDataModel dataModel = base.GenerateFiberCableTypeDataModel(random);
                cableTypes.Add(dataModel);
            }

            storage.CableTypes.AddRange(cableTypes);
            storage.SaveChanges();

            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            foreach (BuildingDataModel item in buildings)
            {
                FiberCableTypeDataModel cable = cableTypes[random.Next(0, cableTypes.Count)];
                ProjectAdapterUpdateBuldingCableModel updateBuldingModel = new ProjectAdapterUpdateBuldingCableModel
                {
                    AdapterId = item.ProjectId,
                    Length = random.Next(30, 500) + random.NextDouble(),
                    AdapterCableTypeId = cable.AdapterId,
                };

                Boolean result = await storage.UpdateBuildingWithCableInfo(updateBuldingModel);

                Assert.True(result);

                BuildingDataModel buildingDataModel =  storage.Buildings.FirstOrDefault(x => x.Id == item.Id);
                Assert.NotNull(buildingDataModel);

                Assert.Equal(updateBuldingModel.Length, buildingDataModel.ConnenctionLength);
                Assert.True(buildingDataModel.CableTypeId.HasValue);
                Assert.Equal(cable.Id, buildingDataModel.CableTypeId.Value);
            }
        }
    }
}
