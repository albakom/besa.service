﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateCableFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateCableFromAdapter|ConstructionStageContextTester")]
        public async Task UpdateCableFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(10, 30);

            List<Int32> existingIds = new List<int>();

            Dictionary<Int32, String> adapterMapper = new Dictionary<int, string>();

            BranchableDataModel branchable = base.GenerateCabinet(random);
            storage.Branchables.Add(branchable);
            storage.SaveChanges();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                existingIds.Add(dataModel.Id);
                adapterMapper.Add(dataModel.Id, dataModel.ProjectAdapterId);
            }

            Dictionary<Int32, ProjectAdapterCableModel> expected = new Dictionary<int, ProjectAdapterCableModel>(amount);

            foreach (Int32 itemId in existingIds)
            {
                ProjectAdapterCableModel adapterModel = base.GenerateProjectAdapterCableModel(random);
                adapterModel.ProjectAdapterId = adapterMapper[itemId];

                Boolean result = await storage.UpdateCableFromAdapter(itemId, adapterModel, branchable.Id, fiberCableTypeDataModel.Id);
                Assert.True(result);

                expected.Add(itemId, adapterModel);
            }

            foreach (KeyValuePair<Int32, ProjectAdapterCableModel> expectedItem in expected)
            {
                FiberCableDataModel dataModel = storage.FiberCables.FirstOrDefault(x => x.Id == expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.NotNull(dataModel.StartingAtBranchableleId);
                Assert.Equal(branchable.Id, dataModel.StartingAtBranchableleId);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.Lenght, dataModel.Length);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
                Assert.Equal(expectedItem.Value.DuctColor.Name, dataModel.DuctColorName);
                Assert.Equal(expectedItem.Value.DuctColor.RGBHexValue, dataModel.DuctHexValue);
            }
        }
    }
}
