﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class ConstructionStageContextTester_UpdateCableType : DatabaseTesterBase
    {
        [Fact]
        public async Task UpdateCableType()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 cableTypeAmount = random.Next(30, 100);
            List<String> projectAdapterIds = new List<string>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                FiberCableTypeDataModel dataModel = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(dataModel);
                projectAdapterIds.Add(dataModel.AdapterId);
            }

            storage.SaveChanges();

            String adapterIdToUpdate = projectAdapterIds[random.Next(0, projectAdapterIds.Count)];

            ProjectAdapterCableTypeModel updateItem = new ProjectAdapterCableTypeModel
            {
                AdapterId = adapterIdToUpdate,
                FiberAmount = random.Next(30, 100),
                Name = $"Update-Kabel-Name-Nr {random.Next()}",
                FiberPerBundle = random.Next(30, 100),
            };

            Boolean result = await storage.UpdateCableType(updateItem);

            Assert.True(result);

            FiberCableTypeDataModel changedDataModel = storage.CableTypes.FirstOrDefault(x => x.AdapterId == updateItem.AdapterId);
            Assert.NotNull(changedDataModel);

            Assert.Equal(updateItem.FiberAmount, changedDataModel.FiberAmount);
            Assert.Equal(updateItem.Name, changedDataModel.Name);
            Assert.Equal(updateItem.FiberPerBundle, changedDataModel.FiberPerBundle);
        }
    }
}
