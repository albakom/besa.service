﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateConstructionStage : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateConstructionStage|ConstructionStageContextTester")]
        public async Task UpdateConstructionStage()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ConstructionStageDataModel constructionStageData = base.GenerateConstructionStage(random);
            storage.ConstructionStages.Add(constructionStageData);
            storage.SaveChanges();

            List<Int32> branchableIds = new List<int>();

            List<Int32> existingBranchable = new List<int>();
            List<Int32> openBranchableIDs = new List<int>();
            Int32 branchableAmount = random.Next(30, 100);
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);

                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                Boolean addToStage = random.NextDouble() > 0.7;
                if (addToStage == true)
                {
                    cabinetDataModel.ConstructionStage = constructionStageData;
                    existingBranchable.Add(cabinetDataModel.Id);
                    storage.SaveChanges();
                }
                else
                {
                    openBranchableIDs.Add(cabinetDataModel.Id);
                }

                branchableIds.Add(cabinetDataModel.Id);
            }

            List<Int32> branchableToAdd = new List<int>();
            List<Int32> branchableToRemove = new List<int>();
            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel { Id = constructionStageData.Id, Name = constructionStageData.Name.Substring(2), AddedBranchables = branchableToAdd, RemovedBranchables = branchableToRemove };

            Int32 branchableAddAmount = random.Next(1, openBranchableIDs.Count);
            for (int i = 0; i < branchableAddAmount; i++)
            {
                Int32 idToAdd = openBranchableIDs[random.Next(0, openBranchableIDs.Count)];
                if (branchableToAdd.Contains(idToAdd) == false)
                {
                    branchableToAdd.Add(idToAdd);
                }
            }

            Int32 branchableRemoveAmount = random.Next(1, existingBranchable.Count - 1);
            for (int i = 0; i < branchableAddAmount; i++)
            {
                Int32 idToRemove = existingBranchable[random.Next(0, existingBranchable.Count)];
                if (branchableToRemove.Contains(idToRemove) == false)
                {
                    branchableToRemove.Add(idToRemove);
                }
            }

            Boolean result = await storage.UpdateConstructionStage(updateModel);
            Assert.True(result);

            ConstructionStageDataModel actual = storage.ConstructionStages.FirstOrDefault(x => x.Id == updateModel.Id);
            Assert.NotNull(actual);

            Assert.Equal(updateModel.Name, actual.Name);

            foreach (Int32 branchableId in updateModel.AddedBranchables)
            {
                BranchableDataModel branchable = storage.Branchables.First(x => x.Id == branchableId);
                Assert.Equal(updateModel.Id, branchable.ConstructionStageId);
            }

            foreach (Int32 branchableId in updateModel.RemovedBranchables)
            {
                BranchableDataModel branchable = storage.Branchables.First(x => x.Id == branchableId);
                Assert.Null(branchable.ConstructionStageId);
            }

            foreach (Int32 branchableId in branchableIds)
            {
                if(updateModel.RemovedBranchables.Contains(branchableId) == true)
                {
                    BranchableDataModel branchable = storage.Branchables.First(x => x.Id == branchableId);
                    Assert.Null(branchable.ConstructionStageId);
                }
                else if(updateModel.AddedBranchables.Contains(branchableId) == true)
                {
                    BranchableDataModel branchable = storage.Branchables.First(x => x.Id == branchableId);
                    Assert.Equal(updateModel.Id, branchable.ConstructionStageId);
                }
            }
        }
    }
}
