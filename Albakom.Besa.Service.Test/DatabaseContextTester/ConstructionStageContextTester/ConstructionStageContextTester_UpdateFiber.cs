﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateFiber : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateFiber|ConstructionStageContextTester")]
        public async Task UpdateFiber()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(10, 30);

            Dictionary<Int32, String> existingIds = new Dictionary<int, string>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                FiberDataModel fiberDataModel = base.GenerateFiberDataModel(random, dataModel);
                storage.Fibers.Add(fiberDataModel);
                storage.SaveChanges();

                existingIds.Add(fiberDataModel.Id,  fiberDataModel.ProjectAdapterId);
            }

            Dictionary<Int32, ProjectAdapterFiberModel> expected = new Dictionary<int, ProjectAdapterFiberModel>(amount);

            foreach (KeyValuePair<Int32,String> item in existingIds)
            {
                ProjectAdapterFiberModel adapterModel = base.GenerateProjectAdapterFiberModel(random);
                adapterModel.ProjectAdapterId = item.Value;

                Boolean result = await storage.UpdateFiber(adapterModel);
                Assert.True(result);

                expected.Add(item.Key, adapterModel);
            }
          
            foreach (KeyValuePair<Int32, ProjectAdapterFiberModel> expectedItem in expected)
            {
                FiberDataModel dataModel = storage.Fibers.FirstOrDefault(x => x.Id == expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
                Assert.Equal(expectedItem.Value.Color, dataModel.Color);
                Assert.Equal(expectedItem.Value.FiberNumber, dataModel.FiberNumber);
                Assert.Equal(expectedItem.Value.NumberinBundle, dataModel.NumberinBundle);
            }
        }
    }
}
