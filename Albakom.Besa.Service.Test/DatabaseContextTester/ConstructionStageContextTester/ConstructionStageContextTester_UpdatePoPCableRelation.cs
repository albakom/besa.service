﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdatePoPCableRelation : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdatePoPCableRelation|ConstructionStageContextTester")]
        public async Task UpdatePoPCableRelation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(100, 300);

            PoPDataModel pop = base.GeneratePoPDataModel(random);
            storage.Pops.Add(pop);
            storage.SaveChanges();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            List<Int32> idsToAddToPoP = new List<int>();

            List<Int32> cableIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                Boolean searched = random.NextDouble() > 0.5;
                if(searched == true)
                {
                    idsToAddToPoP.Add(dataModel.Id);
                }

                cableIds.Add(dataModel.Id);
            }

            Boolean result = await storage.UpdatePoPCableRelation(pop.Id, idsToAddToPoP);
            Assert.True(result);

            foreach (Int32 cableId in cableIds)
            {
                FiberCableDataModel dataModel = storage.FiberCables.FirstOrDefault(x => x.Id == cableId);
                Assert.NotNull(dataModel);

                if(idsToAddToPoP.Contains(cableId) == true)
                {
                    Assert.NotNull(dataModel.StartingAdPopId);
                    Assert.Equal(pop.Id, dataModel.StartingAdPopId.Value);
                }
                else
                {
                    Assert.Null(dataModel.StartingAdPopId);
                }
            }
        }
    }
}
