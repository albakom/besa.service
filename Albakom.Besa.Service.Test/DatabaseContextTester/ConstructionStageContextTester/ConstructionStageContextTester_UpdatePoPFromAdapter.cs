﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdatePoPFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdatePoPFromAdapter|ConstructionStageContextTester")]
        public async Task UpdatePoPFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);

            List<Int32> existingPopIds = new List<int>();
            Dictionary<Int32, String> adapterMapper = new Dictionary<int, string>();

            for (int i = 0; i < popAmount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                existingPopIds.Add(dataModel.Id);
                adapterMapper.Add(dataModel.Id, dataModel.ProjectAdapterId);
            }

            Dictionary<Int32, ProjectAdapterPoPModel> expected = new Dictionary<int, ProjectAdapterPoPModel>(popAmount);

            foreach (Int32 itemId in existingPopIds)
            {
                ProjectAdapterPoPModel adapterModel = base.GenerateProjectAdapterPoPModel(random);
                adapterModel.ProjectAdapterId = adapterMapper[itemId];

                Boolean result = await storage.UpdatePoPFromAdapter(itemId, adapterModel);
                Assert.True(result);

                expected.Add(itemId, adapterModel);
            }
          
            foreach (KeyValuePair<Int32, ProjectAdapterPoPModel> expectedItem in expected)
            {
                PoPDataModel dataModel = storage.Pops.FirstOrDefault(x => x.Id == expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.Name, dataModel.Name);
                Assert.Equal(expectedItem.Value.Location.Latitude, dataModel.Latitude);
                Assert.Equal(expectedItem.Value.Location.Longitude, dataModel.Longitude);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
            }
        }
    }
}
