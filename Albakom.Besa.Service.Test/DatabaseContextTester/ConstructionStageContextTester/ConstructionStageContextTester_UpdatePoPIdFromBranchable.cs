﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdatePoPIdFromBranchable : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdatePoPIdFromBranchable|ConstructionStageContextTester")]
        public async Task UpdatePoPIdFromBranchable()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 popAmount = random.Next(10, 30);
            List<Int32> popIds = new List<int>(popAmount);

            for (int i = 0; i < popAmount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                popIds.Add(dataModel.Id);
            }

            Int32 branchableAmount = random.Next(20, 30);

            Dictionary<Int32, Int32?> expectedResults = new Dictionary<int, int?>();

            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                Boolean addPop = random.NextDouble() > 0.5;
                if (addPop == true)
                {
                    Int32 popId = popIds[random.Next(0, popIds.Count)];
                    await storage.UpdatePoPIdFromBranchable(cabinetDataModel.ProjectId, popId);
                    expectedResults.Add(cabinetDataModel.Id, popId);
                }
                else
                {
                    expectedResults.Add(cabinetDataModel.Id, null);
                }
            }


            foreach (KeyValuePair<Int32, Int32?> expectedItem in expectedResults)
            {
                BranchableDataModel dataModel = storage.Branchables.FirstOrDefault(x => x.Id == expectedItem.Key);
                Assert.NotNull(dataModel);

                if(expectedItem.Value.HasValue == true)
                {
                    Assert.True(dataModel.PoPId.HasValue);
                    Assert.Equal(expectedItem.Value.Value, dataModel.PoPId.Value);
                }
                else
                {
                    Assert.False(dataModel.PoPId.HasValue);
                }
            }
        }
    }
}
