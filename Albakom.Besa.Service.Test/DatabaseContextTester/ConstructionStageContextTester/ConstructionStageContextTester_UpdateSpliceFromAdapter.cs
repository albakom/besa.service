﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ConstructionStageContextTester
{
    public class ConstructionStageContextTester_UpdateSpliceFromAdapter : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateSpliceFromAdapter|ConstructionStageContextTester")]
        public async Task UpdateSpliceFromAdapter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableTypeDataModel);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);
            List<Int32> branchableIds = new List<int>(branchableAmount);
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel branchable = base.GenerateCabinet(random);
                storage.Branchables.Add(branchable);
                storage.SaveChanges();

                branchableIds.Add(branchable.Id);
            }

            Int32 amount = random.Next(10, 30);

            List<Int32> existingIds = new List<int>();
            Dictionary<Int32, String> adapterMapper = new Dictionary<int, string>();

            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel firstCableDataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(firstCableDataModel);
                storage.SaveChanges();

                FiberDataModel firstFiberDataModel = base.GenerateFiberDataModel(random, firstCableDataModel);
                storage.Fibers.Add(firstFiberDataModel);
                storage.SaveChanges();

                FiberCableDataModel secondCableDataModel = base.GenerateFiberCableDataModel(random, fiberCableTypeDataModel);
                storage.FiberCables.Add(secondCableDataModel);
                storage.SaveChanges();

                FiberDataModel secondFiberDataModel = base.GenerateFiberDataModel(random, secondCableDataModel);
                storage.Fibers.Add(secondFiberDataModel);
                storage.SaveChanges();

                FiberSpliceDataModel dataModel = base.GenerateFiberSpliceDataModel(random, firstFiberDataModel,secondFiberDataModel);
                dataModel.BranchableId = branchableIds[random.Next(0, branchableIds.Count)];
                storage.Splices.Add(dataModel);
                storage.SaveChanges();

                existingIds.Add(dataModel.Id);

                adapterMapper.Add(dataModel.Id, dataModel.ProjectAdapterId);
            }

            Dictionary<Int32, ProjectAdapterSpliceModel> expected = new Dictionary<int, ProjectAdapterSpliceModel>(amount);
            Dictionary<Int32, Int32> branchableMapper = new Dictionary<int, int>();

            foreach (Int32 itemId in existingIds)
            {
                ProjectAdapterSpliceModel adapterModel = base.GenerateProjectAdapterSpliceModel(random,false);
                adapterModel.ProjectAdapterId = adapterMapper[itemId];

                Int32 branchableId = branchableIds[random.Next(0, branchableIds.Count)]; 
                Boolean result = await storage.UpdateSpliceFromAdapter(itemId, adapterModel, branchableId) ;
                Assert.True(result);

                expected.Add(itemId, adapterModel);
                branchableMapper.Add(itemId, branchableId);
            }
          
            foreach (KeyValuePair<Int32, ProjectAdapterSpliceModel> expectedItem in expected)
            {
                FiberSpliceDataModel dataModel = storage.Splices.FirstOrDefault(x => x.Id == expectedItem.Key);

                Assert.NotNull(dataModel);

                Assert.Equal(expectedItem.Key, dataModel.Id);
                Assert.Equal(expectedItem.Value.NumberInTray, dataModel.NumberInTray);
                Assert.Equal(expectedItem.Value.TrayNumber, dataModel.TrayNumber);
                Assert.Equal(expectedItem.Value.ProjectAdapterId, dataModel.ProjectAdapterId);
                Assert.Equal(expectedItem.Value.Type, dataModel.Type);

                Assert.Equal(branchableMapper[expectedItem.Key], dataModel.BranchableId);
            }
        }
    }
}
