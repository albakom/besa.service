﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ContactContextTester
{
    public class ContactContextTester_CheckIfContactExists : DatabaseTesterBase
    {
        private class ContactNameTester
        {
            public String Surname { get; set; }
            public String Lastname { get; set; }
        }

        [Fact(DisplayName = "CheckIfContactExists|ContactContextTester")]
        public async Task DeleteJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();
            Int32 amount = random.Next(20, 40);

            Dictionary<ContactNameTester, Boolean> expectedResults = new Dictionary<ContactNameTester, bool>();
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel contactInfo = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(contactInfo);
                storage.SaveChanges();

                ContactNameTester positiveResult1 = new ContactNameTester { Lastname = contactInfo.Lastname, Surname = contactInfo.Surname };
                ContactNameTester positiveResult2 = new ContactNameTester { Lastname = contactInfo.Lastname.ToUpper(), Surname = contactInfo.Surname.ToLower() };

                ContactNameTester negativResult = new ContactNameTester { Lastname = contactInfo.Lastname.Substring(3), Surname = contactInfo.Surname.Substring(3) };

                expectedResults.Add(positiveResult1, true);
                expectedResults.Add(positiveResult2, true);

                expectedResults.Add(negativResult, false);
            }

            foreach (KeyValuePair<ContactNameTester, Boolean> item in expectedResults)
            {
                Boolean actual = await storage.CheckIfContactExists(item.Key.Surname, item.Key.Lastname);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
