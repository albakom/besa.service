﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ContactContextTester
{
    public class ContactContextTester_GetContactByName : DatabaseTesterBase
    {
        private class ContactNameTester
        {
            public String Surname { get; set; }
            public String Lastname { get; set; }
        }

        [Fact(DisplayName = "GetContactByName|ContactContextTester")]
        public async Task GetContactByName()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();
            Int32 amount = random.Next(20, 40);

            Dictionary<ContactNameTester, ContactInfoDataModel> expectedResults = new Dictionary<ContactNameTester, ContactInfoDataModel>();
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel contactInfo = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(contactInfo);
                storage.SaveChanges();

                ContactNameTester input = new ContactNameTester { Lastname = contactInfo.Lastname, Surname = contactInfo.Surname };
                expectedResults.Add(input, contactInfo);
            }

            foreach (KeyValuePair<ContactNameTester, ContactInfoDataModel> item in expectedResults)
            {
                PersonInfo actual = await storage.GetContactByName(item.Key.Surname, item.Key.Lastname);
                base.CheckContact(item.Value, actual);
            }
        }
    }
}
