﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public abstract class DatabaseTesterBase
    {
        protected List<ProcedureStates> GetProcedureStates()
        {
            String[] values = Enum.GetNames(typeof(ProcedureStates));
            List<ProcedureStates> result = new List<ProcedureStates>();
            foreach (String item in values)
            {
                Object state = ProcedureStates.ActivatitionJobAcknlowedged;
                if (Enum.TryParse(typeof(ProcedureStates), item, out state) == true)
                {
                    result.Add((ProcedureStates)state);
                }
            }

            return result;

        }

        protected BesaDataStorage GetStorage()
        {
            Random random = new Random();

            var blub = new Microsoft.EntityFrameworkCore.DbContextOptionsBuilder<BesaDataStorage>();
            blub = Microsoft.EntityFrameworkCore.InMemoryDbContextOptionsExtensions.UseInMemoryDatabase(blub, random.Next().ToString());

            BesaDataStorage storage = new BesaDataStorage(blub.Options);

            return storage;
        }

        internal List<TEnum> GenereteEnumValues<TEnum>() where TEnum : struct
        {
            List<TEnum> result = new List<TEnum>();
            Array values = Enum.GetValues(typeof(TEnum));

            foreach (var item in values)
            {
                TEnum value = default(TEnum);
                Boolean parseResult = Enum.TryParse<TEnum>(item.ToString(), out value);
                if (parseResult == true)
                {
                    result.Add(value);
                }
            }

            return result;
        }


        internal List<MessageActions> GeneratePossibleMessageActions()
        {
            return GenereteEnumValues<MessageActions>();
        }

        internal List<MessageRelatedObjectTypes> GeneratePossibleMessageRelatedObjectTypes()
        {
            return GenereteEnumValues<MessageRelatedObjectTypes>();
        }

        internal UpdateTaskDataModel GenerateUpdateTaskDataModel(Random random, UserDataModel user)
        {
            UpdateTaskDataModel dataModel = new UpdateTaskDataModel
            {
                Canceled = false,
                Ended = DateTimeOffset.Now.AddDays(-random.Next(3, 5)),
                Started = DateTimeOffset.Now.AddDays(-random.Next(6, 10)),
                StartedBy = user,
                Task = (UpdateTasks)random.Next(1, 9),
            };

            dataModel.Heartbeat = dataModel.Started;

            return dataModel;
        }

        internal UpdateTaskElementDataModel GenerateUpdateTaskElementDataModel(Random random, UpdateTaskDataModel task)
        {
            UpdateTaskElementDataModel dataModel = new UpdateTaskElementDataModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                Task = task,
                ObjectId = random.Next(),
                State = (UpdateTaskElementStates)random.Next(0, 5),
            };

            switch (dataModel.State)
            {
                case UpdateTaskElementStates.NotStarted:
                    break;
                case UpdateTaskElementStates.InProgress:
                    dataModel.Started = DateTimeOffset.Now.AddHours(-random.Next(1, 36));
                    break;
                case UpdateTaskElementStates.Error:
                case UpdateTaskElementStates.Waring:
                case UpdateTaskElementStates.Success:
                    dataModel.Ended = DateTimeOffset.Now.AddHours(-random.Next(1, 36));
                    dataModel.Result = $"Resultat mit der Nummer {random.Next()}";
                    break;
                default:
                    break;
            }

            return dataModel;
        }


        protected ProjectAdapterCableModel GenerateProjectAdapterCableModel(Random random)
        {
            ProjectAdapterCableModel item = new ProjectAdapterCableModel
            {
                Name = $"Testkabel Nr {random.Next()}",
                ProjectAdapterId = Guid.NewGuid().ToString(),
                Lenght = random.Next(50, 500) + random.NextDouble(),
                ProjectAdapterCableTypeId = Guid.NewGuid().ToString(),
                DuctColor = new ProjectAdapterColor
                {
                    Name = random.Next().ToString(),
                    RGBHexValue = "0x" + random.Next(999999),
                },
            };

            return item;
        }



        protected ProjectAdapterFiberModel GenerateProjectAdapterFiberModel(Random random)
        {
            ProjectAdapterFiberModel adapterItem = new ProjectAdapterFiberModel
            {
                BundleNumber = random.Next(1, 12),
                CableProjectAdapterId = Guid.NewGuid().ToString(),
                Color = $"Testfarbe {random.Next()}",
                FiberNumber = random.Next(1, 288),
                Name = $"Testfaser NR {random.Next()}",
                NumberinBundle = random.Next(1, 24),
                ProjectAdapterId = Guid.NewGuid().ToString(),
            };

            return adapterItem;
        }

        protected ProjectAdapterSpliceModel GenerateProjectAdapterSpliceModel(Random random, Boolean withFiber)
        {
            ProjectAdapterSpliceModel adapterItem = new ProjectAdapterSpliceModel
            {
                NumberInTray = random.Next(1, 12),
                TrayNumber = random.Next(1, 24),
                ProjectAdapterId = Guid.NewGuid().ToString(),
                Type = SpliceTypes.Normal,
            };

            if (withFiber == true)
            {
                adapterItem.FirstFiber = GenerateProjectAdapterFiberModel(random);
                adapterItem.SecondFiber = GenerateProjectAdapterFiberModel(random);
            }

            return adapterItem;
        }

        internal ProjectAdapterPatchModel GenerateProjectAdapterPatchModel(Random random)
        {
            ProjectAdapterPatchModel projectItem = new ProjectAdapterPatchModel
            {
                Caption = $"Testbeschriftung Nr. {random.Next()}",
                ColumnNumber = random.Next(1, 24).ToString(),
                ModuleName = $"Module Nr {random.Next(1, 20)}",
                RowNumber = random.Next(1, 24).ToString(),
            };

            return projectItem;
        }



        internal FiberConnenctionDataModel GenerateFiberConnenctionDataModel(Random random, FlatDataModel flatData)
        {
            FiberConnenctionDataModel dataModel = new FiberConnenctionDataModel
            {
                Caption = $"Testbeschriftung Nr. {random.Next()}",
                ColumnNumber = random.Next(1, 24).ToString(),
                ModuleName = $"Module Nr {random.Next(1, 20)}",
                RowNumber = random.Next(1, 24).ToString(),
                Flat = flatData,
            };

            return dataModel;
        }



        protected FiberSpliceDataModel GenerateFiberSpliceDataModel(Random random, FiberDataModel firstFiber, FiberDataModel secondFiber)
        {
            FiberSpliceDataModel dataModel = new FiberSpliceDataModel
            {
                FirstFiber = firstFiber,
                SecondFiber = secondFiber,
                ProjectAdapterId = Guid.NewGuid().ToString(),
                NumberInTray = random.Next(1, 24),
                TrayNumber = random.Next(1, 12),
                Type = SpliceTypes.Normal,
            };

            return dataModel;
        }



        internal FiberDataModel GenerateFiberDataModel(Random random, FiberCableDataModel cableDataModel)
        {
            FiberDataModel dataModel = new FiberDataModel
            {
                Cable = cableDataModel,
                ProjectAdapterId = Guid.NewGuid().ToString(),
                BundleNumber = random.Next(1, 12),
                Color = $"Testfarbe {random.Next()}",
                FiberNumber = random.Next(1, 288),
                Name = $"Kabel Nr. {random.Next()}",
                NumberinBundle = random.Next(1, 24),
            };

            return dataModel;
        }

        protected ProjectAdapterPoPModel GenerateProjectAdapterPoPModel(Random random)
        {
            ProjectAdapterPoPModel item = new ProjectAdapterPoPModel
            {
                Location = new GPSCoordinate
                {
                    Latitude = random.Next(20, 30) + random.NextDouble(),
                    Longitude = random.Next(20, 30) + random.NextDouble()
                },
                Name = $"TestPop Nr {random.Next()}",
                ProjectAdapterId = Guid.NewGuid().ToString(),
            };

            return item;
        }

        protected PoPDataModel GeneratePoPDataModel(Random random)
        {
            PoPDataModel item = new PoPDataModel
            {
                Latitude = random.Next(20, 30) + random.NextDouble(),
                Longitude = random.Next(20, 30) + random.NextDouble(),
                Name = $"Pop Nr. {random.Next()}",
                ProjectAdapterId = Guid.NewGuid().ToString(),
            };

            return item;
        }

        protected CompanyDataModel GenerateCompanyDataModel()
        {
            return new CompanyDataModel
            {
                Name = "Testfirma",
                City = "Musterstadt",
                Phone = "00454",
                PostalCode = "01245",
                Street = "Musterstr. 4",
                StreetNumber = "20",
            };

        }

        protected CompanyDataModel GenerateCompanyDataModel(Random random)
        {
            return new CompanyDataModel
            {
                Name = $"Testfirma {random.Next()}",
                City = "Musterstadt",
                Phone = "00454",
                PostalCode = "01245",
                Street = "Musterstr. 4",
                StreetNumber = "20",
            };
        }



        protected BuildingConnectionProcedureCreateModel GenerateBuildingConnectionProcedureCreateModel(Random random, Int32 buildingId, Int32 ownerContactId)
        {
            BuildingConnectionProcedureCreateModel model = new BuildingConnectionProcedureCreateModel
            {
                Appointment = random.NextDouble() > 0.5 ? new Nullable<DateTimeOffset>(DateTimeOffset.Now.AddDays(-random.Next(3, 10))) : null,
                BuildingId = buildingId,
                DeclarationOfAggrementInStock = random.NextDouble() > 0.5,
                ExpectedEnd = ProcedureStates.InjectJobFinished,
                Start = ProcedureStates.RequestCreated,
                Name = $"Testvorang Nr. {random.Next()}",
                OwnerContactId = ownerContactId,
                FirstElement = new ProcedureTimelineCreateModel
                {
                    Comment = $"Mein Kommentar Nr. {random.Next()}",
                    State = ProcedureStates.RequestCreated,
                },
                InformSalesAfterFinish = random.NextDouble() > 0.5,
            };

            return model;
        }

        internal BuildingConnectionProcedureDataModel GenerateBuildingConnectionProcedureDataModel(Random random)
        {
            BuildingConnectionProcedureDataModel model = new BuildingConnectionProcedureDataModel
            {
                Appointment = random.NextDouble() > 0.5 ? DateTimeOffset.Now.AddDays(-random.Next(3, 10)) : new DateTimeOffset?(),
                DeclarationOfAggrementInStock = random.NextDouble() > 0.5,
                ExpectedEnd = ProcedureStates.BranchOffJobFinished,
                Start = ProcedureStates.BranchOffJobAcknowledegd,
                IsFinished = false,
                Name = $"Testname Nr. {random.Next()}",
                InformSalesAfterFinish = random.NextDouble() > 0.5,
            };

            return model;
        }

        public CustomerConnectionProcedureDataModel GenerateCustomerConnectionProcedureDataModel(Random random)
        {
            CustomerConnectionProcedureDataModel model = new CustomerConnectionProcedureDataModel
            {
                Appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                ContractInStock = random.NextDouble() > 0.5,
                DeclarationOfAggrementInStock = random.NextDouble() > 0.5,
                ExpectedEnd = ProcedureStates.BranchOffJobFinished,
                Start = ProcedureStates.BranchOffJobAcknowledegd,
                IsFinished = false,
                Name = $"Testname Nr. {random.Next()}",
            };

            return model;
        }



        internal CustomerConnenctionRequestDataModel GenrateCustomerConnenctionRequestDataModel(Random random, Boolean active, BesaDataStorage storage)
        {
            ContactInfoDataModel architect = null;
            if (active == false)
            {
                architect = GenerateContactInfo(random);
                storage.ContactInfos.Add(architect);
                storage.SaveChanges();
            };

            ContactInfoDataModel customer = null;
            if (active == true)
            {
                customer = GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();
            };

            ///TODO weitere Eigenschaften hinzufügen
            CustomerConnenctionRequestDataModel model = new CustomerConnenctionRequestDataModel
            {
                ActivePointNearSubscriberEndpointDescription = active == false ? String.Empty : $"BEschreibung Aktive TEchnik Nr. {random.Next()}",
                ActivePointNearSubscriberEndpointValue = active == false ? false : random.NextDouble() > 0.5,
                Architect = architect,
                CivilWorkIsDoneByCustomer = active == true ? false : random.NextDouble() > 0.5,
                ConnectionAppointment = active == true ? new Nullable<DateTimeOffset>(DateTimeOffset.Now.AddDays(-random.Next(3, 10))) : null,
                ConnectioOfOtherMediaRequired = active == false ? false : random.NextDouble() > 0.5,
                Customer = customer,
                DescriptionForHouseConnection = active == false ? String.Empty : $"HA Beschreibung Nr. {random.Next()}",
            };

            return model;
        }

        internal CustomerConnectionProcedureCreateModel GenerateCustomerConnectionProcedureCreateModel(Random random, Int32 flatId, Int32 customerContactId)
        {
            CustomerConnectionProcedureCreateModel model = new CustomerConnectionProcedureCreateModel
            {
                Appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                FlatId = flatId,
                DeclarationOfAggrementInStock = random.NextDouble() > 0.5,
                ExpectedEnd = ProcedureStates.InjectJobFinished,
                Start = ProcedureStates.RequestCreated,
                Name = $"Testvorang Nr. {random.Next()}",
                CustomerContactId = customerContactId,
                FirstElement = new ProcedureTimelineCreateModel
                {
                    Comment = $"Mein Kommentar Nr. {random.Next()}",
                    State = ProcedureStates.RequestCreated,
                },
                AsSoonAsPossible = random.NextDouble() > 0.5,
            };

            if (model.AsSoonAsPossible == true)
            {
                model.Appointment = null;
            }

            return model;
        }

        protected ProjectAdapterCabinetModel GenerateProjectAdapterCabinetModel(Random random)
        {
            ProjectAdapterCabinetModel model = new ProjectAdapterCabinetModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                Coordinate = new GPSCoordinate
                {
                    Latitude = random.Next(50, 60) + random.NextDouble(),
                    Longitude = random.Next(50, 60) + random.NextDouble(),
                },
                Name = $"kVz {random.Next()}",
            };

            return model;
        }

        protected ProjectAdapterBranchableModel ProjectAdapterBranchableModel(Random random)
        {
            ProjectAdapterBranchableModel model = new ProjectAdapterCabinetModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                Coordinate = new GPSCoordinate
                {
                    Latitude = random.Next(50, 60) + random.NextDouble(),
                    Longitude = random.Next(50, 60) + random.NextDouble(),
                },
                Name = $"kVz {random.Next()}",
            };

            return model;
        }

        protected ProjectAdapterSleeveModel GenerateProjectAdapterSleeveModel(Random random)
        {
            ProjectAdapterSleeveModel model = new ProjectAdapterSleeveModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                Coordinate = new GPSCoordinate
                {
                    Latitude = random.Next(50, 60) + random.NextDouble(),
                    Longitude = random.Next(50, 60) + random.NextDouble(),
                },
                Name = $"kVz {random.Next()}",
            };

            return model;
        }

        protected UserDataModel GenerateUserDataModel(Random rand)
        {
            if (rand == null) { rand = new Random(); }

            return new UserDataModel
            {
                AuthServiceId = Guid.NewGuid().ToString(),
                Email = $"blub-{rand.Next()}@blub.de",
                Lastname = $"Nachname {rand.Next()}",
                Surname = $"vorname {rand.Next()}",
            };
        }

        internal BuildingDataModel GenerateBuilding(Random random)
        {
            BuildingDataModel model = new BuildingDataModel
            {
                CommercialUnits = random.NextDouble() > 0.5 ? 0 : random.Next(4, 10),
                HouseholdUnits = random.NextDouble() > 0.5 ? 0 : random.Next(1, 5),
                ProjectId = Guid.NewGuid().ToString(),
                Latitude = random.Next(50, 60) + random.NextDouble(),
                Longitude = random.Next(50, 60) + random.NextDouble(),
                StreetName = $"Straße Nr {random.Next()}",
                ConnenctionLength = random.Next(30, 500) + random.NextDouble(),
            };

            model.NormalizedStreetName = model.StreetName.ToLower();
            return model;
        }

        internal CustomerConnenctionRequestDataModel GenerateConnenctionRequestDataModel(Random random, IList<Int32> contactIds)
        {
            CustomerConnenctionRequestDataModel model = new CustomerConnenctionRequestDataModel
            {
                ActivePointNearSubscriberEndpointValue = random.NextDouble() > 0.5,
                ActivePointNearSubscriberEndpointDescription = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                ArchitectPersonId = contactIds[random.Next(0, contactIds.Count)],
                ConnectionAppointment = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                CustomerPersonId = contactIds[random.Next(0, contactIds.Count)],
                CivilWorkIsDoneByCustomer = random.NextDouble() > 0.5,
                ConnectioOfOtherMediaRequired = random.NextDouble() > 0.5,
                DescriptionForHouseConnection = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                DuctAmount = random.NextDouble() > 0.5 ? random.Next(10, 20) + random.NextDouble() : new Nullable<Double>(),
                SubscriberEndpointLength = random.NextDouble() > 0.5 ? random.Next(40, 50) + random.NextDouble() : new Nullable<Double>(),
                OnlyHouseConnection = random.NextDouble() > 0.5,
                PowerForActiveEquipmentValue = random.NextDouble() > 0.5,
                PowerForActiveEquipmentDescription = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                PropertyOwnerPersonId = contactIds[random.Next(0, contactIds.Count)],
                WorkmanPersonId = contactIds[random.Next(0, contactIds.Count)],
                SubscriberEndpointNearConnectionPointValue = random.NextDouble() > 0.5,
                SubscriberEndpointNearConnectionPointDescription = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                IsClosed = random.NextDouble() > 0.5,
            };

            return model;
        }

        protected internal FiberCableTypeDataModel GenerateFiberCableTypeDataModel(Random random)
        {
            FiberCableTypeDataModel model = new FiberCableTypeDataModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                FiberAmount = random.Next(1, 24) * 12,
                FiberPerBundle = 12 * random.Next(1, 3),
                Name = $"Kabeltyp Nr. {random.Next()}",
            };

            return model;
        }

        protected internal FiberCableDataModel GenerateFiberCableDataModel(Random random, FiberCableTypeDataModel type)
        {
            FiberCableDataModel model = new FiberCableDataModel
            {
                CableType = type,
                DuctColorName = $"Orange-{random.Next()}",
                DuctHexValue = $"0x{random.Next(0, 255)}{random.Next(0, 255)}{random.Next(0, 255)}",
                Length = random.Next(100, 1000) + random.NextDouble(),
                ProjectAdapterId = Guid.NewGuid().ToString(),
                Name = $"Kabelnamr Nr. {random.Next()}"
            };

            return model;
        }

        protected internal FiberCableTypeDataModel GenerateAndSaveFiberCableTypeDataModel(Random random, BesaDataStorage storage)
        {
            FiberCableTypeDataModel model = new FiberCableTypeDataModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                FiberAmount = random.Next(1, 24) * 12,
                FiberPerBundle = 12 * random.Next(1, 3),
                Name = $"Kabeltyp Nr. {random.Next()}",
            };

            storage.CableTypes.Add(model);
            storage.SaveChanges();

            return model;
        }

        protected ProjectAdapterBuildingModel GenerateProjectAdapterBuildingModel(Random random)
        {
            ProjectAdapterBuildingModel model = new ProjectAdapterBuildingModel
            {
                AdapterId = Guid.NewGuid().ToString(),
                CommercialUnits = random.Next(3, 10),
                HouseholdUnits = random.Next(3, 10),
                Coordinate = new GPSCoordinate
                {
                    Latitude = random.Next(30, 40) + random.NextDouble(),
                    Longitude = random.Next(30, 40) + random.NextDouble(),
                },
                Street = $"Tolle Straße {random.Next()}",
                HouseConnectionColor = new ProjectAdapterColor { Name = random.Next().ToString(), RGBHexValue = "0x" + random.Next(999999) },
                MicroductColor = new ProjectAdapterColor { Name = random.Next().ToString(), RGBHexValue = "0x" + random.Next(999999) },
            };

            return model;
        }

        internal FileDataModel GenerateFileDataModel(Random random)
        {
            FileDataModel model = new FileDataModel
            {
                CreatedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                Extention = $"jpg-{random.Next()}",
                Name = $"Datei-{random.Next()}",
                MimeType = $"random-type-{random.Next()}",
                Size = random.Next(),
                StorageUrl = $"http://random-url.de/{random.Next()}",
                Type = FileTypes.Image,
            };

            return model;
        }

        internal CabinetDataModel GenerateCabinet(Random random)
        {
            CabinetDataModel model = new CabinetDataModel
            {
                ProjectId = Guid.NewGuid().ToString(),
                Latitude = random.Next(50, 60) + random.NextDouble(),
                Longitude = random.Next(50, 60) + random.NextDouble(),
                Name = $"Kabelverzweiger Nr {random.Next(300)}",
            };

            model.NormalizedName = model.Name.ToLower();
            return model;
        }

        internal SleeveDataModel GenereteSleeve(Random random)
        {
            SleeveDataModel model = new SleeveDataModel
            {
                ProjectId = Guid.NewGuid().ToString(),
                Latitude = random.Next(50, 60) + random.NextDouble(),
                Longitude = random.Next(50, 60) + random.NextDouble(),
                Name = $"Muffe Nr {random.Next(300)}",
            };

            model.NormalizedName = model.Name.ToLower();
            return model;
        }

        protected ConstructionStageDataModel GenerateConstructionStage(Random random)
        {
            ConstructionStageDataModel model = new ConstructionStageDataModel
            {
                Name = $"Testgebiet {random.Next()}",
            };

            return model;
        }

        protected CollectionJobDataModel GenerateCollectionJobDataModel(Random random)
        {
            CollectionJobDataModel model = new CollectionJobDataModel
            {
                Company = GenerateCompanyDataModel(),
                FinishedTill = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                Name = $"Sammelaufgabe {random.Next()}"
            };

            return model;
        }

        protected ArticleDataModel GenerateArticle(Random random)
        {
            ArticleDataModel model = new ArticleDataModel
            {
                Name = $"Produktname {random.Next()}",
                ProductSoldByMeter = random.NextDouble() > 0.5
            };

            return model;
        }

        protected FlatDataModel GenerateFlatDataModel(Random random)
        {
            FlatDataModel model = new FlatDataModel
            {
                Description = "Hinten im Flur und dann Rechts. Nach {random.Next()} Schritten links",
                Floor = "Obergeschoss",
                Number = "301",
                ProjectAdapterCableId = Guid.NewGuid().ToString(),
            };

            return model;
        }

        protected ContactInfoDataModel GenerateContactInfo(Random random)
        {
            ContactInfoDataModel model = new ContactInfoDataModel
            {
                City = $"Musterstadt {random.Next()}",
                CompanyName = random.NextDouble() > 0.5 ? $"Musterfirma {random.Next()}" : "",
                EmailAddress = $"test-{random.Next()}@test.de",
                Lastname = $"Nachname {random.Next()}",
                Surname = $"Vorname {random.Next()}",
                Phone = $"0548754-{random.Next()}",
                Street = $"Musterstr {random.Next()}",
                StreetNumber = random.Next(10, 20).ToString(),
                Type = random.NextDouble() > 0.5 ? Contracts.Models.PersonTypes.Male : Contracts.Models.PersonTypes.Female,
            };

            return model;
        }

        protected class GenerateCustomerAccessRequestsResult
        {
            public List<Int32> Ids { get; set; }
            public Int32 ElementId { get; set; }
            public BesaDataStorage Storage { get; set; }

        }

        protected GenerateCustomerAccessRequestsResult GenerateCustomerAccessRequests(Boolean? isClosed)
        {
            BesaDataStorage storage = GetStorage();

            Random random = new Random();

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();
            List<Int32> contactIds = contacts.Select(x => x.Id).ToList();

            Int32 buildingAmount = random.Next(3, 10);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = GenerateBuilding(random);
                buildings.Add(building);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            Int32 requestAmount = random.Next(10, 30);
            List<CustomerConnenctionRequestDataModel> requests = new List<CustomerConnenctionRequestDataModel>(requestAmount);
            for (int i = 0; i < requestAmount; i++)
            {
                CustomerConnenctionRequestDataModel requestDataModel = GenerateConnenctionRequestDataModel(random, contactIds);
                requestDataModel.Building = buildings[random.Next(0, buildings.Count)];
                if (isClosed.HasValue == true)
                {
                    requestDataModel.IsClosed = isClosed.Value;
                }
                requests.Add(requestDataModel);
            }

            storage.CustomerRequests.AddRange(requests);
            storage.SaveChanges();

            List<Int32> ids = new List<int>(requests.Select(x => x.Id));
            Int32 idToDelete = ids[random.Next(0, ids.Count)];

            return new GenerateCustomerAccessRequestsResult
            {
                Storage = storage,
                ElementId = idToDelete,
                Ids = ids,
            };
        }

        protected ConnectBuildingJobDataModel GenerateConnectBuildingJobDataModel(Random random, BuildingDataModel building, List<ContactInfoDataModel> contacts)
        {
            return new ConnectBuildingJobDataModel
            {
                Building = building,
                Architect = contacts[random.Next(0, contacts.Count)],
                Workman = contacts[random.Next(0, contacts.Count)],
                Owner = contacts[random.Next(0, contacts.Count)],
                Customer = contacts[random.Next(0, contacts.Count)],
                ConnectionAppointment = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                CivilWorkIsDoneByCustomer = random.NextDouble() > 0.5,
                Description = $"Tolle Beschreibung {random.Next()}",
                OnlyHouseConnection = random.NextDouble() > 0.5,
                ConnectionOfOtherMediaRequired = random.NextDouble() > 0.5,
            };
        }

        protected List<FileDataModel> AddFilesToJob(Random random, BesaDataStorage storage, FinishedJobDataModel finishedDataModel)
        {
            Int32 fileAmount = random.Next(3, 10);

            List<FileDataModel> files = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel fileDataModel = GenerateFileDataModel(random);
                fileDataModel.RelatedJob = finishedDataModel;
                files.Add(fileDataModel);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            return files;
        }

        protected internal void CheckContact(ContactInfoDataModel dataModel, PersonInfo model)
        {
            Assert.NotNull(dataModel);
            Assert.NotNull(model);

            Assert.Equal(dataModel.CompanyName, model.CompanyName);
            Assert.Equal(dataModel.EmailAddress, model.EmailAddress);
            Assert.Equal(dataModel.Lastname, model.Lastname);
            Assert.Equal(dataModel.Surname, model.Surname);
            Assert.Equal(dataModel.Type, model.Type);
            Assert.Equal(dataModel.Id, model.Id);

            Assert.NotNull(model.Address);
            Assert.Equal(dataModel.City, model.Address.City);
            Assert.Equal(dataModel.Street, model.Address.Street);
            Assert.Equal(dataModel.StreetNumber, model.Address.StreetNumber);
            Assert.Equal(dataModel.PostalCode, model.Address.PostalCode);
        }


        protected internal void CheckContact(PersonInfo expected, PersonInfo actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.Equal(expected.CompanyName, actual.CompanyName);
            Assert.Equal(expected.EmailAddress, actual.EmailAddress);
            Assert.Equal(expected.Lastname, actual.Lastname);
            Assert.Equal(expected.Surname, actual.Surname);
            Assert.Equal(expected.Type, actual.Type);
            Assert.Equal(expected.Id, actual.Id);

            Assert.Equal(expected.Address.City, actual.Address.City);
            Assert.Equal(expected.Address.Street, actual.Address.Street);
            Assert.Equal(expected.Address.StreetNumber, actual.Address.StreetNumber);
            Assert.Equal(expected.Address.PostalCode, actual.Address.PostalCode);
        }

        internal void CheckBuilding(SimpleBuildingOverviewModel expected, SimpleBuildingOverviewModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.HouseholdUnits, actual.HouseholdUnits);
            Assert.Equal(expected.CommercialUnits, actual.CommercialUnits);
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.StreetName, actual.StreetName);
        }

        internal void CheckBuilding(SimpleBuildingOverviewWithGPSModel expected, SimpleBuildingOverviewWithGPSModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.HouseholdUnits, actual.HouseholdUnits);
            Assert.Equal(expected.CommercialUnits, actual.CommercialUnits);
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.StreetName, actual.StreetName);

            Assert.NotNull(actual.Coordinate);

            Assert.Equal(expected.Coordinate.Latitude, actual.Coordinate.Latitude);
            Assert.Equal(expected.Coordinate.Longitude, actual.Coordinate.Longitude);
        }

        internal void CheckFlat(SimpleFlatOverviewModel expected, SimpleFlatOverviewModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.BuildingId, actual.BuildingId);
            Assert.Equal(expected.Description, actual.Description);
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Floor, actual.Floor);
            Assert.Equal(expected.Number, actual.Number);
        }

        protected internal void CheckUser(CompanyDataModel companyDataModel, UserDataModel acceptor, UserOverviewModel user)
        {
            Assert.NotNull(user);
            Assert.Equal(acceptor.ID, user.Id);
            Assert.Equal(acceptor.Lastname, user.Lastname);
            Assert.Equal(acceptor.Surname, user.Surname);
            Assert.Equal(acceptor.Phone, user.Phone);
            Assert.Equal(acceptor.Email, user.EMailAddress);

            Assert.NotNull(user.CompanyInfo);
            Assert.Equal(companyDataModel.Id, user.CompanyInfo.Id);
            Assert.Equal(companyDataModel.Name, user.CompanyInfo.Name);
        }

        internal void CheckUpdateTaskObjectOverviewModel(UpdateTaskObjectOverviewModel expectedItem, UpdateTaskObjectOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);
            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.ProjectAdapterId, actualItem.ProjectAdapterId);
            Assert.Equal(expectedItem.Name, actualItem.Name);
            Assert.Equal(expectedItem.Type, actualItem.Type);
        }

        internal void CheckCollectionJobOverview(JobCollectionOverviewModel expectedItem, JobCollectionOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);
            Assert.Equal(expectedItem.CollectionJobId, actualItem.CollectionJobId);
            Assert.Equal(expectedItem.FinishedTill, actualItem.FinishedTill);
            Assert.Equal(expectedItem.Name, actualItem.Name);
            Assert.Equal(expectedItem.Percentage, actualItem.Percentage);

            CheckCompanies(expectedItem.BoundedTo, actualItem.BoundedTo);

            foreach (SimpleJobOverview expectedJobItem in expectedItem.Jobs)
            {
                SimpleJobOverview actualJobItem = actualItem.Jobs.FirstOrDefault(x => x.Id == expectedJobItem.Id);
                Assert.NotNull(actualJobItem);

                CheckSimpleJobOverview(expectedJobItem, actualJobItem);
            }
        }

        internal void CheckCompanies(SimpleCompanyOverviewModel expectedItem, SimpleCompanyOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);
            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.Name, actualItem.Name);
        }

        internal void CheckSimpleJobOverview(SimpleJobOverview expectedItem, SimpleJobOverview actualItem)
        {
            Assert.NotNull(actualItem);
            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.Name, actualItem.Name);
            Assert.Equal(expectedItem.JobType, actualItem.JobType);
            Assert.Equal(expectedItem.State, actualItem.State);
        }

        internal void CheckUser(UserOverviewModel expectedItem, UserOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);
            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.Surname, actualItem.Surname);
            Assert.Equal(expectedItem.Lastname, actualItem.Lastname);
            Assert.Equal(expectedItem.EMailAddress, actualItem.EMailAddress);
            Assert.Equal(expectedItem.Phone, actualItem.Phone);
            Assert.Equal(expectedItem.IsMember, actualItem.IsMember);

            if (expectedItem.CompanyInfo == null)
            {
                Assert.Null(actualItem.CompanyInfo);
            }
            else
            {
                Assert.NotNull(actualItem.CompanyInfo);
                CheckCompanies(expectedItem.CompanyInfo, actualItem.CompanyInfo);
            }
        }

        internal void CheckProtocolEntryModel(ProtocolEntryModel expected, ProtocolEntryModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Action, actual.Action);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.RelatedObjectId, actual.RelatedObjectId);
            Assert.Equal(expected.Timestamp, actual.Timestamp);
            Assert.Equal(expected.Type, actual.Type);

            if(expected.SourcedBy == null)
            {
                Assert.Null(actual.SourcedBy);
            }
            else
            {
                CheckUser(expected.SourcedBy, actual.SourcedBy);
            }
        }

        internal void CheckMessageRawModel(MessageRawModel expected, MessageRawModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.MarkedAsRead, actual.MarkedAsRead);
            Assert.Equal(expected.RelatedObjectType, actual.RelatedObjectType);
            Assert.Equal(expected.TimeStamp, actual.TimeStamp);
            Assert.Equal(expected.Action, actual.Action);
            Assert.Equal(expected.Details, actual.Details);

            if(expected.SourceUser != null)
            {
                CheckUser(expected.SourceUser, actual.SourceUser);
            }
            else
            {
                Assert.Null(actual.SourceUser);
            }
        }

        protected internal void GenerateUsersForFinishedJobs(BesaDataStorage storage, out CompanyDataModel companyDataModel, out UserDataModel jobber, out UserDataModel acceptor)
        {
            companyDataModel = GenerateCompanyDataModel();
            storage.Companies.Add(companyDataModel);
            storage.SaveChanges();

            jobber = GenerateUserDataModel(null);
            jobber.Company = companyDataModel;
            storage.Users.Add(jobber);
            storage.SaveChanges();

            acceptor = GenerateUserDataModel(null);
            acceptor.Company = companyDataModel;
            storage.Users.Add(acceptor);
            storage.SaveChanges();
        }

        internal void CheckUpdateTaskOverviewModel(UpdateTaskOverviewModel expectedItem, UpdateTaskOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);

            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.Endend, actualItem.Endend);
            Assert.Equal(expectedItem.LastHearbeat, actualItem.LastHearbeat);
            Assert.Equal(expectedItem.Started, actualItem.Started);
            Assert.Equal(expectedItem.TaskType, actualItem.TaskType);
            Assert.Equal(expectedItem.WasCanceled, actualItem.WasCanceled);

            CheckSimpleUserOverview(expectedItem.CreatedBy, actualItem.CreatedBy);
        }

        private void CheckSimpleUserOverview(SimpleUserOverviewModel expectedItem, SimpleUserOverviewModel actualItem)
        {
            Assert.NotNull(actualItem);

            Assert.Equal(expectedItem.Id, actualItem.Id);
            Assert.Equal(expectedItem.Name, actualItem.Name);
        }

        internal void CheckUpdateTaskRawDetailModel(UpdateTaskRawDetailModel expected, UpdateTaskRawDetailModel actual)
        {
            Assert.NotNull(actual);

            CheckUpdateTaskOverviewModel(expected.Overview, actual.Overview);

            Assert.NotNull(actual.Elements);
            Assert.Equal(expected.Elements.Count(), actual.Elements.Count());

            Dictionary<Int32, UpdateTaskElementRawOverviewModel> tempDicht = expected.Elements.ToDictionary(x => x.Id, x => x);

            foreach (UpdateTaskElementRawOverviewModel item in actual.Elements)
            {
                Assert.NotNull(item);

                Assert.True(tempDicht.ContainsKey(item.Id));

                UpdateTaskElementRawOverviewModel expectedItem = tempDicht[item.Id];
                CheckUpdateTaskElementRawOverviewModel(expectedItem, item);
            }
        }

        internal void CheckUpdateTaskElementRawOverviewModel(UpdateTaskElementRawOverviewModel expected, UpdateTaskElementRawOverviewModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Ended, actual.Ended);
            Assert.Equal(expected.RawResult, actual.RawResult);
            Assert.Equal(expected.Started, actual.Started);
            Assert.Equal(expected.State, actual.State);

            Assert.NotNull(actual.RelatedObject);

            Assert.Equal(expected.RelatedObject.Id, actual.RelatedObject.Id);
            Assert.Equal(expected.RelatedObject.ProjectAdapterId, actual.RelatedObject.ProjectAdapterId);
            if (String.IsNullOrEmpty(expected.RelatedObject.Name) == true)
            {
                Assert.True(String.IsNullOrEmpty(actual.RelatedObject.Name));
            }
            else
            {
                Assert.Equal(expected.RelatedObject.Name, actual.RelatedObject.Name);

            }
        }

        internal void CheckProtocolUserInformModel(ProtocolUserInformModel expected, ProtocolUserInformModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(expected.Action, actual.Action);
            Assert.Equal(expected.NotifcationType, actual.NotifcationType);
            Assert.Equal(expected.RelatedType, actual.RelatedType);
        }


        protected internal TFinisehdJob GetFinishedJobDataModel<TFinisehdJob>(JobDataModel job, Random random, UserDataModel acceptor, UserDataModel jobber) where TFinisehdJob : FinishedJobDataModel, new()
        {
            TFinisehdJob result = new TFinisehdJob();
            result.Job = job;
            result.DoneBy = jobber;
            result.FinishedAt = DateTime.Now.AddDays(-random.Next(3, 10));
            result.Comment = $"Meine Anmerkung {random.Next()}";

            result.ProblemHappend = random.NextDouble() > 0.5;
            result.ProblemHappendDescription = $"Problem da.... {random.Next()}";

            result.AcceptedAt = DateTime.Now.AddHours(-random.Next(3, 60));
            result.Accepter = acceptor;

            return result;
        }

        protected internal void CheckFinishModel(FinishedJobDataModel finishedDataModel, FinishJobDetailModel actual, ICollection<FileDataModel> files, UserDataModel acceptor, UserDataModel jobber, CompanyDataModel companyDataModel)
        {
            Assert.NotNull(actual);
            Assert.Equal(finishedDataModel.FinishedAt, actual.FinishedAt);

            CheckUser(companyDataModel, acceptor, actual.AcceptedBy);
            CheckUser(companyDataModel, jobber, actual.FinishedBy);

            Assert.NotNull(actual.AcceptedAt);
            Assert.Equal(finishedDataModel.AcceptedAt, actual.AcceptedAt);

            Assert.NotNull(actual.ProblemHappend);
            Assert.Equal(finishedDataModel.ProblemHappend, actual.ProblemHappend.Value);
            Assert.Equal(finishedDataModel.ProblemHappendDescription, actual.ProblemHappend.Description);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualItem in actual.Files)
            {
                FileDataModel expectedItem = files.FirstOrDefault(x => x.Id == actualItem.Id);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);
                Assert.Equal(expectedItem.Size, actualItem.Size);
                Assert.Equal(expectedItem.Type, actualItem.Type);
                Assert.Equal(expectedItem.Extention, actualItem.Extention);
            }
        }

        protected internal List<FileDataModel> GenerateAndSavesFiles(Random random, BesaDataStorage storage)
        {
            Int32 fileAmount = random.Next(3, 10);
            List<FileDataModel> files = new List<FileDataModel>(fileAmount);
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel fileDataModel = GenerateFileDataModel(random);
                files.Add(fileDataModel);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            return files;
        }

        protected internal List<ContactInfoDataModel> GenerateAndSavesContacts(Random random, BesaDataStorage storage)
        {
            Int32 amount = random.Next(20, 40);
            List<ContactInfoDataModel> files = new List<ContactInfoDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel dataModel = GenerateContactInfo(random);
                files.Add(dataModel);
            }

            storage.ContactInfos.AddRange(files);
            storage.SaveChanges();

            return files;
        }

        protected internal TFinisehdJob GeneretateFinsihModel<TFinisehdJob>(JobDataModel job, Random random, IEnumerable<FileDataModel> files, UserDataModel jobber) where TFinisehdJob : FinishedJobModel, new()
        {
            TFinisehdJob result = new TFinisehdJob
            {
                Comment = $"Mein Kommentar {random.Next()}",
                JobId = job.Id,
                Timestamp = DateTimeOffset.Now,
                ProblemHappend = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Problem da {random.Next()}",
                },
                FileIds = files.Select(x => x.Id).ToList(),
                UserAuthId = jobber.AuthServiceId,
            };
            return result;
        }

        protected internal void CheckFinishedJob(FinishedJobModel finishModel, FinishedJobDataModel actual, UserDataModel jobber, BesaDataStorage storage, IList<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(finishModel.Comment, actual.Comment);
            Assert.Equal(finishModel.ProblemHappend.Value, actual.ProblemHappend);
            Assert.Equal(finishModel.ProblemHappend.Description, actual.ProblemHappendDescription);
            Assert.Equal(finishModel.Timestamp, actual.FinishedAt);
            Assert.Equal(jobber.ID, actual.JobberId);

            List<FileDataModel> acutalFiles = storage.Files.Where(x => x.JobId == actual.Id).ToList();

            Assert.NotNull(acutalFiles);
            Assert.Equal(files.Count, acutalFiles.Count);

            for (int i = 0; i < files.Count; i++)
            {
                FileDataModel actualItem = acutalFiles[i];

                FileDataModel expectedModel = files.FirstOrDefault(x => x.Id == actualItem.Id);
                Assert.NotNull(expectedModel);
            }

            Assert.Null(actual.AcceptedAt);
            Assert.Null(actual.AccepterId);
        }

        protected List<FileDataModel> GenerateFilesAccessForJobDetails(Random random, BesaDataStorage storage, Int32 jobId, FileAccessObjects jobType, Int32? buildingId)
        {
            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            List<FileDataModel> expectedFiles = new List<FileDataModel>();
            foreach (FileDataModel item in files)
            {
                if (random.NextDouble() < 0.5) { continue; }

                FileAccessEntryDataModel entryDataModel = new FileAccessEntryDataModel { File = item, ObjectId = jobId, ObjectType = jobType };

                if (buildingId.HasValue == true && random.NextDouble() > 0.5)
                {
                    entryDataModel = new FileAccessEntryDataModel { File = item, ObjectId = buildingId.Value, ObjectType = FileAccessObjects.Building };
                }

                storage.FileAccessEntries.Add(entryDataModel);
                storage.SaveChanges();

                expectedFiles.Add(item);
            }

            return expectedFiles;
        }

        public ProtocolNotificationTypes GetNotifcationType(Random random)
        {
            Dictionary<Int32, ProtocolNotificationTypes> protocolNotficationMapper = new Dictionary<int, ProtocolNotificationTypes>
            {
                {  0, ProtocolNotificationTypes.Message },
                {  1, ProtocolNotificationTypes.Email },
                {  2, ProtocolNotificationTypes.SMS },
                {  3, ProtocolNotificationTypes.Push },
            };

            ProtocolNotificationTypes type = ProtocolNotificationTypes.NoAction;
            for (int j = 0; j < protocolNotficationMapper.Count; j++)
            {
                if (random.NextDouble() > 0.5)
                {
                    type |= protocolNotficationMapper[j];
                }
            }

            if (type == ProtocolNotificationTypes.NoAction)
            {
                type = ProtocolNotificationTypes.Message;
            }

            return type;
        }
    }
}
