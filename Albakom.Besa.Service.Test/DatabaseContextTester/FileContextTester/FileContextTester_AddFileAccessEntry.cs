﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_AddFileAccessEntry : DatabaseTesterBase
    {
        [Fact]
        public async Task AddFileAccessEntry()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            FileAccessModel createModel = new FileAccessModel
            {
                ObjectId = random.Next(),
                ObjectType = (FileAccessObjects)random.Next(1, 5),
            };

            Int32 id = await storage.AddFileAccessEntry(fileDataModel.Id, createModel);
            FileAccessEntryDataModel actual = storage.FileAccessEntries.FirstOrDefault(x => x.Id == id);

            Assert.NotNull(actual);

            Assert.Equal(createModel.ObjectId, actual.ObjectId);
            Assert.Equal(createModel.ObjectType, actual.ObjectType);
        }
    }
}
