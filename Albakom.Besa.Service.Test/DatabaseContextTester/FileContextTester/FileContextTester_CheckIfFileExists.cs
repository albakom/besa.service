﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_CheckIfFileExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfFileExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);

            List<FileDataModel> files = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel file = base.GenerateFileDataModel(random);
                files.Add(file);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            List<Int32> fileIds = new List<int>(storage.Files.Select(x => x.Id));

            Int32 idToPass = fileIds[random.Next(0, fileIds.Count)];
            Int32 idToFail = fileIds.Count + 1;
            while (fileIds.Contains(idToFail) == true)
            {
                idToFail = random.Next();
            }

            Boolean passResult = await storage.CheckIfFileExists(idToPass);
            Boolean failResult = await storage.CheckIfFileExists(idToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
