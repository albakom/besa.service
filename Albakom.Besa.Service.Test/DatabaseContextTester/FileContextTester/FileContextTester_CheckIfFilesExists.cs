﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_CheckIfFilesExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfFilesExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);

            List<FileDataModel> files = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel file = base.GenerateFileDataModel(random);
                files.Add(file);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            List<Int32> fileIds = new List<int>(storage.Files.Select(x => x.Id));

            Int32 skipAmount = random.Next(1, fileAmount / 2);
            Int32 takeAmount = random.Next(1, fileAmount / 2);

            List<Int32> idsToPass = new List<int>(fileIds.Skip(skipAmount).Take(takeAmount));
            List<Int32> idsToFail = new List<int>(idsToPass);

            Int32 unknownId = random.Next();
            while(fileIds.Contains(unknownId) == true)
            {
                unknownId = random.Next();
            }

            idsToFail.Add(unknownId);

            Boolean passResult = await storage.CheckIfFileExists(idsToPass);
            Boolean failResult = await storage.CheckIfFileExists(idsToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
