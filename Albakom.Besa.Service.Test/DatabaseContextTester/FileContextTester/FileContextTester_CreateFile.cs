﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_CreateFile : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateFile()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);

            FileCreateModel createModel = new FileCreateModel
            {
                CreatedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                Extention = $"jpg-{random.Next()}",
                Name = $"Datei-{random.Next()}",
                MimeType = $"random-type-{random.Next()}",
                Size = random.Next(),
                StorageUrl = $"http://random-url.de/{random.Next()}",
                Type = FileTypes.Image,
            };

            Int32 id = await storage.CreateFile(createModel);

            FileDataModel actualModel = storage.Files.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(actualModel);

            Assert.Equal(id, actualModel.Id);
            Assert.Equal(createModel.CreatedAt, actualModel.CreatedAt);
            Assert.Equal(createModel.Extention, actualModel.Extention);
            Assert.Equal(createModel.Name, actualModel.Name);
            Assert.Equal(createModel.MimeType, actualModel.MimeType);
            Assert.Equal(createModel.Size, actualModel.Size);
            Assert.Equal(createModel.StorageUrl, actualModel.StorageUrl);
            Assert.Equal(createModel.Type, actualModel.Type);
        }
    }
}
