﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_DeleteFile : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteFile()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);

            List<FileDataModel> files = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel file = base.GenerateFileDataModel(random);
                files.Add(file);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            List<Int32> fileIds = new List<int>(storage.Files.Select(x => x.Id));

            Int32 idToDelete = fileIds[random.Next(0, fileIds.Count)];

            Boolean result = await storage.DeleteFile(idToDelete);

            Assert.True(result);

            foreach (Int32 fileId in fileIds)
            {
                FileDataModel fileData = storage.Files.FirstOrDefault(x => x.Id == fileId);
                if(idToDelete == fileId)
                {
                    Assert.Null(fileData);
                }
                else
                {
                    Assert.NotNull(fileData);
                }
            }


          
        }
    }
}
