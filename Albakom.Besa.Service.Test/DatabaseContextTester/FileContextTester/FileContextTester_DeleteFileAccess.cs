﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_DeleteFileAccess : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteFileAccess()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(dataModel);

                if (random.NextDouble() > 0.5)
                {
                    FileAccessEntryDataModel entryDataModel = new FileAccessEntryDataModel
                    {
                        File = fileDataModel,
                        ObjectType = FileAccessObjects.Building,
                        ObjectId = dataModel.Id
                    };

                    storage.FileAccessEntries.Add(entryDataModel);
                    storage.SaveChanges();
                }
            }

            Int32 preEntriesCount = storage.FileAccessEntries.Count(x => x.FileId == fileDataModel.Id);
            Assert.True(preEntriesCount > 0);

            Boolean result = await storage.DeleteFileAccess(fileDataModel.Id);
            Assert.True(result);

            Int32 entriesCount = storage.FileAccessEntries.Count(x => x.FileId == fileDataModel.Id);
            Assert.True(entriesCount == 0);
        }
    }
}
