﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_DeleteFileAcessEntry : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteFileAcessEntry()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12345);

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<Int32> toRemove = new List<Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(dataModel);

                if (random.NextDouble() > 0.5)
                {
                    FileAccessEntryDataModel entryDataModel = new FileAccessEntryDataModel
                    {
                        File = fileDataModel,
                        ObjectType = FileAccessObjects.Building,
                        ObjectId = dataModel.Id
                    };

                    storage.FileAccessEntries.Add(entryDataModel);
                    storage.SaveChanges();

                    if(random.NextDouble() > 0.5)
                    {
                        toRemove.Add(entryDataModel.Id);
                    }
                }
            }

            foreach (Int32 id in toRemove)
            {
                Boolean result = await storage.DeleteFileAcessEntry(id);
                Assert.True(result);

                List<FileAccessEntryDataModel> actulItems = storage.FileAccessEntries.Where(x => x.FileId == fileDataModel.Id).ToList();

                foreach (FileAccessEntryDataModel item in actulItems)
                {
                    Assert.False(item.Id == id);
                }
            }
        }
    }
}
