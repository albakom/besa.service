﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetAllAccessEntries : DatabaseTesterBase
    {
        [Fact]
        public async Task GetAllAccessEntries()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, FileAccessEntryDataModel> expected = new Dictionary<int, FileAccessEntryDataModel>();
            Int32 fileAmount = random.Next(3, 10);
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel fileDataModel = base.GenerateFileDataModel(random);
                storage.Files.Add(fileDataModel);
                storage.SaveChanges();

                Int32 buildingAmount = random.Next(10, 30);
                for (int j = 0; j < buildingAmount; j++)
                {
                    BuildingDataModel dataModel = base.GenerateBuilding(random);
                    storage.Buildings.Add(dataModel);
                    storage.SaveChanges();

                    if (random.NextDouble() > 0.5)
                    {
                        FileAccessEntryDataModel entryDataModel = new FileAccessEntryDataModel
                        {
                            File = fileDataModel,
                            ObjectType = FileAccessObjects.Building,
                            ObjectId = dataModel.Id
                        };

                        storage.FileAccessEntries.Add(entryDataModel);
                        storage.SaveChanges();

                        expected.Add(entryDataModel.Id, entryDataModel);
                    }
                }
            }

            IEnumerable<FileAccessWithIdModel> actual = await storage.GetAllAccessEntries();

            Assert.NotNull(actual);

            Assert.Equal(expected.Count, actual.Count());

            foreach (FileAccessWithIdModel actualItem in actual)
            {
                Assert.NotNull(actualItem);
                Assert.True(expected.ContainsKey(actualItem.Id));

                FileAccessEntryDataModel expectedItem = expected[actualItem.Id];
                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.ObjectId, actualItem.ObjectId);
                Assert.Equal(expectedItem.ObjectType, actualItem.ObjectType);
            }

        }
    }
}
