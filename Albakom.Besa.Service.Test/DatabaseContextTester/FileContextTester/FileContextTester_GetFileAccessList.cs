﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetFileAccessList : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFileAccessList()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, FileAccessEntryDataModel> expected = new Dictionary<int, FileAccessEntryDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(dataModel);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    FileAccessEntryDataModel entryDataModel = new FileAccessEntryDataModel
                    {
                        File = fileDataModel,
                        ObjectType = FileAccessObjects.Building,
                        ObjectId = dataModel.Id
                    };

                    storage.FileAccessEntries.Add(entryDataModel);
                    storage.SaveChanges();

                    expected.Add(entryDataModel.Id, entryDataModel);
                }
            }

            IEnumerable<FileAccessWithIdModel> actual = await storage.GetFileAccessList(fileDataModel.Id);

            Assert.NotNull(actual);

            Assert.Equal(expected.Count, actual.Count());

            foreach (FileAccessWithIdModel actualItem in actual)
            {
                Assert.NotNull(actualItem);
                Assert.True(expected.ContainsKey(actualItem.Id));

                FileAccessEntryDataModel expectedItem = expected[actualItem.Id];
                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.ObjectId, actualItem.ObjectId);
                Assert.Equal(expectedItem.ObjectType, actualItem.ObjectType);
            }
        }
    }
}
