﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetFileDetails : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFileDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>(buildingAmount);
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel dataModel = base.GenerateBuilding(random);
                buildings.Add(dataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            ContactInfoDataModel owner = base.GenerateContactInfo(random);

            storage.ContactInfos.AddRange(customer, owner);
            storage.SaveChanges();

            BranchOffJobDataModel branchOffJobDataModel = new BranchOffJobDataModel { Building = buildings[random.Next(0, buildings.Count)] };
            ConnectBuildingJobDataModel connectBuildingJobDataModel = new ConnectBuildingJobDataModel { Building = buildings[random.Next(0, buildings.Count)], Owner = owner };
            InjectJobDataModel injectJobDataModel = new InjectJobDataModel { Building = buildings[random.Next(0, buildings.Count)], CustomerContact = customer };

            storage.Jobs.AddRange(branchOffJobDataModel, connectBuildingJobDataModel, injectJobDataModel);
            storage.SaveChanges();

            BuildingDataModel randomBuilding = buildings[random.Next(0, buildings.Count)];

            List<FileAccessEntryDataModel> accessList = new List<FileAccessEntryDataModel>(4);
            accessList.Add(new FileAccessEntryDataModel { File = fileDataModel, ObjectType = FileAccessObjects.Building, ObjectId = randomBuilding.Id });
            accessList.Add(new FileAccessEntryDataModel { File = fileDataModel, ObjectType = FileAccessObjects.BranchOffJob, ObjectId = branchOffJobDataModel.Id });
            accessList.Add(new FileAccessEntryDataModel { File = fileDataModel, ObjectType = FileAccessObjects.HouseConnectionJob, ObjectId = connectBuildingJobDataModel.Id });
            accessList.Add(new FileAccessEntryDataModel { File = fileDataModel, ObjectType = FileAccessObjects.InjectJob, ObjectId = injectJobDataModel.Id });
     
            storage.FileAccessEntries.AddRange(accessList);
            storage.SaveChanges();

            Dictionary<Int32, FileAccessOverviewModel> expectedAccessList = new Dictionary<int, FileAccessOverviewModel>(accessList.Count);
            expectedAccessList.Add(accessList[0].Id, new FileAccessOverviewModel { ObjectId = accessList[0].ObjectId, ObjectType = accessList[0].ObjectType, ObjectName = randomBuilding.StreetName });
            expectedAccessList.Add(accessList[1].Id, new FileAccessOverviewModel { ObjectId = accessList[1].ObjectId, ObjectType = accessList[1].ObjectType, ObjectName = branchOffJobDataModel.Building.StreetName });
            expectedAccessList.Add(accessList[2].Id, new FileAccessOverviewModel { ObjectId = accessList[2].ObjectId, ObjectType = accessList[2].ObjectType, ObjectName = connectBuildingJobDataModel.Building.StreetName });
            expectedAccessList.Add(accessList[3].Id, new FileAccessOverviewModel { ObjectId = accessList[3].ObjectId, ObjectType = accessList[3].ObjectType, ObjectName = injectJobDataModel.Building.StreetName });


            FileDetailModel actualModel = await storage.GetFileDetails(fileDataModel.Id);

            Assert.NotNull(actualModel);

            Assert.Equal(fileDataModel.Id, actualModel.Id);
            Assert.Equal(fileDataModel.Extention, actualModel.Extention);
            Assert.Equal(fileDataModel.Name, actualModel.Name);
            Assert.Equal(fileDataModel.Size, actualModel.Size);
            Assert.Equal(fileDataModel.Type, actualModel.Type);

            Assert.NotNull(actualModel.AccessControl);

            Assert.Equal(accessList.Count, actualModel.AccessControl.Count());

            foreach (FileAccessOverviewModel actualItem in actualModel.AccessControl)
            {
                Assert.NotNull(actualItem);

                FileAccessOverviewModel expectedItem = expectedAccessList.Values.FirstOrDefault(x => x.ObjectId == actualItem.ObjectId && x.ObjectType == actualItem.ObjectType);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.ObjectType, actualItem.ObjectType);
                Assert.Equal(expectedItem.ObjectId, actualItem.ObjectId);
                Assert.Equal(expectedItem.ObjectName, actualItem.ObjectName);
            }

        }
    }
}
