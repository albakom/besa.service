﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetFileInfo : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFileInfo()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            FileModel actualModel = await storage.GetFileInfo(fileDataModel.Id);

            Assert.NotNull(actualModel);

            Assert.Equal(fileDataModel.Id, actualModel.Id);
            Assert.Equal(fileDataModel.Extention, actualModel.Extention);
            Assert.Equal(fileDataModel.Name, actualModel.Name);
            Assert.Equal(fileDataModel.MimeType, actualModel.MimeType);
            Assert.Equal(fileDataModel.Size, actualModel.Size);
            Assert.Equal(fileDataModel.StorageUrl, actualModel.StorageUrl);
            Assert.Equal(fileDataModel.Type, actualModel.Type);
        }
    }
}
