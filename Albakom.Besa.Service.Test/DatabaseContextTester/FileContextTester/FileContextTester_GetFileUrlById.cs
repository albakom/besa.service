﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetFileUrlById : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFileUrlById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);

            Dictionary<Int32, String> expectedUrls = new Dictionary<int, string>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel file = base.GenerateFileDataModel(random);
                storage.Files.Add(file);
                storage.SaveChanges();

                expectedUrls.Add(file.Id,file.StorageUrl);
            }

            foreach (KeyValuePair<Int32,String> expected in expectedUrls)
            {
                String actual = await storage.GetFileUrlById(expected.Key);
                Assert.Equal(expected.Value, actual);
            }
        }
    }
}
