﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_GetFileUrlsByJobId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetFileUrlsByJobId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50,100);
            List<String> expectedUrls = new List<string>(fileAmount);

            BuildingDataModel building = base.GenerateBuilding(random);
            storage.Buildings.Add(building);
            storage.SaveChanges();

            BranchOffJobDataModel branchOffJobDataModel = new BranchOffJobDataModel
            {
                Building = building,
            };

            storage.BranchOffJobs.Add(branchOffJobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            BranchOffJobFinishedDataModel branchOffJobFinished = new BranchOffJobFinishedDataModel
            {
                Job = branchOffJobDataModel,
                DoneBy = jobber,
                FinishedAt = DateTime.Now,
            };

            storage.FinishedBranchOffJobs.Add(branchOffJobFinished);
            storage.SaveChanges();
                

            List<FileDataModel> files = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel file = base.GenerateFileDataModel(random);
                file.RelatedJob = branchOffJobFinished;
                files.Add(file);

                expectedUrls.Add(file.StorageUrl);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            IEnumerable<String> actual = await storage.GetFileUrlsByJobId(branchOffJobDataModel.Id);

            Assert.NotNull(actual);
            Assert.Equal(expectedUrls.Count, actual.Count());

            foreach (String actaulItem in actual)
            {
                Int32 index = expectedUrls.IndexOf(actaulItem);
                Assert.True(index >= 0);
            }
        }
    }
}
