﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_RenameFile : DatabaseTesterBase
    {
        [Fact]
        public async Task RenameFile()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FileDataModel fileDataModel = base.GenerateFileDataModel(random);
            storage.Files.Add(fileDataModel);
            storage.SaveChanges();

            Int32 fileId = fileDataModel.Id;
            String newName = $"Ein wirklicher Toller name {random.Next()}";

            Boolean result = await storage.RenameFile(fileId, newName);

            Assert.True(result);

            FileDataModel actualData = storage.Files.FirstOrDefault(x => x.Id == fileId);

            Assert.NotNull(actualData);
            Assert.Equal(newName, actualData.Name);
        }
    }
}
