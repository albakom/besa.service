﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class FileContextTester_SearchFiles : DatabaseTesterBase
    {
        [Fact]
        public async Task SearchFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 fileAmount = random.Next(50, 250);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            storage.Buildings.Add(building);
            storage.SaveChanges();

            String query = "meine Abfrage";

            List<FileDataModel> validItems = new List<FileDataModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel dataModel = base.GenerateFileDataModel(random);
                storage.Files.Add(dataModel);

                if(random.NextDouble() > 0.5)
                {
                    dataModel.Name = $"{query}-{random.Next()}";
                    if (random.NextDouble() > 0.5)
                    {
                        BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                        {
                            //  Building = building,
                        };

                        storage.BranchOffJobs.Add(jobDataModel);
                        storage.SaveChanges();

                        BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                        {
                            Job = jobDataModel,
                            DoneBy = jobber,
                        };

                        storage.FinishedBranchOffJobs.Add(finishedDataModel);
                        storage.SaveChanges();

                        dataModel.RelatedJob = finishedDataModel;
                    }
                    else
                    {
                        validItems.Add(dataModel);
                    }
                }

                storage.SaveChanges();
            }

            Int32 start = validItems.Count / 4;
            Int32 amount = random.Next(1, validItems.Count / 2);

            IEnumerable<FileOverviewModel> actual = await storage.SearchFiles(query, start, amount);

            Assert.NotNull(actual);
            Assert.Equal(amount, actual.Count());

            List<FileOverviewModel> acutalItemsAsList = actual.ToList();
            List<FileDataModel> expectedItemsSorted = validItems.OrderBy(x => x.Name).Skip(start).Take(amount).ToList();

            for (int i = 0; i < amount; i++)
            {
                FileOverviewModel acutalItem = acutalItemsAsList[i];
                FileDataModel expectedItem = expectedItemsSorted[i];

                Assert.Equal(expectedItem.Id, acutalItem.Id);
                Assert.Equal(expectedItem.Name, acutalItem.Name);
                Assert.Equal(expectedItem.Extention, acutalItem.Extention);
                Assert.Equal(expectedItem.Size, acutalItem.Size);
                Assert.Equal(expectedItem.Type, acutalItem.Type);
            }
        }
    }
}
