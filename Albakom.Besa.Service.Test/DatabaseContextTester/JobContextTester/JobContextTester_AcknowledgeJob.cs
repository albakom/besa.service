﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_AcknowledgeJob : DatabaseTesterBase
    {
        [Fact]
        public async Task AcknowledgeJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            UserDataModel acceptor = base.GenerateUserDataModel(null);
            storage.Users.Add(acceptor);

            BranchOffJobDataModel job = buildings[random.Next(0, buildings.Count)].BranchOffJob;
            storage.SaveChanges();

            BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
            {
                Comment = $"Mein Kommentar {random.Next()}",
                DuctChanged = random.NextDouble() > 0.5,
                DuctChangedDescription = $"Rohr anders, da {random.Next()}",
                Job = job,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                ProblemHappend = random.NextDouble() > 0.5,
                ProblemHappendDescription = $"Problem da {random.Next()}",
                DoneBy = jobber
            };

            storage.FinishedBranchOffJobs.Add(finishedDataModel);
            storage.SaveChanges();

            AcknowledgeJobModel ackModel = new AcknowledgeJobModel
            {
              JobId = job.Id,
              Timestamp = DateTimeOffset.Now,
              UserAuthId = acceptor.AuthServiceId,
            };

            Boolean result = await storage.AcknowledgeJob(ackModel);
            Assert.True(result);

            BranchOffJobFinishedDataModel actual = storage.FinishedBranchOffJobs.FirstOrDefault(x => x.JobId == job.Id);
            Assert.NotNull(actual);

            Assert.NotNull(actual.AcceptedAt);
            Assert.NotNull(actual.AccepterId);

            Assert.Equal(ackModel.Timestamp, actual.AcceptedAt.Value);
        }
    }
}
