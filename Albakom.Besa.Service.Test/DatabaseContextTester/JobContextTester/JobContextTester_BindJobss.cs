﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_BindJobss : DatabaseTesterBase
    {
        [Fact]
        public async Task BindJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));

            Int32 skipAmount = random.Next(1, buildingAmount / 2);
            Int32 takeAmount = random.Next(1, buildingAmount / 2);

            List<Int32> jobsToBound = jobIds.Skip(skipAmount).Take(takeAmount).ToList();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            DateTimeOffset finsihedTill = DateTimeOffset.Now.AddDays(8);

            Int32 collectionJobId =  await storage.BindJobs(jobsToBound, company.Id, finsihedTill,"Testname");

            CollectionJobDataModel collectionJob = storage.CollectionJobs.FirstOrDefault(x => x.Id == collectionJobId);
            Assert.NotNull(collectionJob);

            Assert.Equal(collectionJobId, collectionJob.Id);
            Assert.Equal(finsihedTill, collectionJob.FinishedTill);
            Assert.Equal(company.Id, collectionJob.CompanyId);

            foreach (JobDataModel item in storage.Jobs.ToList())
            {
                if(jobsToBound.Contains(item.Id) == true)
                {
                    Assert.True(item.CollectionId.HasValue);
                    Assert.Equal(collectionJobId, item.CollectionId.Value);
                }
                else
                {
                    Assert.False(item.CollectionId.HasValue);
                }
            }


        }
    }
}
