﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfActivationJobExitsByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfActivationJobExitsByFlatId|JobContextTester")]
        public async Task CheckIfActivationJobExitsByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 30);

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            storage.Buildings.Add(building);

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {
                FlatDataModel flatData = base.GenerateFlatDataModel(random);
                flatData.Building = building;

                storage.Flats.Add(flatData);

                Boolean withJob = random.NextDouble() > 0.5;
                if(withJob == true)
                {
                    ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flatData, Customer = customer };
                    storage.ActivationJobs.Add(jobDataModel);
                    storage.SaveChanges();
                }

                expectedResults.Add(flatData.Id, withJob);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedItem in expectedResults)
            {
                Boolean actual = await storage.CheckIfActivationJobExitsByFlatId(expectedItem.Key);
                Assert.Equal(expectedItem.Value, actual);
            }
        }
    }
}
