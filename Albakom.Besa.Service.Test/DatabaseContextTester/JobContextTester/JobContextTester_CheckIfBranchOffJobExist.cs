﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBranchOffJobExist : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBranchOffJobExist()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfBranchOffJobExist(jobId);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfBranchOffJobExist(random.Next(jobId));
            Assert.False(notExits);
        }
    }
}
