﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBranchoffJobIsBoundByBuildingId : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBranchoffJobIsBoundByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Boolean> expectedResult = new Dictionary<Int32, Boolean>();

            CollectionJobDataModel collectionJob = new CollectionJobDataModel
            {
                Company = company,
                FinishedTill = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                Name = $"Collection-Job {random.Next()}",
            };

            storage.CollectionJobs.Add(collectionJob);
            storage.SaveChanges(); 

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);

                storage.Buildings.Add(building);
                storage.SaveChanges();

                BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                Boolean isBound = random.NextDouble() > 0.5;
                if(isBound == true)
                {
                    jobDataModel.Collection = collectionJob;
                }

                storage.BranchOffJobs.Add(jobDataModel);
                storage.SaveChanges();
                expectedResult.Add(building.Id, isBound);
            }

            foreach (KeyValuePair<Int32, Boolean> item in expectedResult)
            {
                Boolean actual = await storage.CheckIfBranchoffJobIsBoundByBuildingId(item.Key);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
