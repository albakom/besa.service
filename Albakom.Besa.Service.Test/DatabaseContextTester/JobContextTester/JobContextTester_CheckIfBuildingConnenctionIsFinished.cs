﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBuildingConnenctionIsFinished : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingConnenctionIsFinished_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingId = random.Next();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.Id = buildingId;

            storage.SaveChanges();

            ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel { Building = building };
            storage.ConnectBuildingJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ConnectBuildingFinishedDataModel finishedDataModel = new ConnectBuildingFinishedDataModel
            {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                DoneBy = jobber,
                AcceptedAt = DateTimeOffset.Now.AddDays(-random.Next(1, 3)),
                Accepter = jobber,
            };

            storage.FinishedConnectionBuildingJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfBuildingConnenctionIsFinished(buildingId);
            Assert.True(exits);
        }

        [Fact]
        public async Task CheckIfBuildingConnenctionIsFinished_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingId = random.Next();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.Id = buildingId;

            storage.SaveChanges();

            ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel { Building = building };
            storage.ConnectBuildingJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            //ConnectBuildingFinishedDataModel finishedDataModel = new ConnectBuildingFinishedDataModel
            //{
            //    Job = jobDataModel,
            //    FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
            //    DoneBy = jobber
            //};

            //storage.FinishedConnectionBuildingJobs.Add(finishedDataModel);
            //storage.SaveChanges();

            Boolean exits = await storage.CheckIfBuildingConnenctionIsFinished(buildingId);
            Assert.False(exits);
        }
    }
}
