﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBuildingConnenctionJobExist : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingConnenctionJobExist()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.ConnectBuildingJobs.Add(jobDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfBuildingConnenctionJobExist(jobId);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfBuildingConnenctionJobExist(random.Next(jobId));
            Assert.False(notExits);
        }
    }
}
