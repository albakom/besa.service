﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfCollectionJobExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfCollectionJobExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 collectionJobId = random.Next();

            CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
            collectionJob.Id = collectionJobId;
            storage.CollectionJobs.Add(collectionJob);
            storage.SaveChanges();
          
            Boolean exits = await storage.CheckIfCollectionJobExists(collectionJobId);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfCollectionJobExists(random.Next(collectionJobId));
            Assert.False(notExits);
        }
    }
}
