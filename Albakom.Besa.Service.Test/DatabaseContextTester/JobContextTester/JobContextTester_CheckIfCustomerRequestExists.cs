﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfCustomerRequestExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfCustomerRequestExists()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = base.GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();
            List<Int32> contactIds = contacts.Select(x => x.Id).ToList();

            Int32 buildingAmount = random.Next(3, 10);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            Int32 requestAmount = random.Next(10, 30);
            List<CustomerConnenctionRequestDataModel> requests = new List<CustomerConnenctionRequestDataModel>(requestAmount);
            for (int i = 0; i < requestAmount; i++)
            {
                CustomerConnenctionRequestDataModel requestDataModel = base.GenerateConnenctionRequestDataModel(random, contactIds);
               requestDataModel.Building = buildings[random.Next(0, buildings.Count)];
                requests.Add(requestDataModel);
            }

            storage.CustomerRequests.AddRange(requests);
            storage.SaveChanges();

            List<Int32> ids = new List<int>(requests.Select(x => x.Id));

            Int32 idToPass = ids[random.Next(0, ids.Count)];
            Int32 idToFail = ids.Count + 2;
            while (ids.Contains(idToFail) == true)
            {
                idToFail = random.Next();
            }

            Boolean passResult = await storage.CheckIfCustomerRequestExists(idToPass);
            Boolean failResult = await storage.CheckIfCustomerRequestExists(idToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
