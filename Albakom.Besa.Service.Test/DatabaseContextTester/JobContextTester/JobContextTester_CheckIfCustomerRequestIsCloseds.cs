﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfCustomerRequestIsCloseds : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfCustomerRequestIsClosed()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = base.GenerateContactInfo(random);
                contacts.Add(contact);
            }
            List<Int32> contactIds = contacts.Select(x => x.Id).ToList();

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            storage.Buildings.Add(building);
            storage.SaveChanges();

            CustomerConnenctionRequestDataModel requestDataModel = base.GenerateConnenctionRequestDataModel(random, contactIds);
            requestDataModel.IsClosed = false;
            requestDataModel.Building = building;

            storage.CustomerRequests.Add(requestDataModel);
            storage.SaveChanges();


            Boolean firstResult = await storage.CheckIfCustomerRequestIsClosed(requestDataModel.Id);

            Assert.False(firstResult);

            requestDataModel.IsClosed = true;
            storage.SaveChanges();

            Boolean secondResult = await storage.CheckIfCustomerRequestIsClosed(requestDataModel.Id);
            Assert.True(secondResult);
        }
    }
}
