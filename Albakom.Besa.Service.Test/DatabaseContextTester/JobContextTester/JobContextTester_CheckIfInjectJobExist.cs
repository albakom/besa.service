﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfInjectJobExist : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfInjectJobExist()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);

            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = customer };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfInjectJobExist(jobId);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfInjectJobExist(random.Next(jobId));
            Assert.False(notExits);
        }
    }
}
