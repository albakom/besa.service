﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfInventoryJobExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfInventoryJobExists|JobContextTester")]
        public async Task CheckIfInventoryJobExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 jobAmount = random.Next(10, 20);

            List<Int32> existingIds = new List<int>();
            for (int i = 0; i < jobAmount; i++)
            {

                ContactInfoDataModel issuer = base.GenerateContactInfo(random);

                InventoryJobDataModel jobDataModel = new InventoryJobDataModel
                {
                    IssuerContact = issuer,
                };

                storage.InventoryJobs.Add(jobDataModel);
                storage.SaveChanges();

                existingIds.Add(jobDataModel.Id);
            }

            foreach (Int32 id in existingIds)
            {
                Boolean actual = await storage.CheckIfInventoryJobExists(id);
                Assert.True(actual);
            }

            Int32 notExistingId = existingIds.Count + 1;
            while (existingIds.Contains(notExistingId))
            {
                notExistingId = random.Next();
            }

            Boolean notExistingResult = await storage.CheckIfInventoryJobExists(notExistingId);
            Assert.False(notExistingResult);
        }
    }
}
