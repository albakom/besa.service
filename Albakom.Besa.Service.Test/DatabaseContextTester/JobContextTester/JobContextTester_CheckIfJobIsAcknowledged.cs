﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfJobIsAcknowledged : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfJobIsAcknowledged()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            UserDataModel acceptor = base.GenerateUserDataModel(null);
            storage.Users.Add(acceptor);

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            List<JobDataModel> finishedJobs = new List<JobDataModel>();
            List<JobDataModel> acceptedJobs = new List<JobDataModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);

                if (random.NextDouble() > 0.5)
                {
                    BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                    {
                        Job = jobModel,
                        DoneBy = jobber,
                        FinishedAt = DateTimeOffset.Now,
                    };

                    storage.FinishedBranchOffJobs.Add(finishedDataModel);
                    finishedJobs.Add(jobModel);

                    if(random.NextDouble() > 0.5)
                    {
                        finishedDataModel.Accepter = acceptor;
                        finishedDataModel.AcceptedAt = DateTimeOffset.Now;

                        acceptedJobs.Add(jobModel);
                    }
                }
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> finishedJobIds = finishedJobs.Select(x => x.Id).ToList();
            List<Int32> acceptedJobIds = acceptedJobs.Select(x => x.Id).ToList();

            Int32 idToPass = acceptedJobIds[random.Next(0, acceptedJobIds.Count)];
            Int32 idToFail = finishedJobIds[random.Next(0, finishedJobIds.Count)];
            while (acceptedJobIds.Contains(idToFail) == true)
            {
                idToFail = finishedJobIds[random.Next(0, finishedJobIds.Count)];
            }

            Boolean passResult = await storage.CheckIfJobIsAcknowledged(idToPass);
            Boolean failResult = await storage.CheckIfJobIsAcknowledged(idToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
