﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfJobIsFinished : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfJobIsFinished()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            List<JobDataModel> finishedJobs = new List<JobDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);

                if (random.NextDouble() > 0.5)
                {

                    BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                    {
                        Job = jobModel,
                        DoneBy = jobber,
                        FinishedAt = DateTimeOffset.Now,
                    };

                    storage.FinishedBranchOffJobs.Add(finishedDataModel);
                    finishedJobs.Add(jobModel);
                }

            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));
            List<Int32> finishedJobIds = finishedJobs.Select(x => x.Id).ToList();

            Int32 idToPass = finishedJobIds[random.Next(0, finishedJobIds.Count)];
            Int32 idToFail = jobIds[random.Next(0, jobIds.Count)];
            while (finishedJobIds.Contains(idToFail) == true)
            {
                idToFail = jobIds[random.Next(0, jobIds.Count)];
            }

            Boolean passResult = await storage.CheckIfJobIsFinished(idToPass);
            Boolean failResult = await storage.CheckIfJobIsFinished(idToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
