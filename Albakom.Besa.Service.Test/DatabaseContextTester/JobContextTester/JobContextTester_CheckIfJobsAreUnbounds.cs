﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfJobsAreUnbounds : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfJobsAreUnbound()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));

            List<Int32> boundedJobs = new List<int>();
            List<Int32> unboundedJobs = new List<int>();

            CollectionJobDataModel collectionJob = new CollectionJobDataModel
            {
                FinishedTill = DateTimeOffset.Now.AddDays(7),
                Company = base.GenerateCompanyDataModel(),
            };

            storage.CollectionJobs.Add(collectionJob);
            storage.SaveChanges();

            foreach (BranchOffJobDataModel item in storage.BranchOffJobs)
            {
                if(random.NextDouble() > 0.5)
                {
                    collectionJob.Jobs.Add(item);
                    item.CollectionId = collectionJob.Id;
                    item.Collection = collectionJob;

                    boundedJobs.Add(item.Id);
                }
                else
                {
                    unboundedJobs.Add(item.Id);
                }
            }

            storage.SaveChanges();

            List<Int32> idsToPass = new List<int>(unboundedJobs);
            List<Int32> idsToFail = new List<int>(unboundedJobs);
            idsToFail.Add(boundedJobs[0]);

            Boolean passResult = await storage.CheckIfJobsAreUnbound(idsToPass);
            Boolean failResult = await storage.CheckIfJobsAreUnbound(idsToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
