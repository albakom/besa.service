﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfJobsExists : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfJobsExists()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));

            Int32 skipAmount = random.Next(1, buildingAmount / 2);
            Int32 takeAmount = random.Next(1, buildingAmount / 2);

            List<Int32> idsToPass = new List<int>(jobIds.Skip(skipAmount).Take(takeAmount));
            List<Int32> idsToFail = new List<int>(idsToPass);

            Int32 unknownId = random.Next();
            while(jobIds.Contains(unknownId) == true)
            {
                unknownId = random.Next();
            }

            idsToFail.Add(unknownId);

            Boolean passResult = await storage.CheckIfJobsExists(idsToPass);
            Boolean failResult = await storage.CheckIfJobsExists(idsToFail);

            Assert.True(passResult);
            Assert.False(failResult);
        }
    }
}
