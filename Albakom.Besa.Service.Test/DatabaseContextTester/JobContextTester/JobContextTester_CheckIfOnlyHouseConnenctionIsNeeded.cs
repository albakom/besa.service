﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfOnlyHouseConnenctionIsNeeded : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfOnlyHouseConnenctionIsNeeded()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, Boolean>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                Boolean onlyHouseConnection = random.NextDouble() > 0.5;
                ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                {
                    Building = building,
                    Owner = base.GenerateContactInfo(random),
                    OnlyHouseConnection = onlyHouseConnection,
                };

                storage.Jobs.Add(jobModel);
                storage.SaveChanges();
                expectedResults.Add(jobModel.Id, onlyHouseConnection);
            }

            foreach (KeyValuePair<Int32, Boolean> expectedResult in expectedResults)
            {
                Boolean actual = await storage.CheckIfOnlyHouseConnenctionIsNeeded(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
