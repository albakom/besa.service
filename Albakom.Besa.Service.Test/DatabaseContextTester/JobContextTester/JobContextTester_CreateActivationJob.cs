﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateActivationJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateActivationJob|JobContextTester")]
        public async Task CreateActivationJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            List<FlatDataModel> flats = new List<FlatDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 flatAmount = random.Next(2, 5);
                for (int k = 0; k < flatAmount; k++)
                {
                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();
                    flats.Add(flatDataModel);

                }
            }

            ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customerContact);
            storage.SaveChanges();

            ArticleDataModel device = base.GenerateArticle(random);
            storage.Articles.Add(device);
            storage.SaveChanges();

            FlatDataModel flat = flats[random.Next(0, flats.Count)];
            ActivationJobCreateModel jobCreateModel = new ActivationJobCreateModel
            {
                FlatId = flat.Id,
                CustomerContactId = customerContact.Id,
                AddressRequirement = ActivationJobIpAddressRequirements.Slash28,
                UnchagedIpAdress = random.NextDouble() > 0.5,
                DeviceId = device.Id,
            };

            Int32 id = await storage.CreateActivationJob(jobCreateModel);

            ActivationJobDataModel dataModel = storage.ActivationJobs.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(flat.Id, dataModel.FlatId);
            Assert.Equal(customerContact.Id, dataModel.CustomerId);
            Assert.Equal(jobCreateModel.AddressRequirement, dataModel.AddressRequirement);
            Assert.Equal(jobCreateModel.DeviceId, dataModel.DeviceId);
            Assert.Equal(jobCreateModel.UnchagedIpAdress, dataModel.UnchagedIpAdress);

            Assert.Null(dataModel.CollectionId);
            Assert.Null(dataModel.FinishedModel);
        }
    }
}
