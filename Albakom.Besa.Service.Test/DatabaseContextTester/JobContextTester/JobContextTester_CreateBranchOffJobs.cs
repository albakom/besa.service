﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateBranchOffJobs : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateBranchOffJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(12345);

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            List<BranchOffJobCreateModel> createJobModels = new List<BranchOffJobCreateModel>();
            Dictionary<Int32, Boolean> branchOffJobs = new Dictionary<int, bool>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                storage.Buildings.Add(building);
                storage.SaveChanges();

                Boolean jobExits = false;
                if(random.NextDouble() > 0.5 )
                {
                    BranchOffJobCreateModel jobCreateModel = new BranchOffJobCreateModel
                    {
                        BuildingId = building.Id,
                    };

                    createJobModels.Add(jobCreateModel);
                    jobExits = true;
                }

                branchOffJobs.Add(building.Id, jobExits);
            }

            await storage.CreateBranchOffJobs(createJobModels);

            HashSet<Int32> usedIds = new HashSet<int>();
            foreach (BuildingDataModel item in storage.Buildings.ToList())
            {
                Assert.True(branchOffJobs.ContainsKey(item.Id));

                Boolean branchJobExists = branchOffJobs[item.Id];
                Assert.Equal(branchJobExists, item.BranchOffJob != null);

                if(item.BranchOffJob != null)
                {
                    Assert.DoesNotContain(item.BranchOffJob.Id, usedIds);
                    usedIds.Add(item.BranchOffJob.Id);
                }
            }
        }
    }
}
