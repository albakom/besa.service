﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateContact : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateContact()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 customerContactID = random.Next();

            PersonInfo createModel = new PersonInfo
            {
                Address = new AddressModel
                {
                    City = $"Muserstadt {random.Next()}",
                    PostalCode = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Street = $"Straße Nr. {random.Next()}",
                    StreetNumber = $"{random.Next(1, 100)}",
                },
                CompanyName = random.NextDouble() > 0.5 ? $"Firma Nr {random.Next()}" : String.Empty,
                EmailAddress = $"test@test-{random.Next()}.de",
                Phone = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                Lastname = $"Nachname {random.Next()}",
                Surname = $"Vorname {random.Next()}",
            };

            Int32 contactId = await storage.CreateContact(createModel);

            ContactInfoDataModel actual = storage.ContactInfos.FirstOrDefault(x => x.Id == contactId);
            Assert.NotNull(actual);

            Assert.Equal(createModel.Lastname, actual.Lastname);
            Assert.Equal(createModel.Phone, actual.Phone);
            Assert.Equal(createModel.Surname, actual.Surname);
            Assert.Equal(createModel.Type, actual.Type);
            Assert.Equal(createModel.EmailAddress, actual.EmailAddress);
            Assert.Equal(createModel.CompanyName, actual.CompanyName);

            Assert.NotNull(createModel.Address);
            Assert.Equal(createModel.Address.City, actual.City);
            Assert.Equal(createModel.Address.Street, actual.Street);
            Assert.Equal(createModel.Address.StreetNumber, actual.StreetNumber);
            Assert.Equal(createModel.Address.PostalCode, actual.PostalCode);
        }
    }
}
