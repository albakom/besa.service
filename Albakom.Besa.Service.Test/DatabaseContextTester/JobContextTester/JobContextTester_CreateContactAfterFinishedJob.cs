﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateContactAfterFinishedJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateContactAfterFinishedJob|JobContextTester")]
        public async Task CreateContactAfterFinishedJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            ContactInfoDataModel contact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(contact);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(4, 10);
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel procedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                procedureDataModel.Building = building;
                procedureDataModel.Owner = contact;

                storage.BuildingConnectionProcedures.Add(procedureDataModel);
                storage.SaveChanges();

                CreateContactAfterFinishedJobModel createContactAfterFinishedJobModel = new CreateContactAfterFinishedJobModel
                {
                    BuildingId = building.Id,
                    FlatId = flat.Id,
                    RelatedProcedureId = procedureDataModel.Id,
                };

                Int32 id = await storage.CreateContactAfterFinishedJob(createContactAfterFinishedJobModel);
                ContactAfterFinishedJobDataModel jobDataModel = storage.ContactAfterFinishedJobs.FirstOrDefault(x => x.Id == id);

                Assert.NotNull(jobDataModel);

                Assert.Equal(id, jobDataModel.Id);
                Assert.Equal(createContactAfterFinishedJobModel.BuildingId, jobDataModel.BuildingId);
                Assert.Equal(createContactAfterFinishedJobModel.FlatId, jobDataModel.FlatId);
                Assert.Equal(createContactAfterFinishedJobModel.RelatedProcedureId, jobDataModel.RelatedProcedureId);
                Assert.Equal(contact.Id, jobDataModel.CustomerContactId);

                Int32 secondId = await storage.CreateContactAfterFinishedJob(createContactAfterFinishedJobModel);
                Assert.True(secondId <= 0);
            }
        }
    }
}
