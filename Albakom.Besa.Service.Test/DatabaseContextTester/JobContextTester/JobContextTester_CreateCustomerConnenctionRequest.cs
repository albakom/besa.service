﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateCustomerConnenctionRequest : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateCustomerConnenctionRequest|JobContextTester")]
        public async Task CreateCustomerConnenctionRequest()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 connectionAmount = random.Next(5, 15);
            for (int j = 0; j < connectionAmount; j++)
            {
                Int32 customerContactID = random.Next();

                ContactInfoDataModel contactInfo = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(contactInfo);

                List<ContactInfoDataModel> otherContactInfos = new List<ContactInfoDataModel>();
                for (int i = 0; i < 4; i++)
                {
                    ContactInfoDataModel otherInfo = base.GenerateContactInfo(random);
                    storage.ContactInfos.Add(contactInfo);
                    otherContactInfos.Add(otherInfo);
                }
                storage.SaveChanges();

                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                buildingDataModel.Flats.Add(flatDataModel);
                storage.Flats.Add(flatDataModel);

                storage.SaveChanges();

                CustomerConnectRequestCreateModel model = new CustomerConnectRequestCreateModel
                {
                    FlatId = flatDataModel.Id,
                    BuildingId = flatDataModel.BuildingId,
                    CustomerPersonId = otherContactInfos[0].Id,
                    PropertyOwnerPersonId = otherContactInfos[1].Id,
                    ArchitectPersonId = otherContactInfos[2].Id,
                    WorkmanPersonId = otherContactInfos[3].Id,
                    DescriptionForHouseConnection = $"Tolle Beschreibung für Hausanschluss mit der Nummer {random.Next()}",
                    OnlyHouseConnection = random.NextDouble() > 0.5,
                    SubscriberEndpointNearConnectionPoint = new ExplainedBooleanModel
                    {
                        Value = random.NextDouble() > 0.5,
                        Description = $"ANschlussdose Beschreibungspunkt {random.Next()}",
                    },
                    ActivePointNearSubscriberEndpoint = new ExplainedBooleanModel
                    {
                        Value = random.NextDouble() > 0.5,
                        Description = $"FritzBox ist auch neben an. Da {random.Next()}",
                    },
                    PowerForActiveEquipment = new ExplainedBooleanModel
                    {
                        Value = random.NextDouble() > 0.5,
                        Description = $"Strom für das Gerät ist gleich neben an, da {random.Next()}",
                    },
                    CivilWorkIsDoneByCustomer = random.NextDouble() > 0.5,
                    ConnectioOfOtherMediaRequired = random.NextDouble() > 0.5,
                    DuctAmount = random.NextDouble() > 0.5 ? random.Next(20, 30) + random.NextDouble() : new Nullable<Double>(),
                    SubscriberEndpointLength = random.NextDouble() > 0.5 ? random.Next(50, 60) + random.NextDouble() : new Nullable<Double>(),
                    ConnectionAppointment = DateTimeOffset.Now.AddDays(random.Next(10, 20)),
                    ActivationAsSoonAsPossible = random.NextDouble() > 0.5,
                    InformSalesAfterFinish = random.NextDouble() > 0.5,
                };

                Int32 customerId = await storage.CreateCustomerConnenctionRequest(model);

                CustomerConnenctionRequestDataModel actual = storage.CustomerRequests.FirstOrDefault(x => x.Id == customerId);
                Assert.NotNull(actual);

                Assert.Equal(model.FlatId, actual.FlatId);
                Assert.Equal(model.BuildingId, actual.BuildingId);
                Assert.Equal(model.CustomerPersonId, actual.CustomerPersonId);

                Assert.Equal(model.PropertyOwnerPersonId, actual.PropertyOwnerPersonId);
                Assert.Equal(model.ArchitectPersonId, actual.ArchitectPersonId);
                Assert.Equal(model.WorkmanPersonId, actual.WorkmanPersonId);
                Assert.Equal(model.DescriptionForHouseConnection, actual.DescriptionForHouseConnection);
                Assert.Equal(model.OnlyHouseConnection, actual.OnlyHouseConnection);
                Assert.Equal(model.SubscriberEndpointNearConnectionPoint.Value, actual.SubscriberEndpointNearConnectionPointValue);
                Assert.Equal(model.SubscriberEndpointNearConnectionPoint.Description, actual.SubscriberEndpointNearConnectionPointDescription);
                Assert.Equal(model.ActivePointNearSubscriberEndpoint.Value, actual.ActivePointNearSubscriberEndpointValue);
                Assert.Equal(model.ActivePointNearSubscriberEndpoint.Description, actual.ActivePointNearSubscriberEndpointDescription);
                Assert.Equal(model.PowerForActiveEquipment.Value, actual.PowerForActiveEquipmentValue);
                Assert.Equal(model.PowerForActiveEquipment.Description, actual.PowerForActiveEquipmentDescription);
                Assert.Equal(model.CivilWorkIsDoneByCustomer, actual.CivilWorkIsDoneByCustomer);
                Assert.Equal(model.ConnectioOfOtherMediaRequired, actual.ConnectioOfOtherMediaRequired);
                Assert.Equal(model.DuctAmount, actual.DuctAmount);
                Assert.Equal(model.SubscriberEndpointLength, actual.SubscriberEndpointLength);
                Assert.Equal(model.ConnectionAppointment, actual.ConnectionAppointment);
                Assert.Equal(model.ActivationAsSoonAsPossible, actual.ActivationAsSoonAsPossible);
                Assert.Equal(model.InformSalesAfterFinish, actual.InformSalesAfterFinish);
            }
        }
    }
}
