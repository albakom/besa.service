﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateHouseConnenctionJob : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateHouseConnenctionJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            Int32 contactInfoAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contactsInfos = new List<ContactInfoDataModel>(contactInfoAmount);

            for (int i = 0; i < contactInfoAmount; i++)
            {
                ContactInfoDataModel contactInfo = base.GenerateContactInfo(random);
                contactsInfos.Add(contactInfo);
            }

            storage.ContactInfos.AddRange(contactsInfos);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            Dictionary<Int32, Boolean> branchOffJobs = new Dictionary<int, bool>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);
                storage.Buildings.Add(building);
                storage.SaveChanges();
            }

            HouseConnenctionJobCreateModel createModel = new HouseConnenctionJobCreateModel
            {
                ArchitectContactId = contactsInfos[random.Next(0, contactsInfos.Count)].Id,
                WorkmanContactId = contactsInfos[random.Next(0, contactsInfos.Count)].Id,
                CustomerContactId = contactsInfos[random.Next(0, contactsInfos.Count)].Id,
                OwnerContactId = contactsInfos[random.Next(0, contactsInfos.Count)].Id,

                BuildingId = buildings[random.Next(0, buildings.Count)].Id,
                CivilWorkIsDoneByCustomer = random.NextDouble() > 0.5,
                ConnectionAppointment = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                ConnectionOfOtherMediaRequired = random.NextDouble() > 0.5,
                OnlyHouseConnection = random.NextDouble() > 0.5,
                Description = $"Testbeschreibung {random.Next()}",
            };

            Int32 id = await storage.CreateHouseConnenctionJob(createModel);

            ConnectBuildingJobDataModel dataModel = storage.ConnectBuildingJobs.FirstOrDefault(x => x.Id == id);

            Assert.NotNull(dataModel);

            Assert.Equal(id, dataModel.Id);
            Assert.Equal(createModel.ArchitectContactId, dataModel.ArchitectContactId);
            Assert.Equal(createModel.WorkmanContactId, dataModel.WorkmanContactId);
            Assert.Equal(createModel.CustomerContactId, dataModel.CustomerContactId);
            Assert.Equal(createModel.OwnerContactId, dataModel.OwnerContactId);

            Assert.Equal(createModel.BuildingId, dataModel.BuildingId);
            Assert.Equal(createModel.CivilWorkIsDoneByCustomer, dataModel.CivilWorkIsDoneByCustomer);
            Assert.Equal(createModel.ConnectionAppointment, dataModel.ConnectionAppointment);
            Assert.Equal(createModel.ConnectionOfOtherMediaRequired, dataModel.ConnectionOfOtherMediaRequired);
            Assert.Equal(createModel.OnlyHouseConnection, dataModel.OnlyHouseConnection);
            Assert.Equal(createModel.Description, dataModel.Description);
        }
    }
}
