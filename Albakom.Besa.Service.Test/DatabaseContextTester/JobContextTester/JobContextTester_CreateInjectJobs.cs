﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateInjectJobs : DatabaseTesterBase
    {
        [Fact]
        public async Task CreateInjectJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);

            BuildingDataModel building = buildings[random.Next(0, buildings.Count)];
            InjectJobCreateModel jobCreateModel = new InjectJobCreateModel
            {
                BuildingId = building.Id,
            };

            Int32 id = await storage.CreateInjectJob(jobCreateModel);

            InjectJobDataModel dataModel = storage.InjectJobs.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(building.Id, dataModel.BuildingId);
            Assert.Null(dataModel.CollectionId);
            Assert.Null(dataModel.FinishedModel);
        }
    }
}
