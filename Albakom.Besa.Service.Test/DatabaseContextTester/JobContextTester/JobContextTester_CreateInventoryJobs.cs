﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateInventoryJobs : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateInventoryJob|JobContextTester")]
        public async Task CreateInventoryJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 articleAmount = random.Next(30, 100);
            List<ArticleDataModel> articles = new List<ArticleDataModel>(articleAmount);
            for (int i = 0; i < articleAmount; i++)
            {
                ArticleDataModel dataModel = base.GenerateArticle(random);
                articles.Add(dataModel);
            }

            storage.Articles.AddRange(articles);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);

            Int32 jobAmount = random.Next(3, 10);

            for (int i = 0; i < jobAmount; i++)
            {
                Int32 articleJobAmount = random.Next(3, articleAmount / 4);
                ContactInfoDataModel expectedContact = contacts[random.Next(0, contacts.Count)];

                InventoryJobCreateModel inventoryJobCreate = new InventoryJobCreateModel();
                inventoryJobCreate.IssuanceToId = expectedContact.Id;
                inventoryJobCreate.PickupAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10));

                Dictionary<Int32, Double> expectedInventory = new Dictionary<int, double>();
                for (int j = 0; j < articleJobAmount; j++)
                {
                    ArticleDataModel article = articles[random.Next(0, articleAmount)];
                    if (expectedInventory.ContainsKey(article.Id) == false)
                    {
                        Double value = random.Next(30, 100) + random.NextDouble();
                        inventoryJobCreate.Articles.Add(article.Id, value);
                        expectedInventory.Add(article.Id, value);

                    }
                }

                Int32 jobid = await storage.CreateInventoryJob(inventoryJobCreate);

                InventoryJobDataModel jobDataModel = storage.InventoryJobs.FirstOrDefault(x => x.Id == jobid);
                Assert.NotNull(jobDataModel);

                Assert.Equal(jobid, jobDataModel.Id);
                Assert.Equal(inventoryJobCreate.PickupAt, jobDataModel.PickupAt);
                Assert.Equal(expectedContact.Id, jobDataModel.IssuerContactId);

                foreach (KeyValuePair<Int32, double> expectedInventoryItem in expectedInventory)
                {
                    IventoryJobArticleRelationDataModel relationDataModel = storage.InventoryJobArticleRelations.FirstOrDefault(x => x.ArticleId == expectedInventoryItem.Key && x.JobId == jobid);
                    Assert.NotNull(relationDataModel);

                    Assert.Equal(jobid, relationDataModel.JobId);
                    Assert.Equal(expectedInventoryItem.Key, relationDataModel.ArticleId);
                    Assert.Equal(expectedInventoryItem.Value, relationDataModel.Amount);
                }
            }
        }
    }
}
