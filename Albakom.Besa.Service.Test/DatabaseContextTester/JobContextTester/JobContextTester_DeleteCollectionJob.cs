﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_DeleteCollectionJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteCollectionJob|JobContextTester")]
        public async Task DeleteCollectionJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            Int32 jobAmount = random.Next(30, 100);

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>(); ;

            for (int i = 0; i < jobAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                Int32 randomValue = random.Next(0, 5);

                JobDataModel jobDataModel = null;
                if(randomValue == 0)
                {
                    jobDataModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };
                }
                else if(randomValue == 1)
                {
                    jobDataModel = new ConnectBuildingJobDataModel
                    {
                        Building = building,
                        Owner = customer,
                        Customer = customer,
                    };
                }
                else if(randomValue == 2)
                {
                    jobDataModel = new InjectJobDataModel
                    {
                        Building = building,
                        CustomerContact = customer,
                    };
                }
                else if (randomValue == 3)
                {
                    jobDataModel = new SpliceBranchableJobDataModel
                    {
                        Flat = flat,
                        CustomerContact = customer,
                    };
                }
                else if (randomValue == 4)
                {
                    jobDataModel = new ActivationJobDataModel
                    {
                        Flat = flat,
                        Customer = customer,
                    };
                }

                if(jobDataModel == null) { continue; }

                Boolean bindToCollection = random.NextDouble() > 0.5;
                if(bindToCollection == true)
                {
                    jobDataModel.Collection = collectionJob;
                }

                storage.Jobs.Add(jobDataModel);
                storage.SaveChanges();

                expectedResults.Add(jobDataModel.Id, bindToCollection);
            }

            Boolean acutual = await storage.DeleteCollectionJob(collectionJob.Id);
            Assert.True(acutual);

            CollectionJobDataModel expectedModel = storage.CollectionJobs.FirstOrDefault(x => x.Id == collectionJob.Id);
            Assert.Null(expectedModel);

            foreach (KeyValuePair<Int32,Boolean> jobRelation in expectedResults)
            {
                JobDataModel dataModel = storage.Jobs.FirstOrDefault(x => x.Id == jobRelation.Key);
                Assert.NotNull(dataModel);

                if(jobRelation.Value == true)
                {
                    Assert.Null(dataModel.CollectionId);
                }
                else
                {
                    Assert.Null(dataModel.CollectionId);
                }
            }
        }
    }
}
