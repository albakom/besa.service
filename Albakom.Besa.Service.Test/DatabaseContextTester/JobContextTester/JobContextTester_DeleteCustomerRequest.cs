﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_DeleteCustomerRequest : DatabaseTesterBase
    {
        [Fact]
        public async Task DeleteCustomerRequest()
        {
            var prepareResult = GenerateCustomerAccessRequests(false);

            Boolean passResult = await prepareResult.Storage.DeleteCustomerRequest(prepareResult.ElementId);
            Assert.True(passResult);

            foreach (Int32 id in prepareResult.Ids)
            {
                CustomerConnenctionRequestDataModel expectedModel = prepareResult.Storage.CustomerRequests.FirstOrDefault(x => x.Id == id);
                if (prepareResult.ElementId == id)
                {
                    Assert.True(expectedModel.IsClosed);
                }
                else
                {
                    Assert.False(expectedModel.IsClosed);
                }
            }
        }
    }
}
