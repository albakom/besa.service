﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_DeleteJobFinished : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteJobFinished|JobContextTester")]
        public async Task DeleteJobFinished()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random, storage);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(200, 400);
            List<Int32> finisehdIds = new List<Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                InjectJobDataModel jobModel = new InjectJobDataModel
                {
                    Building = building,
                    CustomerContact = contacts[random.Next(0, contacts.Count)],
                };

                building.InjectJob = jobModel;
                storage.InjectJobs.Add(jobModel);
                storage.SaveChanges();

                InjectJobFinishedDataModel finishedDataModel = new InjectJobFinishedDataModel
                {
                    AcceptedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    DoneBy = jobber,
                    Accepter = jobber,
                    ActualLength = random.Next(200, 300) + random.NextDouble(),
                };

                storage.FinishedJobs.Add(finishedDataModel);
                storage.SaveChanges();
                finisehdIds.Add(finishedDataModel.Id);
            }

            await storage.DeleteJobFinished(finisehdIds);

            foreach (Int32 id in finisehdIds)
            {
                FinishedJobDataModel model = storage.FinishedJobs.FirstOrDefault(x => x.Id == id);
                Assert.Null(model);
            }

        }
    }
}
