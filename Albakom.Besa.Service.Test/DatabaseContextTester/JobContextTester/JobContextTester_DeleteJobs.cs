﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_DeleteJobs : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteJobs|JobContextTester")]
        public async Task DeleteJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random, storage);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(200, 400);
            List<Int32> jobIds = new List<Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                InjectJobDataModel jobModel = new InjectJobDataModel
                {
                    Building = building,
                    CustomerContact = contacts[random.Next(0, contacts.Count)],
                };

                building.InjectJob = jobModel;
                storage.InjectJobs.Add(jobModel);
                storage.SaveChanges();

                jobIds.Add(jobModel.Id);
            }

            await storage.DeleteJobs(jobIds);

            foreach (Int32 id in jobIds)
            {
                JobDataModel model = storage.Jobs.FirstOrDefault(x => x.Id == id);
                Assert.Null(model);
            }
        }
    }
}
