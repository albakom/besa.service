﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_DiscardJob : DatabaseTesterBase
    {
        [Fact]
        public async Task DiscardJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(12345);

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            List<JobDataModel> finishedJobs = new List<JobDataModel>();
            Dictionary<Int32, List<Int32>> fileIdsMapper = new Dictionary<Int32, List<int>>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {

                    BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                    {
                        Job = jobModel,
                        DoneBy = jobber,
                        FinishedAt = DateTimeOffset.Now,
                    };

                    storage.FinishedBranchOffJobs.Add(finishedDataModel);
                    storage.SaveChanges();

                    finishedJobs.Add(jobModel);

                    Int32 fileAmount = random.Next(3, 10);
                    List<FileDataModel> files = new List<FileDataModel>(fileAmount);
                    for (int j  = 0; j < fileAmount; j++)
                    {
                        FileDataModel file = base.GenerateFileDataModel(random);
                        file.RelatedJob = finishedDataModel;
                        files.Add(file);
                        storage.Files.Add(file);
                    }

                    storage.SaveChanges();

                    fileIdsMapper.Add(jobModel.Id, files.Select(x=> x.Id).ToList());
                }
            }

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));
            List<Int32> finishedJobIds = finishedJobs.Select(x => x.Id).ToList();

            Int32 idToDelete = finishedJobIds[random.Next(0, finishedJobIds.Count)];
           
            Boolean passResult = await storage.DiscardJob(idToDelete);
            Assert.True(passResult);

            FinishedJobDataModel actual = storage.FinishedJobs.FirstOrDefault(x => x.JobId == idToDelete);
            Assert.Null(actual);

            foreach (Int32 fileId in fileIdsMapper[idToDelete])
            {
                FileDataModel file = storage.Files.FirstOrDefault(x => x.Id == fileId);
                Assert.Null(file);
            }
        }
    }
}
