﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishAndAcknowledgeJobAdministrative : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishAndAcknowledgeJobAdministrative")]
        public async Task FinishAndAcknowledgeJobAdministrative()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(12345);

            UserDataModel userDataModel = base.GenerateUserDataModel(null);
            storage.Users.Add(userDataModel);
            storage.SaveChanges();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
            flatDataModel.Building = buildingDataModel;
            storage.Flats.Add(flatDataModel);

            ContactInfoDataModel contact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(contact);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>();

            ConnectBuildingJobDataModel connectBuildingJobDataModel = new ConnectBuildingJobDataModel { Building = buildingDataModel, Owner = contact };
            InjectJobDataModel injectJobDataModel = new InjectJobDataModel { CustomerContact = contact, Building = buildingDataModel };
            SpliceBranchableJobDataModel spliceBranchableJobDataModel = new SpliceBranchableJobDataModel { Flat = flatDataModel, CustomerContact = contact };

            storage.Jobs.AddRange(connectBuildingJobDataModel, injectJobDataModel, spliceBranchableJobDataModel);
            storage.SaveChanges();
            jobIds.Add(connectBuildingJobDataModel.Id);
            jobIds.Add(injectJobDataModel.Id);
            jobIds.Add(spliceBranchableJobDataModel.Id);

            foreach (Int32 item in jobIds)
            {
                FinishJobAdministrativeModel input = new FinishJobAdministrativeModel
                {
                    JobId = item,
                    UserAuthId = userDataModel.AuthServiceId,
                    FinishedAt = random.NextDouble() > 0.5 ? DateTimeOffset.Now.AddDays(-random.Next(3,10)) : new Nullable<DateTimeOffset>()
                };

                Int32 id = await storage.FinishAndAcknowledgeJobAdministrative(input);

                FinishedJobDataModel actual = storage.FinishedJobs.FirstOrDefault(x => x.Id == id);
                Assert.NotNull(actual);

                Assert.Equal(item, actual.JobId);
                Assert.Equal(userDataModel.ID, actual.JobberId);
                Assert.NotNull(actual.AcceptedAt);
                Assert.NotNull(actual.AccepterId);
                Assert.Equal(userDataModel.ID, actual.AccepterId);
                Assert.True(actual.WasAdministrativeFinished);

                Assert.Equal(actual.FinishedAt, actual.AcceptedAt.Value);

                if(input.FinishedAt.HasValue == true)
                {
                    Assert.Equal(input.FinishedAt.Value, actual.FinishedAt);
                }
                else
                {
                    TimeSpan diff = DateTimeOffset.Now - actual.FinishedAt;
                    Assert.True(diff < TimeSpan.FromMinutes(3));
                }
            }

        }
    }
}
