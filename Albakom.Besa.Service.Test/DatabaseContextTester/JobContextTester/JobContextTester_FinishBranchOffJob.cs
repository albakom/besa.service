﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishBranchOffJob : DatabaseTesterBase
    {
        [Fact]
        public async Task FinishBranchOffJob()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            BranchOffJobDataModel job = buildings[random.Next(0, buildings.Count)].BranchOffJob;
            storage.SaveChanges();

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            FinishBranchOffJobModel finishModel = base.GeneretateFinsihModel<FinishBranchOffJobModel>(job, random, files, jobber);
            finishModel.DuctChanged = new ExplainedBooleanModel
            {
                Value = random.NextDouble() > 0.5,
                Description = $"Rohr anders, da {random.Next()}",
            };

            Boolean result = await storage.FinishBranchOffJob(finishModel);
            Assert.True(result);

            BranchOffJobFinishedDataModel actual = storage.FinishedBranchOffJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
            Assert.Equal(finishModel.DuctChanged.Value, actual.DuctChanged);
            Assert.Equal(finishModel.DuctChanged.Description, actual.DuctChangedDescription);
        }
    }
}
