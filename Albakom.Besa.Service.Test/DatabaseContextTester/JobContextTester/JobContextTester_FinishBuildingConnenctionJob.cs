﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishBuildingConnenctionJob : DatabaseTesterBase
    {
        [Fact]
        public async Task FinishBuildingConnenctionJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();

            Int32 contactAmount = random.Next(3, 10);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                ConnectBuildingJobDataModel jobModel = base.GenerateConnectBuildingJobDataModel(random,building, contacts);

                building.ConnectionJob = jobModel;
                storage.ConnectBuildingJobs.Add(jobModel);
                storage.SaveChanges();
            }

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ConnectBuildingJobDataModel job = buildings[random.Next(0, buildings.Count)].ConnectionJob;

            Int32 fileAmount = random.Next(3, 10);
            List<FileDataModel> files = new List<FileDataModel>(fileAmount);
            for (int i = 0; i < fileAmount; i++)
            {
                FileDataModel fileDataModel = base.GenerateFileDataModel(random);
                files.Add(fileDataModel);
            }

            storage.Files.AddRange(files);
            storage.SaveChanges();

            FinishBuildingConnenctionJobModel finishModel = new FinishBuildingConnenctionJobModel
            {
                Comment = $"Mein Kommentar {random.Next()}",
                DuctChanged = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Rohr anders, da {random.Next()}",
                },
                JobId = job.Id,
                Timestamp = DateTimeOffset.Now,
                ProblemHappend = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Problem da {random.Next()}",
                },
                FileIds = files.Select(x => x.Id).ToList(),
                UserAuthId = jobber.AuthServiceId,
            };

            Boolean result = await storage.FinishBuildingConnenctionJob(finishModel);
            Assert.True(result);

            ConnectBuildingFinishedDataModel actual = storage.FinishedConnectionBuildingJobs.FirstOrDefault(x => x.JobId == job.Id);
            Assert.NotNull(actual);

            Assert.Equal(finishModel.Comment, actual.Comment);
            Assert.Equal(finishModel.DuctChanged.Value, actual.DuctChanged);
            Assert.Equal(finishModel.DuctChanged.Description, actual.DuctChangedDescription);
            Assert.Equal(finishModel.ProblemHappend.Value, actual.ProblemHappend);
            Assert.Equal(finishModel.ProblemHappend.Description, actual.ProblemHappendDescription);
            Assert.Equal(finishModel.Timestamp, actual.FinishedAt);
            Assert.Equal(jobber.ID, actual.JobberId);

            List<FileDataModel> acutalFiles = storage.Files.Where(x => x.JobId == actual.Id).ToList();

            Assert.NotNull(acutalFiles);
            Assert.Equal(files.Count, acutalFiles.Count);

            for (int i = 0; i < files.Count; i++)
            {
                FileDataModel actualItem = acutalFiles[i];

                FileDataModel expectedModel = files.FirstOrDefault(x => x.Id == actualItem.Id);
                Assert.NotNull(expectedModel);
            }

            Assert.Null(actual.AcceptedAt);
            Assert.Null(actual.AccepterId);
        }
    }
}
