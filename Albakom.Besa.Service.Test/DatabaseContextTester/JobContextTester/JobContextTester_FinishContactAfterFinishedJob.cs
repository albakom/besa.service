﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishContactAfterFinishedJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishContactAfterFinishedJob|JobContextTester")]
        public async Task FinishContactAfterFinishedJob()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Dictionary<Int32, ContactAfterFinishedFinishModel> expectedResult = new Dictionary<Int32, ContactAfterFinishedFinishModel>(amount);
            Dictionary<Int32, List<FileDataModel>> fileMapper = new Dictionary<int, List<FileDataModel>>(amount);
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactAfterFinishedJobDataModel jobDataModel = new ContactAfterFinishedJobDataModel { Flat = flat, CustomerContact = customer, Building = building };
                storage.ContactAfterFinishedJobs.Add(jobDataModel);
                storage.SaveChanges();

                ContactAfterFinishedFinishModel finishModel = null;
                Boolean finish = random.NextDouble() > 0.5;
                if(finish == true)
                {
                    Int32? requestId = null;
                    Boolean withRequest = random.NextDouble() > 0.5;
                    if(withRequest == true)
                    {
                        CustomerConnenctionRequestDataModel request = base.GenerateConnenctionRequestDataModel(random, new List<Int32> { customer.Id });
                        request.Building = building;
                        request.Flat = flat;

                        storage.CustomerRequests.Add(request);
                        storage.SaveChanges();

                        requestId = request.Id;
                    }

                    List<FileDataModel> files = GenerateAndSavesFiles(random, storage);
                    fileMapper.Add(jobDataModel.Id, files);

                    finishModel = base.GeneretateFinsihModel<ContactAfterFinishedFinishModel>(jobDataModel, random, files, jobber);
                    finishModel.RequestId = requestId;

                    Int32 id = await storage.FinishContactAfterFinishedJob(finishModel);
                }

                expectedResult.Add(jobDataModel.Id, finishModel);
            }

            foreach (KeyValuePair<Int32, ContactAfterFinishedFinishModel> expected in expectedResult)
            {

                ContactAfterFinishedJobFinishedDataModel actual = storage.FinishedContactAfterFinishedJobs.Where(x => x.JobId == expected.Key).FirstOrDefault();
                if(expected.Value == null)
                {
                    Assert.Null(actual);
                }
                else
                {
                    Assert.NotNull(actual);

                    CheckFinishedJob(expected.Value, actual, jobber, storage, fileMapper[expected.Key]);
                    Assert.Equal(expected.Value.RequestId, actual.CreatedRequestId);
                }
            }
        }
    }
}
