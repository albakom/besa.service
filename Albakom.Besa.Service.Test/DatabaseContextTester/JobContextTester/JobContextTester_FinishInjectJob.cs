﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishInjectJob : DatabaseTesterBase
    {
        [Fact]
        public async Task FinishInjectJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(123145);

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random,storage);

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                InjectJobDataModel jobModel = new InjectJobDataModel
                {
                    Building = building,
                    CustomerContact = contacts[random.Next(0,contacts.Count)],
                };

                building.InjectJob = jobModel;
                storage.InjectJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            InjectJobDataModel job = buildings[random.Next(0, buildings.Count)].InjectJob;
            storage.SaveChanges();

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            InjectJobFinishedModel finishModel = base.GeneretateFinsihModel<InjectJobFinishedModel>(job, random, files, jobber);
            finishModel.Length = random.Next() + random.NextDouble();

            Boolean result = await storage.FinishInjectJob(finishModel);
            Assert.True(result);

            InjectJobFinishedDataModel actual = storage.FinishedInjectJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
            Assert.Equal(finishModel.Length, actual.ActualLength);
        }
    }
}
