﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishInventoryJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishInventoryJob|JobContextTester")]
        public async Task FinishInventoryJob()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel issuer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(issuer);

            InventoryJobDataModel inventoryJobDataModel = new InventoryJobDataModel
            {
                IssuerContact = issuer,
                PickupAt = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
            };

            storage.InventoryJobs.Add(inventoryJobDataModel);
            storage.SaveChanges();

            InventoryFinishedJobModel finishedModel = new InventoryFinishedJobModel
            {
                Comment = $"Mein Testkommentar {random.Next()}",
                JobId = inventoryJobDataModel.Id,
                ProblemHappend = new ExplainedBooleanModel
                {
                    Description = $"Problembeschreibung Nr. {random.Next()}",
                    Value = random.NextDouble() > 0.5,
                },
            };

            Int32 actualId = await storage.FinishInventoryJob(finishedModel);
            InventoryJobFinishedDataModel actual = storage.FinishedInventoryJobs.FirstOrDefault(x => x.Id == actualId);
            Assert.NotNull(actual);


            Assert.Equal(finishedModel.JobId, inventoryJobDataModel.Id);
            Assert.Equal(finishedModel.Comment, actual.Comment);
            Assert.Equal(finishedModel.ProblemHappend.Description, actual.ProblemHappendDescription);
            Assert.Equal(finishedModel.ProblemHappend.Value, actual.ProblemHappend);
        }
    }
}
