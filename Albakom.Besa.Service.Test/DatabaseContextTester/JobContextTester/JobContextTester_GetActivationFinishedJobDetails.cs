﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetActivationFinishedJobDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetActivationFinishedJobDetails|JobContextTester")]
        public async Task GetActivationFinishedJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            CompanyDataModel companyDataModel;
            UserDataModel jobber, acceptor;
            GenerateUsersForFinishedJobs(storage, out companyDataModel, out jobber, out acceptor);

            ActivationJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<ActivationJobFinishedDataModel>(jobDataModel, random, acceptor, jobber);
            //finishedDataModel.ActualLength = random.Next() + random.NextDouble();

            storage.FinishedActivationJobs.Add(finishedDataModel);
            storage.SaveChanges();

            List<FileDataModel> files = AddFilesToJob(random, storage, finishedDataModel);

            FinishActivationJobDetailModel actual = await storage.GetActivationFinishedJobDetails(jobDataModel.Id);

            CheckFinishModel(finishedDataModel, actual, files, acceptor, jobber, companyDataModel);
            //Assert.Equal(finishedDataModel.ActualLength, actual.Lenght);
        }
    }
}
