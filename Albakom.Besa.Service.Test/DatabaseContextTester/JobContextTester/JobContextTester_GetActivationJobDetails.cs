﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetActivationJobDetails : DatabaseTesterBase
    {
        private void CheckModels(BuildingDataModel building, FlatDataModel flat, ActivationJobDataModel job, ActivationJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(job.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.Customer);
            CheckContact(job.Customer, actual.Customer);

            Assert.NotNull(actual.Building);

            Assert.Equal(building.StreetName, actual.Building.StreetName);
            Assert.Equal(building.HouseholdUnits, actual.Building.HouseholdUnits);
            Assert.Equal(building.CommercialUnits, actual.Building.CommercialUnits);

            Assert.NotNull(actual.Building.Coordinate);
            Assert.Equal(building.Latitude, actual.Building.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.Building.Coordinate.Longitude);

            Assert.NotNull(actual.Building.Coordinate);
            Assert.Equal(building.Latitude, actual.Building.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.Building.Coordinate.Longitude);

            Assert.NotNull(actual.Flat);

            Assert.Equal(flat.BuildingId, actual.Flat.BuildingId);
            Assert.Equal(flat.Description, actual.Flat.Description);
            Assert.Equal(flat.Floor, actual.Flat.Floor);
            Assert.Equal(flat.Id, actual.Flat.Id);
            Assert.Equal(flat.Number, actual.Flat.Number);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact(DisplayName = "GetActivationJobDetails|JobContextTester")]
        public async Task GetActivationJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            ActivationJobDetailModel actual = await storage.GetActivationJobDetails(jobDataModel.Id);
            CheckModels(building,flat, jobDataModel, actual, JobStates.Open, new List<FileDataModel>());
        }


        [Fact(DisplayName = "GetActivationJobDetailsWithPopAndConnections|JobContextTester")]
        public async Task GetActivationJobDetailsWithPopAndConnections()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            PoPDataModel popDataModel = base.GeneratePoPDataModel(random);
            storage.Pops.Add(popDataModel);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.PoP = popDataModel;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            Int32 connecitonAmount = random.Next(3, 10);
            Dictionary<Int32, FiberConnenctionDataModel> expectedConnections = new Dictionary<int, FiberConnenctionDataModel>(connecitonAmount);
            for (int i = 0; i < connecitonAmount; i++)
            {
                FiberConnenctionDataModel connenctionDataModel = base.GenerateFiberConnenctionDataModel(random, flat);
                storage.FiberConnenctions.Add(connenctionDataModel);
                storage.SaveChanges();

                expectedConnections.Add(connenctionDataModel.Id, connenctionDataModel);
            }

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            ActivationJobDetailModel actual = await storage.GetActivationJobDetails(jobDataModel.Id);
            CheckModels(building, flat, jobDataModel, actual, JobStates.Open, new List<FileDataModel>());

            Assert.NotNull(actual.Pop);
            Assert.Equal(popDataModel.Id, actual.Pop.Id);
            Assert.Equal(popDataModel.Name, actual.Pop.Name);
            Assert.NotNull(actual.Pop.Location);
            Assert.Equal(popDataModel.Latitude, actual.Pop.Location.Latitude);
            Assert.Equal(popDataModel.Longitude, actual.Pop.Location.Longitude);

            Assert.NotNull(actual.Endpoints);
            Assert.Equal(connecitonAmount, actual.Endpoints.Count());

            foreach (FiberEndpointModel acutalEndpoint in actual.Endpoints)
            {
                Assert.NotNull(acutalEndpoint);
                Assert.True(expectedConnections.ContainsKey(acutalEndpoint.Id));

                FiberConnenctionDataModel expectedItem = expectedConnections[acutalEndpoint.Id];

                Assert.Equal(expectedItem.Id, acutalEndpoint.Id);
                Assert.Equal(expectedItem.Caption, acutalEndpoint.Caption);
                Assert.Equal(expectedItem.ColumnNumber, acutalEndpoint.ColumnNumber);
                Assert.Equal(expectedItem.ModuleName, acutalEndpoint.ModuleName);
                Assert.Equal(expectedItem.RowNumber, acutalEndpoint.RowNumber);
            }
        }

        [Fact(DisplayName = "GetActivationJobDetails_Finished|JobContextTester")]
        public async Task GetActivationJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ActivationJobFinishedDataModel finishedDataModel = new ActivationJobFinishedDataModel
            {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                DoneBy = jobber,
            };

            storage.FinishedActivationJobs.Add(finishedDataModel);
            storage.SaveChanges();

            ActivationJobDetailModel actual = await storage.GetActivationJobDetails(jobDataModel.Id);
            CheckModels(building, flat, jobDataModel, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetActivationJobDetails_Acknowledged|JobContextTester")]
        public async Task GetActivationJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ActivationJobFinishedDataModel finishedDataModel = new ActivationJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                Accepter = jobber,
                AcceptedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
            };

            storage.FinishedActivationJobs.Add(finishedDataModel);
            storage.SaveChanges();

            ActivationJobDetailModel actual = await storage.GetActivationJobDetails(jobDataModel.Id);
            CheckModels(building, flat, jobDataModel, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetActivationJobDetails_WithFiles|JobContextTester")]
        public async Task GetActivationJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel jobDataModel = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.ActivationJob, jobDataModel.Flat.BuildingId);

            ActivationJobDetailModel actual = await storage.GetActivationJobDetails(jobDataModel.Id);
            CheckModels(building, flat, jobDataModel, actual, JobStates.Open, expectedFiles);

            Assert.NotNull(actual.Files);
        }
    }
}
