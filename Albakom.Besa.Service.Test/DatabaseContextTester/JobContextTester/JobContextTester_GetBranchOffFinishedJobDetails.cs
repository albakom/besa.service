﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBranchOffFinishedJobDetails : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBranchOffFinishedJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            CompanyDataModel companyDataModel;
            UserDataModel jobber, acceptor;
            GenerateUsersForFinishedJobs(storage, out companyDataModel, out jobber, out acceptor);

            BranchOffJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<BranchOffJobFinishedDataModel>(jobDataModel, random, acceptor, jobber);
            finishedDataModel.DuctChanged = random.NextDouble() > 0.5;
            finishedDataModel.DuctChangedDescription = "Rohränderung weil....";
          
            storage.FinishedBranchOffJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Int32 fileAmount = random.Next(3, 10);

            List<FileDataModel> files = AddFilesToJob(random, storage, finishedDataModel);

            FinishBranchOffDetailModel actual = await storage.GetBranchOffFinishedJobDetails(jobId);

            CheckFinishModel(finishedDataModel, actual, files, acceptor, jobber, companyDataModel);
            Assert.Equal(finishedDataModel.DuctChanged, actual.DuctChanged.Value);
            Assert.Equal(finishedDataModel.DuctChangedDescription, actual.DuctChanged.Description);
        }
    }
}
