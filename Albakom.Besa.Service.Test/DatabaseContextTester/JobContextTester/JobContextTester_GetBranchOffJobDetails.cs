﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBranchOffJobDetails : DatabaseTesterBase
    {
        private static void CheckModels(BuildingDataModel building, int jobId, BranchOffJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(jobId, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.BuildingInfo);

            Assert.Equal(building.Id, actual.BuildingInfo.Id);
            Assert.Equal(building.StreetName, actual.BuildingInfo.StreetName);
            Assert.Equal(building.Latitude, actual.BuildingInfo.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.BuildingInfo.Coordinate.Longitude);

            Assert.NotNull(actual.BuildingInfo.MainColor);
            Assert.Equal(building.MicroDuctColorName, actual.BuildingInfo.MainColor.Name);
            Assert.Equal(building.MicroDuctHexValue, actual.BuildingInfo.MainColor.RGBHexValue);

            Assert.NotNull(actual.BuildingInfo.BranchColor);
            Assert.Equal(building.HouseConnenctionColorName, actual.BuildingInfo.BranchColor.Name);
            Assert.Equal(building.HouseConnenctionHexValue, actual.BuildingInfo.BranchColor.RGBHexValue);

            Assert.Equal(building.HouseConnenctionHexValue, actual.BuildingInfo.BranchColor.RGBHexValue);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact]
        public async Task GetBranchOffJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            BranchOffJobDetailModel actual = await storage.GetBranchOffJobDetails(jobId);

            CheckModels(building, jobId, actual, JobStates.Open, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };


            storage.FinishedBranchOffJobs.Add(finishedDataModel);
            storage.SaveChanges();

            BranchOffJobDetailModel actual = await storage.GetBranchOffJobDetails(jobId);
            CheckModels(building, jobId, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,
            };

            storage.FinishedBranchOffJobs.Add(finishedDataModel);
            storage.SaveChanges();

            BranchOffJobDetailModel actual = await storage.GetBranchOffJobDetails(jobId);
            CheckModels(building, jobId, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = building };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.BranchOffJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.BranchOffJob, jobDataModel.BuildingId);

            BranchOffJobDetailModel actual = await storage.GetBranchOffJobDetails(jobId);

            CheckModels(building, jobId, actual, JobStates.Open, expectedFiles);
        }
    }
}
