﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBranchOffJobIdByBuildingId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBranchOffJobIdByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(12345);

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Int32> expectedResult = new Dictionary<Int32, Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);

                storage.Buildings.Add(building);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };

                    storage.BranchOffJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    expectedResult.Add(building.Id, jobDataModel.Id);
                }
            }

            foreach (KeyValuePair<Int32, Int32> item in expectedResult)
            {
                Int32 branchOffId = await storage.GetBranchOffJobIdByBuildingId(item.Key);
                Assert.Equal(item.Value, branchOffId);
            }
        }
    }
}
