﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBuildingConnenctionJobDetails : DatabaseTesterBase
    {
        private void CheckModels(ConnectBuildingJobDataModel jobDataModel, HouseConnenctionJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            BuildingDataModel building = jobDataModel.Building;
            Assert.NotNull(actual);

            Assert.Equal(jobDataModel.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            base.CheckContact(jobDataModel.Architect, actual.Architect);
            base.CheckContact(jobDataModel.Workman, actual.Workman);
            base.CheckContact(jobDataModel.Owner, actual.Owner);
            base.CheckContact(jobDataModel.Customer, actual.Customer);

            Assert.Equal(jobDataModel.Description, actual.Description);
            Assert.Equal(jobDataModel.CivilWorkIsDoneByCustomer, actual.CivilWorkIsDoneByCustomer);
            Assert.Equal(jobDataModel.ConnectionAppointment, actual.ConnectionAppointment);
            Assert.Equal(jobDataModel.ConnectionOfOtherMediaRequired, actual.ConnectionOfOtherMediaRequired);
            Assert.Equal(jobDataModel.OnlyHouseConnection, actual.OnlyHouseConnection);

            Assert.NotNull(actual.BuildingInfo);

            Assert.Equal(building.Id, actual.BuildingInfo.Id);
            Assert.Equal(building.StreetName, actual.BuildingInfo.StreetName);
            Assert.Equal(building.Latitude, actual.BuildingInfo.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.BuildingInfo.Coordinate.Longitude);

            Assert.NotNull(actual.BuildingInfo.MainColor);
            Assert.Equal(building.MicroDuctColorName, actual.BuildingInfo.MainColor.Name);
            Assert.Equal(building.MicroDuctHexValue, actual.BuildingInfo.MainColor.RGBHexValue);

            Assert.NotNull(actual.BuildingInfo.BranchColor);
            Assert.Equal(building.HouseConnenctionColorName, actual.BuildingInfo.BranchColor.Name);
            Assert.Equal(building.HouseConnenctionHexValue, actual.BuildingInfo.BranchColor.RGBHexValue);

            Assert.Equal(building.HouseConnenctionHexValue, actual.BuildingInfo.BranchColor.RGBHexValue);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        private ConnectBuildingJobDataModel PrepareConnenctionJob(BesaDataStorage storage, Random random)
        {
            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();

            ConnectBuildingJobDataModel jobDataModel = GenerateConnectBuildingJobDataModel(random, building, contacts);

            storage.ConnectBuildingJobs.Add(jobDataModel);
            storage.SaveChanges();
            return jobDataModel;
        }

        [Fact]
        public async Task GetBuildingConnenctionJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ConnectBuildingJobDataModel jobDataModel = PrepareConnenctionJob(storage, random);

            HouseConnenctionJobDetailModel actual = await storage.GetBuildingConnenctionJobDetails(jobDataModel.Id);
            CheckModels(jobDataModel, actual, JobStates.Open,new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBuildingConnenctionJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ConnectBuildingJobDataModel jobDataModel = PrepareConnenctionJob(storage, random);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ConnectBuildingFinishedDataModel finishedDataModel = new ConnectBuildingFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };

            storage.FinishedConnectionBuildingJobs.Add(finishedDataModel);
            storage.SaveChanges();

            HouseConnenctionJobDetailModel actual = await storage.GetBuildingConnenctionJobDetails(jobDataModel.Id);
            CheckModels(jobDataModel, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ConnectBuildingJobDataModel jobDataModel = PrepareConnenctionJob(storage, random);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ConnectBuildingFinishedDataModel finishedDataModel = new ConnectBuildingFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,
            };

            storage.FinishedConnectionBuildingJobs.Add(finishedDataModel);
            storage.SaveChanges();

            HouseConnenctionJobDetailModel actual = await storage.GetBuildingConnenctionJobDetails(jobDataModel.Id);
            CheckModels(jobDataModel, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBuildingConnenctionJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ConnectBuildingJobDataModel jobDataModel = PrepareConnenctionJob(storage, random);

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.HouseConnectionJob, jobDataModel.BuildingId);

            HouseConnenctionJobDetailModel actual = await storage.GetBuildingConnenctionJobDetails(jobDataModel.Id);
            CheckModels(jobDataModel, actual, JobStates.Open, expectedFiles);
        }
    }
}
