﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBuildingIdByBranchOffJobId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingIdByBranchOffJobId|JobContextTester")]
        public async Task GetBuildingIdByBranchOffJobId()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);

            Dictionary<Int32, Int32> expectedResult = new Dictionary<int, int>(buildingAmount);

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                if (random.NextDouble() > 0.3)
                {
                    BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };

                    building.BranchOffJob = jobModel;
                    storage.BranchOffJobs.Add(jobModel);
                    storage.SaveChanges();

                    expectedResult.Add(jobModel.Id, building.Id);
                }
            }

            foreach (KeyValuePair<Int32,Int32> expectedItem in expectedResult)
            {
                Int32 actual = await storage.GetBuildingIdByBranchOffJobId(expectedItem.Key);
                Assert.Equal(expectedItem.Value, actual);
            }
        }
    }
}
