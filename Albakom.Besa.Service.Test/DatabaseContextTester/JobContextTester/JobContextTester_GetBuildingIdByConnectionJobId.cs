﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBuildingIdByConnectionJobId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingIdByConnectionJobId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Int32> expectedResults = new Dictionary<int, Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                {
                    Building = building,
                    Owner = base.GenerateContactInfo(random),
                };

                storage.Jobs.Add(jobModel);
                storage.SaveChanges();
                expectedResults.Add(jobModel.Id, building.Id);
            }

            foreach (KeyValuePair<Int32, Int32> expectedResult in expectedResults)
            {
                Int32 actual = await storage.GetBuildingIdByConnectionJobId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
