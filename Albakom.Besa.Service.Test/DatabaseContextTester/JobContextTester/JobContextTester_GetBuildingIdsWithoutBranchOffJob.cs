﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetBuildingIdsWithoutBranchOffJob : DatabaseTesterBase
    {
        [Fact]
        public async Task GetBuildingIdsWithoutBranchOffJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random(123456);

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);

            List<Int32> expectedIds = new List<int>();

            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinet = base.GenerateCabinet(random);
                constructionStage.Branchables.Add(cabinet);
                cabinet.ConstructionStage = constructionStage;

                storage.StreetCabintes.Add(cabinet);
                storage.SaveChanges();

                Int32 buildingAmount = random.Next(30, 100);
                List<BuildingDataModel> buildings = new List<BuildingDataModel>();
                for (int j = 0; j < buildingAmount; j++)
                {
                    BuildingDataModel building = base.GenerateBuilding(random);
                    building.Branchable = cabinet;
                    buildings.Add(building);
                }

                storage.Buildings.AddRange(buildings);
                storage.SaveChanges();

                expectedIds.AddRange(buildings.Select(x => x.Id));
            }

            Int32 jobAmount = random.Next(1, expectedIds.Count / 2);

            for (int i = 0; i < jobAmount; i++)
            {
                Int32 index = random.Next(0, expectedIds.Count);
                Int32 buildingId = expectedIds[index];
                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    BuildingId = buildingId,
                };

                storage.BranchOffJobs.Add(jobModel);
                storage.SaveChanges();

                expectedIds.RemoveAt(index);
            }
            
            IEnumerable<Int32> actual = await storage.GetBuildingIdsWithoutBranchOffJob(constructionStageId);

            Assert.NotNull(actual);
            Assert.Equal(expectedIds.Count, actual.Count());

            foreach (Int32 actualId in actual)
            {
                Int32 expectedId = expectedIds.FirstOrDefault(x => x == actualId);

                Assert.NotEqual(default(Int32), expectedId);
                Assert.Equal(expectedId, actualId);
            }
        }
    }
}
