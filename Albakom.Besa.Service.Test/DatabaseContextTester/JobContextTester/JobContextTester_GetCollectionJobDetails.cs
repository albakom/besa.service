﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetCollectionJobDetails : DatabaseTesterBase
    {
        [Fact]
        public async Task GetCollectionJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(123456);

            Int32 collectionJobId = random.Next();

            CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
            collectionJob.Id = collectionJobId;
            storage.CollectionJobs.Add(collectionJob);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);

            List<BranchOffJobDataModel> jobs = new List<BranchOffJobDataModel>();
            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random, storage);
            Dictionary<Int32, SimpleJobOverview> expectedJobs = new Dictionary<int, SimpleJobOverview>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Double randomValue = random.NextDouble();
                if(randomValue < 0.2) { continue; }

                JobDataModel jobToAdd = null;
                String name = "";
                JobTypes jobType = JobTypes.BranchOff;

                if(randomValue < 0.4)
                {
                    BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                    {
                        Building = buildingDataModel,
                        Collection = collectionJob,
                    };

                    buildingDataModel.BranchOffJob = jobDataModel;

                    name = buildingDataModel.StreetName;
                    jobType = JobTypes.BranchOff;
                    jobToAdd = jobDataModel;
                }
                else if (randomValue < 0.6)
                {
                    ConnectBuildingJobDataModel jobDataModel = base.GenerateConnectBuildingJobDataModel(random, buildingDataModel, contacts);
                    jobDataModel.Collection = collectionJob;

                    buildingDataModel.ConnectionJob = jobDataModel;

                    name = buildingDataModel.StreetName;
                    jobType = JobTypes.HouseConnenction;
                    jobToAdd = jobDataModel;
                }
                else if(randomValue < 0.8)
                {
                    InjectJobDataModel jobDataModel = new InjectJobDataModel
                    {
                        Building = buildingDataModel,
                        Collection = collectionJob,
                        CustomerContact = contacts[random.Next(0,contacts.Count)],
                    };

                    buildingDataModel.InjectJob = jobDataModel;

                    name = buildingDataModel.StreetName;
                    jobType = JobTypes.Inject;
                    jobToAdd = jobDataModel;
                }

                if(jobToAdd != null)
                {
                    storage.Jobs.Add(jobToAdd);
                    storage.SaveChanges();
                    expectedJobs.Add(jobToAdd.Id, new SimpleJobOverview { Id = jobToAdd.Id, Name = name, JobType = jobType });
                }
            }

            CollectionJobDetailsModel actual = await storage.GetCollectionJobDetails(collectionJobId);

            Assert.NotNull(actual);

            Assert.Equal(collectionJob.Name, actual.Name);
            Assert.Equal(collectionJob.FinishedTill, actual.End);
            Assert.Equal(collectionJob.Id, actual.Id);

            Assert.Equal(expectedJobs.Count, collectionJob.Jobs.Count());

            foreach (SimpleJobOverview actualItem in actual.Jobs)
            {
                Assert.NotNull(actualItem);

                Assert.True(expectedJobs.ContainsKey(actualItem.Id));
                SimpleJobOverview expectedItem = expectedJobs[actualItem.Id];

                Assert.NotNull(expectedItem);
                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name,actualItem.Name);
                Assert.Equal(expectedItem.JobType, actualItem.JobType);
            }
        }
    }
}
