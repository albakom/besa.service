﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetCollectionJobId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetCollectionJobId()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Int32> expectedResult = new Dictionary<Int32, Int32>(buildingAmount);

            Int32 collectionJobAmount = random.Next(30, 100);
            List<CollectionJobDataModel> collectionJobs = new List<CollectionJobDataModel>(collectionJobAmount);

            for (int i = 0; i < collectionJobAmount; i++)
            {
                CollectionJobDataModel collectionJob = new CollectionJobDataModel
                {
                    Company = company,
                    FinishedTill = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                    Name = $"Collection-Job {random.Next()}",
                };

                collectionJobs.Add(collectionJob);
            }

            storage.CollectionJobs.AddRange(collectionJobs);
            storage.SaveChanges();

            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);

                storage.Buildings.Add(building);
                storage.SaveChanges();

                BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                CollectionJobDataModel collectionJob = collectionJobs[random.Next(0, collectionJobs.Count)];
                jobDataModel.Collection = collectionJob;

                storage.BranchOffJobs.Add(jobDataModel);
                storage.SaveChanges();
                expectedResult.Add(jobDataModel.Id, collectionJob.Id);
            }

            foreach (KeyValuePair<Int32, Int32> item in expectedResult)
            {
                Int32 actual = await storage.GetCollectionJobId(item.Key);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
