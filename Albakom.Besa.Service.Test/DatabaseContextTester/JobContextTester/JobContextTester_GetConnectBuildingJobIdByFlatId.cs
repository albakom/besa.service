﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetConnectBuildingJobIdByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetConnectBuildingJobIdByFlatId|JobContextTester")]
        public async Task GetConnectBuildingJobIdByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20,30);

            Dictionary<Int32, Int32?> expectedResults = new Dictionary<int, int?>();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            ContactInfoDataModel owner = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owner);
            storage.SaveChanges();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                Int32? jobId = null;
                if(random.NextDouble() > 0.5)
                {
                    ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel
                    {
                        Owner = owner,
                        Building = building,
                    };

                    storage.ConnectBuildingJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    jobId = jobDataModel.Id;
                }

                Int32 flatAmount = random.Next(3, 10);
                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = building;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    expectedResults.Add(flatDataModel.Id, jobId);
                }
            }

            foreach (KeyValuePair<Int32,Int32?> expectedResult in expectedResults)
            {
                Int32? result = await storage.GetConnectBuildingJobIdByFlatId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, result);
            }
        }
    }
}
