﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetContactAfterFinishedJobModelById : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetContactAfterFinishedJobModelById|JobContextTester")]
        public async Task GetContactAfterFinishedJobModelById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            Dictionary<Int32, ContactAfterFinishedJobModel> expectedResult = new Dictionary<Int32, ContactAfterFinishedJobModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactAfterFinishedJobDataModel jobDataModel = new ContactAfterFinishedJobDataModel { Flat = flat, CustomerContact = customer, Building = building };
                storage.ContactAfterFinishedJobs.Add(jobDataModel);
                storage.SaveChanges();

                expectedResult.Add(jobDataModel.Id, new ContactAfterFinishedJobModel
                {
                    Id = jobDataModel.Id,
                    BuildingId = building.Id,
                    FlatId = flat.Id,
                    ContactId = customer.Id,
                });
            }

            foreach (KeyValuePair<Int32,ContactAfterFinishedJobModel> expected in expectedResult)
            {
                ContactAfterFinishedJobModel actual = await storage.GetContactAfterFinishedJobModelById(expected.Key);

                Assert.NotNull(actual);

                Assert.Equal(expected.Value.Id, actual.Id);
                Assert.Equal(expected.Value.ContactId, actual.ContactId);
                Assert.Equal(expected.Value.FlatId, actual.FlatId);
                Assert.Equal(expected.Value.BuildingId, actual.BuildingId);
            }
        }
    }
}
