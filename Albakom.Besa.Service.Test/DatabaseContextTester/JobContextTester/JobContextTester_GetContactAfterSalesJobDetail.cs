﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetContactAfterSalesJobDetail : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetContactAfterSalesJobDetail|JobContextTester")]
        public async Task GetContactAfterSalesJobDetail()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            Dictionary<Int32, ContactAfterSalesJobDetails> expectedResult = new Dictionary<Int32, ContactAfterSalesJobDetails>(amount);
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel procedure = base.GenerateBuildingConnectionProcedureDataModel(random);
                procedure.Building = building;
                procedure.Owner = customer;
                storage.BuildingConnectionProcedures.Add(procedure);
                storage.SaveChanges();

                ContactAfterFinishedJobDataModel jobDataModel = new ContactAfterFinishedJobDataModel { Flat = flat, CustomerContact = customer, Building = building, RelatedProcedure = procedure };
                storage.ContactAfterFinishedJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean shouldFinished = random.NextDouble() > 0.5;
                ContactAfterFinishedJobFinishedDataModel finishedDataModel = null;
                if (shouldFinished == true)
                {
                    finishedDataModel = base.GetFinishedJobDataModel<ContactAfterFinishedJobFinishedDataModel>(
                        jobDataModel,
                        random,
                        user,
                        user);

                    storage.FinishedJobs.Add(finishedDataModel);
                }

                ContactAfterSalesJobDetails result = new ContactAfterSalesJobDetails
                {
                    Building = new SimpleBuildingOverviewWithGPSModel
                    {
                        CommercialUnits = building.CommercialUnits,
                        HouseholdUnits = building.HouseholdUnits,
                        Id = building.Id,
                        StreetName = building.StreetName,
                        Coordinate = new GPSCoordinate
                        {
                            Latitude = building.Latitude,
                            Longitude = building.Longitude,
                        }
                    },
                    Contact = new PersonInfo
                    {
                        Address = new AddressModel
                        {
                            City = customer.City,
                            PostalCode = customer.PostalCode,
                            StreetNumber = customer.StreetNumber,
                            Street = customer.Street,
                        },
                        CompanyName = customer.CompanyName,
                        EmailAddress = customer.EmailAddress,
                        Id = customer.Id,
                        Lastname = customer.Lastname,
                        Phone = customer.Phone,
                        Surname = customer.Surname,
                        Type = customer.Type,
                    },
                    Id = jobDataModel.Id,
                    CloseComment = shouldFinished == true ? finishedDataModel.ProblemHappendDescription : String.Empty,
                    Closed = shouldFinished == true,
                    FinishedAt = shouldFinished == true ? finishedDataModel.FinishedAt : new DateTimeOffset?(),
                    Flat = new SimpleFlatOverviewModel
                    {
                        BuildingId = flat.BuildingId,
                        Description = flat.Description,
                        Floor = flat.Floor,
                        Id = flat.Id,
                        Number = flat.Number,
                    },
                    RelatedProcedureId = procedure.Id,
                };

                expectedResult.Add(jobDataModel.Id, result);
            }

            foreach (KeyValuePair<Int32, ContactAfterSalesJobDetails> expected in expectedResult)
            {
                ContactAfterSalesJobDetails actual = await storage.GetContactAfterSalesJobDetail(expected.Key);

                Assert.NotNull(actual);

                Assert.Equal(expected.Value.Id, actual.Id);
                Assert.Equal(expected.Value.RelatedProcedureId, actual.RelatedProcedureId);
                Assert.Equal(expected.Value.FinishedAt, actual.FinishedAt);
                Assert.Equal(expected.Value.Closed, actual.Closed);

                Assert.Equal(String.IsNullOrEmpty(expected.Value.CloseComment), String.IsNullOrEmpty(actual.CloseComment));

                if (String.IsNullOrEmpty(expected.Value.CloseComment) == false)
                {
                    Assert.Equal(0, String.Compare(expected.Value.CloseComment, actual.CloseComment));

                }

                base.CheckBuilding(expected.Value.Building, actual.Building);
                base.CheckContact(expected.Value.Contact, actual.Contact);
                base.CheckFlat(expected.Value.Flat, actual.Flat);
            }
        }
    }
}
