﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetContactAfterSalesJobs : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetContactAfterSalesJobs|JobContextTester")]
        public async Task GetContactAfterSalesJobs()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            String queryLastname = $"Musermann {random.Next()}";
            String quqeryBuildingName = $"musterstr. {random.Next()}";

            Dictionary<ContactAfterFinishedFilterModel, List<ContactAfterSalesJobOverview>> filterToExpectedResultMapping = new Dictionary<ContactAfterFinishedFilterModel, List<ContactAfterSalesJobOverview>>();
            ContactAfterFinishedFilterModel noFilterModel = new ContactAfterFinishedFilterModel { Amount = 8, Start = 2 };
            ContactAfterFinishedFilterModel filterWithLastnameQuery = new ContactAfterFinishedFilterModel { Amount = 8, Start = 2, Query = queryLastname };
            ContactAfterFinishedFilterModel filterWithBuildingName = new ContactAfterFinishedFilterModel { Amount = 8, Start = 2, Query = quqeryBuildingName };
            ContactAfterFinishedFilterModel filterWithOpenState = new ContactAfterFinishedFilterModel { Amount = 8, Start = 2, OpenState = false };
            ContactAfterFinishedFilterModel filterWithBuildingNameAndOpenState = new ContactAfterFinishedFilterModel { Amount = 8, Start = 2, Query = quqeryBuildingName, OpenState = false };

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            List<ContactAfterSalesJobOverview> allModels = new List<ContactAfterSalesJobOverview>();
            List<ContactAfterSalesJobOverview> modelsWithLastname = new List<ContactAfterSalesJobOverview>();
            List<ContactAfterSalesJobOverview> modelsWithBuildingName = new List<ContactAfterSalesJobOverview>();
            List<ContactAfterSalesJobOverview> modelsWithOpenState = new List<ContactAfterSalesJobOverview>();
            List<ContactAfterSalesJobOverview> modelsWithBuildingNameAndOpenState = new List<ContactAfterSalesJobOverview>();


            Int32 amount = random.Next(50, 100);

            for (int i = 0; i < amount; i++)
            {
                Boolean shoudbeAddedToLastNameQuery = random.NextDouble() > 0.5;
                Boolean shoudbeAddedToBuildingQuery = random.NextDouble() > 0.5;
                Boolean addedToOpenState = false;

                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                if (shoudbeAddedToLastNameQuery == true)
                {
                    customer.Lastname = queryLastname;
                }
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                BuildingDataModel building = base.GenerateBuilding(random);
                if (shoudbeAddedToBuildingQuery == true)
                {
                    building.NormalizedStreetName = quqeryBuildingName;
                }
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = null;
                flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactAfterFinishedJobDataModel jobDataModel = new ContactAfterFinishedJobDataModel { Flat = flat, CustomerContact = customer, Building = building };
                storage.ContactAfterFinishedJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean shouldFinished = random.NextDouble() > 0.5;
                if (shouldFinished == true)
                {
                    ContactAfterFinishedJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<ContactAfterFinishedJobFinishedDataModel>(
                        jobDataModel,
                        random,
                        user,
                        user);

                    storage.FinishedJobs.Add(finishedDataModel);
                }

                if (filterWithOpenState.OpenState.Value == true && shouldFinished == false)
                {
                    addedToOpenState = true;
                }
                else if (filterWithOpenState.OpenState.Value == false && shouldFinished == true)
                {
                    addedToOpenState = true;
                }

                ContactAfterSalesJobOverview resultModel = new ContactAfterSalesJobOverview
                {
                    Building = new SimpleBuildingOverviewModel
                    {
                        CommercialUnits = building.CommercialUnits,
                        HouseholdUnits = building.HouseholdUnits,
                        Id = building.Id,
                        StreetName = building.StreetName,
                    },
                    Contact = new PersonInfo
                    {
                        Address = new AddressModel
                        {
                            City = customer.City,
                            PostalCode = customer.PostalCode,
                            StreetNumber = customer.StreetNumber,
                            Street = customer.Street,
                        },
                        CompanyName = customer.CompanyName,
                        EmailAddress = customer.EmailAddress,
                        Id = customer.Id,
                        Lastname = customer.Lastname,
                        Phone = customer.Phone,
                        Surname = customer.Surname,
                        Type = customer.Type,
                    },
                    Id = jobDataModel.Id,
                    IsFinished = shouldFinished,
                };

                if (addedToOpenState == true)
                {
                    modelsWithOpenState.Add(resultModel);
                }
                if (shoudbeAddedToLastNameQuery == true)
                {
                    modelsWithLastname.Add(resultModel);
                }
                if (shoudbeAddedToBuildingQuery == true)
                {
                    modelsWithBuildingName.Add(resultModel);
                }

                if(addedToOpenState == true && shoudbeAddedToBuildingQuery)
                {
                    modelsWithBuildingNameAndOpenState.Add(resultModel);
                }

                allModels.Add(resultModel);
            }

            Dictionary<ContactAfterFinishedFilterModel, IEnumerable<ContactAfterSalesJobOverview>> filterMapper = new Dictionary<ContactAfterFinishedFilterModel, IEnumerable<ContactAfterSalesJobOverview>>();
            filterMapper.Add(noFilterModel, allModels.OrderBy(x => x.Contact.Lastname).Skip(noFilterModel.Start).Take(noFilterModel.Amount));
            filterMapper.Add(filterWithLastnameQuery, modelsWithLastname.OrderBy(x => x.Contact.Lastname).Skip(filterWithLastnameQuery.Start).Take(filterWithLastnameQuery.Amount));
            filterMapper.Add(filterWithBuildingName, modelsWithBuildingName.OrderBy(x => x.Contact.Lastname).Skip(filterWithBuildingName.Start).Take(filterWithBuildingName.Amount));
            filterMapper.Add(filterWithOpenState, modelsWithOpenState.OrderBy(x => x.Contact.Lastname).Skip(filterWithOpenState.Start).Take(filterWithOpenState.Amount));
            filterMapper.Add(filterWithBuildingNameAndOpenState, modelsWithBuildingNameAndOpenState.OrderBy(x => x.Contact.Lastname).Skip(filterWithBuildingNameAndOpenState.Start).Take(filterWithBuildingNameAndOpenState.Amount));

            foreach (KeyValuePair<ContactAfterFinishedFilterModel, IEnumerable<ContactAfterSalesJobOverview>> expected in filterMapper)
            {
                IEnumerable<ContactAfterSalesJobOverview> actual = await storage.GetContactAfterSalesJobs(expected.Key);

                Assert.NotNull(actual);
                Assert.Equal(expected.Value.Count(), actual.Count());

                for (int i = 0; i < expected.Value.Count(); i++)
                {
                    ContactAfterSalesJobOverview expectedItem = expected.Value.ElementAt(i);
                    ContactAfterSalesJobOverview actualItem = actual.ElementAt(i);

                    Assert.Equal(expectedItem.Id, actualItem.Id);
                    Assert.Equal(expectedItem.IsFinished, actualItem.IsFinished);

                    base.CheckContact(expectedItem.Contact, actualItem.Contact);
                    base.CheckBuilding(expectedItem.Building, actualItem.Building);
                }
            }
        }
    }
}
