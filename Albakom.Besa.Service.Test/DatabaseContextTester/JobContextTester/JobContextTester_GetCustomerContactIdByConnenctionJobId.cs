﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetCustomerContactIdByConnenctionJobId : DatabaseTesterBase
    {
        [Fact]
        public async Task GetCustomerContactIdByConnenctionJobId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 contactAmount = random.Next(30, 100);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);

            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contactInfoData = base.GenerateContactInfo(random);
                contacts.Add(contactInfoData);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, Int32> expectedResults = new Dictionary<int, Int32>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                {
                    Building = building,
                    Owner = contacts[random.Next(0, contacts.Count)],
                    OnlyHouseConnection = false,
                    Customer = contacts[random.Next(0,contacts.Count)]
                };

                storage.Jobs.Add(jobModel);
                storage.SaveChanges();
                expectedResults.Add(jobModel.Id, jobModel.CustomerContactId.Value);
            }

            foreach (KeyValuePair<Int32, Int32> expectedResult in expectedResults)
            {
                Int32 actual = await storage.GetCustomerContactIdByConnenctionJobId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
