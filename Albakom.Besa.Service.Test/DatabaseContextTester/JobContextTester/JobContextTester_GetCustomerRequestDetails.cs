﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetCustomerRequestDetails : DatabaseTesterBase
    {
        private void CheckPersonInfo(IEnumerable<ContactInfoDataModel> contacts, Int32 contactId, PersonInfo actual)
        {
            Assert.NotNull(actual);

            ContactInfoDataModel expectedContact = contacts.FirstOrDefault(x => x.Id == contactId);
            Assert.NotNull(expectedContact);

            Assert.Equal(expectedContact.Lastname, actual.Lastname);
            Assert.Equal(expectedContact.Phone, actual.Phone);
            Assert.Equal(expectedContact.Surname, actual.Surname);
            Assert.Equal(expectedContact.Type, actual.Type);
            Assert.Equal(expectedContact.EmailAddress, actual.EmailAddress);
            Assert.Equal(expectedContact.CompanyName, actual.CompanyName);

            Assert.NotNull(actual.Address);
            Assert.Equal(expectedContact.City, actual.Address.City);
            Assert.Equal(expectedContact.Street, actual.Address.Street);
            Assert.Equal(expectedContact.StreetNumber, actual.Address.StreetNumber);
            Assert.Equal(expectedContact.PostalCode, actual.Address.PostalCode);
        }

        [Fact]
        public async Task GetCustomerRequestDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(123456);

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = base.GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();
            List<Int32> contactIds = contacts.Select(x => x.Id).ToList();

            Int32 buildingAmount = random.Next(10, 30);

            storage.SaveChanges();

            Int32 requestAmount = random.Next(10, 30);
            List<CustomerConnenctionRequestDataModel> requests = new List<CustomerConnenctionRequestDataModel>();
            Dictionary<Int32, BuildingDataModel> requestToBuildingMapper = new Dictionary<int, BuildingDataModel>();
            Dictionary<Int32, Boolean> requestToOtherMapper = new Dictionary<int, bool>();

            for (int i = 0; i < requestAmount; i++)
            {
                CustomerConnenctionRequestDataModel requestDataModel = base.GenerateConnenctionRequestDataModel(random, contactIds);
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                if (requestDataModel.OnlyHouseConnection == true)
                {
                    requestDataModel.Building = building;
                }
                else
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = building;

                    storage.Flats.Add(flat);
                    storage.SaveChanges();

                    requestDataModel.Flat = flat;
                }

                Boolean anotherRequestInProgress = random.NextDouble() > 0.5;
                if(anotherRequestInProgress == true)
                {
                    CustomerConnenctionRequestDataModel anotherRequestDataModel = base.GenerateConnenctionRequestDataModel(random, contactIds);
                    ProcedureDataModel procedure = null;
                    if (requestDataModel.OnlyHouseConnection == true)
                    {
                        anotherRequestDataModel.BuildingId = building.Id;
                        procedure = new BuildingConnectionProcedureDataModel { BuildingId = building.Id };
                    }
                    else
                    {
                        anotherRequestDataModel.BuildingId = building.Id;
                        anotherRequestDataModel.FlatId = requestDataModel.Flat.Id;
                        procedure = new CustomerConnectionProcedureDataModel { FlatId = requestDataModel.Flat.Id };
                    }

                    storage.CustomerRequests.Add(anotherRequestDataModel);
                    storage.SaveChanges();

                    storage.Procedures.Add(procedure);
                    storage.SaveChanges();

                    ProcedureTimeLineElementDataModel timeLineElement = new ProcedureTimeLineElementDataModel { RelatedRequestId = anotherRequestDataModel.Id, ProcedureId = procedure.Id };
                    storage.ProcedureTimeLineElements.Add(timeLineElement);
                    storage.SaveChanges();

                    if(random.NextDouble() > 0.5)
                    {
                        //procedure.IsFinished = true;
                        //storage.SaveChanges();

                        //anotherRequestInProgress = false;
                    }
                }


                requests.Add(requestDataModel);
                storage.CustomerRequests.Add(requestDataModel);
                storage.SaveChanges();

                requestToBuildingMapper.Add(requestDataModel.Id, building);

                requestToOtherMapper.Add(requestDataModel.Id, anotherRequestInProgress);
            }

            foreach (KeyValuePair<Int32, BuildingDataModel> item in requestToBuildingMapper)
            {
                CustomerConnenctionRequestDataModel expected = requests.FirstOrDefault(x => x.Id == item.Key);
                Assert.NotNull(expected);

                RequestDetailModel actual = await storage.GetCustomerRequestDetails(item.Key);
                Assert.NotNull(actual);

                Assert.Equal(item.Key, actual.Id);
                Assert.Equal(expected.OnlyHouseConnection, actual.OnlyHouseConnection);
                Assert.Equal(expected.SubscriberEndpointLength, actual.SubscriberEndpointLength);
                Assert.Equal(expected.CivilWorkIsDoneByCustomer, actual.CivilWorkIsDoneByCustomer);
                Assert.Equal(expected.ConnectionAppointment, actual.ConnectionAppointment);
                Assert.Equal(expected.ConnectioOfOtherMediaRequired, actual.ConnectionOfOtherMediaRequired);
                Assert.Equal(expected.DescriptionForHouseConnection, actual.DescriptionForHouseConnection);
                Assert.Equal(expected.DuctAmount, actual.DuctAmount);

                Assert.NotNull(actual.PowerForActiveEquipment);
                Assert.Equal(expected.PowerForActiveEquipmentValue, actual.PowerForActiveEquipment.Value);
                Assert.Equal(expected.PowerForActiveEquipmentDescription, actual.PowerForActiveEquipment.Description);

                Assert.NotNull(actual.SubscriberEndpointNearConnectionPoint);
                Assert.Equal(expected.SubscriberEndpointNearConnectionPointValue, actual.SubscriberEndpointNearConnectionPoint.Value);
                Assert.Equal(expected.SubscriberEndpointNearConnectionPointDescription, actual.SubscriberEndpointNearConnectionPoint.Description);

                Assert.NotNull(actual.ActivePointNearSubscriberEndpoint);
                Assert.Equal(expected.ActivePointNearSubscriberEndpointValue, actual.ActivePointNearSubscriberEndpoint.Value);
                Assert.Equal(expected.ActivePointNearSubscriberEndpointDescription, actual.ActivePointNearSubscriberEndpoint.Description);

                CheckPersonInfo(contacts, expected.CustomerPersonId.Value, actual.Customer);
                CheckPersonInfo(contacts, expected.ArchitectPersonId.Value, actual.Architect);
                CheckPersonInfo(contacts, expected.WorkmanPersonId.Value, actual.Workman);
                CheckPersonInfo(contacts, expected.PropertyOwnerPersonId.Value, actual.Owner);

                BuildingDataModel expectedBuilding = item.Value;

                Assert.NotNull(actual.Building);
                Assert.Equal(expectedBuilding.Id, actual.Building.Id);
                Assert.Equal(expectedBuilding.StreetName, actual.Building.StreetName);
                Assert.Equal(expectedBuilding.HouseholdUnits, actual.Building.HouseholdUnits);
                Assert.Equal(expectedBuilding.CommercialUnits, actual.Building.CommercialUnits);

                Assert.Equal(expected.FlatId.HasValue, actual.Flat != null);
                if(expected.FlatId.HasValue == true)
                {
                    Assert.NotNull(actual.Flat);
                    Assert.Equal(expected.FlatId.Value, actual.Flat.Id);
                    Assert.Equal(expected.Flat.Description, actual.Flat.Description);
                    Assert.Equal(expected.Flat.Floor, actual.Flat.Floor);
                    Assert.Equal(expected.Flat.Number, actual.Flat.Number);
                    Assert.Equal(expected.Flat.BuildingId, actual.Flat.BuildingId);
                }
                else
                {
                    Assert.Null(actual.Flat);
                }

                Assert.Equal(requestToOtherMapper[actual.Id], actual.IsInProgress);
            }
        }
    }
}
