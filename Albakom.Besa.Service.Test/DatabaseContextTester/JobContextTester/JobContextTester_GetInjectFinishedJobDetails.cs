﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetInjectFinishedJobDetails : DatabaseTesterBase
    {
        [Fact]
        public async Task GetInjectFinishedJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;

            storage.SaveChanges();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = customer };
            Int32 jobId = random.Next();
            jobDataModel.Id = jobId;
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            CompanyDataModel companyDataModel;
            UserDataModel jobber, acceptor;
            GenerateUsersForFinishedJobs(storage, out companyDataModel, out jobber, out acceptor);

            InjectJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<InjectJobFinishedDataModel>(jobDataModel, random, acceptor, jobber);
            finishedDataModel.ActualLength = random.Next() + random.NextDouble();

            storage.FinishedInjectJobs.Add(finishedDataModel);
            storage.SaveChanges();

            List<FileDataModel> files = AddFilesToJob(random, storage, finishedDataModel);

            FinishInjectJobDetailModel actual = await storage.GetInjectFinishedJobDetails(jobId);

            CheckFinishModel(finishedDataModel, actual, files, acceptor, jobber, companyDataModel);
            Assert.Equal(finishedDataModel.ActualLength, actual.Lenght);
        }
    }
}
