﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetInjectJobDetails : DatabaseTesterBase
    {
        private void CheckModels(BuildingDataModel building, InjectJobDataModel job, InjectJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(job.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.InjectDuctColor);
            Assert.Equal(building.MicroDuctColorName, actual.InjectDuctColor.Name);
            Assert.Equal(building.MicroDuctHexValue, actual.InjectDuctColor.RGBHexValue);

            Assert.NotNull(actual.Branchable);
            Assert.Equal(building.Branchable.Latitude, actual.Branchable.Coordinate.Latitude);
            Assert.Equal(building.Branchable.Longitude, actual.Branchable.Coordinate.Longitude);
            Assert.Equal(building.Branchable.Name, actual.Branchable.Name);

            Assert.NotNull(actual.Customer);
            CheckContact(job.CustomerContact, actual.Customer);

            Assert.NotNull(actual.Cable);
            Assert.Equal(building.FiberCableType.Name, actual.Cable.Name);
            Assert.Equal(building.FiberCableType.FiberAmount, actual.Cable.FiberAmount);
            Assert.Equal(building.FiberCableType.FiberPerBundle, actual.Cable.FiberPerBundle);

            Assert.NotNull(actual.BuildingInfo);

            Assert.Equal(building.StreetName, actual.BuildingInfo.StreetName);
            Assert.Equal(building.HouseholdUnits, actual.BuildingInfo.HouseholdUnits);
            Assert.Equal(building.CommercialUnits, actual.BuildingInfo.CommercialUnits);

            Assert.NotNull(actual.BuildingInfo.Coordinate);
            Assert.Equal(building.Latitude, actual.BuildingInfo.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.BuildingInfo.Coordinate.Longitude);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact]
        public async Task GetInjectJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random,storage);

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCableType = cableTypeDataModel;

            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            InjectJobDetailModel actual = await storage.GetInjectJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, JobStates.Open, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCableType = cableTypeDataModel;

            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            InjectJobFinishedDataModel finishedDataModel = new InjectJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };

            storage.FinishedInjectJobs.Add(finishedDataModel);
            storage.SaveChanges();

            InjectJobDetailModel actual = await storage.GetInjectJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetBranchOffJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCableType = cableTypeDataModel;

            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            InjectJobFinishedDataModel finishedDataModel = new InjectJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,
            };

            storage.FinishedInjectJobs.Add(finishedDataModel);
            storage.SaveChanges();

            InjectJobDetailModel actual = await storage.GetInjectJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }
        
        [Fact]
        public async Task GetInjectJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCableType = cableTypeDataModel;

            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            InjectJobDataModel jobDataModel = new InjectJobDataModel { Building = building, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.InjectJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.InjectJob, jobDataModel.BuildingId);

            InjectJobDetailModel actual = await storage.GetInjectJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, JobStates.Open, expectedFiles);

            Assert.NotNull(actual.Files);

        }
    }
}
