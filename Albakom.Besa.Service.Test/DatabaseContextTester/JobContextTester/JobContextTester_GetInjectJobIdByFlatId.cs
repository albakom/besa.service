﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetInjectJobIdByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetInjectJobIdByFlatId|JobContextTester")]
        public async Task GetInjectJobIdByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20,30);

            Dictionary<Int32, Int32?> expectedResults = new Dictionary<int, int?>();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            ContactInfoDataModel owner = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owner);
            storage.SaveChanges();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();


                Int32? jobId = null;
                if(random.NextDouble() > 0.5)
                {
                    InjectJobDataModel jobDataModel = new InjectJobDataModel
                    {
                        CustomerContact = owner,
                        Building = building,
                    };

                    storage.InjectJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    jobId = jobDataModel.Id;
                }

                Int32 flatAmount = random.Next(3, 10);
                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flatData = base.GenerateFlatDataModel(random);
                    flatData.Building = building;
                    storage.Flats.Add(flatData);
                    storage.SaveChanges();

                    expectedResults.Add(flatData.Id, jobId);
                }

            }

            foreach (KeyValuePair<Int32,Int32?> expectedResult in expectedResults)
            {
                Int32? result = await storage.GetInjectJobIdByFlatId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, result);
            }
        }
    }
}
