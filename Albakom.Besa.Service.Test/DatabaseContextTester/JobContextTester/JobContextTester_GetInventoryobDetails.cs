﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetInventoryobDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetInventoryobDetails|JobContextTester")]
        public async Task GetInventoryobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(1345);

            Int32 articleAmount = random.Next(10, 30);
            List<ArticleDataModel> articles = new List<ArticleDataModel>();
            for (int i = 0; i < articleAmount; i++)
            {
                ArticleDataModel articleDataModel = base.GenerateArticle(random);
                storage.Articles.Add(articleDataModel);
                storage.SaveChanges();

                articles.Add(articleDataModel);
            }

            Int32 jobAmount = random.Next(10, 30);

            ContactInfoDataModel issuer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(issuer);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Dictionary<Int32, List<InventoryJobPositionModel>> expectedInventory = new Dictionary<int, List<InventoryJobPositionModel>>();
            Dictionary<Int32, InventoryJobDataModel> expected = new Dictionary<int, InventoryJobDataModel>();

            for (int i = 0; i < jobAmount; i++)
            {
                InventoryJobDataModel jobDataModel = new InventoryJobDataModel
                {
                    IssuerContact = issuer,
                    PickupAt = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                };

                storage.InventoryJobs.Add(jobDataModel);
                storage.SaveChanges();

                List<InventoryJobPositionModel> expectedArticles = new List<InventoryJobPositionModel>();
                HashSet<ArticleDataModel> usedArtciles = new HashSet<ArticleDataModel>();
                expectedInventory.Add(jobDataModel.Id, expectedArticles);

                Int32 jobArticleAmount = random.Next(2, articleAmount/2);

                for (int j = 0; j < jobArticleAmount; j++)
                {
                    ArticleDataModel articleToUse = articles[random.Next(0, articles.Count)];
                    while (usedArtciles.Contains(articleToUse) == true)
                    {
                        articleToUse = articles[random.Next(0, articles.Count)];
                    }

                    usedArtciles.Add(articleToUse);

                    IventoryJobArticleRelationDataModel relationDataModel = new IventoryJobArticleRelationDataModel
                    {
                        Amount = random.Next(3, 10),
                        Article = articleToUse,
                        Job = jobDataModel,
                    };

                    storage.InventoryJobArticleRelations.Add(relationDataModel);

                    expectedArticles.Add(new InventoryJobPositionModel
                    {
                        Amount = relationDataModel.Amount,
                        ArticleId = articleToUse.Id,
                        ArticleName = articleToUse.Name,
                    });
                }

                Boolean finished = random.NextDouble() > 0.5;
                if(finished == true)
                {
                    InventoryJobFinishedDataModel finishedDataModel = new InventoryJobFinishedDataModel
                    {
                        Job = jobDataModel,
                        DoneBy = jobber,
                    };
                    storage.FinishedInventoryJobs.Add(finishedDataModel);
                    storage.SaveChanges();
                }

                expected.Add(jobDataModel.Id, jobDataModel);
            }

            foreach (KeyValuePair<Int32,InventoryJobDataModel> item in expected)
            {
                InventoryJobDetailModel actual = await storage.GetInventoryobDetails(item.Key);

                Assert.NotNull(actual);
                Assert.Equal(item.Key, actual.Id);
                base.CheckContact(issuer, actual.Issuer);

                Assert.NotNull(actual.Positions);
                List<InventoryJobPositionModel> expectedItems = expectedInventory[item.Key];

                Assert.Equal(expectedItems.Count, actual.Positions.Count());

                foreach (InventoryJobPositionModel actualPosition in actual.Positions)
                {
                    Assert.NotNull(actualPosition);

                    InventoryJobPositionModel expectedInventoryItem = expectedItems.FirstOrDefault(x => x.ArticleId == actualPosition.ArticleId);

                    Assert.NotNull(expectedInventoryItem);

                    Assert.Equal(expectedInventoryItem.ArticleId, actualPosition.ArticleId);
                    Assert.Equal(expectedInventoryItem.ArticleName, actualPosition.ArticleName);
                    Assert.Equal(expectedInventoryItem.Amount, actualPosition.Amount);
                }
            }
       }
    }
}
