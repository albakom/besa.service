﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobCollectionOverviewModelComparer : IEqualityComparer<JobCollectionOverviewModel>
    {
        public bool Equals(JobCollectionOverviewModel x, JobCollectionOverviewModel y)
        {
            if (x == null && y == null) { return true; }
            else if (x == null || y == null) { return false; }
            else
            {
                return x.CollectionJobId == y.CollectionJobId;
            }
        }

        public int GetHashCode(JobCollectionOverviewModel obj)
        {
            if (obj == null) { return 0; }
            return obj.CollectionJobId.GetHashCode();
        }
    }

    public class JobContextTester_GetJobCollectionOverview : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetJobCollectionOverview|JobContextTester")]
        public async Task GetJobCollectionOverview()
        {
            BesaDataStorage storage = GetStorage();
            Random random = new Random(12345);

            Int32 collectionJobAmount = random.Next(20, 40);

            List<JobCollectionOverviewModel> allItems = new List<JobCollectionOverviewModel>();
            List<JobCollectionOverviewModel> itemsForQuery = new List<JobCollectionOverviewModel>();

            String query = $"Suchparemter {random.Next()}";

            for (int i = 0; i < collectionJobAmount; i++)
            {
                CompanyDataModel companyDataModel = base.GenerateCompanyDataModel(random);
                storage.Companies.Add(companyDataModel);
                storage.SaveChanges();

                CollectionJobDataModel collectionJobDataModel = new CollectionJobDataModel
                {
                    Company = companyDataModel,
                    Name = $"Aufgaben Nr {random.Next()}",
                    FinishedTill = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                };

                storage.CollectionJobs.Add(collectionJobDataModel);
                storage.SaveChanges();

                Boolean addToQuery = random.NextDouble() > 0.5;
                if (addToQuery == true)
                {
                    if (random.NextDouble() > 0.5)
                    {
                        companyDataModel.Name = $"agsgg {query} sgsgsg";
                    }
                    else
                    {
                        collectionJobDataModel.Name = $"sf332 {query} 42424";
                    }

                    storage.SaveChanges();

                }

                JobCollectionOverviewModel expectedItem = new JobCollectionOverviewModel
                {
                    BoundedTo = new SimpleCompanyOverviewModel { Id = companyDataModel.Id, Name = companyDataModel.Name },
                    FinishedTill = collectionJobDataModel.FinishedTill,
                    Percentage = 0.0,
                    Name = collectionJobDataModel.Name,
                    CollectionJobId = collectionJobDataModel.Id,
                };

                if (addToQuery == true)
                {
                    itemsForQuery.Add(expectedItem);
                }

                allItems.Add(expectedItem);

                Int32 jobAmount = random.Next(3, 10);
                List<SimpleJobOverview> simpleJobs = new List<SimpleJobOverview>();
                for (int j = 0; j < jobAmount; j++)
                {
                    Int32 randomValue = random.Next(0, 5);
                    JobDataModel jobData = null;

                    BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingDataModel);
                    storage.SaveChanges();

                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    ContactInfoDataModel contact = base.GenerateContactInfo(random);
                    storage.ContactInfos.Add(contact);
                    storage.SaveChanges();

                    UserDataModel jobber = base.GenerateUserDataModel(null);
                    UserDataModel acceptor = base.GenerateUserDataModel(null);
                    storage.Users.AddRange(jobber, acceptor);
                    storage.SaveChanges();

                    JobTypes jobType = JobTypes.BranchOff;
                    String expectedJobName = String.Empty;
                    switch (randomValue)
                    {
                        case 0:
                            jobData = new BranchOffJobDataModel { Building = buildingDataModel };
                            jobType = JobTypes.BranchOff;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 1:
                            jobData = new ConnectBuildingJobDataModel { Building = buildingDataModel, Owner = contact };
                            jobType = JobTypes.HouseConnenction;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 2:
                            jobData = new InjectJobDataModel { Building = buildingDataModel, CustomerContact = contact };
                            jobType = JobTypes.Inject;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 3:
                            jobData = new SpliceBranchableJobDataModel { Flat = flatDataModel, CustomerContact = contact };
                            jobType = JobTypes.SpliceInBranchable;
                            expectedJobName = $"{buildingDataModel.StreetName} - {flatDataModel.Number}";
                            break;
                        case 4:
                            jobData = new ActivationJobDataModel { Flat = flatDataModel, Customer = contact };
                            jobType = JobTypes.ActivationJob;
                            expectedJobName = $"{buildingDataModel.StreetName} - {flatDataModel.Number}";
                            break;
                        default:
                            break;
                    }

                    jobData.Collection = collectionJobDataModel;
                    storage.Jobs.Add(jobData);
                    storage.SaveChanges();

                    SimpleJobOverview expected = new SimpleJobOverview { Id = jobData.Id, State = JobStates.Open, JobType = jobType, Name = expectedJobName };

                    if (random.NextDouble() > 0.5)
                    {
                        FinishedJobDataModel finishedJob = null;
                        switch (randomValue)
                        {
                            case 0:
                                finishedJob = base.GetFinishedJobDataModel<BranchOffJobFinishedDataModel>(jobData, random, null, jobber);
                                break;
                            case 1:
                                finishedJob = base.GetFinishedJobDataModel<ConnectBuildingFinishedDataModel>(jobData, random, null, jobber);
                                break;
                            case 2:
                                finishedJob = base.GetFinishedJobDataModel<InjectJobFinishedDataModel>(jobData, random, null, jobber);
                                break;
                            case 3:
                                finishedJob = base.GetFinishedJobDataModel<SpliceBranchableJobFinishedDataModel>(jobData, random, null, jobber);
                                break;
                            case 4:
                                finishedJob = base.GetFinishedJobDataModel<ActivationJobFinishedDataModel>(jobData, random, null, jobber);
                                break;
                            default:
                                break;
                        }

                        if (random.NextDouble() > 0.5)
                        {
                            finishedJob.Accepter = acceptor;
                            expected.State = JobStates.Acknowledged;
                        }
                        else
                        {
                            expected.State = JobStates.Finished;
                        }

                        storage.FinishedJobs.Add(finishedJob);
                        storage.SaveChanges();

                    }

                    simpleJobs.Add(expected);
                }

                expectedItem.Jobs = simpleJobs;
                expectedItem.Percentage = (Double)expectedItem.Jobs.Count(x => x.State == JobStates.Acknowledged) / expectedItem.Jobs.Count();

            }

            Int32 amount = random.Next(itemsForQuery.Count / 2, itemsForQuery.Count);
            Int32 start = random.Next(0, itemsForQuery.Count / 2 - 1);

            Dictionary<JobCollectionFilterModel, List<JobCollectionOverviewModel>> sortCollection = new Dictionary<JobCollectionFilterModel, List<JobCollectionOverviewModel>>();
            sortCollection.Add(
                new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Ascending, SortProperty = JobCollectionSortProperties.Name },
                itemsForQuery.OrderBy(x => x.Name).Skip(start).Take(amount).ToList()
                );

            sortCollection.Add(
              new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Descending, SortProperty = JobCollectionSortProperties.Name },
              itemsForQuery.OrderByDescending(x => x.Name).Skip(start).Take(amount).ToList()
              );

            sortCollection.Add(
              new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Ascending, SortProperty = JobCollectionSortProperties.CompanyName },
              itemsForQuery.OrderBy(x => x.BoundedTo.Name).Skip(start).Take(amount).ToList()
            );

            sortCollection.Add(
             new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Descending, SortProperty = JobCollectionSortProperties.CompanyName },
             itemsForQuery.OrderByDescending(x => x.BoundedTo.Name).Skip(start).Take(amount).ToList()
            );

            sortCollection.Add(
            new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Ascending, SortProperty = JobCollectionSortProperties.FinishedTill },
            itemsForQuery.OrderBy(x => x.FinishedTill).Skip(start).Take(amount).ToList()
          );

            sortCollection.Add(
             new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Descending, SortProperty = JobCollectionSortProperties.FinishedTill },
             itemsForQuery.OrderByDescending(x => x.FinishedTill).Skip(start).Take(amount).ToList()
            );

            sortCollection.Add(
                new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Ascending, SortProperty = JobCollectionSortProperties.DoingPercentage },
                itemsForQuery.OrderBy(x => x.Percentage).Skip(start).Take(amount).ToList()
            );

            sortCollection.Add(
             new JobCollectionFilterModel { Query = query, Amount = amount, Start = start, SortDirection = SortDirections.Descending, SortProperty = JobCollectionSortProperties.DoingPercentage },
             itemsForQuery.OrderByDescending(x => x.Percentage).Skip(start).Take(amount).ToList()
            );

            foreach (KeyValuePair<JobCollectionFilterModel, List<JobCollectionOverviewModel>> item in sortCollection)
            {
                IEnumerable<JobCollectionOverviewModel> actual = await storage.GetJobCollectionOverview(item.Key);

                Assert.NotNull(actual);
                Assert.Equal(amount, actual.Count());

                List<JobCollectionOverviewModel> sortedExpectedItems = item.Value;
                Assert.Equal(sortedExpectedItems, actual, new JobCollectionOverviewModelComparer());

                for (int i = 0; i < amount; i++)
                {
                    JobCollectionOverviewModel expectedItem = sortedExpectedItems[i];
                    JobCollectionOverviewModel acutalItem = actual.ElementAt(i);

                    Assert.Equal(expectedItem.CollectionJobId, acutalItem.CollectionJobId);
                    Assert.Equal(expectedItem.Name, acutalItem.Name);
                    Assert.Equal(expectedItem.FinishedTill, acutalItem.FinishedTill);
                    Assert.Equal(expectedItem.Percentage, acutalItem.Percentage);

                    Assert.NotNull(acutalItem.BoundedTo);
                    Assert.Equal(expectedItem.BoundedTo.Id, acutalItem.BoundedTo.Id);
                    Assert.Equal(expectedItem.BoundedTo.Name, acutalItem.BoundedTo.Name);

                    Assert.NotNull(acutalItem.Jobs);
                    Assert.Equal(expectedItem.Jobs.Count(), acutalItem.Jobs.Count());

                    for (int j = 0; j < expectedItem.Jobs.Count(); j++)
                    {
                        SimpleJobOverview expectedJobItem = expectedItem.Jobs.ElementAt(j);
                        SimpleJobOverview acutalJobItem = acutalItem.Jobs.ElementAt(j);

                        Assert.Equal(expectedJobItem.Id, acutalJobItem.Id);
                        Assert.Equal(expectedJobItem.Name, acutalJobItem.Name);
                        Assert.Equal(expectedJobItem.JobType, acutalJobItem.JobType);
                        Assert.Equal(expectedJobItem.State, acutalJobItem.State);
                    }
                }
            }
        }
    }
}
