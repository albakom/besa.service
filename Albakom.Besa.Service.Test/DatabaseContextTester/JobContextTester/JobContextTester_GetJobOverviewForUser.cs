﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetJobOverviewForUser : DatabaseTesterBase
    {
        [Fact]
        public async Task GetJobOverviewForUser()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            CollectionJobDataModel collectionJob = new CollectionJobDataModel
            {
                Name = $"Testaufgabe {random.Next()}",
                FinishedTill = DateTimeOffset.Now.AddDays(8),
                Company = company,
            };

            Int32 buildingAmount = random.Next(30, 100);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                buildings.Add(building);

                BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                building.BranchOffJob = jobModel;
                collectionJob.Jobs.Add(jobModel);
                jobModel.Collection = collectionJob;
                storage.BranchOffJobs.Add(jobModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            UserDataModel user = base.GenerateUserDataModel(null);
            String userId = Guid.NewGuid().ToString();
            user.AuthServiceId = userId;
            user.Company = company;

            storage.Users.Add(user);
            storage.SaveChanges();

            List<Int32> jobIds = new List<int>(storage.Jobs.Select(x => x.Id));

            IEnumerable<PersonalJobOverviewModel> jobs =  await storage.GetJobOverviewForUser(userId);

            Assert.NotNull(jobs);
            Assert.True(jobs.Count() == 1);

            PersonalJobOverviewModel actual = jobs.ElementAt(0);

            Assert.Equal(collectionJob.Id, actual.CollectionJobId);
            Assert.Equal(collectionJob.FinishedTill, actual.EndDate);
            Assert.Null(actual.JobId);
            Assert.Equal(JobTypes.BranchOff, actual.JobType);
            Assert.Equal(collectionJob.Name, actual.Name);
            Assert.Equal(jobIds.Count(), actual.TaskAmount);
        }
    }
}
