﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetJobType : DatabaseTesterBase
    {
        [Fact]
        public async Task GetJobType()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, JobTypes> expectedResults = new Dictionary<int, JobTypes>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                Int32 type = random.Next(0, 3);
                JobDataModel jobData = null;
                JobTypes jobType = JobTypes.BranchOff;
                if(type == 0)
                {
                    BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };

                    jobData = jobModel;
                    jobType = JobTypes.BranchOff;
                }
                else if (type == 1)
                {
                    ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                    {
                        Building = building,
                        Owner = base.GenerateContactInfo(random),
                    };

                    jobData = jobModel;
                    jobType = JobTypes.HouseConnenction;
                }
                else if(type == 2)
                {
                    InjectJobDataModel jobModel = new InjectJobDataModel
                    {
                        Building = building,
                    };
                    jobData = jobModel;
                    jobType = JobTypes.Inject;
                }

                storage.Jobs.Add(jobData);
                storage.SaveChanges();

                expectedResults.Add(jobData.Id, jobType);
            }

            foreach (KeyValuePair<Int32,JobTypes> expectedResult in expectedResults)
            {
                JobTypes actual = await storage.GetJobType(expectedResult.Key);

                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
