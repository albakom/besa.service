﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetMostRecentJobByBuildingId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMostRecentJobByBuildingId|JobContextTester")]
        public async Task GetMostRecentJobByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 constructionStageId = random.Next();

            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random, storage);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 40);

            Dictionary<Int32, SimpleJobOverview> expectedResults = new Dictionary<int, SimpleJobOverview>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                SimpleJobOverview item = null;

                Double randomValue = random.NextDouble();
                if(randomValue > 0.75)
                {
                    InjectJobDataModel jobModel = new InjectJobDataModel
                    {
                        Building = building,
                        CustomerContact = contacts[random.Next(0, contacts.Count)],
                    };

                    building.InjectJob = jobModel;
                    storage.InjectJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.Inject,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else if(randomValue > 0.5)
                {
                    ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                    {
                        Building = building,
                        Owner = contacts[random.Next(0, contacts.Count)],
                    };

                    building.ConnectionJob = jobModel;
                    storage.ConnectBuildingJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.HouseConnenction,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else if(randomValue > 0.25)
                {
                    BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };

                    building.BranchOffJob = jobModel;
                    storage.BranchOffJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.BranchOff,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else
                {
                    
                }

                expectedResults.Add(building.Id, item);
            }

            foreach (KeyValuePair<Int32,SimpleJobOverview> item in expectedResults)
            {
                SimpleJobOverview actual = await storage.GetMostRecentJobByBuildingId(item.Key);

                if(item.Value == null)
                {
                    Assert.Null(actual);
                }
                else
                {
                    Assert.NotNull(actual);

                    Assert.Equal(item.Value.Id, actual.Id);
                    Assert.Equal(item.Value.Name, actual.Name);
                    Assert.Equal(item.Value.State, actual.State);
                    Assert.Equal(item.Value.JobType, actual.JobType);
                }
            }
        }
    }
}
