﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetMostRecentJobByFlatId : DatabaseTesterBase
    {
        private class BuildingFlatMapper
        {
            public Int32 BuildingId { get; set; }
            public Int32 FlatId { get; set; }
        }

        [Fact(DisplayName = "GetMostRecentJobByFlatId|JobContextTester")]
        public async Task GetMostRecentJobByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12345);

            Int32 constructionStageId = random.Next();

            List<ContactInfoDataModel> contacts = GenerateAndSavesContacts(random, storage);

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 40);

            Dictionary<BuildingFlatMapper, SimpleJobOverview> expectedResults = new Dictionary<BuildingFlatMapper, SimpleJobOverview>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat =  base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                SimpleJobOverview item = null;

                Double randomValue = random.NextDouble();
                if(randomValue > 0.833)
                {
                    ActivationJobDataModel jobModel = new ActivationJobDataModel
                    {
                        Flat = flat,
                        Customer = contacts[random.Next(0, contacts.Count)],
                    };

                    flat.ActivationJob = jobModel;
                    storage.ActivationJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.ActivationJob,
                        Name = $"{building.StreetName} - {flat.Number}",
                        State = JobStates.Open,
                    };
                }
                else if(randomValue > 0.666)
                {
                    SpliceBranchableJobDataModel jobModel = new SpliceBranchableJobDataModel
                    {
                        Flat = flat,
                        CustomerContact = contacts[random.Next(0, contacts.Count)],
                    };

                    flat.BranchableSpliceJob = jobModel;
                    storage.SpliceBranchableJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.SpliceInBranchable,
                        Name = $"{building.StreetName} - {flat.Number}",
                        State = JobStates.Open,
                    };
                }
                else if (randomValue > 0.5)
                {
                    InjectJobDataModel jobModel = new InjectJobDataModel
                    {
                        Building = building,
                        CustomerContact = contacts[random.Next(0, contacts.Count)],
                    };

                    building.InjectJob = jobModel;
                    storage.InjectJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.Inject,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else if (randomValue > 0.33)
                {
                    ConnectBuildingJobDataModel jobModel = new ConnectBuildingJobDataModel
                    {
                        Building = building,
                        Owner = contacts[random.Next(0, contacts.Count)],
                    };

                    building.ConnectionJob = jobModel;
                    storage.ConnectBuildingJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.HouseConnenction,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else if (randomValue > 0.1666)
                {
                    BranchOffJobDataModel jobModel = new BranchOffJobDataModel
                    {
                        Building = building,
                    };

                    building.BranchOffJob = jobModel;
                    storage.BranchOffJobs.Add(jobModel);
                    storage.SaveChanges();

                    item = new SimpleJobOverview
                    {
                        Id = jobModel.Id,
                        JobType = JobTypes.BranchOff,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };
                }
                else
                {

                }

                expectedResults.Add(new BuildingFlatMapper { BuildingId = building.Id, FlatId = flat.Id }, item);
            }

            foreach (KeyValuePair<BuildingFlatMapper, SimpleJobOverview> item in expectedResults)
            {
                SimpleJobOverview actual = await storage.GetMostRecentJobByFlatId(item.Key.FlatId, item.Key.BuildingId);

                if(item.Value == null)
                {
                    Assert.Null(actual);
                }
                else
                {
                    Assert.NotNull(actual);

                    Assert.Equal(item.Value.Id, actual.Id);
                    Assert.Equal(item.Value.Name, actual.Name);
                    Assert.Equal(item.Value.State, actual.State);
                    Assert.Equal(item.Value.JobType, actual.JobType);
                }
            }
        }
    }
}
