﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenActivationJobsAsOverview : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenActivationJobsOverview|JobContextTester")]
        public async Task GetOpenActivationJobsAsOverview()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            CompanyDataModel companyDataModel = base.GenerateCompanyDataModel();
            storage.Companies.Add(companyDataModel);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);
            Dictionary<Int32, ActivationJobOverviewModel> expectedResult = new Dictionary<int, ActivationJobOverviewModel>();
            for (int i = 0; i < branchableAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                ActivationJobDataModel jobDataModel = new ActivationJobDataModel
                {
                    Flat = flat,
                    Customer = customer,
                };

                storage.ActivationJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean bounded = random.NextDouble() > 0.5;
                if (bounded == true)
                {
                    CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
                    collectionJob.Company = companyDataModel;
                    storage.CollectionJobs.Add(collectionJob);
                    storage.SaveChanges();

                    jobDataModel.Collection = collectionJob;
                    storage.SaveChanges();
                }

                Boolean isFinsiehd = random.NextDouble() > 0.5;
                if(isFinsiehd == true)
                {
                    ActivationJobFinishedDataModel finishedDataModel = new ActivationJobFinishedDataModel
                    {
                        Job = jobDataModel,
                        DoneBy = jobber,
                        FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    };

                    storage.FinishedActivationJobs.Add(finishedDataModel);
                    storage.SaveChanges();
                }
                else
                {
                    ActivationJobOverviewModel expectedItem = new ActivationJobOverviewModel
                    {
                        JobId = jobDataModel.Id,
                        Building = new SimpleBuildingOverviewModel
                        {
                            Id = building.Id,
                            CommercialUnits = building.CommercialUnits,
                            HouseholdUnits = building.HouseholdUnits,
                            StreetName = building.StreetName,
                        },
                        Customer = new PersonInfo
                        {
                            Address = new AddressModel
                            {
                                City = customer.City,
                                PostalCode = customer.PostalCode,
                                Street = customer.Street,
                                StreetNumber = customer.StreetNumber,
                            },
                            CompanyName = customer.CompanyName,
                            EmailAddress = customer.EmailAddress,
                            Id = customer.Id,
                            Lastname = customer.Lastname,
                            Phone = customer.Phone,
                            Surname = customer.Surname,
                            Type = customer.Type,
                        },
                    };

                    expectedResult.Add(jobDataModel.Id, expectedItem);
                }
            }

            IEnumerable<ActivationJobOverviewModel> actual = await storage.GetOpenActivationJobsAsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (ActivationJobOverviewModel actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.JobId));
                ActivationJobOverviewModel expectedItem = expectedResult[actualItem.JobId];

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Customer);
                Assert.Equal(expectedItem.Customer.Id, actualItem.Customer.Id);
                Assert.Equal(expectedItem.Customer.Lastname, actualItem.Customer.Lastname);
                Assert.Equal(expectedItem.Customer.Surname, actualItem.Customer.Surname);
                Assert.Equal(expectedItem.Customer.CompanyName, actualItem.Customer.CompanyName);
                Assert.Equal(expectedItem.Customer.EmailAddress, actualItem.Customer.EmailAddress);
                Assert.Equal(expectedItem.Customer.Phone, actualItem.Customer.Phone);

                Assert.NotNull(actualItem.Customer.Address);
                Assert.Equal(expectedItem.Customer.Address.City, actualItem.Customer.Address.City);
                Assert.Equal(expectedItem.Customer.Address.PostalCode, actualItem.Customer.Address.PostalCode);
                Assert.Equal(expectedItem.Customer.Address.StreetNumber, actualItem.Customer.Address.StreetNumber);
                Assert.Equal(expectedItem.Customer.Address.Street, actualItem.Customer.Address.Street);
            }
        }
    }
}

