﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenActivationJobsOverview : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenActivationJobsOverview|JobContextTester")]
        public async Task GetOpenActivationJobsOverview()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);
            Dictionary<Int32, SimpleActivationJobOverviewModel> expectedResult = new Dictionary<int, SimpleActivationJobOverviewModel>();
            for (int i = 0; i < branchableAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                ActivationJobDataModel jobDataModel = new ActivationJobDataModel
                {
                    Flat = flat,
                    Customer = customer,
                };

                storage.ActivationJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean bounded = random.NextDouble() > 0.5;
                if (bounded == true)
                {
                    CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
                    storage.CollectionJobs.Add(collectionJob);
                    storage.SaveChanges();

                    jobDataModel.Collection = collectionJob;
                    storage.SaveChanges();
                }
                else
                {
                    SimpleActivationJobOverviewModel expectedItem = new SimpleActivationJobOverviewModel
                    {
                        JobId = jobDataModel.Id,
                        Building = new SimpleBuildingOverviewModel
                        {
                            Id = building.Id,
                            CommercialUnits = building.CommercialUnits,
                            HouseholdUnits = building.HouseholdUnits,
                            StreetName = building.StreetName,
                        },
                        Flat = new SimpleFlatOverviewModel
                        {
                            BuildingId = flat.BuildingId,
                            Floor = flat.Floor,
                            Id = flat.Id,
                            Number = flat.Number,
                            Description = flat.Description,
                        },
                    };

                    expectedResult.Add(jobDataModel.Id, expectedItem);
                }
            }

            IEnumerable<SimpleActivationJobOverviewModel> actual = await storage.GetOpenActivationJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimpleActivationJobOverviewModel actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.JobId));
                SimpleActivationJobOverviewModel expectedItem = expectedResult[actualItem.JobId];

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Building);
                Assert.Equal(expectedItem.Building.Id, actualItem.Building.Id);
                Assert.Equal(expectedItem.Building.StreetName, actualItem.Building.StreetName);
                Assert.Equal(expectedItem.Building.HouseholdUnits, actualItem.Building.HouseholdUnits);
                Assert.Equal(expectedItem.Building.CommercialUnits, actualItem.Building.CommercialUnits);

                Assert.NotNull(actualItem.Flat);
                Assert.Equal(expectedItem.Flat.Id, actualItem.Flat.Id);
                Assert.Equal(expectedItem.Flat.BuildingId, actualItem.Flat.BuildingId);
                Assert.Equal(expectedItem.Flat.Description, actualItem.Flat.Description);
                Assert.Equal(expectedItem.Flat.Floor, actualItem.Flat.Floor);
                Assert.Equal(expectedItem.Flat.Number, actualItem.Flat.Number);
            }
        }
    }
}

