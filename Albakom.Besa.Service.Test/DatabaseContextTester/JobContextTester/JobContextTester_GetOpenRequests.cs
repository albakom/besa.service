﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenRequests : DatabaseTesterBase
    {
        [Fact]
        public async Task GetOpenRequests()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(123456);

            Int32 contactAmount = random.Next(10, 30);
            List<ContactInfoDataModel> contacts = new List<ContactInfoDataModel>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                ContactInfoDataModel contact = base.GenerateContactInfo(random);
                contacts.Add(contact);
            }

            storage.ContactInfos.AddRange(contacts);
            storage.SaveChanges();
            List<Int32> contactIds = contacts.Select(x => x.Id).ToList();

            Int32 buildingAmount = random.Next(10, 30);
            List<BuildingDataModel> buildings = new List<BuildingDataModel>(buildingAmount);
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildings.Add(buildingDataModel);
            }

            storage.Buildings.AddRange(buildings);
            storage.SaveChanges();

            Int32 requestAmount = random.Next(10, 30);
            List<CustomerConnenctionRequestDataModel> requests = new List<CustomerConnenctionRequestDataModel>();
            List<CustomerConnenctionRequestDataModel> openRequest = new List<CustomerConnenctionRequestDataModel>();
            Dictionary<Int32, BuildingDataModel> requestToBuildingMapper = new Dictionary<int, BuildingDataModel>();
            for (int i = 0; i < requestAmount; i++)
            {
                CustomerConnenctionRequestDataModel requestDataModel = base.GenerateConnenctionRequestDataModel(random,contactIds);
                BuildingDataModel building = buildings[random.Next(0, buildings.Count)];

                if (requestDataModel.OnlyHouseConnection == true)
                {
                    requestDataModel.Building = building;
                }
                else
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = building;

                    storage.Flats.Add(flat);
                    storage.SaveChanges();

                    requestDataModel.Flat = flat;
                }
                if(requestDataModel.IsClosed == false)
                {
                    openRequest.Add(requestDataModel);
                }
                requests.Add(requestDataModel);
                storage.CustomerRequests.Add(requestDataModel);
                storage.SaveChanges();

                requestToBuildingMapper.Add(requestDataModel.Id, building);
            }

            IEnumerable<RequestOverviewModel> actual =  await storage.GetOpenRequests();

            Assert.NotNull(actual);
            Assert.Equal(openRequest.Count, actual.Count());

            foreach (RequestOverviewModel actualItem in actual)
            {
                CustomerConnenctionRequestDataModel dataModel = openRequest.FirstOrDefault(x => x.Id == actualItem.Id);

                Assert.NotNull(dataModel);

                Assert.Equal(dataModel.Id, actualItem.Id);
                Assert.Equal(dataModel.OnlyHouseConnection, actualItem.OnlyHouseConnection);

                Assert.NotNull(actualItem.PrimaryPerson);

                Int32 contactId = 0;
                if (dataModel.OnlyHouseConnection == true)
                {
                    contactId = dataModel.PropertyOwnerPersonId.Value;
                }
                else
                {
                    contactId = dataModel.CustomerPersonId.Value;
                }

                ContactInfoDataModel expectedContact = contacts.FirstOrDefault(x => x.Id == contactId);

                Assert.Equal(expectedContact.Lastname, actualItem.PrimaryPerson.Lastname);
                Assert.Equal(expectedContact.Phone, actualItem.PrimaryPerson.Phone);
                Assert.Equal(expectedContact.Surname, actualItem.PrimaryPerson.Surname);
                Assert.Equal(expectedContact.Type, actualItem.PrimaryPerson.Type);
                Assert.Equal(expectedContact.EmailAddress, actualItem.PrimaryPerson.EmailAddress);
                Assert.Equal(expectedContact.CompanyName, actualItem.PrimaryPerson.CompanyName);

                Assert.NotNull(actualItem.PrimaryPerson.Address);
                Assert.Equal(expectedContact.City, actualItem.PrimaryPerson.Address.City);
                Assert.Equal(expectedContact.Street, actualItem.PrimaryPerson.Address.Street);
                Assert.Equal(expectedContact.StreetNumber, actualItem.PrimaryPerson.Address.StreetNumber);
                Assert.Equal(expectedContact.PostalCode, actualItem.PrimaryPerson.Address.PostalCode);

                BuildingDataModel expectedBuilding = requestToBuildingMapper[dataModel.Id];

                Assert.NotNull(actualItem.Building);
                Assert.Equal(expectedBuilding.Id, actualItem.Building.Id);
                Assert.Equal(expectedBuilding.StreetName, actualItem.Building.StreetName);
                Assert.Equal(expectedBuilding.HouseholdUnits, actualItem.Building.HouseholdUnits);
                Assert.Equal(expectedBuilding.CommercialUnits, actualItem.Building.CommercialUnits);
            }
        }
    }
}
