﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetRequestIdByFilterValues : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetRequestIdByFilterValues|JobContextTester")]
        public async Task GetRequestIdByFilterValues()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel type = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(type);
            storage.SaveChanges();

            Int32 amount = random.Next(30, 100);

            Dictionary<RequestFilterModel, Int32?> expectedResults = new Dictionary<RequestFilterModel, int?>();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);

            for (int i = 0; i < amount; i++)
            {
                CustomerConnenctionRequestDataModel dataModel = base.GenerateConnenctionRequestDataModel(random, contacts.Select(x => x.Id).ToList());
                storage.CustomerRequests.Add(dataModel);

                storage.SaveChanges();

                if(dataModel.BuildingId.HasValue == false) { continue;  }

                RequestFilterModel positvFilter = new RequestFilterModel
                {
                    BuildingId = dataModel.BuildingId.Value,
                    OnlyConnection = dataModel.OnlyHouseConnection,
                    OwnerContactId = dataModel.PropertyOwnerPersonId,
                };

                RequestFilterModel negativFilter = new RequestFilterModel
                {
                    BuildingId = dataModel.BuildingId.Value,
                    OnlyConnection = !dataModel.OnlyHouseConnection,
                    OwnerContactId = dataModel.PropertyOwnerPersonId,
                };

                expectedResults.Add(positvFilter, dataModel.Id);
                expectedResults.Add(negativFilter, null);
            }

            foreach (KeyValuePair< RequestFilterModel,Int32?> expectedResult in expectedResults)
            {
                Int32? result = await storage.GetRequestIdByFilterValues(expectedResult.Key);

                Assert.Equal(expectedResult.Value, result);
            }
        }
    }
}
