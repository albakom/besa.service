﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchBranchOffJobByBuildingNameQuery : DatabaseTesterBase
    {
        [Fact(DisplayName = "SearchBranchOffJobByBuildingNameQuery_WithoutJobState")]
        public async Task SearchBranchOffJobByBuildingNameQuery_WithoutJobState()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, SimpleJobOverview> expectedItems = new Dictionary<int, SimpleJobOverview>(buildingAmount);
            String query = "tetst";
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                BranchOffJobDataModel branchOffJobDataModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                storage.BranchOffJobs.Add(branchOffJobDataModel);
                storage.SaveChanges();

                Boolean useForQuery = random.NextDouble() > 0.5;
                if (useForQuery == true)
                {
                    building.StreetName = $"{query}-{random.Next()}";
                    storage.SaveChanges();

                    SimpleJobOverview expectedItem = new SimpleJobOverview
                    {
                        Id = branchOffJobDataModel.Id,
                        JobType = JobTypes.BranchOff,
                        Name = building.StreetName,
                        State = JobStates.Open,
                    };

                    expectedItems.Add(branchOffJobDataModel.Id,expectedItem);
                }
            }

            Int32 amount = expectedItems.Count / 2;

            IEnumerable<SimpleJobOverview> actual = await storage.SearchBranchOffJobByBuildingNameQuery(query,null, amount);
            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count());

            foreach (SimpleJobOverview acutalItem in actual)
            {
                Assert.True(expectedItems.ContainsKey(acutalItem.Id));

                SimpleJobOverview expectedItem = expectedItems[acutalItem.Id];
                Assert.Equal(expectedItem.Id, acutalItem.Id);
                Assert.Equal(expectedItem.Name, acutalItem.Name);
                Assert.Equal(expectedItem.JobType, acutalItem.JobType);
                Assert.Equal(expectedItem.State, acutalItem.State);
            }
        }


        [Fact(DisplayName = "SearchBranchOffJobByBuildingNameQuery_WithJobState")]
        public async Task SearchBranchOffJobByBuildingNameQuery_WithJobState()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            UserDataModel acceptor = base.GenerateUserDataModel(null);
            storage.Users.AddRange(jobber, acceptor);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<JobStates, Dictionary<Int32, SimpleJobOverview>> allItems = new Dictionary<JobStates, Dictionary<int, SimpleJobOverview>>();
            allItems.Add(JobStates.Open, new Dictionary<int, SimpleJobOverview>());
            allItems.Add(JobStates.Finished, new Dictionary<int, SimpleJobOverview>());
            allItems.Add(JobStates.Acknowledged, new Dictionary<int, SimpleJobOverview>());

            String query = "tetst";
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);
                storage.SaveChanges();

                BranchOffJobDataModel branchOffJobDataModel = new BranchOffJobDataModel
                {
                    Building = building,
                };

                storage.BranchOffJobs.Add(branchOffJobDataModel);
                storage.SaveChanges();

                Boolean useForQuery = random.NextDouble() > 0.5;
                if (useForQuery == true)
                {
                    

                    building.StreetName = $"{query}-{random.Next()}";
                    storage.SaveChanges();

                    JobStates state = JobStates.Open;

                    Double randomValue = random.NextDouble();
                    if (randomValue < 0.3)
                    {
                        //do nothing --> open
                    }
                    else if (randomValue < 0.6)
                    {
                        state = JobStates.Finished;
                        BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                        {
                            Job = branchOffJobDataModel,
                            DoneBy = jobber,
                            FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                        };

                        storage.FinishedBranchOffJobs.Add(finishedDataModel);
                        storage.SaveChanges();
                    }
                    else
                    {
                        state = JobStates.Acknowledged;
                        BranchOffJobFinishedDataModel finishedDataModel = new BranchOffJobFinishedDataModel
                        {
                            Job = branchOffJobDataModel,
                            DoneBy = jobber,
                            FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                            Accepter = acceptor,
                            AcceptedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                        };

                        storage.FinishedBranchOffJobs.Add(finishedDataModel);
                        storage.SaveChanges();
                    }

                    SimpleJobOverview expectedItem = new SimpleJobOverview
                    {
                        Id = branchOffJobDataModel.Id,
                        JobType = JobTypes.BranchOff,
                        Name = building.StreetName,
                        State = state,
                    };

                    allItems[state].Add(branchOffJobDataModel.Id, expectedItem);
                }
            }

            foreach (KeyValuePair<JobStates,Dictionary<Int32,SimpleJobOverview>> item in allItems)
            {
                Dictionary<Int32, SimpleJobOverview> expectedItems = item.Value;

                Int32 amount = expectedItems.Count / 2;

                IEnumerable<SimpleJobOverview> actual = await storage.SearchBranchOffJobByBuildingNameQuery(query, item.Key, amount);
                Assert.NotNull(actual);

                Assert.Equal(amount, actual.Count());

                foreach (SimpleJobOverview acutalItem in actual)
                {
                    Assert.True(expectedItems.ContainsKey(acutalItem.Id));

                    SimpleJobOverview expectedItem = expectedItems[acutalItem.Id];
                    Assert.Equal(expectedItem.Id, acutalItem.Id);
                    Assert.Equal(expectedItem.Name, acutalItem.Name);
                    Assert.Equal(expectedItem.JobType, acutalItem.JobType);
                    Assert.Equal(expectedItem.State, acutalItem.State);
                }
            }
        }
    }
}
