﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchBranchablesAndGetPrepareJobId : DatabaseTesterBase
    {
        [Fact(DisplayName = "SearchBranchablesAndGetPrepareJobId|JobContextTester")]
        public async Task SearchBranchablesAndGetPrepareJobId()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            Int32 branchableAmount = random.Next(30, 100);
            Dictionary<Int32, String> expectedResult = new Dictionary<Int32, String>();
            String query = "tetst";
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel branchable = base.GenerateCabinet(random);
                Boolean useForQuery = random.NextDouble() > 0.5;
                if (useForQuery == true)
                {
                    branchable.Name = $"{query}-{random.Next()}";
                }

                storage.Branchables.Add(branchable);
                storage.SaveChanges();

                if (useForQuery == true)
                {
                    PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel
                    {
                        Branchable = branchable
                    };

                    storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    expectedResult.Add(jobDataModel.Id, branchable.Name);
                }
            }

            Int32 amount = expectedResult.Count / 2;

            Dictionary<Int32, String> actual = await storage.SearchBranchablesAndGetPrepareJobId(query, amount);
            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count);

            foreach (KeyValuePair<Int32, String> actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.Key));
                Assert.Equal(expectedResult[actualItem.Key], actualItem.Value);
            }
        }
    }
}
