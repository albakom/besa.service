﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchBuildingsAndGetInjectJobIds : DatabaseTesterBase
    {
        [Fact]
        public async Task SearchBuildingsAndGetInjectJobIds()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 constructionStageId = random.Next();

            ConstructionStageDataModel constructionStage = base.GenerateConstructionStage(random);
            constructionStage.Id = constructionStageId;
            storage.ConstructionStages.Add(constructionStage);
            storage.SaveChanges();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, String> expectedResult = new Dictionary<Int32, String>();
            String query = "tetst";
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                Boolean useForQuery = random.NextDouble() > 0.5;
                if(useForQuery == true)
                {
                    building.StreetName = $"{query}-{random.Next()}";
                }

                storage.Buildings.Add(building);
                storage.SaveChanges();

                if (useForQuery == true)
                {
                    InjectJobDataModel jobDataModel = new InjectJobDataModel
                    {
                        Building = building,
                        CustomerContact = customer,
                    };

                    storage.InjectJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    expectedResult.Add(jobDataModel.Id, building.StreetName);
                }
            }

            Int32 amount = expectedResult.Count / 2;

            Dictionary<Int32, String> actual = await storage.SearchBuildingsAndGetInjectJobIds(query, amount);
            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count);

            foreach (KeyValuePair<Int32, String> actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.Key));
                Assert.Equal(expectedResult[actualItem.Key], actualItem.Value);
            }
        }
    }
}
