﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchBuildingsAndGetSplicesJobsIds : DatabaseTesterBase
    {
        [Fact(DisplayName = "SearchBuildingsAndGetSplicesJobsIds|JobContextTester")]
        public async Task SearchBuildingsAndGetSplicesJobsIds()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            Int32 buildingAmount = random.Next(30, 100);
            Dictionary<Int32, String> expectedResult = new Dictionary<Int32, String>();
            String query = "tetst";
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel building = base.GenerateBuilding(random);
                storage.Buildings.Add(building);

                Boolean useForQuery = random.NextDouble() > 0.5;
                if (useForQuery == true)
                {
                    building.StreetName = $"{query}-{random.Next()}";
                }

                storage.SaveChanges();

                Int32 flatAmount = random.Next(3, 10);
                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = building;
                    storage.Flats.Add(flat);

                    storage.SaveChanges();

                    if (useForQuery == true)
                    {
                        SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel
                        {
                            Flat = flat,
                            CustomerContact = customer,
                        };

                        storage.SpliceBranchableJobs.Add(jobDataModel);
                        storage.SaveChanges();

                        expectedResult.Add(jobDataModel.Id, $"{ building.StreetName} - {flat.Number}");
                    }
                }
            }

            Int32 amount = expectedResult.Count / 2;

            Dictionary<Int32, String> actual = await storage.SearchBuildingsAndGetSplicesJobsIds(query, amount);
            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count);

            foreach (KeyValuePair<Int32, String> actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.Key));
                Assert.Equal(expectedResult[actualItem.Key], actualItem.Value);
            }
        }
    }
}
