﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchContacts : DatabaseTesterBase
    {
        [Fact]
        public async Task SearchContacts()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 amount = random.Next(100, 300);

            String surnameQuery = $"TollerEinfall-{random.Next()}";
            String lastnameQuery = $"WiederEineGuteIdee-{random.Next()}";
            String companyNameQuery = $"NichtsBesonderes-{random.Next()}";

            Dictionary<String, List<ContactInfoDataModel>> expectedResults = new Dictionary<String, List<ContactInfoDataModel>>();
            expectedResults.Add(surnameQuery, new List<ContactInfoDataModel>());
            expectedResults.Add(lastnameQuery, new List<ContactInfoDataModel>());
            expectedResults.Add(companyNameQuery, new List<ContactInfoDataModel>());

            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel dataModel = new ContactInfoDataModel
                {
                    City = $"Muserstadt {random.Next()}",
                    PostalCode = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Street = $"Straße Nr. {random.Next()}",
                    StreetNumber = $"{random.Next(1, 100)}",
                    CompanyName = random.NextDouble() > 0.5 ? $"Firma Nr {random.Next()}" : String.Empty,
                    EmailAddress = $"test@test-{random.Next()}.de",
                    Phone = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Lastname = $"Nachname {random.Next()}",
                    Surname = $"Vorname {random.Next()}",
                };

                Double randomValue = random.NextDouble();
                if (randomValue > 0.66)
                {
                    dataModel.Surname = surnameQuery;
                    expectedResults[surnameQuery].Add(dataModel);
                }
                else if (randomValue > 0.33)
                {
                    dataModel.Lastname = lastnameQuery;
                    expectedResults[lastnameQuery].Add(dataModel);
                }
                else
                {
                    dataModel.CompanyName = companyNameQuery;
                    expectedResults[companyNameQuery].Add(dataModel);
                }

                storage.ContactInfos.Add(dataModel);
            }

            storage.SaveChanges();

            foreach (KeyValuePair<String, List<ContactInfoDataModel>> item in expectedResults)
            {
                Int32 searchAmount = random.Next(1, item.Value.Count * 2);
                if (searchAmount > item.Value.Count)
                {
                    searchAmount = item.Value.Count;
                }

                IEnumerable<PersonInfo> actual = await storage.SearchContacts(item.Key, searchAmount);

                Assert.NotNull(actual);

                Assert.True(actual.Count() <= searchAmount);

                foreach (PersonInfo acutalIem in actual)
                {
                    ContactInfoDataModel dataModel = item.Value.FirstOrDefault(x => x.Id == acutalIem.Id);

                    Assert.NotNull(dataModel);

                    Assert.Equal(dataModel.Lastname, acutalIem.Lastname);
                    Assert.Equal(dataModel.Phone, acutalIem.Phone);
                    Assert.Equal(dataModel.Surname, acutalIem.Surname);
                    Assert.Equal(dataModel.Type, acutalIem.Type);
                    Assert.Equal(dataModel.EmailAddress, acutalIem.EmailAddress);
                    Assert.Equal(dataModel.CompanyName, acutalIem.CompanyName);

                    Assert.NotNull(acutalIem.Address);
                    Assert.Equal(dataModel.City, acutalIem.Address.City);
                    Assert.Equal(dataModel.Street, acutalIem.Address.Street);
                    Assert.Equal(dataModel.StreetNumber, acutalIem.Address.StreetNumber);
                    Assert.Equal(dataModel.PostalCode, acutalIem.Address.PostalCode);
                }
            }
        }
    }
}
