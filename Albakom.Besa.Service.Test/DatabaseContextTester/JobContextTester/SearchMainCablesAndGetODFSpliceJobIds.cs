﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_SearchMainCablesAndGetODFSpliceJobIds : DatabaseTesterBase
    {
        [Fact(DisplayName = "SearchMainCablesAndGetODFSpliceJobIds|JobContextTester")]
        public async Task SearchMainCablesAndGetODFSpliceJobIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(cableTypeDataModel);
            storage.SaveChanges();

            Int32 cableAmount = random.Next(30, 100);
            Dictionary<Int32, String> expectedResult = new Dictionary<Int32, String>();
            String query = "tetst";
            for (int i = 0; i < cableAmount; i++)
            {
                FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, cableTypeDataModel);

                Boolean useForQuery = random.NextDouble() > 0.5;
                if (useForQuery == true)
                {
                    cableDataModel.Name = $"{query}-{random.Next()}";
                }

                storage.FiberCables.Add(cableDataModel);
                storage.SaveChanges();

                if (useForQuery == true)
                {
                    SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel
                    {
                        Cable = cableDataModel
                    };

                    storage.SpliceODFJobs.Add(jobDataModel);
                    storage.SaveChanges();

                    expectedResult.Add(jobDataModel.Id, cableDataModel.Name);
                }
            }

            Int32 amount = expectedResult.Count / 2;

            Dictionary<Int32, String> actual = await storage.SearchMainCablesAndGetODFSpliceJobIds(query, amount);
            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count);

            foreach (KeyValuePair<Int32, String> actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.Key));
                Assert.Equal(expectedResult[actualItem.Key], actualItem.Value);
            }
        }
    }
}
