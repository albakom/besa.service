﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBranchableIsPreparedForSplice : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBranchableIsPreparedForSplice_True|JobContextTester")]
        public async Task CheckIfBranchableIsPreparedForSplice_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = cabinetDataModel };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            PrepareBranchableJobFinishedDataModel finishedDataModel = new PrepareBranchableJobFinishedDataModel
            {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                DoneBy = jobber
            };
            storage.FinishedPrepareBranchableForSpliceJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfBranchableIsPreparedForSplice(cabinetDataModel.Id);
            Assert.True(exits);
        }

        [Fact(DisplayName = "CheckIfBranchableIsPreparedForSplice_False|JobContextTester")]
        public async Task CheckIfBranchableIsPreparedForSplice_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = cabinetDataModel };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            //PrepareBranchableJobFinishedDataModel finishedDataModel = new PrepareBranchableJobFinishedDataModel
            //{
            //    Job = jobDataModel,
            //    FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
            //    DoneBy = jobber
            //};
            //storage.FinishedPrepareBranchableForSpliceJobs.Add(finishedDataModel);
            //storage.SaveChanges();

            Boolean exits = await storage.CheckIfBranchableIsPreparedForSplice(cabinetDataModel.Id);
            Assert.False(exits);
        }
    }
}
