﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfBuildingConnenctionIsFinishedByFlatId : DatabaseTesterBase
    {
        [Fact]
        public async Task CheckIfBuildingConnenctionIsFinishedByFlatId_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingId = random.Next();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.Id = buildingId;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel { Building = building };
            storage.ConnectBuildingJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            ConnectBuildingFinishedDataModel finishedDataModel = new ConnectBuildingFinishedDataModel
            {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                DoneBy = jobber
            };

            storage.FinishedConnectionBuildingJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfBuildingConnenctionIsFinishedByFlatId(flat.Id);
            Assert.True(exits);
        }

        [Fact]
        public async Task CheckIfBuildingConnenctionIsFinishedByFlatId_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 buildingId = random.Next();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            //ConnectBuildingJobDataModel jobDataModel = new ConnectBuildingJobDataModel { Building = building };
            //storage.ConnectBuildingJobs.Add(jobDataModel);
            //storage.SaveChanges();

            Boolean exits = await storage.CheckIfBuildingConnenctionIsFinishedByFlatId(flat.Id);
            Assert.False(exits);
        }
    }
}
