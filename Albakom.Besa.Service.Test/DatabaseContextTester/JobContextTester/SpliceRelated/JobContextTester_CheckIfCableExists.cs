﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfCableExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCableExists|JobContextTester")]
        public async Task CheckIfCableExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel type = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(type);
            storage.SaveChanges();

            Int32 amount = random.Next(30, 100);

            List<FiberCableDataModel> dataModels = new List<FiberCableDataModel>();
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel dataModel = base.GenerateFiberCableDataModel(random, type);
                dataModels.Add(dataModel);
            }

            storage.FiberCables.AddRange(dataModels);
            storage.SaveChanges();

            List<Int32> allIds = dataModels.Select(x => x.Id).ToList();
            Int32 existingId = allIds[random.Next(0, allIds.Count)];
            Int32 notExisitngID = allIds.Count + 2;
            while(allIds.Contains(notExisitngID) == true)
            {
                notExisitngID = random.Next();
            }

            Boolean existingResult = await storage.CheckIfCableExists(existingId);
            Boolean notExistingResult = await storage.CheckIfCableExists(notExisitngID);

            Assert.True(existingResult);
            Assert.False(notExistingResult);
        }
    }
}
