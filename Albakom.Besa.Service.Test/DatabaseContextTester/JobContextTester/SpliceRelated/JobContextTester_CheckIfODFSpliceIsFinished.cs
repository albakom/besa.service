﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfODFSpliceIsFinished : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfODFSpliceIsFinished_True|JobContextTester")]
        public async Task CheckIfODFSpliceIsFinished_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            FiberCableTypeDataModel fiberCableType = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableType);
            storage.SaveChanges();

            FiberCableDataModel fiberCable = base.GenerateFiberCableDataModel(random, fiberCableType);
            storage.FiberCables.Add(fiberCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = fiberCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            SpliceODFJobFinishedDataModel finishedDataModel = new SpliceODFJobFinishedDataModel
            {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                DoneBy = jobber,
                CableMetric = random.Next(0,5) + random.NextDouble(),
            };
            storage.FinishedSpliceODFJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfODFSpliceIsFinished(fiberCable.Id);
            Assert.True(exits);
        }

        [Fact(DisplayName = "CheckIfODFSpliceIsFinished_False|JobContextTester")]
        public async Task CheckIfODFSpliceIsFinished_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            FiberCableTypeDataModel fiberCableType = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableType);
            storage.SaveChanges();

            FiberCableDataModel fiberCable = base.GenerateFiberCableDataModel(random, fiberCableType);
            storage.FiberCables.Add(fiberCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = fiberCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            //SpliceODFJobFinishedDataModel finishedDataModel = new SpliceODFJobFinishedDataModel
            //{
            //    Job = jobDataModel,
            //    FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
            //    DoneBy = jobber,
            //    CableMetric = random.Next(0, 5) + random.NextDouble(),
            //};
            // storage.FinishedSpliceODFJobs.Add(finishedDataModel);
            // storage.SaveChanges();

            Boolean exits = await storage.CheckIfODFSpliceIsFinished(fiberCable.Id);
            Assert.False(exits);
        }
    }
}
