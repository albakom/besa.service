﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfPrepareBranchableJobExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfPrepareBranchableJobExists|JobContextTester")]
        public async Task CheckIfPrepareBranchableJobExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            List<Int32> ids = new List<Int32>(amount);
            for (int i = 0; i < amount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = cabinetDataModel };
                storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
                storage.SaveChanges();

                ids.Add(jobDataModel.Id);
            }

            Int32 idToCheck = ids[random.Next(0, ids.Count)];

            Int32 idToFail = random.Next();
            while(ids.Contains(idToFail) == true)
            {
                idToFail = random.Next();
            }

            Boolean exits = await storage.CheckIfPrepareBranchableJobExists(idToCheck);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfPrepareBranchableJobExists(idToFail);
            Assert.False(notExits);
        }
    }
}
