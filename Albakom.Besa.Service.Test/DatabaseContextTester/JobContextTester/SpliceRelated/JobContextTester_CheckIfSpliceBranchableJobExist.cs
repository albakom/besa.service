﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfSpliceBranchableJobExist : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfSpliceBranchableJobExist|JobContextTester")]
        public async Task CheckIfSpliceBranchableJobExist()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            List<Int32> ids = new List<Int32>(amount);
            for (int i = 0; i < amount; i++)
            {
                ContactInfoDataModel customer = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customer);
                storage.SaveChanges();

                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.StreetCabintes.Add(cabinetDataModel);
                storage.SaveChanges();

                BuildingDataModel building = base.GenerateBuilding(random);
                building.Branchable = cabinetDataModel;
                storage.Buildings.Add(building);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = building;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = customer };
                storage.SpliceBranchableJobs.Add(jobDataModel);
                storage.SaveChanges();

                ids.Add(jobDataModel.Id);
            }

            Int32 idToCheck = ids[random.Next(0, ids.Count)];

            Int32 idToFail = random.Next();
            while (ids.Contains(idToFail) == true)
            {
                idToFail = random.Next();
            }

            Boolean exits = await storage.CheckIfSpliceBranchableJobExist(idToCheck);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfSpliceBranchableJobExist(idToFail);
            Assert.False(notExits);
        }
    }
}
