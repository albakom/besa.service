﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfSpliceIsFinishedByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfSpliceIsFinishedByFlatId_True|JobContextTester")]
        public async Task CheckIfSpliceIsFinishedByFlatId_True()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat,  CustomerContact = customer };
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            SpliceBranchableJobFinishedDataModel finishedDataModel = new SpliceBranchableJobFinishedDataModel {
                Job = jobDataModel,
                FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                DoneBy = jobber,
                CableMetric = random.Next(10, 300) + random.NextDouble() };
            storage.FinishedSpliceBranchableJobs.Add(finishedDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfSpliceIsFinishedByFlatId(flat.Id);
            Assert.True(exits);
        }

        [Fact(DisplayName = "CheckIfSpliceIsFinishedByFlatId_False|JobContextTester")]
        public async Task CheckIfSpliceIsFinishedByFlatId_False()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat,  CustomerContact = customer };
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            Boolean exits = await storage.CheckIfSpliceIsFinishedByFlatId(flat.Id);
            Assert.False(exits);
        }
    }
}
