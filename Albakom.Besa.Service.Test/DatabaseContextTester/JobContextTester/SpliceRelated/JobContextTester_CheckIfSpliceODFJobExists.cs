﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CheckIfSpliceODFJobExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfSpliceODFJobExists|JobContextTester")]
        public async Task CheckIfSpliceODFJobExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 50);

            List<Int32> ids = new List<Int32>(amount);
            for (int i = 0; i < amount; i++)
            {
                FiberCableTypeDataModel fiberCableType = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(fiberCableType);
                storage.SaveChanges();

                FiberCableDataModel fiberCable = base.GenerateFiberCableDataModel(random, fiberCableType);
                storage.FiberCables.Add(fiberCable);
                storage.SaveChanges();

                SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = fiberCable };
                storage.SpliceODFJobs.Add(jobDataModel);
                storage.SaveChanges();

                ids.Add(jobDataModel.Id);
            }

            Int32 idToCheck = ids[random.Next(0, ids.Count)];

            Int32 idToFail = random.Next();
            while(ids.Contains(idToFail) == true)
            {
                idToFail = random.Next();
            }

            Boolean exits = await storage.CheckIfSpliceODFJobExists(idToCheck);
            Assert.True(exits);

            Boolean notExits = await storage.CheckIfSpliceODFJobExists(idToFail);
            Assert.False(notExits);
        }
    }
}
