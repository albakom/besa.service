﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreatePrepareBranchableForSpliceJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreatePrepareBranchableForSpliceJob|JobContextTester")]
        public async Task CreatePrepareBranchableForSpliceJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 branchableAmount = random.Next(3, 10);
            List<CabinetDataModel> branchables = new List<CabinetDataModel>(branchableAmount);
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.Branchables.Add(cabinetDataModel);
                storage.SaveChanges();

                branchables.Add(cabinetDataModel);
            }

            PrepareBranchableForSpliceJobCreateModel jobCreateModel = new PrepareBranchableForSpliceJobCreateModel
            {
                BranchableId = branchables[random.Next(0,branchables.Count)].Id,
            };

            Int32 id = await storage.CreatePrepareBranchableForSpliceJob(jobCreateModel);

            PrepareBranchableJobDataModel dataModel = storage.PrepareBranchableForSpliceJobs.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(jobCreateModel.BranchableId, dataModel.BranchableId);

            Assert.Null(dataModel.CollectionId);
            Assert.Null(dataModel.FinishedModel);
        }
    }
}
