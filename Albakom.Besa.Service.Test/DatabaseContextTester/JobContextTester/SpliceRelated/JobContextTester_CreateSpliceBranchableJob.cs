﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateSpliceBranchableJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateSpliceBranchableJob|JobContextTester")]
        public async Task CreateSpliceBranchableJob()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 branchableAmount = random.Next(3, 10);
            List<CabinetDataModel> branchables = new List<CabinetDataModel>(branchableAmount);
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.Branchables.Add(cabinetDataModel);
                storage.SaveChanges();

                branchables.Add(cabinetDataModel);
            }

            Int32 buildingAmount = random.Next(30, 100);
            List<FlatDataModel> flats = new List<FlatDataModel>();
            for (int i = 0; i < buildingAmount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                buildingDataModel.Branchable = branchables[random.Next(0, branchables.Count)];
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 flatAmount = random.Next(1, 10);
                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = buildingDataModel;
                    storage.Flats.Add(flat);
                    storage.SaveChanges();

                    flats.Add(flat);
                }
            }

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);

            FlatDataModel choosenFlat = flats[random.Next(0, flats.Count)];
            SpliceBranchableJobCreateModel jobCreateModel = new SpliceBranchableJobCreateModel
            {
                FlatId = choosenFlat.Id,
                CustomerContactId = contacts[random.Next(0, contacts.Count)].Id,
            };

            Int32 id = await storage.CreateSpliceBranchableJob(jobCreateModel);

            SpliceBranchableJobDataModel dataModel = storage.SpliceBranchableJobs.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(jobCreateModel.FlatId, dataModel.FlatId);
            Assert.Equal(jobCreateModel.CustomerContactId, dataModel.CustomerContactId);

            Assert.Null(dataModel.CollectionId);
            Assert.Null(dataModel.FinishedModel);
        }
    }
}
