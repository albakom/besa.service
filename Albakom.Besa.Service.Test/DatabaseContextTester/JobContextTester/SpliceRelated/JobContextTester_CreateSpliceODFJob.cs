﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_CreateSpliceODFJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateSpliceODFJob|JobContextTester")]
        public async Task CreateSpliceODFJob()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableType = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableType);
            storage.SaveChanges();

            Int32 cableAmount = random.Next(10, 30);
            List<FiberCableDataModel> cables = new List<FiberCableDataModel>(cableAmount);
            for (int i = 0; i < cableAmount; i++)
            {
                FiberCableDataModel fiberCable = base.GenerateFiberCableDataModel(random, fiberCableType);
                storage.FiberCables.Add(fiberCable);
                storage.SaveChanges();

                cables.Add(fiberCable);
            }

            SpliceODFJobCreateModel jobCreateModel = new SpliceODFJobCreateModel
            {
                MainCableId = cables[random.Next(0,cables.Count)].Id,
            };

            Int32 id = await storage.CreateSpliceODFJob(jobCreateModel);

            SpliceODFJobDataModel dataModel = storage.SpliceODFJobs.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(jobCreateModel.MainCableId, dataModel.CableId);

            Assert.Null(dataModel.CollectionId);
            Assert.Null(dataModel.FinishedModel);
        }
    }
}
