﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishActivationJob : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishActivationJob|JobContextTester")]
        public async Task FinishActivationJob()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            ActivationJobDataModel job = new ActivationJobDataModel { Flat = flat, Customer = customer };
            storage.ActivationJobs.Add(job);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            ActivationJobFinishModel finishModel = base.GeneretateFinsihModel<ActivationJobFinishModel>(job, random, files, jobber);
            //finishModel.CableMetric = random.NextDouble() + random.Next(30, 100);

            Boolean result = await storage.FinishActivationJob(finishModel);
            Assert.True(result);

            ActivationJobFinishedDataModel actual = storage.FinishedActivationJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
            //Assert.Equal(finishModel.CableMetric, actual.CableMetric);
        }
    }
}
