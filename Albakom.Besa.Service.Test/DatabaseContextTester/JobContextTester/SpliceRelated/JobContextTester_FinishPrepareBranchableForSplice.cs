﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishPrepareBranchableForSplice : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishPrepareBranchableForSplice|JobContextTester")]
        public async Task FinishPrepareBranchableForSplice()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();
           
            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            PrepareBranchableJobDataModel job = new PrepareBranchableJobDataModel {  Branchable = cabinetDataModel };
            storage.PrepareBranchableForSpliceJobs.Add(job);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            PrepareBranchableForSpliceJobFinishModel finishModel = base.GeneretateFinsihModel<PrepareBranchableForSpliceJobFinishModel>(job, random, files, jobber);

            Boolean result = await storage.FinishPrepareBranchableForSplice(finishModel);
            Assert.True(result);

            PrepareBranchableJobFinishedDataModel actual = storage.FinishedPrepareBranchableForSpliceJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
        }
    }
}
