﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishSpliceBranchable : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishSpliceBranchable|JobContextTester")]
        public async Task FinishSpliceBranchable()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            ContactInfoDataModel customer = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customer);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            storage.Buildings.Add(building);
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            SpliceBranchableJobDataModel job = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = customer };
            storage.SpliceBranchableJobs.Add(job);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            SpliceBranchableJobFinishModel finishModel = base.GeneretateFinsihModel<SpliceBranchableJobFinishModel>(job, random, files, jobber);
            finishModel.CableMetric = random.NextDouble() + random.Next(30, 100);

            Boolean result = await storage.FinishSpliceBranchable(finishModel);
            Assert.True(result);

            SpliceBranchableJobFinishedDataModel actual = storage.FinishedSpliceBranchableJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
            Assert.Equal(finishModel.CableMetric, actual.CableMetric);
        }
    }
}
