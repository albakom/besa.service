﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_FinishSpliceODF : DatabaseTesterBase
    {
        [Fact(DisplayName = "FinishSpliceODF|JobContextTester")]
        public async Task FinishSpliceODF()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel fiberCableType = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(fiberCableType);
            storage.SaveChanges();

            FiberCableDataModel fiberCable = base.GenerateFiberCableDataModel(random, fiberCableType);
            storage.FiberCables.Add(fiberCable);
            storage.SaveChanges();

            SpliceODFJobDataModel job = new SpliceODFJobDataModel { Cable = fiberCable };
            storage.SpliceODFJobs.Add(job);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);

            List<FileDataModel> files = GenerateAndSavesFiles(random, storage);

            SpliceODFJobFinishModel finishModel = base.GeneretateFinsihModel<SpliceODFJobFinishModel>(job, random, files, jobber);
            finishModel.CableMetric = random.Next() + random.NextDouble();

            Boolean result = await storage.FinishSpliceODF(finishModel);
            Assert.True(result);

            SpliceODFJobFinishedDataModel actual = storage.FinishedSpliceODFJobs.FirstOrDefault(x => x.JobId == job.Id);
            CheckFinishedJob(finishModel, actual, jobber, storage, files);
            Assert.Equal(finishModel.CableMetric, actual.CableMetric);
        }
    }
}
