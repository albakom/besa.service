﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenPrepareBranchableForSpliceJobsOverview : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenPrepareBranchableForSpliceJobsOverview|JobContextTester")]
        public async Task GetOpenPrepareBranchableForSpliceJobsOverview()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);
            Dictionary<Int32, SimplePrepareBranchableForSpliceJobOverviewModel> expectedResult = new Dictionary<int, SimplePrepareBranchableForSpliceJobOverviewModel>();
            for (int i = 0; i < branchableAmount; i++)
            {
                CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
                storage.Branchables.Add(cabinetDataModel);
                storage.SaveChanges();

                PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel
                {
                    Branchable = cabinetDataModel,
                };

                storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean bounded = random.NextDouble() > 0.5;
                if (bounded == true)
                {
                    CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
                    storage.CollectionJobs.Add(collectionJob);
                    storage.SaveChanges();

                    jobDataModel.Collection = collectionJob;
                    storage.SaveChanges();
                }
                else
                {
                    SimplePrepareBranchableForSpliceJobOverviewModel expectedItem = new SimplePrepareBranchableForSpliceJobOverviewModel
                    {
                        JobId = jobDataModel.Id,
                        Branchable = new SimpleBranchableWithGPSModel
                        {
                            Id = cabinetDataModel.Id,
                            Coordinate = new GPSCoordinate { Latitude = cabinetDataModel.Latitude, Longitude = cabinetDataModel.Longitude },
                            Name = cabinetDataModel.Name,
                            Type = BranchableTypes.Cabinet,
                        }
                    };

                    expectedResult.Add(jobDataModel.Id, expectedItem);
                }
            }

            IEnumerable<SimplePrepareBranchableForSpliceJobOverviewModel> actual = await storage.GetOpenPrepareBranchableForSpliceJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimplePrepareBranchableForSpliceJobOverviewModel actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.JobId));
                SimplePrepareBranchableForSpliceJobOverviewModel expectedItem = expectedResult[actualItem.JobId];

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Branchable);
                Assert.Equal(expectedItem.Branchable.Id, actualItem.Branchable.Id);
                Assert.Equal(expectedItem.Branchable.Name, actualItem.Branchable.Name);
                Assert.Equal(expectedItem.Branchable.Type, actualItem.Branchable.Type);

                Assert.NotNull(actualItem.Branchable.Coordinate);
                Assert.Equal(expectedItem.Branchable.Coordinate.Latitude, actualItem.Branchable.Coordinate.Latitude);
                Assert.Equal(expectedItem.Branchable.Coordinate.Longitude, actualItem.Branchable.Coordinate.Longitude);
            }
        }
    }
}

