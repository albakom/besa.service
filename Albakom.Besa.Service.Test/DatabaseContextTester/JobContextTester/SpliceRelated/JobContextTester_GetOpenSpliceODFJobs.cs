﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenSpliceODFJobs : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenSpliceODFJobs|JobContextTester")]
        public async Task GetOpenSpliceODFJobs()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 branchableAmount = random.Next(3, 10);
            Dictionary<Int32, SpliceODFJobOverviewModel> expectedResult = new Dictionary<int, SpliceODFJobOverviewModel>();
            for (int i = 0; i < branchableAmount; i++)
            {
                FiberCableTypeDataModel cableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
                storage.CableTypes.Add(cableTypeDataModel);
                storage.SaveChanges();

                UserDataModel jobber = base.GenerateUserDataModel(null);
                storage.Users.Add(jobber);
                storage.SaveChanges();

                CompanyDataModel company = base.GenerateCompanyDataModel();
                storage.Companies.Add(company);
                storage.SaveChanges();

                FiberCableDataModel fiberCableData = base.GenerateFiberCableDataModel(random, cableTypeDataModel);
                storage.FiberCables.Add(fiberCableData);
                storage.SaveChanges();

                SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel
                {
                    Cable = fiberCableData,
                };

                storage.SpliceODFJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean bounded = random.NextDouble() > 0.5;
                if (bounded == true)
                {
                    CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
                    collectionJob.Company = company;
                    storage.CollectionJobs.Add(collectionJob);
                    storage.SaveChanges();

                    jobDataModel.Collection = collectionJob;
                    storage.SaveChanges();
                }
                
                Boolean finished = random.NextDouble() > 0.5;
                if(finished == true)
                {
                    SpliceODFJobFinishedDataModel finishedDataModel = new SpliceODFJobFinishedDataModel {
                        Job = jobDataModel,
                        DoneBy = jobber,
                        FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3, 10))
                    };

                    storage.FinishedSpliceODFJobs.Add(finishedDataModel);
                    storage.SaveChanges();
                }
                else
                {
                    SpliceODFJobOverviewModel expectedItem = new SpliceODFJobOverviewModel
                    {
                        JobId = jobDataModel.Id,
                        State = JobStates.Open,
                        BoundedCompanyName = bounded == true ? company.Name : null,
                        Location = new GPSCoordinate(),
                        MainCable = new SpliceCableInfoModel {  Name = fiberCableData.Name, FiberAmount = cableTypeDataModel.FiberAmount },
                        PopName  = "",
                    };

                    expectedResult.Add(jobDataModel.Id, expectedItem);
                }
            }

            IEnumerable<SpliceODFJobOverviewModel> actual = await storage.GetOpenSpliceODFJobs();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SpliceODFJobOverviewModel actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.JobId));
                SpliceODFJobOverviewModel expectedItem = expectedResult[actualItem.JobId];

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);
                Assert.Equal(expectedItem.State, actualItem.State);
                Assert.Equal(expectedItem.PopName, actualItem.PopName);
                Assert.Equal(expectedItem.BoundedCompanyName, actualItem.BoundedCompanyName);

                Assert.NotNull(actualItem.Location);
                Assert.Equal(expectedItem.Location.Latitude, expectedItem.Location.Latitude);
                Assert.Equal(expectedItem.Location.Longitude, expectedItem.Location.Longitude);

                Assert.NotNull(actualItem.MainCable);
                Assert.Equal(expectedItem.MainCable.FiberAmount, expectedItem.MainCable.FiberAmount);
                Assert.Equal(expectedItem.MainCable.Name, expectedItem.MainCable.Name);
            }
        }
    }
}

