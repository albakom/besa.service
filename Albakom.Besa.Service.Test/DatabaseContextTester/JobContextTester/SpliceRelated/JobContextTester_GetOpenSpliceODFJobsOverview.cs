﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetOpenSpliceODFJobsOverview : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenSpliceODFJobsOverview|JobContextTester")]
        public async Task GetOpenSpliceODFJobsOverview()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            FiberCableTypeDataModel cableTypeDataModel = base.GenerateFiberCableTypeDataModel(random);
            storage.CableTypes.Add(cableTypeDataModel);
            storage.SaveChanges();

            Int32 branchableAmount = random.Next(3, 10);
            Dictionary<Int32, SimpleSpliceODFJobOverviewModel> expectedResult = new Dictionary<int, SimpleSpliceODFJobOverviewModel>();
            for (int i = 0; i < branchableAmount; i++)
            {
                FiberCableDataModel fiberCableData = base.GenerateFiberCableDataModel(random, cableTypeDataModel);
                storage.FiberCables.Add(fiberCableData);
                storage.SaveChanges();

                SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel
                {
                    Cable = fiberCableData,
                };

                storage.SpliceODFJobs.Add(jobDataModel);
                storage.SaveChanges();

                Boolean bounded = random.NextDouble() > 0.5;
                if (bounded == true)
                {
                    CollectionJobDataModel collectionJob = base.GenerateCollectionJobDataModel(random);
                    storage.CollectionJobs.Add(collectionJob);
                    storage.SaveChanges();

                    jobDataModel.Collection = collectionJob;
                    storage.SaveChanges();
                }
                else
                {
                    SimpleSpliceODFJobOverviewModel expectedItem = new SimpleSpliceODFJobOverviewModel
                    {
                        JobId = jobDataModel.Id,
                       CableName = fiberCableData.Name,
                    };

                    expectedResult.Add(jobDataModel.Id, expectedItem);
                }
            }

            IEnumerable<SimpleSpliceODFJobOverviewModel> actual = await storage.GetOpenSpliceODFJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimpleSpliceODFJobOverviewModel actualItem in actual)
            {
                Assert.True(expectedResult.ContainsKey(actualItem.JobId));
                SimpleSpliceODFJobOverviewModel expectedItem = expectedResult[actualItem.JobId];

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.Equal(expectedItem.CableName, actualItem.CableName);
            }
        }
    }
}

