﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetPrepareBranchbaleForSpliceJobDetails : DatabaseTesterBase
    {
        private void CheckModels(BranchableDataModel branchable, PrepareBranchableJobDataModel job, PrepareBranchableForSpliceJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(job.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.Branchable);
            Assert.Equal(branchable.Latitude, actual.Branchable.Coordinate.Latitude);
            Assert.Equal(branchable.Longitude, actual.Branchable.Coordinate.Longitude);
            Assert.Equal(branchable.Name, actual.Branchable.Name);

            Assert.NotNull(actual.MainCable);

            Assert.NotNull(actual.MainCable);
            Assert.Equal(branchable.MainCable.Id, actual.MainCable.Id);
            Assert.Equal(branchable.MainCable.Name, actual.MainCable.Name);
            Assert.Equal(branchable.MainCable.CableType.FiberAmount, actual.MainCable.FiberAmount);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails|JobContextTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            CabinetDataModel branchable = base.GenerateCabinet(random);
            branchable.MainCable = mainCable;
            storage.StreetCabintes.Add(branchable);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = branchable };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            PrepareBranchableForSpliceJobDetailModel actual = await storage.GetPrepareBranchbaleForSpliceJobDetails(jobDataModel.Id);
            CheckModels(branchable, jobDataModel, actual, JobStates.Open, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_Finished|JobContextTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            CabinetDataModel branchable = base.GenerateCabinet(random);
            branchable.MainCable = mainCable;
            storage.StreetCabintes.Add(branchable);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = branchable };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            PrepareBranchableJobFinishedDataModel finishedDataModel = new PrepareBranchableJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };

            storage.FinishedPrepareBranchableForSpliceJobs.Add(finishedDataModel);
            storage.SaveChanges();

            PrepareBranchableForSpliceJobDetailModel actual = await storage.GetPrepareBranchbaleForSpliceJobDetails(jobDataModel.Id);
            CheckModels(branchable, jobDataModel, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_Acknowledged|JobContextTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            CabinetDataModel branchable = base.GenerateCabinet(random);
            branchable.MainCable = mainCable;
            storage.StreetCabintes.Add(branchable);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = branchable };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            PrepareBranchableJobFinishedDataModel finishedDataModel = new PrepareBranchableJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,

            };

            storage.FinishedPrepareBranchableForSpliceJobs.Add(finishedDataModel);
            storage.SaveChanges();

            PrepareBranchableForSpliceJobDetailModel actual = await storage.GetPrepareBranchbaleForSpliceJobDetails(jobDataModel.Id);
            CheckModels(branchable, jobDataModel, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_WithFiles|JobContextTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            CabinetDataModel branchable = base.GenerateCabinet(random);
            branchable.MainCable = mainCable;
            storage.StreetCabintes.Add(branchable);
            storage.SaveChanges();

            PrepareBranchableJobDataModel jobDataModel = new PrepareBranchableJobDataModel { Branchable = branchable };
            storage.PrepareBranchableForSpliceJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.PrepareBranchableJob, null);

            PrepareBranchableForSpliceJobDetailModel actual = await storage.GetPrepareBranchbaleForSpliceJobDetails(jobDataModel.Id);
            CheckModels(branchable, jobDataModel, actual, JobStates.Open, expectedFiles);

            Assert.NotNull(actual.Files);
        }
    }
}
