﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetSpliceBranchableJobDetails : DatabaseTesterBase
    {
        private void CheckModels(BuildingDataModel building, SpliceBranchableJobDataModel job, SpliceBranchableJobDetailModel actual, BranchableTypes branchableType, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(job.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.DuctColor);
            Assert.Equal(building.MicroDuctColorName, actual.DuctColor.Name);
            Assert.Equal(building.MicroDuctHexValue, actual.DuctColor.RGBHexValue);

            Assert.NotNull(actual.Branchable);
            Assert.Equal(building.Branchable.Latitude, actual.Branchable.Coordinate.Latitude);
            Assert.Equal(building.Branchable.Longitude, actual.Branchable.Coordinate.Longitude);
            Assert.Equal(building.Branchable.Name, actual.Branchable.Name);

            Assert.NotNull(actual.Customer);
            CheckContact(job.CustomerContact, actual.Customer);

             Assert.NotNull(actual.BuildingCable);
            Assert.Equal(building.FiberCable.Id, actual.BuildingCable.Id);
            Assert.Equal(building.FiberCable.Name, actual.BuildingCable.Name);
            Assert.Equal(building.FiberCable.CableType.FiberAmount, actual.BuildingCable.FiberAmount);

            Assert.NotNull(actual.MainCable);
            Assert.Equal(building.Branchable.MainCable.Id, actual.MainCable.Id);
            Assert.Equal(building.Branchable.MainCable.Name, actual.MainCable.Name);
            Assert.Equal(building.Branchable.MainCable.CableType.FiberAmount, actual.MainCable.FiberAmount);

            Assert.NotNull(actual.BuildingInfo);

            Assert.Equal(building.StreetName, actual.BuildingInfo.StreetName);
            Assert.Equal(building.HouseholdUnits, actual.BuildingInfo.HouseholdUnits);
            Assert.Equal(building.CommercialUnits, actual.BuildingInfo.CommercialUnits);

            Assert.NotNull(actual.BuildingInfo.Coordinate);
            Assert.Equal(building.Latitude, actual.BuildingInfo.Coordinate.Latitude);
            Assert.Equal(building.Longitude, actual.BuildingInfo.Coordinate.Longitude);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact]
        public async Task GetSpliceBranchableJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random,storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random,storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            FiberCableDataModel houseConnectionCable = base.GenerateFiberCableDataModel(random, houseConnectionCableType);
            storage.FiberCables.AddRange(mainCable, houseConnectionCable);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.MainCable = mainCable;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCable = houseConnectionCable;
            building.Branchable = cabinetDataModel;
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = contacts[random.Next(0, contacts.Count)]};
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            SpliceBranchableJobDetailModel actual = await storage.GetSpliceBranchableJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual,BranchableTypes.Cabinet, JobStates.Open, new List<FileDataModel>());
        }

        private FiberOverviewModel GetFiberOverviewModelFromDataModel(FiberDataModel fiber)
        {
            FiberOverviewModel result = new FiberOverviewModel
            {
                BundleNumber = fiber.BundleNumber,
                Color = fiber.Color,
                FiberNumber = fiber.FiberNumber,
                Id = fiber.Id,
                Name = fiber.Name,
                NumberinBundle = fiber.NumberinBundle,
            };

            return result;
        }

        private void CheckFiber(FiberOverviewModel expected, FiberOverviewModel acutal)
        {
            Assert.Equal(expected.Id, acutal.Id);
            Assert.Equal(expected.Name, acutal.Name);
            Assert.Equal(expected.BundleNumber, acutal.BundleNumber);
            Assert.Equal(expected.Color, acutal.Color);
            Assert.Equal(expected.FiberNumber, acutal.FiberNumber);
            Assert.Equal(expected.NumberinBundle, acutal.NumberinBundle);
        }

        [Fact(DisplayName = "GetSpliceBranchableJobDetailsWithSplices|JobContextTester")]
        public async Task GetSpliceBranchableJobDetailsWithSplices()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            FiberCableDataModel houseConnectionCable = base.GenerateFiberCableDataModel(random, houseConnectionCableType);
            storage.FiberCables.AddRange(mainCable, houseConnectionCable);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.MainCable = mainCable;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCable = houseConnectionCable;
            building.Branchable = cabinetDataModel;
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            Int32 spliceAmount = random.Next(2, 12);

            Dictionary<Int32, SpliceEntryModel> expectedSpliceEntries = new Dictionary<int, SpliceEntryModel>();

            for (int i = 0; i < spliceAmount; i++)
            {
                FiberDataModel mainCableFiberDataModel = base.GenerateFiberDataModel(random, mainCable);
                FiberDataModel buildingCableDataModel = base.GenerateFiberDataModel(random, houseConnectionCable);
                storage.Fibers.AddRange(mainCableFiberDataModel, buildingCableDataModel);
                storage.SaveChanges();

                FiberSpliceDataModel spliceDataModel = base.GenerateFiberSpliceDataModel(random, mainCableFiberDataModel, buildingCableDataModel);
                storage.Splices.Add(spliceDataModel);
                storage.SaveChanges();

                expectedSpliceEntries.Add(spliceDataModel.Id, new SpliceEntryModel
                {
                    Id = spliceDataModel.Id,
                    Number = spliceDataModel.NumberInTray,
                    Tray = spliceDataModel.TrayNumber,
                    FirstFiber = GetFiberOverviewModelFromDataModel(mainCableFiberDataModel),
                    SecondFiber = GetFiberOverviewModelFromDataModel(buildingCableDataModel)
                });
            }

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            SpliceBranchableJobDetailModel actual = await storage.GetSpliceBranchableJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, BranchableTypes.Cabinet, JobStates.Open, new List<FileDataModel>());

            Assert.NotNull(actual.Entries);
            Assert.Equal(expectedSpliceEntries.Count, actual.Entries.Count());

            foreach (SpliceEntryModel actualItem in actual.Entries)
            {
                Assert.NotNull(actualItem);
                Assert.True(expectedSpliceEntries.ContainsKey(actualItem.Id));

                SpliceEntryModel expectedItem = expectedSpliceEntries[actualItem.Id];

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Tray, actualItem.Tray);
                Assert.Equal(expectedItem.Number, actualItem.Number);

                Assert.NotNull(actualItem.FirstFiber);
                Assert.NotNull(actualItem.SecondFiber);

                CheckFiber(expectedItem.FirstFiber, actualItem.FirstFiber);
                CheckFiber(expectedItem.SecondFiber, actualItem.SecondFiber);
            }
        }

        [Fact]
        public async Task GetSpliceBranchableJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            FiberCableDataModel houseConnectionCable = base.GenerateFiberCableDataModel(random, houseConnectionCableType);
            storage.FiberCables.AddRange(mainCable, houseConnectionCable);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.MainCable = mainCable;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCable = houseConnectionCable;
            building.Branchable = cabinetDataModel;
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = contacts[random.Next(0, contacts.Count)]};
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            SpliceBranchableJobFinishedDataModel finishedDataModel = new SpliceBranchableJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };

            storage.FinishedSpliceBranchableJobs.Add(finishedDataModel);
            storage.SaveChanges();

            SpliceBranchableJobDetailModel actual = await storage.GetSpliceBranchableJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, BranchableTypes.Cabinet, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact]
        public async Task GetSpliceBranchableJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            FiberCableDataModel houseConnectionCable = base.GenerateFiberCableDataModel(random, houseConnectionCableType);
            storage.FiberCables.AddRange(mainCable, houseConnectionCable);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.MainCable = mainCable;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCable = houseConnectionCable;
            building.Branchable = cabinetDataModel;
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            SpliceBranchableJobFinishedDataModel finishedDataModel = new SpliceBranchableJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,
            };

            storage.FinishedSpliceBranchableJobs.Add(finishedDataModel);
            storage.SaveChanges();

            SpliceBranchableJobDetailModel actual = await storage.GetSpliceBranchableJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual,BranchableTypes.Cabinet, JobStates.Acknowledged, new List<FileDataModel>());
        }
        
        [Fact]
        public async Task GetSpliceBranchableJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
            FiberCableTypeDataModel houseConnectionCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            FiberCableDataModel houseConnectionCable = base.GenerateFiberCableDataModel(random, houseConnectionCableType);
            storage.FiberCables.AddRange(mainCable, houseConnectionCable);
            storage.SaveChanges();

            CabinetDataModel cabinetDataModel = base.GenerateCabinet(random);
            cabinetDataModel.MainCable = mainCable;
            storage.StreetCabintes.Add(cabinetDataModel);
            storage.SaveChanges();

            BuildingDataModel building = base.GenerateBuilding(random);
            building.Branchable = cabinetDataModel;
            building.FiberCable = houseConnectionCable;
            building.Branchable = cabinetDataModel;
            storage.SaveChanges();

            FlatDataModel flat = base.GenerateFlatDataModel(random);
            flat.Building = building;
            storage.Flats.Add(flat);
            storage.SaveChanges();

            List<ContactInfoDataModel> contacts = base.GenerateAndSavesContacts(random, storage);
            SpliceBranchableJobDataModel jobDataModel = new SpliceBranchableJobDataModel { Flat = flat, CustomerContact = contacts[random.Next(0, contacts.Count)] };
            storage.SpliceBranchableJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.SpliceInBranchableJob, null);

            SpliceBranchableJobDetailModel actual = await storage.GetSpliceBranchableJobDetails(jobDataModel.Id);
            CheckModels(building, jobDataModel, actual, BranchableTypes.Cabinet, JobStates.Open, expectedFiles);

            Assert.NotNull(actual.Files);
        }
    }
}
