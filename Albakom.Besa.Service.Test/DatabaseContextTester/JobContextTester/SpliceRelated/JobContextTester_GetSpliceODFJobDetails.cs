﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.JobContextTester
{
    public class JobContextTester_GetSpliceODFJobDetails : DatabaseTesterBase
    {
        private void CheckModels(PoPDataModel pop, SpliceODFJobDataModel job, SpliceODFJobDetailModel actual, JobStates expectedJobType, List<FileDataModel> files)
        {
            Assert.NotNull(actual);

            Assert.Equal(job.Id, actual.Id);
            Assert.Equal(expectedJobType, actual.State);

            Assert.NotNull(actual.Pop);
            Assert.Equal(pop.Name, actual.Pop.Name);

            Assert.NotNull(actual.Pop.Location);
            Assert.Equal(pop.Longitude, actual.Pop.Location.Longitude);
            Assert.Equal(pop.Latitude, actual.Pop.Location.Latitude);

            Assert.NotNull(actual.MainCable);

            Assert.NotNull(actual.MainCable);
            Assert.Equal(job.Cable.Id, actual.MainCable.Id);
            Assert.Equal(job.Cable.Name, actual.MainCable.Name);
            Assert.Equal(job.Cable.CableType.FiberAmount, actual.MainCable.FiberAmount);

            Assert.NotNull(actual.Files);
            Assert.Equal(files.Count, actual.Files.Count());

            foreach (FileOverviewModel actualFile in actual.Files)
            {
                Assert.NotNull(actualFile);

                FileDataModel expectedFile = files.FirstOrDefault(x => x.Id == actualFile.Id);
                Assert.NotNull(expectedFile);

                Assert.Equal(expectedFile.Id, actualFile.Id);
                Assert.Equal(expectedFile.Size, actualFile.Size);
                Assert.Equal(expectedFile.Name, actualFile.Name);
                Assert.Equal(expectedFile.Type, actualFile.Type);
                Assert.Equal(expectedFile.Extention, actualFile.Extention);
            }
        }

        [Fact(DisplayName = "GetSpliceODFJobDetails|JobContextTester")]
        public async Task GetSpliceODFJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
            storage.Pops.Add(poPDataModel);
            storage.SaveChanges();

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            mainCable.StartingAtPop = poPDataModel;
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = mainCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            SpliceODFJobDetailModel actual = await storage.GetSpliceODFJobDetails(jobDataModel.Id);
            CheckModels(poPDataModel, jobDataModel, actual, JobStates.Open, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetSpliceODFJobDetails_Finished|JobContextTester")]
        public async Task GetSpliceODFJobDetails_Finished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
            storage.Pops.Add(poPDataModel);
            storage.SaveChanges();

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            mainCable.StartingAtPop = poPDataModel;
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = mainCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            SpliceODFJobFinishedDataModel finishedDataModel = new SpliceODFJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
            };

            storage.FinishedSpliceODFJobs.Add(finishedDataModel);
            storage.SaveChanges();

            SpliceODFJobDetailModel actual = await storage.GetSpliceODFJobDetails(jobDataModel.Id);
            CheckModels(poPDataModel, jobDataModel, actual, JobStates.Finished, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetSpliceODFJobDetails_Acknowledged|JobContextTester")]
        public async Task GetSpliceODFJobDetails_Acknowledged()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
            storage.Pops.Add(poPDataModel);
            storage.SaveChanges();

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            mainCable.StartingAtPop = poPDataModel;
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = mainCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            UserDataModel jobber = base.GenerateUserDataModel(null);
            storage.Users.Add(jobber);
            storage.SaveChanges();

            SpliceODFJobFinishedDataModel finishedDataModel = new SpliceODFJobFinishedDataModel
            {
                Job = jobDataModel,
                DoneBy = jobber,
                Accepter = jobber,

            };

            storage.FinishedSpliceODFJobs.Add(finishedDataModel);
            storage.SaveChanges();

            SpliceODFJobDetailModel actual = await storage.GetSpliceODFJobDetails(jobDataModel.Id);
            CheckModels(poPDataModel, jobDataModel, actual, JobStates.Acknowledged, new List<FileDataModel>());
        }

        [Fact(DisplayName = "GetSpliceODFJobDetails_WithFiles|JobContextTester")]
        public async Task GetSpliceODFJobDetails_WithFiles()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            FiberCableTypeDataModel mainCableType = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

            PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
            storage.Pops.Add(poPDataModel);
            storage.SaveChanges();

            FiberCableDataModel mainCable = base.GenerateFiberCableDataModel(random, mainCableType);
            mainCable.StartingAtPop = poPDataModel;
            storage.FiberCables.Add(mainCable);
            storage.SaveChanges();

            SpliceODFJobDataModel jobDataModel = new SpliceODFJobDataModel { Cable = mainCable };
            storage.SpliceODFJobs.Add(jobDataModel);
            storage.SaveChanges();

            List<FileDataModel> expectedFiles = base.GenerateFilesAccessForJobDetails(random, storage, jobDataModel.Id, FileAccessObjects.SpliceInPopJob, null);

            SpliceODFJobDetailModel actual = await storage.GetSpliceODFJobDetails(jobDataModel.Id);
            CheckModels(poPDataModel, jobDataModel, actual, JobStates.Open, expectedFiles);

            Assert.NotNull(actual.Files);
        }
    }
}
