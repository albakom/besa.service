﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_CheckIfMessageBelongsToUser : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfMessageBelongsToUser|MessageRelatedStorageTester")]
        public async Task CheckIfMessageBelongsToUser()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            Dictionary<String, HashSet<Int32>> relatedMessages = new Dictionary<string, HashSet<int>>();

            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);

                relatedMessages.Add(user.AuthServiceId, new HashSet<int>());
            }

            List<MessageActions> actions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> types = base.GeneratePossibleMessageRelatedObjectTypes();

            List<Int32> messageIds = new List<int>();

            Int32 amount = random.Next(30, 60);

            for (int i = 0; i < amount; i++)
            {
                MessageDataModel dataModel = new MessageDataModel
                {
                    Action = actions[random.Next(0, actions.Count)],
                    Details = $"TestDetails {random.Next()}",
                    MarkedAsRead = random.NextDouble() > 0.5,
                    Title = $"Nachricht Nr {random.Next()}",
                    Receiver = users[random.Next(0, users.Count)],
                    RelatedObjectType = MessageRelatedObjectTypes.NotSpecified,
                    Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Sender = users[random.Next(0, users.Count)],
                };

                storage.Messages.Add(dataModel);
                storage.SaveChanges();

                messageIds.Add(dataModel.Id);

                relatedMessages[dataModel.Receiver.AuthServiceId].Add(dataModel.Id);
            }

            foreach (KeyValuePair<String, HashSet<Int32>> input in relatedMessages)
            {
                foreach (Int32 messageId in messageIds)
                {
                    Boolean existsResult = await storage.CheckIfMessageBelongsToUser(messageId, input.Key);

                    if (input.Value.Contains(messageId) == true)
                    {
                        Assert.True(existsResult);
                    }
                    else
                    {
                        Assert.False(existsResult);
                    }
                }
            }
        }
    }
}
