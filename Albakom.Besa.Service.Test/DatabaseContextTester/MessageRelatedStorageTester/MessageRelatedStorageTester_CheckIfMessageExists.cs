﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_CheckIfMessageExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfMessageExists|MessageRelatedStorageTester")]
        public async Task CheckIfMessageExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();

            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
            }

            List<MessageActions> actions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> types = base.GeneratePossibleMessageRelatedObjectTypes();

            HashSet<Int32> messageIds = new HashSet<int>();

            Int32 amount = random.Next(30, 60);

            for (int i = 0; i < amount; i++)
            {
                MessageDataModel dataModel = new MessageDataModel
                {
                    Action = actions[random.Next(0, actions.Count)],
                    Details = $"TestDetails {random.Next()}",
                    MarkedAsRead = random.NextDouble() > 0.5,
                    Title = $"Nachricht Nr {random.Next()}",
                    Receiver = users[random.Next(0, users.Count)],
                    RelatedObjectType = MessageRelatedObjectTypes.NotSpecified,
                    Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Sender = users[random.Next(0, users.Count)],
                };

                storage.Messages.Add(dataModel);
                storage.SaveChanges();
            }

            foreach (Int32 input in messageIds)
            {
                Boolean existsResult = await storage.CheckIfMessageExists(input);
                Assert.True(existsResult);
            }

            Int32 notExistingIdAmount = random.Next(10, 30);
            for (int i = 0; i < notExistingIdAmount; i++)
            {
                Int32 notExisingId = random.Next();
                while(messageIds.Contains(notExisingId) == true)
                {
                    notExisingId = random.Next();
                }

                Boolean existsResult = await storage.CheckIfMessageExists(notExisingId);
                Assert.False(existsResult);
            }
        }
    }
}
