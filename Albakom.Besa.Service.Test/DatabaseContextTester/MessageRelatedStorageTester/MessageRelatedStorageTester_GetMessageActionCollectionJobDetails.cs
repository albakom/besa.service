﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionCollectionJobDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionCollectionJobDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionCollectionJobDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionCollectionJobDetailsModel> expectedResults = new Dictionary<Int32, MessageActionCollectionJobDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                CompanyDataModel companyData = base.GenerateCompanyDataModel(random);
                storage.Companies.Add(companyData);
                storage.SaveChanges();

                CollectionJobDataModel collectionJobData = new CollectionJobDataModel
                {
                    Company = companyData,
                    FinishedTill = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                    Name = $"Aufgabebündel Nr. {random.Next()}",
                };

                storage.CollectionJobs.Add(collectionJobData);
                storage.SaveChanges();

                JobCollectionOverviewModel overviewModel = new JobCollectionOverviewModel
                {
                    BoundedTo = new SimpleCompanyOverviewModel
                    {
                        Id = companyData.Id,
                        Name = companyData.Name,
                    },
                    Name = collectionJobData.Name,
                    FinishedTill = collectionJobData.FinishedTill,
                    CollectionJobId = collectionJobData.Id,
                    Percentage = 0.0,
                };

                Int32 jobAmount = random.Next(3, 10);
                List<SimpleJobOverview> simpleJobs = new List<SimpleJobOverview>();
                overviewModel.Jobs = simpleJobs;
                for (int j = 0; j < jobAmount; j++)
                {
                    Int32 randomValue = random.Next(0, 5);
                    JobDataModel jobData = null;

                    BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingDataModel);
                    storage.SaveChanges();

                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingDataModel;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    ContactInfoDataModel contact = base.GenerateContactInfo(random);
                    storage.ContactInfos.Add(contact);
                    storage.SaveChanges();

                    UserDataModel jobber = base.GenerateUserDataModel(null);
                    UserDataModel acceptor = base.GenerateUserDataModel(null);
                    storage.Users.AddRange(jobber, acceptor);
                    storage.SaveChanges();

                    JobTypes jobType = JobTypes.BranchOff;
                    String expectedJobName = String.Empty;
                    switch (randomValue)
                    {
                        case 0:
                            jobData = new BranchOffJobDataModel { Building = buildingDataModel };
                            jobType = JobTypes.BranchOff;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 1:
                            jobData = new ConnectBuildingJobDataModel { Building = buildingDataModel, Owner = contact };
                            jobType = JobTypes.HouseConnenction;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 2:
                            jobData = new InjectJobDataModel { Building = buildingDataModel, CustomerContact = contact };
                            jobType = JobTypes.Inject;
                            expectedJobName = buildingDataModel.StreetName;
                            break;
                        case 3:
                            jobData = new SpliceBranchableJobDataModel { Flat = flatDataModel, CustomerContact = contact };
                            jobType = JobTypes.SpliceInBranchable;
                            expectedJobName = $"{buildingDataModel.StreetName} - {flatDataModel.Number}";
                            break;
                        case 4:
                            jobData = new ActivationJobDataModel { Flat = flatDataModel, Customer = contact };
                            jobType = JobTypes.ActivationJob;
                            expectedJobName = $"{buildingDataModel.StreetName} - {flatDataModel.Number}";
                            break;
                        default:
                            break;
                    }

                    jobData.Collection = collectionJobData;
                    storage.Jobs.Add(jobData);
                    storage.SaveChanges();

                    SimpleJobOverview simpleJobOverview = new SimpleJobOverview
                    {
                        Id = jobData.Id,
                        Name = expectedJobName,
                        JobType = jobType,
                        State = JobStates.Open,
                    };

                    simpleJobs.Add(simpleJobOverview);
                }

                MessageActionCollectionJobDetailsModel expectedItem = new MessageActionCollectionJobDetailsModel
                {
                    CollectionJob = overviewModel,
                    Id = collectionJobData.Id,
                    Name = collectionJobData.Name,
                };

                expectedResults.Add(collectionJobData.Id, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionCollectionJobDetailsModel> input in expectedResults)
            {
                MessageActionCollectionJobDetailsModel actual = await storage.GetMessageActionCollectionJobDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);

                base.CheckCollectionJobOverview(input.Value.CollectionJob, actual.CollectionJob);
            }
        }

        [Fact(DisplayName = "GetMessageActionCollectionJobDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionCollectionJobDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionCollectionJobDetailsModel actual = await storage.GetMessageActionCollectionJobDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
