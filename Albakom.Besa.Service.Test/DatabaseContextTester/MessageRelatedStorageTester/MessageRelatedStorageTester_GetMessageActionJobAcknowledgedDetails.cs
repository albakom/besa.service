﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionJobAcknowledgedDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionJobAcknowledgedDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobAcknowledgedDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionJobAcknowledgedDetailsModel> expectedResults = new Dictionary<Int32, MessageActionJobAcknowledgedDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel userDataModel = base.GenerateUserDataModel(random);
                storage.Users.Add(userDataModel);
                storage.SaveChanges();

                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                InjectJobDataModel jobDataModel = new InjectJobDataModel
                {
                    Building = buildingData,
                    CustomerContact = owner,
                };

                storage.InjectJobs.Add(jobDataModel);
                storage.SaveChanges();

                InjectJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<InjectJobFinishedDataModel>(
                    jobDataModel,
                    random,
                    userDataModel,
                    userDataModel
                    );

                storage.FinishedInjectJobs.Add(finishedDataModel);
                storage.SaveChanges();

                MessageActionJobAcknowledgedDetailsModel expectedItem = new MessageActionJobAcknowledgedDetailsModel
                {
                    Id = jobDataModel.Id,
                    Name = jobDataModel.Building.StreetName,
                    AcceptedAt = finishedDataModel.AcceptedAt.Value,
                    AcepptedBy = new UserOverviewModel
                    {
                        CompanyInfo = null,
                        EMailAddress = userDataModel.Email,
                        Id = userDataModel.ID,
                        IsMember = userDataModel.AuthServiceId != null,
                        Lastname = userDataModel.Lastname,
                        Phone = userDataModel.Phone,
                        Surname = userDataModel.Surname
                    },
                    JobOverview = new SimpleJobOverview
                    {
                        Id = jobDataModel.Id,
                        JobType = JobTypes.Inject,
                        Name = buildingData.StreetName,
                        State = JobStates.Acknowledged,
                    },
                };

                expectedResults.Add(jobDataModel.Id, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionJobAcknowledgedDetailsModel> input in expectedResults)
            {
                MessageActionJobAcknowledgedDetailsModel actual = await storage.GetMessageActionJobAcknowledgedDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);

                Assert.Equal(input.Value.AcceptedAt, actual.AcceptedAt);

                base.CheckUser(input.Value.AcepptedBy, actual.AcepptedBy);
                base.CheckSimpleJobOverview(input.Value.JobOverview, actual.JobOverview);
            }
        }

        [Fact(DisplayName = "GetMessageActionJobAcknowledgedDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobAcknowledgedDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionJobAcknowledgedDetailsModel actual = await storage.GetMessageActionJobAcknowledgedDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
