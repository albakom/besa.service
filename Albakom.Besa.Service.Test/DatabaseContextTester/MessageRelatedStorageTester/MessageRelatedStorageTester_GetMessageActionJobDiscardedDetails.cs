﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionJobDiscardedDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionJobDiscardedDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobDiscardedDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionJobDiscardDetailsModel> expectedResults = new Dictionary<Int32, MessageActionJobDiscardDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel userDataModel = base.GenerateUserDataModel(random);
                storage.Users.Add(userDataModel);
                storage.SaveChanges();

                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                InjectJobDataModel jobDataModel = new InjectJobDataModel
                {
                    Building = buildingData,
                    CustomerContact = owner,
                };

                storage.InjectJobs.Add(jobDataModel);
                storage.SaveChanges();

                InjectJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<InjectJobFinishedDataModel>(
                    jobDataModel,
                    random,
                    userDataModel,
                    userDataModel
                    );

                storage.FinishedInjectJobs.Add(finishedDataModel);
                storage.SaveChanges();

                MessageActionJobDiscardDetailsModel expectedItem = new MessageActionJobDiscardDetailsModel
                {
                    Id = jobDataModel.Id,
                    Name = jobDataModel.Building.StreetName,
                    JobOverview = new SimpleJobOverview
                    {
                        Id = jobDataModel.Id,
                        JobType = JobTypes.Inject,
                        Name = buildingData.StreetName,
                        State = JobStates.Acknowledged,
                    },
                };

                expectedResults.Add(jobDataModel.Id, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionJobDiscardDetailsModel> input in expectedResults)
            {
                MessageActionJobDiscardDetailsModel actual = await storage.GetMessageActionJobDiscardedDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);

                base.CheckSimpleJobOverview(input.Value.JobOverview, actual.JobOverview);
            }
        }

        [Fact(DisplayName = "GetMessageActionJobDiscardedDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobDiscardedDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionJobDiscardDetailsModel actual = await storage.GetMessageActionJobDiscardedDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
