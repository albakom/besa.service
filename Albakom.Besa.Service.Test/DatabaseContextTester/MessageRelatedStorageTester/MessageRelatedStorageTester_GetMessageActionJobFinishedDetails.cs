﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionJobFinishedDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionJobFinishedDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobFinishedDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionJobFinishedDetailsModel> expectedResults = new Dictionary<Int32, MessageActionJobFinishedDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel userDataModel = base.GenerateUserDataModel(random);
                storage.Users.Add(userDataModel);
                storage.SaveChanges();

                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                InjectJobDataModel jobDataModel = new InjectJobDataModel
                {
                    Building = buildingData,
                    CustomerContact = owner,
                };

                storage.InjectJobs.Add(jobDataModel);
                storage.SaveChanges();

                InjectJobFinishedDataModel finishedDataModel = base.GetFinishedJobDataModel<InjectJobFinishedDataModel>(
                    jobDataModel,
                    random,
                    null,
                    userDataModel
                    );

                storage.FinishedInjectJobs.Add(finishedDataModel);
                storage.SaveChanges();

                MessageActionJobFinishedDetailsModel expectedItem = new MessageActionJobFinishedDetailsModel
                {
                    Id = jobDataModel.Id,
                    Name = jobDataModel.Building.StreetName,
                    FinishedAt = finishedDataModel.FinishedAt,
                    FinishedBy = new UserOverviewModel
                    {
                        CompanyInfo = null,
                        EMailAddress = userDataModel.Email,
                        Id = userDataModel.ID,
                        IsMember = userDataModel.AuthServiceId != null,
                        Lastname = userDataModel.Lastname,
                        Phone = userDataModel.Phone,
                        Surname = userDataModel.Surname
                    },
                    ProblemHappend = new ExplainedBooleanModel
                    {
                        Value = finishedDataModel.ProblemHappend,
                        Description = finishedDataModel.ProblemHappendDescription
                    },
                    JobOverview = new SimpleJobOverview
                    {
                        Id = jobDataModel.Id,
                        JobType = JobTypes.Inject,
                        Name = buildingData.StreetName,
                        State = JobStates.Finished,
                    },
                };

                expectedResults.Add(jobDataModel.Id, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionJobFinishedDetailsModel> input in expectedResults)
            {
                MessageActionJobFinishedDetailsModel actual = await storage.GetMessageActionJobFinishedDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);

                Assert.Equal(input.Value.FinishedAt, actual.FinishedAt);

                Assert.Equal(input.Value.ProblemHappend.Value, actual.ProblemHappend.Value);
                Assert.Equal(input.Value.ProblemHappend.Description, actual.ProblemHappend.Description);

                base.CheckUser(input.Value.FinishedBy, actual.FinishedBy);
                base.CheckSimpleJobOverview(input.Value.JobOverview, actual.JobOverview);
            }
        }

        [Fact(DisplayName = "GetMessageActionJobFinishedDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionJobFinishedDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionJobFinishedDetailsModel actual = await storage.GetMessageActionJobFinishedDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
