﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionProcedureFinishedDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionProcedureFinishedDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionProcedureFinishedDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionDetailsModel> expectedResults = new Dictionary<Int32, MessageActionDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingData = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingData);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel procedureDetailModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                procedureDetailModel.Building = buildingData;
                procedureDetailModel.Owner = owner;

                storage.BuildingConnectionProcedures.Add(procedureDetailModel);
                storage.SaveChanges();

                MessageActionDetailsModel expectedItem = new MessageActionDetailsModel
                {
                    Id = procedureDetailModel.Id,
                    Name = procedureDetailModel.Name,
                };

                expectedResults.Add(procedureDetailModel.Id, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionDetailsModel> input in expectedResults)
            {
                MessageActionDetailsModel actual = await storage.GetMessageActionProcedureFinishedDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);
            }
        }

        [Fact(DisplayName = "GetMessageActionProcedureFinishedDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionProcedureFinishedDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionDetailsModel actual = await storage.GetMessageActionProcedureFinishedDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
