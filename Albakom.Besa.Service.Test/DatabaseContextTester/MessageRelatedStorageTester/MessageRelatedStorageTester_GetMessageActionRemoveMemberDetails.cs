﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessageActionRemoveMemberDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessageActionRemoveMemberDetails|MessageRelatedStorageTester")]
        public async Task GetMessageActionRemoveMemberDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, MessageActionRemoveMemberDetailsModel> expectedResults = new Dictionary<Int32, MessageActionRemoveMemberDetailsModel>();
            Int32 amount = random.Next(5, 20);

            for (int i = 0; i < amount; i++)
            {
                CompanyDataModel company = base.GenerateCompanyDataModel(random);
                storage.Companies.Add(company);
                storage.SaveChanges();

                UserDataModel user = base.GenerateUserDataModel(random);
                user.Company = company;
                storage.Users.Add(user);
                storage.SaveChanges();

                MessageActionRemoveMemberDetailsModel expectedItem = new MessageActionRemoveMemberDetailsModel
                {
                    Id = user.ID,
                    Member = new UserOverviewModel
                    {
                        Id = user.ID,
                        EMailAddress = user.Email,
                        IsMember = user.AuthServiceId != null,
                        Lastname = user.Lastname,
                        Surname = user.Surname,
                        Phone = user.Phone,
                        CompanyInfo = new SimpleCompanyOverviewModel
                        {
                            Id = user.CompanyId.Value,
                            Name = user.Company.Name,
                        }
                    },
                    Name = user.Surname + " " + user.Lastname,
                };

                expectedResults.Add(user.ID, expectedItem);
            }

            foreach (KeyValuePair<Int32, MessageActionRemoveMemberDetailsModel> input in expectedResults)
            {
                MessageActionRemoveMemberDetailsModel actual = await storage.GetMessageActionRemoveMemberDetails(input.Key);

                Assert.NotNull(actual);
                Assert.Equal(input.Value.Id, actual.Id);
                Assert.Equal(input.Value.Name, actual.Name);

                base.CheckUser(input.Value.Member, actual.Member);
            }
        }

        [Fact(DisplayName = "GetMessageActionRemoveMemberDetails_NotFound|MessageRelatedStorageTester")]
        public async Task GetMessageActionRemoveMemberDetails_NotFound()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                Int32 randomId = random.Next();

                MessageActionDetailsModel actual = await storage.GetMessageActionRemoveMemberDetails(randomId);
                Assert.Null(actual);
            }
        }
    }
}
