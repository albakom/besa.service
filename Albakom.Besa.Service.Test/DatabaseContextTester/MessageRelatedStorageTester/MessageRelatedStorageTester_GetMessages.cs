﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_GetMessages : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetMessages_Unread|MessageRelatedStorageTester")]
        public async Task GetMessages_Unread()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
            }

            List<MessageActions> actions = base.GeneratePossibleMessageActions();

            Dictionary<String, List<MessageRawModel>> expectedResults = new Dictionary<string, List<MessageRawModel>>();
            Int32 amount = random.Next(30, 60);

            for (int i = 0; i < amount; i++)
            {
                MessageDataModel dataModel = new MessageDataModel
                {
                    Action = actions[random.Next(0, actions.Count)],
                    Details = $"TestDetails {random.Next()}",
                    MarkedAsRead = random.NextDouble() > 0.5,
                    Title = $"Nachricht Nr {random.Next()}",
                    Receiver = users[random.Next(0, users.Count)],
                    RelatedObjectType = MessageRelatedObjectTypes.NotSpecified,
                    Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Sender = users[random.Next(0, users.Count)],
                };

                storage.Messages.Add(dataModel);
                storage.SaveChanges();

                if (expectedResults.ContainsKey(dataModel.Receiver.AuthServiceId) == false)
                {
                    expectedResults.Add(dataModel.Receiver.AuthServiceId, new List<MessageRawModel>());
                }

                expectedResults[dataModel.Receiver.AuthServiceId].Add(new MessageRawModel
                {
                    Action = dataModel.Action,
                    Details = dataModel.Details,
                    Id = dataModel.Id,
                    MarkedAsRead = dataModel.MarkedAsRead,
                    RelatedObjectType = dataModel.RelatedObjectType,
                    SourceUser = new UserOverviewModel
                    {
                        Id = dataModel.Sender.ID,
                        CompanyInfo = null,
                        EMailAddress = dataModel.Sender.Email,
                        IsMember = String.IsNullOrEmpty(dataModel.Sender.AuthServiceId) == false,
                        Lastname = dataModel.Sender.Lastname,
                        Phone = dataModel.Sender.Phone,
                        Surname = dataModel.Sender.Surname,
                    },
                    TimeStamp = dataModel.Timestamp,
                });
            }

            foreach (KeyValuePair<String, List<MessageRawModel>> input in expectedResults)
            {
                Boolean onlyUnreadedMessages = random.NextDouble() > 0.5;

                IEnumerable<MessageRawModel> expectedResult = input.Value.Where(x => x.MarkedAsRead == !onlyUnreadedMessages).OrderByDescending(x => x.TimeStamp);
                IEnumerable<MessageRawModel> actualResult = await storage.GetMessages(onlyUnreadedMessages, input.Key);

                Assert.NotNull(actualResult);
                Assert.Equal(expectedResult.Count(), actualResult.Count());

                Int32 index = 0;
                foreach (MessageRawModel expectedItem in expectedResult)
                {
                    MessageRawModel actualItem = actualResult.ElementAt(index);

                    base.CheckMessageRawModel(expectedItem, actualItem);

                    index += 1;
                }
            }
        }

        [Fact(DisplayName = "GetMessages_Filter|MessageRelatedStorageTester")]
        public async Task GetMessages_Filter()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 entryAmount = random.Next(100, 200);

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
            }

            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            MessageFilterModel timeFilterModel = new MessageFilterModel
            {
                StartTime = DateTimeOffset.Now.AddDays(-random.Next(5, 10)),

            };

            timeFilterModel.EndTime = timeFilterModel.StartTime.Value.AddDays(random.Next(5, 15));

            MessageFilterModel actionFilter = new MessageFilterModel();
            for (int i = 0; i < random.Next(3, 10); i++)
            {
                actionFilter.Actions.Add(possibleActions[random.Next(0, possibleActions.Count)]);
            }

            MessageFilterModel typeFilter = new MessageFilterModel();
            for (int i = 0; i < random.Next(3, 10); i++)
            {
                typeFilter.Types.Add(possibleObjects[random.Next(0, possibleObjects.Count)]);
            }

            MessageFilterModel queryFilter = new MessageFilterModel
            {
                Query = $"Suchevorkommen {random.Next()}",
            };

            Dictionary<MessageFilterModel, List<MessageRawModel>> expectedResult = new Dictionary<MessageFilterModel, List<MessageRawModel>>
            {
                { timeFilterModel,new List<MessageRawModel>()  },
                { actionFilter,new List<MessageRawModel>()  },
                { typeFilter,new List<MessageRawModel>()  },
                { queryFilter,new List<MessageRawModel>()  },
            };

            Dictionary<String, List<MessageRawModel>> messagesForUsers = new Dictionary<string, List<MessageRawModel>>();

            for (int i = 0; i < entryAmount; i++)
            {
                MessageDataModel model = new MessageDataModel
                {
                    Action = possibleActions[random.Next(0, possibleActions.Count)],
                    Details = $"TestDetails {random.Next()}",
                    MarkedAsRead = random.NextDouble() > 0.5,
                    Title = $"Nachricht Nr {random.Next()}",
                    Receiver = users[random.Next(0, users.Count)],
                    RelatedObjectType = possibleObjects[random.Next(0, possibleObjects.Count)],
                    Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Sender = users[random.Next(0, users.Count)],
                };

                Boolean addQueryFilter = random.NextDouble() > 0.7;
                if (addQueryFilter == true)
                {
                    if (random.NextDouble() > 0.5)
                    {
                        model.Title += queryFilter.Query;
                    }
                    else
                    {
                        model.Title = queryFilter.Query + model.Title;
                    }
                }

                storage.Messages.Add(model);
                storage.SaveChanges();

                MessageRawModel expectedItem = new MessageRawModel
                {
                    Action = model.Action,
                    Details = model.Details,
                    Id = model.Id,
                    MarkedAsRead = model.MarkedAsRead,
                    RelatedObjectType = model.RelatedObjectType,
                    SourceUser = new UserOverviewModel
                    {
                        Id = model.Sender.ID,
                        CompanyInfo = null,
                        EMailAddress = model.Sender.Email,
                        IsMember = String.IsNullOrEmpty(model.Sender.AuthServiceId) == false,
                        Lastname = model.Sender.Lastname,
                        Phone = model.Sender.Phone,
                        Surname = model.Sender.Surname,
                    },
                    TimeStamp = model.Timestamp,
                };

                if (messagesForUsers.ContainsKey(model.Receiver.AuthServiceId) == false)
                {
                    messagesForUsers.Add(model.Receiver.AuthServiceId, new List<MessageRawModel>());
                }

                messagesForUsers[model.Receiver.AuthServiceId].Add(expectedItem);

                if (model.Timestamp > timeFilterModel.StartTime && model.Timestamp < timeFilterModel.EndTime)
                {
                    expectedResult[timeFilterModel].Add(expectedItem);
                }

                if (actionFilter.Actions.Contains(model.Action) == true)
                {
                    expectedResult[actionFilter].Add(expectedItem);
                }

                if (typeFilter.Types.Contains(model.RelatedObjectType) == true)
                {
                    expectedResult[typeFilter].Add(expectedItem);
                }

                if (addQueryFilter == true)
                {
                    expectedResult[queryFilter].Add(expectedItem);
                }
            }

            foreach (UserDataModel user in users)
            {
                foreach (KeyValuePair<MessageFilterModel, List<MessageRawModel>> expected in expectedResult)
                {
                    String userAuthId = user.AuthServiceId;

                    List<MessageRawModel> messageForUsers = messagesForUsers[userAuthId];
                    if (messageForUsers.Count == 0) { continue; }

                    List<MessageRawModel> filteredMessagedAndOwnend = expected.Value.Intersect(messageForUsers).OrderByDescending(x => x.TimeStamp).ToList();

                    if (filteredMessagedAndOwnend.Count > 3)
                    {

                        expected.Key.Start = random.Next(1, filteredMessagedAndOwnend.Count / 2);
                        expected.Key.Amount = random.Next(1, (filteredMessagedAndOwnend.Count - expected.Key.Start) / 2);
                    }
                    else
                    {
                        expected.Key.Start = 0;
                        expected.Key.Amount = filteredMessagedAndOwnend.Count;
                    }

                    List<MessageRawModel> reducuedResult = filteredMessagedAndOwnend.Skip(expected.Key.Start).Take(expected.Key.Amount).ToList();

                    IEnumerable<MessageRawModel> actual = await storage.GetMessages(expected.Key, userAuthId);
                    Assert.NotNull(actual);
                    Assert.Equal(reducuedResult.Count, actual.Count());

                    Int32 index = 0;
                    foreach (MessageRawModel expectedItem in reducuedResult)
                    {
                        MessageRawModel actualItem = actual.ElementAt(index);

                        base.CheckMessageRawModel(expectedItem, actualItem);

                        index += 1;
                    }
                }
            }
        }
    }
}
