﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.MessageRelatedStorageTester
{
    public class MessageRelatedStorageTester_SetReadStateToMessage : DatabaseTesterBase
    {
        [Fact(DisplayName = "SetReadStateToMessage|MessageRelatedStorageTester")]
        public async Task SetReadStateToMessage()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();

            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
            }

            List<MessageActions> actions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> types = base.GeneratePossibleMessageRelatedObjectTypes();

            List<Int32> messageIds = new List<int>();

            Int32 amount = random.Next(30, 60);

            Dictionary<Int32, Boolean> readStates = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {
                MessageDataModel dataModel = new MessageDataModel
                {
                    Action = actions[random.Next(0, actions.Count)],
                    Details = $"TestDetails {random.Next()}",
                    MarkedAsRead = random.NextDouble() > 0.5,
                    Title = $"Nachricht Nr {random.Next()}",
                    Receiver = users[random.Next(0, users.Count)],
                    RelatedObjectType = MessageRelatedObjectTypes.NotSpecified,
                    Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Sender = users[random.Next(0, users.Count)],
                };

                storage.Messages.Add(dataModel);
                storage.SaveChanges();

                readStates.Add(dataModel.Id, dataModel.MarkedAsRead);
            }

            foreach (KeyValuePair<Int32, Boolean> input in readStates)
            {
                Boolean changeReadStates = random.NextDouble() > 0.3;
                if(changeReadStates == false)
                {
                    Boolean actualResult = await storage.SetReadStateToMessage(input.Value, input.Key);
                    Assert.False(actualResult);

                    Boolean readState = storage.Messages.Where(x => x.Id == input.Key).Select(x => x.MarkedAsRead).First();
                    Assert.Equal(input.Value, readState);
                }
                else
                {
                    Boolean actualResult = await storage.SetReadStateToMessage(!input.Value, input.Key);
                    Assert.True(actualResult);

                    Boolean readState = storage.Messages.Where(x => x.Id == input.Key).Select(x => x.MarkedAsRead).First();
                    Assert.Equal(!input.Value, readState);
                }
            }
        }
    }
}
