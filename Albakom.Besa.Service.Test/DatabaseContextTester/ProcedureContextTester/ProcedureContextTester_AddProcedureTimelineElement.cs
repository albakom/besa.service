﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_AddProcedureTimelineElement : DatabaseTesterBase
    {
        [Fact(DisplayName = "AddProcedureTimelineElement|ProcedureContextTester")]
        public async Task AddProcedureTimelineElement()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
            flatDataModel.Building = buildingDataModel;
            storage.Flats.Add(flatDataModel);
            storage.SaveChanges();

            ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customerContact);
            storage.SaveChanges();

            CustomerConnectionProcedureDataModel procedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
            procedureDataModel.Customer = customerContact;
            procedureDataModel.Flat = flatDataModel;

            storage.Procedures.Add(procedureDataModel);
            storage.SaveChanges();

            BranchOffJobDataModel jobDataModel = new BranchOffJobDataModel { Building = buildingDataModel };
            storage.Jobs.Add(jobDataModel);
            storage.SaveChanges();

            ProcedureTimelineCreateModel procedureTimelineCreateModel = new ProcedureTimelineCreateModel
            {
                Comment = $"TEstkommentar NR {random.Next()}",
                ProcedureId = procedureDataModel.Id,
                RelatedJobId = jobDataModel.Id,
                State = ProcedureStates.InjectJobBound
            };

            Int32 id = await storage.AddProcedureTimelineElementIfNessesary(procedureTimelineCreateModel);
            CheckTimelineElement(storage, jobDataModel, procedureTimelineCreateModel, id);

            Int32 identicalId = await storage.AddProcedureTimelineElementIfNessesary(procedureTimelineCreateModel);
            Assert.True(identicalId <= 0);

            ProcedureTimelineCreateModel procedureTimelineWithCustomDateCreateModel = new ProcedureTimelineCreateModel
            {
                Comment = $"TEstkommentar NR {random.Next()}",
                ProcedureId = procedureDataModel.Id,
                RelatedJobId = jobDataModel.Id,
                Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                State = ProcedureStates.InjectJobCreated,
            };

            Int32 idWithCustomtime = await storage.AddProcedureTimelineElementIfNessesary(procedureTimelineWithCustomDateCreateModel);
            CheckTimelineElement(storage, jobDataModel, procedureTimelineWithCustomDateCreateModel, idWithCustomtime);

        }

        private static void CheckTimelineElement(BesaDataStorage storage, BranchOffJobDataModel jobDataModel, ProcedureTimelineCreateModel procedureTimelineCreateModel, int id)
        {
            ProcedureTimeLineElementDataModel dataModel = storage.ProcedureTimeLineElements.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(procedureTimelineCreateModel.Comment, dataModel.Comment);
            Assert.Equal(procedureTimelineCreateModel.State, dataModel.State);
            if(procedureTimelineCreateModel.Timestamp.HasValue == false)
            {
                Assert.True(dataModel.TimeStamp >= DateTimeOffset.Now.AddHours(-1) && DateTimeOffset.Now.AddHours(1) >= dataModel.TimeStamp);
            }
            else
            {
                Assert.Equal(procedureTimelineCreateModel.Timestamp.Value, dataModel.TimeStamp);
            }
            Assert.Null(dataModel.RelatedRequestId);
            Assert.Equal(procedureTimelineCreateModel.ProcedureId, dataModel.ProcedureId);
            Assert.Equal(jobDataModel.Id, dataModel.RelatedJobId);
        }

    }
}
