﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CheckCheckIfProcedureIsFinished : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckCheckIfProcedureIsFinished|ProcedureContextTester")]
        public async Task CheckCheckIfProcedureIsFinished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();
            List<ProcedureStates> possibleStates = GetProcedureStates();

            Int32 amount = random.Next(10,20);
            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customerContact);
                storage.SaveChanges();

                CustomerConnectionProcedureDataModel procedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                procedureDataModel.IsFinished = false;
                procedureDataModel.Customer = customerContact;
                procedureDataModel.Flat = flatDataModel;
                procedureDataModel.IsFinished = random.NextDouble() > 0.5;

                storage.Procedures.Add(procedureDataModel);
                storage.SaveChanges();


                Int32 timelineElements = random.Next(3, 10);
                Boolean shouldFinished = random.NextDouble() > 0.5;
                Boolean endElementAdded = false;
                for (int j = 0; j < timelineElements; j++)
                {
                    ProcedureTimeLineElementDataModel dataModel = new ProcedureTimeLineElementDataModel
                    {
                        Procedure = procedureDataModel,
                        State = possibleStates[random.Next(0, possibleStates.Count)],
                        TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    };

                    if(dataModel.State == procedureDataModel.ExpectedEnd)
                    {
                        if(shouldFinished == false) { continue; }
                        else
                        {
                            if(endElementAdded == true) { continue; }

                            endElementAdded = true;
                            storage.ProcedureTimeLineElements.Add(dataModel);
                        }
                    }
                    else
                    {
                        storage.ProcedureTimeLineElements.Add(dataModel);
                    }
                }

                if(shouldFinished == true && endElementAdded == false)
                {
                    ProcedureTimeLineElementDataModel dataModel = new ProcedureTimeLineElementDataModel
                    {
                        Procedure = procedureDataModel,
                        State = procedureDataModel.ExpectedEnd,
                        TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    };

                    storage.ProcedureTimeLineElements.Add(dataModel);

                }

                storage.SaveChanges();

                expectedResults.Add(procedureDataModel.Id, shouldFinished);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedResult in expectedResults)
            {
                Boolean actual = await storage.CheckCheckIfProcedureIsFinished(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
