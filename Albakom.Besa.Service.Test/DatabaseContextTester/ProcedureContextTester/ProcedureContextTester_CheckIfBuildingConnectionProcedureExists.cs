﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CheckIfBuildingConnectionProcedureExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBuildingConnectionProcedureExists|ProcedureContextTester")]
        public async Task CheckIfBuildingConnectionProcedureExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            List<Int32> proceduresIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel buildingConnectionProcedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                buildingConnectionProcedureDataModel.Owner = owner;
                buildingConnectionProcedureDataModel.Building = buildingDataModel;

                storage.BuildingConnectionProcedures.Add(buildingConnectionProcedureDataModel);
                storage.SaveChanges();
            }

            foreach (Int32 item in proceduresIds)
            {
                Boolean result = await storage.CheckIfBuildingConnectionProcedureExists(item);
                Assert.True(result);
            }

            Int32 notExistingId = amount + 1;
            while (proceduresIds.Contains(notExistingId) == true)
            {
                notExistingId = random.Next();
            }

            Boolean falseResult = await storage.CheckIfBuildingConnectionProcedureExists(notExistingId);
            Assert.False(falseResult);
        }
    }
}
