﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetBuildingConnectionProcedureByBuildingId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetBuildingConnectionProcedureByBuildingId|ProcedureContextTester")]
        public async Task GetBuildingConnectionProcedureByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            ContactInfoDataModel owenerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owenerContact);
            storage.SaveChanges();

            Dictionary<Int32, Int32> expectedResults = new Dictionary<Int32, Int32>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel procedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                procedureDataModel.Building = buildingDataModel;
                procedureDataModel.Owner = owenerContact;

                storage.BuildingConnectionProcedures.Add(procedureDataModel);
                storage.SaveChanges();

                expectedResults.Add(buildingDataModel.Id, procedureDataModel.Id);
            }

            foreach (KeyValuePair<Int32, Int32> expectedResult in expectedResults)
            {
                Int32 actualResult = await storage.GetBuildingConnectionProcedureByBuildingId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actualResult);
            }
        }
    }
}
