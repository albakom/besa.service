﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CheckIfBuildingConnectionProcedureExistsByBuildingId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfBuildingConnectionProcedureExistsByBuildingId|ProcedureContextTester")]
        public async Task CheckIfBuildingConnectionProcedureExistsByBuildingId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            ContactInfoDataModel owenerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owenerContact);
            storage.SaveChanges();

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<Int32, Boolean>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Boolean createProcudere = random.NextDouble() > 0.5;
                if (createProcudere == true)
                {
                    BuildingConnectionProcedureDataModel procedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                    procedureDataModel.Building = buildingDataModel;
                    procedureDataModel.Owner  = owenerContact;

                    storage.BuildingConnectionProcedures.Add(procedureDataModel);
                    storage.SaveChanges();
                }

                expectedResults.Add(buildingDataModel.Id, createProcudere);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedResult in expectedResults)
            {
                Boolean actualResult = await storage.CheckIfBuildingConnectionProcedureExistsByBuildingId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actualResult);
            }
        }
    }
}
