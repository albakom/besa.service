﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CheckIfCustomerConnectionProcedureExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCustomerConnectionProcedureExists|ProcedureContextTester")]
        public async Task CheckIfCustomerConnectionProcedureExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            List<Int32> proceduresIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                CustomerConnectionProcedureDataModel customerConnectionProcedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                customerConnectionProcedureDataModel.Customer = owner;
                customerConnectionProcedureDataModel.Flat = flatDataModel;

                storage.CustomerConnectionProcedures.Add(customerConnectionProcedureDataModel);
                storage.SaveChanges();
            }

            foreach (Int32 item in proceduresIds)
            {
                Boolean result = await storage.CheckIfCustomerConnectionProcedureExists(item);
                Assert.True(result);
            }

            Int32 notExistingId = amount + 1;
            while (proceduresIds.Contains(notExistingId) == true)
            {
                notExistingId = random.Next();
            }

            Boolean falseResult = await storage.CheckIfCustomerConnectionProcedureExists(notExistingId);
            Assert.False(falseResult);
        }
    }
}
