﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CheckIfCustomerConnectionProcedureExistsByFlatId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfCustomerConnectionProcedureExistsByFlatId|ProcedureContextTester")]
        public async Task CheckIfCustomerConnectionProcedureExistsByFlatId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            ContactInfoDataModel owenerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owenerContact);
            storage.SaveChanges();

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<Int32, Boolean>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                Int32 flatAmount = random.Next(3, 10);

                for (int j = 0; j < flatAmount; j++)
                {
                    FlatDataModel flat = base.GenerateFlatDataModel(random);
                    flat.Building = buildingDataModel;
                    storage.Flats.Add(flat);
                    storage.SaveChanges();

                    Boolean createProcudere = random.NextDouble() > 0.5;
                    if (createProcudere == true)
                    {
                        CustomerConnectionProcedureDataModel procedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                        procedureDataModel.Flat = flat;
                        procedureDataModel.Customer = owenerContact;

                        storage.CustomerConnectionProcedures.Add(procedureDataModel);
                        storage.SaveChanges();
                    }

                    expectedResults.Add(flat.Id, createProcudere);
                }
            }

            foreach (KeyValuePair<Int32,Boolean> expectedResult in expectedResults)
            {
                Boolean actualResult = await storage.CheckIfCustomerConnectionProcedureExistsByFlatId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actualResult);
            }
        }
    }
}
