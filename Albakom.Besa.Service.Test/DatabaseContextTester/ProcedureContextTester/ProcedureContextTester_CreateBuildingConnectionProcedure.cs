﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CreateBuildingConnectionProcedure : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateBuildingConnectionProcedure|ProcedureContextTester")]
        public async Task CreateBuildingConnectionProcedure()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                CustomerConnenctionRequestDataModel request = base.GenrateCustomerConnenctionRequestDataModel(random, false, storage);
                request.PropertyOwner = owner;
                request.Building = buildingDataModel;
                storage.CustomerRequests.Add(request);
                storage.SaveChanges();

                BuildingConnectionProcedureCreateModel createModel = base.GenerateBuildingConnectionProcedureCreateModel(random, buildingDataModel.Id, owner.Id);
                createModel.FirstElement.RelatedRequestId = request.Id;

                Int32 id = await storage.CreateBuildingConnectionProcedure(createModel);

                BuildingConnectionProcedureDataModel dataModel = storage.BuildingConnectionProcedures.FirstOrDefault(x => x.Id == id);
                Assert.NotNull(dataModel);

                Assert.Equal(id, dataModel.Id);
                Assert.Equal(createModel.Name, dataModel.Name);
                Assert.Equal(createModel.Appointment, dataModel.Appointment);
                Assert.Equal(createModel.BuildingId, dataModel.BuildingId);
                Assert.Equal(createModel.DeclarationOfAggrementInStock, dataModel.DeclarationOfAggrementInStock);
                Assert.Equal(createModel.ExpectedEnd, dataModel.ExpectedEnd);
                Assert.Equal(createModel.OwnerContactId, dataModel.OwnerContactId);
                Assert.Equal(createModel.Start, dataModel.Start);
                Assert.Equal(createModel.InformSalesAfterFinish, dataModel.InformSalesAfterFinish);

                List<ProcedureTimeLineElementDataModel> procedureTimeLine = storage.ProcedureTimeLineElements.Where(x => x.ProcedureId == id).ToList();
                Assert.NotNull(procedureTimeLine);
                Assert.True(procedureTimeLine.Count == 1);

                ProcedureTimeLineElementDataModel firstTimelineElement = procedureTimeLine[0];
                Assert.Equal(createModel.FirstElement.Comment, firstTimelineElement.Comment);
                Assert.Equal(createModel.FirstElement.State, firstTimelineElement.State);
                Assert.True(firstTimelineElement.TimeStamp >= DateTimeOffset.Now.AddHours(-1) && DateTimeOffset.Now.AddHours(1) >= firstTimelineElement.TimeStamp);
                Assert.NotNull(firstTimelineElement.RelatedRequestId);
                Assert.Equal(request.Id, firstTimelineElement.RelatedRequestId);
            }
        }
    }
}
