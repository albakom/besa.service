﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_CreateCustomerConnectionProcedure : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateCustomerConnectionProcedure|ProcedureContextTester")]
        public async Task CreateCustomerConnectionProcedure()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
            flatDataModel.Building = buildingDataModel;
            storage.Flats.Add(flatDataModel);
            storage.SaveChanges();

            ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customerContact);
            storage.SaveChanges();

            CustomerConnenctionRequestDataModel request = base.GenrateCustomerConnenctionRequestDataModel(random, false, storage);
            request.Customer = customerContact;
            request.Flat = flatDataModel;
            storage.CustomerRequests.Add(request);
            storage.SaveChanges();

            CustomerConnectionProcedureCreateModel createModel = base.GenerateCustomerConnectionProcedureCreateModel(random, flatDataModel.Id, customerContact.Id);
            createModel.FirstElement.RelatedRequestId = request.Id;


            Int32 id = await storage.CreateCustomerConnectionProcedure(createModel);

            CustomerConnectionProcedureDataModel dataModel = storage.CustomerConnectionProcedures.FirstOrDefault(x => x.Id == id);
            Assert.NotNull(dataModel);

            Assert.Equal(id, dataModel.Id);
            Assert.Equal(createModel.Name, dataModel.Name);
            Assert.Equal(createModel.Appointment, dataModel.Appointment);
            Assert.Equal(createModel.FlatId, dataModel.FlatId);
            Assert.Equal(createModel.DeclarationOfAggrementInStock, dataModel.DeclarationOfAggrementInStock);
            Assert.Equal(createModel.ExpectedEnd, dataModel.ExpectedEnd);
            Assert.Equal(createModel.CustomerContactId, dataModel.CustomerContactId);
            Assert.Equal(createModel.Start, dataModel.Start);
            Assert.Equal(createModel.AsSoonAsPossible, dataModel.ActivationAsSoonAsPossible);

            List<ProcedureTimeLineElementDataModel> procedureTimeLine = storage.ProcedureTimeLineElements.Where(x => x.ProcedureId == id).ToList();
            Assert.NotNull(procedureTimeLine);
            Assert.True(procedureTimeLine.Count == 1);

            ProcedureTimeLineElementDataModel firstTimelineElement = procedureTimeLine[0];
            Assert.Equal(createModel.FirstElement.Comment, firstTimelineElement.Comment);
            Assert.Equal(createModel.FirstElement.State, firstTimelineElement.State);
            Assert.True( firstTimelineElement.TimeStamp >= DateTimeOffset.Now.AddHours(-1) && DateTimeOffset.Now.AddHours(1) >= firstTimelineElement.TimeStamp);
            Assert.NotNull(firstTimelineElement.RelatedRequestId);
            Assert.Equal(request.Id, firstTimelineElement.RelatedRequestId);
        }
    }
}
