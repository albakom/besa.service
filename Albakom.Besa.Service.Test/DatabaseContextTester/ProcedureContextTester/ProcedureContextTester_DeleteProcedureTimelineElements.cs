﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_DeleteProcedureTimelineElements : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteProcedureTimelineElements|ProcedureContextTester")]
        public async Task DeleteProcedureTimelineElements()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            ContactInfoDataModel owner = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owner);
            storage.SaveChanges();

            BuildingConnectionProcedureDataModel buildingConnectionProcedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
            buildingConnectionProcedureDataModel.Owner = owner;
            buildingConnectionProcedureDataModel.Building = buildingDataModel;

            storage.BuildingConnectionProcedures.Add(buildingConnectionProcedureDataModel);
            storage.SaveChanges();

            Int32 amount = random.Next(200, 400);
            List<Int32> timelineElements = new List<Int32>();

            for (int i = 0; i < amount; i++)
            {
                ProcedureTimeLineElementDataModel timeLineElementDataModel = new ProcedureTimeLineElementDataModel
                {
                    TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    Comment = "Irgendwas",
                    Procedure = buildingConnectionProcedureDataModel,
                    State = ProcedureStates.ActivatitionJobDiscarded,
                };

                storage.ProcedureTimeLineElements.Add(timeLineElementDataModel);
                storage.SaveChanges();
            }

            await storage.DeleteProcedureTimelineElements(timelineElements);

            foreach (Int32 item in timelineElements)
            {
                ProcedureTimeLineElementDataModel dataModel = storage.ProcedureTimeLineElements.FirstOrDefault(x => x.Id == item);
                Assert.Null(dataModel);
            }
        }
    }
}
