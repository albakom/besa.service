﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetConnectBuildingDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetConnectBuildingDetails|ProcedureContextTester")]
        public async Task GetConnectBuildingDetails()
        {
            List<ProcedureStates> possibleStates = GetProcedureStates();

            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12345);

            Int32 amount = random.Next(10, 20);
            List<Int32> proceduresIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {

                Dictionary<Int32, JobTypes> jobTypes = new Dictionary<int, JobTypes>();

                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                BuildingConnectionProcedureDataModel procedureData = base.GenerateBuildingConnectionProcedureDataModel(random);
                procedureData.Owner = owner;
                procedureData.Building = buildingDataModel;

                storage.BuildingConnectionProcedures.Add(procedureData);
                storage.SaveChanges();

                Int32 timelineAmount = random.Next(3, 10);
                ProcedureTimeLineElementDataModel latestTimelineElement = null;
                List<ProcedureTimeLineElementDataModel> timelineElements = new List<ProcedureTimeLineElementDataModel>();
                Boolean injectJobCreated = false, connectBuildingJobCreated = false;

                for (int j = 0; j < timelineAmount; j++)
                {
                    ProcedureTimeLineElementDataModel dataModel = new ProcedureTimeLineElementDataModel
                    {
                        Procedure = procedureData,
                        State = possibleStates[random.Next(0, possibleStates.Count)],
                        TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    };

                    if (random.NextDouble() > 0.5)
                    {
                        Double randomValue = random.NextDouble();
                        JobDataModel job = null;
                        JobTypes type = JobTypes.SpliceInPoP;
                        if (randomValue < 0.3)
                        {
                            if (connectBuildingJobCreated == false)
                            {
                                job = new ConnectBuildingJobDataModel { Owner = owner, Building = buildingDataModel };
                                type = JobTypes.HouseConnenction;
                                connectBuildingJobCreated = true;
                            }
                        }
                        else
                        {
                            if (injectJobCreated == false)
                            {
                                job = new InjectJobDataModel { CustomerContact = owner, Building = buildingDataModel };
                                type = JobTypes.Inject;
                                injectJobCreated = true;
                            }
                        }
                        if (job != null)
                        {
                            storage.Jobs.Add(job);
                            storage.SaveChanges();
                            jobTypes.Add(job.Id, type);
                            dataModel.RelatedJob = job;
                        }
                    }

                    if (latestTimelineElement == null || latestTimelineElement.TimeStamp < dataModel.TimeStamp)
                    {
                        latestTimelineElement = dataModel;
                    }

                    storage.ProcedureTimeLineElements.Add(dataModel);
                    storage.SaveChanges();

                    timelineElements.Add(dataModel);
                }

                BuildingConnectionProcedureDetailModel actualResult = await storage.GetConnectBuildingDetails(procedureData.Id);
                Assert.NotNull(actualResult);

                Assert.Equal(procedureData.Id, actualResult.Id);
                Assert.Equal(procedureData.Name, actualResult.Name);
                Assert.Equal(procedureData.Start, actualResult.Start);
                Assert.Equal(procedureData.ExpectedEnd, actualResult.ExpectedEnd);
                Assert.Equal(procedureData.IsFinished, actualResult.IsClosed);
                Assert.Equal(procedureData.Appointment, actualResult.Appointment);
                Assert.Equal(latestTimelineElement.State, actualResult.Current);
                Assert.Equal(procedureData.DeclarationOfAggrementInStock, actualResult.DeclarationOfAggrementInStock);
                Assert.Equal(procedureData.InformSalesAfterFinish, actualResult.InformSalesAfterFinish);

                base.CheckContact(procedureData.Owner, actualResult.Contact);

                Assert.NotNull(actualResult.Timeline);
                Assert.True(timelineAmount == actualResult.Timeline.Count());

                foreach (ProcedureTimelineElementDetailModel acutalItem in actualResult.Timeline)
                {
                    Assert.NotNull(acutalItem);

                    ProcedureTimeLineElementDataModel expectedItem = timelineElements.FirstOrDefault(x => x.Id == acutalItem.Id);
                    Assert.NotNull(expectedItem);

                    Assert.Equal(expectedItem.Id, acutalItem.Id);
                    Assert.Equal(expectedItem.RelatedJobId, acutalItem.RelatedJobId);
                    Assert.Equal(expectedItem.RelatedRequestId, acutalItem.RelatedRequestId);
                    Assert.Equal(expectedItem.State, acutalItem.State);
                    Assert.Equal(expectedItem.TimeStamp, acutalItem.Timestamp);
                    Assert.Equal(expectedItem.Comment, acutalItem.Comment);

                    if (expectedItem.RelatedJobId.HasValue == true)
                    {
                        Assert.NotNull(acutalItem.RelatedJobType);
                        Assert.True(jobTypes.ContainsKey(expectedItem.RelatedJobId.Value));
                        Assert.Equal(jobTypes[expectedItem.RelatedJobId.Value], acutalItem.RelatedJobType.Value);
                    }
                }
            }
        }
    }
}
