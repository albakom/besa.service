﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetCreateActivationJobInfoByCustomerConnectionProcedureId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetCreateActivationJobInfoByCustomerConnectionProcedureId|ProcedureContextTester")]
        public async Task GetCreateActivationJobInfoByCustomerConnectionProcedureId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            Dictionary<Int32, CustomerConnectionProcedureDataModel> procedures = new Dictionary<int, CustomerConnectionProcedureDataModel>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flat = base.GenerateFlatDataModel(random);
                flat.Building = buildingDataModel;
                storage.Flats.Add(flat);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                CustomerConnectionProcedureDataModel procedureData = base.GenerateCustomerConnectionProcedureDataModel(random);
                procedureData.Customer = owner;
                procedureData.Flat = flat;

                storage.CustomerConnectionProcedures.Add(procedureData);
                storage.SaveChanges();

                procedures.Add(procedureData.Id, procedureData);
            }

            foreach (KeyValuePair<Int32,CustomerConnectionProcedureDataModel> item in procedures)
            {
                ActivationJobCreateModel actual = await storage.GetCreateActivationJobInfoByCustomerConnectionProcedureId(item.Key);

                Assert.NotNull(actual);
                Assert.Equal(item.Value.CustomerContactId, actual.CustomerContactId);
                Assert.Equal(item.Value.FlatId, actual.FlatId);
            }
        }
    }
}
