﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetProcecureIdByJobId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProcecureIdByJobId|ProcedureContextTester")]
        public async Task GetProcecureIdByJobId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            ContactInfoDataModel owenerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(owenerContact);
            storage.SaveChanges();

            Dictionary<Int32, Int32?> expectedResults = new Dictionary<int, int?>();

            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                ConnectBuildingJobDataModel connectBuildingJobDataModel = new ConnectBuildingJobDataModel
                {
                    Building = buildingDataModel,
                    Owner = owenerContact,
                };

                storage.ConnectBuildingJobs.Add(connectBuildingJobDataModel);
                storage.SaveChanges();

                Int32? procedureId = null;
                if (random.NextDouble() > 0.5)
                {
                    BuildingConnectionProcedureDataModel procedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                    procedureDataModel.Building = buildingDataModel;
                    procedureDataModel.Owner = owenerContact;

                    ProcedureTimeLineElementDataModel procedureTimeLineElementDataModel = new ProcedureTimeLineElementDataModel
                    {
                        Procedure = procedureDataModel,
                        RelatedJob = connectBuildingJobDataModel,
                        State = ProcedureStates.ConnectBuildingJobCreated,
                        TimeStamp = DateTimeOffset.Now,
                    };

                    procedureDataModel.Timeline.Add(procedureTimeLineElementDataModel);

                    storage.Procedures.Add(procedureDataModel);
                    storage.SaveChanges();

                    procedureId = procedureDataModel.Id;
                }

                expectedResults.Add(connectBuildingJobDataModel.Id, procedureId);
            }

            foreach (KeyValuePair<Int32,Int32?> expectedResult in expectedResults)
            {
                Int32? actaulProcedureId = await storage.GetProcecureIdByJobId(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actaulProcedureId);
            }
        }
    }
}
