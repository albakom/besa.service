﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetProcecureIdByRequestId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProcecureIdByRequestId|ProcedureContextTester")]
        public async Task GetProcecureIdByRequestId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
            storage.Buildings.Add(buildingDataModel);
            storage.SaveChanges();

            FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
            flatDataModel.Building = buildingDataModel;
            storage.Flats.Add(flatDataModel);
            storage.SaveChanges();

            ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
            storage.ContactInfos.Add(customerContact);
            storage.SaveChanges();



            List<CustomerConnectionProcedureDataModel> procedures = new List<CustomerConnectionProcedureDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                CustomerConnectionProcedureDataModel procedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                procedureDataModel.Customer = customerContact;
                procedureDataModel.Flat = flatDataModel;

                storage.Procedures.Add(procedureDataModel);
                storage.SaveChanges();
                procedures.Add(procedureDataModel);
            }

            foreach (CustomerConnectionProcedureDataModel item in procedures)
            {
                CustomerConnenctionRequestDataModel request = base.GenrateCustomerConnenctionRequestDataModel(random, false, storage);
                request.Customer = customerContact;
                request.Flat = flatDataModel;
                storage.CustomerRequests.Add(request);
                storage.SaveChanges();

                ProcedureTimeLineElementDataModel timeLineElementDataModel = new ProcedureTimeLineElementDataModel
                {
                    Procedure = item,
                    RelatedRequest = request,
                    State = ProcedureStates.BranchOffJobFinished,
                    TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                };

                item.Timeline.Add(timeLineElementDataModel);

                storage.ProcedureTimeLineElements.Add(timeLineElementDataModel);
                storage.SaveChanges();

                Int32? procedureId = await storage.GetProcecureIdByRequestId(request.Id);
                Assert.NotNull(procedureId);

                Assert.Equal(item.Id, procedureId.Value);
            }

          
        }
    }
}
