﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetProcedureTypeById : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProcedureTypeById|ProcedureContextTester")]
        public async Task GetProcedureTypeById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10,20);
            Dictionary<Int32, ProcedureTypes> expectedResults = new Dictionary<Int32, ProcedureTypes>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customerContact);
                storage.SaveChanges();

                ProcedureTypes type = ProcedureTypes.CustomerConnection;
                if(random.NextDouble() > 0.5)
                {
                    type = ProcedureTypes.HouseConnection;
                }

                ProcedureDataModel procedureDataModel = null;
                if(type == ProcedureTypes.CustomerConnection)
                {
                    CustomerConnectionProcedureDataModel dataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                    dataModel.IsFinished = false;
                    dataModel.Customer = customerContact;
                    dataModel.Flat = flatDataModel;
                    dataModel.IsFinished = random.NextDouble() > 0.5;

                    procedureDataModel = dataModel;
                }
                else if(type == ProcedureTypes.HouseConnection)
                {

                    BuildingConnectionProcedureDataModel buildingConnectionProcedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                    buildingConnectionProcedureDataModel.Owner = customerContact;
                    buildingConnectionProcedureDataModel.Building = buildingDataModel;

                    procedureDataModel = buildingConnectionProcedureDataModel;
                }

                storage.Procedures.Add(procedureDataModel);
                storage.SaveChanges();
                expectedResults.Add(procedureDataModel.Id, type);
            }

            foreach (KeyValuePair<Int32, ProcedureTypes> expectedResult in expectedResults)
            {
                ProcedureTypes actual = await storage.GetProcedureTypeById(expectedResult.Key);
                Assert.Equal(expectedResult.Value, actual);
            }
        }
    }
}
