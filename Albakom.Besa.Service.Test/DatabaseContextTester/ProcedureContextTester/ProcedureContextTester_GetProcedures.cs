﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_GetProcedures : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProcedures|ProcedureContextTester")]
        public async Task GetProcedures()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            Dictionary<Int32, ProcedureOverviewModel> expectedResults =
                GenerateInputs(amount, storage, random);

            IEnumerable<ProcedureOverviewModel> actualResult = await storage.GetProcedures(new ProcedureFilterModel { Start = 0, Amount = amount }, ProcedureSortProperties.BuildingName, SortDirections.Ascending);
            Assert.NotNull(actualResult);
            Assert.True(expectedResults.Count == actualResult.Count());

            foreach (ProcedureOverviewModel item in actualResult)
            {
                CheckResult(item, expectedResults);
            }
        }

        private void CheckResult(ProcedureOverviewModel item, Dictionary<Int32, ProcedureOverviewModel> expectedResults)
        {
            Assert.True(expectedResults.ContainsKey(item.Id));

            ProcedureOverviewModel expected = expectedResults[item.Id];

            Assert.Equal(expected.Id, item.Id);
            Assert.Equal(expected.Current, item.Current);
            Assert.Equal(expected.Start, item.Start);
            Assert.Equal(expected.End, item.End);
            Assert.Equal(expected.Type, item.Type);
            Assert.Equal(expected.IsOpen, item.IsOpen);
            Assert.Equal(expected.Name, item.Name);

            Assert.NotNull(item.Contact);
            base.CheckContact(expected.Contact, item.Contact);

            Assert.NotNull(item.Building);
            Assert.Equal(expected.Building.Id, item.Building.Id);
            Assert.Equal(expected.Building.StreetName, item.Building.StreetName);
            Assert.Equal(expected.Building.HouseholdUnits, item.Building.HouseholdUnits);
            Assert.Equal(expected.Building.CommercialUnits, item.Building.CommercialUnits);

        }

        private Dictionary<Int32, ProcedureOverviewModel> GenerateInputs(Int32 amount, BesaDataStorage storage, Random random)
        {
            List<ProcedureStates> possibleStates = GetProcedureStates();

            Dictionary<Int32, ProcedureOverviewModel> expectedResults = new Dictionary<int, ProcedureOverviewModel>();

            for (int i = 0; i < amount; i++)
            {
                ProcedureOverviewModel expectedModel = new ProcedureOverviewModel();

                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                ContactInfoDataModel owner = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(owner);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                ProcedureDataModel procedureData = null;
                ProcedureTypes type = ProcedureTypes.CustomerConnection;
                if (random.NextDouble() > 0.5)
                {
                    BuildingConnectionProcedureDataModel buildingConnectionProcedureDataModel = base.GenerateBuildingConnectionProcedureDataModel(random);
                    buildingConnectionProcedureDataModel.Owner = owner;
                    buildingConnectionProcedureDataModel.Building = buildingDataModel;

                    procedureData = buildingConnectionProcedureDataModel;
                    type = ProcedureTypes.HouseConnection;
                }
                else
                {
                    CustomerConnectionProcedureDataModel customerConnectionProcedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                    customerConnectionProcedureDataModel.Customer = owner;
                    customerConnectionProcedureDataModel.Flat = flatDataModel;

                    procedureData = customerConnectionProcedureDataModel;
                    type = ProcedureTypes.CustomerConnection;
                }

                storage.Procedures.Add(procedureData);
                storage.SaveChanges();

                expectedModel.Id = procedureData.Id;
                expectedModel.Start = procedureData.Start;
                expectedModel.Name = procedureData.Name;
                expectedModel.IsOpen = !procedureData.IsFinished;
                expectedModel.End = procedureData.ExpectedEnd;
                expectedModel.Type = type;

                Int32 timelineAmount = random.Next(3, 10);
                ProcedureTimeLineElementDataModel latestTimelineElement = null;
                for (int j = 0; j < timelineAmount; j++)
                {
                    ProcedureTimeLineElementDataModel dataModel = new ProcedureTimeLineElementDataModel
                    {
                        Procedure = procedureData,
                        State = possibleStates[random.Next(0, possibleStates.Count)],
                        TimeStamp = DateTimeOffset.Now.AddDays(-random.Next(3, 10)),
                    };

                    if (latestTimelineElement == null || latestTimelineElement.TimeStamp < dataModel.TimeStamp)
                    {
                        latestTimelineElement = dataModel;
                    }

                    storage.ProcedureTimeLineElements.Add(dataModel);
                    storage.SaveChanges();
                }

                expectedModel.Current = latestTimelineElement.State;
                expectedModel.Contact = new PersonInfo
                {
                    Address = new AddressModel { City = owner.City, PostalCode = owner.PostalCode, Street = owner.Street, StreetNumber = owner.StreetNumber },
                    CompanyName = owner.CompanyName,
                    EmailAddress = owner.EmailAddress,
                    Id = owner.Id,
                    Lastname = owner.Lastname,
                    Phone = owner.Phone,
                    Surname = owner.Surname,
                    Type = owner.Type,
                };
                expectedModel.Building = new SimpleBuildingOverviewModel
                {
                    StreetName = buildingDataModel.StreetName,
                    CommercialUnits = buildingDataModel.CommercialUnits,
                    HouseholdUnits = buildingDataModel.HouseholdUnits,
                    Id = buildingDataModel.Id,
                };

                expectedResults.Add(procedureData.Id, expectedModel);
            }

            return expectedResults;
        }

        [Fact(DisplayName = "GetProcedures_WithFilterForActivationAsSoonAsPossible|ProcedureContextTester")]
        public async Task GetProcedures_WithFilterForActivationAsSoonAsPossible()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            Dictionary<Int32, ProcedureOverviewModel> expectedResults =
                GenerateInputs(amount, storage, random);

            Dictionary<Boolean, Dictionary<Int32, ProcedureOverviewModel>> realExpectedResult = new Dictionary<bool, Dictionary<int, ProcedureOverviewModel>>();
            realExpectedResult.Add(false, new Dictionary<int, ProcedureOverviewModel>());
            realExpectedResult.Add(true, new Dictionary<int, ProcedureOverviewModel>());

            foreach (KeyValuePair<Int32, ProcedureOverviewModel> item in expectedResults)
            {
                CustomerConnectionProcedureDataModel dataModel = storage.CustomerConnectionProcedures.FirstOrDefault(x => x.Id == item.Key);
                if (dataModel == null) { continue; }

                Boolean addAsSoonAsPossible = random.NextDouble() > 0.5;
                if (addAsSoonAsPossible == true)
                {
                    dataModel.ActivationAsSoonAsPossible = addAsSoonAsPossible;
                    dataModel.Appointment = null;
                }

                storage.SaveChanges();

                realExpectedResult[addAsSoonAsPossible].Add(item.Key, item.Value);
            }

            foreach (KeyValuePair<Boolean, Dictionary<Int32, ProcedureOverviewModel>> input in realExpectedResult)
            {
                IEnumerable<ProcedureOverviewModel> actualResult = await storage.GetProcedures(
                    new ProcedureFilterModel
                    {
                        Start = 0,
                        Amount = amount,
                        AsSoonAsPossibleState = input.Key,
                    }, ProcedureSortProperties.BuildingName, SortDirections.Ascending);
                Assert.NotNull(actualResult);
                Assert.True(input.Value.Count == actualResult.Count());

                foreach (ProcedureOverviewModel item in actualResult)
                {
                    CheckResult(item, input.Value);
                }
            }
        }

        [Fact(DisplayName = "GetProcedures_WithFilterForInformsAfterSail|ProcedureContextTester")]
        public async Task GetProcedures_WithFilterForInformsAfterSail()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);
            Dictionary<Int32, ProcedureOverviewModel> expectedResults =
                GenerateInputs(amount, storage, random);

            Dictionary<Boolean, Dictionary<Int32, ProcedureOverviewModel>> realExpectedResult = new Dictionary<bool, Dictionary<int, ProcedureOverviewModel>>();
            realExpectedResult.Add(false, new Dictionary<int, ProcedureOverviewModel>());
            realExpectedResult.Add(true, new Dictionary<int, ProcedureOverviewModel>());

            foreach (KeyValuePair<Int32, ProcedureOverviewModel> item in expectedResults)
            {
                BuildingConnectionProcedureDataModel dataModel = storage.BuildingConnectionProcedures.FirstOrDefault(x => x.Id == item.Key);
                if (dataModel == null) { continue; }

                Boolean addInformSalesAfterFinished = random.NextDouble() > 0.5;
                if (addInformSalesAfterFinished == true)
                {
                    dataModel.InformSalesAfterFinish = addInformSalesAfterFinished;
                }

                storage.SaveChanges();

                realExpectedResult[addInformSalesAfterFinished].Add(item.Key, item.Value);
            }

            foreach (KeyValuePair<Boolean, Dictionary<Int32, ProcedureOverviewModel>> input in realExpectedResult)
            {
                IEnumerable<ProcedureOverviewModel> actualResult = await storage.GetProcedures(
                    new ProcedureFilterModel
                    {
                        Start = 0,
                        Amount = amount,
                        InformSalesAfterFinishedState = input.Key,
                    }, ProcedureSortProperties.BuildingName, SortDirections.Ascending);
                Assert.NotNull(actualResult);
                Assert.True(input.Value.Count == actualResult.Count());

                foreach (ProcedureOverviewModel item in actualResult)
                {
                    CheckResult(item, input.Value);
                }
            }
        }
    }
}
