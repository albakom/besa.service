﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProcedureContextTester
{
    public class ProcedureContextTester_MarkProcedureAsFinished : DatabaseTesterBase
    {
        [Fact(DisplayName = "MarkProcedureAsFinished|ProcedureContextTester")]
        public async Task MarkProcedureAsFinished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10,20);
            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();
            for (int i = 0; i < amount; i++)
            {
                BuildingDataModel buildingDataModel = base.GenerateBuilding(random);
                storage.Buildings.Add(buildingDataModel);
                storage.SaveChanges();

                FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                flatDataModel.Building = buildingDataModel;
                storage.Flats.Add(flatDataModel);
                storage.SaveChanges();

                ContactInfoDataModel customerContact = base.GenerateContactInfo(random);
                storage.ContactInfos.Add(customerContact);
                storage.SaveChanges();

                CustomerConnectionProcedureDataModel procedureDataModel = base.GenerateCustomerConnectionProcedureDataModel(random);
                procedureDataModel.Customer = customerContact;
                procedureDataModel.Flat = flatDataModel;

                storage.Procedures.Add(procedureDataModel);
                storage.SaveChanges();

                Boolean markAsFinsiehd = random.NextDouble() > 0.5;
                if(markAsFinsiehd == true)
                {
                    await storage.MarkProcedureAsFinished(procedureDataModel.Id);
                }

                expectedResults.Add(procedureDataModel.Id, markAsFinsiehd);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedResult in expectedResults)
            {
                ProcedureDataModel procedureDataModelFromDb = storage.Procedures.FirstOrDefault(x => x.Id == expectedResult.Key);
                Assert.Equal(expectedResult.Value, procedureDataModelFromDb.IsFinished);
            }
        }
    }
}
