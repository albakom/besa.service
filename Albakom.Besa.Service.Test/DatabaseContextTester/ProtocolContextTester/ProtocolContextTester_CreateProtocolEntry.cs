﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_CreateProtocolEntry : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateProtocolEntry|ProtocolContextTester")]
        public async Task CreateProtocolEntry()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, ProtocolEntyCreateModel> inputs = new Dictionary<int, ProtocolEntyCreateModel>();
            Int32 entryAmount = random.Next(20, 40);

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            for (int i = 0; i < entryAmount; i++)
            {
                ProtocolEntyCreateModel model = new ProtocolEntyCreateModel();
                model.Action = MessageActions.JobBound;
                model.Timestamp = DateTimeOffset.Now.AddHours(-random.Next(3, 10));
                model.RelatedType = MessageRelatedObjectTypes.CollectionJob;

                model.TriggeredByUserId = random.NextDouble() > 0.5 ? user.ID : new Int32?();
                model.RelatedObjectId = random.NextDouble() > 0.5 ? random.Next(): new Int32?();

                Int32 id = await storage.CreateProtocolEntry(model);
                inputs.Add(id,model);
            }

            foreach (KeyValuePair<Int32,ProtocolEntyCreateModel> input in inputs)
            {
                ProtocolEntryDataModel dataModel = storage.ProtocolEntries.FirstOrDefault(x => x.Id == input.Key);

                Assert.NotNull(dataModel);
                Assert.Equal(input.Key, dataModel.Id);
                Assert.Equal(input.Value.Action, dataModel.Action);
                Assert.Equal(input.Value.RelatedType, dataModel.RelatedType);
                Assert.Equal(input.Value.Timestamp, dataModel.Timestamp);
                Assert.Equal(input.Value.TriggeredByUserId, dataModel.TriggeredByUserId);


                if (input.Value.RelatedObjectId.HasValue == true)
                {
                    Assert.Equal(input.Value.RelatedObjectId.Value, dataModel.RelatedObjectId);
                }
                else
                {
                    Assert.True(dataModel.RelatedObjectId < 0);
                }
            }
        }
    }
}
