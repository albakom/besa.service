﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_CreateProtocolNotificaiton : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateProtocolNotificaiton|ProtocolContextTester")]
        public async Task CreateProtocolNotificaiton()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();


            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            Int32 userAmount = random.Next(3, 10);
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                Int32 entryAmount = random.Next(6, 20);

                for (int j = 0; j < entryAmount; j++)
                {
                    ProtocolNotificationTypes type = base.GetNotifcationType(random);
                    ProtocolUserInformModel createModel = new ProtocolUserInformModel
                    {
                        NotifcationType = type,
                        Action = possibleActions[random.Next(0, possibleActions.Count)],
                        RelatedType = possibleObjects[random.Next(0, possibleObjects.Count)],
                    };

                    Boolean createResult = await storage.CreateProtocolNotificaiton(user.ID, createModel);

                    Assert.True(createResult);

                    ProtocolUserInformDataModel dbObject = storage.ProtocolInformEntries.FirstOrDefault(x =>
                    x.User.ID == user.ID && x.RelatedObject == createModel.RelatedType && x.Action == createModel.Action);

                    Assert.NotNull(dbObject);
                    Assert.Equal(createModel.Action, dbObject.Action);
                    Assert.Equal(createModel.NotifcationType, dbObject.NotifationTypes);
                    Assert.Equal(createModel.RelatedType, dbObject.RelatedObject);
                    Assert.Equal(user.ID, dbObject.UserId);
                }
            }
        }
    }
}
