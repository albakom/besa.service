﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_DeleteProtocolNotification : DatabaseTesterBase
    {
        [Fact(DisplayName = "DeleteProtocolNotification|ProtocolContextTester")]
        public async Task DeleteProtocolNotification()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, List<ProtocolUserInformModel>> expectedResult = new Dictionary<Int32, List<ProtocolUserInformModel>>();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
                expectedResult.Add(user.ID, new List<ProtocolUserInformModel>());
            }

            Int32 entryAmount = random.Next(60, 100);

            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            for (int i = 0; i < entryAmount; i++)
            {
                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = users[random.Next(0, users.Count)],
                    NotifationTypes = type,
                    Action = possibleActions[random.Next(0,possibleActions.Count)],
                    RelatedObject = possibleObjects[random.Next(0, possibleObjects.Count)],
                };

                storage.ProtocolInformEntries.Add(informDataModel);
                storage.SaveChanges();

                expectedResult[informDataModel.User.ID].Add(new ProtocolUserInformModel
                {
                    Action = informDataModel.Action,
                    NotifcationType = informDataModel.NotifationTypes,
                    RelatedType = informDataModel.RelatedObject,
                });
            }

            foreach (KeyValuePair<Int32,List<ProtocolUserInformModel>> expectedItem in expectedResult)
            {
                foreach (ProtocolUserInformModel item in expectedItem.Value)
                {
                    Boolean deleteResult = await storage.DeleteProtocolNotification(expectedItem.Key, item);
                    Assert.True(deleteResult);

                    ProtocolUserInformDataModel dbObject = storage.ProtocolInformEntries.FirstOrDefault(x =>
                       x.User.ID == expectedItem.Key && x.RelatedObject == item.RelatedType && x.Action == item.Action);

                    Assert.Null(dbObject);
                }
            }
        }
    }
}
