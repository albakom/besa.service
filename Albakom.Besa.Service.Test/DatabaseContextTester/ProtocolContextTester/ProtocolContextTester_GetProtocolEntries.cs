﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_GetProtocolEntries : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProtocolEntries|ProtocolContextTester")]
        public async Task GetProtocolEntries()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(12345);

            Dictionary<Int32, ProtocolEntyCreateModel> inputs = new Dictionary<int, ProtocolEntyCreateModel>();
            Int32 entryAmount = random.Next(100, 200);

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            ProtocolFilterModel timeFilterModel = new ProtocolFilterModel
            {
                StartTime = DateTimeOffset.Now.AddDays(-random.Next(5, 10)),

            };
            timeFilterModel.EndTime = timeFilterModel.StartTime.Value.AddDays(random.Next(5, 15));

            ProtocolFilterModel actionFilter = new ProtocolFilterModel();
            for (int i = 0; i < random.Next(3, 10); i++)
            {
                actionFilter.Actions.Add(possibleActions[random.Next(0, possibleActions.Count)]);
            }

            ProtocolFilterModel typeFilter = new ProtocolFilterModel();
            for (int i = 0; i < random.Next(3, 10); i++)
            {
                typeFilter.Types.Add(possibleObjects[random.Next(0, possibleObjects.Count)]);
            }

            Dictionary<ProtocolFilterModel, List<ProtocolEntryModel>> expectedResult = new Dictionary<ProtocolFilterModel, List<ProtocolEntryModel>>
            {
                { timeFilterModel,new List<ProtocolEntryModel>()  },
                { actionFilter,new List<ProtocolEntryModel>()  },
                { typeFilter,new List<ProtocolEntryModel>()  },
            };

            for (int i = 0; i < entryAmount; i++)
            {
                ProtocolEntryDataModel model = new ProtocolEntryDataModel();
                model.Action = possibleActions[random.Next(0, possibleActions.Count)];
                model.RelatedType = possibleObjects[random.Next(0, possibleObjects.Count)];
                model.Timestamp = DateTimeOffset.Now.AddDays(-random.Next(3, 20));
                model.TriggeredUser = random.NextDouble() > 0.5 ? user : null;
                model.RelatedObjectId = 0;

                storage.ProtocolEntries.Add(model);
                storage.SaveChanges();

                ProtocolEntryModel expectedItem = new ProtocolEntryModel
                {
                    Action = model.Action,
                    Id = model.Id,
                    RelatedObjectId = model.RelatedObjectId,
                    Timestamp = model.Timestamp,
                    SourcedBy = model.TriggeredUser != null ? new UserOverviewModel
                    {
                        Id = user.ID,
                        CompanyInfo = null,
                        EMailAddress = user.Email,
                        IsMember = String.IsNullOrEmpty(user.AuthServiceId) == false,
                        Lastname = user.Lastname,
                        Phone = user.Phone,
                        Surname = user.Surname,
                    } : null,
                    Type = model.RelatedType,
                };

                if (model.Timestamp > timeFilterModel.StartTime && model.Timestamp < timeFilterModel.EndTime)
                {
                    expectedResult[timeFilterModel].Add(expectedItem);
                }

                if (actionFilter.Actions.Contains(model.Action) == true)
                {
                    expectedResult[actionFilter].Add(expectedItem);
                }

                if (typeFilter.Types.Contains(model.RelatedType) == true)
                {
                    expectedResult[typeFilter].Add(expectedItem);
                }
            }

            foreach (KeyValuePair<ProtocolFilterModel, List<ProtocolEntryModel>> expected in expectedResult)
            {
                expected.Key.Start = random.Next(1, expected.Value.Count / 2);
                expected.Key.Amount = random.Next(1, (expected.Value.Count - expected.Key.Start) / 2);
                List<ProtocolEntryModel> reducuedResult = expected.Value.Skip(expected.Key.Start).Take(expected.Key.Amount).ToList();

                IEnumerable<ProtocolEntryModel> actual = await storage.GetProtocolEntries(expected.Key);
                Assert.NotNull(actual);
                Assert.Equal(reducuedResult.Count, actual.Count());

                Int32 index = 0;
                foreach (ProtocolEntryModel actualItem in actual)
                {
                    ProtocolEntryModel expectedItem = reducuedResult[index];
                    base.CheckProtocolEntryModel(expectedItem, actualItem);

                    index += 1;
                }
            }
        }
    }
}
