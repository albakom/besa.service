﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_GetProtocolNofitcation : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetProtocolNofitcation|ProtocolContextTester")]
        public async Task GetProtocolNofitcation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, ProtocolNotificationTypes> expectedResult = new Dictionary<int, ProtocolNotificationTypes>();
            Int32 entryAmount = random.Next(20, 40);

            MessageActions action = MessageActions.Delete;
            MessageRelatedObjectTypes objectType = MessageRelatedObjectTypes.Company;

            for (int i = 0; i < entryAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    expectedResult.Add(user.ID, ProtocolNotificationTypes.NoAction);
                    continue;
                }

                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = user,
                    NotifationTypes = type,
                    Action = action,
                    RelatedObject = objectType,
                };

                storage.ProtocolInformEntries.Add(informDataModel);
                storage.SaveChanges();

                expectedResult.Add(user.ID, type);
            }

            foreach (KeyValuePair<Int32,ProtocolNotificationTypes> expectedItem in expectedResult)
            {
                ProtocolNotificationTypes actualResult = await storage.GetProtocolNofitcation(objectType, action, expectedItem.Key);
                Assert.Equal(expectedItem.Value, actualResult);
            }
        }
    }
}
