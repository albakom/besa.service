﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_GetUserProtoclInformation : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUserProtoclInformation|ProtocolContextTester")]
        public async Task GetUserProtoclInformation()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<String, List<ProtocolUserInformModel>> expectedResult = new Dictionary<string, List<ProtocolUserInformModel>>();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
                expectedResult.Add(user.AuthServiceId, new List<ProtocolUserInformModel>());
            }

            Int32 entryAmount = random.Next(60, 100);

            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            for (int i = 0; i < entryAmount; i++)
            {
                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = users[random.Next(0, users.Count)],
                    NotifationTypes = type,
                    Action = possibleActions[random.Next(0,possibleActions.Count)],
                    RelatedObject = possibleObjects[random.Next(0, possibleObjects.Count)],
                };

                storage.ProtocolInformEntries.Add(informDataModel);
                storage.SaveChanges();

                expectedResult[informDataModel.User.AuthServiceId].Add(new ProtocolUserInformModel
                {
                    Action = informDataModel.Action,
                    NotifcationType = informDataModel.NotifationTypes,
                    RelatedType = informDataModel.RelatedObject,
                });
            }

            foreach (KeyValuePair<String,List<ProtocolUserInformModel>> expectedItem in expectedResult)
            {
                IEnumerable<ProtocolUserInformModel> sortedExpectedResult = expectedItem.Value.OrderBy(x => x.RelatedType).ThenBy(x => x.Action);

                IEnumerable<ProtocolUserInformModel> actualResult = await storage.GetUserProtocolInformation(expectedItem.Key);
                Assert.NotNull(actualResult);
                Assert.Equal(sortedExpectedResult.Count(), actualResult.Count());
                Int32 index = 0;
                foreach (ProtocolUserInformModel item in sortedExpectedResult)
                {
                    ProtocolUserInformModel acutal = actualResult.ElementAt(index);

                    base.CheckProtocolUserInformModel(item, acutal);

                    index += 1;
                }

            }
        }
    }
}
