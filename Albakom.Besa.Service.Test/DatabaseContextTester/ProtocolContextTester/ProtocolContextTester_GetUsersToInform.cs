﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_GetUsersToInform : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUsersToInform|ProtocolContextTester")]
        public async Task GetUsersToInform()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, ProtocolNotificationTypes> expectedResult = new Dictionary<int, ProtocolNotificationTypes>();
            Int32 entryAmount = random.Next(20, 40);

            MessageActions action = MessageActions.Delete;
            MessageRelatedObjectTypes objectType = MessageRelatedObjectTypes.Company;

            for (int i = 0; i < entryAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    continue;
                }


                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = user,
                    NotifationTypes = type,
                    Action = action,
                    RelatedObject = objectType,
                };

                storage.ProtocolInformEntries.Add(informDataModel);

                expectedResult.Add(user.ID, type);
            }

            IDictionary<Int32, ProtocolNotificationTypes> actualResult = await storage.GetUsersToInform(objectType, action);
            Assert.NotNull(actualResult);
            Assert.Equal(expectedResult.OrderBy(x => x.Key), actualResult.OrderBy(x => x.Key));
        }


        [Fact(DisplayName = "GetUsersToInform_WithIds|ProtocolContextTester")]
        public async Task GetUsersToInform_WithIds()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<Int32, ProtocolNotificationTypes> expectedResult = new Dictionary<int, ProtocolNotificationTypes>();
            Int32 entryAmount = random.Next(40, 100);

            MessageActions action = MessageActions.Delete;
            MessageRelatedObjectTypes objectType = MessageRelatedObjectTypes.Company;

            List<Int32> userIds = new List<int>();

            for (int i = 0; i < entryAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                if (random.NextDouble() > 0.5)
                {
                    continue;
                }

                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = user,
                    NotifationTypes = type,
                    Action = action,
                    RelatedObject = objectType,
                };

                storage.ProtocolInformEntries.Add(informDataModel);

                if (random.NextDouble() > 0.5)
                {
                    expectedResult.Add(user.ID, type);
                    userIds.Add(user.ID);
                }

            }

            IDictionary<Int32, ProtocolNotificationTypes> actualResult = await storage.GetUsersToInform(objectType, action, userIds);
            Assert.NotNull(actualResult);
            Assert.Equal(expectedResult.OrderBy(x => x.Key), actualResult.OrderBy(x => x.Key));
        }
    }
}
