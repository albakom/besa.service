﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.ProtocolContextTester
{
    public class ProtocolContextTester_UpdateProtocolNotification : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateProtocolNotification|ProtocolContextTester")]
        public async Task UpdateProtocolNotification()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<String, List<ProtocolUserInformModel>> expectedResult = new Dictionary<string, List<ProtocolUserInformModel>>();

            Int32 userAmount = random.Next(3, 10);
            List<UserDataModel> users = new List<UserDataModel>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                users.Add(user);
                expectedResult.Add(user.AuthServiceId, new List<ProtocolUserInformModel>());
            }

            Int32 entryAmount = random.Next(60, 100);

            List<MessageActions> possibleActions = base.GeneratePossibleMessageActions();
            List<MessageRelatedObjectTypes> possibleObjects = base.GeneratePossibleMessageRelatedObjectTypes();

            for (int i = 0; i < entryAmount; i++)
            {
                ProtocolNotificationTypes type = base.GetNotifcationType(random);
                ProtocolUserInformDataModel informDataModel = new ProtocolUserInformDataModel
                {
                    User = users[random.Next(0, users.Count)],
                    NotifationTypes = type,
                    Action = possibleActions[random.Next(0,possibleActions.Count)],
                    RelatedObject = possibleObjects[random.Next(0, possibleObjects.Count)],
                };

                storage.ProtocolInformEntries.Add(informDataModel);
                storage.SaveChanges();

                expectedResult[informDataModel.User.AuthServiceId].Add(new ProtocolUserInformModel
                {
                    Action = informDataModel.Action,
                    NotifcationType = informDataModel.NotifationTypes,
                    RelatedType = informDataModel.RelatedObject,
                });
            }

            foreach (KeyValuePair<String,List<ProtocolUserInformModel>> expectedItem in expectedResult)
            {
                foreach (ProtocolUserInformModel item in expectedItem.Value)
                {
                    ProtocolUserInformModel updateItem = new ProtocolUserInformModel
                    {
                        Action = item.Action,
                        NotifcationType = item.NotifcationType,
                        RelatedType = item.RelatedType,
                    };

                    Boolean changeEntry = random.NextDouble() > 0.5;
                    if(changeEntry == true)
                    {
                        ProtocolNotificationTypes type = base.GetNotifcationType(random);
                        while(type == updateItem.NotifcationType)
                        {
                            type = base.GetNotifcationType(random);
                        }

                        updateItem.NotifcationType = type;

                        Boolean changeResult = await storage.UpdateProtocolNotification(expectedItem.Key, updateItem);
                        Assert.True(changeResult);

                        ProtocolNotificationTypes dbType = storage.ProtocolInformEntries.Where(x =>
                        x.User.AuthServiceId == expectedItem.Key && x.RelatedObject == updateItem.RelatedType && x.Action == updateItem.Action).Select(x => x.NotifationTypes).First();

                        Assert.Equal(type, dbType);
                    }
                    else
                    {
                        Boolean changeResult = await storage.UpdateProtocolNotification(expectedItem.Key, updateItem);
                        Assert.False(changeResult);
                    }
                }
            }
        }
    }
}
