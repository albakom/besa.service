﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CancelUpdateTask : DatabaseTesterBase
    {
        [Fact(DisplayName = "CancelUpdateTask|UpdateTaskContextTester")]
        public async Task CancelUpdateTask()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);

            Dictionary<Int32, UpdateTaskElementStates> expectedStates = new Dictionary<int, UpdateTaskElementStates>();
            List<Int32> updateTasksIds = new List<int>();
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                updateTasksIds.Add(dataModel.Id);

                Int32 elementAmount = random.Next(10, 30);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    expectedStates.Add(elementDataModel.Id, elementDataModel.State);
                }
            }

            Int32 cancelAmount = random.Next(1, updateTasksIds.Count);

            for (int i = 0; i < cancelAmount; i++)
            {
                Int32 idToCancel = updateTasksIds[random.Next(0, updateTasksIds.Count)];

                Boolean result = await storage.CancelUpdateTask(idToCancel);
                Assert.True(result);

                UpdateTaskDataModel dataModel = storage.UpdateTasks.FirstOrDefault(X => X.Id == idToCancel);
                Assert.NotNull(dataModel);

                Assert.True(dataModel.Canceled);
                Assert.NotNull(dataModel.Ended);
                Assert.True((DateTimeOffset.Now - dataModel.Ended.Value).TotalMinutes < 2);

                IEnumerable<UpdateTaskElementDataModel> elements = storage.UpdateTaskElements.Where(X => X.TaskId == idToCancel).ToList();

                foreach (UpdateTaskElementDataModel item in elements)
                {
                    UpdateTaskElementStates expectedState = expectedStates[item.Id];
                    if(expectedState == UpdateTaskElementStates.NotStarted)
                    {
                        expectedState = UpdateTaskElementStates.Canceled;
                    }

                    Assert.Equal(expectedState, item.State);
                }


                updateTasksIds.Remove(idToCancel);
            }
        }
    }
}
