﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfOpenUpdateTasksExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfOpenUpdateTasksExists|UpdateTaskContextTester")]
        public async Task CheckIfOpenUpdateTasksExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<UpdateTasks, bool> expectedResult = new Dictionary<UpdateTasks, bool>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);

                if (expectedResult.ContainsKey(dataModel.Task) == false)
                {
                    expectedResult.Add(dataModel.Task, false);
                }

                Boolean value = expectedResult[dataModel.Task];
                if (value == false && random.NextDouble() > 0.5)
                {
                    dataModel.Ended = null;
                    expectedResult[dataModel.Task] = true;
                }

                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();
            }

            foreach (KeyValuePair<UpdateTasks, Boolean> item in expectedResult)
            {
                Boolean actualResult = await storage.CheckIfOpenUpdateTasksExists(item.Key);
                Assert.Equal(item.Value, actualResult);
            }
        }
    }
}
