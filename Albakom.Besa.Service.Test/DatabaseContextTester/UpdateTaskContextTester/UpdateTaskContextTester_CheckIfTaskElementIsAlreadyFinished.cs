﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfTaskElementIsAlreadyFinished : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfTaskElementIsAlreadyFinished|UpdateTaskContextTester")]
        public async Task CheckIfTaskElementIsAlreadyFinished()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            Dictionary<Int32, Boolean> exectedResults = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    Boolean result = elementDataModel.Ended.HasValue == true;
                    exectedResults.Add(elementDataModel.Id, result);
                }
            }

            foreach (KeyValuePair<Int32,Boolean> item in exectedResults)
            {
                Boolean actual = await storage.CheckIfTaskElementIsAlreadyFinished(item.Key);
                Assert.Equal(item.Value, actual);
            }
           
        }
    }
}
