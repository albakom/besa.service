﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfTaskHasOpenElements : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfTaskHasOpenElements|UpdateTaskContextTester")]
        public async Task CheckIfTaskHasOpenElements()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<UpdateTasks, Boolean> expectedResult = new Dictionary<UpdateTasks, Boolean>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if (expectedResult.ContainsKey(dataModel.Task) == true) { continue; }

                Boolean hasOpenTask = random.NextDouble() > 0.5;
                expectedResult.Add(dataModel.Task, hasOpenTask);

                if (hasOpenTask == true)
                {
                    dataModel.Ended = null;
                    storage.SaveChanges();
                }

                Int32 elementAmount = random.Next(10, 50);

                List<UpdateTaskElementModel> openElements = new List<UpdateTaskElementModel>();

                Boolean notStartedElementGenerated = false;
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    if (hasOpenTask == false && elementDataModel.State == UpdateTaskElementStates.NotStarted)
                    {
                        continue;
                    }

                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();
                    if (elementDataModel.State == UpdateTaskElementStates.NotStarted)
                    {
                        notStartedElementGenerated = true;
                    }
                }

                if (notStartedElementGenerated == false && hasOpenTask == true)
                {
                    UpdateTaskElementDataModel elementDataModel = new UpdateTaskElementDataModel
                    {
                        Task = dataModel,
                        State = UpdateTaskElementStates.NotStarted,
                        Ended = null,
                        Started = null,
                        Result = String.Empty,
                        AdapterId = Guid.NewGuid().ToString(),
                        ObjectId = random.Next(),
                    };

                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();
                }
            }

            foreach (KeyValuePair<UpdateTasks, Boolean> item in expectedResult)
            {
                Boolean actualResult = await storage.CheckIfTaskHasOpenElements(item.Key);
                Assert.Equal(item.Value, actualResult);
            }
        }
    }
}
