﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfTaskHeartBeatIsOlderThen : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfTaskHeartBeatIsOlderThen|UpdateTaskContextTester")]
        public async Task CheckIfTaskHeartBeatIsOlderThen()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<UpdateTasks, Boolean> expectedResult = new Dictionary<UpdateTasks, bool>();

            Int32 amount = random.Next(10, 40);

            DateTimeOffset treasure = DateTimeOffset.Now.AddMinutes(-random.Next(3, 10));

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if (expectedResult.ContainsKey(dataModel.Task) == true)
                {
                    dataModel.Ended = DateTimeOffset.Now;
                    storage.SaveChanges();
                }
                else
                {
                    Boolean isOlderThen = random.NextDouble() > 0.5;
                    if (isOlderThen == true)
                    {
                        dataModel.Ended = null;
                        dataModel.Heartbeat = treasure.AddMinutes(-random.Next(0, 10));
                        storage.SaveChanges();
                    }

                    expectedResult[dataModel.Task] = isOlderThen;
                }
            }

            foreach (KeyValuePair<UpdateTasks, Boolean> item in expectedResult)
            {
                Boolean actual = await storage.CheckIfTaskHeartBeatIsOlderThen(treasure, item.Key);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
