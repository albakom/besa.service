﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfTaskIsCanceledByElementId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfTaskIsCanceledByElementId|UpdateTaskContextTester")]
        public async Task CheckIfTaskIsCanceledByElementId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<Int32, Boolean> expectedResult = new Dictionary<int, bool>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                Boolean cancelTask = random.NextDouble() > 0.5;
                if (cancelTask)
                {
                    dataModel.Canceled = true;
                    dataModel.Ended = DateTimeOffset.Now;
                }

                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    expectedResult.Add(elementDataModel.Id, cancelTask);
                }
            }

            foreach (KeyValuePair<Int32,Boolean> item in expectedResult)
            {
                Boolean actual = await storage.CheckIfTaskIsCanceledByElementId(item.Key);
                Assert.Equal(item.Value, actual);
            }
        }
    }
}
