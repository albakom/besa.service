﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfTaskIsLocked : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfTaskIsLocked|UpdateTaskContextTester")]
        public async Task CheckIfTaskIsLocked()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<UpdateTasks, Boolean> expectedResult = new Dictionary<UpdateTasks, Boolean>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if (expectedResult.ContainsKey(dataModel.Task) == true) { continue; }

                Boolean locked = random.NextDouble() > 0.5;

                expectedResult.Add(dataModel.Task, locked);
                dataModel.Ended = null;
                dataModel.IsLocked = locked;
                storage.SaveChanges();
            }

            foreach (KeyValuePair<UpdateTasks, Boolean> item in expectedResult)
            {
                Boolean actualResult = await storage.CheckIfTaskIsLocked(item.Key);
                Assert.Equal(item.Value, actualResult);
            }
        }
    }
}
