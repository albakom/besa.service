﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfUpdateTaskElementExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfUpdateTaskElementExists|UpdateTaskContextTester")]
        public async Task CheckIfUpdateTaskElementExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            HashSet<Int32> exitingIds = new HashSet<int>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    exitingIds.Add(elementDataModel.Id);
                }
            }

            HashSet<Int32> notExistingIds = new HashSet<int>();
            Int32 notExistingAmount = random.Next(10, 50);
            while(notExistingAmount != notExistingIds.Count)
            {
                Int32 id = random.Next();
                if(exitingIds.Contains(id) == true) { continue; }

                notExistingIds.Add(id);
            }

            foreach (Int32 id in exitingIds)
            {
                Boolean result = await storage.CheckIfUpdateTaskElementExists(id);
                Assert.True(result);
            }

            foreach (Int32 id in notExistingIds)
            {
                Boolean result = await storage.CheckIfUpdateTaskElementExists(id);
                Assert.False(result);
            }
        }
    }
}
