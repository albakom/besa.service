﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfUpdateTaskIsCanceledByTaskId : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfUpdateTaskIsCanceledByTaskId|UpdateTaskContextTester")]
        public async Task CheckIfUpdateTaskIsCanceledByTaskId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(40, 100);

            Dictionary<Int32, Boolean> expectedResults = new Dictionary<int, bool>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                dataModel.Canceled = random.NextDouble() > 0.5;
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                expectedResults.Add(dataModel.Id, dataModel.Canceled);
            }

            foreach (KeyValuePair<Int32,Boolean> expectedItem in expectedResults)
            {
                Boolean actual = await storage.CheckIfUpdateTaskIsCanceledByTaskId(expectedItem.Key);
                Assert.Equal(expectedItem.Value, actual);
            }
        }
    }
}
