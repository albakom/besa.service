﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CheckIfUpdateTasksExists : DatabaseTesterBase
    {
        [Fact(DisplayName = "CheckIfUpdateTasksExists|UpdateTaskContextTester")]
        public async Task CheckIfUpdateTasksExists()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(40, 100);

            HashSet<Int32> exitingIds = new HashSet<int>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                exitingIds.Add(dataModel.Id);
            }

            HashSet<Int32> notExistingIds = new HashSet<int>();
            Int32 notExistingAmount = random.Next(10, 50);
            while(notExistingAmount != notExistingIds.Count)
            {
                Int32 id = random.Next();
                if(exitingIds.Contains(id) == true) { continue; }

                notExistingIds.Add(id);
            }

            foreach (Int32 id in exitingIds)
            {
                Boolean result = await storage.CheckIfUpdateTasksExists(id);
                Assert.True(result);
            }

            foreach (Int32 id in notExistingIds)
            {
                Boolean result = await storage.CheckIfUpdateTasksExists(id);
                Assert.False(result);
            }
        }
    }
}
