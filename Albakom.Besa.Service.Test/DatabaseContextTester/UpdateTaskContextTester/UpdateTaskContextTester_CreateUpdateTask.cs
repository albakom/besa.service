﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_CreateUpdateTask : DatabaseTesterBase
    {
        [Fact(DisplayName = "CreateUpdateTask|UpdateTaskContextTester")]
        public async Task CreateUpdateTask()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Int32 userAmount = random.Next(10, 30);
            List<String> authIds = new List<string>(userAmount);
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.AuthServiceId = Guid.NewGuid().ToString();
                storage.Users.Add(user);
                storage.SaveChanges();

                authIds.Add(user.AuthServiceId);
            }

            for (int i = 0; i < amount; i++)
            {
                UpdateTaskCreateModel createModel = new UpdateTaskCreateModel
                {
                    Task = (UpdateTasks)random.Next(1, 9),
                    Timestamp = DateTimeOffset.Now,
                    UserAuthId = authIds[random.Next(0, authIds.Count)],
                };

                Int32 elementAmount = random.Next(5, 10);
                List<UpdateTaskElementModel> elementModels = new List<UpdateTaskElementModel>(elementAmount);
                for (int j = 0; j < elementAmount; j++)
                {
                    Int32 objectId = random.Next();
                    String projectAdapterId = Guid.NewGuid().ToString();

                    createModel.Elements.Add(projectAdapterId, objectId);
                    elementModels.Add(new UpdateTaskElementModel { AdapterId = projectAdapterId, ObjectId = objectId });
                }

                Int32 result = await storage.CreateUpdateTask(createModel);

                UpdateTaskDataModel dataModel = storage.UpdateTasks.FirstOrDefault(x => x.Id == result);
                Assert.NotNull(dataModel);

                Assert.Equal(dataModel.Started, createModel.Timestamp);
                Assert.Equal(dataModel.Task, createModel.Task);

                UserDataModel userDataModel = storage.Users.FirstOrDefault(x => x.ID == dataModel.StartedByUserId);
                Assert.NotNull(userDataModel);

                Assert.Equal(createModel.UserAuthId, userDataModel.AuthServiceId);

                List<UpdateTaskElementDataModel> elements = storage.UpdateTaskElements.Where(x => x.TaskId == result).ToList();
                Assert.NotNull(elements);

                Assert.Equal(elementModels.Count, elements.Count);

                foreach (UpdateTaskElementDataModel element in elements)
                {
                    UpdateTaskElementModel expectedItem = elementModels.FirstOrDefault(x => x.ObjectId == element.ObjectId && x.AdapterId == element.AdapterId);
                    Assert.NotNull(expectedItem);

                    expectedItem.Id = element.Id;
                }

                Assert.Equal(0, elementModels.Count(x => x.Id <= 0));
            }
        }
    }
}
