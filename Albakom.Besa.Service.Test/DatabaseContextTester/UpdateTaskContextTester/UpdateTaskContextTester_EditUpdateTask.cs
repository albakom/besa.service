﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_EditUpdateTask : DatabaseTesterBase
    {
        [Fact(DisplayName = "EditUpdateTask|UpdateTaskContextTester")]
        public async Task EditUpdateTask()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if(random.NextDouble() > 0.5)
                {
                    UpdateTaskFinishModel updateModel = new UpdateTaskFinishModel
                    {
                        Cancel = random.NextDouble() > 0.5,
                        TaskId = dataModel.Id,
                        Timestamp = DateTimeOffset.Now,
                    };

                    Boolean result = await storage.EditUpdateTask(updateModel);
                    Assert.True(result);

                    UpdateTaskDataModel changedDataModel = storage.UpdateTasks.FirstOrDefault(x => x.Id == updateModel.TaskId);
                    Assert.NotNull(changedDataModel);

                    Assert.Equal(updateModel.Timestamp, changedDataModel.Ended);
                    Assert.Equal(updateModel.Cancel, changedDataModel.Canceled);
                    Assert.Equal(updateModel.TaskId, changedDataModel.Id);
                }
            }
        }
    }
}
