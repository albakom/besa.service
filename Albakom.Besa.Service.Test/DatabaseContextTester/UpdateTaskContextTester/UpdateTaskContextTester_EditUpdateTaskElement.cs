﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_EditUpdateTaskElement : DatabaseTesterBase
    {
        [Fact(DisplayName = "EditUpdateTaskElement|UpdateTaskContextTester")]
        public async Task EditUpdateTaskElement()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            List<Int32> possibleIds = new List<int>();

            Dictionary<Int32, Int32> elementToTaskMapper = new Dictionary<int, int> ();
            Dictionary<Int32, DateTimeOffset> expectedHeartbeat = new Dictionary<int, DateTimeOffset>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                expectedHeartbeat.Add(dataModel.Id, DateTimeOffset.Now);

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    if(elementDataModel.State == UpdateTaskElementStates.InProgress)
                    {
                        possibleIds.Add(elementDataModel.Id);
                    }

                    elementToTaskMapper.Add(elementDataModel.Id, dataModel.Id);
                }
            }

            foreach (Int32 id in possibleIds)
            {
                UpdateTaskElementUpdateModel updateModel = new UpdateTaskElementUpdateModel
                {
                    ElementId = id,
                    Timestamp = DateTimeOffset.Now,
                    Result = $"Resultat Nr {random.Next()}",
                };

                Int32 taskId = elementToTaskMapper[id];
                expectedHeartbeat[taskId] = updateModel.Timestamp;

                Int32 randomValue = random.Next(0, 3);
                if(randomValue == 0)
                {
                    updateModel.State = UpdateTaskElementStates.Success;
                }
                else if(randomValue == 1)
                {
                    updateModel.State = UpdateTaskElementStates.Waring;
                }
                else
                {
                    updateModel.State = UpdateTaskElementStates.Error;
                }

                Boolean result = await storage.EditUpdateTaskElement(updateModel);
                Assert.True(result);

                UpdateTaskElementDataModel elementDataModel = storage.UpdateTaskElements.FirstOrDefault(x => x.Id == id);
                Assert.NotNull(elementDataModel);

                UpdateTaskDataModel updateTaskDataModel = storage.UpdateTasks.First(X => X.Id == elementDataModel.TaskId);

                Assert.Equal(updateModel.State, elementDataModel.State);
                Assert.Equal(updateModel.Result, elementDataModel.Result);
                Assert.Equal(updateModel.Timestamp, elementDataModel.Ended);

                Assert.Equal(expectedHeartbeat[elementDataModel.TaskId], updateTaskDataModel.Heartbeat);
            }
        }
    }
}
