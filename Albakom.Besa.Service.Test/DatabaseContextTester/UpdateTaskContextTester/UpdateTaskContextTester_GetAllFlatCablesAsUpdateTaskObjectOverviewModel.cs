﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetAllFlatCablesAsUpdateTaskObjectOverviewModel : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetAllFlatCablesAsUpdateTaskObjectOverviewModel|UpdateTaskContextTester")]
        public async Task GetAllFlatCablesAsUpdateTaskObjectOverviewModel()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(40, 50);

            Dictionary<Int32, UpdateTaskObjectOverviewModel> expected = new Dictionary<int, UpdateTaskObjectOverviewModel>();

            FiberCableTypeDataModel typeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);

           
            for (int i = 0; i < amount; i++)
            {
                FiberCableDataModel  dataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                storage.FiberCables.Add(dataModel);
                storage.SaveChanges();

                Double randomValue = random.NextDouble();
                if(randomValue < 0.3)
                {
                    BuildingDataModel buildingData = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingData);

                    storage.SaveChanges();

                    dataModel.ForBuilding = buildingData;
                    storage.SaveChanges();

                    UpdateTaskObjectOverviewModel expectedItem = new UpdateTaskObjectOverviewModel
                    {
                        Id = dataModel.Id,
                        Name = buildingData.StreetName,
                        ProjectAdapterId = dataModel.ProjectAdapterId,
                        Type = MessageRelatedObjectTypes.FiberCable,
                    };

                    expected.Add(dataModel.Id, expectedItem);

                }
                else if(randomValue > 0.7)
                {
                    BuildingDataModel buildingData = base.GenerateBuilding(random);
                    storage.Buildings.Add(buildingData);
                    storage.SaveChanges();

                    FlatDataModel flatDataModel = base.GenerateFlatDataModel(random);
                    flatDataModel.Building = buildingData;
                    storage.Flats.Add(flatDataModel);
                    storage.SaveChanges();

                    dataModel.StartingFlat = flatDataModel;
                    storage.SaveChanges();

                    UpdateTaskObjectOverviewModel expectedItem = new UpdateTaskObjectOverviewModel
                    {
                        Id = dataModel.Id,
                        Name = $"{buildingData.StreetName} - {flatDataModel.Number}",
                        ProjectAdapterId = dataModel.ProjectAdapterId,
                        Type = MessageRelatedObjectTypes.FiberCable,
                    };

                    expected.Add(dataModel.Id, expectedItem);
                }
            }

            IEnumerable<UpdateTaskObjectOverviewModel> actualResult = await storage.GetAllFlatCablesAsUpdateTaskObjectOverviewModel();
            Assert.NotNull(actualResult);
            Assert.Equal(expected.Count, actualResult.Count());

            foreach (UpdateTaskObjectOverviewModel actualItem in actualResult)
            {
                Assert.True(expected.ContainsKey(actualItem.Id));
                UpdateTaskObjectOverviewModel expectedItem = expected[actualItem.Id];

                base.CheckUpdateTaskObjectOverviewModel(expectedItem, actualItem);

            }
        }
    }
}
