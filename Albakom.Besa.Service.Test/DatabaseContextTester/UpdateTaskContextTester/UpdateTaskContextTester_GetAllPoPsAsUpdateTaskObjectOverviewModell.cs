﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetAllPoPsAsUpdateTaskObjectOverviewModell : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetAllPoPsAsUpdateTaskObjectOverviewModel|UpdateTaskContextTester")]
        public async Task GetAllPoPsAsUpdateTaskObjectOverviewModel()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(40, 50);

            Dictionary<Int32, UpdateTaskObjectOverviewModel> expected = new Dictionary<int, UpdateTaskObjectOverviewModel>();

            for (int i = 0; i < amount; i++)
            {
                PoPDataModel dataModel = base.GeneratePoPDataModel(random);
                storage.Pops.Add(dataModel);
                storage.SaveChanges();

                UpdateTaskObjectOverviewModel expectedItem = new UpdateTaskObjectOverviewModel
                {
                    Id = dataModel.Id,
                    Name = dataModel.Name,
                    ProjectAdapterId = dataModel.ProjectAdapterId,
                    Type = MessageRelatedObjectTypes.PoP,
                };

                expected.Add(dataModel.Id, expectedItem);
            }

            IEnumerable<UpdateTaskObjectOverviewModel> actualResult = await storage.GetAllPoPsAsUpdateTaskObjectOverviewModel();
            Assert.NotNull(actualResult);
            Assert.Equal(expected.Count, actualResult.Count());

            foreach (UpdateTaskObjectOverviewModel actualItem in actualResult)
            {
                Assert.True(expected.ContainsKey(actualItem.Id));
                UpdateTaskObjectOverviewModel expectedItem = expected[actualItem.Id];

                base.CheckUpdateTaskObjectOverviewModel(expectedItem, actualItem);

            }
        }
    }
}
