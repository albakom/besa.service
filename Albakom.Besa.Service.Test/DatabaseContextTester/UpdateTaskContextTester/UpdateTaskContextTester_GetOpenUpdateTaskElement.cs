﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetOpenUpdateTaskElement : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetOpenUpdateTaskElement|UpdateTaskContextTester")]
        public async Task GetOpenUpdateTaskElement()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<UpdateTasks, List<UpdateTaskElementModel>> expectedResult = new Dictionary<UpdateTasks, List<UpdateTaskElementModel>>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if (expectedResult.ContainsKey(dataModel.Task) == true) { continue; }

                dataModel.Ended = null;
                storage.SaveChanges();

                Int32 elementAmount = random.Next(10, 50);

                List<UpdateTaskElementModel> openElements = new List<UpdateTaskElementModel>();
                expectedResult.Add(dataModel.Task, openElements);

                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    if(elementDataModel.State == UpdateTaskElementStates.NotStarted)
                    {
                        openElements.Add(new UpdateTaskElementModel
                        {
                            AdapterId = elementDataModel.AdapterId,
                            ObjectId = elementDataModel.ObjectId,
                            Id = elementDataModel.Id
                        });
                    }
                }
            }

            foreach (KeyValuePair<UpdateTasks, List<UpdateTaskElementModel>> item in expectedResult)
            {
                List<UpdateTaskElementModel> actualResult = (await storage.GetOpenUpdateTaskElement(item.Key)).OrderBy(x => x.Id).ToList();
                Assert.Equal(item.Value.Count, actualResult.Count);

                for (int i = 0; i < item.Value.Count; i++)
                {
                    UpdateTaskElementModel expectedItem = item.Value[i];
                    UpdateTaskElementModel actualItem = actualResult[i];

                    Assert.Equal(expectedItem.Id, actualItem.Id);
                    Assert.Equal(expectedItem.ObjectId, actualItem.ObjectId);
                    Assert.Equal(expectedItem.AdapterId, actualItem.AdapterId);
                }
            }
        }
    }
}
