﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTaskDetails : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTaskDetails|UpdateTaskContextTester")]
        public async Task GetUpdateTaskDetails()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random(1235);

            Int32 amount = random.Next(4, 10);

            Dictionary<Int32, UpdateTaskRawDetailModel> expectedResult = new Dictionary<Int32, UpdateTaskRawDetailModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                UpdateTaskOverviewModel overviewModel = new UpdateTaskOverviewModel
                {
                    Id = dataModel.Id,
                    CreatedBy = new SimpleUserOverviewModel
                    {
                        Id = user.ID,
                        Name = user.Lastname,
                    },
                    Endend = dataModel.Ended,
                    LastHearbeat = dataModel.Heartbeat,
                    Started = dataModel.Started,
                    TaskType = dataModel.Task,
                    WasCanceled = dataModel.Canceled,
                };

                Int32 elementAmount = random.Next(5, 15);
                List<UpdateTaskElementRawOverviewModel> elementModels = new List<UpdateTaskElementRawOverviewModel>(elementAmount);
               
                for (int j = 0; j < elementAmount; j++)
                {
                    String objectName = String.Empty;
                    Int32 objectId = 0;
                    String projectAdapterId = String.Empty;

                    switch (dataModel.Task)
                    {
                        case UpdateTasks.Cables:
                        case UpdateTasks.Splices:
                        case UpdateTasks.MainCableForBranchable:
                            BranchableDataModel branchableDataModel = base.GenerateCabinet(random);
                            storage.Branchables.Add(branchableDataModel);
                            storage.SaveChanges();

                            objectName = branchableDataModel.Name;
                            objectId = branchableDataModel.Id;
                            projectAdapterId = branchableDataModel.ProjectId;
                            break;
                        case UpdateTasks.MainCableForPops:
                            PoPDataModel poPDataModel = base.GeneratePoPDataModel(random);
                            storage.Pops.Add(poPDataModel);
                            storage.SaveChanges();

                            objectName = poPDataModel.Name;
                            objectId = poPDataModel.Id;
                            projectAdapterId = poPDataModel.ProjectAdapterId;
                            break;
                        case UpdateTasks.PatchConnections:
                            FiberCableTypeDataModel typeDataModel = base.GenerateAndSaveFiberCableTypeDataModel(random, storage);
                            FiberCableDataModel cableDataModel = base.GenerateFiberCableDataModel(random, typeDataModel);
                            storage.FiberCables.Add(cableDataModel);
                            storage.SaveChanges();

                            objectName = cableDataModel.Name;
                            objectId = cableDataModel.Id;
                            projectAdapterId = cableDataModel.ProjectAdapterId;

                            break;
                        default:
                            break;
                    }

                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    elementDataModel.AdapterId = projectAdapterId;
                    elementDataModel.ObjectId = objectId;

                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    UpdateTaskElementRawOverviewModel updateTaskElement = new UpdateTaskElementRawOverviewModel
                    {
                        Ended = elementDataModel.Ended,
                        Id = elementDataModel.Id,
                        RawResult = elementDataModel.Result,
                        RelatedObject = new UpdateTaskObjectOverviewModel
                        {
                            Id = objectId,
                            Name = objectName,
                            ProjectAdapterId = projectAdapterId,
                        },
                        Started = elementDataModel.Started,
                        State = elementDataModel.State,
                    };

                    elementModels.Add(updateTaskElement);
                }

                UpdateTaskRawDetailModel detailModel = new UpdateTaskRawDetailModel
                {
                    Elements = elementModels,
                    Overview = overviewModel,
                };

                expectedResult.Add(dataModel.Id, detailModel);
            }

            foreach (KeyValuePair<Int32, UpdateTaskRawDetailModel> item in expectedResult)
            {
                UpdateTaskRawDetailModel actual = await storage.GetUpdateTaskDetails(item.Key);
                Assert.NotNull(actual);

                base.CheckUpdateTaskRawDetailModel(item.Value,actual);
            }
        }
    }
}
