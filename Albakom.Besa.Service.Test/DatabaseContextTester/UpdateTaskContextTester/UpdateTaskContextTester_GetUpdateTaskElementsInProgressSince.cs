﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTaskElementsInProgressSince : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTaskElementsInProgressSince|UpdateTaskContextTester")]
        public async Task GetUpdateTaskElementsInProgressSince()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            List<Int32> expectedIds = new List<int> ();

            DateTimeOffset treasure = DateTimeOffset.Now.AddMinutes(-random.Next(5, 10));

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Boolean heartbeatSet = random.NextDouble() > 0.5;
                if (heartbeatSet == true)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    elementDataModel.State = UpdateTaskElementStates.InProgress;
                    elementDataModel.Started = treasure.AddMinutes(-random.Next(10, 20));

                    dataModel.Heartbeat = elementDataModel.Started.Value;
                    expectedIds.Add(elementDataModel.Id);

                    continue;
                }

                Int32 elementAmount = random.Next(3, 10);
                Boolean elementFound = false;
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    if(random.NextDouble() > 0.5)
                    {
                        elementDataModel.State = UpdateTaskElementStates.InProgress;
                        elementDataModel.Started = treasure.AddMinutes(-random.Next(10, 20));

                        expectedIds.Add(elementDataModel.Id);

                        elementFound = true;
                    }
                    else
                    {
                        elementDataModel.State = UpdateTaskElementStates.Success;
                        elementDataModel.Started = treasure.AddMinutes(+random.Next(5, 10));
                    }

                    storage.SaveChanges();
                }

                if(elementFound == true)
                {
                    dataModel.Heartbeat = treasure.AddMinutes(-random.Next(10, 20));
                }
            }

            IEnumerable<Int32> actualIds = await storage.GetUpdateTaskElementsInProgressSince(treasure);
            Assert.Equal(expectedIds.OrderBy(x => x), actualIds.OrderBy(x => x));
        }
    }
}
