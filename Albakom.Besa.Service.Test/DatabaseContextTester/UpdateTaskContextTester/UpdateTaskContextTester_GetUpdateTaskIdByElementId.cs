﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTaskIdByElementId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTaskIdByElementId|UpdateTaskContextTester")]
        public async Task GetUpdateTaskIdByElementId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            Dictionary<Int32, Int32> expectedResults = new Dictionary<int, int>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    expectedResults.Add(elementDataModel.Id,dataModel.Id);
                }
            }

            foreach (KeyValuePair<Int32,Int32> expected in expectedResults)
            {
                Int32 actual = await storage.GetUpdateTaskIdByElementId(expected.Key);
                Assert.Equal(expected.Value, actual);
            }
        }
    }
}
