﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTaskIdByTaskTye : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTaskIdByTaskTye|UpdateTaskContextTester")]
        public async Task GetUpdateTaskIdByTaskTye()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<UpdateTasks, Int32> expectedResult = new Dictionary<UpdateTasks, Int32>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                if (expectedResult.ContainsKey(dataModel.Task) == true) { continue; }

                expectedResult.Add(dataModel.Task, dataModel.Id);
                dataModel.Ended = null;
                storage.SaveChanges();
            }

            foreach (KeyValuePair<UpdateTasks, Int32> item in expectedResult)
            {
                Int32 actualResult = await storage.GetUpdateTaskIdByTaskTye(item.Key);
                Assert.Equal(item.Value, actualResult);
            }
        }
    }
}
