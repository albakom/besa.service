﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTaskOverviewById : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTaskOverviewById|UpdateTaskContextTester")]
        public async Task GetUpdateTaskOverviewById()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<Int32, UpdateTaskOverviewModel> expectedResult = new Dictionary<Int32, UpdateTaskOverviewModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                UpdateTaskOverviewModel overviewModel = new UpdateTaskOverviewModel
                {
                    Id = dataModel.Id,
                    CreatedBy = new SimpleUserOverviewModel
                    {
                        Id = user.ID,
                        Name = user.Lastname,
                    },
                    Endend = dataModel.Ended,
                    LastHearbeat = dataModel.Heartbeat,
                    Started = dataModel.Started,
                    TaskType = dataModel.Task,
                    WasCanceled = dataModel.Canceled,
                };

                expectedResult.Add(dataModel.Id, overviewModel);
            }

            foreach (KeyValuePair<Int32, UpdateTaskOverviewModel> item in expectedResult)
            {
                UpdateTaskOverviewModel actual = await storage.GetUpdateTaskOverviewById(item.Key);
                Assert.NotNull(actual);

                base.CheckUpdateTaskOverviewModel(item.Value,actual);
            }
        }
    }
}
