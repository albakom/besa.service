﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_GetUpdateTasks : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUpdateTasks|UpdateTaskContextTester")]
        public async Task GetUpdateTasks()
        {

            List<UpdateTasks> tasks = new List<UpdateTasks>
            {
                UpdateTasks.Cables,
                UpdateTasks.CableTypes,
                UpdateTasks.MainCableForBranchable,
                UpdateTasks.MainCableForPops,
                UpdateTasks.PatchConnections,
                UpdateTasks.PoPs,
                UpdateTasks.Splices,
                UpdateTasks.UpdateBuildings,
            };

            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 20);

            Dictionary<Int32, UpdateTaskOverviewModel> expectedResults = new Dictionary<int, UpdateTaskOverviewModel>();
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                UpdateTaskOverviewModel expected = new UpdateTaskOverviewModel
                {
                    CreatedBy = new SimpleUserOverviewModel
                    {
                        Id = user.ID,
                        Name = user.Lastname,
                    },
                    Endend = dataModel.Ended,
                    Id = dataModel.Id,
                    LastHearbeat = dataModel.Heartbeat,
                    Started = dataModel.Started,
                    TaskType = dataModel.Task,
                    WasCanceled = dataModel.Canceled,
                };

                expectedResults.Add(dataModel.Id, expected);
            }

            {
                IEnumerable<UpdateTaskOverviewModel> actual = await storage.GetUpdateTasks(null);
                Assert.NotNull(actual);
                Assert.Equal(expectedResults.Count, actual.Count());

                foreach (UpdateTaskOverviewModel actualItem in actual)
                {
                    Assert.True(expectedResults.ContainsKey(actualItem.Id));

                    UpdateTaskOverviewModel expectedItem = expectedResults[actualItem.Id];

                    base.CheckUpdateTaskOverviewModel(expectedItem, actualItem);
                }
            }

            foreach (UpdateTasks item in tasks)
            {
                Dictionary<Int32, UpdateTaskOverviewModel> partialExpectedResults = expectedResults.Where(x => x.Value.TaskType == item).ToDictionary
                    (x => x.Key, x => x.Value);

                IEnumerable<UpdateTaskOverviewModel> actual = await storage.GetUpdateTasks(item);
                Assert.NotNull(actual);
                Assert.Equal(partialExpectedResults.Count, actual.Count());

                foreach (UpdateTaskOverviewModel actualItem in actual)
                {
                    Assert.True(partialExpectedResults.ContainsKey(actualItem.Id));

                    UpdateTaskOverviewModel expectedItem = partialExpectedResults[actualItem.Id];

                    base.CheckUpdateTaskOverviewModel(expectedItem, actualItem);
                }
            }
        }
    }
}
