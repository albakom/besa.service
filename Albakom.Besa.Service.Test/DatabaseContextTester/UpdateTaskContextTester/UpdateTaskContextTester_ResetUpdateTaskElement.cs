﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_ResetUpdateTaskElement : DatabaseTesterBase
    {
        [Fact(DisplayName = "ResetUpdateTaskElement|UpdateTaskContextTester")]
        public async Task ResetUpdateTaskElement()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            List<Int32> idToReset = new List<int>();
            List<Int32> unchagedIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    if (random.NextDouble() > 0.5)
                    {
                        idToReset.Add(elementDataModel.Id);
                    }
                    else
                    {
                        unchagedIds.Add(elementDataModel.Id);
                    }
                }
            }

            foreach (Int32 id in idToReset)
            {
                Boolean actaul = await storage.ResetUpdateTaskElement(id);
                Assert.True(actaul);
            }

            foreach (Int32 id in idToReset)
            {
                UpdateTaskElementDataModel dataModel = storage.UpdateTaskElements.FirstOrDefault(X => X.Id == id);
                Assert.NotNull(dataModel);

                Assert.Null(dataModel.Started);
                Assert.Null(dataModel.Ended);
                Assert.True(String.IsNullOrEmpty(dataModel.Result));
                Assert.Equal(UpdateTaskElementStates.NotStarted, dataModel.State);
            }
        }
    }
}
