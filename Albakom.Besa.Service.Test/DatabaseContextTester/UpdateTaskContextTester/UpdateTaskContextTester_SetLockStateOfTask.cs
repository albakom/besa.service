﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_SetLockStateOfTask : DatabaseTesterBase
    {
        [Fact(DisplayName = "SetLockStateOfTask|UpdateTaskContextTester")]
        public async Task SetLockStateOfTask()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(10, 40);

            Dictionary<Int32, Boolean> itemsToChange = new Dictionary<Int32, Boolean>(amount);
            Dictionary<Int32, Boolean> allItems = new Dictionary<int, bool>();
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                dataModel.IsLocked = random.NextDouble() > 0.5;
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                allItems.Add(dataModel.Id, dataModel.IsLocked);

                if (random.NextDouble() > 0.5)
                {
                    itemsToChange.Add(dataModel.Id, dataModel.IsLocked);
                }
            }

            foreach (KeyValuePair<Int32, Boolean> item in itemsToChange)
            {
                Boolean newValue = !item.Value;
                Boolean actualResult = await storage.SetLockStateOfTask(item.Key, newValue);

                Assert.True(actualResult);
                allItems[item.Key] = newValue;
            }

            foreach (KeyValuePair<Int32, Boolean> item in allItems)
            {
                UpdateTaskDataModel dataModel = storage.UpdateTasks.FirstOrDefault(x => x.Id == item.Key);
                Assert.NotNull(dataModel);

                Assert.Equal(item.Value, dataModel.IsLocked);
            }
        }
    }
}
