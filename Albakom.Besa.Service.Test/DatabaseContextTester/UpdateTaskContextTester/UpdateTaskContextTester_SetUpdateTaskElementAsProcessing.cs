﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_SetUpdateTaskElementAsProcessing : DatabaseTesterBase
    {
        [Fact(DisplayName = "SetUpdateTaskElementAsProcessing|UpdateTaskContextTester")]
        public async Task SetUpdateTaskElementAsProcessing()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(4, 10);

            List<Int32> possibleIds = new List<int>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Int32 elementAmount = random.Next(3, 10);
                for (int j = 0; j < elementAmount; j++)
                {
                    UpdateTaskElementDataModel elementDataModel = base.GenerateUpdateTaskElementDataModel(random, dataModel);
                    storage.UpdateTaskElements.Add(elementDataModel);
                    storage.SaveChanges();

                    if(elementDataModel.State == UpdateTaskElementStates.NotStarted)
                    {
                        possibleIds.Add(elementDataModel.Id);
                    }
                }
            }

            foreach (Int32 id in possibleIds)
            {
                Boolean result = await storage.SetUpdateTaskElementAsProcessing(id);
                Assert.True(result);

                UpdateTaskElementDataModel elementDataModel = storage.UpdateTaskElements.FirstOrDefault(x => x.Id == id);
                Assert.NotNull(elementDataModel);

                Assert.Equal(UpdateTaskElementStates.InProgress, elementDataModel.State);
                Assert.NotNull(elementDataModel.Started);
                Assert.True(Math.Abs((DateTimeOffset.Now - elementDataModel.Started.Value).TotalMinutes) <= 5);

                UpdateTaskDataModel updateTaskDataModel = storage.UpdateTasks.First(X => X.Id == elementDataModel.TaskId);
                Assert.True(Math.Abs((DateTimeOffset.Now - updateTaskDataModel.Heartbeat).TotalMinutes) <= 5);

            }
        }
    }
}
