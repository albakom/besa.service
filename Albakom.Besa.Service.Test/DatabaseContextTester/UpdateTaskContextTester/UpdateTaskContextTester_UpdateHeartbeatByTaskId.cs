﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UpdateTaskContextTester
{
    public class UpdateTaskContextTester_UpdateHeartbeatByTaskId : DatabaseTesterBase
    {
        [Fact(DisplayName = "UpdateHeartbeatByTaskId|UpdateTaskContextTester")]
        public async Task UpdateHeartbeatByTaskId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, DateTimeOffset?> expectedResults = new Dictionary<int, DateTimeOffset?>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                storage.Users.Add(user);
                storage.SaveChanges();

                UpdateTaskDataModel dataModel = base.GenerateUpdateTaskDataModel(random, user);
                storage.UpdateTasks.Add(dataModel);
                storage.SaveChanges();

                Double randomValue = random.NextDouble();
                DateTimeOffset? heartbeatValue = null;
                if(randomValue < 0.3)
                {
                }
                else if(randomValue > 0.7)
                {
                    DateTimeOffset now = DateTimeOffset.Now;
                    dataModel.Heartbeat = now;
                    heartbeatValue = now;
                    await storage.SaveChangesAsync();
                }
                else
                {
                    DateTimeOffset hearbeat = DateTimeOffset.Now.AddSeconds(-random.Next(30, 100));
                    Boolean result = await storage.UpdateHeartbeatByTaskId(dataModel.Id, hearbeat);
                    Assert.True(result);

                    heartbeatValue = hearbeat;
                }

                if(heartbeatValue.HasValue == true)
                {
                    expectedResults.Add(dataModel.Id, heartbeatValue);
                }
            }

            foreach (KeyValuePair<Int32,DateTimeOffset?> expected in expectedResults)
            {
                UpdateTaskDataModel dataModel = storage.UpdateTasks.FirstOrDefault(x => x.Id == expected.Key);

                Assert.NotNull(dataModel);
                Assert.Equal(expected.Value, dataModel.Heartbeat);
            }
        }
    }
}
