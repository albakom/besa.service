﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UserContextTester
{
    public class ProcedureContextTester_GetUserRoleRelations : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUserRoleRelations|UserContextTester")]
        public async Task GetUserRoleRelations()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Int32 amount = random.Next(20, 30);

            Dictionary<Int32, BesaRoles> expectedResults = new Dictionary<Int32, BesaRoles>();

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(random);
                storage.Users.Add(user);
                storage.SaveChanges();

                expectedResults.Add(user.ID, user.Role);
            }

            IDictionary<Int32, BesaRoles> actual = await storage.GetUserRoleRelations();

            Assert.NotNull(actual);
            Assert.Equal(expectedResults.OrderBy(x => x.Key), actual.OrderBy(x => x.Key));
        }
    }
}
