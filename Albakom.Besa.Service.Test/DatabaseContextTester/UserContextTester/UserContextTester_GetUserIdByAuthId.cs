﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester.UserContextTester
{
    public class UserContextTester_GetUserIdByAuthId : DatabaseTesterBase
    {
        [Fact(DisplayName = "GetUserIdByAuthId|UserContextTester")]
        public async Task GetUserIdByAuthId()
        {
            BesaDataStorage storage = base.GetStorage();
            Random random = new Random();

            Dictionary<String, Int32> expectedResults = new Dictionary<String, Int32>();
            Int32 amount = random.Next(20, 40);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.AuthServiceId = Guid.NewGuid().ToString();

                storage.Users.Add(user);
                storage.SaveChanges();

                expectedResults.Add(user.AuthServiceId, user.ID);
            }

            foreach (KeyValuePair<String,Int32> input in expectedResults)
            {
                Int32 actual = await storage.GetUserIdByAuthId(input.Key);
                Assert.Equal(input.Value, actual);
            }
        }
    }
}
