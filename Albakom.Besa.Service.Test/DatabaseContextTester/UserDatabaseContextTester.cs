﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.DatabaseContextTester
{
    public class UserDatabaseContextTester : DatabaseTesterBase
    {
        private UserAccessRequestDataModel CreateAccessRequestModel()
        {
            UserAccessRequestDataModel model = new UserAccessRequestDataModel
            {
                AuthServiceId = (new Guid()).ToString(),
                CompanyId = 3,
                Email = "blub@blub.de",
                CreatedAt = DateTimeOffset.Now,
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "1245",
            };

            return model;
        }

        private UserDataModel SeedUser(BesaDataStorage storage, String userId)
        {
            UserDataModel user = new UserDataModel
            {
                AuthServiceId = userId,
                Email = "test@test.de",
                Surname = "blub",
                Phone = "035235",
                Role = Contracts.Models.BesaRoles.AirInjector | Contracts.Models.BesaRoles.Admin,
            };

            storage.Users.Add(user);
            storage.SaveChanges();

            return user;
        }

        [Fact]
        public async Task CheckIfUserExists_Pass()
        {
            String userId = (new Guid()).ToString();

            BesaDataStorage storage = base.GetStorage();
            SeedUser(storage, userId);

            Boolean found = await storage.CheckIfUserExists(userId);
            Assert.True(found);
        }

        [Fact]
        public async Task CheckIfUserExists_Fail()
        {
            String userId = (new Guid()).ToString();

            BesaDataStorage storage = base.GetStorage();
            SeedUser(storage, userId);

            Boolean found = await storage.CheckIfUserExists("");
            Assert.False(found);
        }

        [Fact]
        public async Task CheckIfUserExists_WithID_Pass()
        {
            String userId = (new Guid()).ToString();

            BesaDataStorage storage = base.GetStorage();
            UserDataModel user = SeedUser(storage, userId);

            Boolean found = await storage.CheckIfUserExists(user.ID);
            Assert.True(found);
        }

        [Fact]
        public async Task CheckIfUserExists_WithID_Fail()
        {
            String userId = (new Guid()).ToString();

            BesaDataStorage storage = base.GetStorage();
            UserDataModel user = SeedUser(storage, userId);

            Boolean found = await storage.CheckIfUserExists(user.ID - 1);
            Assert.False(found);
        }

        [Fact]
        public async Task CheckIfUsersExists_WithID_Pass()
        {
            Random random = new Random();
            Int32 amount = random.Next(4, 100);

            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                usersToAdd.Add(user);
            }

            BesaDataStorage storage = base.GetStorage();
            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> ids = usersToAdd.Select(x => x.ID).ToList();


            Boolean found = await storage.CheckIfUsersExists(ids);
            Assert.True(found);
        }

        [Fact]
        public async Task CheckIfUsersExists_WithID_Fail()
        {
            Random random = new Random();
            Int32 amount = random.Next(4, 100);

            List<UserDataModel> usersToAdd = new List<UserDataModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                usersToAdd.Add(user);
            }

            BesaDataStorage storage = base.GetStorage();
            storage.Users.AddRange(usersToAdd);
            storage.SaveChanges();

            List<Int32> idsToCheck = new List<int>(amount);
            HashSet<Int32> usedIds = new HashSet<int>(amount);
            foreach (UserDataModel user in usersToAdd)
            {
                if (random.NextDouble() > 0.5)
                {
                    idsToCheck.Add(user.ID);
                }

                usedIds.Add(user.ID);
            }

            // um sicher zu gehen, dass sich die Liste definitiv unterscheiden
            Int32 invalidId = random.Next();
            while (usedIds.Contains(invalidId) == true)
            {
                invalidId = random.Next();
            }

            idsToCheck.Add(invalidId);

            Boolean found = await storage.CheckIfUsersExists(idsToCheck);
            Assert.False(found);
        }

        [Fact]
        public async Task CheckIfUserHasRole_Pass()
        {
            String userId = (new Guid()).ToString();

            BesaDataStorage storage = base.GetStorage();
            SeedUser(storage, userId);

            UserDataModel user = storage.Users.First();

            Dictionary<BesaRoles, Boolean> exceptedResults = new Dictionary<BesaRoles, Boolean>();
            exceptedResults[BesaRoles.Admin] = (user.Role & BesaRoles.Admin) == BesaRoles.Admin;
            exceptedResults[BesaRoles.AirInjector] = (user.Role & BesaRoles.AirInjector) == BesaRoles.AirInjector;
            exceptedResults[BesaRoles.CivilWorker] = (user.Role & BesaRoles.CivilWorker) == BesaRoles.CivilWorker;
            exceptedResults[BesaRoles.ProjectManager] = (user.Role & BesaRoles.ProjectManager) == BesaRoles.ProjectManager;
            exceptedResults[BesaRoles.Unknown] = true;

            foreach (var expectedResult in exceptedResults)
            {
                Boolean found = await storage.CheckIfUserHasRole(userId, expectedResult.Key);
                Assert.Equal(expectedResult.Value, found);
            }
        }

        [Fact]
        public async Task CheckCreateUserAccessRequest_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestCreateModel model = new UserAccessRequestCreateModel
            {
                AuthServiceId = (new Guid()).ToString(),
                CompanyId = 3,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now,
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "1245",
            };

            Int32 id = await storage.CreateUserAccessRequest(model);

            UserAccessRequestDataModel stored = storage.UserAccessRequests.FirstOrDefault();
            Assert.NotNull(stored);
            Assert.Equal(id, stored.Id);
            Assert.Equal(model.AuthServiceId, stored.AuthServiceId);
            Assert.Equal(model.CompanyId, stored.CompanyId);
            Assert.Equal(model.GeneratedAt, stored.CreatedAt);
            Assert.Equal(model.Lastname, stored.Lastname);
            Assert.Equal(model.Surname, stored.Surname);
        }

        [Fact]
        public async Task CheckIfUserAccessRequestExists_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestDataModel model = CreateAccessRequestModel();

            storage.UserAccessRequests.Add(model);
            storage.SaveChanges();

            Boolean result = await storage.CheckIfUserAccessRequestExists(model.AuthServiceId);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfUserAccessRequestExists_Pass_False()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestDataModel model = CreateAccessRequestModel();


            storage.UserAccessRequests.Add(model);
            storage.SaveChanges();

            Boolean result = await storage.CheckIfUserAccessRequestExists(model.AuthServiceId.Substring(1));
            Assert.False(result);
        }

        [Fact]
        public async Task CheckIfUserAccessRequestExistsByID_Pass_False()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestDataModel model = CreateAccessRequestModel();

            storage.UserAccessRequests.Add(model);
            storage.SaveChanges();

            Int32 id = model.Id;

            Boolean result = await storage.CheckIfUserAccessRequestExists(id + 4);
            Assert.False(result);
        }

        [Fact]
        public async Task CheckIfUserAccessRequestExistsByID_Pass_True()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestDataModel model = CreateAccessRequestModel();

            storage.UserAccessRequests.Add(model);
            storage.SaveChanges();

            Int32 id = model.Id;

            Boolean result = await storage.CheckIfUserAccessRequestExists(id);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfUserHasCompany_True()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            String userWithCompanyId = Guid.NewGuid().ToString();
            String userWithoutCompanyId = Guid.NewGuid().ToString();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            UserDataModel userWithoutCompany = GenerateUserDataModel(null);
            userWithoutCompany.AuthServiceId = userWithoutCompanyId;

            storage.Users.Add(userWithoutCompany);

            UserDataModel userWithCompany = GenerateUserDataModel(null);
            userWithCompany.AuthServiceId = userWithCompanyId;

            userWithCompany.CompanyId = company.Id;
            userWithCompany.Company = company;

            storage.Users.Add(userWithCompany);

            storage.SaveChanges();

            Boolean withCompanyResult = await storage.CheckIfUserHasCompany(userWithCompanyId);
            Boolean withoutCompanyResult = await storage.CheckIfUserHasCompany(userWithoutCompanyId);

            Assert.True(withCompanyResult);
            Assert.False(withoutCompanyResult);
        }

        [Fact]
        public async Task GetAllUserAccessRequest_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();

            Int32 amount = random.Next(3, 10);

            Dictionary<Int32, UserAccessRequestDataModel> expectedResult = new Dictionary<Int32, UserAccessRequestDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserAccessRequestDataModel model = new UserAccessRequestDataModel
                {
                    AuthServiceId = i.ToString(),
                    Email = $"test-{i}@test.de",
                    Lastname = $"Nachmane {i}",
                    Surname = $"Vorname {i}",
                    CreatedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(random.Next(10, 1000))),
                    Phone = $"01545456 {i}",
                };

                if (random.NextDouble() > 0.5)
                {
                    CompanyDataModel companyModel = new CompanyDataModel
                    {
                        Name = "Testfirma",
                        City = "Musterstadt",
                        Phone = "00454",
                        PostalCode = "01245",
                        Street = "Musterstr. 4",
                        StreetNumber = "20",
                    };

                    storage.Companies.Add(companyModel);
                    storage.SaveChanges();

                    model.CompanyId = companyModel.Id;
                    model.Company = companyModel;
                }

                storage.UserAccessRequests.Add(model);
                storage.SaveChanges();

                expectedResult.Add(model.Id, model);
            }

            IEnumerable<UserAccessRequestOverviewModel> results = await storage.GetAllUserAccessRequest();

            Assert.NotNull(results);
            Assert.Equal(expectedResult.Count, results.Count());

            foreach (UserAccessRequestOverviewModel result in results)
            {
                Assert.True(expectedResult.ContainsKey(result.Id));

                UserAccessRequestDataModel expected = expectedResult[result.Id];
                Assert.Equal(expected.Id, result.Id);
                Assert.Equal(expected.Email, result.EMailAddress);
                Assert.Equal(expected.Lastname, result.Lastname);
                Assert.Equal(expected.Surname, result.Surname);
                Assert.Equal(expected.CreatedAt, result.GeneratedAt);
                Assert.Equal(expected.Phone, result.Phone);

                if (expected.CompanyId.HasValue == true)
                {
                    Assert.NotNull(result.CompanyInfo);
                    Assert.Equal(expected.CompanyId, result.CompanyInfo.Id);
                    Assert.Equal(expected.Company.Id, result.CompanyInfo.Id);
                    Assert.Equal(expected.Company.Name, result.CompanyInfo.Name);
                }
                else
                {
                    Assert.Null(result.CompanyInfo);
                }
            }


            List<Int32> exceptedSorting = expectedResult.OrderBy(x => x.Value.CreatedAt).Select(x => x.Key).ToList();
            List<Int32> actualSorting = results.OrderBy(x => x.GeneratedAt).Select(x => x.Id).ToList();

            for (int i = 0; i < expectedResult.Count; i++)
            {
                Assert.Equal(exceptedSorting[i], actualSorting[i]);
            }
        }

        [Fact]
        public async Task DeleteAuthRequest_Pass()
        {
            BesaDataStorage storage = base.GetStorage();

            UserAccessRequestDataModel model = CreateAccessRequestModel();

            storage.UserAccessRequests.Add(model);
            storage.SaveChanges();

            Int32 id = model.Id;

            await storage.DeleteAuthRequest(id);

            Assert.Equal(0, storage.UserAccessRequests.Count(x => x.Id == id));
        }

        [Fact]
        public async Task CreateUser_Pass()
        {
            UserCreateModel model = new UserCreateModel
            {
                AuthServiceId = (new Guid()).ToString(),
                CompanyId = null,
                EMailAddress = "test@test.de",
                Lastname = "Nachname",
                Surname = "Vorname",
                Phone = "0154",
                Role = BesaRoles.Admin | BesaRoles.ProjectManager,
            };

            BesaDataStorage storage = base.GetStorage();

            Int32 id = await storage.CreateUser(model);

            UserDataModel dataModel = storage.Users.FirstOrDefault(x => x.ID == id);

            Assert.NotNull(dataModel);

            Assert.Equal(id, dataModel.ID);
            Assert.Equal(model.AuthServiceId, dataModel.AuthServiceId);
            Assert.Equal(model.CompanyId, dataModel.CompanyId);
            Assert.Equal(model.EMailAddress, dataModel.Email);
            Assert.Equal(model.Lastname, dataModel.Lastname);
            Assert.Equal(model.Surname, dataModel.Surname);
            Assert.Equal(model.Phone, dataModel.Phone);
            Assert.Equal(model.Role, dataModel.Role);
        }

        [Fact]
        public async Task CreateUser_WithCompany_Pass()
        {
            UserCreateModel model = new UserCreateModel
            {
                AuthServiceId = (new Guid()).ToString(),
                CompanyId = null,
                EMailAddress = "test@test.de",
                Lastname = "Nachname",
                Surname = "Vorname",
                Phone = "0154",
                Role = BesaRoles.Admin | BesaRoles.ProjectManager,
            };

            BesaDataStorage storage = base.GetStorage();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            model.CompanyId = company.Id;

            Int32 id = await storage.CreateUser(model);

            UserDataModel dataModel = storage.Users.FirstOrDefault(x => x.ID == id);

            Assert.NotNull(dataModel);

            Assert.Equal(id, dataModel.ID);
            Assert.Equal(model.AuthServiceId, dataModel.AuthServiceId);
            Assert.Equal(model.CompanyId, dataModel.CompanyId);
            Assert.Equal(model.EMailAddress, dataModel.Email);
            Assert.Equal(model.Lastname, dataModel.Lastname);
            Assert.Equal(model.Surname, dataModel.Surname);
            Assert.Equal(model.Phone, dataModel.Phone);
            Assert.Equal(model.Role, dataModel.Role);
        }

        [Fact]
        public async Task RemoveAuthIdFromUser()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            Boolean result = await storage.RemoveAuthIdFromUser(user.ID);
            Assert.True(result);

            Assert.Equal(BesaRoles.Unknown, user.Role);
            Assert.True(String.IsNullOrEmpty(user.AuthServiceId));
        }

        [Fact]
        public async Task CheckIfHasAuthId()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            Boolean result = await storage.CheckIfHasAuthId(user.ID);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfHasAuthId_False()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel user = base.GenerateUserDataModel(null);
            user.AuthServiceId = String.Empty;
            storage.Users.Add(user);
            storage.SaveChanges();

            Boolean result = await storage.CheckIfHasAuthId(user.ID);
            Assert.False(result);
        }

        [Fact]
        public async Task GetUserRoles()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel user = base.GenerateUserDataModel(null);
            storage.Users.Add(user);
            storage.SaveChanges();

            BesaRoles expected = user.Role;

            BesaRoles actual = await storage.GetUserRoles(user.AuthServiceId);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "CheckIfUserIsManagement|UserDatabaseContextTester")]
        public async Task CheckIfUserIsManagement()
        {
            Random random = new Random();
            BesaDataStorage storage = base.GetStorage();

            Int32 userAmount = random.Next(3, 10);

            Dictionary<String, Boolean> expectedResult = new Dictionary<string, bool>();
            for (int i = 0; i < userAmount; i++)
            {
                UserDataModel user = base.GenerateUserDataModel(null);
                user.IsManagement = random.NextDouble() > 0.5;
                user.AuthServiceId = Guid.NewGuid().ToString();

                storage.Users.Add(user);
                storage.SaveChanges();

                expectedResult.Add(user.AuthServiceId, user.IsManagement);
            }

            foreach (KeyValuePair<string, Boolean> expected in expectedResult)
            {
                Boolean actual = await storage.CheckIfUserIsManagement(expected.Key);
                Assert.Equal(expected.Value, actual);
            }
        }

        [Fact]
        public async Task GetAllUsers()
        {
            BesaDataStorage storage = base.GetStorage();

            Random random = new Random();
            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, UserDataModel> expectedResult = new Dictionary<Int32, UserDataModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                UserDataModel model = new UserDataModel
                {
                    Email = $"test-{i}@test.de",
                    Lastname = $"Nachmane {i}",
                    Surname = $"Vorname {i}",
                    Phone = $"01545456 {i}",
                    Role = BesaRoles.AirInjector,
                };

                if (random.NextDouble() > 0.5)
                {
                    model.AuthServiceId = Guid.NewGuid().ToString();
                }

                if (random.NextDouble() > 0.5)
                {
                    CompanyDataModel companyModel = new CompanyDataModel
                    {
                        Name = "Testfirma",
                        City = "Musterstadt",
                        Phone = "00454",
                        PostalCode = "01245",
                        Street = "Musterstr. 4",
                        StreetNumber = "20",
                    };

                    storage.Companies.Add(companyModel);
                    storage.SaveChanges();

                    model.CompanyId = companyModel.Id;
                    model.Company = companyModel;
                }

                storage.Users.Add(model);
                storage.SaveChanges();

                if (String.IsNullOrEmpty(model.AuthServiceId) == false)
                {
                    expectedResult.Add(model.ID, model);
                }
            }

            IEnumerable<UserOverviewModel> results = await storage.GetAllUsers();

            Assert.NotNull(results);
            Assert.Equal(expectedResult.Count, results.Count());

            foreach (UserOverviewModel result in results)
            {
                Assert.True(expectedResult.ContainsKey(result.Id));

                UserDataModel expected = expectedResult[result.Id];
                Assert.Equal(expected.ID, result.Id);
                Assert.Equal(expected.Email, result.EMailAddress);
                Assert.Equal(expected.Lastname, result.Lastname);
                Assert.Equal(expected.Surname, result.Surname);
                Assert.Equal(expected.Phone, result.Phone);

                if (expected.CompanyId.HasValue == true)
                {
                    Assert.NotNull(result.CompanyInfo);
                    Assert.Equal(expected.CompanyId, result.CompanyInfo.Id);
                    Assert.Equal(expected.Company.Id, result.CompanyInfo.Id);
                    Assert.Equal(expected.Company.Name, result.CompanyInfo.Name);
                }
                else
                {
                    Assert.Null(result.CompanyInfo);
                }

                if (String.IsNullOrEmpty(expected.AuthServiceId) == false)
                {
                    Assert.True(result.IsMember);
                }
                else
                {
                    Assert.False(result.IsMember);

                }
            }
        }

        [Fact]
        public async Task GetAuthServiceIdByUserId()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel model = base.GenerateUserDataModel(null);
            String authId = model.AuthServiceId;

            storage.Users.Add(model);
            storage.SaveChanges();

            String actual = await storage.GetAuthServiceIdByUserId(model.ID);

            Assert.Equal(authId, actual);
        }

        [Fact]
        public async Task GetUserOverviewById()
        {
            Random rand = new Random();
            BesaDataStorage storage = base.GetStorage();

            UserDataModel model = base.GenerateUserDataModel(null);
            if (rand.NextDouble() > 0.5)
            {
                model.AuthServiceId = String.Empty;
            }

            storage.Users.Add(model);
            storage.SaveChanges();

            UserOverviewModel userOverview = await storage.GetUserOverviewById(model.ID);

            Assert.NotNull(userOverview);
            Assert.Equal(model.ID, userOverview.Id);
            Assert.Equal(model.Lastname, userOverview.Lastname);
            Assert.Equal(model.Surname, userOverview.Surname);
            Assert.Equal(model.Phone, userOverview.Phone);
            Assert.Equal(!String.IsNullOrEmpty(model.AuthServiceId), userOverview.IsMember);
            Assert.Equal(model.Email, userOverview.EMailAddress);
        }

        [Fact]
        public async Task GetUserRoleById()
        {
            BesaDataStorage storage = base.GetStorage();

            UserDataModel model = base.GenerateUserDataModel(null);
            BesaRoles role = model.Role;

            storage.Users.Add(model);
            storage.SaveChanges();

            BesaRoles actual = await storage.GetUserRoleById(model.ID);

            Assert.Equal(role, actual);
        }

        [Fact]
        public async Task EditUser()
        {
            Random random = new Random();

            BesaDataStorage storage = base.GetStorage();

            UserDataModel model = base.GenerateUserDataModel(null);

            storage.Users.Add(model);
            storage.SaveChanges();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            storage.Companies.Add(company);
            storage.SaveChanges();

            UserEditModel editModel = new UserEditModel
            {
                Id = model.ID,
                CompanyId = company.Id,
                EMailAddress = "neueMail@test.de",
                Lastname = model.Lastname.Substring(0, model.Lastname.Length / 2),
                Surname = model.Surname.Substring(0, model.Surname.Length / 2),
                Phone = "015782-487501",
                Role = BesaRoles.All,
                IsManagement = random.NextDouble() > 0.5,
            };

            await storage.EditUser(editModel);

            Assert.Equal(editModel.CompanyId, model.CompanyId);
            Assert.Equal(editModel.EMailAddress, model.Email);
            Assert.Equal(editModel.Lastname, model.Lastname);
            Assert.Equal(editModel.Phone, model.Phone);
            Assert.Equal(editModel.Role, model.Role);
            Assert.Equal(editModel.Surname, model.Surname);
            Assert.Equal(editModel.IsManagement, model.IsManagement);
        }

        [Fact]
        public async Task GetUserDataForEdit()
        {
            Random random = new Random();
            BesaDataStorage storage = base.GetStorage();

            UserDataModel model = base.GenerateUserDataModel(null);
            model.IsManagement = random.NextDouble() > 0.5;
            storage.Users.Add(model);
            storage.SaveChanges();

            CompanyDataModel company = base.GenerateCompanyDataModel();
            model.Company = company;
            company.Employes.Add(model);

            storage.Companies.Add(company);
            storage.SaveChanges();

            UserEditModel actual = await storage.GetUserDataForEdit(model.ID);

            Assert.NotNull(actual);

            Assert.Equal(model.CompanyId, actual.CompanyId);
            Assert.Equal(model.Email, actual.EMailAddress);
            Assert.Equal(model.Lastname, actual.Lastname);
            Assert.Equal(model.Phone, actual.Phone);
            Assert.Equal(model.Role, actual.Role);
            Assert.Equal(model.Surname, actual.Surname);
            Assert.Equal(model.IsManagement, actual.IsManagement);

        }

        [Fact(DisplayName = "GetUserInfoModel|UserContextTester")]
        public async Task GetUserInfoModel()
        {
            Random random = new Random();
            BesaDataStorage storage = base.GetStorage();

            Int32 companyAmount = random.Next(3, 10);

            List<CompanyDataModel> companies = new List<CompanyDataModel>();
            for (int i = 0; i < companyAmount; i++)
            {
                CompanyDataModel company = base.GenerateCompanyDataModel();
                company.Name = $"Testfirma {random.Next()}";

                storage.Companies.Add(company);
                storage.SaveChanges();

                companies.Add(company);
            }

            Int32 amount = random.Next(10, 30);

            Dictionary<String, UserInfoModel> expectedResults = new Dictionary<String, UserInfoModel>();
            for (int i = 0; i < amount; i++)
            {
                UserInfoModel expectedItem = new UserInfoModel();

                UserDataModel user = base.GenerateUserDataModel(null);
                user.Lastname = $"Vorname {random.Next()}";
                user.Surname = $"Vorname {random.Next()}";
                user.IsManagement = random.NextDouble() > 0.5;
                user.AuthServiceId = Guid.NewGuid().ToString();

                expectedItem.Surname = user.Surname;
                expectedItem.Lastname = user.Lastname;
                expectedItem.IsManagement = user.IsManagement;
                expectedItem.Roles = user.Role;

                Boolean useCompany = random.NextDouble() > 0.5;
                if (useCompany == true)
                {
                    user.Company = companies[random.Next(0, companies.Count)];

                    expectedItem.Company = new SimpleCompanyOverviewModel
                    {
                        Id = user.Company.Id,
                        Name = user.Company.Name,
                    };
                }

                storage.Users.Add(user);
                storage.SaveChanges();


                expectedResults.Add(user.AuthServiceId, expectedItem);
            }

            foreach (KeyValuePair<String, UserInfoModel> item in expectedResults)
            {
                UserInfoModel actual = await storage.GetUserInfoModel(item.Key);

                Assert.NotNull(actual);
                Assert.Equal(item.Value.Surname, actual.Surname);
                Assert.Equal(item.Value.Lastname, actual.Lastname);
                Assert.Equal(item.Value.Roles, actual.Roles);
                Assert.Equal(item.Value.IsManagement, actual.IsManagement);

                if (item.Value.Company == null)
                {
                    Assert.Null(actual.Company);
                }
                else
                {
                    Assert.NotNull(actual.Company);
                    Assert.Equal(item.Value.Company.Id, actual.Company.Id);
                    Assert.Equal(item.Value.Company.Name, actual.Company.Name);
                }
            }
        }
    }
}
