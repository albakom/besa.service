﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ArticleManagamentServiceTester
{
    public class ArticleManagamentServiceTester_CreateArticle : ProtocolTesterBase
    {
        [Fact(DisplayName = "CreateArticle_Pass|ArticleManagamentServiceTester")]
        public async Task CreateArticle_Pass()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleCreateModel model = new ArticleCreateModel
            {
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            ArticleOverviewModel overviewModel = new ArticleOverviewModel
            {
                Name = model.Name,
                Id = articleId,
                ProductSoldByMeter = model.ProductSoldByMeter,
                IsInUse = false,
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, null)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateArticle(model)).ReturnsAsync(articleId);
            mockStorage.Setup(ctx => ctx.GetArticleOverviewById(articleId)).ReturnsAsync(overviewModel);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.Article, articleId);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleOverviewModel actual = await service.CreateArticle(model, userAuthId);

            Assert.NotNull(actual);

            Assert.Equal(articleId, actual.Id);
            Assert.Equal(overviewModel.Name, actual.Name);
            Assert.Equal(overviewModel.ProductSoldByMeter, actual.ProductSoldByMeter);
            Assert.Equal(overviewModel.IsInUse, actual.IsInUse);
        }

        [Fact(DisplayName = "CreateArticle_Fail_NoModel|ArticleManagamentServiceTester")]
        public async Task CreateArticle_Fail_NoModel()
        {
            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.CreateArticle(null, null));
            Assert.Equal(ArticleManagementServicExceptionReasons.NoData, exception.Reason);
        }

        [Fact(DisplayName = "CreateArticle_Fail_NoName|ArticleManagamentServiceTester")]
        public async Task CreateArticle_Fail_NoName()
        {
            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.CreateArticle(new ArticleCreateModel(), null));
            Assert.Equal(ArticleManagementServicExceptionReasons.InvalidData, exception.Reason);
        }

        [Fact(DisplayName = "CreateArticle_Fail_NameInUse|ArticleManagamentServiceTester")]
        public async Task CreateArticle_Fail_NameInUse()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleCreateModel model = new ArticleCreateModel
            {
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, null)).ReturnsAsync(true);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.CreateArticle(model, null));
            Assert.Equal(ArticleManagementServicExceptionReasons.InvalidData, exception.Reason);
        }

        [Fact(DisplayName = "CreateArticle_Fail_UserNotFound|ArticleManagamentServiceTester")]
        public async Task CreateArticle_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ArticleCreateModel model = new ArticleCreateModel
            {
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            ArticleOverviewModel overviewModel = new ArticleOverviewModel
            {
                Name = model.Name,
                Id = articleId,
                ProductSoldByMeter = model.ProductSoldByMeter,
                IsInUse = false,
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, null)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateArticle(model)).ReturnsAsync(articleId);
            mockStorage.Setup(ctx => ctx.GetArticleOverviewById(articleId)).ReturnsAsync(overviewModel);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.CreateArticle(model, userAuthId));
            Assert.Equal(ArticleManagementServicExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
