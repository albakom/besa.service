﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ArticleManagamentServiceTester
{
    public class ArticleManagamentServiceTester_DeleteArticle : ProtocolTesterBase
    {
        [Fact(DisplayName = "DeleteArticle_NoUse_True|ArticleManagamentServiceTester")]
        public async Task DeleteArticle_NoUse_Pass_True()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists( articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleIsInUse(articleId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.DeleteArticle(articleId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Article, articleId);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            Boolean actual = await service.DeleteArticle(articleId,false, userAuthId);
            Assert.True(actual);
        }

        [Fact(DisplayName = "DeleteArticle_NoUse_Pass_False|ArticleManagamentServiceTester")]
        public async Task DeleteArticle_NoUse_Pass_False()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleIsInUse(articleId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.DeleteArticle(articleId)).ReturnsAsync(false);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            Boolean actual = await service.DeleteArticle(articleId, false, null);
            Assert.False(actual);
        }

        [Fact(DisplayName = "DeleteArticle_InUse_Pass_True|ArticleManagamentServiceTester")]
        public async Task DeleteArticle_InUse_Pass_True()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleIsInUse(articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.DeleteArticle(articleId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Article, articleId);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            Boolean actual = await service.DeleteArticle(articleId, true, userAuthId);
            Assert.True(actual);
        }

        [Fact(DisplayName = "GetDetails_Fail_NotFound|ArticleManagamentServiceTester")]
        public async Task GetDetails_Fail_NotFound()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(false);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.DeleteArticle(articleId, false,null));
            Assert.Equal(ArticleManagementServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "GetDetails_Fail_NoForce|ArticleManagamentServiceTester")]
        public async Task GetDetails_Fail_NoForce()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleIsInUse(articleId)).ReturnsAsync(true);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.DeleteArticle(articleId,false,null));
            Assert.Equal(ArticleManagementServicExceptionReasons.DeleteNeedToBeForced, exception.Reason);
        }
    }
}
