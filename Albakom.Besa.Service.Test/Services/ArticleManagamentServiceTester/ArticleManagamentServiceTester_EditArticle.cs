﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ArticleManagamentServiceTester
{
    public class ArticleManagamentServiceTester_EditArticle : ProtocolTesterBase
    {
        [Fact(DisplayName = "EditArticle_Pass|ArticleManagamentServiceTester")]
        public async Task EditArticle_Pass()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleEditModel model = new ArticleEditModel
            {
                Id = articleId,
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            ArticleOverviewModel overviewModel = new ArticleOverviewModel
            {
                Name = model.Name,
                Id = articleId,
                ProductSoldByMeter = model.ProductSoldByMeter,
                IsInUse = false,
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists( articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, articleId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.EditArticle(model)).Returns(Task.CompletedTask);
            mockStorage.Setup(ctx => ctx.GetArticleOverviewById(articleId)).ReturnsAsync(overviewModel);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Update, MessageRelatedObjectTypes.Article, articleId);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleOverviewModel actual = await service.EditArticle(model, userAuthId);

            Assert.NotNull(actual);

            Assert.Equal(articleId, actual.Id);
            Assert.Equal(overviewModel.Name, actual.Name);
            Assert.Equal(overviewModel.ProductSoldByMeter, actual.ProductSoldByMeter);
            Assert.Equal(overviewModel.IsInUse, actual.IsInUse);
        }

        [Fact(DisplayName = "EditArticle_Fail_NoModel|ArticleManagamentServiceTester")]
        public async Task EditArticle_Fail_NoModel()
        {
            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.EditArticle(null,null));
            Assert.Equal(ArticleManagementServicExceptionReasons.NoData, exception.Reason);
        }

        [Fact(DisplayName = "EditArticle_Fail_NoName|ArticleManagamentServiceTester")]
        public async Task EditArticle_Fail_NoName()
        {
            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.EditArticle(new ArticleEditModel(),null));
            Assert.Equal(ArticleManagementServicExceptionReasons.InvalidData, exception.Reason);
        }

        [Fact(DisplayName = "EditArticle_Fail_NotFound|ArticleManagamentServiceTester")]
        public async Task EditArticle_Fail_NotFound()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleEditModel model = new ArticleEditModel
            {
                Id = articleId,
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, articleId)).ReturnsAsync(false);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.EditArticle(model,null));
            Assert.Equal(ArticleManagementServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "Article_Fail_NameInUse|ArticleManagamentServiceTester")]
        public async Task Article_Fail_NameInUse()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleEditModel model = new ArticleEditModel
            {
                Id = articleId,
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfArticleNameExists(model.Name, articleId)).ReturnsAsync(true);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.EditArticle(model,null));
            Assert.Equal(ArticleManagementServicExceptionReasons.InvalidData, exception.Reason);
        }
    }
}
