﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ArticleManagamentServiceTester
{
    public class ArticleManagamentServiceTester_GetAllArticles : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetAllArticles|ArticleManagamentServiceTester")]
        public async Task GetAllArticles()
        {
            Random rand = new Random();

            Int32 articleAmount = rand.Next(3, 10);

            List<ArticleOverviewModel> articles = new List<ArticleOverviewModel>(articleAmount);
            for (int i = 0; i < articleAmount; i++)
            {
                ArticleOverviewModel article = new ArticleOverviewModel
                {
                    Id = i + 1,
                    Name = $"Produktname {rand.Next()}",
                    ProductSoldByMeter = rand.NextDouble() > 0.5,
                    IsInUse = rand.NextDouble() > 0.5,
                };

                articles.Add(article);
            }

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetAllArticles()).ReturnsAsync(articles);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger,base.GetProtocolService(mockStorage.Object));
            IEnumerable< ArticleOverviewModel> actual = await service.GetAllArticles();

            Assert.NotNull(actual);

            Assert.Equal(articles.Count, actual.Count());

            foreach (ArticleOverviewModel acutalItem in actual)
            {
                ArticleOverviewModel expectedItem = articles.FirstOrDefault(x => x.Id == acutalItem.Id);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.Id, acutalItem.Id);
                Assert.Equal(expectedItem.Name, acutalItem.Name);
                Assert.Equal(expectedItem.ProductSoldByMeter, acutalItem.ProductSoldByMeter);
            }
        }
    }
}
