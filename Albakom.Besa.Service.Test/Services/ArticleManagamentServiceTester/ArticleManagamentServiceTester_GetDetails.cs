﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.ArticleManagamentServiceTester
{
    public class ArticleManagamentServiceTester_GetDetails : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetDetails_Pass|ArticleManagamentServiceTester")]
        public async Task GetDetails_Pass()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            ArticleDetailModel model = new ArticleDetailModel
            {
                Id = articleId,
                Name = $"Produkt-Name {rand.Next()}",
                ProductSoldByMeter = rand.NextDouble() > 0.5
            };

            var logger = Mock.Of<ILogger<ArticleManagementService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists( articleId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetArticleDetails(articleId)).ReturnsAsync(model);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleDetailModel actual = await service.GetDetails(articleId);

            Assert.NotNull(actual);

            Assert.Equal(articleId, actual.Id);
            Assert.Equal(model.Name, actual.Name);
            Assert.Equal(model.ProductSoldByMeter, actual.ProductSoldByMeter);
        }

        [Fact(DisplayName = "GetDetails_Fail_NotFound|ArticleManagamentServiceTester")]
        public async Task GetDetails_Fail_NotFound()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();

            var logger = Mock.Of<ILogger<ArticleManagementService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(articleId)).ReturnsAsync(false);

            IArticleManagementService service = new ArticleManagementService(mockStorage.Object, logger, base.GetProtocolService(mockStorage.Object));
            ArticleManagementServicException exception = await Assert.ThrowsAsync<ArticleManagementServicException>(() => service.GetDetails(articleId));
            Assert.Equal(ArticleManagementServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
