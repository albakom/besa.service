﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.AzureFileManagerTester
{
    public class AzureFileManagerTester_DownloadFile
    {
        [Fact]
        public async Task Download_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<AzureFileManager>>();

            String connenctionString = @"DefaultEndpointsProtocol=https;AccountName=albakomunittesting;AccountKey=UiLk5jw2Nu1/uJv9MJ2BRxW8SzjYx0FqM4lRAwKydyD1Dd3UD3iRNxPtvWoRyRUdKwDJgVxS+tgm0yHlFzMjXQ==;EndpointSuffix=core.windows.net";

            IFileManager service = new AzureFileManager(logger, connenctionString);
            String fileUrl = @"https://albakomunittesting.blob.core.windows.net/unit-test-read/MimeTypes.csv";
            Stream result = await service.Download(fileUrl, 3096);

            Assert.NotNull(result);
            Assert.True(result.CanSeek);

            result.Seek(0, SeekOrigin.Begin);

            //Byte vergleich

        }
    }
}
