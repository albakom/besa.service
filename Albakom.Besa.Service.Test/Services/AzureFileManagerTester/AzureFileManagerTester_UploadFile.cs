﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.AzureFileManagerTester
{
    public class AzureFileManagerTester_UploadFile
    {
        private FileCreateModel GetValidModel(Random rand)
        {
            FileCreateModel createModel = new FileCreateModel
            {
                Extention = $"jpg",
                Name = $"Datei-{rand.Next()}",
                MimeType = "image/jpeg",
                Size = rand.Next(),
            };

            return createModel;
        }

        [Fact]
        public async Task Upload_Pass()
        {
            Random rand = new Random();

            FileCreateModel createModel = GetValidModel(rand);

            Byte[] pseudoData = new Byte[512];
            rand.NextBytes(pseudoData);
            MemoryStream stream = new MemoryStream(pseudoData, false);

            var logger = Mock.Of<ILogger<AzureFileManager>>();

            String connenctionString = @"DefaultEndpointsProtocol=https;AccountName=albakomunittesting;AccountKey=UiLk5jw2Nu1/uJv9MJ2BRxW8SzjYx0FqM4lRAwKydyD1Dd3UD3iRNxPtvWoRyRUdKwDJgVxS+tgm0yHlFzMjXQ==;EndpointSuffix=core.windows.net";

            IFileManager service = new AzureFileManager(logger, connenctionString);
            String url = await service.Upload(createModel, stream, "unit-test");

            Assert.False(String.IsNullOrEmpty(url));
        }
    }
}
