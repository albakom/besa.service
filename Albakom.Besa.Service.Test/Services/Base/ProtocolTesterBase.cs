﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Albakom.Besa.Service.Test.Services
{
    public class ProtocolTesterBase
    {
        #region Methods

        protected String PrepareStorage(Random rand, Mock<IBesaDataStorage> mockStorage, MessageActions action, MessageRelatedObjectTypes type)
        {
            String userAuthId = Guid.NewGuid().ToString();
            Int32 userId = rand.Next();

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserIdByAuthId(userAuthId)).ReturnsAsync(userId);
            mockStorage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == action &&
           x.RelatedObjectId.HasValue == false &&
            x.RelatedType == type &&
           x.TriggeredByUserId == userId &&
          Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) < 10
            ))).ReturnsAsync(userId);

            return userAuthId;
        }

        protected String PrepareStorage(Random rand, Mock<IBesaDataStorage> mockStorage, MessageActions action, MessageRelatedObjectTypes type, Int32 objectId )
        {
            String userAuthId = Guid.NewGuid().ToString();
            Int32 userId = rand.Next();

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserIdByAuthId(userAuthId)).ReturnsAsync(userId);
            mockStorage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == action &&
           x.RelatedObjectId == objectId  &&
           x.RelatedType == type &&
           x.TriggeredByUserId == userId &&
          Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) < 10
            ))).ReturnsAsync(userId);

            return userAuthId;
        }

        public IProtocolService GetProtocolService(Mock<IBesaDataStorage> mockStorage)
        {
            return GetProtocolService(mockStorage.Object);
        }

        public IProtocolService GetProtocolService(IBesaDataStorage storage)
        {
            IProtocolService protocolService = new ProtocolService(
                storage, 
                Mock.Of<IMessageSendEngine>(), 
                Mock.Of<IEmailSendEngine>(), 
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(), 
                Mock.Of<ILogger<ProtocolService>>());
            return protocolService;
        }

        #endregion

    }
}
