﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_AddEmployee : ProtocolTesterBase
    {
        [Fact(DisplayName = "AddEmployee_Pass|ProtocolTesterBase")]
        public async Task AddEmployee_Pass()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId,employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.AddEmployeeToCompany(companyId,employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));
            Boolean actual = await service.AddEmployee(companyId, employeeId, userAuthId);
            Assert.True(actual);
        }

        [Fact(DisplayName = "AddeEmployee_Fail_CompanyNotFoud|ProtocolTesterBase")]
        public async Task AddeEmployee_Fail_CompanyNotFoud()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.AddEmployee(companyId, employeeId,userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "AddeEmployee_Fail_UserNotFoud|ProtocolTesterBase")]
        public async Task AddeEmployee_Fail_UserNotFoud()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.AddEmployee(companyId, employeeId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "AddeEmployee_Fail_IsInCompany|ProtocolTesterBase")]
        public async Task AddeEmployee_Fail_IsInCompany()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.AddEmployeeToCompany(companyId,employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.AddEmployee(companyId, employeeId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.UserAlreadyMember, exception.Reason);
        }

        [Fact(DisplayName = "AddEmployee_Fail_DatabaseFailed|ProtocolTesterBase")]
        public async Task AddEmployee_Fail_DatabaseFailed()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            Boolean actual = await service.AddEmployee(companyId, employeeId, userAuthId);
            Assert.False(actual);
        }

        [Fact(DisplayName = "AddEmployee_Fail_ProtocolUserNotFound|ProtocolTesterBase")]
        public async Task AddEmployee_Fail_ProtocolUserNotFound()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.AddEmployeeToCompany(companyId, employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.AddEmployeeToCompany, MessageRelatedObjectTypes.Article, companyId);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            Boolean actual = await service.AddEmployee(companyId, employeeId, userAuthId);
            Assert.True(actual);
        }

    }
}
