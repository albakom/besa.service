﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_CreateCompany : ProtocolTesterBase
    {
        [Fact(DisplayName = "CreateCompany_Pass|CompanyServiceTester")]
        public async Task CreateCompany_Pass()
        {
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<CompanyService>>();

            String companyName = "Testfirma";

            CompanyCreateModel model = new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            };

            Random random = new Random();

            Int32 companyId = random.Next();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyName)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateCompany(model)).ReturnsAsync(companyId);
            mockStorage.Setup(ctx => ctx.GetCompanyOverviewById(companyId)).ReturnsAsync(new CompanyOverviewModel
            {
                Name = companyName,
                Id = 1,
                EmployeeCount = 0,
            });

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyOverviewModel result = await service.CreateCompany(model, userAuthId);

            Assert.NotNull(result);
            Assert.Equal(model.Name, result.Name);
            Assert.Equal(0, result.EmployeeCount);
            Assert.True(result.Id > 0);
        }

        [Fact(DisplayName = "CreateCompany_Fail|CompanyServiceTester")]
        public async Task CreateCompany_Fail()
        {
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<CompanyService>>();

            String exisitnName = "Firma mit bekannten Namen";

            Random random = new Random();


            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(exisitnName)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.Company, 1);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException nullException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.CreateCompany(null, userAuthId));
            Assert.Equal(ComponyServiceExceptionReasons.CreateModelIsNull, nullException.Reason);

            List<CompanyCreateModel> invalidModels = new List<CompanyCreateModel>();
            invalidModels.Add(new CompanyCreateModel
            {
                // Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = null,
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    // City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    // PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    // Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    // StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                // Phone = "15454",
            });
            invalidModels.Add(new CompanyCreateModel
            {
                Name = exisitnName,
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                // Phone = "15454",
            });


            foreach (CompanyCreateModel invalidRequest in invalidModels)
            {
                CompanyServiceException invalidException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.CreateCompany(invalidRequest, userAuthId));
                Assert.Equal(ComponyServiceExceptionReasons.InvalidEditModel, invalidException.Reason);
            }
        }


        [Fact(DisplayName = "CreateCompany_Fail_UserNotFound|CompanyServiceTester")]
        public async Task CreateCompany_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 articleId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            CompanyCreateModel model = new CompanyCreateModel
            {
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            };

            Random random = new Random();

            Int32 companyId = random.Next();
            String companyName = "Testfirma";

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyName)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateCompany(model)).ReturnsAsync(companyId);
            mockStorage.Setup(ctx => ctx.GetCompanyOverviewById(companyId)).ReturnsAsync(new CompanyOverviewModel
            {
                Name = companyName,
                Id = 1,
                EmployeeCount = 0,
            });
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            var logger = Mock.Of<ILogger<CompanyService>>();
            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.CreateCompany(model, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
