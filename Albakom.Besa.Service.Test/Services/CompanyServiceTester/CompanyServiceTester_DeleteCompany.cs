﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_DeleteCompany : ProtocolTesterBase
    {
        [Fact(DisplayName = "DeleteCompany_Pass|CompanyServiceTester")]
        public async Task DeleteCompany_Pass()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.DeleteCompany(companyId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            Boolean actual = await service.DeleteCompany(companyId,userAuthId);
            Assert.True(actual);
        }

        [Fact(DisplayName = "DeleteCompany_DatabseFailed|CompanyServiceTester")]
        public async Task DeleteCompany_DatabseFailed()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.DeleteCompany(companyId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            Boolean actual = await service.DeleteCompany(companyId, userAuthId);
            Assert.False(actual);
        }

        [Fact(DisplayName = "DeleteCompany_CompanyNotFoud|CompanyServiceTester")]
        public async Task DeleteCompany_CompanyNotFoud()
        {
            Random rand = new Random();
            Int32 companyId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.DeleteCompany(companyId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }
    }
}
