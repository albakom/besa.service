﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_EditCompany : ProtocolTesterBase
    {
        [Fact(DisplayName = "NormalEditCompany_Pass|CompanyServiceTester")]
        public async Task NormalEditCompany_Pass()
        {
            Random random = new Random();
            Int32 companyId = random.Next();
            String companyname = "Testfirma";

            CompanyEditModel model = new CompanyEditModel
            {
                Id = companyId,
                Name = companyname,
                Phone = "01523",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    Street = "Musterstr.",
                    PostalCode = "01574",
                    StreetNumber = "156a",
                }
            };

            IStorageContraints contraints = new BesaDataStorageContraints();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyname, companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.GetCompanyOverviewById(companyId)).ReturnsAsync(new CompanyOverviewModel
            {
                Name = companyname,
                Id = companyId,
                EmployeeCount = 0,
            });

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            var logger = Mock.Of<ILogger<CompanyService>>();

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));
            CompanyOverviewModel result = await service.EditCompany(model, userAuthId);

            Assert.NotNull(result);
            Assert.Equal(model.Name, result.Name);
            Assert.Equal(0, result.EmployeeCount);
            Assert.Equal(companyId, result.Id);
        }

        [Fact(DisplayName = "EditCompanyWithAddedUsers_Pass|CompanyServiceTester")]
        public async Task EditCompanyWithAddedUsers_Pass()
        {
            Random random = new Random();
            Int32 companyId = random.Next();
            String companyname = "Testfirma";

            Int32 addedUserAmount = random.Next(10, 40);

            HashSet<Int32> addedUsers = new HashSet<int>();
            for (int i = 0; i < addedUserAmount; i++)
            {
                addedUsers.Add(random.Next());
            }

            CompanyEditModel model = new CompanyEditModel
            {
                Id = companyId,
                Name = companyname,
                Phone = "01523",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    Street = "Musterstr.",
                    PostalCode = "01574",
                    StreetNumber = "156a",
                },
                AddedEmployees = addedUsers,
            };

            IStorageContraints contraints = new BesaDataStorageContraints();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyname, companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUsersExists(addedUsers)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.AddEmployeesToCompany(addedUsers, companyId)).Returns(Task.CompletedTask);

            mockStorage.Setup(ctx => ctx.GetCompanyOverviewById(companyId)).ReturnsAsync(new CompanyOverviewModel
            {
                Name = companyname,
                Id = companyId,
                EmployeeCount = 0,
            });

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            var logger = Mock.Of<ILogger<CompanyService>>();

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));
            CompanyOverviewModel result = await service.EditCompany(model, userAuthId);

            Assert.NotNull(result);
            Assert.Equal(model.Name, result.Name);
            Assert.Equal(0, result.EmployeeCount);
            Assert.Equal(companyId, result.Id);
        }

        [Fact(DisplayName = "EditCompanyWithRemvoedUsers_Pass|CompanyServiceTester")]
        public async Task EditCompanyWithRemvoedUsers_Pass()
        {
            Random random = new Random();
            Int32 companyId = random.Next();
            String companyname = "Testfirma";

            Int32 addedUserAmount = random.Next(10, 40);
            HashSet<Int32> addedUsers = new HashSet<int>();
            for (int i = 0; i < addedUserAmount; i++)
            {
                addedUsers.Add(random.Next());
            }

            CompanyEditModel model = new CompanyEditModel
            {
                Id = companyId,
                Name = companyname,
                Phone = "01523",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    Street = "Musterstr.",
                    PostalCode = "01574",
                    StreetNumber = "156a",
                },
                RemovedEmployees = addedUsers,
            };

            IStorageContraints contraints = new BesaDataStorageContraints();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyname, companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUsersExists(addedUsers)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUsersExists(addedUsers)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUsersBelongsToCompany(companyId, addedUsers)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveEmployeesFromCompany(addedUsers)).Returns(Task.CompletedTask);

            mockStorage.Setup(ctx => ctx.EditCompany(model)).Returns(Task.CompletedTask);

            mockStorage.Setup(ctx => ctx.GetCompanyOverviewById(companyId)).ReturnsAsync(new CompanyOverviewModel
            {
                Name = companyname,
                Id = companyId,
                EmployeeCount = 0,
            });

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Update, MessageRelatedObjectTypes.Company, companyId);

            var logger = Mock.Of<ILogger<CompanyService>>();

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyOverviewModel result = await service.EditCompany(model, userAuthId);

            Assert.NotNull(result);
            Assert.Equal(model.Name, result.Name);
            Assert.Equal(0, result.EmployeeCount);
            Assert.Equal(companyId, result.Id);
        }

        [Fact(DisplayName = "EditCompany_Fail|CompanyServiceTester")]
        public async Task EditCompany_Fail()
        {
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<CompanyService>>();

            Random random = new Random();
            Int32 companyId = random.Next();

            String exisitnName = "Firma mit bekannten Namen";

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(exisitnName, companyId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException nullException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.CreateCompany(null, userAuthId));
            Assert.Equal(ComponyServiceExceptionReasons.CreateModelIsNull, nullException.Reason);

            List<CompanyEditModel> invalidModels = new List<CompanyEditModel>();
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                // Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = null,
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = new AddressModel
                {
                    // City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    // PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    // Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    // StreetNumber = "545a",
                },
                Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = "Testfirma",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                // Phone = "15454",
            });
            invalidModels.Add(new CompanyEditModel
            {
                Id = companyId,
                Name = exisitnName,
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    PostalCode = "1458",
                    Street = "Musterstr. 4",
                    StreetNumber = "545a",
                },
                // Phone = "15454",
            });

            foreach (CompanyEditModel invalidRequest in invalidModels)
            {
                CompanyServiceException invalidException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.EditCompany(invalidRequest, userAuthId));
                Assert.Equal(ComponyServiceExceptionReasons.InvalidEditModel, invalidException.Reason);
            }
        }

        [Fact(DisplayName = "EditCompany_InvalidID|CompanyServiceTester")]
        public async Task EditCompany_InvalidID()
        {
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<CompanyService>>();

            Random random = new Random();
            Int32 companyId = random.Next();

            String companyName = "Firma mit bekannten Namen";

            CompanyEditModel model = new CompanyEditModel
            {
                Id = companyId,
                Name = companyName,
                Phone = "01523",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    Street = "Musterstr.",
                    PostalCode = "01574",
                    StreetNumber = "156a",
                }
            };

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyName, companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyServiceException invalidException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.EditCompany(model, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, invalidException.Reason);
        }

        [Fact(DisplayName = "EditCompany_Fail_AddedAndRemovedContainsSameIds|CompanyServiceTester")]
        public async Task EditCompany_Fail_AddedAndRemovedContainsSameIds()
        {
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<CompanyService>>();

            Random random = new Random();
            Int32 companyId = random.Next();

            Int32 addedUserAmount = random.Next(10, 40);
            HashSet<Int32> addedUsers = new HashSet<int>();
            HashSet<Int32> removedUsers = new HashSet<int>();
            for (int i = 0; i < addedUserAmount; i++)
            {
                Int32 itemId = random.Next();
                if (random.NextDouble() > 0.5)
                {
                    removedUsers.Add(itemId);
                }
                addedUsers.Add(itemId);
            }

            String companyName = "Firma mit bekannten Namen";

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyNameExists(companyName, companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            CompanyEditModel model = new CompanyEditModel
            {
                Id = companyId,
                Name = companyName,
                Phone = "01523",
                Address = new AddressModel
                {
                    City = "Musterstadt",
                    Street = "Musterstr.",
                    PostalCode = "01574",
                    StreetNumber = "156a",
                },
                RemovedEmployees = removedUsers,
                AddedEmployees = addedUsers,
            };

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyServiceException invalidException = await Assert.ThrowsAsync<CompanyServiceException>(() => service.EditCompany(model, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.InvalidEditModel, invalidException.Reason);
        }
    }
}
