﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_GetAllCompanies : ProtocolTesterBase
    {
        [Fact]
        public async Task GetAllCompanies_Pass()
        {
            var logger = Mock.Of<ILogger<CompanyService>>();

            Random rand = new Random();
            Int32 amount = rand.Next(0, 20);
            Dictionary<Int32,CompanyOverviewModel> overview = new Dictionary<Int32,CompanyOverviewModel>();
            for (int i = 0; i < amount; i++)
            {
                overview.Add(i+1,new CompanyOverviewModel
                {
                    Id = i +1,
                    EmployeeCount = rand.Next(0,50),
                    Name = $"Testfirma {i + 1}",
                });
            }

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.GetAllCompanies()).ReturnsAsync(overview.Values);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            IEnumerable<CompanyOverviewModel> result = await service.GetAllCompanies();

            Assert.NotNull(result);
            Assert.Equal(overview.Count, result.Count());

            foreach (CompanyOverviewModel resultItem in result)
            {
                Assert.True(overview.ContainsKey(resultItem.Id));

                CompanyOverviewModel expected = overview[resultItem.Id];

                Assert.Equal(expected.Name, resultItem.Name);
                Assert.Equal(expected.EmployeeCount, resultItem.EmployeeCount);
            }
        }
    }
}
