﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_GetDetails : ProtocolTesterBase
    {
        [Fact]
        public async Task GetCompanyDetails_Pass()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            CompanyDetailModel expected = new CompanyDetailModel
            {
                Address = new AddressModel
                {
                    City = "Teststadt",
                    PostalCode = "15472",
                    Street = "Straeße",
                    StreetNumber = "5a"
                },
                Name = "Testfirma",
                Phone = "031446641",
                Id = companyId,
            };

            Int32 employeeAmount = rand.Next(3, 10);
            List<SimpleEmployeeModel> employees = new List<SimpleEmployeeModel>(employeeAmount);
            for (int i = 0; i < employeeAmount; i++)
            {
                SimpleEmployeeModel simpleEmployee = new SimpleEmployeeModel
                {
                    Id = rand.Next(),
                    Lastname = $"Vorname {rand.Next()}",
                    Surname = $"Nachanme {rand.Next()}",
                };
                employees.Add(simpleEmployee);
            }

            expected.Employees = employees;

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCompanyDetails(companyId)).ReturnsAsync(expected);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            CompanyDetailModel actual = await service.GetCompanyDetails(companyId);

            Assert.NotNull(actual);

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Phone, actual.Phone);
            Assert.Equal(expected.Address.City, actual.Address.City);
            Assert.Equal(expected.Address.PostalCode, actual.Address.PostalCode);
            Assert.Equal(expected.Address.Street, actual.Address.Street);
            Assert.Equal(expected.Address.StreetNumber, actual.Address.StreetNumber);

            Assert.NotNull(expected.Employees);
            Assert.Equal(expected.Employees.Count(), actual.Employees.Count());

            foreach (SimpleEmployeeModel exptectedEmployee in expected.Employees)
            {
                SimpleEmployeeModel actualEmployee = actual.Employees.FirstOrDefault(x => x.Id == exptectedEmployee.Id);

                Assert.NotNull(actualEmployee);

                Assert.Equal(exptectedEmployee.Id, actualEmployee.Id);
                Assert.Equal(exptectedEmployee.Surname, actualEmployee.Surname);
                Assert.Equal(exptectedEmployee.Lastname, actualEmployee.Lastname);
            }
        }

        [Fact]
        public async Task GetCompanyDetails_Fail()
        {
            Random rand = new Random();
            Int32 companyId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.GetCompanyDetails(companyId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }
    }
}
