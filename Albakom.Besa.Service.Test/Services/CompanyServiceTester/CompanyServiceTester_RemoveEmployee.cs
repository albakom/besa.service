﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class CompanyServiceTester_RemoveEmployee : ProtocolTesterBase
    {
        [Fact(DisplayName = "RemoveEmployee_Pass|CompanyServiceTester")]
        public async Task RemoveEmployee_Pass()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId,employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));
            Boolean actual = await service.RemoveEmployee(companyId, employeeId, userAuthId);
            Assert.True(actual);
        }

        [Fact(DisplayName = "RemoveEmployee_Fail_CompanyNotFoud|CompanyServiceTester")]
        public async Task RemoveEmployee_Fail_CompanyNotFoud()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.RemoveEmployee(companyId, employeeId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "RemoveEmployee_Fail_UserNotFoud|CompanyServiceTester")]
        public async Task RemoveEmployee_Fail_UserNotFoud()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.RemoveEmployee(companyId, employeeId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "RemoveEmployee_Fail_UserNotInCompany|CompanyServiceTester")]
        public async Task RemoveEmployee_Fail_UserNotInCompany()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger, base.GetProtocolService(mockStorage));

            CompanyServiceException exception = await Assert.ThrowsAsync<CompanyServiceException>(() => service.RemoveEmployee(companyId, employeeId, userAuthId));

            Assert.Equal(ComponyServiceExceptionReasons.UserNotCompanyMember, exception.Reason);
        }

        [Fact(DisplayName = "RemoveEmployee_Fail_DatabaseFailed|CompanyServiceTester")]
        public async Task RemoveEmployee_Fail_DatabaseFailed()
        {
            Random rand = new Random();

            Int32 companyId = rand.Next();
            Int32 employeeId = rand.Next();

            var logger = Mock.Of<ILogger<CompanyService>>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserBelongsToCompany(companyId, employeeId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveEmployeeFromCompany(employeeId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Delete, MessageRelatedObjectTypes.Company, companyId);

            ICompanyService service = new CompanyService(mockStorage.Object, logger,base.GetProtocolService(mockStorage));
            Boolean actual = await service.RemoveEmployee(companyId, employeeId,userAuthId);
            Assert.False(actual);
        }

    }
}
