﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_AddFlatToBuilding : ProtocolTesterBase
    {
        [Fact(DisplayName = "AddFlatToBuilding|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            FlatCreateModel createModel = new FlatCreateModel
            {
                BuildingId = buildingId,
                Description = $"Tolle Beschreibung Nr. {random.Next()}",
                Floor = $"Erdgeschoss Nr. {random.Next()}",
                Number = $"Nr. {random.Next()}",
            };

            SimpleFlatOverviewModel simpleFlatModel = new SimpleFlatOverviewModel
            {
                Id = flatId,
                BuildingId = buildingId,
                Description = createModel.Description,
                Floor = createModel.Floor,
                Number = createModel.Number,
            };

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfBuildingHasSpaceForAFlat(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CreateFlat(createModel)).ReturnsAsync(flatId);

            storage.Setup(ctx => ctx.GetFlatOverviewById(flatId)).ReturnsAsync(simpleFlatModel);
            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            SimpleFlatOverviewModel actual = await service.AddFlatToBuilding(createModel,userAuthId);

            Assert.NotNull(actual);

            Assert.Equal(simpleFlatModel.Id, actual.Id);
            Assert.Equal(simpleFlatModel.BuildingId, actual.BuildingId);
            Assert.Equal(simpleFlatModel.Floor, actual.Floor);
            Assert.Equal(simpleFlatModel.Number, actual.Number);
            Assert.Equal(simpleFlatModel.Description, actual.Description);
        }

        [Fact(DisplayName = "AddFlatToBuilding_Fail_BuildingNotFound|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding_Fail_BuildingNotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            FlatCreateModel createModel = new FlatCreateModel
            {
                BuildingId = buildingId,
                Description = $"Tolle Beschreibung Nr. {random.Next()}",
                Floor = $"Erdgeschoss Nr. {random.Next()}",
                Number = $"Nr. {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);
            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.AddFlatToBuilding(createModel, userAuthId));

            Assert.Equal(ConstructionStageServicExceptionReasons.InvalidCreateModel, exception.Reason);
        }

        [Fact(DisplayName = "AddFlatToBuilding_Fail_BuildingNotSpaceLeft|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding_Fail_BuildingNotSpaceLeft()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            FlatCreateModel createModel = new FlatCreateModel
            {
                BuildingId = buildingId,
                Description = $"Tolle Beschreibung Nr. {random.Next()}",
                Floor = $"Erdgeschoss Nr. {random.Next()}",
                Number = $"Nr. {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfBuildingHasSpaceForAFlat(buildingId)).ReturnsAsync(false);

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.AddFlatToBuilding(createModel, userAuthId));

            Assert.Equal(ConstructionStageServicExceptionReasons.InvalidCreateModel, exception.Reason);
        }

        [Fact(DisplayName = "AddFlatToBuilding_Fail_ModelIsNull|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding_Fail_ModelIsNull()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.AddFlatToBuilding(null, userAuthId));

            Assert.Equal(ConstructionStageServicExceptionReasons.ModelIsNull, exception.Reason);
        }

        [Fact(DisplayName = "AddFlatToBuilding_Fail_InvalidModel|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding_Fail_InvalidModel()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            FlatCreateModel createModel = new FlatCreateModel
            {
                BuildingId = buildingId,
                Description = $"Tolle Beschreibung Nr. {random.Next()}",
                Floor = $"Erdgeschoss Nr. {random.Next()}",
                // Number = $"Nr. {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.AddFlatToBuilding(createModel, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.InvalidCreateModel, exception.Reason);
        }

        [Fact(DisplayName = "AddFlatToBuilding_Fail_UserNotFound|ConstructionStageServiceTester")]
        public async Task AddFlatToBuilding_Fail_UserNotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatId = random.Next();

            FlatCreateModel createModel = new FlatCreateModel
            {
                BuildingId = buildingId,
                Description = $"Tolle Beschreibung Nr. {random.Next()}",
                Floor = $"Erdgeschoss Nr. {random.Next()}",
                Number = $"Nr. {random.Next()}",
            };

            SimpleFlatOverviewModel simpleFlatModel = new SimpleFlatOverviewModel
            {
                Id = flatId,
                BuildingId = buildingId,
                Description = createModel.Description,
                Floor = createModel.Floor,
                Number = createModel.Number,
            };

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfBuildingHasSpaceForAFlat(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CreateFlat(createModel)).ReturnsAsync(flatId);

            storage.Setup(ctx => ctx.GetFlatOverviewById(flatId)).ReturnsAsync(simpleFlatModel);
            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.AddFlatToBuilding, MessageRelatedObjectTypes.Flat, flatId);
            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.AddFlatToBuilding(createModel, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
