﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_AddFlatsToBuildingWithOneUnit : ProtocolTesterBase
    {
        private Boolean FlatTester(IEnumerable<FlatCreateModel> models, List<SimpleBuildingOverviewModel> expectedModels)
        {
            if (models.Count() != expectedModels.Count) { return false; }

            foreach (FlatCreateModel item in models)
            {
                SimpleBuildingOverviewModel expectedBuilding = expectedModels.FirstOrDefault(x => x.Id == item.BuildingId);
                if (expectedBuilding == null) { return false; }

                if(String.Compare(expectedBuilding.StreetName,item.Number,true) != 0) { return false; }
            }

            return true;
        }

        [Fact(DisplayName = "AddFlatsToBuildingWithOneUnit|ConstructionStageServiceTester")]
        public async Task AddFlatsToBuildingWithOneUnit()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 1000);

            List<SimpleBuildingOverviewModel> buildings = new List<SimpleBuildingOverviewModel>(buildingAmount);
            HashSet<Int32> flatIds = new HashSet<Int32>(buildingAmount);
            for (int i = 0; i < buildingAmount; i++)
            {
                SimpleBuildingOverviewModel simpleBuilding = new SimpleBuildingOverviewModel
                {
                    Id = i + 1,
                    StreetName = $"Teststraße {random.Next()}",
                };

                if (random.NextDouble() > 0.5)
                {
                    simpleBuilding.CommercialUnits = 1;
                }
                else
                {
                    simpleBuilding.HouseholdUnits = 1;
                }

                buildings.Add(simpleBuilding);

                Int32 flatId = random.Next();
                while (flatIds.Contains(flatId))
                {
                    flatId = random.Next();
                }

                flatIds.Add(flatId);
            }

            HashSet<Int32> expectedIds = buildings.Select(x => x.Id).ToHashSet();

            storage.Setup(ctx => ctx.GetBuildingsWithOneUnitAndWithoutFlats()).ReturnsAsync(buildings);
            storage.Setup(ctx => ctx.CreateFlats(It.Is<IEnumerable<FlatCreateModel>>((input) => FlatTester(input, buildings)))).ReturnsAsync(flatIds);
            storage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x => x.Level == ProtocolLevels.Sucesss && x.Action == MessageActions.AddFlatsToBuildingWithOneUnit))).ReturnsAsync(random.Next());

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<Int32> actual =  await service.AddFlatsToBuildingWithOneUnit();

            Assert.NotNull(actual);

            Assert.Equal(flatIds.Count, actual.Count());

            foreach (Int32 flatId in actual)
            {
                Assert.True(flatIds.Contains(flatId) == true);
            }
        }

        [Fact(DisplayName = "AddFlatsToBuildingWithOneUnit_EmptyResult|ConstructionStageServiceTester")]
        public async Task AddFlatsToBuildingWithOneUnit_EmptyResult()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingAmount = random.Next(30, 1000);

            storage.Setup(ctx => ctx.GetBuildingsWithOneUnitAndWithoutFlats()).ReturnsAsync(new List<SimpleBuildingOverviewModel>());
            storage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x => x.Level == ProtocolLevels.Warning && x.Action == MessageActions.AddFlatsToBuildingWithOneUnit))).ReturnsAsync(random.Next());

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<Int32> actual = await service.AddFlatsToBuildingWithOneUnit();

            Assert.NotNull(actual);
            Assert.True(actual.Count() == 0);
        }
    }
}
