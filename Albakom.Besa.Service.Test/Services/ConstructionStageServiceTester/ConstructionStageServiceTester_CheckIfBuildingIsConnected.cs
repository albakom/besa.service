﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_CheckIfBuildingIsConnected : ProtocolTesterBase
    {
        [Fact(DisplayName = "CheckIfBuildingIsConnected_Pass|ConstructionStageServiceTester")]
        public async Task CheckIfBuildingIsConnected_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();

            Boolean result = random.NextDouble() > 0.5;

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(result);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean actual = await service.CheckIfBuildingIsConnected(buildingId);

            Assert.Equal(result,actual);
        }

        [Fact(DisplayName = "CheckIfBuildingIsConnected_Fail_BuildingNotFound|ConstructionStageServiceTester")]
        public async Task CheckIfBuildingIsConnected_Fail_BuildingNotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.CheckIfBuildingIsConnected(buildingId));

            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
