﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_CreateBranchOffJob : ProtocolTesterBase
    {

        private Boolean JobTestester(IEnumerable<BranchOffJobCreateModel> models, HashSet<Int32> expectedIds)
        {
            HashSet<Int32> usedIds = models.Select(x => x.BuildingId).ToHashSet();

            if (usedIds.Count != expectedIds.Count) { return false; }

            foreach (Int32 item in usedIds)
            {
                if (expectedIds.Contains(item) == false) { return false; }
            }

            return true;
        }

        [Fact(DisplayName = "CreateBranchOffJob_Pass|ConstructionStageServiceTester")]
        public async Task CreateBranchOffJob_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 constructionStageId = random.Next();

            Int32 buildingAmount = random.Next(10, 100);

            HashSet<Int32> buildingIds = new HashSet<int>();

            for (int i = 0; i < buildingAmount; i++)
            {
                buildingIds.Add(random.Next());
            }

            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(constructionStageId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingIdsWithoutBranchOffJob(constructionStageId)).ReturnsAsync(buildingIds);
            storage.Setup(ctx => ctx.CreateBranchOffJobs(It.Is<IEnumerable<BranchOffJobCreateModel>>((input) => JobTestester(input, buildingIds)))).Returns(Task.CompletedTask);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateBranchOffJobs, MessageRelatedObjectTypes.BranchOffJob);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.CreateBranchOffJob(constructionStageId,userAuthId);
        }

        [Fact(DisplayName = "CreateBranchOffJob_Fail|ConstructionStageServiceTester")]
        public async Task CreateBranchOffJob_Fail()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 constructionStageId = random.Next();

            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(constructionStageId)).ReturnsAsync(false);
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateBranchOffJobs, MessageRelatedObjectTypes.BranchOffJob);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.CreateBranchOffJob(constructionStageId, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateBranchOffJob_WithNoBulidngs_Pass|ConstructionStageServiceTester")]
        public async Task CreateBranchOffJob_WithNoBulidngs_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 constructionStageId = random.Next();

            HashSet<Int32> buildingIds = new HashSet<int>();

            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(constructionStageId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingIdsWithoutBranchOffJob(constructionStageId)).ReturnsAsync(buildingIds);
            storage.Setup(ctx => ctx.CreateBranchOffJobs(It.Is<IEnumerable<BranchOffJobCreateModel>>((input) => JobTestester(input, buildingIds)))).Returns(Task.CompletedTask);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateBranchOffJobs, MessageRelatedObjectTypes.BranchOffJob);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.CreateBranchOffJob(constructionStageId, userAuthId);
        }
    }
}
