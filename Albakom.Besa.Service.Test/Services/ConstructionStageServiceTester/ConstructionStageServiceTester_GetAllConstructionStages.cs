﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetAllConstructionStages : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetAllConstructionStages_Pass|ConstructionStageServiceTester")]
        public async Task GetAllConstructionStages_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var projectAdapter = Mock.Of<IProjectAdapter>();

            Random rand = new Random();
            Int32 amount = rand.Next(0, 20);
            Dictionary<Int32, ConstructionStageOverviewModel> overview = new Dictionary<Int32, ConstructionStageOverviewModel>();
            for (int i = 0; i < amount; i++)
            {
                overview.Add(i + 1, new ConstructionStageOverviewModel
                {
                    Id = i + 1,
                    HouseholdsUnits = rand.Next(15, 5000),
                    CommercialUnits = rand.Next(50,1000),
                    Name = $"Testfirma {i + 1}",
                    CabinetAmount = rand.Next(30,100),
                    SleeveAmount = rand.Next(3,10),
                    DoingPercentage = rand.Next(30,60) + rand.NextDouble(),
                });
            }

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.GetAllConstructionStages()).ReturnsAsync(overview.Values);

            IConstructionStageService service = new ConstructionStageService(mockStorage.Object, projectAdapter, logger,base.GetProtocolService(mockStorage), Mock.Of<IUpdateTaskService>());
            IEnumerable<ConstructionStageOverviewModel> result = await service.GetAllConstructionStages();

            Assert.NotNull(result);
            Assert.Equal(overview.Count, result.Count());

            foreach (ConstructionStageOverviewModel resultItem in result)
            {
                Assert.True(overview.ContainsKey(resultItem.Id));

                ConstructionStageOverviewModel expected = overview[resultItem.Id];

                Assert.Equal(expected.Id, resultItem.Id);
                Assert.Equal(expected.HouseholdsUnits, resultItem.HouseholdsUnits);
                Assert.Equal(expected.CommercialUnits, resultItem.CommercialUnits);
                Assert.Equal(expected.Name, resultItem.Name);
                Assert.Equal(expected.CabinetAmount, resultItem.CabinetAmount);
                Assert.Equal(expected.SleeveAmount, resultItem.SleeveAmount);
                Assert.Equal(expected.DoingPercentage, resultItem.DoingPercentage);
            }
        }
    }

}
