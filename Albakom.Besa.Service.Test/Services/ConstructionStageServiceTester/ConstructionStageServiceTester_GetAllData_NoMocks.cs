﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetAllData_NoMocks : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetAllData|ConstructionStageServiceTester")]
        public async Task GetAllData()
        {
            IBesaDataStorage storage = Mock.Of<IBesaDataStorage>();
            ILogger<ConstructionStageService> logger = Mock.Of<ILogger<ConstructionStageService>>();
            ILogger<HttpBasedProjectAdapter> secondLogger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();

            String serviceUrl = "http://localhost:52819/";
            IHttpHandler handler = new HttpClientBasedHttpHandler();
            ITokenClient tokenClient = new SimpleTokenClient(new TokenClientConfig
            {
                ClientId = "besa-server-tester",
                ClientSecret = "sicheresPassw0rd",
                Scope = "tki-adapter-test",
                AuthUrl =  "http://localhost:5000",
            });

            IProjectAdapter projectAdapter = new HttpBasedProjectAdapter(serviceUrl, handler, tokenClient, secondLogger);
            IConstructionStageService service = new ConstructionStageService(storage, projectAdapter, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            var result = await service.GetDataFromAdapter(false,String.Empty);
        }

    }
}
