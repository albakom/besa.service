﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetBuildingsByBranchable : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetBuildingsByBranchable_Pass|ConstructionStageServiceTester")]
        public async Task GetBuildingsByBranchable_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>();

            Random random = new Random();
            Int32 branchableId = random.Next();

            Int32 buildingAmount = random.Next(10, 100);

            List<SimpleBuildingOverviewModel> buildings = new List<SimpleBuildingOverviewModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                SimpleBuildingOverviewModel model = new SimpleBuildingOverviewModel
                {
                    Id = i,
                    CommercialUnits = random.Next(3, 10),
                    HouseholdUnits = random.Next(3, 10),
                    StreetName = $"Straßenzug {random.Next()}",
                };

                buildings.Add(model);
            }

            storage.Setup(ctx => ctx.CheckIfBranchableExists(branchableId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingsByBranchable(branchableId)).ReturnsAsync(buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<SimpleBuildingOverviewModel> result = await service.GetBuildingsByBranchable(branchableId);

            Assert.NotNull(result);
            Assert.Equal(buildings.Count, result.Count());

            foreach (SimpleBuildingOverviewModel actual in result)
            {
                SimpleBuildingOverviewModel expected = buildings.FirstOrDefault(x => x.Id == actual.Id);

                Assert.NotNull(expected);
                Assert.Equal(expected.Id, actual.Id);
                Assert.Equal(expected.StreetName, actual.StreetName);
                Assert.Equal(expected.CommercialUnits, actual.CommercialUnits);
                Assert.Equal(expected.HouseholdUnits, actual.HouseholdUnits);
            }
        }

        [Fact(DisplayName = "GetBuildingsByBranchable_Fail|ConstructionStageServiceTester")]
        public async Task GetBuildingsByBranchable_Fail()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = Mock.Of<IProjectAdapter>();

            Random random = new Random();
            Int32 branchableId = random.Next();

            var storage = new Mock<IBesaDataStorage>();

            storage.Setup(ctx => ctx.CheckIfBranchableExists(branchableId)).ReturnsAsync(false);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.GetBuildingsByBranchable(branchableId));
            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
