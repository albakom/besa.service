﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetDataFromAdapter : ProtocolTesterBase
    {
        private List<ProjectAdapterBuildingModel> GenerateBuilding(Random random, Int32 amount)
        {
            List<ProjectAdapterBuildingModel> buildings = new List<ProjectAdapterBuildingModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterBuildingModel buildingModel = new ProjectAdapterBuildingModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    CommercialUnits = random.Next(0, 5),
                    HouseholdUnits = random.Next(0, 5),
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = random.Next(40, 50) + random.NextDouble(),
                        Longitude = random.Next(40, 50) + random.NextDouble(),
                    },
                    Street = $"Straße Nr {random.Next()}",
                };
                buildings.Add(buildingModel);
            }

            return buildings;
        }

        private List<ProjectAdapterCabinetModel> GenerateCabinents(Random random, Int32 amount)
        {
            List<ProjectAdapterCabinetModel> items = new List<ProjectAdapterCabinetModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterCabinetModel item = new ProjectAdapterCabinetModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = random.Next(40, 50) + random.NextDouble(),
                        Longitude = random.Next(40, 50) + random.NextDouble(),
                    },
                };
                items.Add(item);
            }

            return items;
        }

        private List<ProjectAdapterSleeveModel> GenerateSleeves(Random random, Int32 amount)
        {
            List<ProjectAdapterSleeveModel> items = new List<ProjectAdapterSleeveModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterSleeveModel item = new ProjectAdapterSleeveModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = random.Next(40, 50) + random.NextDouble(),
                        Longitude = random.Next(40, 50) + random.NextDouble(),
                    },
                };
                items.Add(item);
            }

            return items;
        }

        [Fact(DisplayName = "GetDataFromAdapter_NoForce_Pass|ConstructionStageServiceTester")]
        public async Task GetDataFromAdapter_NoForce_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>();

            Random random = new Random();

            Int32 buildAmount = random.Next(10, 100);
            Int32 cabinentAmount = random.Next(10, 100);
            Int32 sleeveAmount = random.Next(10, 100);

            List<ProjectAdapterCabinetModel> cabinents = GenerateCabinents(random, buildAmount);
            List<ProjectAdapterSleeveModel> sleeves = GenerateSleeves(random, buildAmount);

            adapter.Setup(adp => adp.GetAllCabinets()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>(cabinents));
            adapter.Setup(adp => adp.GetAllSleeves()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>(sleeves));

            storage.Setup(ctx => ctx.GetBuildingAmount()).ReturnsAsync(0);
            storage.Setup(ctx => ctx.CreateCabinets(cabinents)).ReturnsAsync(cabinentAmount);
            storage.Setup(ctx => ctx.CreateSleeves(sleeves)).ReturnsAsync(sleeveAmount);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateFromAdapter, MessageRelatedObjectTypes.NotSpecified);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            GetDataFromAdapterResult result = await service.GetDataFromAdapter(false,userAuthId);

            Assert.NotNull(result);
            Assert.Equal(cabinentAmount, result.CabinentAmount);
            Assert.Equal(sleeveAmount, result.SleevesAmount);
        }

        [Fact(DisplayName = "GetDataFromAdapter_Fail_InvalidState|ConstructionStageServiceTester")]
        public async Task GetDataFromAdapter_Fail_InvalidState()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = Mock.Of<IProjectAdapter>();

            var storage = new Mock<IBesaDataStorage>();
            Random random = new Random();

            storage.Setup(ctx => ctx.GetBuildingAmount()).ReturnsAsync(12);
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateFromAdapter, MessageRelatedObjectTypes.NotSpecified);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.GetDataFromAdapter(false,userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.DataUpdateInvalid, exception.Reason);
        }

        [Fact(DisplayName = "GetDataFromAdapter_Fail_InvalidAdapterResponse|ConstructionStageServiceTester")]
        public async Task GetDataFromAdapter_Fail_InvalidAdapterResponse()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>();

            Random random = new Random();

            Int32 buildAmount = random.Next(10, 100);
            Int32 cabinentAmount = random.Next(10, 100);
            Int32 sleeveAmount = random.Next(10, 100);

            List<ProjectAdapterBuildingModel> buildings = GenerateBuilding(random, buildAmount);
            List<ProjectAdapterCabinetModel> cabinents = GenerateCabinents(random, buildAmount);
            List<ProjectAdapterSleeveModel> sleeves = GenerateSleeves(random, buildAmount);

            adapter.Setup(adp => adp.GetAllCabinets()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>(cabinents));
            adapter.Setup(adp => adp.GetAllSleeves()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>(sleeves));

            storage.Setup(ctx => ctx.GetBuildingAmount()).ReturnsAsync(0);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateFromAdapter, MessageRelatedObjectTypes.NotSpecified);


            for (int i = 0; i < 2; i++)
            {
                switch (i)
                {
                    case 0:
                        adapter.Setup(adp => adp.GetAllCabinets()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>(ServiceErrros.NotFound));
                        break;
                    case 1:
                        adapter.Setup(adp => adp.GetAllSleeves()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>(ServiceErrros.Unvaible));
                        break;
                    default:
                        break;


                }
                IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

                ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.GetDataFromAdapter(false, userAuthId));
                Assert.Equal(ConstructionStageServicExceptionReasons.InvalidDataFromAdapter, exception.Reason);
            }


        }
    }
}
