﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetDetailsForEdit : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetDetailsForEdit_Pass|ConstructionStageServiceTester")]
        public async Task GetDetailsForEdit_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();

            ConstructionStageDetailsForEditModel expected = new ConstructionStageDetailsForEditModel
            {
                Id = id,
            };

            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetDetailsForEdit(id)).ReturnsAsync(expected);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageDetailsForEditModel actual = await service.GetDetailsForEdit(id);

            Assert.NotNull(actual);
            Assert.Equal(expected.Id, actual.Id);
        }

        [Fact(DisplayName = "GetDetailsForEdit_Fail_NotFound|ConstructionStageServiceTester")]
        public async Task GetDetailsForEdit_Fail_NotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();

            ConstructionStageDetailsForEditModel expected = new ConstructionStageDetailsForEditModel
            {
                Id = id,
            };

            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.GetDetailsForEdit(id)).ReturnsAsync(expected);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.GetDetailsForEdit(id));
            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
