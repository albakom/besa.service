﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetFlatsByBuildingId : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetFlatsByBuildingId_Pass|ConstructionStageServiceTester")]
        public async Task GetFlatsByBuildingId_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();
            Int32 flatAmount = random.Next(3, 10);

            List<SimpleFlatOverviewModel> flats = new List<SimpleFlatOverviewModel>();

            for (int i = 0; i < flatAmount; i++)
            {
                SimpleFlatOverviewModel flat = new SimpleFlatOverviewModel
                {
                    BuildingId = buildingId,
                    Description = $"Beschreibung Nr {random.Next()}",
                    Floor = $"Ergeschoss Nr. { random.Next() }",
                    Id = i + 1,
                    Number = $"Wohnung Nr {random.Next()}",
                };

                flats.Add(flat);
            }

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(buildingId)).ReturnsAsync(flats);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<SimpleFlatOverviewModel> actual = await service.GetFlatsByBuildingId(buildingId);

            Assert.NotNull(actual);
            Assert.Equal(flats.Count, actual.Count());

            foreach (SimpleFlatOverviewModel acutalItem in actual)
            {
                Assert.NotNull(acutalItem);

                SimpleFlatOverviewModel expectedItem = flats.FirstOrDefault(x => x.Id == acutalItem.Id);
                Assert.NotNull(expectedItem);

                Assert.Equal(acutalItem.Id, expectedItem.Id);
                Assert.Equal(acutalItem.Floor, expectedItem.Floor);
                Assert.Equal(acutalItem.Description, expectedItem.Description);
                Assert.Equal(acutalItem.Number, expectedItem.Number);
            }
        }

        [Fact(DisplayName = "GetFlatsByBuildingId_Pass_Fail_BuildingNotFound|ConstructionStageServiceTester")]
        public async Task GetFlatsByBuildingId_Pass_Fail_BuildingNotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.GetFlatsByBuildingId(buildingId));

            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
