﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetMainCables : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetMainCables|ConstructionStageServiceTester")]
        public async Task GetMainCables()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 amount = random.Next(10, 100);

            List<MainCableOverviewModel> expectedResult = new List<MainCableOverviewModel>(amount);

            for (int i = 0; i < amount; i++)
            {
                MainCableOverviewModel overview = new MainCableOverviewModel
                {
                    Id = random.Next(),
                    JobExists = random.NextDouble() > 0.5,
                    Name = $"Testname {random.Next()}",
                };

                expectedResult.Add(overview);
            }

            storage.Setup(ctx => ctx.GetMainCableWithJobAssocatiation()).ReturnsAsync(expectedResult);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<MainCableOverviewModel> actual = await service.GetMainCables();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (MainCableOverviewModel actualItem in actual)
            {
                MainCableOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.Id == actualItem.Id);

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);
                Assert.Equal(expectedItem.JobExists, actualItem.JobExists);
            }
        }
    }
}
