﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_GetOverviewForCilivWork : ProtocolTesterBase
    { 
        [Fact]
        public async Task GetOverviewForCilivWork()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 branchableId = random.Next();

            Int32 constructionStageAmount = random.Next(10, 100);

            List<ConstructionStageOverviewForJobs> expectedResult = new List<ConstructionStageOverviewForJobs>(constructionStageAmount);

            for (int i = 0; i < constructionStageAmount; i++)
            {
                ConstructionStageOverviewForJobs overview = new ConstructionStageOverviewForJobs
                {
                    DoingPercentage = random.Next(10, 100) + random.NextDouble(),
                    Name = $"Bauabschnitt Nr {i + 1}",
                    Id = i + 1,
                };

                Int32 branchableAmount = random.Next(3, 10);
                List<SimpleCabinetOverviewModel> branchables = new List<SimpleCabinetOverviewModel>(branchableAmount);
                for (int j = 0; j < branchableAmount; j++)
                {
                    SimpleCabinetOverviewModel simpleCabinet = new SimpleCabinetOverviewModel
                    {
                        Id = j + 1,
                        Name = $"KVZ Nr. {j+1}",
                    };

                    branchables.Add(simpleCabinet);
                }

                overview.Branchables = branchables;
            }


            storage.Setup(ctx => ctx.GetConstructionStageOverviewForCilivWork(false)).ReturnsAsync(expectedResult);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            IEnumerable<ConstructionStageOverviewForJobs> actual = await service.GetOverviewForCilivWork(false);

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (ConstructionStageOverviewForJobs actualItem in actual)
            {
                ConstructionStageOverviewForJobs expectedItem = expectedResult.FirstOrDefault(x => x.Id == actualItem.Id);

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.Name, actualItem.Name);
                Assert.Equal(expectedItem.DoingPercentage, actualItem.DoingPercentage);

                Assert.Equal(expectedItem.Branchables.Count(), actualItem.Branchables.Count());

                foreach (SimpleCabinetOverviewModel actualCabinet in actualItem.Branchables)
                {
                    SimpleCabinetOverviewModel expetedCabinet = expectedItem.Branchables.FirstOrDefault(x => x.Id == actualCabinet.Id);

                    Assert.NotNull(expetedCabinet);

                    Assert.Equal(expetedCabinet.Id, actualCabinet.Id);
                    Assert.Equal(expetedCabinet.Name, actualCabinet.Name);
                }
            }
        }
    }
}
