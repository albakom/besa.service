﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_SetCableToFlatsWithOneBuildingUnit : ProtocolTesterBase
    {
        [Fact(DisplayName = "SetCableToFlatsWithOneBuildingUnit|ConstructionStageServiceTester")]
        public async Task SetCableToFlatsWithOneBuildingUnit()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 amount = random.Next(10, 100);

            HashSet<Int32> buildingIds = new HashSet<Int32>(amount);

            for (int i = 0; i < amount; i++)
            {
                buildingIds.Add(random.Next());
            }

            storage.Setup(ctx => ctx.GetBuildingsWithOneUnit()).ReturnsAsync(buildingIds);
            storage.Setup(ctx => ctx.SetCableToFlatByBuildingId(It.Is<Int32>((x) => buildingIds.Contains(x)))).ReturnsAsync(true);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.SetCableToFlatsWithOneBuildingUnit();
         
        }
    }
}
