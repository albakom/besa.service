﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateAllCableTypesFromAdapter : ProtocolTesterBase
    { 
        [Fact(DisplayName = "UpdateAllCableTypesFromAdapter_WithoutDelete_Pass|ConstructionStageServiceTester")]
        public async Task UpdateAllCableTypesFromAdapter_WithoutDelete_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 cableTypeAmount = random.Next(4, 10);
            List<ProjectAdapterCableTypeModel> cables = new List<ProjectAdapterCableTypeModel>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                ProjectAdapterCableTypeModel cable = new ProjectAdapterCableTypeModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    FiberAmount = random.Next(3, 100),
                    FiberPerBundle = random.Next(4, 10),
                    Name = $"Tolles Kabel {random.Next()}",
                };

                cables.Add(cable);
            }

            List<ProjectAdapterCableTypeModel> cablesToEdit = new List<ProjectAdapterCableTypeModel>();
            List<ProjectAdapterCableTypeModel> cablesToAdd = new List<ProjectAdapterCableTypeModel>(cables);

            Int32 existingAmount = random.Next(1, cableTypeAmount / 2);

            for (int i = 0; i < cableTypeAmount; i++)
            {
                ProjectAdapterCableTypeModel cableToEdit = cablesToAdd[random.Next(0, cablesToAdd.Count)];
                cablesToEdit.Add(cableToEdit);
                cablesToAdd.Remove(cableToEdit);
            }

            adapter.Setup(adp => adp.GetAllCableTypes()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>>(cables));
            storage.Setup(ctx => ctx.GetAllCableProjectAdapterIds()).ReturnsAsync(cablesToEdit.Select(x => x.AdapterId).ToList());
            storage.Setup(ctx => ctx.UpdateCableType(It.Is<ProjectAdapterCableTypeModel>((input) => cablesToEdit.Contains(input)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.AddCableType(It.Is<ProjectAdapterCableTypeModel>((input) => cablesToAdd.Contains(input)))).ReturnsAsync(random.Next());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateAllCableTypesFromAdapter, MessageRelatedObjectTypes.FiberCableType);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdateAllCableTypesFromAdapter(userAuthId);
        }

        [Fact(DisplayName = "UpdateAllCableTypesFromAdapter_WithDelete_Pass|ConstructionStageServiceTester")]
        public async Task UpdateAllCableTypesFromAdapter_WithDelete_Pass()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 cableTypeAmount = random.Next(4, 10);
            List<ProjectAdapterCableTypeModel> cables = new List<ProjectAdapterCableTypeModel>(cableTypeAmount);
            for (int i = 0; i < cableTypeAmount; i++)
            {
                ProjectAdapterCableTypeModel cable = new ProjectAdapterCableTypeModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    FiberAmount = random.Next(3, 100),
                    FiberPerBundle = random.Next(4, 10),
                    Name = $"Tolles Kabel {random.Next()}",
                };

                cables.Add(cable);
            }

            HashSet<String> adapterIdsToDelete = new HashSet<string>();
            Int32 deleteAmount = random.Next(3, 10);
            for (int i = 0; i < deleteAmount; i++)
            {
                adapterIdsToDelete.Add(Guid.NewGuid().ToString());
            }

            List<ProjectAdapterCableTypeModel> cablesToEdit = new List<ProjectAdapterCableTypeModel>();
            List<ProjectAdapterCableTypeModel> cablesToAdd = new List<ProjectAdapterCableTypeModel>(cables);

            Int32 existingAmount = random.Next(1, cableTypeAmount / 2);

            for (int i = 0; i < cableTypeAmount; i++)
            {
                ProjectAdapterCableTypeModel cableToEdit = cablesToAdd[random.Next(0, cablesToAdd.Count)];
                cablesToEdit.Add(cableToEdit);
                cablesToAdd.Remove(cableToEdit);
            }

            adapter.Setup(adp => adp.GetAllCableTypes()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>>(cables));
            storage.Setup(ctx => ctx.GetAllCableProjectAdapterIds()).ReturnsAsync(cablesToEdit.Select(x => x.AdapterId).Union(adapterIdsToDelete).ToList());
            storage.Setup(ctx => ctx.UpdateCableType(It.Is<ProjectAdapterCableTypeModel>((input) => cablesToEdit.Contains(input)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.AddCableType(It.Is<ProjectAdapterCableTypeModel>((input) => cablesToAdd.Contains(input)))).ReturnsAsync(random.Next());
            storage.Setup(ctx => ctx.DeleteCableTypes(adapterIdsToDelete)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateAllCableTypesFromAdapter, MessageRelatedObjectTypes.FiberCableType);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdateAllCableTypesFromAdapter(userAuthId);
        }

        [Fact(DisplayName = "UpdateAllCableTypesFromAdapter_Fail_InvalidDataFromAdapter|ConstructionStageServiceTester")]
        public async Task UpdateAllCableTypesFromAdapter_Fail_InvalidDataFromAdapter()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage =  new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetAllCableTypes()).ReturnsAsync(new ServiceResult<IEnumerable<ProjectAdapterCableTypeModel>>(ServiceErrros.NotFound));

            String userAuthId = base.PrepareStorage(new Random(), storage, MessageActions.UpdateAllCableTypesFromAdapter, MessageRelatedObjectTypes.FiberCableType);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.UpdateAllCableTypesFromAdapter(userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.InvalidDataFromAdapter, exception.Reason);
        }
    }
}
