﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateBuildigsFromAdapter : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdateBuildigsFromAdapter|ConstructionStageServiceTester")]
        public async Task UpdateBuildigsFromAdapter()
        {
            Random random = new Random();

            Int32 branchableAmount = random.Next(10, 30);
            Dictionary<String, Int32> storedBranchableIds = new Dictionary<string, int>(branchableAmount);
            for (int i = 0; i < branchableAmount; i++)
            {
                storedBranchableIds.Add(Guid.NewGuid().ToString(), i);
            }

            Dictionary<String, Int32> storedBuildingIds = new Dictionary<string, int>();

            List<ProjectAdapterCabinetModel> adapterBranchables = new List<ProjectAdapterCabinetModel>();

            Dictionary<String, ProjectAdapterCabinetModel> branchableToUpdate = new Dictionary<string, ProjectAdapterCabinetModel>();
            Dictionary<String, ProjectAdapterCabinetModel> branchableToAdd = new Dictionary<string, ProjectAdapterCabinetModel>();
            Dictionary<String, Int32> branchableCreateIds = new Dictionary<string, int>();
            List<Int32> branchableToDelete = new List<Int32>();

            Dictionary<String, ProjectAdapterBuildingModel> buildingsToUpdate = new Dictionary<string, ProjectAdapterBuildingModel>();
            Dictionary<String, ProjectAdapterBuildingModel> buildingsToAdd = new Dictionary<string, ProjectAdapterBuildingModel>();
            Dictionary<String, Int32> buildingCreateIds = new Dictionary<string, int>();

            List<Int32> buildingsToDelete = new List<Int32>();

            Int32 buildingId = storedBuildingIds.Count;
            Int32 currentBranchableId = storedBranchableIds.Count;
            foreach (KeyValuePair<String, Int32> item in storedBranchableIds)
            {
                Double randomValue = random.NextDouble();
                ProjectAdapterCabinetModel adapterModel = null;
                if (randomValue < 0.3)
                {
                    String adapterId = Guid.NewGuid().ToString();
                    adapterModel = new ProjectAdapterCabinetModel
                    {
                        AdapterId = adapterId,
                        Coordinate = new GPSCoordinate { Latitude = random.Next(10, 20) + random.NextDouble(), Longitude = random.Next(10, 20) + random.NextDouble() },
                        Name = $"kvz Nr {random.Next()}",
                    };

                    adapterBranchables.Add(adapterModel);
                    branchableToAdd.Add(adapterId, adapterModel);
                    currentBranchableId += 1;
                    branchableCreateIds.Add(adapterId, currentBranchableId);

                    branchableToDelete.Add(item.Value);
                }
                else if (randomValue > 0.6)
                {
                    adapterModel = new ProjectAdapterCabinetModel
                    {
                        AdapterId = item.Key,
                        Coordinate = new GPSCoordinate { Latitude = random.Next(10, 20) + random.NextDouble(), Longitude = random.Next(10, 20) + random.NextDouble() },
                        Name = $"kvz Nr {random.Next()}",
                    };

                    adapterBranchables.Add(adapterModel);
                    branchableToUpdate.Add(item.Key, adapterModel);
                }
                else
                {
                    branchableToDelete.Add(item.Value);
                }

                if (adapterModel == null) continue;

                List<ProjectAdapterBuildingModel> buildings = new List<ProjectAdapterBuildingModel>();
                adapterModel.Buildings = buildings;

                Int32 buildingAmount = random.Next(10, 100);
                for (int i = 0; i < buildingAmount; i++)
                {
                    buildingId += 1;

                    Double nextRandomValue = random.NextDouble();

                    String buildingAdapterId = Guid.NewGuid().ToString();
                    ProjectAdapterBuildingModel model = new ProjectAdapterBuildingModel
                    {
                        AdapterId = buildingAdapterId,
                        CommercialUnits = random.Next(0, 5),
                        HouseholdUnits = random.Next(0, 5),
                        Coordinate = new GPSCoordinate
                        {
                            Latitude = random.Next(40, 50) + random.NextDouble(),
                            Longitude = random.Next(40, 50) + random.NextDouble(),
                        },
                        Street = $"Straße Nr {random.Next()}",
                    };

                    if (randomValue < 0.3)
                    {
                        buildings.Add(model);
                        buildingsToAdd.Add(buildingAdapterId, model);
                        buildingCreateIds.Add(buildingAdapterId, buildingId);
                    }
                    else if (nextRandomValue > 0.6)
                    {
                        buildings.Add(model);
                        storedBuildingIds.Add(buildingAdapterId, buildingId);
                        buildingsToUpdate.Add(buildingAdapterId, model);
                    }
                    else
                    {
                        storedBuildingIds.Add(buildingAdapterId, buildingId);
                        buildingsToDelete.Add(buildingId);
                    }
                }
            }

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> cabinetServiceResult = new ServiceResult<IEnumerable<ProjectAdapterCabinetModel>>(adapterBranchables);
            ServiceResult<IEnumerable<ProjectAdapterSleeveModel>> sleeveServiceResult = new ServiceResult<IEnumerable<ProjectAdapterSleeveModel>>(new List<ProjectAdapterSleeveModel>());

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetAllCabinets()).ReturnsAsync(cabinetServiceResult);
            adapter.Setup(adp => adp.GetAllSleeves()).ReturnsAsync(sleeveServiceResult);
            adapter.Setup(adp => adp.GetBuildingCableInfo(It.IsAny<String>())).ReturnsAsync(new ServiceResult<ProjectAdapterUpdateBuldingCableModel>(ServiceErrros.UnknowError));

            storage.Setup(ctx => ctx.GetBuildingIdsAndProjectAdapterIds()).ReturnsAsync(storedBuildingIds);
            storage.Setup(ctx => ctx.GetBranchablesIdsAndProjectAdapterIds()).ReturnsAsync(storedBranchableIds);
            storage.Setup(ctx => ctx.CreateSleeve(It.IsAny<ProjectAdapterSleeveModel>())).ReturnsAsync(-1);

            storage.Setup(ctx => ctx.CreateCabinet(
                It.Is<ProjectAdapterCabinetModel>(x => branchableToAdd.ContainsKey(x.AdapterId))))
                .ReturnsAsync<ProjectAdapterCabinetModel, IBesaDataStorage, Int32>(x => branchableCreateIds[x.AdapterId]);

            storage.Setup(ctx => ctx.UpdateBranchableFromAdapter(It.Is<Int32>(x => storedBranchableIds.Values.Contains(x)),
                It.Is<ProjectAdapterBranchableModel>(x => branchableToUpdate.ContainsKey(x.AdapterId)))
                ).ReturnsAsync(true);

            storage.Setup(ctx => ctx.UpdateBuildingFromAdapter(
                It.Is<Int32>(x => storedBuildingIds.Values.Contains(x)),
                It.Is<ProjectAdapterBuildingModel>(x => buildingsToUpdate.ContainsKey(x.AdapterId)),
                It.Is<Int32>(x => storedBranchableIds.Values.Contains(x) || branchableCreateIds.Values.Contains(x))))
                .ReturnsAsync(true);

            storage.Setup(ctx => ctx.CreateBuilding(
              It.Is<ProjectAdapterBuildingModel>(x => buildingsToAdd.ContainsKey(x.AdapterId)),
              It.Is<Int32>(x => storedBranchableIds.Values.Contains(x) || branchableCreateIds.Values.Contains(x))))
              .ReturnsAsync<ProjectAdapterBuildingModel, Int32, IBesaDataStorage, Int32>((x, brachableId) => buildingCreateIds[x.AdapterId]);

            storage.Setup(ctx => ctx.DeleteBuilding(It.Is<Int32>(x => buildingsToDelete.Contains(x)))
    ).ReturnsAsync(true);

            storage.Setup(ctx => ctx.DeleteBranchable(It.Is<Int32>(x => branchableToDelete.Contains(x)))
).ReturnsAsync(true);


            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateBuildigsFromAdapter, MessageRelatedObjectTypes.Buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdateBuildigsFromAdapter(userAuthId);
        }
    }
}
