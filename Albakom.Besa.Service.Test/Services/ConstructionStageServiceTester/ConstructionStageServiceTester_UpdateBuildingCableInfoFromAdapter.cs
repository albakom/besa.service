﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateBuildingCableInfoFromAdapter : ProtocolTesterBase
    {
        private Boolean CheckIfExistisAndRemove<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
        {
            Boolean exists = dictionary.ContainsKey(key);
            if (exists == true)
            {
                dictionary.Remove(key);
            }
            return exists;
        }

        [Fact(DisplayName = "UpdateBuildingCableInfoFromAdapter_Pass|ConstructionStageServiceTester")]
        public async Task UpdateBuildingCableInfoFromAdapter_Pass()
        {
            ILogger<ConstructionStageService> logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();

            Int32 buildingAmount = random.Next(4, 100);
            List<String> buldingProjectAdapterIDs = new List<String>();

            for (int i = 0; i < buildingAmount; i++)
            {
                buldingProjectAdapterIDs.Add(Guid.NewGuid().ToString());
            }

            storage.Setup(ctx => ctx.GetBuildingProjectAdapterIds()).ReturnsAsync(buldingProjectAdapterIDs);


            Dictionary<string, ProjectAdapterUpdateBuldingCableModel> dict = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                String adapterId = buldingProjectAdapterIDs[i];
                ProjectAdapterUpdateBuldingCableModel buildingToUpdate = new ProjectAdapterUpdateBuldingCableModel
                {
                    AdapterId = adapterId,
                    AdapterCableTypeId = Guid.NewGuid().ToString(),
                    Length = random.Next(200, 300) + random.NextDouble()
                };

                dict.Add(adapterId, buildingToUpdate);
            }



            Dictionary<String, ProjectAdapterUpdateBuldingCableModel> expectedResultAsCopy = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>(dict);

            adapter.Setup(adp => adp.GetBuildingCableInfo(
                It.Is<string>(
                    (input) => CheckIfExistisAndRemove(expectedResultAsCopy, input))))

                .ReturnsAsync<String, IProjectAdapter, ServiceResult<ProjectAdapterUpdateBuldingCableModel>>((input) =>
                new ServiceResult<ProjectAdapterUpdateBuldingCableModel>(dict[input]));

            Dictionary<String, ProjectAdapterUpdateBuldingCableModel> expectedResultAsCopyForStroage = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>(dict);

            storage.Setup(ctx => ctx.UpdateBuildingWithCableInfo(
                It.Is<ProjectAdapterUpdateBuldingCableModel>((input) => CheckIfExistisAndRemove(expectedResultAsCopyForStroage, input.AdapterId)))).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateBuildingCableInfoFromAdapter, MessageRelatedObjectTypes.Buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean result = await service.UpdateBuildingCableInfoFromAdapter(userAuthId);

            Assert.False(result);
            Assert.True(expectedResultAsCopy.Count == 0);
            Assert.True(expectedResultAsCopyForStroage.Count == 0);
        }

        [Fact(DisplayName = "UpdateBuildingCableInfoFromAdapter_Service_Error|ConstructionStageServiceTester")]
        public async Task UpdateBuildingCableInfoFromAdapter_Service_Error()
        {
            ILogger<ConstructionStageService> logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();

            Int32 buildingAmount = random.Next(4, 100);
            List<String> buldingProjectAdapterIDs = new List<String>();

            for (int i = 0; i < buildingAmount; i++)
            {
                buldingProjectAdapterIDs.Add(Guid.NewGuid().ToString());
            }

            storage.Setup(ctx => ctx.GetBuildingProjectAdapterIds()).ReturnsAsync(buldingProjectAdapterIDs);


            Dictionary<string, ProjectAdapterUpdateBuldingCableModel> dict = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                String adapterId = buldingProjectAdapterIDs[i];
                ProjectAdapterUpdateBuldingCableModel buildingToUpdate = new ProjectAdapterUpdateBuldingCableModel
                {
                    AdapterId = adapterId,
                    AdapterCableTypeId = Guid.NewGuid().ToString(),
                    Length = random.Next(200, 300) + random.NextDouble()
                };

                dict.Add(adapterId, buildingToUpdate);
            }


            Boolean serviceErrorHappend = false;
            Dictionary<String, ProjectAdapterUpdateBuldingCableModel> expectedResultAsCopy = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>(dict);
            Dictionary<String, ProjectAdapterUpdateBuldingCableModel> expectedResultAsCopyForStroage = new Dictionary<string, ProjectAdapterUpdateBuldingCableModel>(dict);

            adapter.Setup(adp => adp.GetBuildingCableInfo(
                It.Is<string>(
                    (input) => CheckIfExistisAndRemove(expectedResultAsCopy, input))))

                .ReturnsAsync<String, IProjectAdapter, ServiceResult<ProjectAdapterUpdateBuldingCableModel>>((input) =>
            {
                ProjectAdapterUpdateBuldingCableModel projectAdapterUpdateBuldingModel = dict[input];

                if (random.NextDouble() > 0.5)
                {
                    return new ServiceResult<ProjectAdapterUpdateBuldingCableModel>(projectAdapterUpdateBuldingModel);
                }
                else
                {
                    serviceErrorHappend = true;
                    expectedResultAsCopyForStroage.Remove(input);
                    if (random.NextDouble() > 0.5)
                    {
                        return new ServiceResult<ProjectAdapterUpdateBuldingCableModel>(ServiceErrros.NotFound);
                    }
                    else
                    {
                        projectAdapterUpdateBuldingModel.Length = 0;
                        return new ServiceResult<ProjectAdapterUpdateBuldingCableModel>(projectAdapterUpdateBuldingModel);
                    }
                }
            });

            storage.Setup(ctx => ctx.UpdateBuildingWithCableInfo(
                It.Is<ProjectAdapterUpdateBuldingCableModel>((input) => CheckIfExistisAndRemove(expectedResultAsCopyForStroage, input.AdapterId)))).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateBuildingCableInfoFromAdapter, MessageRelatedObjectTypes.Buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean result = await service.UpdateBuildingCableInfoFromAdapter(userAuthId);

            Assert.Equal(serviceErrorHappend, result);
            Assert.True(expectedResultAsCopy.Count == 0);
            Assert.True(expectedResultAsCopyForStroage.Count == 0);
        }
    }
}
