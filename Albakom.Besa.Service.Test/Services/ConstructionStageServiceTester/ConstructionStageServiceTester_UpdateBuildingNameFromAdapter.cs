﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateBuildingNameFromAdapter : ProtocolTesterBase
    {
        private Boolean CheckIfExistisAndRemove<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
        {
            Boolean exists = dictionary.ContainsKey(key);
            if (exists == true)
            {
                dictionary.Remove(key);
            }
            return exists;
        }

        [Fact(DisplayName = "UpdateBuildingNameFromAdapter_Pass|ConstructionStageServiceTester")]
        public async Task UpdateBuildingNameFromAdapter_Pass()
        {
            ILogger<ConstructionStageService> logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();

            Int32 buildingAmount = random.Next(4, 100);
            List<String> buldingProjectAdapterIDs = new List<String>();

            for (int i = 0; i < buildingAmount; i++)
            {
                buldingProjectAdapterIDs.Add(Guid.NewGuid().ToString());
            }

            storage.Setup(ctx => ctx.GetBuildingProjectAdapterIds()).ReturnsAsync(buldingProjectAdapterIDs);


            Dictionary<string, ProjectAdapterUpdateBuildingNameModel> dict = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                String adapterId = buldingProjectAdapterIDs[i];
                ProjectAdapterUpdateBuildingNameModel buildingToUpdate = new ProjectAdapterUpdateBuildingNameModel
                {
                    AdapterId = adapterId,
                    Name = $"Wirklich guter Name {adapterId}",
                };

                dict.Add(adapterId, buildingToUpdate);
            }

            Dictionary<String, ProjectAdapterUpdateBuildingNameModel> expectedResultAsCopy = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>(dict);

            adapter.Setup(adp => adp.GetBuildingNameUpdate(
                It.Is<string>(
                    (input) => CheckIfExistisAndRemove(expectedResultAsCopy, input))))

                .ReturnsAsync<String, IProjectAdapter, ServiceResult<ProjectAdapterUpdateBuildingNameModel>>((input) =>
                new ServiceResult<ProjectAdapterUpdateBuildingNameModel>(dict[input]));

            Dictionary<String, ProjectAdapterUpdateBuildingNameModel> expectedResultAsCopyForStroage = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>(dict);

            storage.Setup(ctx => ctx.UpdateBuildingName(
                It.Is<ProjectAdapterUpdateBuildingNameModel>((input) => CheckIfExistisAndRemove(expectedResultAsCopyForStroage, input.AdapterId)))).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateBuildingNameFromAdapter, MessageRelatedObjectTypes.Buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean result = await service.UpdateBuildingNameFromAdapter(userAuthId);

            Assert.False(result);
            Assert.True(expectedResultAsCopy.Count == 0);
            Assert.True(expectedResultAsCopyForStroage.Count == 0);
        }

        [Fact(DisplayName = "UpdateBuildingNameFromAdapter_ServiceError|ConstructionStageServiceTester")]
        public async Task UpdateBuildingNameFromAdapter_ServiceError()
        {
            ILogger<ConstructionStageService> logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 buildingId = random.Next();

            Int32 buildingAmount = random.Next(4, 100);
            List<String> buldingProjectAdapterIDs = new List<String>();

            for (int i = 0; i < buildingAmount; i++)
            {
                buldingProjectAdapterIDs.Add(Guid.NewGuid().ToString());
            }

            storage.Setup(ctx => ctx.GetBuildingProjectAdapterIds()).ReturnsAsync(buldingProjectAdapterIDs);

            Dictionary<string, ProjectAdapterUpdateBuildingNameModel> dict = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>();

            for (int i = 0; i < buildingAmount; i++)
            {
                String adapterId = buldingProjectAdapterIDs[i];
                ProjectAdapterUpdateBuildingNameModel buildingToUpdate = new ProjectAdapterUpdateBuildingNameModel
                {
                    AdapterId = adapterId,
                    Name = $"Wirklich guter Name {adapterId}",
                };

                dict.Add(adapterId, buildingToUpdate);
            }

            Boolean serviceErrorHappend = false;
            Dictionary<String, ProjectAdapterUpdateBuildingNameModel> expectedResultAsCopy = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>(dict);
            Dictionary<String, ProjectAdapterUpdateBuildingNameModel> expectedResultAsCopyForStroage = new Dictionary<string, ProjectAdapterUpdateBuildingNameModel>(dict);

            adapter.Setup(adp => adp.GetBuildingNameUpdate(
                It.Is<string>(
                    (input) => CheckIfExistisAndRemove(expectedResultAsCopy, input))))

                .ReturnsAsync<String, IProjectAdapter, ServiceResult<ProjectAdapterUpdateBuildingNameModel>>((input) =>
            {
                ProjectAdapterUpdateBuildingNameModel projectAdapterUpdateBuldingModel = dict[input];

                if (random.NextDouble() > 0.5)
                {
                    return new ServiceResult<ProjectAdapterUpdateBuildingNameModel>(projectAdapterUpdateBuldingModel);
                }
                else
                {
                    serviceErrorHappend = true;
                    expectedResultAsCopyForStroage.Remove(input);
                    if (random.NextDouble() > 0.5)
                    {
                        return new ServiceResult<ProjectAdapterUpdateBuildingNameModel>(ServiceErrros.NotFound);
                    }
                    else
                    {
                        projectAdapterUpdateBuldingModel.Name = String.Empty;
                        return new ServiceResult<ProjectAdapterUpdateBuildingNameModel>(projectAdapterUpdateBuldingModel);
                    }
                }
            });

            storage.Setup(ctx => ctx.UpdateBuildingName(
                It.Is<ProjectAdapterUpdateBuildingNameModel>((input) => CheckIfExistisAndRemove(expectedResultAsCopyForStroage, input.AdapterId)))).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateBuildingNameFromAdapter, MessageRelatedObjectTypes.Buildings);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean result = await service.UpdateBuildingNameFromAdapter(userAuthId);

            Assert.Equal(serviceErrorHappend, result);
            Assert.True(expectedResultAsCopy.Count == 0);
            Assert.True(expectedResultAsCopyForStroage.Count == 0);
        }
    }
}
