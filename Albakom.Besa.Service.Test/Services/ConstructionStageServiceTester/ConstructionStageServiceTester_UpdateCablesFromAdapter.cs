﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateCablesFromAdapter : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdateCablesFromAdapter|ConstructionStageServiceTester")]
        public async Task UpdateCablesFromAdapter()
        {
            Random random = new Random();

            Int32 branchableId = 1;

            Dictionary<String, Int32> storedBranchableIds = new Dictionary<string, int>();
            storedBranchableIds.Add(Guid.NewGuid().ToString(), branchableId);

            String cableTypeId = Guid.NewGuid().ToString();
            Int32 storedCableTypeId = random.Next();

            Int32 storedCableAmount = random.Next(20, 300);
            Dictionary<String, Int32> storedCableIds = new Dictionary<string, int>(storedCableAmount);

            for (int i = 0; i < storedCableAmount; i++)
            {
                storedCableIds.Add(Guid.NewGuid().ToString(), i);
            }

            List<ProjectAdapterCableModel> cableAdapterResult = new List<ProjectAdapterCableModel>();

            Dictionary<String, ProjectAdapterCableModel> cablesToUpdate = new Dictionary<string, ProjectAdapterCableModel>();
            Dictionary<String, ProjectAdapterCableModel> cablesToAdd = new Dictionary<string, ProjectAdapterCableModel>();
            Dictionary<String, Int32> cableCreateIds = new Dictionary<string, int>();

            Int32 buildingAmount = random.Next(10, 100);
            Dictionary<String, Int32> buildingMapper = new Dictionary<string, int>();
            Dictionary<Int32, Int32> bindBuildingInputs = new Dictionary<int, int>();

            for (int i = 1; i <= buildingAmount; i++)
            {
                String projectAdapterId = Guid.NewGuid().ToString();
                Int32 id = i;

                buildingMapper.Add(projectAdapterId, id);
            }


            List<Int32> cablesToDelete = new List<Int32>();

            Int32 currentCableid = storedCableIds.Count;
            foreach (KeyValuePair<String, Int32> item in storedCableIds)
            {
                Boolean addBuilding = random.NextDouble() > 0.5;
                Double randomValue = random.NextDouble();
                ProjectAdapterCableModel adapterModel = null;
                if (randomValue < 0.3)
                {
                    String adapterId = Guid.NewGuid().ToString();
                    adapterModel = new ProjectAdapterCableModel
                    {
                        ProjectAdapterId = adapterId,
                        ProjectAdapterCableTypeId = cableTypeId,
                        DuctColor = new ProjectAdapterColor
                        {
                            Name = $"Testfarbe {random.Next()}",
                            RGBHexValue = $"0x{random.Next(0, 999999) + 1}",
                        },
                        Lenght = random.Next(10, 200) + random.NextDouble(),
                        Name = $"Kabel Nr {random.Next()}",
                    };

                    cableAdapterResult.Add(adapterModel);
                    cablesToAdd.Add(adapterId, adapterModel);
                    currentCableid += 1;
                    cableCreateIds.Add(adapterId, currentCableid);

                    if (addBuilding == true)
                    {
                        KeyValuePair<String, Int32> mapperItem = buildingMapper.ElementAt(random.Next(0, buildingMapper.Count));
                        adapterModel.ConnectedBuildingAdapterId = mapperItem.Key;
                        bindBuildingInputs.Add(currentCableid, mapperItem.Value);
                    }

                    cablesToDelete.Add(item.Value);
                }
                else if (randomValue > 0.6)
                {
                    adapterModel = new ProjectAdapterCableModel
                    {
                        ProjectAdapterId = item.Key,
                        ProjectAdapterCableTypeId = cableTypeId,
                        DuctColor = new ProjectAdapterColor
                        {
                            Name = $"Testfarbe {random.Next()}",
                            RGBHexValue = $"0x{random.Next(0, 999999) + 1}",
                        },
                        Lenght = random.Next(10, 200) + random.NextDouble(),
                        Name = $"Kabel Nr {random.Next()}",
                    };

                    cableAdapterResult.Add(adapterModel);
                    cablesToUpdate.Add(item.Key, adapterModel);

                    if(addBuilding == true)
                    {
                        KeyValuePair<String, Int32> mapperItem = buildingMapper.ElementAt(random.Next(0, buildingMapper.Count));
                        adapterModel.ConnectedBuildingAdapterId = mapperItem.Key;
                        bindBuildingInputs.Add(item.Value, mapperItem.Value);
                    }
                }
                else
                {
                    cablesToDelete.Add(item.Value);
                }

                if (adapterModel == null) continue;
            }

            ServiceResult<IEnumerable<ProjectAdapterCableModel>> serviceResult = new ServiceResult<IEnumerable<ProjectAdapterCableModel>>(cableAdapterResult);

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetCableByBranchableId(storedBranchableIds.First().Key)).ReturnsAsync(serviceResult);

            storage.Setup(ctx => ctx.GetBranchablesWithProjectAdapterId()).ReturnsAsync(storedBranchableIds);
            storage.Setup(ctx => ctx.GetCableFromBranchbaleWithProjectAdapterId(storedBranchableIds.First().Value)).ReturnsAsync(storedCableIds);

            storage.Setup(ctx => ctx.CheckIfCableTypeExistsByProjectAdapterId(cableTypeId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetCableTypeIdByProjectAdapterId(cableTypeId)).ReturnsAsync(storedCableTypeId);

            storage.Setup(ctx => ctx.UpdateCableFromAdapter(
                It.Is<Int32>(x => storedCableIds.Values.Contains(x)),
                It.Is<ProjectAdapterCableModel>(x => cablesToUpdate.ContainsKey(x.ProjectAdapterId)),
                branchableId,
              storedCableTypeId))
                .ReturnsAsync(true);

            storage.Setup(ctx => ctx.AddCableFromAdapter(
              It.Is<ProjectAdapterCableModel>(x => cablesToAdd.ContainsKey(x.ProjectAdapterId)),
              branchableId,
              storedCableTypeId))
              .ReturnsAsync<ProjectAdapterCableModel, Int32, Int32, IBesaDataStorage, Int32>((x,branchable, typeId) => cableCreateIds[x.ProjectAdapterId]);

            storage.Setup(ctx => ctx.DeleteCable(It.Is<Int32>(x => cablesToDelete.Contains(x)))
    ).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingExistsByProjectAdapterId(
     It.Is<String>(x => buildingMapper.ContainsKey(x))))
    .ReturnsAsync(true);

            storage.Setup(ctx => ctx.GetBuildingIdByProjectAdapterId(
  It.Is<String>(x => buildingMapper.ContainsKey(x))
  ))
  .ReturnsAsync<String, IBesaDataStorage, Int32>((adapterID) => buildingMapper[adapterID]);

            storage.Setup(ctx => ctx.UpdateBuildingCableRelation(
It.Is<Int32>(x => bindBuildingInputs.ContainsKey(x)),
It.Is<Int32>(x => bindBuildingInputs.ContainsValue(x))
))
.ReturnsAsync(true);

            storage.Setup(ctx => ctx.GetBuildingsWithOneUnit()).ReturnsAsync(new List<Int32>());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateCablesFromAdapter, MessageRelatedObjectTypes.FiberCable);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdateCablesFromAdapter(userAuthId);
        }
    }
}
