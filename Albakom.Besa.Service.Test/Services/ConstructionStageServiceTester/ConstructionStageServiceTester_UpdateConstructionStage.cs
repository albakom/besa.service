﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateConstructionStage : ProtocolTesterBase
    {
        private Boolean CheckCollections(IEnumerable<Int32> firstCollection, IEnumerable<Int32> secondCollection)
        {
            try
            {
                Assert.Equal(firstCollection.OrderBy(x => x), secondCollection.OrderBy(x => x));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [Fact(DisplayName = "UpdateConstructionStage_Pass_NoBranchableChanged|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Pass_NoBranchableChanged()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random(12345);
            Int32 id = random.Next();
            String name = $"Testname {random.Next()}";

            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel
            {
                Id = id,
                Name = name,
            };

            ConstructionStageDetailsForEditModel expectedResult = new ConstructionStageDetailsForEditModel
            {
                Id = id,
                Name = name,
            };

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfConstructionStageNameExists(name, id)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.UpdateConstructionStage(It.Is<ConstructionStageUpdateModel>(x => x.Name == name && x.Id == id && x.AddedBranchables.Count() == 0 && x.RemovedBranchables.Count() == 0))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetDetailsForEdit(id)).ReturnsAsync(expectedResult);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, id);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageDetailsForEditModel actual = await service.UpdateConstructionStage(updateModel, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Id, actual.Id);
            Assert.Equal(expectedResult.Name, actual.Name);
        }

        [Fact(DisplayName = "UpdateConstructionStage_Pass_WithBranchablesChanged|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Pass_WithBranchablesChanged()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();
            String name = $"Testname {random.Next()}";

            List<Int32> branchableIds = new List<int>();

            List<Int32> existingBranchable = new List<int>();
            List<Int32> branchableToAdd = new List<int>();
            List<Int32> branchableToAddWithoutExisitng = new List<int>();
            List<Int32> branchableToRemove = new List<int>();
            List<Int32> branhcbaleToRemoveWithoutAlreadyRemoved = new List<int>();

            Int32 branchableAmount = random.Next(30, 100);
            for (int i = 0; i < branchableAmount; i++)
            {
                Int32 branchableId = random.Next();

                Double randomVlaue = random.NextDouble();
                if(randomVlaue > 0.75)
                {
                    branchableToAdd.Add(branchableId);
                    branchableToAddWithoutExisitng.Add(branchableId);
                }
                else if (randomVlaue > 0.5)
                {
                    branchableToAdd.Add(branchableId);
                    existingBranchable.Add(branchableId);
                }
                else if(randomVlaue > 0.25)
                {
                    branchableToRemove.Add(branchableId);
                    existingBranchable.Add(branchableId);
                    branhcbaleToRemoveWithoutAlreadyRemoved.Add(branchableId);
                }
                else
                {
                    branchableToRemove.Add(branchableId);
                }
            }

            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel
            {
                Id = id,
                Name = name,
                AddedBranchables = branchableToAdd,
                RemovedBranchables = branchableToRemove,
            };

            ConstructionStageDetailsForEditModel expectedResult = new ConstructionStageDetailsForEditModel
            {
                Id = id,
                Name = name,
            };

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfConstructionStageNameExists(name, id)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfCabinetsExists(It.Is<IEnumerable<Int32>>(x => CheckCollections(x, branchableToRemove.Union(branchableToAdd).ToList())))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBranchableIdsByConstructionStageId(id)).ReturnsAsync(existingBranchable);

            storage.Setup(ctx => ctx.UpdateConstructionStage(It.Is<ConstructionStageUpdateModel>(
                x => x.Name == name && 
                x.Id == id && 
                CheckCollections(x.AddedBranchables, branchableToAddWithoutExisitng) &&
                CheckCollections(x.RemovedBranchables, branhcbaleToRemoveWithoutAlreadyRemoved)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetDetailsForEdit(id)).ReturnsAsync(expectedResult);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, id);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            ConstructionStageDetailsForEditModel actual = await service.UpdateConstructionStage(updateModel, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Id, actual.Id);
            Assert.Equal(expectedResult.Name, actual.Name);
        }

        [Fact(DisplayName = "UpdateConstructionStage_Fail_NoModel|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Fail_NoModel()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            String userAuthId = base.PrepareStorage(new Random(), storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, -1);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.UpdateConstructionStage(null, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.ModelIsNull,exception.Reason);
        }

        [Fact(DisplayName = "UpdateConstructionStage_Fail_NotFound|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Fail_NotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();
            String name = $"Testname {random.Next()}";

            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel
            {
                Id = id,
                Name = name,
            };

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfConstructionStageNameExists(name, id)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, id);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.UpdateConstructionStage(updateModel,userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "UpdateConstructionStage_Fail_NameInUse|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Fail_NameInUse()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();
            String name = $"Testname {random.Next()}";

            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel
            {
                Id = id,
                Name = name,
            };

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfConstructionStageNameExists(name, id)).ReturnsAsync(true);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, id);

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.UpdateConstructionStage(updateModel, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.NameInUse, exception.Reason);
        }

        [Fact(DisplayName = "UpdateConstructionStage_Fail_BranchableNotFound|ConstructionStageServiceTester")]
        public async Task UpdateConstructionStage_Fail_BranchableNotFound()
        {
            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            Int32 id = random.Next();
            String name = $"Testname {random.Next()}";

            List<Int32> branchableIds = new List<int>();

            List<Int32> existingBranchable = new List<int>();
            List<Int32> openBranchableIDs = new List<int>();
            Int32 branchableAmount = random.Next(30, 100);
            for (int i = 0; i < branchableAmount; i++)
            {
                Int32 branchableId = random.Next();

                Boolean addToStage = random.NextDouble() > 0.7;
                if (addToStage == true)
                {
                    existingBranchable.Add(branchableId);
                }
                else
                {
                    openBranchableIDs.Add(branchableId);
                }

                branchableIds.Add(branchableId);
            }

            ConstructionStageUpdateModel updateModel = new ConstructionStageUpdateModel
            {
                Id = id,
                Name = name,
                AddedBranchables = openBranchableIDs,
                RemovedBranchables = existingBranchable,
            };

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfConstructionStageExists(id)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfConstructionStageNameExists(name, id)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfCabinetsExists(It.Is<IEnumerable<Int32>>( x => CheckCollections(x, openBranchableIDs.Union(existingBranchable))))).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Update, MessageRelatedObjectTypes.ConstructionStage, id);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());

            ConstructionStageServicException exception = await Assert.ThrowsAsync<ConstructionStageServicException>(() => service.UpdateConstructionStage(updateModel, userAuthId));
            Assert.Equal(ConstructionStageServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
