﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateMainCableForBranchable : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdateMainCableForBranchable|ConstructionStageServiceTester")]
        public async Task UpdateMainCableForBranchable()
        {
            Random random = new Random();

            Int32 branchableAmount = random.Next(3, 10);

            Dictionary<String, Int32> storedBranchableIds = new Dictionary<string, int>();
            Dictionary<String, ProjectAdapterMainCableForBranchableResult> mainCableResult = new Dictionary<string, ProjectAdapterMainCableForBranchableResult>();

            Dictionary<String, Boolean> cableExistsResult = new Dictionary<string, bool>();
            Dictionary<String, Int32> storedCables = new Dictionary<string, Int32>();
            Dictionary<Int32, Int32> updateRelations = new Dictionary<int, int>();

            Int32 lastCableId = 1;

            for (int i = 0; i < branchableAmount; i++)
            {
                String branchableProjectAdapterId = Guid.NewGuid().ToString();
                Int32 branchableStorageId = i + 1;
                storedBranchableIds.Add(branchableProjectAdapterId, branchableStorageId);
                Int32 currentCableId = lastCableId;
                String cableProjectID = Guid.NewGuid().ToString();

                Boolean createCable = random.NextDouble() > 0.5;
                if (createCable == true)
                {
                    storedCables.Add(cableProjectID, currentCableId);

                    lastCableId += 1;
                }

                Boolean createServiceResult = random.NextDouble() > 0.5;

                if (createServiceResult == true)
                {
                    ProjectAdapterMainCableForBranchableResult projectResult = new ProjectAdapterMainCableForBranchableResult
                    {
                        ProjectAdapterBranchableId = branchableProjectAdapterId,
                        ProjectAdapterCableId = cableProjectID,
                    };

                    mainCableResult.Add(branchableProjectAdapterId, projectResult);
                    cableExistsResult.Add(projectResult.ProjectAdapterCableId, createCable);
                }

                if (createCable == true && createServiceResult == true)
                {
                    updateRelations.Add(branchableStorageId, currentCableId);
                }
            }

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var taskService = new Mock<IUpdateTaskService>(MockBehavior.Strict);


            storage.Setup(ctx => ctx.GetBranchablesWithProjectAdapterId()).ReturnsAsync(storedBranchableIds);
            storage.Setup(ctx => ctx.CheckIfCableExistsByProjectId(
    It.Is<String>(x => cableExistsResult.ContainsKey(x))))
    .ReturnsAsync<String, IBesaDataStorage, Boolean>((x) => cableExistsResult[x]);

            storage.Setup(ctx => ctx.GetCableIdByProjectId(
    It.Is<String>(x => storedCables.ContainsKey(x))))
    .ReturnsAsync<String, IBesaDataStorage, Int32>((x) => storedCables[x]);

            storage.Setup(ctx => ctx.UpdateBranchableCableRelation(
    It.Is<Int32>(x => updateRelations.ContainsKey(x)),
    It.Is<Int32>(x => updateRelations.ContainsValue(x))
    ))
    .ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateMainCableForBranchable, MessageRelatedObjectTypes.MainCables);

            adapter.Setup(adp => adp.GetMainCableForBranchable(It.Is<String>(x => storedBranchableIds.ContainsKey(x)))).ReturnsAsync<String, IProjectAdapter, ServiceResult<ProjectAdapterMainCableForBranchableResult>>(x =>
            {
                if (mainCableResult.ContainsKey(x) == true)
                {
                    return new ServiceResult<ProjectAdapterMainCableForBranchableResult>(mainCableResult[x]);
                }
                else
                {
                    return new ServiceResult<ProjectAdapterMainCableForBranchableResult>(null);
                }
            });

            Int32 lastId = 0;
            Dictionary<Int32, Int32> objectIdToTaskElementMapper = storedBranchableIds.ToDictionary(x => x.Value, x => ++lastId);

            taskService.Setup(svc => svc.GetOpenElements(UpdateTasks.MainCableForBranchable, userAuthId)).ReturnsAsync(storedBranchableIds.Select(x =>
             new UpdateTaskElementModel
             {
                 AdapterId = x.Key,
                 ObjectId = x.Value,
                 Id = objectIdToTaskElementMapper[x.Value],
             }));

            taskService.Setup(svc => svc.CheckIfTaskIsCanceledByElementId(It.Is<Int32>(x => objectIdToTaskElementMapper.Values.Contains(x) == true))).ReturnsAsync(false);
            taskService.Setup(svc => svc.StartUpdateTaskElement(It.Is<Int32>(x => objectIdToTaskElementMapper.Values.Contains(x) == true))).ReturnsAsync(false);
            taskService.Setup(svc => svc.FinishUpdateElement(It.Is<UpdateTaskElementFinishModel>(x => 
            objectIdToTaskElementMapper.Values.Contains(x.ElementId) == true &&
            (x.State == UpdateTaskElementStates.Success || x.State == UpdateTaskElementStates.Waring) ))).ReturnsAsync(true);
            taskService.Setup(svc => svc.FinishTask( UpdateTasks.MainCableForBranchable)).ReturnsAsync(true);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), taskService.Object);
            Boolean actual = await service.UpdateMainCableForBranchable(userAuthId);

            Assert.True(actual);
        }
    }
}
