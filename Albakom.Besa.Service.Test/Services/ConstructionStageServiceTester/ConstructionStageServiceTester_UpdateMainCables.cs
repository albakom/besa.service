﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateMainCables : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdateMainCables|ConstructionStageServiceTester")]
        public async Task UpdateMainCables()
        {
            Random random = new Random();

            Int32 branchableAmount = random.Next(3,10);

            Dictionary<String, Int32> storedPoPIds = new Dictionary<string, int>();
            Dictionary<String, List<String>> mainCableResult = new Dictionary<string, List<String>>();

            Dictionary<IEnumerable<String>, List<Int32>> cableExistsResult = new Dictionary<IEnumerable<string>, List<int>>();
            Dictionary<Int32, IEnumerable<Int32>> updatePopResult = new Dictionary<int, IEnumerable<int>>();

            Int32 lastCableId = 1;

            for (int i = 0; i < branchableAmount; i++)
            {
                String popProjectAdapterId = Guid.NewGuid().ToString();
                Int32 popStorageId = i + 1;
                storedPoPIds.Add(popProjectAdapterId, popStorageId);

                Boolean hasCableResult = random.NextDouble() > 0.5;

                if(hasCableResult == true)
                {
                    List<String> cableItems = new List<string>();
                    List<Int32> storedCalbeItemIds = new List<int>();
                    Int32 cableAmount = random.Next(5, 10);

                    for (int j = 0; j < cableAmount; j++)
                    {
                        String cableProjectId = Guid.NewGuid().ToString();
                        cableItems.Add(cableProjectId);

                        Boolean existsInDatabase = random.NextDouble() > 0.5;

                        if(existsInDatabase == true)
                        {
                            storedCalbeItemIds.Add(lastCableId);
                            lastCableId += 1;
                        }

                    }

                    mainCableResult.Add(popProjectAdapterId, cableItems);
                    cableExistsResult.Add(cableItems, storedCalbeItemIds);
                    updatePopResult.Add(popStorageId, storedCalbeItemIds);
                }
            }

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetMainCableStartingFromPop(It.Is<String>( x => storedPoPIds.ContainsKey(x)))).ReturnsAsync<String, IProjectAdapter,ServiceResult< IEnumerable<String>>>(x =>
            {
                if(mainCableResult.ContainsKey(x) == true)
                {
                    return new ServiceResult<IEnumerable<String>>(mainCableResult[x]);
                }
                else
                {
                    return new ServiceResult<IEnumerable<String>>(new List<String>());
                }
            });


            storage.Setup(ctx => ctx.GetPopsWithProjectAdapterId()).ReturnsAsync(storedPoPIds);

            storage.Setup(ctx => ctx.GetCableIdsByProjectIds(
    It.Is<IEnumerable<String>>(x => cableExistsResult.ContainsKey(x))))
    .ReturnsAsync<IEnumerable<String>, IBesaDataStorage, IEnumerable<Int32>>((x) => cableExistsResult[x]);

            storage.Setup(ctx => ctx.UpdatePoPCableRelation(
    It.Is<Int32>(x => updatePopResult.ContainsKey(x)),
    It.Is<IEnumerable<Int32>>(x => updatePopResult.ContainsValue(x))
    ))
    .ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdateMainCables, MessageRelatedObjectTypes.MainCables);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean actual = await service.UpdateMainCables(userAuthId);

            Assert.True(actual);
        }
    }
}
