﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdatePatchConnections : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdatePatchConnections|ConstructionStageServiceTester")]
        public async Task UpdatePatchConnections()
        {
            Random random = new Random();

            Int32 storedCableAmount = random.Next(3,10);

            Dictionary<String, Int32> storedCableToFlatRelation = new Dictionary<string, int>();
            Dictionary<String, List<ProjectAdapterPatchModel>> patchConnectionResult = new Dictionary<string, List<ProjectAdapterPatchModel>>();

            Dictionary<Int32, IEnumerable<ProjectAdapterPatchModel>> patchesToAddPerFlat = new Dictionary<int, IEnumerable<ProjectAdapterPatchModel>>();

            for (int i = 0; i < storedCableAmount; i++)
            {
                String cableProjectAdapterId = Guid.NewGuid().ToString();
                Int32 flatStorageId = i + 1;
                storedCableToFlatRelation.Add(cableProjectAdapterId, flatStorageId);

                Boolean hasConnections = random.NextDouble() > 0.5;
                if(hasConnections == true)
                {
                    List<ProjectAdapterPatchModel> connections = new List<ProjectAdapterPatchModel>();
                    Int32 connectionAmount = random.Next(5, 10);

                    for (int j = 0; j < connectionAmount; j++)
                    {
                        ProjectAdapterPatchModel item = new ProjectAdapterPatchModel
                        {
                            Caption = $"Blub23-{random.Next()}",
                            ColumnNumber = random.Next(3, 10).ToString(),
                            ModuleName = random.Next(30, 100).ToString(),
                            RowNumber = random.Next(200, 300).ToString(),
                        };

                        connections.Add(item);
                    }

                    patchConnectionResult.Add(cableProjectAdapterId, connections);
                    patchesToAddPerFlat.Add(flatStorageId, connections);
                }
            }

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetConnectionByCableId(It.Is<String>( x => storedCableToFlatRelation.ContainsKey(x)))).ReturnsAsync<String, IProjectAdapter,ServiceResult< IEnumerable<ProjectAdapterPatchModel>>>(x =>
            {
                if(patchConnectionResult.ContainsKey(x) == true)
                {
                    return new ServiceResult<IEnumerable<ProjectAdapterPatchModel>>(patchConnectionResult[x]);
                }
                else
                {
                    return new ServiceResult<IEnumerable<ProjectAdapterPatchModel>>(new List<ProjectAdapterPatchModel>());
                }
            });

            storage.Setup(ctx => ctx.GetCablesWithFlatIDs()).ReturnsAsync(storedCableToFlatRelation);

            storage.Setup(ctx => ctx.DeletePatchConnectionByFlatId(
    It.Is<Int32>(x => storedCableToFlatRelation.ContainsValue(x))))
    .ReturnsAsync(true);

            storage.Setup(ctx => ctx.AddPatchConnectionByFlatId(
It.Is<Int32>(x => storedCableToFlatRelation.ContainsValue(x)),
It.Is<IEnumerable<ProjectAdapterPatchModel>>(x => patchesToAddPerFlat.ContainsValue(x))))
.ReturnsAsync(new List<Int32>());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.UpdatePatchConnections, MessageRelatedObjectTypes.FiberConnection);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            Boolean actual = await service.UpdatePatchConnections(userAuthId);

            Assert.True(actual);
        }
    }
}
