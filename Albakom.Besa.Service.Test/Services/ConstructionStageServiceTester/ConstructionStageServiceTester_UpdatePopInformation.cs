﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdatePopInformation : ProtocolTesterBase
    {
        [Fact(DisplayName = "UpdatePopInformation|ConstructionStageServiceTester")]
        public async Task UpdatePopInformation()
        {
            Random random = new Random();

            Int32 storedPopAmount = random.Next(20, 300);
            Dictionary<String, Int32> storedPoPs = new Dictionary<string, int>(storedPopAmount);

            for (int i = 1; i <= storedPopAmount; i++)
            {
                storedPoPs.Add(Guid.NewGuid().ToString(), i);
            }

            List<ProjectAdapterPoPModel> adapterResult = new List<ProjectAdapterPoPModel>();

            Dictionary<String, ProjectAdapterPoPModel> popsToUpdate = new Dictionary<string, ProjectAdapterPoPModel>();
            Dictionary<String, ProjectAdapterPoPModel> popsToAdd = new Dictionary<string, ProjectAdapterPoPModel>();
            Dictionary<String, Int32> popsCreateIds = new Dictionary<string, int>();

            Dictionary<String, Boolean> branchableExistsResult = new Dictionary<string, bool>();
            Dictionary<Int32, List<String>> branchableToUpdatePerPoP = new Dictionary<int, List<string>>();


            List<Int32> popsToDelete = new List<Int32>();

            Int32 currentPopId = storedPoPs.Count;
            foreach (KeyValuePair<String, Int32> item in storedPoPs)
            {
                Boolean addBuilding = random.NextDouble() > 0.5;
                Double randomValue = random.NextDouble();
                ProjectAdapterPoPModel adapterModel = null;
                if (randomValue < 0.3)
                {
                    String adapterId = Guid.NewGuid().ToString();
                    adapterModel = new ProjectAdapterPoPModel
                    {
                        ProjectAdapterId = adapterId,
                        Location = new GPSCoordinate
                        {
                            Latitude = random.Next(20,30) + random.NextDouble(),
                            Longitude = random.Next(20, 30) + random.NextDouble(),
                        },
                        Name = $"PoP Nr {random.Next()}",
                    };

                    adapterResult.Add(adapterModel);
                    popsToAdd.Add(adapterId, adapterModel);
                    currentPopId += 1;
                    popsCreateIds.Add(adapterId, currentPopId);

                    popsToDelete.Add(item.Value);
                }
                else if (randomValue > 0.6)
                {
                    adapterModel = new ProjectAdapterPoPModel
                    {
                        ProjectAdapterId = item.Key,
                        Location = new GPSCoordinate
                        {
                            Latitude = random.Next(20, 30) + random.NextDouble(),
                            Longitude = random.Next(20, 30) + random.NextDouble(),
                        },
                        Name = $"PoP Nr {random.Next()}",
                    };

                    adapterResult.Add(adapterModel);
                    popsToUpdate.Add(item.Key, adapterModel);
                }
                else
                {
                    popsToDelete.Add(item.Value);
                }

                if (adapterModel == null) continue;

                Int32 branchableAmount = random.Next(10, 20);

                List<String> branchableIds = new List<string>();
                List<String> branchableToUpdate = new List<string>();
                for (int i = 0; i < branchableAmount; i++)
                {
                    String branchableId = Guid.NewGuid().ToString();
                    Boolean existsInDatabse = random.NextDouble() > 0.5;

                    branchableExistsResult.Add(branchableId, existsInDatabse);
                    branchableIds.Add(branchableId);

                    if(existsInDatabse == true)
                    {
                        branchableToUpdate.Add(branchableId);
                    }
                }

                adapterModel.Branchables = branchableIds;
                branchableToUpdatePerPoP.Add(item.Value, branchableToUpdate);
            }

            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> serviceResult = new ServiceResult<IEnumerable<ProjectAdapterPoPModel>>(adapterResult);

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            adapter.Setup(adp => adp.GetAllPoPs()).ReturnsAsync(serviceResult);
            storage.Setup(ctx => ctx.GetPopsWithProjectAdapterId()).ReturnsAsync(new Dictionary<String,Int32>(storedPoPs));

            storage.Setup(ctx => ctx.UpdatePoPFromAdapter(
                It.Is<Int32>(x => storedPoPs.Values.Contains(x)),
                It.Is<ProjectAdapterPoPModel>(x => popsToUpdate.ContainsKey(x.ProjectAdapterId))
              ))
                .ReturnsAsync(true);

            storage.Setup(ctx => ctx.AddPoPFromAdapter(
              It.Is<ProjectAdapterPoPModel>(x => popsToAdd.ContainsKey(x.ProjectAdapterId))))
              .ReturnsAsync<ProjectAdapterPoPModel, IBesaDataStorage, Int32>((x) => popsCreateIds[x.ProjectAdapterId]);

            storage.Setup(ctx => ctx.UpdatePoPIdFromBranchable(
    It.Is<String>(x => branchableToUpdatePerPoP.Count(y => y.Value.Contains(x) == true) > 0),
    It.Is<Int32>(x => storedPoPs.ContainsValue(x) || popsCreateIds.ContainsValue(x) )
  ))
    .ReturnsAsync(true);


            storage.Setup(ctx => ctx.CheckIfBranchableExistsByAdapterId(
It.Is<String>(x => branchableExistsResult.ContainsKey(x))
))
.ReturnsAsync<String, IBesaDataStorage, Boolean>(x => branchableExistsResult[x]);


            storage.Setup(ctx => ctx.DeletePopById(It.Is<Int32>(x => popsToDelete.Contains(x)))
    ).ReturnsAsync(true);

            String authUserId = base.PrepareStorage(random, storage, MessageActions.UpdatePopInformation, MessageRelatedObjectTypes.PoP);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger, base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdatePopInformation(authUserId);
        }
    }
}
