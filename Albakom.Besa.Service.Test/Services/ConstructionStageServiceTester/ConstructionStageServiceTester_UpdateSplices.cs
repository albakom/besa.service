﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.ConstructionStageServiceTester
{
    public class ConstructionStageServiceTester_UpdateSplices : ProtocolTesterBase
    {

        private Boolean FiberSpliceCreateModelTester(FiberSpliceCreateModel modelToRest, IDictionary<String, FiberSpliceCreateModel> exepctedModels)
        {
            if (exepctedModels.ContainsKey(modelToRest.SpliceModel.ProjectAdapterId) == false) { return false; }

            FiberSpliceCreateModel expectedModel = exepctedModels[modelToRest.SpliceModel.ProjectAdapterId];
            return expectedModel.FirstFiberId == modelToRest.FirstFiberId && expectedModel.SecondFiberId == modelToRest.SecondFiberId;
        }

        [Fact(DisplayName = "UpdateSplices|ConstructionStageServiceTester")]
        public async Task UpdateSplices()
        {
            Random random = new Random();

            Int32 branchableId = 1;

            Dictionary<String, Int32> storedBranchableIds = new Dictionary<string, int>();
            storedBranchableIds.Add(Guid.NewGuid().ToString(), branchableId);

            Int32 storedSpliceAmount = random.Next(20, 300);
            Dictionary<String, Int32> storedSpliceIds = new Dictionary<string, int>(storedSpliceAmount);

            for (int i = 0; i < storedSpliceAmount; i++)
            {
                storedSpliceIds.Add(Guid.NewGuid().ToString(), i);
            }

            List<ProjectAdapterSpliceModel> spliceAdapterResult = new List<ProjectAdapterSpliceModel>();

            Dictionary<String, ProjectAdapterSpliceModel> splicesToUpdate = new Dictionary<string, ProjectAdapterSpliceModel>();
            Dictionary<String, FiberSpliceCreateModel> splicesToAdd = new Dictionary<string, FiberSpliceCreateModel>();
            Dictionary<String, Int32> spliceCreateIds = new Dictionary<string, int>();

            Dictionary<String, Boolean> cableExistingResults = new Dictionary<string, bool>();
            Dictionary<String, Int32> cableMapper = new Dictionary<string, int>();

            Dictionary<String, Boolean> fiberExistisResult = new Dictionary<string, bool>();

            Dictionary<String, ProjectAdapterFiberModel> fibersToCreate = new Dictionary<string, ProjectAdapterFiberModel>();
            Dictionary<String, Int32> fiberCreateIds = new Dictionary<string, int>();

            List<Int32> splicesToDelete = new List<Int32>();

            Dictionary<String, ProjectAdapterFiberModel> fiberToUpdates = new Dictionary<string, ProjectAdapterFiberModel>();

            Int32 currentSpliceid = storedSpliceIds.Count;
            //foreach (KeyValuePair<String, Int32> item in storedSpliceIds)
            //{
            //    Double randomValue = random.NextDouble();
            //    ProjectAdapterSpliceModel adapterModel = null;
            //    if (randomValue < 0.3)
            //    {
            //        String adapterId = Guid.NewGuid().ToString();
            //        adapterModel = new ProjectAdapterSpliceModel
            //        {
            //            ProjectAdapterId = adapterId,
            //           NumberInTray = random.Next(1,24),
            //           TrayNumber = random.Next(1,24),
            //           Type = SpliceTypes.Normal,
            //           FirstFiber = new ProjectAdapterFiberModel
            //           {
            //               BundleNumber = random.Next(1,12),
            //               NumberinBundle = random.Next(1,24),
            //               FiberNumber = random.Next(1,244),
            //               Color = $"Schöne Farbe NR. {random.Next()}",
            //               Name = $"Testfaser Nr {random.Next()}",
            //           }
            //        };

            //        spliceAdapterResult.Add(adapterModel);
            //        splicesToAdd.Add(adapterId, adapterModel);
            //        currentSpliceid += 1;
            //        spliceCreateIds.Add(adapterId, currentSpliceid);

            //        if (addBuilding == true)
            //        {
            //            KeyValuePair<String, Int32> mapperItem = buildingMapper.ElementAt(random.Next(0, buildingMapper.Count));
            //            adapterModel.ConnectedBuildingAdapterId = mapperItem.Key;
            //            bindBuildingInputs.Add(currentSpliceid, mapperItem.Value);
            //        }

            //        splicesToDelete.Add(item.Value);
            //    }
            //    else if (randomValue > 0.6)
            //    {
            //        adapterModel = new ProjectAdapterSpliceModel
            //        {
            //            ProjectAdapterId = item.Key,
            //            ProjectAdapterCableTypeId = cableTypeId,
            //            DuctColor = new ProjectAdapterColor
            //            {
            //                Name = $"Testfarbe {random.Next()}",
            //                RGBHexValue = $"0x{random.Next(0, 999999) + 1}",
            //            },
            //            Lenght = random.Next(10, 200) + random.NextDouble(),
            //            Name = $"Kabel Nr {random.Next()}",
            //        };

            //        spliceAdapterResult.Add(adapterModel);
            //        splicesToUpdate.Add(item.Key, adapterModel);

            //        if (addBuilding == true)
            //        {
            //            KeyValuePair<String, Int32> mapperItem = buildingMapper.ElementAt(random.Next(0, buildingMapper.Count));
            //            adapterModel.ConnectedBuildingAdapterId = mapperItem.Key;
            //            bindBuildingInputs.Add(item.Value, mapperItem.Value);
            //        }
            //    }
            //    else
            //    {
            //        splicesToDelete.Add(item.Value);
            //    }

            //    if (adapterModel == null) continue;
            //}

            ServiceResult<IEnumerable<ProjectAdapterSpliceModel>> serviceResult = new ServiceResult<IEnumerable<ProjectAdapterSpliceModel>>(spliceAdapterResult);

            var logger = Mock.Of<ILogger<ConstructionStageService>>();
            var adapter = new Mock<IProjectAdapter>(MockBehavior.Strict);
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            //storage.Setup(ctx => ctx.GetFibersWithProjectAdapterId()).ReturnsAsync(fiberToUpdates.ToDictionary(x => x.Key, x => x.Value));

            adapter.Setup(adp => adp.GetSplicesForBranchable(storedBranchableIds.First().Key)).ReturnsAsync(serviceResult);

            storage.Setup(ctx => ctx.GetBranchablesWithProjectAdapterId()).ReturnsAsync(storedBranchableIds);
            storage.Setup(ctx => ctx.GetSplicesFromBranchbaleWithProjectAdapterId(storedBranchableIds.First().Value)).ReturnsAsync(storedSpliceIds);

            storage.Setup(ctx => ctx.CheckIfCableExistsByProjectId(
  It.Is<String>(x => cableExistingResults.ContainsKey(x))))
  .ReturnsAsync<String, IBesaDataStorage, Boolean>((x) => cableExistingResults[x]);

            storage.Setup(ctx => ctx.GetCableIdByProjectId(
It.Is<String>(x => cableMapper.ContainsKey(x))))
.ReturnsAsync<String, IBesaDataStorage, Int32>((x) => cableMapper[x]);

            storage.Setup(ctx => ctx.CheckIfFiberExistsByProjectAdapterId(
It.Is<String>(x => fiberExistisResult.ContainsKey(x)),
It.Is<Int32>(x => cableMapper.ContainsValue(x))))
.ReturnsAsync<String, Int32, IBesaDataStorage, Boolean>((x,id) => fiberExistisResult[x]);

            storage.Setup(ctx => ctx.UpdateFiber(
                It.Is<ProjectAdapterFiberModel>(x => fiberToUpdates.ContainsKey(x.ProjectAdapterId))))
                .ReturnsAsync(true);

            storage.Setup(ctx => ctx.UpdateSpliceFromAdapter(
                It.Is<Int32>(x => storedSpliceIds.Values.Contains(x)),
                It.Is<ProjectAdapterSpliceModel>(x => splicesToUpdate.ContainsKey(x.ProjectAdapterId)),
                branchableId
                ))
                .ReturnsAsync(true);

            storage.Setup(ctx => ctx.AddSpliceFromAdapter(
              It.Is<FiberSpliceCreateModel>(x => FiberSpliceCreateModelTester(x,splicesToAdd)
              )))
              .ReturnsAsync<FiberSpliceCreateModel, IBesaDataStorage, Int32>((x) => spliceCreateIds[x.SpliceModel.ProjectAdapterId]);

            storage.Setup(ctx => ctx.AddFiberFromAdapter(
  It.Is<ProjectAdapterFiberModel>(x => fibersToCreate.ContainsKey(x.ProjectAdapterId)),
  It.Is<Int32>(x => cableMapper.ContainsValue(x))
  ))
  .ReturnsAsync<ProjectAdapterFiberModel, Int32, IBesaDataStorage, Int32>((x,id) => fiberCreateIds[x.ProjectAdapterId]);

            storage.Setup(ctx => ctx.DeleteSpliceAndFibers(It.Is<Int32>(x => splicesToDelete.Contains(x)))
    ).ReturnsAsync(true);

            String authUserId = base.PrepareStorage(random, storage, MessageActions.UpdateCablesFromAdapter, MessageRelatedObjectTypes.FiberCable);

            IConstructionStageService service = new ConstructionStageService(storage.Object, adapter.Object, logger,base.GetProtocolService(storage), Mock.Of<IUpdateTaskService>());
            await service.UpdateCablesFromAdapter(authUserId);
        }
    }
}
