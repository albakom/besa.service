﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.CustomerServiceTester
{
    public class CustomerServiceTester_CreateContact : ProtocolTesterBase
    {
        private static PersonInfo GetConactInfo(Random random)
        {
            return new PersonInfo
            {
                Address = new AddressModel
                {
                    City = $"Muserstadt {random.Next()}",
                    PostalCode = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Street = $"Straße Nr. {random.Next()}",
                    StreetNumber = $"{random.Next(1,100)}",
                },
                CompanyName = random.NextDouble() > 0.5 ? $"Firma Nr {random.Next()}" : String.Empty,
                EmailAddress = $"test@test-{random.Next()}.de",
                Phone = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                Lastname = $"Nachname {random.Next()}",
                Surname = $"Vorname {random.Next()}",
            };
        }

        [Fact(DisplayName = "CreateContact_Pass|CustomerServiceTester")]
        public async Task CreateContact_Pass()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 contactId = random.Next();

            PersonInfo contactModel = GetConactInfo(random);

            storage.Setup(ctx => ctx.CreateContact(contactModel)).ReturnsAsync(contactId);
            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Create, MessageRelatedObjectTypes.ContactInfo, contactId);

            ICustomerService service = new CustomerService(storage.Object, logger,base.GetProtocolService(storage));
            Int32 actualId = await service.CreateContact(contactModel,userAuthId);

            Assert.Equal(contactId, actualId);
        }

        [Fact(DisplayName = "CreateCustomer_Failed_NoModel|CustomerServiceTester")]
        public async Task CreateCustomer_Failed_NoModel()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Create, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger,base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateContact(null, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NoData, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_Failed_InvalidModel|CustomerServiceTester")]
        public async Task CreateCustomer_Failed_InvalidModel()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            Random random = new Random();

            List<PersonInfo> modelsToFail = new List<PersonInfo>();

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.City = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.Street = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.StreetNumber = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.PostalCode = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Lastname = null;
                modelsToFail.Add(failedContactModel);
            }

            //{
            //    PersonInfo failedContactModel = GetConactInfo(random);
            //    failedContactModel.EmailAddress = null;
            //    modelsToFail.Add(failedContactModel);
            //}

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Phone = null;
                modelsToFail.Add(failedContactModel);
            }

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.Create, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger,base.GetProtocolService(storage));

            foreach (PersonInfo item in modelsToFail)
            {
                CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateContact(item,userAuthId));
                Assert.Equal(CustomerServicExceptionReasons.InvalidData, exception.Reason);
            }
        }

    }
}
