﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.CustomerServiceTester
{
    public class CustomerServiceTester_CreateCustomer : ProtocolTesterBase
    {
        private Boolean ContactIdTester(IEnumerable<Int32> idsToCheck, HashSet<Int32> expectedIds)
        {
            HashSet<Int32> usedIds = new HashSet<int>(idsToCheck);

            if (usedIds.Count != expectedIds.Count) { return false; }

            foreach (Int32 item in usedIds)
            {
                if (expectedIds.Contains(item) == false) { return false; }
            }

            return true;
        }

        private static CustomerConnectRequestCreateModel GenerateValidCustomerCreateModel(Random random, int flatID, int archtiectId, int workmanID, int customerPersonId, int ownerPersonId)
        {
            return new CustomerConnectRequestCreateModel
            {
                FlatId = flatID,
                ActivePointNearSubscriberEndpoint = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                },
                ArchitectPersonId = archtiectId,
                ConnectionAppointment = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                CustomerPersonId = customerPersonId,
                CivilWorkIsDoneByCustomer = random.NextDouble() > 0.5,
                ConnectioOfOtherMediaRequired = random.NextDouble() > 0.5,
                DescriptionForHouseConnection = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                DuctAmount = random.NextDouble() > 0.5 ? random.Next(10, 20) + random.NextDouble() : new Nullable<Double>(),
                SubscriberEndpointLength = random.NextDouble() > 0.5 ? random.Next(40, 50) + random.NextDouble() : new Nullable<Double>(),
                OnlyHouseConnection = false,
                PowerForActiveEquipment = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                },

                PropertyOwnerPersonId = ownerPersonId,
                WorkmanPersonId = workmanID,
                SubscriberEndpointNearConnectionPoint = new ExplainedBooleanModel
                {
                    Value = random.NextDouble() > 0.5,
                    Description = $"Tolle Beschreibung mit dem Inhalt {random.Next()}",
                },
            };
        }

        private PersonInfo GetPerson(Random random)
        {
            PersonInfo info = new PersonInfo
            {
                Address = new AddressModel
                {
                    Street = $"Teststraße {random.Next()}",
                    City = $"Testort {random.Next()}",
                    PostalCode = random.Next(0, 10000).ToString(),
                    StreetNumber = random.Next(3, 10).ToString(),
                },
                Lastname = $"Blub {random.Next()}",
                Surname = $"Vonrmae  {random.Next()}",
                EmailAddress = "blub@nlub.de",
                CompanyName = "Testfirma {random.Next()}",
            };

            return info;
        }

        [Fact(DisplayName = "CreateCustomer_BuildingNotFinsiehd_Pass|CustomerServiceTester")]
        public async Task CreateCustomer_BuildingNotFinsiehd_Pass()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 requestId = random.Next();

            String buildingName = $"Testrstraße {random.Next()}";
            PersonInfo customerPerson = GetPerson(random);

            HashSet<Int32> peronIds = new HashSet<int> { archtiectId, workmanID, customerPersonId, ownerPersonId };

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);

            Int32 procedureId = random.Next();
            String userAuthId = Guid.NewGuid().ToString();

            storage.Setup(ctx => ctx.CheckIfFlatExists(flatID)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingIdByFlatId(flatID)).ReturnsAsync(buildingId);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, peronIds)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CreateCustomerConnenctionRequest(model)).ReturnsAsync(requestId);
            storage.Setup(ctx => ctx.GetContactById(customerPersonId)).ReturnsAsync(customerPerson);
            storage.Setup(ctx => ctx.GetBuildingStreetNameByFlatId(flatID)).ReturnsAsync(buildingName);
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
            String.IsNullOrEmpty(x.Name) == false &&
            x.Start == ProcedureStates.RequestCreated &&
            x.ExpectedEnd == ProcedureStates.ActivatitionJobAcknlowedged &&
            x.FlatId == flatID &&
            x.Appointment == model.ConnectionAppointment.Value &&
            x.CustomerContactId == customerPersonId &&
            x.FirstElement != null &&
            x.FirstElement.RelatedJobId.HasValue == false &&
            x.FirstElement.RelatedRequestId == requestId &&
            x.FirstElement.State == ProcedureStates.RequestCreated
          ))).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            var protocolService = new Mock<IProtocolService>(MockBehavior.Strict);

            protocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Action == MessageActions.Create &&
           x.TriggeredByUserId.HasValue == false &&
         Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) < 10 &&
         ( x.RelatedType == MessageRelatedObjectTypes.CustomerConnenctionProcedure && x.RelatedObjectId == procedureId) ||
         ( x.RelatedType == MessageRelatedObjectTypes.CustomerRequest && x.RelatedObjectId == requestId)
            ), userAuthId)).ReturnsAsync(random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, protocolService.Object);
            Int32 actualCustomerId = await service.CreateCustomerConnectRequest(model,userAuthId);
        }

        [Fact(DisplayName = "CreateCustomer_OnlyHouseConnection_Pass|CustomerServiceTester")]
        public async Task CreateCustomer_OnlyHouseConnection_Pass()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 requestId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { archtiectId, workmanID, ownerPersonId };

            String buildingName = $"Testrstraße {random.Next()}";
            PersonInfo ownerPerson = GetPerson(random);
            String userAuthId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, buildingId, archtiectId, workmanID, customerPersonId, ownerPersonId);
            model.FlatId = null;
            model.BuildingId = buildingId;
            model.CustomerPersonId = null;
            model.OnlyHouseConnection = true;
            model.InformSalesAfterFinish = random.NextDouble() > 0.5;

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, peronIds)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CreateCustomerConnenctionRequest(model)).ReturnsAsync(requestId);
            storage.Setup(ctx => ctx.GetBuildingStreetNameById(buildingId)).ReturnsAsync(buildingName);
            storage.Setup(ctx => ctx.GetContactById(ownerPersonId)).ReturnsAsync(ownerPerson);
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
              String.IsNullOrEmpty(x.Name) == false &&
              x.Start == ProcedureStates.RequestCreated &&
              x.ExpectedEnd == ProcedureStates.ConnectBuildingJobAcknlowedged &&
              x.BuildingId == buildingId &&
              x.OwnerContactId == ownerPersonId &&
              x.FirstElement != null &&
              x.FirstElement.RelatedJobId.HasValue == false &&
              x.FirstElement.RelatedRequestId == requestId &&
              x.FirstElement.State == ProcedureStates.RequestCreated && 
              x.InformSalesAfterFinish == model.InformSalesAfterFinish
            ))).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            var protocolService = new Mock<IProtocolService>(MockBehavior.Strict);

            protocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Action == MessageActions.Create &&
           x.TriggeredByUserId.HasValue == false &&
         Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) < 10 &&
         (x.RelatedType == MessageRelatedObjectTypes.BuildingConnenctionProcedure && x.RelatedObjectId == procedureId) ||
         (x.RelatedType == MessageRelatedObjectTypes.CustomerRequest && x.RelatedObjectId == requestId)
            ), userAuthId)).ReturnsAsync(random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, protocolService.Object);
            Int32 actualCustomerId = await service.CreateCustomerConnectRequest(model,userAuthId);
        }

        [Fact(DisplayName = "CreateCustomer_BuildingFinished_Pass|CustomerServiceTester")]
        public async Task CreateCustomer_BuildingFinished_Pass()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 requestId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { customerPersonId, workmanID, ownerPersonId, archtiectId };
            String buildingName = $"Testrstraße {random.Next()}";
            PersonInfo customerPerson = GetPerson(random);
            String userAuthId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
            if(random.NextDouble() > 0.5)
            {
                model.ActivationAsSoonAsPossible = true;
                model.ConnectionAppointment = null;
            }

            storage.Setup(ctx => ctx.CheckIfFlatExists(flatID)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingIdByFlatId(flatID)).ReturnsAsync(buildingId);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, peronIds)))).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CreateCustomerConnenctionRequest(model)).ReturnsAsync(requestId);
            storage.Setup(ctx => ctx.GetContactById(customerPersonId)).ReturnsAsync(customerPerson);
            storage.Setup(ctx => ctx.GetBuildingStreetNameByFlatId(flatID)).ReturnsAsync(buildingName);
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
            String.IsNullOrEmpty(x.Name) == false &&
            x.Start == ProcedureStates.RequestCreated &&
            x.ExpectedEnd == ProcedureStates.ActivatitionJobAcknlowedged &&
            x.FlatId == flatID &&
            x.Appointment == model.ConnectionAppointment &&
            x.AsSoonAsPossible == model.ActivationAsSoonAsPossible &&
            x.CustomerContactId == customerPersonId &&
            x.FirstElement != null &&
            x.FirstElement.RelatedJobId.HasValue == false &&
            x.FirstElement.RelatedRequestId == requestId &&
            x.FirstElement.State == ProcedureStates.RequestCreated
          ))).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            var protocolService = new Mock<IProtocolService>(MockBehavior.Strict);

            protocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Action == MessageActions.Create &&
           x.TriggeredByUserId.HasValue == false &&
         Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) < 10 &&
         (x.RelatedType == MessageRelatedObjectTypes.CustomerConnenctionProcedure && x.RelatedObjectId == procedureId) ||
         (x.RelatedType == MessageRelatedObjectTypes.CustomerRequest && x.RelatedObjectId == requestId)
            ), userAuthId)).ReturnsAsync(random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, protocolService.Object);
            Int32 actualCustomerId = await service.CreateCustomerConnectRequest(model,userAuthId);
        }

        [Fact(DisplayName = "CreateCustomer_NoModel_Failed|CustomerServiceTester")]
        public async Task CreateCustomer_NoModel_Failed()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(null, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NoData, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_OnlyConnectionWileBuildingIsFinished_Fail|CustomerServiceTester")]
        public async Task CreateCustomer_OnlyConnectionWileBuildingIsFinished_Fail()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 customerId = random.Next();
            Int32 jobId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { customerPersonId };

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
            model.OnlyHouseConnection = true;

            storage.Setup(ctx => ctx.CheckIfFlatExists(flatID)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingIdByFlatId(flatID)).ReturnsAsync(buildingId);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(model, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_OnlyHouseConnection_Fail_BuildingNotFound|CustomerServiceTester")]
        public async Task CreateCustomer_OnlyHouseConnection_Fail_BuildingNotFound()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 requestId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { archtiectId, workmanID, customerPersonId, ownerPersonId };

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, buildingId, archtiectId, workmanID, customerPersonId, ownerPersonId);
            model.FlatId = null;
            model.BuildingId = buildingId;
            model.CustomerPersonId = null;
            model.OnlyHouseConnection = true;

            storage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(model, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_FlatNotFound_Fail|CustomerServiceTester")]
        public async Task CreateCustomer_FlatNotFound_Fail()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 customerId = random.Next();
            Int32 jobId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { customerPersonId };

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
            model.OnlyHouseConnection = true;

            storage.Setup(ctx => ctx.CheckIfFlatExists(flatID)).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(model, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_InvalidModel_Fail|CustomerServiceTester")]
        public async Task CreateCustomer_InvalidModel_Fail()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 customerId = random.Next();
            Int32 jobId = random.Next();

            List<CustomerConnectRequestCreateModel> modelsToFail = new List<CustomerConnectRequestCreateModel>();
            {
                CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
                model.ConnectionAppointment = null;
                model.ActivationAsSoonAsPossible = false;

                modelsToFail.Add(model);
            }
            {
                CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
                model.CustomerPersonId = null;
                model.PropertyOwnerPersonId = null;

                modelsToFail.Add(model);
            }
            {
                CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
                model.FlatId = null;
                model.BuildingId = null;

                modelsToFail.Add(model);
            }
            {
                CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
                model.FlatId = null;
                model.PropertyOwnerPersonId = null;

                modelsToFail.Add(model);
            }
            {
                CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
                model.BuildingId = null;
                model.CustomerPersonId = null;

                modelsToFail.Add(model);
            }
           

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            foreach (CustomerConnectRequestCreateModel model in modelsToFail)
            {
                CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(model,userAuthId));
                Assert.Equal(CustomerServicExceptionReasons.InvalidData, exception.Reason);
            }
        }

        [Fact(DisplayName = "CreateCustomer_ContactNotFound_Fail|CustomerServiceTester")]
        public async Task CreateCustomer_ContactNotFound_Fail()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 buildingId = random.Next();
            Int32 flatID = random.Next();
            Int32 archtiectId = random.Next();
            Int32 workmanID = random.Next();
            Int32 customerPersonId = random.Next();
            Int32 ownerPersonId = random.Next();
            Int32 customerId = random.Next();
            Int32 jobId = random.Next();

            HashSet<Int32> peronIds = new HashSet<int> { customerPersonId };

            CustomerConnectRequestCreateModel model = GenerateValidCustomerCreateModel(random, flatID, archtiectId, workmanID, customerPersonId, ownerPersonId);
            model.OnlyHouseConnection = true;

            storage.Setup(ctx => ctx.CheckIfFlatExists(flatID)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.GetBuildingIdByFlatId(flatID)).ReturnsAsync(buildingId);
            storage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, peronIds)))).ReturnsAsync(false);

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateCustomerConnectRequest(model, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
