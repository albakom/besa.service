﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.CustomerServiceTester
{
    public class CustomerServiceTester_SearchContacts : ProtocolTesterBase
    {
        private static PersonInfo GetConactInfo(Random random)
        {
            return new PersonInfo
            {
                Address = new AddressModel
                {
                    City = $"Muserstadt {random.Next()}",
                    PostalCode = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Street = $"Straße Nr. {random.Next()}",
                    StreetNumber = $"{random.Next(1, 100)}",
                },
                CompanyName = random.NextDouble() > 0.5 ? $"Firma Nr {random.Next()}" : String.Empty,
                EmailAddress = $"test@test-{random.Next()}.de",
                Phone = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                Lastname = $"Nachname {random.Next()}",
                Surname = $"Vorname {random.Next()}",
            };
        }

        [Fact(DisplayName = "SearchContacts_Pass|CustomerServiceTester")]
        public async Task SearchContacts_Pass()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 contactAmount = random.Next(30, 100);

            List<PersonInfo> list = new List<PersonInfo>(contactAmount);
            for (int i = 0; i < contactAmount; i++)
            {
                PersonInfo info = GetConactInfo(random);
                info.Id = i + 1;
                list.Add(info);
            }

            Int32 amount = random.Next(1, 100);
            String query = "Testabfrage";

            storage.Setup(ctx => ctx.SearchContacts(query, amount)).ReturnsAsync(list);

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            IEnumerable<PersonInfo> actual = await service.SearchContacts(query, amount);

            Assert.NotNull(actual);
            Assert.Equal(list.Count, actual.Count());

            foreach (PersonInfo actualItem in actual)
            {
                Assert.NotNull(actualItem);

                PersonInfo expectedItem = list.FirstOrDefault(x => x.Id == actualItem.Id);
                Assert.NotNull(expectedItem);

                Assert.Equal(actualItem.Id, expectedItem.Id);
                Assert.Equal(actualItem.Lastname, expectedItem.Lastname);
                Assert.Equal(actualItem.Phone, expectedItem.Phone);
                Assert.Equal(actualItem.Surname, expectedItem.Surname);
                Assert.Equal(actualItem.Type, expectedItem.Type);
                Assert.Equal(actualItem.EmailAddress, expectedItem.EmailAddress);
                Assert.Equal(actualItem.CompanyName, expectedItem.CompanyName);

                Assert.NotNull(actualItem.Address);
                Assert.Equal(actualItem.Address.City, expectedItem.Address.City);
                Assert.Equal(actualItem.Address.Street, expectedItem.Address.Street);
                Assert.Equal(actualItem.Address.StreetNumber, expectedItem.Address.StreetNumber);
                Assert.Equal(actualItem.Address.PostalCode, expectedItem.Address.PostalCode);
            }
        }

        [Fact(DisplayName = "SearchContacts_NoQuery|CustomerServiceTester")]
        public async Task SearchContacts_NoQuery()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            IEnumerable<PersonInfo> actual = await service.SearchContacts(String.Empty, 1);

            Assert.NotNull(actual);
            Assert.True(actual.Count() == 0);
        }

        [Fact(DisplayName = "SearchContacts_InvalidAmount|CustomerServiceTester")]
        public async Task SearchContacts_InvalidAmount()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String query = "Testabfrage";

            List<Int32> invalidAmounts = new List<Int32> {
                -random.Next(),
                0,
                random.Next(101,1000),
            };

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));

            foreach (Int32 item in invalidAmounts)
            {
                CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.SearchContacts(query, item));
                Assert.Equal(CustomerServicExceptionReasons.InvalidOperation, exception.Reason);
            }
        }
    }
}
