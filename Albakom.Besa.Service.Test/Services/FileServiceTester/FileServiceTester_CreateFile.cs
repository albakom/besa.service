﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.FileServiceTester
{
    public class FileServiceTester_CreateFile : ProtocolTesterBase
    {
        private FileCreateModel GetValidModel(Random rand)
        {
            FileCreateModel createModel = new FileCreateModel
            {
                Extention = $"jpg",
                Name = $"Datei-{rand.Next()}",
                MimeType = "image/jpeg",
                Size = rand.Next(),
            };

            return createModel;
        }

        [Fact(DisplayName = "CreateFile_Pass|FileServiceTester")]
        public async Task CreateFile_Pass()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            FileCreateModel createModel = GetValidModel(rand);

            Byte[] pseudoData = new Byte[2048];
            rand.NextBytes(pseudoData);
            MemoryStream stream = new MemoryStream(pseudoData, false);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            mockUploader.Setup(f => f.Upload(createModel, stream, null)).ReturnsAsync(storageUrl);

            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CreateFile(createModel)).ReturnsAsync(fileId);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.File, fileId);

            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));
            Int32 acutalId = await service.CreateFile(createModel, stream, userAuthId);

            Assert.Equal(fileId, acutalId);

            Assert.True((DateTimeOffset.Now - createModel.CreatedAt) <= TimeSpan.FromMinutes(3));
            Assert.Equal(storageUrl, createModel.StorageUrl);
            Assert.Equal(FileTypes.Image, createModel.Type);
        }

        [Fact(DisplayName = "CreateFile_Fail_NoStream|FileServiceTester")]
        public async Task CreateFile_Fail_NoStream()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            FileCreateModel createModel = GetValidModel(rand);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.File, fileId);

            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));

            FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.CreateFile(createModel, null, userAuthId));
            Assert.Equal(FileServicExceptionReasons.InvalidFileData, exception.Reason);
        }

        [Fact(DisplayName = "CreateFile_Fail_NoModel|FileServiceTester")]
        public async Task CreateFile_Fail_NoModel()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            FileCreateModel createModel = GetValidModel(rand);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.File, fileId);

            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));

            FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.CreateFile(null, null, userAuthId));
            Assert.Equal(FileServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "CreateFile_Fail_InvalidModel|FileServiceTester")]
        public async Task CreateFile_Fail_InvalidModel()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            List<FileCreateModel> modelsToFail = new List<FileCreateModel>();
            {
                FileCreateModel createModel = GetValidModel(rand);
                createModel.Extention = String.Empty;
                modelsToFail.Add(createModel);
            }
            {
                FileCreateModel createModel = GetValidModel(rand);
                createModel.Name = String.Empty;
                modelsToFail.Add(createModel);
            }
            {
                FileCreateModel createModel = GetValidModel(rand);
                createModel.MimeType = String.Empty;
                modelsToFail.Add(createModel);
            }

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));

            String userAuthId = base.PrepareStorage(rand, mockStorage, MessageActions.Create, MessageRelatedObjectTypes.File, fileId);

            foreach (FileCreateModel item in modelsToFail)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.CreateFile(item, null, userAuthId));
                Assert.Equal(FileServicExceptionReasons.InvalidModel, exception.Reason);
            }
        }
    }
}
