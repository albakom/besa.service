﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.FileServiceTester
{
    public class FileServiceTester_GetDetails : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetDetails_Pass|FileServiceTester")]
        public async Task GetDetails_Pass()
        {
            Random rand = new Random();

            Int32 fileAmount = rand.Next(30, 100);
            List<FileAccessOverviewModel> result = new List<FileAccessOverviewModel>(fileAmount);
            List<FileDetailModel> fileDetails = new List<FileDetailModel>(fileAmount);
            List<FileOverviewModel> files = new List<FileOverviewModel>(fileAmount);

            for (int i = 0; i < fileAmount; i++)
            {
                FileAccessOverviewModel overviewModel = new FileAccessOverviewModel
                {
                    ObjectId = i + 1,
                    ObjectName = $"Toller Dateiname {rand.Next()}",
                    ObjectType = (FileAccessObjects)rand.Next(1, 4),
                };

                result.Add(overviewModel);
            }

            for (int i = 0; i < fileAmount; i++)
            {
                FileOverviewModel file = new FileOverviewModel
                {
                    Id = i + 1,
                    Extention = $"ext{i}",
                    Size = rand.Next(1, 100),
                    Name = $"Toller Dateiname {rand.Next()}",
                    Type = (FileTypes)rand.Next(1, 10)
                };

                FileDetailModel detail = new FileDetailModel
                {
                    Id = file.Id,
                    Name = file.Name,
                    Size = file.Size,
                    Type = file.Type,
                    Extention = file.Extention,
                    AccessControl = new List<FileAccessOverviewModel>(result.Take(rand.Next(1, fileAmount)))
                };

                fileDetails.Add(detail);
                files.Add(file);
            };
         
            Int32 amount = rand.Next(10, 20);
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            int fileID = fileDetails[rand.Next(1, fileDetails.Count)].Id;

            mockStorage.Setup(ctx => ctx.GetFilesSortByName(start, amount)).ReturnsAsync(files);
            mockStorage.Setup(ctx => ctx.CheckIfFileExists(fileID)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetFileDetails(fileID)).ReturnsAsync(fileDetails.FirstOrDefault(x => x.Id == fileID));

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));
            FileDetailModel fileDetail = await service.GetDetails(fileID);
            FileDetailModel expected = fileDetails.FirstOrDefault(x => x.Id == fileID);
            Assert.NotNull(fileDetail);

            Assert.Equal(expected.AccessControl, fileDetail.AccessControl);
            Assert.Equal(expected.Extention, fileDetail.Extention);
            Assert.Equal(expected.Id, fileDetail.Id);
            Assert.Equal(expected.Name, fileDetail.Name);
            Assert.Equal(expected.Size, fileDetail.Size);
            Assert.Equal(expected.Type, fileDetail.Type);
        }

        [Fact(DisplayName = "GetDetails_FileNotFound_Fail|FileServiceTester")]
        public async Task GetDetails_FileNotFound_Fail()
        {

            Random rand = new Random();

            Int32 fileAmount = rand.Next(30, 100);
            List<FileAccessOverviewModel> result = new List<FileAccessOverviewModel>(fileAmount);
            List<FileDetailModel> fileDetails = new List<FileDetailModel>(fileAmount);
            List<FileOverviewModel> files = new List<FileOverviewModel>(fileAmount);

            for (int i = 0; i < fileAmount; i++)
            {
                FileAccessOverviewModel overviewModel = new FileAccessOverviewModel
                {
                    ObjectId = i + 1,
                    ObjectName = $"Toller Dateiname {rand.Next()}",
                    ObjectType = (FileAccessObjects)rand.Next(1, 4),
                };

                result.Add(overviewModel);
            }

            for (int i = 0; i < fileAmount; i++)
            {
                FileOverviewModel file = new FileOverviewModel
                {
                    Id = i + 1,
                    Extention = $"ext{i}",
                    Size = rand.Next(1, 100),
                    Name = $"Toller Dateiname {rand.Next()}",
                    Type = (FileTypes)rand.Next(1, 10)
                };

                FileDetailModel detail = new FileDetailModel
                {
                    Id = file.Id,
                    Name = file.Name,
                    Size = file.Size,
                    Type = file.Type,
                    Extention = file.Extention,
                    AccessControl = new List<FileAccessOverviewModel>(result.Take(rand.Next(1, fileAmount)))
                };

                fileDetails.Add(detail);
                files.Add(file);
            };

            Int32 amount = rand.Next(10, 20);
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            //int fileID = fileDetails[rand.Next(1, fileDetails.Count)].Id;
            int fileID = files[rand.Next(1, fileAmount)].Id;



            mockStorage.Setup(ctx => ctx.GetFilesSortByName(start, amount)).ReturnsAsync(files.Where(x => x.Id != fileID));
            mockStorage.Setup(ctx => ctx.CheckIfFileExists(fileID)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetFileDetails(fileID)).ReturnsAsync(fileDetails.FirstOrDefault(x => x.Id == fileID));

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));

            FileServicException ex = await Assert.ThrowsAsync<FileServicException>(() => service.GetDetails(fileID));
            Assert.Equal(FileServicExceptionReasons.NotFound, ex.Reason);
            
        }

        /*
        [Fact]
        public async Task GetFiles_InvalidAmount_Fail()
        {
            Random rand = new Random();

            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger);

            Int32[] invalidAmounts = new Int32[] { 0, -rand.Next(), -rand.Next(1, FileService.MaxQueryAmount), FileService.MaxQueryAmount + 1, rand.Next(FileService.MaxQueryAmount, Int32.MaxValue) };

            foreach (Int32 amount in invalidAmounts)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.GetFiles(start, amount));
                Assert.Equal(FileServicExceptionReasons.InvalidParameter, exception.Reason);
            }
        }

        [Fact]
        public async Task SearchFile_InvalidStart_Fail()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(10, 20);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger);

            Int32[] invalidStarts = new Int32[] { -1, -rand.Next() };

            foreach (Int32 start in invalidStarts)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.GetFiles(start, amount));
                Assert.Equal(FileServicExceptionReasons.InvalidParameter, exception.Reason);
            }
        }*/
    }
}
