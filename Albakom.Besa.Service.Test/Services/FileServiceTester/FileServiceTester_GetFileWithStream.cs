﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.FileServiceTester
{
    public class FileServiceTester_GetFileWithStream : ProtocolTesterBase
    {
        [Fact(DisplayName = "GetFileWithStream_Pass|FileServiceTester")]
        public async Task GetFileWithStream_Pass()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            Byte[] pseudoData = new Byte[2048];
            rand.NextBytes(pseudoData);
            MemoryStream stream = new MemoryStream(pseudoData, false);

            FileModel expected = new FileModel
            {
                Extention = $"jpg",
                Name = $"Datei-{rand.Next()}",
                MimeType = "image/jpeg",
                Size = pseudoData.Length,
                StorageUrl = storageUrl,
                Id = fileId,
                Type = FileTypes.Image,
            };

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            mockUploader.Setup(f => f.Download(expected.StorageUrl, expected.Size)).ReturnsAsync(stream);

            mockStorage.Setup(ctx => ctx.CheckIfFileExists(fileId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetFileInfo(fileId)).ReturnsAsync(expected);


            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));
            FileWithStreamModel actualModel = await service.GetFileWithStream(fileId);

            Assert.NotNull(actualModel);

            Assert.Equal(fileId, actualModel.Id);
            Assert.Equal(expected.Extention, actualModel.Extention);
            Assert.Equal(expected.MimeType, actualModel.MimeType);
            Assert.Equal(expected.Name, actualModel.Name);

            Assert.NotNull(actualModel.Data);

            Assert.Equal(0, actualModel.Data.Position);

            Int32 index = 0;
            using (BinaryReader reader = new BinaryReader(actualModel.Data))
            {
                Byte actualData = reader.ReadByte();
                Byte expectedData = pseudoData[index];

                Assert.Equal(expectedData, actualData);

                index += 1;
            }
        }

        [Fact(DisplayName = "GetFileWithStream_Fail_FileNotFound|FileServiceTester")]
        public async Task GetFileWithStream_Fail_FileNotFound()
        {
            Random rand = new Random();

            String storageUrl = $"http://random-url.de/{rand.Next()}";
            Int32 fileId = rand.Next();

            FileModel expected = new FileModel
            {
                Extention = $"jpg",
                Name = $"Datei-{rand.Next()}",
                MimeType = "image/jpeg",
                Size = rand.Next(),
                StorageUrl = storageUrl,
                Id = fileId,
                Type = FileTypes.Image,
            };

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = new Mock<IFileManager>(MockBehavior.Strict);

            mockStorage.Setup(ctx => ctx.CheckIfFileExists(fileId)).ReturnsAsync(false);

            IFileService service = new FileService(mockStorage.Object, mockUploader.Object, logger, base.GetProtocolService(mockStorage));

            FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.GetFileWithStream(fileId));
            Assert.Equal(FileServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
