﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.FileServiceTester
{
    public class FileServiceTester_SearchFile : ProtocolTesterBase
    {
        [Fact(DisplayName = "SearchFile_Pass|FileServiceTester")]
        public async Task SearchFile_Pass()
        {
            Random rand = new Random();

            Int32 fileAmount = rand.Next(30, 100);
            List<FileOverviewModel> result = new List<FileOverviewModel>(fileAmount);

            for (int i = 0; i < fileAmount; i++)
            {
                FileOverviewModel overviewModel = new FileOverviewModel
                {
                    Extention = "jpg",
                    Id = i + 1,
                    Name = $"Toller Dateiname {rand.Next()}",
                    Size = rand.Next(),
                    Type = (FileTypes)rand.Next(1, 10),
                };

                result.Add(overviewModel);
            }
            String query = "ler dat";
            Int32 amount = rand.Next(10, 20);
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.SearchFiles(query, start, amount)).ReturnsAsync(result);

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));
            IEnumerable<FileOverviewModel> actual = await service.SearchFiles(query, start, amount);

            Assert.NotNull(actual);

            foreach (FileOverviewModel acutalItem in actual)
            {
                FileOverviewModel expectedItem = result.FirstOrDefault(x => x.Id == acutalItem.Id);

                Assert.NotNull(expectedItem);
                Assert.Equal(expectedItem.Id, acutalItem.Id);
                Assert.Equal(expectedItem.Extention, acutalItem.Extention);
                Assert.Equal(expectedItem.Name, acutalItem.Name);
                Assert.Equal(expectedItem.Size, acutalItem.Size);
                Assert.Equal(expectedItem.Type, acutalItem.Type);
            }
        }

        [Fact(DisplayName = "SearchFile_EmptyQuery_Pass|FileServiceTester")]
        public async Task SearchFile_EmptyQuery_Pass()
        {
            Random rand = new Random();

            String query = String.Empty;
            Int32 amount = rand.Next(10, 20);
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));
            IEnumerable<FileOverviewModel> actual = await service.SearchFiles(query, start, amount);

            Assert.NotNull(actual);
            Assert.True(actual.Count() == 0);
        }

        [Fact(DisplayName = "SearchFile_InvalidAmount_Fail|FileServiceTester")]
        public async Task SearchFile_InvalidAmount_Fail()
        {
            Random rand = new Random();

            String query = "ler Dat";
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));

            Int32[] invalidAmounts = new Int32[] { 0, -rand.Next(), -rand.Next(1, FileService.MaxQueryAmount), FileService.MaxQueryAmount + 1, rand.Next(FileService.MaxQueryAmount, Int32.MaxValue) };

            foreach (Int32 item in invalidAmounts)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.SearchFiles(query, start, item));
                Assert.Equal(FileServicExceptionReasons.InvalidParameter, exception.Reason);
            }
        }

        [Fact(DisplayName = "SearchFile_InvalidStart_Fail|FileServiceTester")]
        public async Task SearchFile_InvalidStart_Fail()
        {
            Random rand = new Random();

            String query = "ler Dat";
            Int32 amount = rand.Next(10, 20);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));

            Int32[] invalidAmounts = new Int32[] { -1, -rand.Next() };

            foreach (Int32 item in invalidAmounts)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.SearchFiles(query, item, amount));
                Assert.Equal(FileServicExceptionReasons.InvalidParameter, exception.Reason);
            }
        }
    }
}
