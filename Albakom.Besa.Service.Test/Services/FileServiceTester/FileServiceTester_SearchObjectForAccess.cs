﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.FileServiceTester
{
    public class FileServiceTester_SearchObjectForAccess : ProtocolTesterBase
    {
        [Fact(DisplayName = "SearchObjectForAccess_Pass|FileServiceTester")]
        public async Task SearchObjectForAccess_Pass()
        {
            Random rand = new Random();

            Int32 buildingAmount = rand.Next(30, 100);
            Int32 branchOffAmount = rand.Next(30, 100);
            Int32 connenctionAmount = rand.Next(30, 100);

            String query = String.Empty;
            Int32 amount = rand.Next(10, 20);

            Dictionary<Int32, String> branchOffResult = new Dictionary<int, string>(branchOffAmount);
            Dictionary<Int32, String> connenctionResult = new Dictionary<int, string>(connenctionAmount);
            List<SimpleBuildingOverviewModel> buildingResult = new List<SimpleBuildingOverviewModel>(buildingAmount);

            for (int i = 0; i < buildingAmount; i++)
            {
                buildingResult.Add(new SimpleBuildingOverviewModel
                {
                    CommercialUnits = rand.Next(0, 5),
                    HouseholdUnits = rand.Next(0, 10),
                    Id = i + 1,
                    StreetName = $"Testr. {rand.Next()}",
                });
            }

            for (int i = 0; i < branchOffAmount; i++)
            {
                branchOffResult.Add(i + i, $"Testr. {rand.Next()}");
            }

            for (int i = 0; i < connenctionAmount; i++)
            {
                connenctionResult.Add(i + i, $"Testr. {rand.Next()}");
            }
            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.SearchBuildingsAndGetBranchOffJobIds(query, amount)).ReturnsAsync(branchOffResult);
            mockStorage.Setup(ctx => ctx.SearchBuildingsAndGetConnenctionJobIds(query, amount)).ReturnsAsync(connenctionResult);
            mockStorage.Setup(ctx => ctx.SearchStreets(query, amount)).ReturnsAsync(buildingResult);

            Dictionary<FileAccessObjects, IDictionary<Int32, String>> expectedResult = new Dictionary<FileAccessObjects, IDictionary<int, string>>();
            expectedResult.Add(FileAccessObjects.Building, buildingResult.ToDictionary(x => x.Id, x => x.StreetName));
            expectedResult.Add(FileAccessObjects.BranchOffJob, branchOffResult);
            expectedResult.Add(FileAccessObjects.HouseConnectionJob, connenctionResult);

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));

            foreach (KeyValuePair<FileAccessObjects, IDictionary<Int32, String>> item in expectedResult)
            {
                IEnumerable<FileObjectOverviewModel> actual = await service.SearchObjectForAccess(item.Key, query, amount);

                Assert.NotNull(actual);

                IDictionary<Int32, String> expectedItems = item.Value;

                foreach (FileObjectOverviewModel actualItem in actual)
                {
                    Assert.True(expectedItems.ContainsKey(actualItem.Id));
                    Assert.Equal(expectedItems[actualItem.Id], actualItem.Name);
                }
            }
        }

        [Fact(DisplayName = "SearchObjectForAccess_EmptyQuery_Pass|FileServiceTester")]
        public async Task SearchObjectForAccess_EmptyQuery_Pass()
        {
            Random rand = new Random();

            String query = String.Empty;
            Int32 amount = rand.Next(10, 20);
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));
            IEnumerable<FileObjectOverviewModel> actual = await service.SearchObjectForAccess(FileAccessObjects.Building, query, amount);

            Assert.NotNull(actual);
            Assert.True(actual.Count() == 0);
        }

        [Fact(DisplayName = "SearchObjectForAccess_InvalidAmount_Fail|FileServiceTester")]
        public async Task SearchObjectForAccess_InvalidAmount_Fail()
        {
            Random rand = new Random();

            String query = "ler Dat";
            Int32 start = rand.Next(0, 10);

            var logger = Mock.Of<ILogger<FileService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockUploader = Mock.Of<IFileManager>();

            IFileService service = new FileService(mockStorage.Object, mockUploader, logger, base.GetProtocolService(mockStorage));

            Int32[] invalidAmounts = new Int32[] { 0, -rand.Next(), -rand.Next(1, FileService.MaxQueryAmount), FileService.MaxQueryAmount + 1, rand.Next(FileService.MaxQueryAmount, Int32.MaxValue) };

            foreach (Int32 item in invalidAmounts)
            {
                FileServicException exception = await Assert.ThrowsAsync<FileServicException>(() => service.SearchObjectForAccess(FileAccessObjects.HouseConnectionJob, query, item));
                Assert.Equal(FileServicExceptionReasons.InvalidParameter, exception.Reason);
            }
        }
    }
}
