﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Albakom.Besa.Service.Test.Services.HttpBasedProjectAdapterTester
{
    public class HttpBasedProjectAdapter_GetAllBuildings
    {
        [Fact]
        public async Task GetAllBuildings_TokenUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(false);


            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await adapter.GetAllCabinets();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetAllBuildings_TokenFirstUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await adapter.GetAllCabinets();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetAllBuildings_TokenServiceAvaibleWithNoToken()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await adapter.GetAllCabinets();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetAllBuildings_Errors()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            Dictionary<HttpStatusCode, ServiceErrros> erros = new Dictionary<HttpStatusCode, ServiceErrros>();
            erros.Add(HttpStatusCode.Forbidden, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.Unauthorized, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.InternalServerError, ServiceErrros.UnknowError);
            erros.Add(HttpStatusCode.NotFound, ServiceErrros.NotFound);
            erros.Add(HttpStatusCode.OK, ServiceErrros.UnknowError);

            foreach (KeyValuePair<HttpStatusCode, ServiceErrros> error in erros)
            {
                httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(new HttpResponseMessage(error.Key));

                ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await adapter.GetAllCabinets();

                Assert.NotNull(result);
                Assert.True(result.HasError);
                Assert.Equal(error.Value, result.Error);
            }
        }

        [Fact]
        public async Task GetAllBuildings_Pass()
        {
            Random random = new Random();

            Int32 amount = random.Next(3,10);

            List<ProjectAdapterCabinetModel> models = new List<ProjectAdapterCabinetModel>();
            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterCabinetModel item = new ProjectAdapterCabinetModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = random.Next(30, 40) + random.NextDouble(),
                        Longitude = random.Next(30, 40) + random.NextDouble(),
                    },
                    Name = $"KVZ Nr {i}",
                };

                Int32 buildingAmount = random.Next(5, 10);
                List<ProjectAdapterBuildingModel> buildings = new List<ProjectAdapterBuildingModel>(buildingAmount);
                for (int j = 0; j < buildingAmount; j++)
                {
                    ProjectAdapterBuildingModel buildingItem = new ProjectAdapterBuildingModel
                    {
                        AdapterId = Guid.NewGuid().ToString(),
                        CommercialUnits = random.Next(3, 10),
                        HouseholdUnits = random.Next(3, 10),
                        Coordinate = new GPSCoordinate
                        {
                            Latitude = random.Next(30, 40) + random.NextDouble(),
                            Longitude = random.Next(30, 40) + random.NextDouble(),
                        },
                        Street = $"Tolle Straße {random.Next()}",
                    };

                    buildings.Add(buildingItem);
                }

                item.Buildings = buildings;

                models.Add(item);
            }

            String rawContent = Newtonsoft.Json.JsonConvert.SerializeObject(models);
            StringContent content = new StringContent(rawContent, new UTF8Encoding(), "application/json");

            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = content;
            httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(response);

            ServiceResult<IEnumerable<ProjectAdapterCabinetModel>> result = await adapter.GetAllCabinets();

            Assert.NotNull(result);
            Assert.False(result.HasError);
            Assert.NotNull(result.Data);
            Assert.Equal(models.Count, result.Data.Count());

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterCabinetModel expected = models[i];
                ProjectAdapterCabinetModel actual = result.Data.ElementAt(i);

                Assert.Equal(expected.AdapterId, actual.AdapterId);
                Assert.Equal(expected.Coordinate.Latitude, actual.Coordinate.Latitude);
                Assert.Equal(expected.Coordinate.Longitude, actual.Coordinate.Longitude);

                Assert.Equal(expected.Buildings.Count(), actual.Buildings.Count());

                for (int j = 0; j < expected.Buildings.Count(); j++)
                {
                    ProjectAdapterBuildingModel expectedBuilding = expected.Buildings.ElementAt(j);
                    ProjectAdapterBuildingModel actualBuilding = actual.Buildings.ElementAt(j);

                    Assert.Equal(expectedBuilding.CommercialUnits, actualBuilding.CommercialUnits);
                    Assert.Equal(expectedBuilding.HouseholdUnits, actualBuilding.HouseholdUnits);
                    Assert.Equal(expectedBuilding.Street, actualBuilding.Street);

                    Assert.Equal(expectedBuilding.HouseholdUnits, actualBuilding.HouseholdUnits);
                    Assert.NotNull(actual.Coordinate);
                    Assert.Equal(expectedBuilding.Coordinate.Latitude, actualBuilding.Coordinate.Latitude);
                    Assert.Equal(expectedBuilding.Coordinate.Longitude, actualBuilding.Coordinate.Longitude);
                }
            }
        }
    }
}
