﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Albakom.Besa.Service.Test.Services.HttpBasedProjectAdapterTester
{
    public class HttpBasedProjectAdapter_GetAllPoPs
    {
        [Fact(DisplayName = "GetAllPoPs_TokenUnavaible|HttpBasedProjectAdapterTester")]
        public async Task GetAllPoPs_TokenUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);
            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await adapter.GetAllPoPs();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact(DisplayName = "GetAllPoPs_TokenFirstUnavaible|HttpBasedProjectAdapterTester")]
        public async Task GetAllPoPs_TokenFirstUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await adapter.GetAllPoPs();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact(DisplayName = "GetAllPoPs_TokenServiceAvaibleWithNoToken|HttpBasedProjectAdapterTester")]
        public async Task GetAllPoPs_TokenServiceAvaibleWithNoToken()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);
            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await adapter.GetAllPoPs();

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact(DisplayName = "GetAllPoPs_Errors|HttpBasedProjectAdapterTester")]
        public async Task GetAllPoPs_Errors()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            Dictionary<HttpStatusCode, ServiceErrros> erros = new Dictionary<HttpStatusCode, ServiceErrros>();
            erros.Add(HttpStatusCode.Forbidden, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.Unauthorized, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.InternalServerError, ServiceErrros.UnknowError);
            erros.Add(HttpStatusCode.NotFound, ServiceErrros.NotFound);
            erros.Add(HttpStatusCode.OK, ServiceErrros.UnknowError);

            foreach (KeyValuePair<HttpStatusCode, ServiceErrros> error in erros)
            {
                httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(new HttpResponseMessage(error.Key));

                ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await adapter.GetAllPoPs();

                Assert.NotNull(result);
                Assert.True(result.HasError);
                Assert.Equal(error.Value, result.Error);
            }
        }

        [Fact(DisplayName = "GetAllPoPs_Pass|HttpBasedProjectAdapterTester")]
        public async Task GetAllPoPs_Pass()
        {
            Random random = new Random();
            Int32 amount = random.Next(3, 10);

            List<ProjectAdapterPoPModel> models = new List<ProjectAdapterPoPModel>();
            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterPoPModel item = new ProjectAdapterPoPModel
                {
                    ProjectAdapterId = Guid.NewGuid().ToString(),
                    Name = $"Kabeltype Nr. {random.Next()}",
                    Location = new GPSCoordinate { Latitude = random.Next(20,30) + random.NextDouble(), Longitude = random.Next(20, 30) + random.NextDouble() },
                };

                List<String> branchables = new List<string>();
                Int32 branchableAmount = random.Next(3, 10);
                for (int j = 0; j < branchableAmount; j++)
                {
                    branchables.Add(Guid.NewGuid().ToString());
                }

                item.Branchables = branchables;

                models.Add(item);
            }

            String rawContent = Newtonsoft.Json.JsonConvert.SerializeObject(models);
            StringContent content = new StringContent(rawContent, new UTF8Encoding(), "application/json");

            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);
            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = content;
            httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(response);

            ServiceResult<IEnumerable<ProjectAdapterPoPModel>> result = await adapter.GetAllPoPs();

            Assert.NotNull(result);
            Assert.False(result.HasError);
            Assert.NotNull(result.Data);
            Assert.Equal(models.Count, result.Data.Count());

            for (int i = 0; i < amount; i++)
            {
                ProjectAdapterPoPModel expected = models[i];
                ProjectAdapterPoPModel actual = result.Data.ElementAt(i);

                Assert.Equal(expected.ProjectAdapterId, actual.ProjectAdapterId);
                Assert.Equal(expected.Name, actual.Name);

                Assert.NotNull(actual.Location);
                Assert.Equal(expected.Location.Latitude, actual.Location.Latitude);
                Assert.Equal(expected.Location.Longitude, actual.Location.Longitude);

                Assert.Equal(expected.Branchables, actual.Branchables);
            }
        }
    }
}
