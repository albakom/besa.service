﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
namespace Albakom.Besa.Service.Test.Services.HttpBasedProjectAdapterTester
{
    public class HttpBasedProjectAdapter_GetBuildingCableInfo
    {
        [Fact]
        public async Task GetBuildingCableInfo_TokenUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);
            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await adapter.GetBuildingCableInfo(Guid.NewGuid().ToString());

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetBuildingCableInfo_TokenFirstUnavaible()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(false);
            tokenHandler.Setup(tk => tk.Prepare()).ReturnsAsync(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await adapter.GetBuildingCableInfo(Guid.NewGuid().ToString());

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetBuildingCableInfo_TokenServiceAvaibleWithNoToken()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);
            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(false);
            tokenHandler.Setup(tk => tk.RequestToken()).ReturnsAsync(false);

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);
            ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await adapter.GetBuildingCableInfo(Guid.NewGuid().ToString());

            Assert.NotNull(result);
            Assert.True(result.HasError);
            Assert.Equal(ServiceErrros.TokenUnvaible, result.Error);
        }

        [Fact]
        public async Task GetBuildingCableInfo_Errors()
        {
            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);

            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            Dictionary<HttpStatusCode, ServiceErrros> erros = new Dictionary<HttpStatusCode, ServiceErrros>();
            erros.Add(HttpStatusCode.Forbidden, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.Unauthorized, ServiceErrros.NotAuthorize);
            erros.Add(HttpStatusCode.InternalServerError, ServiceErrros.UnknowError);
            erros.Add(HttpStatusCode.NotFound, ServiceErrros.NotFound);
            erros.Add(HttpStatusCode.OK, ServiceErrros.UnknowError);

            foreach (KeyValuePair<HttpStatusCode, ServiceErrros> error in erros)
            {
                httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(new HttpResponseMessage(error.Key));

                ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await adapter.GetBuildingCableInfo(Guid.NewGuid().ToString());

                Assert.NotNull(result);
                Assert.True(result.HasError);
                Assert.Equal(error.Value, result.Error);
            }
        }

        [Fact]
        public async Task GetBuildingCableInfo_Pass()
        {
            Random random = new Random();
            Int32 amount = random.Next(3, 10);

            String buildingAdapterId = Guid.NewGuid().ToString();

            ProjectAdapterUpdateBuldingCableModel model = new ProjectAdapterUpdateBuldingCableModel
            {
                AdapterCableTypeId = Guid.NewGuid().ToString(),
                Length = random.Next(100, 400) + random.NextDouble(),
                AdapterId = buildingAdapterId,
            };

            String rawContent = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(rawContent, new UTF8Encoding(), "application/json");

            String baseUri = "http://localhost:45121/";

            var logger = Mock.Of<ILogger<HttpBasedProjectAdapter>>();
            var httpHandler = new Mock<IHttpHandler>(MockBehavior.Strict);
            var tokenHandler = new Mock<ITokenClient>(MockBehavior.Strict);

            String tokenValue = "sgsgsgsgswer2422f";

            tokenHandler.Setup(tk => tk.IsReady()).Returns(true);
            tokenHandler.Setup(tk => tk.HasToken()).Returns(true);
            tokenHandler.Setup(tk => tk.GetToken()).Returns(tokenValue);

            httpHandler.Setup(http => http.AddHeader("Authorization", $"Bearer {tokenValue}"));

            IProjectAdapter adapter = new HttpBasedProjectAdapter(baseUri, httpHandler.Object, tokenHandler.Object, logger);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = content;
            httpHandler.Setup(http => http.GetAsync(It.IsAny<String>())).ReturnsAsync(response);

            ServiceResult<ProjectAdapterUpdateBuldingCableModel> result = await adapter.GetBuildingCableInfo(buildingAdapterId);

            Assert.NotNull(result);
            Assert.False(result.HasError);
            Assert.NotNull(result.Data);

            ProjectAdapterUpdateBuldingCableModel actual = result.Data;

            Assert.Equal(model.AdapterId, actual.AdapterId);
            Assert.Equal(model.Length, actual.Length);
            Assert.Equal(model.AdapterCableTypeId, actual.AdapterCableTypeId);
        }
    }
}
