﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.DataAccess.Context;

namespace Albakom.Besa.Service.Test.Services.ImportServiceTester
{
    public class ImportServiceTester_ImportProcedures : ProtocolTesterBase
    {
        private static PersonInfo GetConactInfo(Random random)
        {
            return new PersonInfo
            {
                Address = new AddressModel
                {
                    City = $"Muserstadt {random.Next()}",
                    PostalCode = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                    Street = $"Straße Nr. {random.Next()}",
                    StreetNumber = $"{random.Next(1, 100)}",
                },
                CompanyName = random.NextDouble() > 0.5 ? $"Firma Nr {random.Next()}" : String.Empty,
                EmailAddress = $"test@test-{random.Next()}.de",
                Phone = $"{random.Next(0, 99999)}".PadLeft(5, '0'),
                Lastname = $"Nachname {random.Next()}",
                Surname = $"Vorname {random.Next()}",
                Id = random.Next(),
            };
        }

        private static CSVProcedureImportInfoModel GetMinimalCSVProcedureImportInfoModel()
        {
            CSVProcedureImportInfoModel importInfoModel = new CSVProcedureImportInfoModel();
            importInfoModel.RolebackIfErrorHappend = importInfoModel.SkipError = true;
            importInfoModel.Mappings.Add(ProcedureImportProperties.ContactSurname, 0);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ContactLastname, 1);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Street, 2);
            importInfoModel.Mappings.Add(ProcedureImportProperties.StreetNumber, 3);

            return importInfoModel;
        }

        private static SimpleBuildingOverviewModel GetBuildingOverviewModel(Random random, out String street, out String streetNumber)
        {
            street = $"Tolle Straße {random.Next()}";
            streetNumber = random.Next().ToString();

            SimpleBuildingOverviewModel simpleBuilding = new SimpleBuildingOverviewModel
            {
                CommercialUnits = random.Next(3, 10),
                HouseholdUnits = random.Next(1, 10),
                Id = random.Next(),
                StreetName = $"{street} {streetNumber}",
            };

            return simpleBuilding;
        }

        private static SimpleFlatOverviewModel GetFlat(Random random, Int32 buildingId)
        {
            SimpleFlatOverviewModel flatOverviewModel = new SimpleFlatOverviewModel
            {
                BuildingId = buildingId,
                Id = random.Next(),
                Number = $"Nr {random.Next()}",
            };

            return flatOverviewModel;
        }

        private static void PrepareStorage(Mock<IBesaDataStorage> storage, SimpleBuildingOverviewModel simpleBuilding, PersonInfo personInfo)
        {
            storage.Setup(ctx => ctx.CheckIfContactExists(personInfo.Surname, personInfo.Lastname)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetContactByName(personInfo.Surname, personInfo.Lastname)).ReturnsAsync(personInfo);
            storage.Setup(ctx => ctx.CheckIfBuildingExists(simpleBuilding.StreetName)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingByStreetName(simpleBuilding.StreetName)).ReturnsAsync(simpleBuilding);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_WithInformSales_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_WithInformSales_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x" });
            importInfoModel.Lines = lines;
            importInfoModel.Mappings.Add(ProcedureImportProperties.InformSalesAfterFinished, 4);

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && x.InformSalesAfterFinish == true)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_Finished_WithInformSales_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_Finished_WithInformSales_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x", "x" });
            importInfoModel.Lines = lines;
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InformSalesAfterFinished, 5);

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && x.InformSalesAfterFinish == true)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))).ReturnsAsync(random.Next());

            storage.Setup(ctx => ctx.CreateContactAfterFinishedJob(It.Is<CreateContactAfterFinishedJobModel>(x =>
x.BuildingId == simpleBuilding.Id && x.FlatId == flatOverviewModel.Id && x.RelatedProcedureId == procedureId))).ReturnsAsync(random.Next());

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_FinishedWIthJobId_WithInformSales_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_FinishedWIthJobId_WithInformSales_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x", "x" });
            importInfoModel.Lines = lines;
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InformSalesAfterFinished, 5);

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && x.InformSalesAfterFinish == true)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId)
)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CreateContactAfterFinishedJob(It.Is<CreateContactAfterFinishedJobModel>(x =>
x.BuildingId == simpleBuilding.Id && x.FlatId == flatOverviewModel.Id && x.RelatedProcedureId == procedureId))).ReturnsAsync(random.Next());

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_WithFlatCreate_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_WithFlatCreate_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { });
            storage.Setup(ctx => ctx.CreateFlat(It.Is<FlatCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && String.IsNullOrEmpty(x.Description) == false)
            )).ReturnsAsync(flatOverviewModel.Id);

            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnectionWithActiveRequest_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnectionWithActiveRequest_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 6);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == false)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnectionWithActiveRequest_AndAsSoonAsPossibleValueSet_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnectionWithActiveRequest_AndAsSoonAsPossibleValueSet_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 6);

            String asapString = "ASAP";

            importInfoModel.AsSoonAsPossibleString = asapString;

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, asapString, "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false &&  x.AsSoonAsPossible == true)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == false)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }


        [Fact(DisplayName = "ImportProcedures_HouseConnectionFished_WithActiveRequest_Pass|ImportServiceTester")]
        public async Task ImportProcedures_HouseConnectionFished_WithActiveRequest_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();

            List<Int32> jobIds = new List<int> { injectJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 7);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == false)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_JobExists_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_JobExists_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                           x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                           (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                           (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                           ))).
                           ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_Finsiehd_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_Finsiehd_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_JobExist_Finished_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_JobExist_Finished_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }


        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_JobExistAndFinsiehd_Finished_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_JobExistAndFinsiehd_Finished_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId)
)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_JobExistFinsiehdAndAcknowledged_Finished_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_JobExistFinsiehdAndAcknowledged_Finished_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 4);
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
           x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }


        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnectionWithActiveRequest_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithoutConnectionJob_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { connectBuildingJobId, injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == false)
)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>(x =>
 x.BuildingId == simpleBuilding.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WitFinsiehdConnectionJob_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WitFinsiehdConnectionJob_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>(x =>
 x.BuildingId == simpleBuilding.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithInjectJobCreated_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithInjectJobCreated_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(false);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithInjectJobFinsihed_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithInjectJobFinsihed_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);


            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => false);

            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x => x.JobId == injectJobId && x.UserAuthId == authId))).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithInjectJobFinsihedAndAcknowledged_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithInjectJobFinsihedAndAcknowledged_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);

            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithSpliceJobExists_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithSpliceJobExists_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithSpliceJobFinished_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithSpliceJobFinished_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId);

            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == spliceBranchableJobid && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithSpliceJobAcknowleged_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithSpliceJobAcknowleged_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(new Int32?());

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithActivationJobExists_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithActivationJobExists_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id)
)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithActivationJobFinsiehd_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithActivationJobFinsiehd_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid || x == activationJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid);

            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == activationJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithActivationJobFinishedAndAcknowledged_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithActivationJobFinishedAndAcknowledged_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });
            storage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
           x.FlatId == flatOverviewModel.Id && x.CustomerContactId == personInfo.Id && String.IsNullOrEmpty(x.Name) == false && Math.Abs((x.Appointment.Value - appointment).TotalHours) < 1)
            )).ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (x.State == ProcedureStates.Imported && x.RelatedJobId == null) ||
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsFinished(connectBuildingJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(connectBuildingJobId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
x.JobId == connectBuildingJobId && x.UserAuthId == authId))
).ReturnsAsync(true);

            storage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(It.Is<FinishJobAdministrativeModel>(x =>
jobIds.Contains(x.JobId) == true && x.UserAuthId == authId)
)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(false);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid || x == activationJobId);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => x == injectJobId || x == spliceBranchableJobid || x == activationJobId);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_FullActiveRequest_WithActivationJobFinishedAndAcknowledged_WithExistingProcedure_Pass|ImportServiceTester")]
        public async Task ImportProcedures_FullActiveRequest_WithActivationJobFinishedAndAcknowledged_WithExistingProcedure_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();
            Int32 injectJobId = random.Next();
            Int32 spliceBranchableJobid = random.Next();
            Int32 activationJobId = random.Next();

            List<Int32> jobIds = new List<Int32> { connectBuildingJobId, injectJobId, spliceBranchableJobid, activationJobId };
            List<Int32> ackJobIds = new List<Int32> { connectBuildingJobId, injectJobId, spliceBranchableJobid, activationJobId };

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobFinished, connectBuildingJobId);
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobAcknlowedged, connectBuildingJobId);

            expectedProcedureStates.Add(ProcedureStates.InjectJobCreated, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobFinished, injectJobId);
            expectedProcedureStates.Add(ProcedureStates.InjectJobAcknlowedged, injectJobId);

            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobCreated, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobFinished, spliceBranchableJobid);
            expectedProcedureStates.Add(ProcedureStates.SpliceBranchableJobAcknlowedged, spliceBranchableJobid);

            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobCreated, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobFinished, activationJobId);
            expectedProcedureStates.Add(ProcedureStates.ActivatitionJobAcknlowedged, activationJobId);

            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            importInfoModel.Mappings.Add(ProcedureImportProperties.Appointment, 4);
            importInfoModel.Mappings.Add(ProcedureImportProperties.Aggrement, 5);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ConnectBuildingFinished, 6);
            importInfoModel.Mappings.Add(ProcedureImportProperties.InjectFinished, 7);
            importInfoModel.Mappings.Add(ProcedureImportProperties.SpliceFinished, 8);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActivationFinished, 9);
            importInfoModel.Mappings.Add(ProcedureImportProperties.ActiveCustomer, 10);

            DateTimeOffset appointment = DateTimeOffset.Now.AddDays(-random.Next(3, 10));
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber, appointment.ToString(), "x", "x", "x", "x", "x", "x" });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });

            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(connectBuildingJobId);

            storage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExistsByFlatId(flatOverviewModel.Id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetCustomerConnectionProcedureByFlatId(flatOverviewModel.Id)).ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
    x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && 
    (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State])
    ))).
    ReturnsAsync(procedureId);

            storage.Setup(ctx => ctx.GetInjectJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(injectJobId);
            storage.Setup(ctx => ctx.GetSpliceBranachableJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(spliceBranchableJobid);
            storage.Setup(ctx => ctx.GetActivationJobIdByFlatId(flatOverviewModel.Id)).ReturnsAsync(activationJobId);

            storage.Setup(ctx => ctx.CheckIfJobIsFinished(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => true);
            storage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(It.Is<Int32>(x => ackJobIds.Contains(x)))).ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x => true);

            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);

            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "ImportProcedures_OnlyHouseConnection_WithExitingProcedure_Pass|ImportServiceTester")]
        public async Task ImportProcedures_OnlyHouseConnection_WithExitingProcedure_Pass()
        {
            var logger = Mock.Of<ILogger<ImportService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String authId = Guid.NewGuid().ToString();
            Int32 procedureId = random.Next();
            Int32 connectBuildingJobId = random.Next();

            Dictionary<ProcedureStates, Int32> expectedProcedureStates = new Dictionary<ProcedureStates, int>();
            expectedProcedureStates.Add(ProcedureStates.ConnectBuildingJobCreated, connectBuildingJobId);

            CSVProcedureImportInfoModel importInfoModel = GetMinimalCSVProcedureImportInfoModel();
            String street;
            String streetNumber;
            SimpleBuildingOverviewModel simpleBuilding = GetBuildingOverviewModel(random, out street, out streetNumber);
            SimpleFlatOverviewModel flatOverviewModel = GetFlat(random, simpleBuilding.Id);

            PersonInfo personInfo = GetConactInfo(random);
            List<String[]> lines = new List<String[]>();
            lines.Add(new String[] { personInfo.Surname, personInfo.Lastname, street, streetNumber });
            importInfoModel.Lines = lines;

            PrepareStorage(storage, simpleBuilding, personInfo);
            storage.Setup(ctx => ctx.GetFlatsByBuildingId(simpleBuilding.Id)).ReturnsAsync(new List<SimpleFlatOverviewModel> { flatOverviewModel });

            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
                x.ProcedureId == procedureId && String.IsNullOrEmpty(x.Comment) == false && x.RelatedRequestId == null && (
                (expectedProcedureStates.ContainsKey(x.State) == true && x.RelatedJobId == expectedProcedureStates[x.State]))
                ))).
                ReturnsAsync(procedureId);
            storage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(simpleBuilding.Id)).ReturnsAsync(new Int32?());
            storage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>(x =>
x.BuildingId == simpleBuilding.Id && x.OwnerContactId == personInfo.Id && x.CustomerContactId == personInfo.Id && x.FlatId == flatOverviewModel.Id && x.OnlyHouseConnection == true)
)).ReturnsAsync(connectBuildingJobId);

            storage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExistsByBuildingId(simpleBuilding.Id)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.GetBuildingConnectionProcedureByBuildingId(simpleBuilding.Id)).ReturnsAsync(procedureId);


            storage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);


            IImportService service = new ImportService(storage.Object, logger);
            CSVImportProcedureResultModel result = await service.ImportProcedures(importInfoModel, authId);
        }

        [Fact(DisplayName = "CreateCustomer_Failed_NoModel|ImportServiceTester")]
        public async Task CreateCustomer_Failed_NoModel()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));
            CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateContact(null, userAuthId));
            Assert.Equal(CustomerServicExceptionReasons.NoData, exception.Reason);
        }

        [Fact(DisplayName = "CreateCustomer_Failed_InvalidModel|ImportServiceTester")]
        public async Task CreateCustomer_Failed_InvalidModel()
        {
            var logger = Mock.Of<ILogger<CustomerService>>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            storage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            Random random = new Random();

            List<PersonInfo> modelsToFail = new List<PersonInfo>();

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.City = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.Street = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.StreetNumber = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Address.PostalCode = null;
                modelsToFail.Add(failedContactModel);
            }

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Lastname = null;
                modelsToFail.Add(failedContactModel);
            }

            //{
            //    PersonInfo failedContactModel = GetConactInfo(random);
            //    failedContactModel.EmailAddress = null;
            //    modelsToFail.Add(failedContactModel);
            //}

            {
                PersonInfo failedContactModel = GetConactInfo(random);
                failedContactModel.Phone = null;
                modelsToFail.Add(failedContactModel);
            }

            String userAuthId = base.PrepareStorage(random, storage, MessageActions.CreateViaImport, MessageRelatedObjectTypes.ContactInfo, random.Next());

            ICustomerService service = new CustomerService(storage.Object, logger, base.GetProtocolService(storage));

            foreach (PersonInfo item in modelsToFail)
            {
                CustomerServicException exception = await Assert.ThrowsAsync<CustomerServicException>(() => service.CreateContact(item, userAuthId));
                Assert.Equal(CustomerServicExceptionReasons.InvalidData, exception.Reason);
            }
        }

    }
}
