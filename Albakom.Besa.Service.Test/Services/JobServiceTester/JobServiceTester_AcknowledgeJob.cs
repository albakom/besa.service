﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_AcknowledgeJob
    {
        private class AcknowledgeJobData
        {
            public Int32 JobId { get; set; }
            public String AuthId { get; set; }
            public AcknowledgeJobModel Model { get; set; }

            public AcknowledgeJobData(Random rand)
            {
                JobId = rand.Next();
                AuthId = Guid.NewGuid().ToString();

                Int32 fileAmount = rand.Next(1, 10);

                Model = new AcknowledgeJobModel
                {
                    JobId = JobId,
                    UserAuthId = AuthId,
                };
            }
        }

        private static void PrepareMockStorage(AcknowledgeJobData inputData, bool expectedCreateResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.AcknowledgeJob(inputData.Model)).ReturnsAsync(expectedCreateResult);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.BranchOff);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(new Int32?());
        }

        private static void CheckAcknowledgeJobOutput(AcknowledgeJobData inputData, bool expectedCreateResult, bool result)
        {
            Assert.Equal(expectedCreateResult, result);
            TimeSpan diff = DateTimeOffset.Now - inputData.Model.Timestamp;
            Assert.True(diff < TimeSpan.FromMinutes(3));
        }

        [Fact]
        public async Task AcknowledgeJob_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.BranchOffJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(1, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_OnlyHouseConnection_WithoutProcedure_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_OnlyHouseConnection_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(1, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_OnlyHouseConnection_WithProcedure_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_OnlyHouseConnection_WithProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ConnectBuildingJobAcknlowedged
))).ReturnsAsync(timelineId);
            mockStorage.Setup(ctx => ctx.CheckIfContactAfterFinishedJobShouldCreated(procedureId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
          x.Action == MessageActions.FinishedProcedures &&
          x.Level == ProtocolLevels.Sucesss &&
          x.RelatedObjectId == procedureId &&
          x.RelatedType == MessageRelatedObjectTypes.BuildingConnenctionProcedure &&
          (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(2, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_OnlyHouseConnection_WithProcedure_WithInformSales_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_OnlyHouseConnection_WithProcedure_WithInformSales_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactAfterFinishedJobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ConnectBuildingJobAcknlowedged
))).ReturnsAsync(timelineId);
            mockStorage.Setup(ctx => ctx.CheckIfContactAfterFinishedJobShouldCreated(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByConnectionJobId(inputData.JobId)).ReturnsAsync(buildingId);
            mockStorage.Setup(ctx => ctx.CreateContactAfterFinishedJob(It.Is<CreateContactAfterFinishedJobModel>(x =>
x.BuildingId == buildingId && x.FlatId.HasValue == false && x.RelatedProcedureId == procedureId)
)).ReturnsAsync(contactAfterFinishedJobId);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Level == ProtocolLevels.Sucesss &&
             (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60 &&
          (x.RelatedObjectId == procedureId && x.Action == MessageActions.FinishedProcedures && x.RelatedType == MessageRelatedObjectTypes.BuildingConnenctionProcedure) ||
           (x.RelatedObjectId == contactAfterFinishedJobId && x.Action == MessageActions.Create && x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob)
       ))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(3, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_HouseConnectionWithInjectJob_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_HouseConnectionWithInjectJob_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 buildingId = rand.Next();
            Int32 injectJobId = rand.Next();
            Int32 customerId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByConnectionJobId(inputData.JobId)).ReturnsAsync(buildingId);
            mockStorage.Setup(ctx => ctx.GetCustomerContactIdByConnenctionJobId(inputData.JobId)).ReturnsAsync(customerId);

            mockStorage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>((input) =>
            (input.BuildingId == buildingId && input.CustomerContactId == customerId))))
            .ReturnsAsync(injectJobId);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
            (x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ConnectBuildingJobAcknlowedged) ||
            (x.RelatedJobId == injectJobId && x.ProcedureId == procedureId && x.State == ProcedureStates.InjectJobCreated)
))).ReturnsAsync(timelineId);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Level == ProtocolLevels.Sucesss &&
             (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60 &&
           x.RelatedObjectId == injectJobId && x.Action == MessageActions.Create && x.RelatedType == MessageRelatedObjectTypes.InjectJob
       ))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);
            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(2, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_HouseConnectionWithInjectJob_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_HouseConnectionWithInjectJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 buildingId = rand.Next();
            Int32 injectJobId = rand.Next();
            Int32 customerId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByConnectionJobId(inputData.JobId)).ReturnsAsync(buildingId);
            mockStorage.Setup(ctx => ctx.GetCustomerContactIdByConnenctionJobId(inputData.JobId)).ReturnsAsync(customerId);

            mockStorage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>((input) =>
            (input.BuildingId == buildingId && input.CustomerContactId == customerId))))
            .ReturnsAsync(injectJobId);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Level == ProtocolLevels.Sucesss &&
             (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60 &&
           x.RelatedObjectId == injectJobId && x.Action == MessageActions.Create && x.RelatedType == MessageRelatedObjectTypes.InjectJob
       ))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);
            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(2, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_InjectJob_WithProcedure_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_InjectJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 injectJobId = rand.Next();
            Int32 customerId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 spliceJobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.Inject);

            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
    (x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.InjectJobAcknlowedged) ||
    (x.RelatedJobId == spliceJobId && x.ProcedureId == procedureId && x.State == ProcedureStates.SpliceBranchableJobCreated)
))).ReturnsAsync(timelineId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExists(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCreateSpliceJobInfoByCustomerConnectionProcedureId(procedureId)).ReturnsAsync(new SpliceBranchableJobCreateModel
            {
                CustomerContactId = customerId,
                FlatId = flatId,
            });
            mockStorage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.CustomerContactId == customerId && x.FlatId == flatId
))).ReturnsAsync(spliceJobId);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.InjectJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Level == ProtocolLevels.Sucesss &&
             (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60 &&
          (x.RelatedObjectId == procedureId && x.Action == MessageActions.FinishedProcedures && x.RelatedType == MessageRelatedObjectTypes.CustomerConnenctionProcedure) ||
          (x.RelatedObjectId == spliceJobId && x.Action == MessageActions.Create && x.RelatedType == MessageRelatedObjectTypes.SpliceInBranchableJob)
       ))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);
            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(3, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_SpliceBranchableJob_WithProcedure_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_SpliceBranchableJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 spliceBranchableJobId = rand.Next();
            Int32 customerId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 activationJobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.SpliceInBranchable);

            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
    (x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.SpliceBranchableJobAcknlowedged) ||
    (x.RelatedJobId == activationJobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ActivatitionJobCreated)
))).ReturnsAsync(timelineId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExists(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCreateActivationJobInfoByCustomerConnectionProcedureId(procedureId)).ReturnsAsync(new ActivationJobCreateModel
            {
                CustomerContactId = customerId,
                FlatId = flatId,
            });
            mockStorage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.CustomerContactId == customerId && x.FlatId == flatId
))).ReturnsAsync(activationJobId);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobAcknowledged &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.SpliceInBranchableJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
             x.Level == ProtocolLevels.Sucesss &&
             (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60 &&
          (x.RelatedObjectId == procedureId && x.Action == MessageActions.FinishedProcedures && x.RelatedType == MessageRelatedObjectTypes.CustomerConnenctionProcedure) ||
          (x.RelatedObjectId == activationJobId && x.Action == MessageActions.Create && x.RelatedType == MessageRelatedObjectTypes.ActivationJob)
       ))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.AcknowledgeJob(inputData.Model);
            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(3, createCounter);
        }

        [Fact(DisplayName = "AcknowledgeJob_WithProcedure_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            Dictionary<JobTypes, ProcedureStates> states = new Dictionary<JobTypes, ProcedureStates>();
            states.Add(JobTypes.HouseConnenction, ProcedureStates.ConnectBuildingJobAcknlowedged);
            states.Add(JobTypes.Inject, ProcedureStates.InjectJobAcknlowedged);
            states.Add(JobTypes.SpliceInBranchable, ProcedureStates.SpliceBranchableJobAcknlowedged);
            states.Add(JobTypes.ActivationJob, ProcedureStates.ActivatitionJobAcknlowedged);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.CheckIfContactAfterFinishedJobShouldCreated(procedureId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExists(procedureId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);

            foreach (KeyValuePair<JobTypes, ProcedureStates> item in states)
            {
                Int32 timelineCounter = 0;
                mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(item.Key);
                mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == item.Value
))).ReturnsAsync(timelineId).Callback(() => timelineCounter++);

                IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
                Boolean result = await service.AcknowledgeJob(inputData.Model);

                CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
                Assert.Equal(1, timelineCounter);
            }
        }

        [Fact(DisplayName = "AcknowledgeJob_WithProtocol_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_WithProtocol_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            Dictionary<JobTypes, MessageRelatedObjectTypes> states = new Dictionary<JobTypes, MessageRelatedObjectTypes>();
            states.Add(JobTypes.ActivationJob, MessageRelatedObjectTypes.ActivationJob);
            states.Add(JobTypes.BranchOff, MessageRelatedObjectTypes.BranchOffJob);
            states.Add(JobTypes.HouseConnenction, MessageRelatedObjectTypes.ConnectBuildingJob);
            states.Add(JobTypes.Inject, MessageRelatedObjectTypes.InjectJob);
            states.Add(JobTypes.Inventory, MessageRelatedObjectTypes.InventoryJob);
            states.Add(JobTypes.PrepareBranchableForSplice, MessageRelatedObjectTypes.PrepareBranchableJob);
            states.Add(JobTypes.SpliceInBranchable, MessageRelatedObjectTypes.SpliceInBranchableJob);
            states.Add(JobTypes.SpliceInPoP, MessageRelatedObjectTypes.SpliceMainCableInPoPJob);
            states.Add(JobTypes.ContactAfterFinished, MessageRelatedObjectTypes.ContactAfterFinishedJob);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(new Int32?());

            foreach (KeyValuePair<JobTypes, MessageRelatedObjectTypes> item in states)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(item.Key);

                Int32 createCounter = 0;
                var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.JobAcknowledged &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == inputData.JobId &&
               x.RelatedType == item.Value &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

                IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
                Boolean result = await service.AcknowledgeJob(inputData.Model);

                CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
                Assert.Equal(1, createCounter);
            }
        }

        [Fact(DisplayName = "AcknowledgeJob_WithProcedure_ButWithoutValidJobType_Pass|JobServiceTester")]
        public async Task AcknowledgeJob_WithProcedure_ButWithoutValidJobType_Pass()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            List<JobTypes> typesWithNoProcedures = new List<JobTypes> { JobTypes.BranchOff, JobTypes.PrepareBranchableForSplice, JobTypes.SpliceInPoP };

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(inputData.JobId)).ReturnsAsync(true);

            foreach (JobTypes item in typesWithNoProcedures)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(item);

                IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
                Boolean result = await service.AcknowledgeJob(inputData.Model);

                CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            }
        }

        [Fact]
        public async Task AcknowledgeJob_Fail_JobNotFound()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidAcknowlegdeModel, exception.Reason);
        }

        [Fact]
        public async Task AcknowledgeJob_Fail_JobNotFinished()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task AcknowledgeJob_Fail_JobAllreadyAcknowldged()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(inputData.JobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task AcknowledgeJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            AcknowledgeJobData inputData = new AcknowledgeJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            String authId = inputData.AuthId;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);

            inputData.Model.UserAuthId = String.Empty;

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidAcknowlegdeModel, exception.Reason);

            inputData.Model.UserAuthId = authId;
            exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidAcknowlegdeModel, exception.Reason);
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Fail_InvalidModel()
        {
            Random rand = new Random();
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            Dictionary<AcknowledgeJobModel, JobServicExceptionReasons> modelsToFail = new Dictionary<AcknowledgeJobModel, JobServicExceptionReasons>();
            {
                AcknowledgeJobModel inputData = new AcknowledgeJobModel
                {
                    UserAuthId = String.Empty,
                };

                modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidAcknowlegdeModel);
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(null));
            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);

            foreach (KeyValuePair<AcknowledgeJobModel, JobServicExceptionReasons> item in modelsToFail)
            {
                JobServicException itemException = await Assert.ThrowsAsync<JobServicException>(() => service.AcknowledgeJob(item.Key));
                Assert.Equal(item.Value, itemException.Reason);
            }
        }
    }
}
