﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_BindJobs
    {
        private static void PrepareStorage(Mock<IBesaDataStorage> storage, int companyId, String userAuthId, HashSet<int> jobIds, BindJobModel model, Int32 collectionJobId)
        {
            storage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsExists(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsAreUnbound(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.BindJobs(model.JobIds, model.CompanyId, model.EndDate, model.Name)).ReturnsAsync(collectionJobId);
            storage.Setup(ctx => ctx.GetProcecureIdByJobId(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(new Int32?());
        }

        private static void CreateValidModel(Random random, out int companyId, out HashSet<int> jobIds, out BindJobModel model, out String userAuthId)
        {
            userAuthId = Guid.NewGuid().ToString();
            companyId = random.Next();
            Int32 jobAmount = random.Next(10, 100);
            jobIds = new HashSet<int>();
            for (int i = 0; i < jobAmount; i++)
            {
                jobIds.Add(random.Next());
            }

            model = new BindJobModel
            {
                EndDate = DateTimeOffset.Now.AddDays(7),
                CompanyId = companyId,
                JobIds = jobIds,
                Name = $"Testaufgabe {random.Next()}"
            };
        }

        [Fact(DisplayName = "BindJobs_WithoutProccdure_Pass|JobServiceTester")]
        public async Task BindJobs_WithoutProccdure_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random rand = new Random();
            int companyId;
            HashSet<int> jobIds;
            BindJobModel model;
            String userAuthId;
            CreateValidModel(rand, out companyId, out jobIds, out model, out userAuthId);
            Int32 collectionJobid = rand.Next();
            PrepareStorage(storage, companyId, userAuthId, jobIds, model, collectionJobid);

            Int32 protocolConuter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == collectionJobid &&
           x.RelatedType == MessageRelatedObjectTypes.CollectionJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => protocolConuter++);

            IJobService service = new JobService(storage.Object, fileManager, mockProtocolService.Object, logger);
            await service.BindJobs(model, userAuthId);
            Assert.Equal(1, protocolConuter);
        }

        [Fact(DisplayName = "BindJobs_WithProcedure_Pass|JobServiceTester")]
        public async Task BindJobs_WithProcedure_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random rand = new Random();

            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            Dictionary<JobTypes, ProcedureStates> states = new Dictionary<JobTypes, ProcedureStates>();
            states.Add(JobTypes.HouseConnenction, ProcedureStates.ConnectBuildingJobBound);
            states.Add(JobTypes.Inject, ProcedureStates.InjectJobBound);
            states.Add(JobTypes.PrepareBranchableForSplice, ProcedureStates.PrepareBranchableForSpliceJobBound);

            int companyId;
            HashSet<int> jobIds;
            BindJobModel model;
            String userAuthId;
            CreateValidModel(rand, out companyId, out jobIds, out model, out userAuthId);
            Int32 collectionJobid = rand.Next();
            PrepareStorage(mockStorage, companyId, userAuthId, jobIds, model, collectionJobid);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(procedureId);

            foreach (KeyValuePair<JobTypes, ProcedureStates> item in states)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(It.Is<Int32>(x => jobIds.Contains(x)))).ReturnsAsync(item.Key);
                mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
jobIds.Contains(x.RelatedJobId.Value) == true && x.ProcedureId == procedureId && x.State == item.Value
))).ReturnsAsync(timelineId);

                Int32 createCounter = 0;
                var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.Create &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == collectionJobid &&
               x.RelatedType == MessageRelatedObjectTypes.CollectionJob &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

                IJobService service = new JobService(mockStorage.Object, fileManager, mockProtocolService.Object, logger);
                await service.BindJobs(model, userAuthId);
                Assert.Equal(1, createCounter);

            }
        }

        [Fact]
        public async Task BindJobs_Fail_CompanyNotFound()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 companyId = random.Next();

            Int32 jobAmount = random.Next(10, 100);
            HashSet<Int32> jobIds = new HashSet<int>();

            for (int i = 0; i < jobAmount; i++)
            {
                jobIds.Add(random.Next());
            }

            BindJobModel model = new BindJobModel
            {
                EndDate = DateTimeOffset.Now.AddDays(7),
                CompanyId = companyId,
                JobIds = jobIds,
                Name = $"Testaufgabe {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfJobsExists(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsAreUnbound(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.BindJobs(model.JobIds, model.CompanyId, model.EndDate, model.Name)).ReturnsAsync(model.JobIds.Count());

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
            Assert.Equal(JobServicExceptionReasons.InvalidBindJobModel, exception.Reason);
        }

        [Fact]
        public async Task BindJobs_Fail_JobsNotFound()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 companyId = random.Next();

            Int32 jobAmount = random.Next(10, 100);
            HashSet<Int32> jobIds = new HashSet<int>();

            for (int i = 0; i < jobAmount; i++)
            {
                jobIds.Add(random.Next());
            }

            BindJobModel model = new BindJobModel
            {
                EndDate = DateTimeOffset.Now.AddDays(7),
                CompanyId = companyId,
                JobIds = jobIds,
                Name = $"Testaufgabe {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsExists(jobIds)).ReturnsAsync(false);
            storage.Setup(ctx => ctx.CheckIfJobsAreUnbound(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.BindJobs(model.JobIds, model.CompanyId, model.EndDate, model.Name)).ReturnsAsync(model.JobIds.Count());

            {
                IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
                Assert.Equal(JobServicExceptionReasons.InvalidBindJobModel, exception.Reason);
            }

            storage.Setup(ctx => ctx.CheckIfJobsExists(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsAreUnbound(jobIds)).ReturnsAsync(false);

            {
                IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
                Assert.Equal(JobServicExceptionReasons.InvalidBindJobModel, exception.Reason);
            }
        }

        [Fact]
        public async Task BindJobs_Fail_InvalidModel()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict); ;

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 companyId = random.Next();

            Int32 jobAmount = random.Next(10, 100);
            HashSet<Int32> jobIds = new HashSet<int>();

            for (int i = 0; i < jobAmount; i++)
            {
                jobIds.Add(random.Next());
            }

            BindJobModel model = new BindJobModel
            {
                EndDate = DateTimeOffset.Now.AddDays(7),
                CompanyId = companyId,
                JobIds = jobIds,
                Name = $"Testaufgabe {random.Next()}",
            };

            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsExists(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfJobsAreUnbound(jobIds)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.BindJobs(model.JobIds, model.CompanyId, model.EndDate, model.Name)).ReturnsAsync(model.JobIds.Count());


            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException modelNullException = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(null, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NoModel, modelNullException.Reason);

            IEnumerable<Int32>[] jobIdsToFail = new IEnumerable<Int32>[] { null, new List<Int32>() };
            foreach (IEnumerable<Int32> item in jobIdsToFail)
            {
                model.JobIds = item;
                JobServicException modelJobsNullException = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
                Assert.Equal(JobServicExceptionReasons.InvalidBindJobModel, modelJobsNullException.Reason);
            }

            model.JobIds = jobIds;

            String[] testNames = new string[] { String.Empty, "", null };
            foreach (String item in testNames)
            {
                model.Name = item;
                JobServicException modelJobsNullException = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
                Assert.Equal(JobServicExceptionReasons.InvalidBindJobModel, modelJobsNullException.Reason);
            }
        }

        [Fact(DisplayName = "BindJobs_Fail_UserNotFound|JobServiceTester")]
        public async Task BindJobs_Fail_UserNotFound()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random rand = new Random();
            int companyId;
            HashSet<int> jobIds;
            BindJobModel model;
            String userAuthId;
            CreateValidModel(rand, out companyId, out jobIds, out model, out userAuthId);
            Int32 collectionJobid = rand.Next();
            PrepareStorage(mockStorage, companyId, userAuthId, jobIds, model, collectionJobid);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            JobServicException modelJobsNullException = await Assert.ThrowsAsync<JobServicException>(() => service.BindJobs(model, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NotFound, modelJobsNullException.Reason);
        }
    }
}
