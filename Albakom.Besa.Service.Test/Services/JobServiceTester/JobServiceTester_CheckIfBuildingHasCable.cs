﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class JobServiceTester_CheckIfBuildingHasCable
    {

        [Fact(DisplayName = "CheckIfBuildingHasCable|CompanyServiceTester")]
        public async Task CheckIfBuildingHasCable()
        {
            Random rand = new Random();
            Int32 buildingId = rand.Next();
            Boolean returnResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCableIsInjected(buildingId)).ReturnsAsync(returnResult);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            Boolean result = await service.CheckIfBuildingHasCable(buildingId);

            Assert.Equal(returnResult, result);
        }

        [Fact(DisplayName = "CheckIfBuildingHasCable_Fail_BuildingNotFound|CompanyServiceTester")]
        public async Task CheckIfBuildingHasCable_Fail_BuildingNotFound()
        {
            Random rand = new Random();
            Int32 buildingId = rand.Next();
            Boolean returnResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CheckIfBuildingHasCable(buildingId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
