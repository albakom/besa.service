﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CopyFinishFromBranchOffToConnectBuildingJob
    {
        private class InputData
        {
            public String UserAuthId { get; set; }
            public Int32 BranchOffJobId { get; set; }
            public Int32 BuildingId { get; set; }
            public Int32 ConnectBuildingJobId { get; set; }
            public Int32 ProcedureId { get; set; }
            public Boolean JobIsAcknowledged { get; set; }
            public FinishBranchOffDetailModel FinishBranchOffDetails { get; set; }
        }

        private static Boolean CheckCollection<T>(IEnumerable<T> expected, IEnumerable<T> actual)
        {
            try
            {
                Assert.Equal(expected, actual);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void PrepareMockStorage(InputData data, Boolean withProcedure, Random random, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(data.UserAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(data.BranchOffJobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(data.BranchOffJobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByBranchOffJobId(data.BranchOffJobId)).ReturnsAsync(data.BuildingId);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(data.BuildingId)).ReturnsAsync(data.ConnectBuildingJobId);
            mockStorage.Setup(ctx => ctx.GetBranchOffFinishedJobDetails(data.BranchOffJobId)).ReturnsAsync(data.FinishBranchOffDetails);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(data.ConnectBuildingJobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.FinishBuildingConnenctionJob(It.Is< FinishBuildingConnenctionJobModel>(x => 
               x.Comment == data.FinishBranchOffDetails.Comment &&
               x.DuctChanged.Value == data.FinishBranchOffDetails.DuctChanged.Value &&
               x.DuctChanged.Description == data.FinishBranchOffDetails.DuctChanged.Description &&
               x.ProblemHappend.Value == data.FinishBranchOffDetails.ProblemHappend.Value &&
               x.ProblemHappend.Description == data.FinishBranchOffDetails.ProblemHappend.Description &&
               x.JobId == data.ConnectBuildingJobId &&
              x.Timestamp == data.FinishBranchOffDetails.FinishedAt &&
               x.UserAuthId == data.UserAuthId &&
               CheckCollection(data.FinishBranchOffDetails.Files.Select(y => y.Id).ToList(),x.FileIds) == true
            ) )).ReturnsAsync(true);

            if (withProcedure == false)
            {
                mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(data.ConnectBuildingJobId)).ReturnsAsync(new Int32?());
            }
            else
            {
                mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(data.ConnectBuildingJobId)).ReturnsAsync(data.ProcedureId);

                mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
    x.ProcedureId == data.ProcedureId && (
    (x.State == ProcedureStates.BranchOffFinsshedTransfromToConnectBuilding && x.RelatedJobId == data.BranchOffJobId) ||
    (x.State == ProcedureStates.ConnectBuildingJobFinished && x.RelatedJobId == data.ConnectBuildingJobId) ||
    (data.JobIsAcknowledged == true && x.State == ProcedureStates.ConnectBuildingJobAcknlowedged && x.RelatedJobId == data.ConnectBuildingJobId)
    )
))).ReturnsAsync(random.Next());

                mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(data.ProcedureId)).ReturnsAsync(data.JobIsAcknowledged);
                if(data.JobIsAcknowledged == true)
                {
                    mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(data.ProcedureId)).ReturnsAsync(true);
                }
            }

            mockStorage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(data.BranchOffJobId)).ReturnsAsync(data.JobIsAcknowledged);
            if (data.JobIsAcknowledged == true)
            {
                mockStorage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(x =>
            x.JobId == data.ConnectBuildingJobId &&
           x.UserAuthId == data.UserAuthId &&
           Math.Abs( (DateTimeOffset.Now - x.Timestamp).TotalMinutes ) < 2
          ))).ReturnsAsync(true);
                mockStorage.Setup(ctx => ctx.CheckIfOnlyHouseConnenctionIsNeeded(data.ConnectBuildingJobId)).ReturnsAsync(true);

            }
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand, int jobId, string userAuthId, Boolean acknowledged, Int32? procedureId,  Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobFinished &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            if(acknowledged == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.JobAcknowledged &&
                x.Level == ProtocolLevels.Sucesss &&
                x.RelatedObjectId == jobId &&
                x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
                (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }

            if (procedureId.HasValue == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.FinishedProcedures &&
                x.Level == ProtocolLevels.Sucesss &&
                x.RelatedObjectId == procedureId &&
                x.RelatedType == MessageRelatedObjectTypes.BuildingConnenctionProcedure &&
                (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }
        }

        private static InputData GenerateValidInputData(Random random, Boolean withProcedure, Boolean ack)
        {
            InputData data = new InputData
            {
                BranchOffJobId = random.Next(),
                BuildingId = random.Next(),
                ConnectBuildingJobId = random.Next(),
                JobIsAcknowledged = ack,
                ProcedureId = withProcedure == true ? random.Next() : -1,
                UserAuthId = Guid.NewGuid().ToString(),
                FinishBranchOffDetails = new FinishBranchOffDetailModel
                {
                    FinishedAt = DateTimeOffset.Now.AddDays(-random.Next(3,10)),
                    Comment = $"Mein toller Kommentar {random.Next()}",
                    DuctChanged = new ExplainedBooleanModel
                    {
                        Description = $"Gute BEschreibung {random.Next()}",
                        Value = random.NextDouble() > 0.5,
                    },
                    ProblemHappend = new ExplainedBooleanModel
                    {
                        Description = $"Problem Beschreibung {random.Next()}",
                        Value = random.NextDouble() > 0.5,
                    },
                }
            };

            data.FinishBranchOffDetails.JobId = data.BranchOffJobId;

            Int32 fileAmount = random.Next(3, 10);

            List<FileOverviewModel> fileOverviews = new List<FileOverviewModel>();
            for (int i = 0; i < fileAmount; i++)
            {
                FileOverviewModel file = new FileOverviewModel
                {
                    Id = i + 1,
                };

                fileOverviews.Add(file);
            }

            data.FinishBranchOffDetails.Files = fileOverviews;

            return data;
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Pass|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, false, false);
            PrepareMockStorage(data, false, rand, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict); ;
            PrepareProtocolService(rand, data.ConnectBuildingJobId, data.UserAuthId, false, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 result = await service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId,data.UserAuthId);

            Assert.Equal(data.ConnectBuildingJobId, result);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_WithProcedure_Pass|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_WithProcedure_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, false);
            PrepareMockStorage(data, true, rand, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict); ;
            PrepareProtocolService(rand, data.ConnectBuildingJobId, data.UserAuthId, false, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 result = await service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId);

            Assert.Equal(data.ConnectBuildingJobId, result);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_WithProcedure_AndAcknowledge_Pass|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_WithProcedure_AndAcknowledge_Pass()
        {
            Random rand = new Random(123345);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict); ;
            PrepareProtocolService(rand, data.ConnectBuildingJobId, data.UserAuthId, true, data.ProcedureId, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 result = await service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId);

            Assert.Equal(data.ConnectBuildingJobId, result);
            Assert.Equal(3, _protocolCounter);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Fail_UserNotFound|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Fail_UserNotFound()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(data.UserAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Fail_JobNotFound|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Fail_JobNotFound()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(data.BranchOffJobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Fail_JobNotFinished|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Fail_JobNotFinished()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(data.BranchOffJobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Fail_ConnectBuildingNotFound|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Fail_ConnectBuildingNotFound()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(data.BuildingId)).ReturnsAsync(new Int32?());

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CopyFinishFromBranchOffToConnectBuildingJob_Fail_ConnectBuildingJobAlreadyFinished|CopyFinishFromBranchOffToConnectBuildingJob")]
        public async Task CopyFinishFromBranchOffToConnectBuildingJob_Fail_ConnectBuildingJobAlreadyFinished()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            InputData data = GenerateValidInputData(rand, true, true);
            PrepareMockStorage(data, true, rand, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(data.ConnectBuildingJobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CopyFinishFromBranchOffToConnectBuildingJob(data.BranchOffJobId, data.UserAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

    }
}
