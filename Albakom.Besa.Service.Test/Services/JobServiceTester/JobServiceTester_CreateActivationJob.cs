﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateActivationJob
    {
        private static void PrepareMockStorage(ActivationJobCreateModel createModel, Int32 jobId, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfFlatExists(createModel.FlatId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfContactExists(createModel.CustomerContactId)).ReturnsAsync(true);
            if (createModel.DeviceId.HasValue == true)
            {
                mockStorage.Setup(ctx => ctx.CheckIfArticleExists(createModel.DeviceId.Value)).ReturnsAsync(true);
            }
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExitsByFlatId(createModel.FlatId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateActivationJob(createModel)).ReturnsAsync(jobId);

            mockStorage.Setup(ctx => ctx.GetInjectJobIdByFlatId(createModel.FlatId)).ReturnsAsync(new Int32?());
        }

        [Fact(DisplayName = "CreateActivationJob_WithoutProcedure_Pass|JobServiceTester")]
        public async Task CreateActivationJob_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };

            Int32 protolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.ActivationJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => protolCounter++);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateActivationJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, protolCounter);
        }

        [Fact(DisplayName = "CreateActivationJob_WithProcedure_Pass|JobServiceTester")]
        public async Task CreateActivationJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 injectJobId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetInjectJobIdByFlatId(flatId)).ReturnsAsync(injectJobId);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(injectJobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == jobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ActivatitionJobCreated
))).ReturnsAsync(timelineId);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            Int32 actualJobId = await service.CreateActivationJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_NoModel|JobServiceTester")]
        public async Task CreateActivationJobFail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            //PrepareMockStorage(buildingId, createModel, buildingId, mockStorage);
            // mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(null, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_ContactNotFound|JobServiceTester")]
        public async Task CreateActivationJob_Fail_ContactNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfContactExists(contactId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_FlatNotFound|JobServiceTester")]
        public async Task CreateActivationJob_Fail_FlatNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfFlatExists(flatId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_DeviceNotFound|JobServiceTester")]
        public async Task CreateActivationJob_Fail_DeviceNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfArticleExists(deviceId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_JobAlreadyExsits|JobServiceTester")]
        public async Task CreateActivationJob_Fail_JobAlreadyExsits()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExitsByFlatId(flatId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateActivationJob_Fail_UserNotFound|JobServiceTester")]
        public async Task CreateActivationJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 deviceId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            ActivationJobCreateModel createModel = new ActivationJobCreateModel
            {
                FlatId = flatId,
                CustomerContactId = contactId,
                DeviceId = deviceId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateActivationJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
