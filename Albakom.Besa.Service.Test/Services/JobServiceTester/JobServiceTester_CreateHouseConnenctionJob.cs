﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateHouseConnenctionJob
    {
        private Boolean ContactIdTester(IEnumerable<Int32> idsToCheck, HashSet<Int32> expectedIds)
        {
            HashSet<Int32> usedIds = new HashSet<int>(idsToCheck);

            if (usedIds.Count != expectedIds.Count) { return false; }

            foreach (Int32 item in usedIds)
            {
                if (expectedIds.Contains(item) == false) { return false; }
            }

            return true;
        }

        private void PrepareMockStorage(Int32 buildingId, HouseConnenctionJobCreateModel createModel, Int32 jobId, HashSet<Int32> peronIds, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, peronIds)))).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CreateHouseConnenctionJob(createModel)).ReturnsAsync(jobId);
            mockStorage.Setup(ctx => ctx.GetRequestIdByFilterValues(It.Is<RequestFilterModel>(x =>
              x.BuildingId == buildingId && x.OnlyConnection == createModel.OnlyHouseConnection && x.OwnerContactId == createModel.OwnerContactId
            ))).ReturnsAsync(new Int32?());
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand, int jobId, string userAuthId, Int32? procedureId, MessageRelatedObjectTypes? procedureType, Int32? requestId, Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            if (procedureId.HasValue == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.Create &&
                x.Level == ProtocolLevels.Sucesss &&
                x.RelatedObjectId == procedureId &&
                x.RelatedType == procedureType.Value &&
                (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }

            if (requestId.HasValue == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.Delete &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == requestId.Value &&
               x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_OnlyHouseConnection_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_OnlyHouseConnection_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, null, null, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithoutProcedure_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetRequestIdByFilterValues(It.Is<RequestFilterModel>(x =>
        x.BuildingId == buildingId && x.OnlyConnection == createModel.OnlyHouseConnection && x.OwnerContactId == createModel.OwnerContactId
      ))).ReturnsAsync(requestId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CloseRequest(requestId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(new Int32?());

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, null, null, requestId, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithoutProcedure_ClosedFailed_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithoutProcedure_ClosedFailed_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetRequestIdByFilterValues(It.Is<RequestFilterModel>(x =>
        x.BuildingId == buildingId && x.OnlyConnection == createModel.OnlyHouseConnection && x.OwnerContactId == createModel.OwnerContactId
      ))).ReturnsAsync(requestId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CloseRequest(requestId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(new Int32?());

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, null, null, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithProcedure_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_OnlyHouseConnection_WithRequest_WithProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 requestId = rand.Next();
            Int32 procedureId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetRequestIdByFilterValues(It.Is<RequestFilterModel>(x =>
        x.BuildingId == buildingId && x.OnlyConnection == createModel.OnlyHouseConnection && x.OwnerContactId == createModel.OwnerContactId
      ))).ReturnsAsync(requestId);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CloseRequest(requestId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedRequestId == requestId && x.ProcedureId == procedureId && x.State == ProcedureStates.RequestAcknkowledged
))).ReturnsAsync(requestId);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == jobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ConnectBuildingJobCreated
))).ReturnsAsync(requestId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, null, null, requestId, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_OnlyHouseConnection_CreateRequest_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_OnlyHouseConnection_CreateRequest_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 requestId = rand.Next();
            Int32 procedureId = rand.Next();
            String buildingStreetname = $"Teststr. {rand.Next()}";
            PersonInfo personInfo = new PersonInfo { Surname = $"Vorname {rand.Next()}", Lastname = $"Nachname {rand.Next()}", CompanyName = $"Firma {rand.Next()}" };
            String userAuthId = Guid.NewGuid().ToString();

            Int32 architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = true;
            createModel.CreateProcedureIfNotAlreadyExists = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetBuildingStreetNameById(buildingId)).ReturnsAsync(buildingStreetname);
            mockStorage.Setup(ctx => ctx.GetContactById(ownerId)).ReturnsAsync(personInfo);
            mockStorage.Setup(ctx => ctx.CreateBuildingConnectionProcedure(It.Is<BuildingConnectionProcedureCreateModel>(x =>
                x.BuildingId == buildingId &&
                x.ExpectedEnd == ProcedureStates.ConnectBuildingJobAcknlowedged &&
                x.Start == ProcedureStates.ConnectBuildingJobCreated &&
                x.OwnerContactId == ownerId &&
                x.FirstElement != null &&
                x.FirstElement.RelatedJobId == jobId &&
                x.FirstElement.State == ProcedureStates.ConnectBuildingJobCreated))).ReturnsAsync(procedureId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, procedureId, MessageRelatedObjectTypes.BuildingConnenctionProcedure, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJob_NotOnlyHouseConnection_CreateRequest_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJob_NotOnlyHouseConnection_CreateRequest_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 requestId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 flatId = rand.Next();
            String buildingStreetname = $"Teststr. {rand.Next()}";
            PersonInfo personInfo = new PersonInfo { Surname = $"Vorname {rand.Next()}", Lastname = $"Nachname {rand.Next()}", CompanyName = $"Firma {rand.Next()}" };
            String userAuthId = Guid.NewGuid().ToString();

            Int32 architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            createModel.OnlyHouseConnection = false;
            createModel.CreateProcedureIfNotAlreadyExists = true;
            createModel.FlatId = flatId;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetBuildingStreetNameById(buildingId)).ReturnsAsync(buildingStreetname);
            mockStorage.Setup(ctx => ctx.GetContactById(customerId)).ReturnsAsync(personInfo);
            mockStorage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(x =>
                x.FlatId == flatId &&
                x.ExpectedEnd == ProcedureStates.ActivatitionJobAcknlowedged &&
                x.Start == ProcedureStates.ConnectBuildingJobCreated &&
                x.CustomerContactId == customerId &&
                x.FirstElement != null &&
                x.FirstElement.RelatedJobId == jobId &&
                x.FirstElement.State == ProcedureStates.ConnectBuildingJobCreated))).ReturnsAsync(procedureId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, procedureId, MessageRelatedObjectTypes.CustomerConnenctionProcedure, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_NotOnlyHouseConnection_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, null, null, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateHouseConnenctionJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, _protocolCounter);
        }

        private static void GenerateConnectionJobModel(Random rand, int buildingId, out int architektId, out int workmanId, out int ownerId, out int customerId, out HouseConnenctionJobCreateModel createModel, HashSet<Int32> contactIds)
        {
            architektId = rand.Next();
            workmanId = rand.Next();
            ownerId = rand.Next();
            customerId = rand.Next();
            createModel = new HouseConnenctionJobCreateModel
            {
                BuildingId = buildingId,
                ArchitectContactId = rand.NextDouble() > 0.5 ? architektId : new Nullable<Int32>(),
                WorkmanContactId = rand.NextDouble() > 0.5 ? workmanId : new Nullable<Int32>(),
                CivilWorkIsDoneByCustomer = rand.NextDouble() > 0.5,
                ConnectionOfOtherMediaRequired = rand.NextDouble() > 0.5,
                ConnectionAppointment = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                Description = $"Meine Beschreibung... {rand.Next()}",
                OwnerContactId = ownerId,
                CustomerContactId = customerId,
                OnlyHouseConnection = true,
            };

            contactIds.Add(ownerId);
            contactIds.Add(customerId);

            if (createModel.ArchitectContactId.HasValue == true)
            {
                contactIds.Add(architektId);
            }
            if (createModel.WorkmanContactId.HasValue == true)
            {
                contactIds.Add(workmanId);
            }
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, new HouseConnenctionJobCreateModel(), buildingId, new HashSet<int>(), userAuthId, mockStorage);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(null, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_InvalidModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);

            createModel.OnlyHouseConnection = false;
            createModel.CustomerContactId = null;

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(null, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_BuildingNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_BuildingConnectionFinished()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_ContactsNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfContactInfosExists(It.Is<IEnumerable<Int32>>((input) => ContactIdTester(input, contactIds)))).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateHouseConnenctionJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            int architektId, workmanId, ownerId, customerId;
            HouseConnenctionJobCreateModel createModel;
            HashSet<Int32> contactIds = new HashSet<int>();
            GenerateConnectionJobModel(rand, buildingId, out architektId, out workmanId, out ownerId, out customerId, out createModel, contactIds);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, contactIds, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
