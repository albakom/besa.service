﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateHouseConnenctionJobByRequest
    {

        private Boolean RequestInputTester(HouseConnenctionJobCreateModel input, RequestDetailModel request)
        {
            Boolean result =
                input.ArchitectContactId.HasValue == true ? input.ArchitectContactId.Value == request.Architect.Id : request.Architect == null &&
                input.BuildingId == request.Building.Id &&
                input.CivilWorkIsDoneByCustomer == request.CivilWorkIsDoneByCustomer &&
                input.ConnectionAppointment == request.ConnectionAppointment &&
                input.ConnectionOfOtherMediaRequired == request.ConnectionOfOtherMediaRequired &&
                input.CustomerContactId.HasValue == true ? input.CustomerContactId.Value == request.Customer.Id : request.Customer == null &&
                input.Description == request.DescriptionForHouseConnection &&
                input.OnlyHouseConnection == request.OnlyHouseConnection &&
                input.WorkmanContactId.HasValue == true ? input.WorkmanContactId.Value == request.Workman.Id : request.Workman == null;

            return result;
        }

        private static RequestDetailModel GenerateDetailModel(Random rand, int buildingId)
        {
            RequestDetailModel request = new RequestDetailModel
            {
                CivilWorkIsDoneByCustomer = rand.NextDouble() > 0.5,
                Building = new SimpleBuildingOverviewModel { Id = buildingId },
                OnlyHouseConnection = rand.NextDouble() > 0.5,
                ConnectionAppointment = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                ConnectionOfOtherMediaRequired = rand.NextDouble() > 0.5,
                DescriptionForHouseConnection = $"Tolle BEschreb {rand.Next()}",
                Owner = new PersonInfo { Id = rand.Next() },
            };

            if (rand.NextDouble() > 0.5)
            {
                request.Architect = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() };
            }
            if (rand.NextDouble() > 0.5)
            {
                request.Customer = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() };
            }
            if (rand.NextDouble() > 0.5)
            {
                request.Workman = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() };
            }

            return request;
        }

        private Boolean TestProcedureTimelineCreateModelWithRequestId(ProcedureTimelineCreateModel input, Int32 procedureId, ProcedureStates expectedState, Int32 requestId)
        {
            Boolean result =
              input.ProcedureId == procedureId &&
              input.State == expectedState &&
              input.RelatedJobId.HasValue == false &&
              input.RelatedRequestId == requestId;

            return result;
        }

        private Boolean TestProcedureTimelineCreateModelWithJobId(ProcedureTimelineCreateModel input, Int32 procedureId, ProcedureStates expectedState, Int32 jobId)
        {
            Boolean result =
              input.ProcedureId == procedureId &&
              input.State == expectedState &&
              input.RelatedRequestId.HasValue == false &&
              input.RelatedJobId == jobId;

            return result;
        }

        private void PrepareStorage(int requestId, int jobId, int buildingId, RequestDetailModel request, String userAuhtId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuhtId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCustomerRequestDetails(requestId)).ReturnsAsync(request);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CloseRequest(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CreateHouseConnenctionJob(It.Is<HouseConnenctionJobCreateModel>((input) => RequestInputTester(input, request)))).ReturnsAsync(jobId);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(new Int32?());
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand, int requestId, int jobId, string userAuthId, Boolean wihtBound, Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Delete &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == requestId &&
           x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            if (wihtBound == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.JobBound &&
                x.Level == ProtocolLevels.Sucesss &&
                x.RelatedObjectId == jobId &&
                x.RelatedType == MessageRelatedObjectTypes.ConnectBuildingJob &&
                (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_NoBinding_NoProcedureId_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_NoBinding_NoProcedureId_Pass()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, false, userAuthId);

            Assert.Equal(jobId, actual);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_NoBinding_WithProcedureId_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_NoBinding_WithProcedureId_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            Int32 procedureId = rand.Next();

            Int32 timelineCounter = 0;

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         TestProcedureTimelineCreateModelWithRequestId(x, procedureId, ProcedureStates.RequestAcknkowledged, requestId) ||
         TestProcedureTimelineCreateModelWithJobId(x, procedureId, ProcedureStates.ConnectBuildingJobCreated, jobId)
            ))).ReturnsAsync(procedureId).Callback(() => timelineCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, false, userAuthId);

            Assert.Equal(jobId, actual);
            Assert.Equal(2, timelineCounter);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_WithBindingButNotBound_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_WithBindingButNotBound_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchoffJobIsBoundByBuildingId(buildingId)).ReturnsAsync(false);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, true, userAuthId);

            Assert.Equal(2, _protocolCounter);
            Assert.Equal(jobId, actual);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_WithBindingButNotBound_WithProcedure_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_WithBindingButNotBound_WithProcedure_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 procedureId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);
            Int32 timelineCounter = 0;
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchoffJobIsBoundByBuildingId(buildingId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         TestProcedureTimelineCreateModelWithRequestId(x, procedureId, ProcedureStates.RequestAcknkowledged, requestId) ||
         TestProcedureTimelineCreateModelWithJobId(x, procedureId, ProcedureStates.ConnectBuildingJobCreated, jobId)
            ))).ReturnsAsync(procedureId).Callback(() => timelineCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, true, userAuthId);

            Assert.Equal(jobId, actual);
            Assert.Equal(2, timelineCounter);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_WithBindingAndBound_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_WithBindingAndBound_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 branchOffId = rand.Next();
            Int32 collectionId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchoffJobIsBoundByBuildingId(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobIdByBuildingId(buildingId)).ReturnsAsync(branchOffId);
            mockStorage.Setup(ctx => ctx.GetCollectionJobId(branchOffId)).ReturnsAsync(collectionId);
            mockStorage.Setup(ctx => ctx.AddJobToCollectionJob(collectionId, jobId)).ReturnsAsync(true);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, true, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, true, userAuthId);

            Assert.Equal(jobId, actual);
            Assert.Equal(3, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_WithBindingAndBound_WithProcedure_Pass|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_WithBindingAndBound_WithProcedure_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 branchOffId = rand.Next();
            Int32 collectionId = rand.Next();
            Int32 procedureId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand, buildingId);
            Int32 timelineCounter = 0;
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);

            mockStorage.Setup(ctx => ctx.CheckIfBranchoffJobIsBoundByBuildingId(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobIdByBuildingId(buildingId)).ReturnsAsync(branchOffId);
            mockStorage.Setup(ctx => ctx.GetCollectionJobId(branchOffId)).ReturnsAsync(collectionId);
            mockStorage.Setup(ctx => ctx.AddJobToCollectionJob(collectionId, jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         TestProcedureTimelineCreateModelWithRequestId(x, procedureId, ProcedureStates.RequestAcknkowledged, requestId) ||
         TestProcedureTimelineCreateModelWithJobId(x, procedureId, ProcedureStates.ConnectBuildingJobCreated, jobId) ||
         TestProcedureTimelineCreateModelWithJobId(x, procedureId, ProcedureStates.ConnectBuildingJobBound, jobId)
            ))).ReturnsAsync(procedureId).Callback(() => timelineCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, requestId, jobId, userAuthId, true, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Int32 actual = await service.CreateHouseConnenctionJobByRequest(requestId, true, userAuthId);

            Assert.Equal(jobId, actual);
            Assert.Equal(3, timelineCounter);
            Assert.Equal(3, _protocolCounter);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_Fail_RequestNotFound|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_Fail_RequestNotFound()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJobByRequest(requestId, false, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_Fail_RequestIsClosed|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_Fail_RequestIsClosed()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJobByRequest(requestId, false, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_Fail_UserNotFound|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_Fail_UserNotFound()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJobByRequest(requestId, false, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateHouseConnenctionJobByRequest_Fail_BuildingIsConnected|JobServiceTester")]
        public async Task CreateHouseConnenctionJobByRequest_Fail_BuildingIsConnected()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            RequestDetailModel request = GenerateDetailModel(rand, buildingId);

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(requestId, jobId, buildingId, request, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateHouseConnenctionJobByRequest(requestId, true, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
