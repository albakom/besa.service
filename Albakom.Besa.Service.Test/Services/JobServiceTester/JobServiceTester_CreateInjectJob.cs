﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateInjectJob
    {
        private static void PrepareMockStorage(Int32 buildingId, InjectJobCreateModel createModel, Int32 jobId, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfContactExists(createModel.CustomerContactId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CreateInjectJob(createModel)).ReturnsAsync(jobId);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(buildingId)).ReturnsAsync(new Int32?());
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand,  int jobId, string userAuthId, Boolean wihtBound, Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.InjectJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
        }

        [Fact(DisplayName = "CreateInjectJob_WithoutProcedure_Pass|JobServiceTester")]
        public async Task CreateInjectJob_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, userAuthId, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthId, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateInjectJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CreateInjectJob_WithProcedure_Pass|JobServiceTester")]
        public async Task CreateInjectJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 houseConnectionJobId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, jobId, userAuthid, mockStorage);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByBuildingId(buildingId)).ReturnsAsync(houseConnectionJobId);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(houseConnectionJobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == jobId && x.ProcedureId == procedureId && x.State == ProcedureStates.InjectJobCreated
))).ReturnsAsync(timelineId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, jobId, userAuthid, false, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateInjectJob(createModel, userAuthid);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "CreateInjectJob_Fail_NoModel|JobServiceTester")]
        public async Task CreateInjectJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            //PrepareMockStorage(buildingId, createModel, buildingId, mockStorage);
            // mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInjectJob(null, userAuthid));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "CreateInjectJob_Fail_ContactNotFound|JobServiceTester")]
        public async Task CreateInjectJob_Fail_ContactNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, buildingId, userAuthid, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfContactExists(contactId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInjectJob(createModel, userAuthid));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateInjectJob_Fail_BuildingNotFound|JobServiceTester")]
        public async Task CreateInjectJob_Fail_BuildingNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, buildingId, userAuthid, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingExists(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInjectJob(createModel, userAuthid));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateInjectJob_Fail_BuildingNotConnected()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, buildingId, userAuthid, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinished(buildingId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInjectJob(createModel, userAuthid));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreateInjectJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthid = Guid.NewGuid().ToString();

            InjectJobCreateModel createModel = new InjectJobCreateModel
            {
                BuildingId = buildingId,
                CustomerContactId = contactId,
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(buildingId, createModel, buildingId, userAuthid, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthid)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInjectJob(createModel, userAuthid));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
