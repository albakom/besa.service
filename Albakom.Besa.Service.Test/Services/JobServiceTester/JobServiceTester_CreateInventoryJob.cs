﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateInventoryJob
    {
        private static void PrepareMockStorage(InventoryJobCreateModel createModel, Int32 jobId, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfArticlesExists(createModel.Articles.Keys)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfContactExists(createModel.IssuanceToId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CreateInventoryJob(createModel)).ReturnsAsync(jobId);
        }

        private Dictionary<Int32, Double> GetArticles(Random random)
        {
            Int32 amount = random.Next(3, 10);
            Dictionary<Int32, Double> result = new Dictionary<int, double>(amount);

            for (int i = 0; i < amount; i++)
            {
                Int32 articleId = random.Next();
                Double value = random.Next(100, 200) + random.NextDouble();
                if (result.ContainsKey(articleId) == false)
                {
                    result.Add(articleId, value);
                }
            }

            return result;
        }

        [Fact(DisplayName = "CreateInventoryJob_Pass|JobServiceTester")]
        public async Task CreateInventoryJob_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            InventoryJobCreateModel createModel = new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = GetArticles(rand),
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId,userAuthId, mockStorage);

            Int32 protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.InventoryJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => protocolCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateInventoryJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, protocolCounter);
        }

        [Fact(DisplayName = "CreateInventoryJob_Fail_NoModel|JobServiceTester")]
        public async Task CreateInventoryJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInventoryJob(null, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "CreateInventoryJob_Fail_NoInventory|JobServiceTester")]
        public async Task CreateInventoryJob_Fail_NoInventory()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            List<InventoryJobCreateModel> modelToFail = new List<InventoryJobCreateModel>();
            modelToFail.Add(new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = null,
            });
            modelToFail.Add(new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = new Dictionary<int, double>(),
            });

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            foreach (InventoryJobCreateModel createModel in modelToFail)
            {
                IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInventoryJob(createModel, userAuthId));

                Assert.Equal(JobServicExceptionReasons.InvalidData, exception.Reason);
            }
        }

        [Fact(DisplayName = "CreateInventoryJob_Fail_ContactNotFound|JobServiceTester")]
        public async Task CreateInventoryJob_Fail_ContactNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            InventoryJobCreateModel createModel = new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = GetArticles(rand),
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);

            mockStorage.Setup(ctx => ctx.CheckIfContactExists(contactId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInventoryJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateInventoryJob_Fail_ArticlesNotFound|JobServiceTester")]
        public async Task CreateInventoryJob_Fail_ArticlesNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            InventoryJobCreateModel createModel = new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = GetArticles(rand),
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);

            mockStorage.Setup(ctx => ctx.CheckIfArticlesExists(createModel.Articles.Keys)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInventoryJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateInventoryJob_Fail_UserNotNotFound|JobServiceTester")]
        public async Task CreateInventoryJob_Fail_UserNotNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 contactId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            InventoryJobCreateModel createModel = new InventoryJobCreateModel
            {
                PickupAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                IssuanceToId = contactId,
                Articles = GetArticles(rand),
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateInventoryJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
