﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateJobByRequestId
    {
        private static RequestDetailModel GenerateDetailModel(Random rand)
        {
            RequestDetailModel request = new RequestDetailModel
            {
                Id = rand.Next(),
                CivilWorkIsDoneByCustomer = rand.NextDouble() > 0.5,
                Building = new SimpleBuildingOverviewModel { Id = rand.Next() },
                Flat = new SimpleFlatOverviewModel { Id = rand.Next() },
                OnlyHouseConnection = false,
                ConnectionAppointment = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                ConnectionOfOtherMediaRequired = rand.NextDouble() > 0.5,
                DescriptionForHouseConnection = $"Tolle BEschreb {rand.Next()}",
                Owner = new PersonInfo { Id = rand.Next() },
                IsInProgress = false,
                Customer = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() },
            };

            if (rand.NextDouble() > 0.5)
            {
                request.Architect = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() };
            }
            if (rand.NextDouble() > 0.5)
            {
                request.Workman = new PersonInfo { Address = new AddressModel { }, Id = rand.Next() };
            }

            return request;
        }

        private static void PrepareStorage(Random rand, RequestDetailModel request, int createdJobId, int procedureId, SimpleJobOverview currentJob, ProcedureStates expectedJobState, Boolean withJob, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(request.Id)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(request.Id)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetCustomerRequestDetails(request.Id)).ReturnsAsync(request);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.GetMostRecentJobByFlatId(request.Flat.Id, request.Building.Id)).ReturnsAsync(currentJob);
            if (withJob == true)
            {
                mockStorage.Setup(ctx => ctx.CreateInjectJob(It.Is<InjectJobCreateModel>(
                    x => x.CustomerContactId == request.Customer.Id &&
                    x.BuildingId == request.Building.Id))).ReturnsAsync(createdJobId);
            }

            mockStorage.Setup(ctx => ctx.CloseRequest(request.Id)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(request.Id)).ReturnsAsync(procedureId);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(
    x => x.ProcedureId == procedureId &&
    (x.RelatedRequestId == request.Id && x.State == ProcedureStates.RequestAcknkowledged) || (x.RelatedJobId == createdJobId && x.State == expectedJobState)
    ))).ReturnsAsync(rand.Next());
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand, int requestId, int jobId, string userAuthId, MessageRelatedObjectTypes jobType, Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == jobType &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Delete &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == requestId &&
           x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
        }

        [Fact(DisplayName = "CreateJobByRequestId_InjectJob_Pass|JobServiceTester")]
        public async Task CreateJobByRequestId_InjectJob_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand);
            Int32 createdJobId = rand.Next();
            Int32 procedureId = rand.Next();
            SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = JobStates.Acknowledged, JobType = JobTypes.HouseConnenction };
            SimpleJobOverview nextJob = new SimpleJobOverview { Id = createdJobId, State = JobStates.Open, JobType = JobTypes.Inject };
            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.InjectJobCreated, true, userAuthId, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, request.Id, createdJobId, userAuthId, MessageRelatedObjectTypes.InjectJob, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            SimpleJobOverview actual = await service.CreateJobByRequestId(request.Id, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateJobByRequestId_SpliceBranchable_Pass|JobServiceTester")]
        public async Task CreateJobByRequestId_SpliceBranchable_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand);
            Int32 createdJobId = rand.Next();
            Int32 procedureId = rand.Next();
            SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = JobStates.Acknowledged, JobType = JobTypes.Inject };
            SimpleJobOverview nextJob = new SimpleJobOverview { Id = createdJobId, State = JobStates.Open, JobType = JobTypes.SpliceInBranchable };
            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.SpliceBranchableJobCreated, false, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(
            x => x.CustomerContactId == request.Customer.Id &&
            x.FlatId == request.Flat.Id))).ReturnsAsync(createdJobId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, request.Id, createdJobId, userAuthId, MessageRelatedObjectTypes.SpliceInBranchableJob, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            SimpleJobOverview actual = await service.CreateJobByRequestId(request.Id, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateJobByRequestId_ActivationJob_Pass|JobServiceTester")]
        public async Task CreateJobByRequestId_ActivationJob_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand);
            Int32 createdJobId = rand.Next();
            Int32 procedureId = rand.Next();
            SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = JobStates.Acknowledged, JobType = JobTypes.SpliceInBranchable };
            SimpleJobOverview nextJob = new SimpleJobOverview { Id = createdJobId, State = JobStates.Open, JobType = JobTypes.ActivationJob };
            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.ActivatitionJobCreated, false, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(
            x => x.CustomerContactId == request.Customer.Id &&
            x.FlatId == request.Flat.Id))).ReturnsAsync(createdJobId);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, request.Id, createdJobId, userAuthId, MessageRelatedObjectTypes.ActivationJob, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            SimpleJobOverview actual = await service.CreateJobByRequestId(request.Id, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_RequestNotFound|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_RequestNotFound()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(requestId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_RequestNotFound|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_RequestIsClosed()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(requestId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_RequestIsInProgress|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_RequestIsInProgress()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand);
            Int32 createdJobId = rand.Next();
            Int32 procedureId = rand.Next();
            SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = JobStates.Acknowledged, JobType = JobTypes.HouseConnenction };
            SimpleJobOverview nextJob = new SimpleJobOverview { Id = createdJobId, State = JobStates.Open, JobType = JobTypes.Inject };
            request.IsInProgress = true;

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.InjectJobCreated, true, userAuthId, mockStorage);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(request.Id, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_CurrentJobNotFound|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_CurrentJobNotFound()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = GenerateDetailModel(rand);
            Int32 createdJobId = rand.Next();
            Int32 procedureId = rand.Next();
            SimpleJobOverview currentJob = null;

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.InjectJobCreated, true, userAuthId, mockStorage);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(request.Id, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_CurrentJobInvalidState|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_CurrentJobInvalidState()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            List<JobStates> invalidStates = new List<JobStates> { JobStates.Open, JobStates.Finished };

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            foreach (JobStates item in invalidStates)
            {
                RequestDetailModel request = GenerateDetailModel(rand);
                Int32 createdJobId = rand.Next();
                Int32 procedureId = rand.Next();
                SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = item, JobType = JobTypes.HouseConnenction }; ;

                var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
                PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.InjectJobCreated, true, userAuthId, mockStorage);

                IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(request.Id, userAuthId));

                Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
            }
        }

        [Fact(DisplayName = "CreateJobByRequestId_Fail_InvalidJobType|JobServiceTester")]
        public async Task CreateJobByRequestId_Fail_InvalidJobType()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            List<JobTypes> invalidTypes = new List<JobTypes> { JobTypes.BranchOff, JobTypes.Inventory, JobTypes.PrepareBranchableForSplice, JobTypes.SpliceInPoP };

            foreach (JobTypes invalidType in invalidTypes)
            {
                RequestDetailModel request = GenerateDetailModel(rand);
                Int32 createdJobId = rand.Next();
                Int32 procedureId = rand.Next();
                SimpleJobOverview currentJob = new SimpleJobOverview { Id = rand.Next(), State = JobStates.Acknowledged, JobType = invalidType }; ;

                var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
                PrepareStorage(rand, request, createdJobId, procedureId, currentJob, ProcedureStates.InjectJobCreated, true, userAuthId, mockStorage);

                IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateJobByRequestId(request.Id, userAuthId));

                Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
            }
        }
    }
}
