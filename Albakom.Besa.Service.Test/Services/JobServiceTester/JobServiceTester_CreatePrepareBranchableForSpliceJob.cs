﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreatePrepareBranchableForSpliceJob
    {
        private static void PrepareMockStorage(PrepareBranchableForSpliceJobCreateModel createModel, Int32 jobId, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfBranchableExists(createModel.BranchableId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfBranchableIsPreparedForSplice(createModel.BranchableId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.CreatePrepareBranchableForSpliceJob(createModel)).ReturnsAsync(jobId);
        }

        private PrepareBranchableForSpliceJobCreateModel GetValidCreateModel(Random rand)
        {
            Int32 branchableId = rand.Next();

            PrepareBranchableForSpliceJobCreateModel createModel = new PrepareBranchableForSpliceJobCreateModel
            {
                BranchableId = branchableId,
            };

            return createModel;
        }

        [Fact]
        public async Task CreatePrepareBranchableForSpliceJob_Pass()  
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            PrepareBranchableForSpliceJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.PrepareBranchableJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreatePrepareBranchableForSpliceJob(createModel, userAuthId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, createCounter);

        }

        [Fact]
        public async Task CreatePrepareBranchableForSpliceJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreatePrepareBranchableForSpliceJob(null, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact]
        public async Task CreatePrepareBranchableForSpliceJob_Fail_BranchableNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            PrepareBranchableForSpliceJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchableExists(createModel.BranchableId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreatePrepareBranchableForSpliceJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreatePrepareBranchableForSpliceJob_Fail_BranchableAlreadyPrepared()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            PrepareBranchableForSpliceJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBranchableIsPreparedForSplice(createModel.BranchableId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreatePrepareBranchableForSpliceJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreatePrepareBranchableForSpliceJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            PrepareBranchableForSpliceJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreatePrepareBranchableForSpliceJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
