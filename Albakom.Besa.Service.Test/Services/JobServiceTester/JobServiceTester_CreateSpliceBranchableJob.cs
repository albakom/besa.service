﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateSpliceBranchableJob
    {
        private static void PrepareMockStorage(SpliceBranchableJobCreateModel createModel, Int32 jobId, String userAuhtId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfFlatExists(createModel.FlatId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuhtId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfContactExists(createModel.CustomerContactId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinishedByFlatId(createModel.FlatId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCableIsInjectedByFlatId(createModel.FlatId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceIsFinishedByFlatId(createModel.FlatId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.CreateSpliceBranchableJob(createModel)).ReturnsAsync(jobId);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByFlatId(createModel.FlatId)).ReturnsAsync(new Int32?());
        }

        private SpliceBranchableJobCreateModel GetValidCreateModel(Random rand)
        {
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();

            SpliceBranchableJobCreateModel createModel = new SpliceBranchableJobCreateModel
            {
                CustomerContactId = contactId,
                FlatId = flatId,
            };

            return createModel;
        }

        [Fact(DisplayName = "CreateSpliceBranchableJob_Pass|JobServiceTester")]
        public async Task CreateSpliceBranchableJob_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);
            Int32 buildingId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
          

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.SpliceInBranchableJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);

            Int32 actualJobId = await service.CreateSpliceBranchableJob(createModel, userAuthId);
            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, createCounter);

        }

        [Fact(DisplayName = "CreateSpliceBranchableJob_Pass|JobServiceTester")]
        public async Task CreateSpliceBranchableJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);
            Int32 buildingId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingJobIdByFlatId(createModel.FlatId)).ReturnsAsync(buildingId);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(buildingId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == jobId && x.ProcedureId == procedureId && x.State == ProcedureStates.SpliceBranchableJobCreated
))).ReturnsAsync(timelineId);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.SpliceInBranchableJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);

            Int32 actualJobId = await service.CreateSpliceBranchableJob(createModel, userAuthId);
            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, createCounter);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(null, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_FlatNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfFlatExists(createModel.FlatId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_ContactNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfContactExists(createModel.CustomerContactId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_BuildingNotConnected()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnenctionIsFinishedByFlatId(createModel.FlatId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateSpliceBranchableJob_Fail_BuildingHasNoCable|JobServiceTester")]
        public async Task CreateSpliceBranchableJob_Fail_BuildingHasNoCable()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCableIsInjectedByFlatId(createModel.FlatId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_SpliceAlreadyFinished()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceIsFinishedByFlatId(createModel.FlatId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceBranchableJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            SpliceBranchableJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceBranchableJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
