﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateSpliceODFJob
    {
        private static void PrepareMockStorage(SpliceODFJobCreateModel createModel, Int32 jobId, String userAuhtId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfCableExists(createModel.MainCableId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuhtId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfODFSpliceIsFinished(createModel.MainCableId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.CreateSpliceODFJob(createModel)).ReturnsAsync(jobId);
        }

        private SpliceODFJobCreateModel GetValidCreateModel(Random rand)
        {
            Int32 mainCableID = rand.Next();

            SpliceODFJobCreateModel createModel = new SpliceODFJobCreateModel
            {
                MainCableId = mainCableID,
            };

            return createModel;
        }

        [Fact]
        public async Task CreateSpliceODFJob_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuhtId = Guid.NewGuid().ToString();

            SpliceODFJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuhtId, mockStorage);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.SpliceMainCableInPoPJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuhtId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualJobId = await service.CreateSpliceODFJob(createModel, userAuhtId);

            Assert.Equal(jobId, actualJobId);
            Assert.Equal(1, createCounter);
        }

        [Fact]
        public async Task CreateSpliceODFJob_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            Int32 buildingId = rand.Next();
            String userAuhtId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceODFJob(null, userAuhtId));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceODFJob_Fail_MainCableNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuhtId = Guid.NewGuid().ToString();

            SpliceODFJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuhtId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCableExists(createModel.MainCableId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceODFJob(createModel, userAuhtId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceODFJob_Fail_CableAlreadySpliced()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuhtId = Guid.NewGuid().ToString();

            SpliceODFJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuhtId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfODFSpliceIsFinished(createModel.MainCableId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceODFJob(createModel, userAuhtId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task CreateSpliceODFJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuhtId = Guid.NewGuid().ToString();

            SpliceODFJobCreateModel createModel = GetValidCreateModel(rand);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();
            PrepareMockStorage(createModel, jobId, userAuhtId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuhtId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateSpliceODFJob(createModel, userAuhtId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
