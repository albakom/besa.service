﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_DeleteCollectionJob
    {
        private class JobTypeInfo
        {
            public Int32 Id { get; set; }
            public JobTypes Type { get; set; }
            public Int32? ProcedureId { get; set; }
            public ProcedureStates? State { get; set; }
        }

        private ProcedureStates? GetState(JobTypes type)
        {
            ProcedureStates? result = null;
            switch (type)
            {
                case JobTypes.BranchOff:
                    result = ProcedureStates.BranchOffJobUnboud;
                    break;
                case JobTypes.HouseConnenction:
                    result = ProcedureStates.ConnectBuildingJobUnboud;
                    break;
                case JobTypes.Inject:
                    result = ProcedureStates.InjectJobUnboud;
                    break;
                case JobTypes.SpliceInBranchable:
                    result = ProcedureStates.SpliceBranchableJobUnboud;
                    break;
                case JobTypes.SpliceInPoP:
                    result = ProcedureStates.SpliceODFJobUnboud;
                    break;
                case JobTypes.PrepareBranchableForSplice:
                    result = ProcedureStates.PrepareBranchableForSpliceJobUnboud;
                    break;
                case JobTypes.ActivationJob:
                    result = ProcedureStates.ActivatitionJobUnboud;
                    break;
                case JobTypes.Inventory:
                    break;
                case JobTypes.ContactAfterFinished:
                    break;
                default:
                    break;
            }

            return result;
        }

        [Fact(DisplayName = "DeleteCollectionJob_Pass|JobServiceTester")]
        public async Task DeleteCollectionJob_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 collectionJobId = random.Next();
            Boolean result = true;

            Int32 boundedJobAmount = random.Next(30, 50);
            Dictionary<Int32, JobTypeInfo> boundedJobs = new Dictionary<int, JobTypeInfo>();
            for (int i = 0; i < boundedJobAmount; i++)
            {
                JobTypeInfo info = new JobTypeInfo
                {
                    Id = random.Next(),
                    ProcedureId = random.NextDouble() > 0.5 ? random.Next() : new Int32?(),
                    Type = (JobTypes)(random.Next(1, 8) * 10)
                };

                info.State = GetState(info.Type);
                if(info.State.HasValue == false) { continue; }
                boundedJobs.Add(info.Id, info);
            }

            List<Int32> boundedJobIds = new List<int>();

            Int32 timelineCounter = 0;
            storage.Setup(ctx => ctx.CheckIfCollectionJobExists(collectionJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            storage.Setup(ctx => ctx.DeleteCollectionJob(collectionJobId)).ReturnsAsync(result);
            storage.Setup(ctx => ctx.GetAllJobIdsFromCollectionJobId(collectionJobId)).ReturnsAsync(boundedJobs.Keys);
            storage.Setup(ctx => ctx.GetProcecureIdByJobId(It.Is<Int32>(x => boundedJobs.ContainsKey(x)))).ReturnsAsync<Int32,IBesaDataStorage,Int32?>((x) => boundedJobs[x].ProcedureId);
            storage.Setup(ctx => ctx.GetJobType(It.Is<Int32>(x => boundedJobs.ContainsKey(x)))).ReturnsAsync<Int32, IBesaDataStorage, JobTypes>((x) => boundedJobs[x].Type);
            storage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x => x.RelatedJobId.HasValue == true &&
boundedJobs.ContainsKey(x.RelatedJobId.Value) == true && x.ProcedureId == boundedJobs[x.RelatedJobId.Value].ProcedureId && x.State == boundedJobs[x.RelatedJobId.Value].State
))).ReturnsAsync(random.Next()).Callback( () => timelineCounter++);


            Int32 protocolConuter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Delete &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == collectionJobId &&
           x.RelatedType == MessageRelatedObjectTypes.CollectionJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(random.Next()).Callback(() => protocolConuter++);

            IJobService service = new JobService(storage.Object, fileManager, mockProtocolService.Object, logger);
            Boolean actual = await service.DeleteCollectionJob(collectionJobId, userAuthId);
            Assert.Equal(result, actual);
            Assert.Equal(1, protocolConuter);

            Assert.Equal(boundedJobs.Values.Count(x => x.ProcedureId.HasValue == true), timelineCounter);
        }

        [Fact(DisplayName = "DeleteCollectionJob_Failed_NotFound|JobServiceTester")]
        public async Task DeleteCollectionJob_Failed_NotFound()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 collectionJobId = random.Next();
            Boolean result = random.NextDouble() > 0.5;

            storage.Setup(ctx => ctx.CheckIfCollectionJobExists(collectionJobId)).ReturnsAsync(false);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DeleteCollectionJob(collectionJobId, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "DeleteCollectionJob_Failed_UserNotFound|JobServiceTester")]
        public async Task DeleteCollectionJob_Failed_UserNotFound()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 collectionJobId = random.Next();
            Boolean result = random.NextDouble() > 0.5;

            storage.Setup(ctx => ctx.CheckIfCollectionJobExists(collectionJobId)).ReturnsAsync(true);
            storage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DeleteCollectionJob(collectionJobId, userAuthId));
            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
