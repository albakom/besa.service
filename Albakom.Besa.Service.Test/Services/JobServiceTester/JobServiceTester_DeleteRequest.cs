﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_DeleteRequest
    {
        private static void PrepareStorage(bool expectedResult, int requestId, String userAuthId, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(new Int32?());
            mockStorage.Setup(ctx => ctx.DeleteCustomerRequest(requestId)).ReturnsAsync(expectedResult);
        }

        [Fact(DisplayName = "DeleteRequest_Pass|JobServiceTester")]
        public async Task DeleteRequest_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = true;
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(expectedResult, requestId, userAuthId, mockStorage);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Delete &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == requestId &&
           x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.DeleteRequest(requestId, userAuthId);

            Assert.Equal(expectedResult, result);
            Assert.Equal(1, createCounter);
        }

        [Fact(DisplayName = "DeleteRequest_WithProcedure_Pass|JobServiceTester")]
        public async Task DeleteRequest_WithProcedure_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = true;
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 procedureId = rand.Next();
            ProcedureTypes type = rand.NextDouble() > 0.5 ? ProcedureTypes.CustomerConnection : ProcedureTypes.HouseConnection;
            MessageRelatedObjectTypes objectType = (type == ProcedureTypes.CustomerConnection ? MessageRelatedObjectTypes.CustomerConnenctionProcedure : MessageRelatedObjectTypes.BuildingConnenctionProcedure);
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(expectedResult, requestId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByRequestId(requestId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.GetProcedureTypeById(procedureId)).ReturnsAsync(type);

            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(
                It.Is<ProcedureTimelineCreateModel>(x =>
               x.RelatedJobId.HasValue == false &&
               x.ProcedureId == procedureId &&
               x.RelatedRequestId == requestId &&
               x.State == ProcedureStates.RequestDeleted))).ReturnsAsync(rand.Next());

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Delete &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == requestId &&
           x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
            x.Action == MessageActions.FinishedProcedures &&
            x.Level == ProtocolLevels.Sucesss &&
            x.RelatedObjectId == procedureId &&
            x.RelatedType == objectType &&
            (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.DeleteRequest(requestId, userAuthId);

            Assert.Equal(expectedResult, result);
            Assert.Equal(2, createCounter);
        }

        [Fact(DisplayName = "DeleteRequest_Fail_RequestNotFound|JobServiceTester")]
        public async Task DeleteRequest_Fail_RequestNotFound()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(true, requestId, userAuthId, mockStorage);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DeleteRequest(requestId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "DeleteRequest_Fail_RequestIsClosed|JobServiceTester")]
        public async Task DeleteRequest_Fail_RequestIsClosed()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(true, requestId, userAuthId, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DeleteRequest(requestId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "DeleteRequest_Fail_UserNotFound|JobServiceTester")]
        public async Task DeleteRequest_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(true, requestId, userAuthId, mockStorage);

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DeleteRequest(requestId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
