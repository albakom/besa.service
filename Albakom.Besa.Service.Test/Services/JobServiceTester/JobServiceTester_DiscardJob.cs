﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_DiscardJob
    {
        private Boolean CheckSets(IEnumerable<String> urlsToCheck, HashSet<String> expectedIds)
        {
            HashSet<String> usedIds = new HashSet<String>(urlsToCheck);

            if (usedIds.Count != expectedIds.Count) { return false; }

            foreach (String item in usedIds)
            {
                if (expectedIds.Contains(item) == false) { return false; }
            }

            return true;
        }

        private static void PrepareMockStorage(Int32 jobId, bool expectedCreateResult, String userAuthId, IEnumerable<String> fileUrls, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(jobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetFileUrlsByJobId(jobId)).ReturnsAsync(fileUrls);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.DiscardJob(jobId)).ReturnsAsync(expectedCreateResult);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(jobId)).ReturnsAsync(new Int32?());
            mockStorage.Setup(ctx => ctx.GetJobType(jobId)).ReturnsAsync(JobTypes.BranchOff);
        }

        [Fact(DisplayName = "DiscardJob_Pass|JobServiceTester")]
        public async Task DiscardJob_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = true;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 fileAmount = rand.Next(3, 10);
            HashSet<String> urls = new HashSet<string>(fileAmount);

            for (int i = 0; i < fileAmount; i++)
            {
                String url = $"https://meineSeite.de/irgendeinPfad/Nochtwas/{rand.Next()}";
                urls.Add(url);
            }

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            var mockFileManager = new Mock<IFileManager>(MockBehavior.Strict);
            mockFileManager.Setup(ctx => ctx.Delete(It.Is<IEnumerable<String>>((input) => CheckSets(input, urls)))).Returns(Task.CompletedTask);
            PrepareMockStorage(jobId, expectedResult, userAuthId, urls, mockStorage);

            IJobService service = new JobService(mockStorage.Object, mockFileManager.Object, Mock.Of<IProtocolService>(), logger);
            Boolean result = await service.DiscardJob(jobId, userAuthId);

            Assert.Equal(expectedResult, result);
        }

        [Fact(DisplayName = "DiscardJob_WithProtocol_Pass|JobServiceTester")]
        public async Task DiscardJob_WithProtocol_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = true;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Dictionary<JobTypes, MessageRelatedObjectTypes> states = new Dictionary<JobTypes, MessageRelatedObjectTypes>();
            states.Add(JobTypes.ActivationJob, MessageRelatedObjectTypes.ActivationJob);
            states.Add(JobTypes.BranchOff, MessageRelatedObjectTypes.BranchOffJob);
            states.Add(JobTypes.HouseConnenction, MessageRelatedObjectTypes.ConnectBuildingJob);
            states.Add(JobTypes.Inject, MessageRelatedObjectTypes.InjectJob);
            states.Add(JobTypes.Inventory, MessageRelatedObjectTypes.InventoryJob);
            states.Add(JobTypes.PrepareBranchableForSplice, MessageRelatedObjectTypes.PrepareBranchableJob);
            states.Add(JobTypes.SpliceInBranchable, MessageRelatedObjectTypes.SpliceInBranchableJob);
            states.Add(JobTypes.SpliceInPoP, MessageRelatedObjectTypes.SpliceMainCableInPoPJob);
            states.Add(JobTypes.ContactAfterFinished, MessageRelatedObjectTypes.ContactAfterFinishedJob);

            Int32 fileAmount = rand.Next(3, 10);
            HashSet<String> urls = new HashSet<string>(fileAmount);

            for (int i = 0; i < fileAmount; i++)
            {
                String url = $"https://meineSeite.de/irgendeinPfad/Nochtwas/{rand.Next()}";
                urls.Add(url);
            }

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            var mockFileManager = new Mock<IFileManager>(MockBehavior.Strict);
            mockFileManager.Setup(ctx => ctx.Delete(It.Is<IEnumerable<String>>((input) => CheckSets(input, urls)))).Returns(Task.CompletedTask);
            PrepareMockStorage(jobId, expectedResult, userAuthId, urls, mockStorage);

            foreach (KeyValuePair<JobTypes, MessageRelatedObjectTypes> item in states)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(jobId)).ReturnsAsync(item.Key);

                Int32 createCounter = 0;
                var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.JobDiscared &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == jobId&&
               x.RelatedType == item.Value &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

                IJobService service = new JobService(mockStorage.Object, mockFileManager.Object, mockProtocolService.Object, logger);
                Boolean result = await service.DiscardJob(jobId, userAuthId);

                Assert.Equal(expectedResult, result);
                Assert.Equal(1, createCounter);
            }
        }

        [Fact(DisplayName = "DiscardJob_WithProcedure_Pass|JobServiceTester")]
        public async Task DiscardJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 jobId = rand.Next();
            Int32 timelineId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 fileAmount = rand.Next(3, 10);
            HashSet<String> urls = new HashSet<string>(fileAmount);

            Int32 procedureId = rand.Next();

            for (int i = 0; i < fileAmount; i++)
            {
                String url = $"https://meineSeite.de/irgendeinPfad/Nochtwas/{rand.Next()}";
                urls.Add(url);
            }

            Dictionary<JobTypes, ProcedureStates> states = new Dictionary<JobTypes, ProcedureStates>
            {
                { JobTypes.HouseConnenction, ProcedureStates.ConnectBuildingJobDiscarded },
                { JobTypes.Inject, ProcedureStates.InjectJobDiscarded },
                { JobTypes.PrepareBranchableForSplice, ProcedureStates.SpliceBranchableJobDiscarded },
                { JobTypes.ActivationJob, ProcedureStates.ActivatitionJobDiscarded }
            };

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(jobId)).ReturnsAsync(procedureId);

            var mockFileManager = new Mock<IFileManager>(MockBehavior.Strict);
            mockFileManager.Setup(ctx => ctx.Delete(It.Is<IEnumerable<String>>((input) => CheckSets(input, urls)))).Returns(Task.CompletedTask);
            PrepareMockStorage(jobId, expectedResult, userAuthId, urls, mockStorage);

            foreach (KeyValuePair<JobTypes, ProcedureStates> item in states)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(jobId)).ReturnsAsync(item.Key);
                mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == jobId && x.ProcedureId == procedureId && x.State == item.Value
))).ReturnsAsync(timelineId);

                IJobService service = new JobService(mockStorage.Object, mockFileManager.Object, Mock.Of<IProtocolService>(), logger);
                Boolean result = await service.DiscardJob(jobId, userAuthId);

                Assert.Equal(expectedResult, result);
            }
        }

        [Fact(DisplayName = "DiscardJob_WithProcedure_Pass|JobServiceTester")]
        public async Task DiscardJob_WithProcedure_ButWithoutValidJobTypes_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 fileAmount = rand.Next(3, 10);
            HashSet<String> urls = new HashSet<string>(fileAmount);

            Int32 procedureId = rand.Next();

            for (int i = 0; i < fileAmount; i++)
            {
                String url = $"https://meineSeite.de/irgendeinPfad/Nochtwas/{rand.Next()}";
                urls.Add(url);
            }

            List<JobTypes> typesWithNoProcedures = new List<JobTypes> { JobTypes.BranchOff, JobTypes.PrepareBranchableForSplice, JobTypes.SpliceInPoP };


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(jobId)).ReturnsAsync(procedureId);

            var mockFileManager = new Mock<IFileManager>(MockBehavior.Strict);
            mockFileManager.Setup(ctx => ctx.Delete(It.Is<IEnumerable<String>>((input) => CheckSets(input, urls)))).Returns(Task.CompletedTask);
            PrepareMockStorage(jobId, expectedResult, userAuthId, urls, mockStorage);

            foreach (JobTypes item in typesWithNoProcedures)
            {
                mockStorage.Setup(ctx => ctx.GetJobType(jobId)).ReturnsAsync(item);

                IJobService service = new JobService(mockStorage.Object, mockFileManager.Object, Mock.Of<IProtocolService>(), logger);
                Boolean result = await service.DiscardJob(jobId, userAuthId);

                Assert.Equal(expectedResult, result);
            }
        }

        [Fact(DisplayName = "DiscardJob_Fail_JobNotFound|JobServiceTester")]
        public async Task DiscardJob_Fail_JobNotFound()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(jobId, expectedResult, userAuthId, new List<String>(), mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DiscardJob(jobId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "DiscardJob_Fail_JobNotFinished|JobServiceTester")]
        public async Task DiscardJob_Fail_JobNotFinished()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(jobId, expectedResult, userAuthId, new List<String>(), mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DiscardJob(jobId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "DiscardJob_Fail_UserNotFound|JobServiceTester")]
        public async Task DiscardJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(jobId, expectedResult, userAuthId, new List<String>(), mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DiscardJob(jobId, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound , exception.Reason);
        }

        [Fact(DisplayName = "DiscardJob_Fail_JobIsAcknowledged|JobServiceTester")]
        public async Task DiscardJob_Fail_JobIsAcknowledged()
        {
            Random rand = new Random();
            Boolean expectedResult = true;
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(jobId, expectedResult, userAuthId, new List<String>(), mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsAcknowledged(jobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.DiscardJob(jobId, userAuthId));
            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
