﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_FinishACtivationJob
    {
        private class FinishJobTestData
        {
            public Int32 JobId { get; set; }
            public String AuthId { get; set; }
            public HashSet<Int32> FileIds { get; set; }
            public ActivationJobFinishModel Model { get; set; }

            public FinishJobTestData(Random rand)
            {
                JobId = rand.Next();
                AuthId = Guid.NewGuid().ToString();

                Int32 fileAmount = rand.Next(1, 10);

                FileIds = new HashSet<Int32>(fileAmount);
                while (FileIds.Count < fileAmount)
                {
                    Int32 fileId = rand.Next();
                    if (FileIds.Contains(fileId) == true) continue;

                    FileIds.Add(fileId);
                }

                Model = new ActivationJobFinishModel
                {
                    Comment = $"Meine tolle BEschreibung {rand.Next()}",
                    JobId = JobId,
                    ProblemHappend = new ExplainedBooleanModel
                    {
                        Value = true,
                        Description = $"Verlegefehler mit der Nummer {rand.Next()}",
                    },
                    UserAuthId = AuthId,
                    FileIds = FileIds,
                };
            }
        }

        private static void PrepareMockStorage(FinishJobTestData inputData, bool expectedCreateResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfFileExists(inputData.FileIds)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.FinishActivationJob(inputData.Model)).ReturnsAsync(expectedCreateResult);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.Model.JobId)).ReturnsAsync(new Int32?());
        }

        [Fact(DisplayName = "FinishActivationJob_WithoutProcedure_Pass|JobServiceTester")]
        public async Task FinishActivationJob_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            FinishJobTestData inputData = new FinishJobTestData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);

            Int32 protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobFinished &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.ActivationJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => protocolCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.FinishActivationJob(inputData.Model);

            Assert.Equal(expectedCreateResult, result);
            Assert.Equal(1, protocolCounter);


            TimeSpan diff = DateTimeOffset.Now - inputData.Model.Timestamp;
            Assert.True(diff < TimeSpan.FromMinutes(3));
        }

        [Fact(DisplayName = "FinishActivationJob_WithProcedure_Pass|JobServiceTester")]
        public async Task FinishActivationJob_WithProcedure_Pass()
        {
            Random rand = new Random();
            FinishJobTestData inputData = new FinishJobTestData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.Model.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ActivatitionJobFinished
))).ReturnsAsync(timelineId);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            Boolean result = await service.FinishActivationJob(inputData.Model);

            Assert.Equal(expectedCreateResult, result);

            TimeSpan diff = DateTimeOffset.Now - inputData.Model.Timestamp;
            Assert.True(diff < TimeSpan.FromMinutes(3));
        }

        [Fact(DisplayName = "FinishActivationJob_Fail_JobNotFound|JobServiceTester")]
        public async Task FinishActivationJob_Fail_JobNotFound()
        {
            Random rand = new Random();
            FinishJobTestData inputData = new FinishJobTestData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishActivationJob(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        }

        [Fact(DisplayName = "FinishActivationJob_Fail_JobNotFound|JobServiceTester")]
        public async Task FinishSpliceBranchableJob_Fail_JobAlreadyFinished()
        {
            Random rand = new Random();
            FinishJobTestData inputData = new FinishJobTestData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishActivationJob(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "FinishActivationJob_Fail_JobNotFound|JobServiceTester")]
        public async Task FinishActivationJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            FinishJobTestData inputData = new FinishJobTestData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            String authId = inputData.AuthId;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            inputData.Model.UserAuthId = String.Empty;

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishActivationJob(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);

            inputData.Model.UserAuthId = authId;
            exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishActivationJob(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        }

        //[Fact(DisplayName = "FinishActivationJob_Fail_FileNotFound|JobServiceTester")]
        //public async Task FinishActivationJob_Fail_FileNotFound()
        //{
        //    Random rand = new Random();
        //    FinishJobTestData inputData = new FinishJobTestData(rand);
        //    Boolean expectedCreateResult = rand.NextDouble() > 0.5;

        //    var logger = Mock.Of<ILogger<JobService>>();
        //    var fileManager = Mock.Of<IFileManager>();

        //    var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
        //    PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
        //    mockStorage.Setup(ctx => ctx.CheckIfFileExists(inputData.FileIds)).ReturnsAsync(false);

        //    IJobService service = new JobService(mockStorage.Object, fileManager, logger);
        //    JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishActivationJob(inputData.Model));

        //    Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        //}

        //[Fact(DisplayName = "FinishActivationJob_Fail_InvalidModel|JobServiceTester")]
        //public async Task FinishActivationJob_Fail_InvalidModel()
        //{
        //    Random rand = new Random();
        //    var logger = Mock.Of<ILogger<JobService>>();
        //    var fileManager = Mock.Of<IFileManager>();

        //    Dictionary<FinishJobTestData, JobServicExceptionReasons> modelsToFail = new Dictionary<FinishJobTestData, JobServicExceptionReasons>();
        //    {
        //        FinishJobTestData inputData = new FinishJobTestData(rand);
        //        inputData.Model.UserAuthId = String.Empty;
        //        modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
        //    }

        //    {
        //        FinishJobTestData inputData = new FinishJobTestData(rand);
        //        inputData.Model.CableMetric = 0;
        //        modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
        //    }

        //    {
        //        FinishJobTestData inputData = new FinishJobTestData(rand);
        //        inputData.Model.CableMetric = -rand.Next();
        //        modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
        //    }

        //    {
        //        FinishJobTestData inputData = new FinishJobTestData(rand);
        //        inputData.Model.FileIds = null;
        //        modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
        //    }

        //    {
        //        FinishJobTestData inputData = new FinishJobTestData(rand);
        //        inputData.Model.ProblemHappend = new ExplainedBooleanModel { Value = true, Description = null };
        //        modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
        //    }

        //    var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
        //    mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

        //    IJobService service = new JobService(mockStorage.Object, fileManager, logger);

        //    JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(null));
        //    Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);

        //    foreach (KeyValuePair<FinishJobTestData, JobServicExceptionReasons> item in modelsToFail)
        //    {
        //        JobServicException itemException = await Assert.ThrowsAsync<JobServicException>(() => service.FinishSpliceBranchableJob(item.Key.Model));
        //        Assert.Equal(item.Value, itemException.Reason);
        //    }
        //}
    }
}
