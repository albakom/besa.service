﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.CompanyServiceTester
{
    public class JobServiceTester_FinishBranchOffJobModel
    {
        private class FinishBranchOffData
        {
            public Int32 JobId { get; set; }
            public String AuthId { get; set; }
            public HashSet<Int32> FileIds { get; set; }
            public FinishBranchOffJobModel Model { get; set; }

            public FinishBranchOffData(Random rand)
            {
                JobId = rand.Next();
                AuthId = Guid.NewGuid().ToString();

                Int32 fileAmount = rand.Next(1, 10);

                FileIds = new HashSet<Int32>(fileAmount);
                while (FileIds.Count < fileAmount)
                {
                    Int32 fileId = rand.Next();
                    if (FileIds.Contains(fileId) == true) continue;

                    FileIds.Add(fileId);
                }

                Model = new FinishBranchOffJobModel
                {
                    Comment = $"Meine tolle BEschreibung {rand.Next()}",
                    DuctChanged = new ExplainedBooleanModel
                    {
                        Value = true,
                        Description = $"Fehler mit der Nummer {rand.Next()}",
                    },
                    JobId = JobId,
                    ProblemHappend = new ExplainedBooleanModel
                    {
                        Value = true,
                        Description = $"Verlegefehler mit der Nummer {rand.Next()}",
                    },
                    UserAuthId = AuthId,
                    FileIds = FileIds,
                };
            }
        }


        private static void PrepareMockStorage(FinishBranchOffData inputData, bool expectedCreateResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfFileExists(inputData.FileIds)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.FinishBranchOffJob(inputData.Model)).ReturnsAsync(expectedCreateResult);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(new Int32?());
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Pass()
        {
            Random rand = new Random();
            FinishBranchOffData inputData = new FinishBranchOffData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);

            Int32 createCounter = 0;

            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobFinished &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == inputData.JobId &&
           x.RelatedType == MessageRelatedObjectTypes.BranchOffJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), inputData.AuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.FinishBranchOffJobModel(inputData.Model);

            Assert.Equal(expectedCreateResult, result);
            Assert.Equal(1, createCounter);

            TimeSpan diff = DateTimeOffset.Now - inputData.Model.Timestamp;
            Assert.True(diff < TimeSpan.FromMinutes(3));
        }


        [Fact]
        public async Task FinishBranchOffJobModel_Fail_JobNotFound()
        {
            Random rand = new Random();
            FinishBranchOffData inputData = new FinishBranchOffData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Fail_JobAlreadyFinished()
        {
            Random rand = new Random();
            FinishBranchOffData inputData = new FinishBranchOffData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Fail_UserNotFound()
        {
            Random rand = new Random();
            FinishBranchOffData inputData = new FinishBranchOffData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            String authId = inputData.AuthId;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            inputData.Model.UserAuthId = String.Empty;

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);

            inputData.Model.UserAuthId = authId;
            exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Fail_FileNotFound()
        {
            Random rand = new Random();
            FinishBranchOffData inputData = new FinishBranchOffData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfFileExists(inputData.FileIds)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidFinishedModel, exception.Reason);
        }

        [Fact]
        public async Task FinishBranchOffJobModel_Fail_InvalidModel()
        {
            Random rand = new Random();
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            Dictionary<FinishBranchOffData, JobServicExceptionReasons> modelsToFail = new Dictionary<FinishBranchOffData, JobServicExceptionReasons>();
            {
                FinishBranchOffData inputData = new FinishBranchOffData(rand);
                inputData.Model.UserAuthId = String.Empty;
                modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
            }

            {
                FinishBranchOffData inputData = new FinishBranchOffData(rand);
                inputData.Model.DuctChanged = new ExplainedBooleanModel { Value = true, Description = null };
                modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
            }

            {
                FinishBranchOffData inputData = new FinishBranchOffData(rand);
                inputData.Model.FileIds = null;
                modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
            }

            {
                FinishBranchOffData inputData = new FinishBranchOffData(rand);
                inputData.Model.ProblemHappend = new ExplainedBooleanModel { Value = true, Description = null };
                modelsToFail.Add(inputData, JobServicExceptionReasons.InvalidFinishedModel);
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(null));
            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);

            foreach (KeyValuePair<FinishBranchOffData, JobServicExceptionReasons> item in modelsToFail)
            {
                JobServicException itemException = await Assert.ThrowsAsync<JobServicException>(() => service.FinishBranchOffJobModel(item.Key.Model));
                Assert.Equal(item.Value, itemException.Reason);
            }



        }
    }
}
