﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_FinishJobAdministrative
    {
        private class FinishByAdminJobData
        {
            public Int32 JobId { get; set; }
            public String AuthId { get; set; }
            public FinishJobAdministrativeModel Model { get; set; }

            public FinishByAdminJobData(Random rand)
            {
                JobId = rand.Next();
                AuthId = Guid.NewGuid().ToString();

                Int32 fileAmount = rand.Next(1, 10);

                Model = new FinishJobAdministrativeModel
                {
                    JobId = JobId,
                    UserAuthId = AuthId,
                    FinishedAt = rand.NextDouble() > 0.5 ? DateTimeOffset.Now : new Nullable<DateTimeOffset>(),
                };
            }
        }

        private static void PrepareMockStorage(FinishByAdminJobData inputData, bool expectedCreateResult, JobTypes type, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserHasRole(inputData.AuthId, BesaRoles.ProjectManager)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(new Int32?());
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(type);

            mockStorage.Setup(ctx => ctx.FinishAndAcknowledgeJobAdministrative(inputData.Model)).ReturnsAsync(1);
        }

        private static void CheckAcknowledgeJobOutput(FinishByAdminJobData inputData, bool expectedCreateResult, bool result)
        {
            Assert.Equal(expectedCreateResult, result);
            if (inputData.Model.FinishedAt.HasValue == false)
            {
                // TimeSpan diff = DateTimeOffset.Now - inputData.Model.FinishedAt.Value;
                // Assert.True(diff < TimeSpan.FromMinutes(3));
            }
            else
            {
                //  Assert.Equal(inputData.Model.FinishedAt,)
            }
        }

        private Int32 _protocolCounter = 0;

        private void PrepareProtocolService(Random rand, int jobId, MessageRelatedObjectTypes jobType, string userAuthId, Int32? procedureId, MessageRelatedObjectTypes? procedureType, Mock<IProtocolService> mockProtocolService)
        {
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobFinishJobAdministrative &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == jobType &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            if (procedureId.HasValue == true)
            {
                mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.FinishedProcedures &&
                x.Level == ProtocolLevels.Sucesss &&
                x.RelatedObjectId == procedureId &&
                x.RelatedType == procedureType.Value &&
                (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);
            }
        }

        [Fact(DisplayName = "FinishJobAdministrative_WithoutProcedure_Pass|JobServiceTester")]
        public async Task FinishJobAdministrative_WithoutProcedure_Pass()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = true;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.BranchOff, mockStorage);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, inputData.JobId, MessageRelatedObjectTypes.BranchOffJob, inputData.AuthId, null, null, mockProtocolService);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.FinishJobAdministrative(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
            Assert.Equal(1, _protocolCounter);
        }

        [Fact(DisplayName = "FinishJobAdministrative_WitProcedure_Pass|JobServiceTester")]
        public async Task FinishJobAdministrative_WitProcedure_Pass()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            Dictionary<JobTypes, List<ProcedureStates>> types = new Dictionary<JobTypes, List<ProcedureStates>>();
            types.Add(JobTypes.HouseConnenction, new List<ProcedureStates> { ProcedureStates.ConnectBuildingJobAcknlowedged, ProcedureStates.ConnectBuildingJobFinished });
            types.Add(JobTypes.Inject, new List<ProcedureStates> { ProcedureStates.InjectJobFinished, ProcedureStates.InjectJobAcknlowedged });
            types.Add(JobTypes.SpliceInBranchable, new List<ProcedureStates> { ProcedureStates.SpliceBranchableJobFinished, ProcedureStates.SpliceBranchableJobAcknlowedged });
            types.Add(JobTypes.ActivationJob, new List<ProcedureStates> { ProcedureStates.ActivatitionJobFinished, ProcedureStates.ActivatitionJobAcknlowedged });

            Dictionary<JobTypes, MessageRelatedObjectTypes> messageTypes = new Dictionary<JobTypes, MessageRelatedObjectTypes>()
            {
                { JobTypes.HouseConnenction, MessageRelatedObjectTypes.ConnectBuildingJob  },
                { JobTypes.Inject, MessageRelatedObjectTypes.InjectJob  },
                { JobTypes.SpliceInBranchable, MessageRelatedObjectTypes.SpliceInBranchableJob  },
                { JobTypes.ActivationJob, MessageRelatedObjectTypes.ActivationJob  },
            };

            foreach (KeyValuePair<JobTypes, List<ProcedureStates>> item in types)
            {
                Int32 timelineCounter = 0;

                var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
                PrepareMockStorage(inputData, expectedCreateResult, item.Key, mockStorage);
                mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
                mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);
                mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(item.Key);
                mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
             x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && item.Value.Contains(x.State)
))).ReturnsAsync(timelineId).Callback(() => timelineCounter++);

                if (item.Key == JobTypes.HouseConnenction)
                {
                    mockStorage.Setup(ctx => ctx.CheckIfContactAfterFinishedJobShouldCreated(procedureId)).ReturnsAsync(false);
                }

                _protocolCounter = 0;
                var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
                PrepareProtocolService(rand, inputData.JobId, messageTypes[item.Key], inputData.AuthId, null, null, mockProtocolService);

                IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
                Boolean result = await service.FinishJobAdministrative(inputData.Model);

                CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
                Assert.Equal(1, _protocolCounter);
                Assert.Equal(2, timelineCounter);
            }
        }

        [Fact(DisplayName = "FinishJobAdministrative_WitProcedureAndCreateContactAfterSales_Pass|JobServiceTester")]
        public async Task FinishJobAdministrative_WitProcedureAndCreateContactAfterSales_Pass()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 buildingId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            List<ProcedureStates> expectedProcedureStates = new List<ProcedureStates> {
                ProcedureStates.ConnectBuildingJobAcknlowedged,
                ProcedureStates.ConnectBuildingJobFinished
            };

            Int32 timelineCounter = 0;
            Int32 jobCounter = 0;
            Int32 contactAfterFinishedJobid = rand.Next();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.HouseConnenction, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);
            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetJobType(inputData.JobId)).ReturnsAsync(JobTypes.HouseConnenction);
            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && expectedProcedureStates.Contains(x.State)
))).ReturnsAsync(timelineId).Callback(() => timelineCounter++);

            mockStorage.Setup(ctx => ctx.CheckIfContactAfterFinishedJobShouldCreated(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByConnectionJobId(inputData.JobId)).ReturnsAsync(buildingId);
            mockStorage.Setup(ctx => ctx.CreateContactAfterFinishedJob(It.Is<CreateContactAfterFinishedJobModel>(x =>
x.BuildingId == buildingId && x.RelatedProcedureId == procedureId))).ReturnsAsync(contactAfterFinishedJobid).Callback(() => jobCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, inputData.JobId, MessageRelatedObjectTypes.ConnectBuildingJob, inputData.AuthId, null, null, mockProtocolService);

            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.Create &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == contactAfterFinishedJobid &&
               x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.FinishJobAdministrative(inputData.Model);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);

            Assert.Equal(2, timelineCounter);
            Assert.Equal(1, jobCounter);
            Assert.Equal(2, _protocolCounter);
        }

        [Fact(DisplayName = "FinishJobAdministrative_InjectJob_WitProcedure_AndTriggerNextJob_Pass|JobServiceTester")]
        public async Task FinishJobAdministrative_InjectJob_WitProcedure_AndTriggerNextJob_Pass()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            inputData.Model.CreateNextJobInPipeLine = true;
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 customerId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 nextJobId = rand.Next();

            Int32 timelineCounter = 0;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.Inject, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExists(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCreateSpliceJobInfoByCustomerConnectionProcedureId(procedureId)).ReturnsAsync(new SpliceBranchableJobCreateModel
            {
                CustomerContactId = customerId,
                FlatId = flatId,
            });
            mockStorage.Setup(ctx => ctx.CreateSpliceBranchableJob(It.Is<SpliceBranchableJobCreateModel>(x =>
x.CustomerContactId == customerId && x.FlatId == flatId
))).ReturnsAsync(nextJobId);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         (x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && (x.State == ProcedureStates.InjectJobFinished || x.State == ProcedureStates.InjectJobAcknlowedged)) ||
         (x.RelatedJobId == nextJobId && x.ProcedureId == procedureId && x.State == ProcedureStates.SpliceBranchableJobCreated)
))).ReturnsAsync(timelineId).Callback(() => timelineCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, inputData.JobId, MessageRelatedObjectTypes.InjectJob, inputData.AuthId, procedureId, MessageRelatedObjectTypes.CustomerConnenctionProcedure, mockProtocolService);

            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.Create &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == nextJobId &&
               x.RelatedType == MessageRelatedObjectTypes.SpliceInBranchableJob &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);

            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);
            Boolean result = await service.FinishJobAdministrative(inputData.Model);

            Assert.Equal(3, timelineCounter);
            Assert.Equal(3, _protocolCounter);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
        }

        [Fact(DisplayName = "FinishJobAdministrative_SpliceBranchableJob_WitProcedure_AndTriggerNextJob_Pass|JobServiceTester")]
        public async Task FinishJobAdministrative_SpliceBranchableJob_WitProcedure_AndTriggerNextJob_Pass()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            inputData.Model.CreateNextJobInPipeLine = true;
            Boolean expectedCreateResult = true;
            Int32 procedureId = rand.Next();
            Int32 timelineId = rand.Next();
            Int32 customerId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 nextJobId = rand.Next();

            Int32 timelineCounter = 0;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.SpliceInBranchable, mockStorage);
            mockStorage.Setup(ctx => ctx.GetProcecureIdByJobId(inputData.JobId)).ReturnsAsync(procedureId);

            mockStorage.Setup(ctx => ctx.CheckIfCustomerConnectionProcedureExists(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetCreateActivationJobInfoByCustomerConnectionProcedureId(procedureId)).ReturnsAsync(new ActivationJobCreateModel
            {
                CustomerContactId = customerId,
                FlatId = flatId,
            });
            mockStorage.Setup(ctx => ctx.CreateActivationJob(It.Is<ActivationJobCreateModel>(x =>
x.CustomerContactId == customerId && x.FlatId == flatId
))).ReturnsAsync(nextJobId);

            mockStorage.Setup(ctx => ctx.CheckCheckIfProcedureIsFinished(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.MarkProcedureAsFinished(procedureId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.AddProcedureTimelineElementIfNessesary(It.Is<ProcedureTimelineCreateModel>(x =>
         (x.RelatedJobId == inputData.JobId && x.ProcedureId == procedureId && (x.State == ProcedureStates.SpliceBranchableJobFinished || x.State == ProcedureStates.SpliceBranchableJobAcknlowedged)) ||
         (x.RelatedJobId == nextJobId && x.ProcedureId == procedureId && x.State == ProcedureStates.ActivatitionJobCreated)
))).ReturnsAsync(timelineId).Callback(() => timelineCounter++);

            _protocolCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            PrepareProtocolService(rand, inputData.JobId, MessageRelatedObjectTypes.SpliceInBranchableJob, inputData.AuthId, procedureId, MessageRelatedObjectTypes.CustomerConnenctionProcedure, mockProtocolService);

            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
               x.Action == MessageActions.Create &&
               x.Level == ProtocolLevels.Sucesss &&
               x.RelatedObjectId == nextJobId &&
               x.RelatedType == MessageRelatedObjectTypes.ActivationJob &&
               (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => _protocolCounter++);


            IJobService service = new JobService(mockStorage.Object, fileManger, mockProtocolService.Object, logger);

            Boolean result = await service.FinishJobAdministrative(inputData.Model);

            Assert.Equal(3, timelineCounter);
            Assert.Equal(3, _protocolCounter);

            CheckAcknowledgeJobOutput(inputData, expectedCreateResult, result);
        }

        [Fact(DisplayName = "FinishJobAdministrative_JobNotFound_Fail|JobServiceTester")]
        public async Task FinishJobAdministrative_JobNotFound_Fail()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.BranchOff, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobExists(inputData.JobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishJobAdministrative(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "FinishJobAdministrative_JobAllreadyFinished_Fail|JobServiceTester")]
        public async Task FinishJobAdministrative_JobAllreadyFinished_Fail()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.BranchOff, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(inputData.JobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishJobAdministrative(inputData.Model));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "FinishJobAdministrative_UserNotFound_Fail|JobServiceTester")]
        public async Task FinishJobAdministrative_UserNotFound_Fail()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;

            String authId = inputData.AuthId;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.BranchOff, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists("")).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(inputData.AuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);

            inputData.Model.UserAuthId = String.Empty;

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishJobAdministrative(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);

            inputData.Model.UserAuthId = authId;
            exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishJobAdministrative(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "FinishJobAdministrative_WrongRole_Fail|JobServiceTester")]
        public async Task FinishJobAdministrative_WrongRole_Fail()
        {
            Random rand = new Random();
            FinishByAdminJobData inputData = new FinishByAdminJobData(rand);
            Boolean expectedCreateResult = rand.NextDouble() > 0.5;
            String authId = inputData.AuthId;

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareMockStorage(inputData, expectedCreateResult, JobTypes.BranchOff, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserHasRole(inputData.AuthId, BesaRoles.ProjectManager)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger, Mock.Of<IProtocolService>(), logger);

            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishJobAdministrative(inputData.Model));
            Assert.Equal(JobServicExceptionReasons.WrongRole, exception.Reason);
        }
    }
}
