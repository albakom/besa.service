﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetActivationJobDetails
    {
        [Fact(DisplayName = "GetActivationJobDetails|JobServiceTester")]
        public async Task GetActivationJobDetailss_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            ActivationJobDetailModel detailModel = new ActivationJobDetailModel
            {
                Id = jobId,
                State = (JobStates)rand.Next(1, 4),
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.Splice);
            mockStorage.Setup(ctx => ctx.GetActivationJobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            ActivationJobDetailModel actual = await service.GetActivationJobDetails(jobId, requesterId);
            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetActivationJobDetails_WithFinishedInfo_Pass|JobServiceTester")]
        public async Task GetActivationJobDetails_WithFinishedInfo_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            ActivationJobDetailModel detailModel = new ActivationJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishActivationJobDetailModel finishDetails = new FinishActivationJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.ProjectManager);
            mockStorage.Setup(ctx => ctx.GetActivationJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetActivationFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            ActivationJobDetailModel actual = await service.GetActivationJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetActivationJobDetails_WithFinishedInfo_ByManagement_Pass|JobServiceTester")]
        public async Task GetActivationJobDetails_WithFinishedInfo_ByManagement_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            ActivationJobDetailModel detailModel = new ActivationJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishActivationJobDetailModel finishDetails = new FinishActivationJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetActivationJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetActivationFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            ActivationJobDetailModel actual = await service.GetActivationJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetActivationJobDetails_Fail_JobNotFound|JobServiceTester")]
        public async Task GetActivationJobDetails_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfActivationJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetActivationJobDetails(jobId, String.Empty));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
