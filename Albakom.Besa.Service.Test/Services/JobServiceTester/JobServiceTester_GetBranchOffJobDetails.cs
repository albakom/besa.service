﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetBranchOffJobDetails
    {
        private static void CheckModel(int jobId, BranchOffJobDetailModel detailModel, BranchOffJobDetailModel actual)
        {
            Assert.NotNull(actual);

            Assert.Equal(jobId, actual.Id);
            Assert.NotNull(actual.BuildingInfo);

            Assert.Equal(detailModel.State, actual.State);

            Assert.Equal(detailModel.BuildingInfo.Id, actual.BuildingInfo.Id);
            Assert.Equal(detailModel.BuildingInfo.StreetName, actual.BuildingInfo.StreetName);

            Assert.NotNull(actual.BuildingInfo.Coordinate);
            Assert.Equal(detailModel.BuildingInfo.Coordinate.Latitude, actual.BuildingInfo.Coordinate.Latitude);
            Assert.Equal(detailModel.BuildingInfo.Coordinate.Longitude, actual.BuildingInfo.Coordinate.Longitude);

            Assert.NotNull(detailModel.BuildingInfo.BranchColor);
            Assert.Equal(detailModel.BuildingInfo.BranchColor.Name, actual.BuildingInfo.BranchColor.Name);
            Assert.Equal(detailModel.BuildingInfo.BranchColor.RGBHexValue, actual.BuildingInfo.BranchColor.RGBHexValue);

            Assert.NotNull(detailModel.BuildingInfo.MainColor);
            Assert.Equal(detailModel.BuildingInfo.MainColor.Name, actual.BuildingInfo.MainColor.Name);
            Assert.Equal(detailModel.BuildingInfo.MainColor.RGBHexValue, actual.BuildingInfo.MainColor.RGBHexValue);
        }

        [Fact(DisplayName = "GetBranchOffJobDetails_Pass|JobServiceTester")]
        public async Task GetBranchOffJobDetails_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            BranchOffJobDetailModel detailModel = new BranchOffJobDetailModel
            {
                BuildingInfo = new BuildingInfoForBranchOffModel
                {
                    BranchColor = new ProjectAdapterColor
                    {
                        Name = "Orange",
                        RGBHexValue = "0x44444",
                    },
                    MainColor = new ProjectAdapterColor
                    {
                        Name = "Blau",
                        RGBHexValue = "0x487987",
                    },
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = rand.Next(40, 50) + rand.NextDouble(),
                        Longitude = rand.Next(40, 50) + rand.NextDouble(),
                    },
                    Id = rand.Next(),
                    StreetName = $"Straße Nr {rand.Next()}",
                },
                Id = jobId,
                State = (JobStates)rand.Next(1, 4),
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.CivilWorker);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            BranchOffJobDetailModel actual = await service.GetBranchOffJobDetails(jobId, requesterId);

            CheckModel(jobId, detailModel, actual);
        }

        [Fact(DisplayName = "GetBranchOffJobDetails_WithFinishedInfo_Pass|JobServiceTester")]
        public async Task GetBranchOffJobDetails_WithFinishedInfo_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            BranchOffJobDetailModel detailModel = new BranchOffJobDetailModel
            {
                BuildingInfo = new BuildingInfoForBranchOffModel
                {
                    BranchColor = new ProjectAdapterColor
                    {
                        Name = "Orange",
                        RGBHexValue = "0x44444",
                    },
                    MainColor = new ProjectAdapterColor
                    {
                        Name = "Blau",
                        RGBHexValue = "0x487987",
                    },
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = rand.Next(40, 50) + rand.NextDouble(),
                        Longitude = rand.Next(40, 50) + rand.NextDouble(),
                    },
                    Id = rand.Next(),
                    StreetName = $"Straße Nr {rand.Next()}",
                },
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishBranchOffDetailModel finishDetails = new FinishBranchOffDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.ProjectManager);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetBranchOffFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);


            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            BranchOffJobDetailModel actual = await service.GetBranchOffJobDetails(jobId, requesterId);

            CheckModel(jobId, detailModel, actual);

            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetBranchOffJobDetails_WithFinishedInfo_ByManagement_Pass|JobServiceTester")]
        public async Task GetBranchOffJobDetails_WithFinishedInfo_ByManagement_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            BranchOffJobDetailModel detailModel = new BranchOffJobDetailModel
            {
                BuildingInfo = new BuildingInfoForBranchOffModel
                {
                    BranchColor = new ProjectAdapterColor
                    {
                        Name = "Orange",
                        RGBHexValue = "0x44444",
                    },
                    MainColor = new ProjectAdapterColor
                    {
                        Name = "Blau",
                        RGBHexValue = "0x487987",
                    },
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = rand.Next(40, 50) + rand.NextDouble(),
                        Longitude = rand.Next(40, 50) + rand.NextDouble(),
                    },
                    Id = rand.Next(),
                    StreetName = $"Straße Nr {rand.Next()}",
                },
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishBranchOffDetailModel finishDetails = new FinishBranchOffDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetBranchOffFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);


            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            BranchOffJobDetailModel actual = await service.GetBranchOffJobDetails(jobId, requesterId);

            CheckModel(jobId, detailModel, actual);

            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetBranchOffJobDetails_Fail_JobNotFound|JobServiceTester")]
        public async Task GetBranchOffJobDetails_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            BranchOffJobDetailModel detailModel = new BranchOffJobDetailModel();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBranchOffJobExist(jobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetBranchOffJobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetBranchOffJobDetails(jobId,String.Empty));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
