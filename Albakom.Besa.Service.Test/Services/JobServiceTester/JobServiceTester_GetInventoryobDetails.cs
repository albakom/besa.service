﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetInventoryobDetails
    {
        [Fact(DisplayName = "GetInventoryobDetails_Pass|JobServiceTester")]
        public async Task GetInventoryobDetails_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            InventoryJobDetailModel detailModel = new InventoryJobDetailModel
            {
                Id = jobId,
            };


            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfInventoryJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetInventoryobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            InventoryJobDetailModel actual = await service.GetInventoryobDetails(jobId);
            Assert.NotNull(actual);
        }

   
        [Fact(DisplayName = "GetInventoryobDetails_Fail_JobNotFound|JobServiceTester")]
        public async Task GetInventoryobDetails_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfInventoryJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetInventoryobDetails(jobId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
