﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetJobCollectionOverview
    {
        private JobCollectionFilterModel GenerateFilterModel(Random random)
        {
            String query = $"Testabfrage Nr. {random.Next()}";
            Int32 queryAmount = random.Next(1, 50);
            Int32 start = random.Next(1, 30);
            SortDirections direction = SortDirections.Ascending;
            JobCollectionSortProperties property = JobCollectionSortProperties.CompanyName;

            JobCollectionFilterModel filterModel = new JobCollectionFilterModel
            {
                Amount = queryAmount,
                Query = query,
                Start = start,
                SortDirection = direction,
                SortProperty = property,
            };

            return filterModel;
        }

        [Fact(DisplayName = "GetJobCollectionOverview_Pass|JobServiceTester")]
        public async Task GetJobCollectionOverview_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<JobCollectionOverviewModel> items = new List<JobCollectionOverviewModel>();

            JobCollectionFilterModel filterModel = GenerateFilterModel(random);

            storage.Setup(ctx => ctx.GetJobCollectionOverview(It.Is<JobCollectionFilterModel>(
                x => x.Amount == filterModel.Amount &&
                x.Query == filterModel.Query &&
                x.SortDirection == filterModel.SortDirection &&
                x.SortProperty == filterModel.SortProperty))).ReturnsAsync(items);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<JobCollectionOverviewModel> actual = await service.GetJobCollectionOverview(filterModel);

            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetJobCollectionOverview_Pass_EmptyQuery|JobServiceTester")]
        public async Task GetJobCollectionOverview_Pass_EmptyQuery()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<JobCollectionOverviewModel> items = new List<JobCollectionOverviewModel>();

            JobCollectionFilterModel filterModel = GenerateFilterModel(random);
            filterModel.Query = String.Empty;

            storage.Setup(ctx => ctx.GetJobCollectionOverview(It.Is<JobCollectionFilterModel>(
                x => x.Amount == filterModel.Amount &&
                x.Query == filterModel.Query &&
                x.SortDirection == filterModel.SortDirection &&
                x.SortProperty == filterModel.SortProperty))).ReturnsAsync(items);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<JobCollectionOverviewModel> actual = await service.GetJobCollectionOverview(filterModel);

            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetJobCollectionOverview_Fail_NoModel|JobServiceTester")]
        public async Task GetJobCollectionOverview_Fail_NoModel()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetJobCollectionOverview(null));
            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "GetJobCollectionOverview_Fail_InvalidStart|JobServiceTester")]
        public async Task GetJobCollectionOverview_Fail_InvalidStart()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<JobCollectionFilterModel> invalidFilters = new List<JobCollectionFilterModel>();
            {
                JobCollectionFilterModel filterModel = GenerateFilterModel(random);
                filterModel.Start = -random.Next(3,10);
                invalidFilters.Add(filterModel);
            };


            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            foreach (JobCollectionFilterModel item in invalidFilters)
            {
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetJobCollectionOverview(item));
                Assert.Equal(JobServicExceptionReasons.InvalidFilterModel, exception.Reason);
            }
        }


        [Fact(DisplayName = "GetJobCollectionOverview_Fail_InvalidAmount|JobServiceTester")]
        public async Task GetJobCollectionOverview_Fail_InvalidAmount()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<JobCollectionFilterModel> invalidFilters = new List<JobCollectionFilterModel>();
            {
                JobCollectionFilterModel filterModel = GenerateFilterModel(random);
                filterModel.Amount = 0;
                invalidFilters.Add(filterModel);
            }
            {
                JobCollectionFilterModel filterModel = GenerateFilterModel(random);
                filterModel.Amount = -random.Next(3, 10);
                invalidFilters.Add(filterModel);
            };

            {
                JobCollectionFilterModel filterModel = GenerateFilterModel(random);
                filterModel.Amount = random.Next(50, 100);
                invalidFilters.Add(filterModel);
            };

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            foreach (JobCollectionFilterModel item in invalidFilters)
            {
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetJobCollectionOverview(item));
                Assert.Equal(JobServicExceptionReasons.InvalidFilterModel, exception.Reason);
            }
        }

    }
}
