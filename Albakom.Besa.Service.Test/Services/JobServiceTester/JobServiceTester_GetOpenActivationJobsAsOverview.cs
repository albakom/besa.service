﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenActivationJobsAsOverview
    {
        [Fact(DisplayName = "GetOpenActivationJobsAsOverview|JobServiceTester")]
        public async Task GetOpenActivationJobsAsOverview()
        {
            Random rand = new Random();


            Int32 amount = rand.Next(10, 30);

            List<ActivationJobOverviewModel> expectedResult = new List<ActivationJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ActivationJobOverviewModel model = new ActivationJobOverviewModel
                {
                    JobId = rand.Next(),
                    Building = new SimpleBuildingOverviewModel
                    {
                        Id = rand.Next(),
                        CommercialUnits = rand.Next(3,10),
                        HouseholdUnits = rand.Next(3, 10),
                    },
                    Customer = new PersonInfo
                    {
                        Address = new AddressModel
                        {
                            City = $"Teststadt Nr. {rand.Next()}",
                            Street = $"Testtr Nr. {rand.Next()}",
                            StreetNumber = $"Nr. {rand.Next()}",
                            PostalCode = rand.Next(0,100000).ToString(),
                        },
                         Id = rand.Next(),
                         CompanyName = $"Firma Nr. {rand.Next()}",
                         EmailAddress = $"test@{rand.Next()}.de",
                         Lastname = $"Nachame {rand.Next()}",
                         Surname = $"Vorname {rand.Next()}",
                         Phone = $"Telefon Nr {rand.Next()}"
                    },
                    PoPName = $"Pop Nr. {rand.Next()}",
                };

                expectedResult.Add(model);
            }


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.GetOpenActivationJobsAsOverview()).ReturnsAsync(expectedResult);


            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<ActivationJobOverviewModel> actual = await service.GetOpenActivationJobsAsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (ActivationJobOverviewModel actualItem in actual)
            {
                ActivationJobOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.JobId == actualItem.JobId);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Building);
                Assert.Equal(expectedItem.Building.Id, actualItem.Building.Id);
                Assert.Equal(expectedItem.Building.StreetName, actualItem.Building.StreetName);
                Assert.Equal(expectedItem.Building.HouseholdUnits, actualItem.Building.HouseholdUnits);
                Assert.Equal(expectedItem.Building.CommercialUnits, actualItem.Building.CommercialUnits);

                Assert.NotNull(actualItem.Customer);
                Assert.Equal(expectedItem.Customer.Id, actualItem.Customer.Id);
                Assert.Equal(expectedItem.Customer.Lastname, actualItem.Customer.Lastname);
                Assert.Equal(expectedItem.Customer.Surname, actualItem.Customer.Surname);
                Assert.Equal(expectedItem.Customer.CompanyName, actualItem.Customer.CompanyName);
                Assert.Equal(expectedItem.Customer.EmailAddress, actualItem.Customer.EmailAddress);
                Assert.Equal(expectedItem.Customer.Phone, actualItem.Customer.Phone);

                Assert.NotNull(actualItem.Customer.Address);
                Assert.Equal(expectedItem.Customer.Address.City, actualItem.Customer.Address.City);
                Assert.Equal(expectedItem.Customer.Address.PostalCode, actualItem.Customer.Address.PostalCode);
                Assert.Equal(expectedItem.Customer.Address.StreetNumber, actualItem.Customer.Address.StreetNumber);
                Assert.Equal(expectedItem.Customer.Address.Street, actualItem.Customer.Address.Street);
            }
        }
    }
}
