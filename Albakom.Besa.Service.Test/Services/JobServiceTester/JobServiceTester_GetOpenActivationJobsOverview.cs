﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenActivationJobsOverview
    {
        [Fact(DisplayName = "GetOpenActivationJobsOverview|JobServiceTester")]
        public async Task GetOpenActivationJobsOverview()
        {
            Random rand = new Random();


            Int32 amount = rand.Next(10, 30);

            List<SimpleActivationJobOverviewModel> expectedResult = new List<SimpleActivationJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                SimpleActivationJobOverviewModel model = new SimpleActivationJobOverviewModel
                {
                    JobId = rand.Next(),
                    Flat = new SimpleFlatOverviewModel
                    {
                        Id = rand.Next(),
                        BuildingId = rand.Next(),
                        Description = $"Tolle Beschreibung {rand.Next()}",
                        Floor = $"Geschoss {rand.Next()}",
                        Number = $"Nummer {rand.Next()}",
                    },
                    Building = new SimpleBuildingOverviewModel
                    {
                        Id = rand.Next(),
                        CommercialUnits = rand.Next(3,10),
                        HouseholdUnits = rand.Next(3, 10),
                    },

                };

                expectedResult.Add(model);
            }


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.GetOpenActivationJobsOverview()).ReturnsAsync(expectedResult);


            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<SimpleActivationJobOverviewModel> actual = await service.GetOpenActivationJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimpleActivationJobOverviewModel actualItem in actual)
            {
                SimpleActivationJobOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.JobId == actualItem.JobId);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Building);
                Assert.Equal(expectedItem.Building.Id, actualItem.Building.Id);
                Assert.Equal(expectedItem.Building.StreetName, actualItem.Building.StreetName);
                Assert.Equal(expectedItem.Building.HouseholdUnits, actualItem.Building.HouseholdUnits);
                Assert.Equal(expectedItem.Building.CommercialUnits, actualItem.Building.CommercialUnits);

                Assert.NotNull(actualItem.Flat);
                Assert.Equal(expectedItem.Flat.Id, actualItem.Flat.Id);
                Assert.Equal(expectedItem.Flat.BuildingId, actualItem.Flat.BuildingId);
                Assert.Equal(expectedItem.Flat.Description, actualItem.Flat.Description);
                Assert.Equal(expectedItem.Flat.Floor, actualItem.Flat.Floor);
                Assert.Equal(expectedItem.Flat.Number, actualItem.Flat.Number);
            }
        }
    }
}
