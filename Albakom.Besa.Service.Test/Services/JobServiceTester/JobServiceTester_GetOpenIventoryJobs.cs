﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenIventoryJobs
    {
        [Fact(DisplayName = "GetOpenIventoryJobs|JobServiceTester")]
        public async Task GetOpenIventoryJobs()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            Int32 amount = rand.Next(3, 10);

            List<InventoryJobOverviewModel> result = new List<InventoryJobOverviewModel>();
            for (int i = 0; i < amount; i++)
            {
                InventoryJobOverviewModel overviewModel = new InventoryJobOverviewModel
                {
                    Id = rand.Next(),
                };

                result.Add(overviewModel);
            }


            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetOpenIventoryJobs()).ReturnsAsync(result);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<InventoryJobOverviewModel> actual = await service.GetOpenIventoryJobs();
            Assert.NotNull(actual);

            Assert.Equal(result.Count, actual.Count());
        }
    }
}
