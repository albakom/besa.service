﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenPrepareBranchableForSpliceJobsOverview
    {
        [Fact(DisplayName = "GetOpenPrepareBranchableForSpliceJobsOverview|JobServiceTester")]
        public async Task GetOpenPrepareBranchableForSpliceJobsOverview()
        {
            Random rand = new Random();


            Int32 amount = rand.Next(10, 30);

            List<SimplePrepareBranchableForSpliceJobOverviewModel> expectedResult = new List<SimplePrepareBranchableForSpliceJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                SimplePrepareBranchableForSpliceJobOverviewModel model = new SimplePrepareBranchableForSpliceJobOverviewModel
                {
                    JobId = rand.Next(),
                    Branchable = new SimpleBranchableWithGPSModel
                    {
                        Coordinate = new GPSCoordinate
                        {
                            Latitude = rand.NextDouble() + rand.Next(30, 50),
                            Longitude = rand.NextDouble() + rand.Next(30, 50),
                        },
                        Id = rand.Next(),
                        Name = $"Testname {rand.Next()}",
                        Type = BranchableTypes.Cabinet,
                    }
                };

                expectedResult.Add(model);
            }


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.GetOpenPrepareBranchableForSpliceJobsOverview()).ReturnsAsync(expectedResult);


            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable< SimplePrepareBranchableForSpliceJobOverviewModel> actual = await service.GetOpenPrepareBranchableForSpliceJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimplePrepareBranchableForSpliceJobOverviewModel actualItem in actual)
            {
                SimplePrepareBranchableForSpliceJobOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.JobId == actualItem.JobId);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);

                Assert.NotNull(actualItem.Branchable);
                Assert.Equal(expectedItem.Branchable.Id, actualItem.Branchable.Id);
                Assert.Equal(expectedItem.Branchable.Name, actualItem.Branchable.Name);
                Assert.Equal(expectedItem.Branchable.Type, actualItem.Branchable.Type);

                Assert.NotNull(actualItem.Branchable.Coordinate);
                Assert.Equal(expectedItem.Branchable.Coordinate.Latitude, actualItem.Branchable.Coordinate.Latitude);
                Assert.Equal(expectedItem.Branchable.Coordinate.Longitude, actualItem.Branchable.Coordinate.Longitude);
            }
        }
    }
}
