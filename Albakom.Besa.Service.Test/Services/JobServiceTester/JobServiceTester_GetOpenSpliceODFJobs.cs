﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenSpliceODFJobs
    {
        [Fact(DisplayName = "GetOpenSpliceODFJobs|JobServiceTester")]
        public async Task GetOpenSpliceODFJobs()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(10, 30);

            List<SpliceODFJobOverviewModel> expectedResult = new List<SpliceODFJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                SpliceODFJobOverviewModel model = new SpliceODFJobOverviewModel
                {
                    JobId = rand.Next(),
                    BoundedCompanyName = rand.NextDouble() > 0.5 ? $"Testfirma {rand.Next()}" : null,
                    Location = new GPSCoordinate {  Latitude = rand.Next(20,30)  + rand.NextDouble(), Longitude = rand.Next(20, 30) + rand.NextDouble() },
                    MainCable = new SpliceCableInfoModel {  Name = $"Testname {rand.Next()}", FiberAmount = rand.Next(12,288)},
                    PopName = $"Standort Nr {rand.Next()}",
                    State = JobStates.Acknowledged, 
                };

                expectedResult.Add(model);
            }

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.GetOpenSpliceODFJobs()).ReturnsAsync(expectedResult);


            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<SpliceODFJobOverviewModel> actual = await service.GetOpenSpliceODFJobs();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SpliceODFJobOverviewModel actualItem in actual)
            {
                SpliceODFJobOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.JobId == actualItem.JobId);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);
                Assert.Equal(expectedItem.State, actualItem.State);
                Assert.Equal(expectedItem.PopName, actualItem.PopName);
                Assert.Equal(expectedItem.BoundedCompanyName, actualItem.BoundedCompanyName);

                Assert.NotNull(actualItem.Location);
                Assert.Equal(expectedItem.Location.Latitude, expectedItem.Location.Latitude);
                Assert.Equal(expectedItem.Location.Longitude, expectedItem.Location.Longitude);

                Assert.NotNull(actualItem.MainCable);
                Assert.Equal(expectedItem.MainCable.FiberAmount, expectedItem.MainCable.FiberAmount);
                Assert.Equal(expectedItem.MainCable.Name, expectedItem.MainCable.Name);
            }
        }
    }
}
