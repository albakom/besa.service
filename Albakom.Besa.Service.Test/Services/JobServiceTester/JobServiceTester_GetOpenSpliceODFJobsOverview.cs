﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetOpenSpliceODFJobsOverview
    {
        [Fact(DisplayName = "GetOpenSpliceODFJobsOverview|JobServiceTester")]
        public async Task GetOpenSpliceODFJobsOverview()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(10, 30);

            List<SimpleSpliceODFJobOverviewModel> expectedResult = new List<SimpleSpliceODFJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                SimpleSpliceODFJobOverviewModel model = new SimpleSpliceODFJobOverviewModel
                {
                    JobId = rand.Next(),
                    CableName = $"Kabel Nr {rand.Next()}",
                };

                expectedResult.Add(model);
            }


            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.GetOpenSpliceODFJobsOverview()).ReturnsAsync(expectedResult);


            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<SimpleSpliceODFJobOverviewModel> actual = await service.GetOpenSpliceODFJobsOverview();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (SimpleSpliceODFJobOverviewModel actualItem in actual)
            {
                SimpleSpliceODFJobOverviewModel expectedItem = expectedResult.FirstOrDefault(x => x.JobId == actualItem.JobId);
                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.JobId, actualItem.JobId);
                Assert.Equal(expectedItem.CableName, expectedItem.CableName);
            }
        }
    }
}
