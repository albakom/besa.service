﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetPrepareBranchbaleForSpliceJobDetails
    {
        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_Pass|JobServiceTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            PrepareBranchableForSpliceJobDetailModel detailModel = new PrepareBranchableForSpliceJobDetailModel
            {
                Id = jobId,
                State = (JobStates)rand.Next(1, 4),
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfPrepareBranchableJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.AirInjector);
            mockStorage.Setup(ctx => ctx.GetPrepareBranchbaleForSpliceJobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            PrepareBranchableForSpliceJobDetailModel actual = await service.GetPrepareBranchbaleForSpliceJobDetails(jobId, requesterId);
            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_WithFinishedInfo_Pass|JobServiceTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_WithFinishedInfo_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            PrepareBranchableForSpliceJobDetailModel detailModel = new PrepareBranchableForSpliceJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishPrepareBranchableForSpliceJobDetailModel finishDetails = new FinishPrepareBranchableForSpliceJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfPrepareBranchableJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.ProjectManager);
            mockStorage.Setup(ctx => ctx.GetPrepareBranchbaleForSpliceJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetPrepareBranchbaleForSpliceFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            PrepareBranchableForSpliceJobDetailModel actual = await service.GetPrepareBranchbaleForSpliceJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_WithFinishedInfo_ByManagement_Pass|JobServiceTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_WithFinishedInfo_ByManagement_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            PrepareBranchableForSpliceJobDetailModel detailModel = new PrepareBranchableForSpliceJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishPrepareBranchableForSpliceJobDetailModel finishDetails = new FinishPrepareBranchableForSpliceJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfPrepareBranchableJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetPrepareBranchbaleForSpliceJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetPrepareBranchbaleForSpliceFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            PrepareBranchableForSpliceJobDetailModel actual = await service.GetPrepareBranchbaleForSpliceJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetPrepareBranchbaleForSpliceJobDetails_Fail_JobNotFound|JobServiceTester")]
        public async Task GetPrepareBranchbaleForSpliceJobDetails_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            HouseConnenctionJobDetailModel detailModel = new HouseConnenctionJobDetailModel();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfPrepareBranchableJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetPrepareBranchbaleForSpliceJobDetails(jobId,String.Empty));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
