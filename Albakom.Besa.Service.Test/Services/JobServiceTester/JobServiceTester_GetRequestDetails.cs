﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetRequestDetails
    {
        [Fact(DisplayName = "GetRequestDetails_OnlyHouseConnection_Pass|JobServiceTester")]
        public async Task GetRequestDetails_OnlyHouseConnection_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 mostRecentJobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = new RequestDetailModel();
            request.OnlyHouseConnection = true;
            request.Building = new SimpleBuildingOverviewModel { Id = buildingId };

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetCustomerRequestDetails(requestId)).ReturnsAsync(request);
            mockStorage.Setup(ctx => ctx.GetMostRecentJobByBuildingId(buildingId)).ReturnsAsync(new SimpleJobOverview { Id = mostRecentJobId } );

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            RequestDetailModel actual = await service.GetRequestDetails(requestId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.MostRecentJob);

            Assert.Equal(mostRecentJobId,actual.MostRecentJob.Id);
            ///TODO Vergleich auf alle anderen Eigenschaften hinzufügen
        }


        [Fact(DisplayName = "GetRequestDetails_Pass|JobServiceTester")]
        public async Task GetRequestDetails_Pass()
        {
            Random rand = new Random();
            Boolean expectedResult = rand.NextDouble() > 0.5;
            Int32 requestId = rand.Next();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 mostRecentJobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            RequestDetailModel request = new RequestDetailModel();
            request.OnlyHouseConnection = false;
            request.Building = new SimpleBuildingOverviewModel { Id = buildingId };
            request.Flat = new SimpleFlatOverviewModel { Id = flatId };

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetCustomerRequestDetails(requestId)).ReturnsAsync(request);
            mockStorage.Setup(ctx => ctx.GetMostRecentJobByFlatId(flatId,buildingId)).ReturnsAsync(new SimpleJobOverview { Id = mostRecentJobId });

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            RequestDetailModel actual = await service.GetRequestDetails(requestId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.MostRecentJob);

            Assert.Equal(mostRecentJobId, actual.MostRecentJob.Id);
            ///TODO Vergleich auf alle anderen Eigenschaften hinzufügen
        }

        [Fact]
        public async Task GetRequestDetails_Fail_RequestNotFound()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetRequestDetails(requestId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact]
        public async Task GetRequestDetails_Fail_RequestIsClosed()
        {
            Random rand = new Random();
            Int32 requestId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManger = Mock.Of<IFileManager>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerRequestIsClosed(requestId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, fileManger,  Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetRequestDetails(requestId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
