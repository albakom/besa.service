﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetSpliceBranchableJobDetails
    {
        [Fact(DisplayName = "GetSpliceBranchableJobDetails_Pass|JobServiceTester")]
        public async Task GetSpliceBranchableJobDetails_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            SpliceBranchableJobDetailModel detailModel = new SpliceBranchableJobDetailModel
            {
                Id = jobId,
                State = (JobStates)rand.Next(1, 4),
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceBranchableJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.AirInjector);
            mockStorage.Setup(ctx => ctx.GetSpliceBranchableJobDetails(jobId)).ReturnsAsync(detailModel);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            SpliceBranchableJobDetailModel actual = await service.GetSpliceBranchableJobDetails(jobId, requesterId);
            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetSpliceBranchableJobDetails_WithFinishedInfo_Pass|JobServiceTester")]
        public async Task GetSpliceBranchableJobDetails_WithFinishedInfo_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            SpliceBranchableJobDetailModel detailModel = new SpliceBranchableJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishSpliceBranchableJobDetailModel finishDetails = new FinishSpliceBranchableJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceBranchableJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(requesterId)).ReturnsAsync(BesaRoles.ProjectManager);
            mockStorage.Setup(ctx => ctx.GetSpliceBranchableJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetSpliceBranchableFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            SpliceBranchableJobDetailModel actual = await service.GetSpliceBranchableJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetSpliceBranchableJobDetails_WithFinishedInfo_ByManagement_Pass|JobServiceTester")]
        public async Task GetSpliceBranchableJobDetails_WithFinishedInfo_ByManagement_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            SpliceBranchableJobDetailModel detailModel = new SpliceBranchableJobDetailModel
            {
                Id = jobId,
                State = JobStates.Finished,
            };

            FinishSpliceBranchableJobDetailModel finishDetails = new FinishSpliceBranchableJobDetailModel
            {
                AcceptedAt = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                AcceptedBy = new UserOverviewModel(),
                Comment = "",
            };

            String requesterId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceBranchableJobExist(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserIsManagement(requesterId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetSpliceBranchableJobDetails(jobId)).ReturnsAsync(detailModel);
            mockStorage.Setup(ctx => ctx.GetSpliceBranchableFinishedJobDetails(jobId)).ReturnsAsync(finishDetails);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            SpliceBranchableJobDetailModel actual = await service.GetSpliceBranchableJobDetails(jobId, requesterId);

            Assert.NotNull(actual);
            Assert.NotNull(actual.FinishInfo);
        }

        [Fact(DisplayName = "GetSpliceBranchableJobDetails_Fail_JobNotFound|JobServiceTester")]
        public async Task GetSpliceBranchableJobDetails_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();

            HouseConnenctionJobDetailModel detailModel = new HouseConnenctionJobDetailModel();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfSpliceBranchableJobExist(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetSpliceBranchableJobDetails(jobId,String.Empty));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
