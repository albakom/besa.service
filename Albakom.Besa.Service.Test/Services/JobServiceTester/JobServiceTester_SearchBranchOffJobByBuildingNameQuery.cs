﻿using System;
using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Albakom.Besa.Service.Contracts.Models.Exceptions;

namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_SearchBranchOffJobByBuildingNameQuery
    {
        [Fact(DisplayName = "SearchBranchOffJobByBuildingNameQuery_Pass|JobServiceTester")]
        public async Task SearchBranchOffJobByBuildingNameQuery_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<SimpleJobOverview> items = new List<SimpleJobOverview>();
            Int32 amount = random.Next(5, 10);
            for (int i = 0; i < amount; i++)
            {
                SimpleJobOverview item = new SimpleJobOverview
                {
                    Id = i + 1,
                    JobType = JobTypes.BranchOff,
                    Name = $"Testaufgabe Nr. {random.Next()}",
                    State = JobStates.Open,
                };

                items.Add(item);
            }

            String query = $"Testabfrage Nr. {random.Next()}";
            Int32 queryAmount = random.Next(1, 50);
            JobStates? state = null;
            if (random.NextDouble() > 0.5)
            {
                state = JobStates.Open;
            }

            storage.Setup(ctx => ctx.SearchBranchOffJobByBuildingNameQuery(query, state, queryAmount)).ReturnsAsync(items);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<SimpleJobOverview> actual = await service.SearchBranchOffJobByBuildingNameQuery(query, state, queryAmount);

            Assert.NotNull(actual);

            Assert.Equal(amount, actual.Count());

            Int32 index = 0;
            foreach (SimpleJobOverview acutalItem in actual)
            {
                SimpleJobOverview expectedItem = items[index++];

                Assert.Equal(expectedItem.Id, acutalItem.Id);
                Assert.Equal(expectedItem.Name, acutalItem.Name);
                Assert.Equal(expectedItem.JobType, acutalItem.JobType);
                Assert.Equal(expectedItem.State, acutalItem.State);
            }
        }

        [Fact(DisplayName = "SearchBranchOffJobByBuildingNameQuery_EmtpyQuery_Pass|JobServiceTester")]
        public async Task SearchBranchOffJobByBuildingNameQuery_EmtpyQuery_Pass()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            Int32 queryAmount = random.Next(1, 50);
            String query = String.Empty;
            JobStates? state = null;

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<SimpleJobOverview> actual = await service.SearchBranchOffJobByBuildingNameQuery(query, state, queryAmount);

            Assert.NotNull(actual);
            Assert.Empty(actual);
        }

        [Fact(DisplayName = "SearchBranchOffJobByBuildingNameQuery_EmtpyQuery_Pass|JobServiceTester")]
        public async Task SearchBranchOffJobByBuildingNameQuery_Fail_InvalidAmount()
        {
            var logger = Mock.Of<ILogger<JobService>>();
            var fileManager = Mock.Of<IFileManager>();
            var storage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            Random random = new Random();

            List<Int32> invalidAmounts = new List<int>();
            invalidAmounts.Add(0);
            invalidAmounts.Add(51);
            invalidAmounts.Add(-1);

            IJobService service = new JobService(storage.Object, fileManager, Mock.Of<IProtocolService>(), logger);

            foreach (Int32 invalidAmount in invalidAmounts)
            {
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.SearchBranchOffJobByBuildingNameQuery("Meine Abfrage", null, invalidAmount));
                Assert.Equal(JobServicExceptionReasons.InvalidAmount, exception.Reason);
            }
        }
    }
}
