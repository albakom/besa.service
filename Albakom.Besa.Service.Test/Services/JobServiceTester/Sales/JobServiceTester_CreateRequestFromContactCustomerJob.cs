﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_CreateRequestFromContactCustomerJob
    {
        private static RequestCustomerConnectionByContactModel GenerateInputModel(Random rand, int jobId)
        {
            return new RequestCustomerConnectionByContactModel
            {

                SubscriberEndpointLength = rand.NextDouble() > 0.5 ? rand.NextDouble() + rand.Next(20, 30) : new Double?(),
                DuctAmount = rand.NextDouble() > 0.5 ? rand.NextDouble() + rand.Next(20, 30) : new Double?(),
                JobId = jobId,
                PowerForActiveEquipment = new ExplainedBooleanModel
                {
                    Description = $"Powerbeschreibung {rand.Next()}",
                    Value = rand.NextDouble() > 0.5
                },
                ActivePointNearSubscriberEndpoint = new ExplainedBooleanModel
                {
                    Description = $"Endpunktbeschreibung {rand.Next()}",
                    Value = rand.NextDouble() > 0.5
                },
                SubscriberEndpointNearConnectionPoint = new ExplainedBooleanModel
                {
                    Description = $"Anschluss nahe {rand.Next()}",
                    Value = rand.NextDouble() > 0.5
                },
                ConnectionAppointment = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                ActivationAsSoonAsPossible = false,
            };
        }

        private static PersonInfo GeneratePesonInfo(Random rand, int contactId)
        {
            return new PersonInfo
            {
                Id = contactId,
                Address = new AddressModel
                {
                    City = $"Musterstadt {rand.Next()}",
                    PostalCode = rand.Next(10000, 99999).ToString(),
                    Street = $"Musterstr {rand.Next()}",
                    StreetNumber = $"{rand.Next(1, 500)}",
                },
                Surname = $"Vorname Nr. {rand.Next()}",
                Lastname = $"Nachname Nr. {rand.Next()}",
            };
        }

        private static ContactAfterFinishedJobModel GenerateJobModel(int jobId, int buildingId, int flatId, int contactId)
        {
            return new ContactAfterFinishedJobModel
            {
                Id = jobId,
                FlatId = flatId,
                BuildingId = buildingId,
                ContactId = contactId
            };
        }

        private static void PrepareMockStorage(int jobId, string userAuthId, int flatId, int contactId, int requestId, string streetName, int procedureId, int finishedJobid, ContactAfterFinishedJobModel jobModel, PersonInfo personInfo, RequestCustomerConnectionByContactModel createModel, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetContactAfterFinishedJobModelById(jobId)).ReturnsAsync(jobModel);
            mockStorage.Setup(ctx => ctx.CreateCustomerConnenctionRequest(It.Is<CustomerConnectRequestCreateModel>(
                x =>
                x.ActivationAsSoonAsPossible == createModel.ActivationAsSoonAsPossible &&
                x.ActivePointNearSubscriberEndpoint.Value == createModel.ActivePointNearSubscriberEndpoint.Value &&
                x.ActivePointNearSubscriberEndpoint.Description == createModel.ActivePointNearSubscriberEndpoint.Description &&
                x.ConnectionAppointment == createModel.ConnectionAppointment &&
                x.CustomerPersonId == jobModel.ContactId &&
                x.DuctAmount == createModel.DuctAmount &&
                x.FlatId == jobModel.FlatId &&
                x.OnlyHouseConnection == false &&
                x.PowerForActiveEquipment.Value == createModel.PowerForActiveEquipment.Value &&
                x.PowerForActiveEquipment.Description == createModel.PowerForActiveEquipment.Description &&
                x.SubscriberEndpointLength == createModel.SubscriberEndpointLength &&
                x.SubscriberEndpointNearConnectionPoint.Value == createModel.SubscriberEndpointNearConnectionPoint.Value &&
                x.SubscriberEndpointNearConnectionPoint.Description == createModel.SubscriberEndpointNearConnectionPoint.Description
                ))).ReturnsAsync(requestId);
            mockStorage.Setup(ctx => ctx.GetContactById(contactId)).ReturnsAsync(personInfo);

            mockStorage.Setup(ctx => ctx.GetBuildingStreetNameByFlatId(flatId)).ReturnsAsync(streetName);
            mockStorage.Setup(ctx => ctx.CreateCustomerConnectionProcedure(It.Is<CustomerConnectionProcedureCreateModel>(
                x =>
                x.Appointment == createModel.ConnectionAppointment &&
                x.AsSoonAsPossible == createModel.ActivationAsSoonAsPossible &&
                x.ContractInStock == false &&
                x.CustomerContactId == contactId &&
                x.DeclarationOfAggrementInStock == false &&
                x.ExpectedEnd == ProcedureStates.ActivatitionJobAcknlowedged &&
                x.FlatId == jobModel.FlatId &&
                x.Name.ToLower().Contains(streetName.ToLower()) == true && x.Name.ToLower().Contains(personInfo.Lastname.ToLower()) == true &&
                x.FirstElement != null &&
                x.FirstElement.RelatedRequestId == requestId &&
                x.FirstElement.State == ProcedureStates.RequestCreated
                ))).ReturnsAsync(procedureId);

            mockStorage.Setup(ctx => ctx.FinishContactAfterFinishedJob(It.Is<ContactAfterFinishedFinishModel>(
                x =>
                x.JobId == jobId &&
                x.RequestId == requestId &&
                x.UserAuthId == userAuthId &&
                Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
                ))).ReturnsAsync(finishedJobid);

            mockStorage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(
                x =>
                x.JobId == jobId &&
                x.UserAuthId == userAuthId &&
                Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
                ))).ReturnsAsync(true);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_Pass|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);

            Int32 createCounter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == requestId &&
           x.RelatedType == MessageRelatedObjectTypes.CustomerRequest &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
            x.Action == MessageActions.Create &&
            x.Level == ProtocolLevels.Sucesss &&
            x.RelatedObjectId == procedureId &&
            x.RelatedType == MessageRelatedObjectTypes.CustomerConnenctionProcedure &&
            (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
            x.Action == MessageActions.JobFinished &&
            x.Level == ProtocolLevels.Sucesss &&
            x.RelatedObjectId == jobId &&
            x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob &&
            (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => createCounter++);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
            x.Action == MessageActions.JobAcknowledged &&
            x.Level == ProtocolLevels.Sucesss &&
            x.RelatedObjectId == jobId &&
            x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob &&
            (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => createCounter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Int32 actualRequestId = await service.CreateRequestFromContactCustomerJob(createModel, userAuthId);

            Assert.Equal(requestId, actualRequestId);
            Assert.Equal(4, createCounter);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_withoutFlatId_Pass|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_withoutFlatId_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);
            jobModel.FlatId = null;

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.GetFlatsByBuildingId(buildingId)).ReturnsAsync(new List<SimpleFlatOverviewModel>{ new SimpleFlatOverviewModel
            {
                BuildingId = buildingId,
                Id = flatId,
            }
            }
            );

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            Int32 actualRequestId = await service.CreateRequestFromContactCustomerJob(createModel, userAuthId);

            Assert.Equal(requestId, actualRequestId);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_withoutBuildingId_Pass|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_withoutBuildingId_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);
            jobModel.BuildingId = null;

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.GetBuildingIdByFlatId(flatId)).ReturnsAsync(buildingId);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            Int32 actualRequestId = await service.CreateRequestFromContactCustomerJob(createModel, userAuthId);

            Assert.Equal(requestId, actualRequestId);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_Fail_UserNotFound|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateRequestFromContactCustomerJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_Fail_JobNotFound|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateRequestFromContactCustomerJob(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_Fail_JobAlreadyFinished|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Fail_JobAlreadyFinished()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateRequestFromContactCustomerJob(createModel, userAuthId));


            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_Failed_NoModel|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Failed_NoModel()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateRequestFromContactCustomerJob(null, String.Empty));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "CreateRequestFromContactCustomerJob_InvalidDate|JobServiceTester")]
        public async Task CreateRequestFromContactCustomerJob_Failed_InvalidDate()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 buildingId = rand.Next();
            Int32 flatId = rand.Next();
            Int32 contactId = rand.Next();
            Int32 requestId = rand.Next();
            String streetName = $"Musterstraße {rand.Next()}";
            Int32 procedureId = rand.Next();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedJobModel jobModel = GenerateJobModel(jobId, buildingId, flatId, contactId);

            PersonInfo personInfo = GeneratePesonInfo(rand, contactId);

            RequestCustomerConnectionByContactModel createModel = GenerateInputModel(rand, jobId);
            createModel.ConnectionAppointment = null;
            createModel.ActivationAsSoonAsPossible = false;

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, flatId, contactId, requestId, streetName, procedureId, finishedJobid, jobModel, personInfo, createModel, mockStorage);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.CreateRequestFromContactCustomerJob(createModel, userAuthId));


            Assert.Equal(JobServicExceptionReasons.InvalidData, exception.Reason);
        }
    }
}
