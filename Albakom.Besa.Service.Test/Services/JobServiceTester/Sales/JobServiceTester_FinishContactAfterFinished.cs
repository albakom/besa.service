﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_FinishContactAfterFinished
    {
        private static ContactAfterFinishedUnsucessfullyModel GenerateInputModel(Random rand, int jobId)
        {
            return new ContactAfterFinishedUnsucessfullyModel
            {
                JobId = jobId,
                Comment = $"Ganz langer und toller Text {rand.Next()}"
            };
        }
        
        private static void PrepareMockStorage(int jobId, string userAuthId, Int32 finishedJobid, ContactAfterFinishedUnsucessfullyModel createModel, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(jobId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(false);
           
            mockStorage.Setup(ctx => ctx.FinishContactAfterFinishedJob(It.Is<ContactAfterFinishedFinishModel>(
                x =>
                x.JobId == jobId &&
                x.UserAuthId == userAuthId &&
                x.ProblemHappend.Value == true &&
                x.RequestId == null &&
                x.ProblemHappend.Description == createModel.Comment &&
                Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
                ))).ReturnsAsync(finishedJobid);

            mockStorage.Setup(ctx => ctx.AcknowledgeJob(It.Is<AcknowledgeJobModel>(
                x =>
                x.JobId == jobId &&
                x.UserAuthId == userAuthId &&
                Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
                ))).ReturnsAsync(true);
        }

        [Fact(DisplayName = "FinishContactAfterFinished_Pass|JobServiceTester")]
        public async Task FinishContactAfterFinished_Pass()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedUnsucessfullyModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, finishedJobid, createModel, mockStorage);

            Int32 protocolConuter = 0;
            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.JobFinished &&
           x.Level == ProtocolLevels.Sucesss &&
           x.RelatedObjectId == jobId &&
           x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob &&
           (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60), userAuthId)).ReturnsAsync(rand.Next()).Callback(() => protocolConuter++);

            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
            x.Action == MessageActions.JobAcknowledged &&
            x.Level == ProtocolLevels.Sucesss &&
            x.RelatedObjectId == jobId &&
            x.RelatedType == MessageRelatedObjectTypes.ContactAfterFinishedJob &&
            (DateTimeOffset.Now - x.Timestamp).TotalSeconds < 60))).ReturnsAsync(rand.Next()).Callback(() => protocolConuter++);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, mockProtocolService.Object, logger);
            Boolean actualResult = await service.FinishContactAfterFinished(createModel, userAuthId);

            Assert.True(actualResult);
            Assert.Equal(2, protocolConuter);
        }

        [Fact(DisplayName = "FinishContactAfterFinished_Fail_UserNotFound|JobServiceTester")]
        public async Task FinishContactAfterFinished_Fail_UserNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedUnsucessfullyModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, finishedJobid, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishContactAfterFinished(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "FinishContactAfterFinished_Fail_JobNotFound|JobServiceTester")]
        public async Task FinishContactAfterFinished_Fail_JobNotFound()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedUnsucessfullyModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, finishedJobid, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(jobId)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishContactAfterFinished(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }

        [Fact(DisplayName = "FinishContactAfterFinished_Fail_JobAlreadyFinished|JobServiceTester")]
        public async Task FinishContactAfterFinished_Fail_JobAlreadyFinished()
        {
            Random rand = new Random();
            Int32 jobId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();
            Int32 finishedJobid = rand.Next();

            ContactAfterFinishedUnsucessfullyModel createModel = GenerateInputModel(rand, jobId);

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            PrepareMockStorage(jobId, userAuthId, finishedJobid, createModel, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfJobIsFinished(jobId)).ReturnsAsync(true);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishContactAfterFinished(createModel, userAuthId));

            Assert.Equal(JobServicExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "FinishContactAfterFinished_Failed_NoModel|JobServiceTester")]
        public async Task FinishContactAfterFinished_Failed_NoModel()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.FinishContactAfterFinished(null,String.Empty));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }
    }
}
