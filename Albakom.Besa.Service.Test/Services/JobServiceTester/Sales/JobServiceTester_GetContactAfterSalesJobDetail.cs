﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetContactAfterSalesJobDetail
    {
        [Fact(DisplayName = "GetContactAfterSalesJobDetail|JobServiceTester")]
        public async Task GetContactAfterSalesJobDetail_Pass()
        {
            Random rand = new Random();

            Int32 id = rand.Next();

            ContactAfterSalesJobDetails result = new ContactAfterSalesJobDetails
            {
                Building = new SimpleBuildingOverviewWithGPSModel
                {
                    CommercialUnits = rand.Next(3, 10),
                    HouseholdUnits = rand.Next(3, 10),
                    Id = rand.Next(),
                    StreetName = $"Tolle Straße Nr. {rand.Next()}",
                    Coordinate = new GPSCoordinate
                    {
                        Latitude = rand.NextDouble() + rand.Next(30, 40),
                        Longitude = rand.NextDouble() + rand.Next(30, 40),
                    },
                },
                Closed = rand.NextDouble() > 0.5,
                Contact = new PersonInfo
                {
                    Lastname = $"Nachanme 3",
                    Id = rand.Next(),
                },
                Id = id,
                CloseComment  = $"Blub {rand.Next()}",
            };

              var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(id)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetContactAfterSalesJobDetail(id)).ReturnsAsync(result);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            ContactAfterSalesJobDetails actualResult = await service.GetContactAfterSalesJobDetail(id);

            Assert.Equal(result, actualResult);
        }

        [Fact(DisplayName = "GetContactAfterSalesJobDetail_Fail_NotFound|JobServiceTester")]
        public async Task GetContactAfterSalesJobDetail_Fail_NotFound()
        {
            Random rand = new Random();

            Int32 id = rand.Next();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            mockStorage.Setup(ctx => ctx.CheckIfCustomerContactJobExists(id)).ReturnsAsync(false);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetContactAfterSalesJobDetail(id));

            Assert.Equal(JobServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
