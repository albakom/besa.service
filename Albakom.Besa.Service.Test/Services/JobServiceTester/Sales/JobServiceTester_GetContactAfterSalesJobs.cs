﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.JobServiceTester
{
    public class JobServiceTester_GetContactAfterSalesJobs
    {
        [Fact(DisplayName = "GetContactAfterSalesJobs_Pass|JobServiceTester")]
        public async Task GetContactAfterSalesJobs_Pass()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(3, 10);
            List<ContactAfterSalesJobOverview> result = new List<ContactAfterSalesJobOverview>();
            for (int i = 0; i < amount; i++)
            {
                result.Add(new ContactAfterSalesJobOverview
                {
                    Building = new SimpleBuildingOverviewModel
                    {
                        CommercialUnits = rand.Next(3, 10),
                        HouseholdUnits = rand.Next(3, 10),
                        Id = rand.Next(),
                        StreetName = $"Tolle Straße Nr. {rand.Next()}",
                    },
                    IsFinished = rand.NextDouble() > 0.5,
                    Contact = new PersonInfo
                    {
                        Lastname = $"Nachanme 3",
                        Id = rand.Next(),
                    },
                    Id = rand.Next(),
                });
            }

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            ContactAfterFinishedFilterModel filterInput = new ContactAfterFinishedFilterModel
            {
                Amount = rand.Next(3, 10),
                OpenState = rand.NextDouble() > 0.5 ? rand.NextDouble() > 0.5 : new bool?(),
                Query = $"Testabfrage Nr {rand.Next()}",
                Start = rand.Next(3, 10),
            };

            mockStorage.Setup(ctx => ctx.GetContactAfterSalesJobs(It.Is<ContactAfterFinishedFilterModel>(x =>
                x.Amount == filterInput.Amount &&
                x.OpenState == filterInput.OpenState &&
                x.Query == filterInput.Query &&
                x.Start == filterInput.Start
                ))).ReturnsAsync(result);

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            IEnumerable<ContactAfterSalesJobOverview> actualResult = await service.GetContactAfterSalesJobs(filterInput);

            Assert.Equal(result, actualResult);
        }

        [Fact(DisplayName = "GetContactAfterSalesJobs_Fail_NoFilter|JobServiceTester")]
        public async Task GetContactAfterSalesJobs_Fail_NoFilter()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<JobService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            var mockFileManager = Mock.Of<IFileManager>();

            IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
            JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetContactAfterSalesJobs(null));

            Assert.Equal(JobServicExceptionReasons.NoModel, exception.Reason);
        }

        [Fact(DisplayName = "GetContactAfterSalesJobs_Fail_InvalidFilter|JobServiceTester")]
        public async Task GetContactAfterSalesJobs_Fail_InvalidFilter()
        {
            Random rand = new Random();

            List<ContactAfterFinishedFilterModel> invalidFilters = new List<ContactAfterFinishedFilterModel>();
            invalidFilters.Add(new ContactAfterFinishedFilterModel
            {
                Amount = -rand.Next(1, 100),
                Start = 0
            });
            invalidFilters.Add(new ContactAfterFinishedFilterModel
            {
                Amount = rand.Next(51, 100),
                Start = 0,
            });
            invalidFilters.Add(new ContactAfterFinishedFilterModel
            {
                Amount = rand.Next(1, 10),
                Start = -rand.Next(3, 10),
            });
            foreach (ContactAfterFinishedFilterModel filter in invalidFilters)
            {
                var logger = Mock.Of<ILogger<JobService>>();
                var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
                var mockFileManager = Mock.Of<IFileManager>();

                IJobService service = new JobService(mockStorage.Object, mockFileManager, Mock.Of<IProtocolService>(), logger);
                JobServicException exception = await Assert.ThrowsAsync<JobServicException>(() => service.GetContactAfterSalesJobs(filter));

                Assert.Equal(JobServicExceptionReasons.InvalidFilterModel, exception.Reason);
            }

        }
    }
}
