﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.MeesageSendEngineTester
{
    public class MeesageSendEngineTester_SendMessage
    {
        [Fact(DisplayName = "SendMessage_Pass|ProtocolServiceTester")]
        public async Task SendMessage_Pass()
        {
            Random rand = new Random();

            Int32 receiverId = rand.Next();
            Int32 mssageId = rand.Next();
            String userAuthId = Guid.NewGuid().ToString();

            MessageCreateModel entryModel = new MessageCreateModel
            {
                Action = MessageActions.Create,
                ProtocolEntryId = rand.Next(),
                ReceiverId = receiverId,
                SenderId = rand.Next(),
                RelatedObjectType = MessageRelatedObjectTypes.Article,
                Timestamp = DateTimeOffset.Now,
                Details = new MessageActionDetailsModel
                {
                    Id = rand.Next(),
                    Name = $"Testnachricht Nr. {rand.Next()}",
                }
            };

            UserOverviewModel senderOverview = new UserOverviewModel
            {
                Id = entryModel.SenderId.Value,
            };

            MessageModel msgModelToNotifier = new MessageModel
            {
                Action = entryModel.Action,
                ActionDetails = entryModel.Details,
                Id = mssageId,
                MarkedAsRead = false,
                RelatedObjectType = entryModel.RelatedObjectType,
                TimeStamp = entryModel.Timestamp,
                SourceUser = senderOverview
            };

            var logger = Mock.Of<ILogger<MessageSendEngine>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CreateMessage(It.Is<MessageRawCreateModel>(x =>
           x.Action == entryModel.Action &&
           x.ProtocolEntryId == entryModel.ProtocolEntryId &&
           x.ReceiverId == entryModel.ReceiverId &&
           x.SenderId == entryModel.SenderId &&
           x.Timestamp == entryModel.Timestamp &&
           x.Title == entryModel.Details.Name &&
           String.IsNullOrEmpty(x.Details) == false
           ))).ReturnsAsync(mssageId);

            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByUserId(receiverId)).ReturnsAsync(userAuthId);
            mockStorage.Setup(ctx => ctx.GetUserOverviewById(entryModel.SenderId.Value)).ReturnsAsync(senderOverview);

            var msgNotifier = new Mock<IMessageNotifier>(MockBehavior.Strict);
            msgNotifier.Setup(engine => engine.NewMessageArrivied(userAuthId,
               It.Is<MessageModel>(x => x.Action == msgModelToNotifier.Action && x.Id == msgModelToNotifier.Id && x.MarkedAsRead == msgModelToNotifier.MarkedAsRead &&
               x.RelatedObjectType == msgModelToNotifier.RelatedObjectType && x.TimeStamp == msgModelToNotifier.TimeStamp && x.SourceUser != null && x.SourceUser.Id == senderOverview.Id &&
               x.ActionDetails == entryModel.Details
               ))).Returns(Task.FromResult(new Object()));

            IMessageSendEngine service = new MessageSendEngine(mockStorage.Object,
                msgNotifier.Object,
                logger);

            await service.SendMessage(receiverId, entryModel);
        }
    }
}
