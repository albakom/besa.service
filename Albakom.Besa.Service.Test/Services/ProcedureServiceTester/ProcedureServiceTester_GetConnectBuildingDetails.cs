﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProcedureServiceTester
{
    public class ProcedureServiceTester_GetConnectBuildingDetails
    {
        [Fact(DisplayName = "GetConnectBuildingDetails|ProcedureServiceTester")]
        public async Task GetConnectBuildingDetails_Pass()
        {
            Random rand = new Random();
            Int32 procedureId = rand.Next();

            BuildingConnectionProcedureDetailModel model = new BuildingConnectionProcedureDetailModel
            {
                Id = procedureId,
                Name = $"Vorgang-Name {rand.Next()}",
            };

            var logger = Mock.Of<ILogger<ProcedureService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExists(procedureId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetConnectBuildingDetails(procedureId)).ReturnsAsync(model);

            IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>());
            BuildingConnectionProcedureDetailModel actual = await service.GetConnectBuildingDetails(procedureId);

            Assert.NotNull(actual);

            Assert.Equal(procedureId, actual.Id);
            Assert.Equal(model.Name, actual.Name);
        }

        [Fact(DisplayName = "GetConnectBuildingDetails_Fail_NotFound|ProcedureServiceTester")]
        public async Task GetConnectBuildingDetails_Fail_NotFound()
        {
            Random rand = new Random();
            Int32 procedureId = rand.Next();

            var logger = Mock.Of<ILogger<ProcedureService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExists(procedureId)).ReturnsAsync(false);

            IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>()); ;
            ProcedureServiceException exception = await Assert.ThrowsAsync<ProcedureServiceException>(() => service.GetConnectBuildingDetails(procedureId));
            Assert.Equal(ProcedureServicExceptionReasons.NotFound, exception.Reason);
        }
    }
}
