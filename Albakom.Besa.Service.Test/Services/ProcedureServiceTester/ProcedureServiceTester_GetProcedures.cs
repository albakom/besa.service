﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProcedureServiceTester
{
    public class ProcedureServiceTester_GetProcedures
    {
        [Fact(DisplayName = "GetProcedures|ProcedureServiceTester")]
        public async Task GetProcedures_Pass()
        {
            Random rand = new Random();
            Int32 procedureId = rand.Next();

            ProcedureFilterModel filterModel = new ProcedureFilterModel
            {
                Amount = 30,
                Start = 0,
            };

            var logger = Mock.Of<ILogger<ProcedureService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetProcedures(filterModel, ProcedureSortProperties.BuildingName, SortDirections.Ascending)).ReturnsAsync(new List<ProcedureOverviewModel>());

            IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>());
            IEnumerable<ProcedureOverviewModel> actual = await service.GetProcedures(filterModel, ProcedureSortProperties.BuildingName, SortDirections.Ascending);

            Assert.NotNull(actual);
        }

        [Fact(DisplayName = "GetProcedures_Fail_NoModel|ProcedureServiceTester")]
        public async Task GetProcedures_Fail_NoModel()
        {
            Random rand = new Random();
            Int32 procedureId = rand.Next();

            var logger = Mock.Of<ILogger<ProcedureService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>());
            ProcedureServiceException exception = await Assert.ThrowsAsync<ProcedureServiceException>(() => service.GetProcedures(null, ProcedureSortProperties.BuildingName, SortDirections.Ascending));
            Assert.Equal(ProcedureServicExceptionReasons.NoFilter, exception.Reason);
        }

        [Fact(DisplayName = "GetProcedures_Fail_InvalidFilter|ProcedureServiceTester")]
        public async Task GetProcedures_Fail_InvalidFilter()
        {
            Random rand = new Random();
            Int32 procedureId = rand.Next();

            var logger = Mock.Of<ILogger<ProcedureService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            List<ProcedureFilterModel> invalidFilterModels = new List<ProcedureFilterModel>();
            invalidFilterModels.Add(new ProcedureFilterModel());
            invalidFilterModels.Add(new ProcedureFilterModel { Amount = -30 });
            invalidFilterModels.Add(new ProcedureFilterModel { Start = -1, Amount = 20 });
            invalidFilterModels.Add(new ProcedureFilterModel { Start = -1, Amount = -20 });
            invalidFilterModels.Add(new ProcedureFilterModel { Start = 0, Amount = 150 });

            foreach (ProcedureFilterModel item in invalidFilterModels)
            {
                IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>());
                ProcedureServiceException exception = await Assert.ThrowsAsync<ProcedureServiceException>(() => service.GetProcedures(item,ProcedureSortProperties.BuildingName,SortDirections.Ascending));
                Assert.Equal(ProcedureServicExceptionReasons.InvalidFilterModel, exception.Reason);
            }
        }
    }
}
