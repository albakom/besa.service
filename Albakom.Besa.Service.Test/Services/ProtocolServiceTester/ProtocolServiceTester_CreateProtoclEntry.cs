﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProtocolServiceTester
{
    public class ProtocolServiceTester_CreateProtoclEntry
    {
        [Fact(DisplayName = "CreateProtocolEntry_Simple_Pass|ProtocolServiceTester")]
        public async Task CreateProtocolEntry_Simple_Pass()
        {
            Dictionary<Int32, ProtocolNotificationTypes> protocolNotficationMapper = new Dictionary<int, ProtocolNotificationTypes>
            {
                {  0, ProtocolNotificationTypes.Message },
                {  1, ProtocolNotificationTypes.Email },
                {  2, ProtocolNotificationTypes.SMS },
                {  3, ProtocolNotificationTypes.Push },
            };

            Random rand = new Random();

            ProtocolEntyCreateModel entryModel = new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                Level = ProtocolLevels.Sucesss,
                RelatedType = MessageRelatedObjectTypes.Article,
                RelatedObjectId = rand.Next(),
                Timestamp = DateTimeOffset.Now,
                TriggeredByUserId = rand.Next(),
            };

            Int32 protoclEntryId = rand.Next();
            String name = $"Testeintrag Nr {rand.Next()}";

            var logger = Mock.Of<ILogger<ProtocolService>>();

            Dictionary<Int32, ProtocolNotificationTypes> usersToInform = new Dictionary<int, ProtocolNotificationTypes>();
            Int32 userAmount = rand.Next(20, 30);
            for (int i = 0; i < userAmount; i++)
            {
                ProtocolNotificationTypes type = ProtocolNotificationTypes.NoAction;
                for (int j = 0; j < protocolNotficationMapper.Count; j++)
                {
                    if (rand.NextDouble() > 0.5)
                    {
                        type |= protocolNotficationMapper[j];
                    }
                }

                if (type == ProtocolNotificationTypes.NoAction)
                {
                    type = ProtocolNotificationTypes.Message;
                }
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == entryModel.Action &&
           x.Level == entryModel.Level &&
           x.RelatedObjectId == entryModel.RelatedObjectId &&
           x.RelatedType == entryModel.RelatedType &&
           x.Timestamp == entryModel.Timestamp &&
           x.TriggeredByUserId == entryModel.TriggeredByUserId))).ReturnsAsync(protoclEntryId);

            mockStorage.Setup(ctx => ctx.GetUsersToInform(entryModel.RelatedType, entryModel.Action)).ReturnsAsync(usersToInform.ToDictionary(x => x.Key, x => x.Value));
            mockStorage.Setup(ctx => ctx.GetMessageObjectName(entryModel.RelatedObjectId.Value, entryModel.RelatedType)).ReturnsAsync(name);

            Int32 messageCounter = 0;
            Int32 emaiCounter = 0;
            Int32 smsCounter = 0;
            Int32 pushCounter = 0;

            var messageSendEngine = new Mock<IMessageSendEngine>(MockBehavior.Strict);
            messageSendEngine.Setup(engine => engine.SendMessage(
                It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message),
                It.Is<MessageCreateModel>(x => x.Action == entryModel.Action && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == name &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => messageCounter++);

            var emailSendEngine = new Mock<IEmailSendEngine>(MockBehavior.Strict);
            emailSendEngine.Setup(engine => engine.SendMessage(
                It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email),
                It.Is<MessageCreateModel>(x => x.Action == entryModel.Action && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == name &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => emaiCounter++);

            var smsSendEngine = new Mock<ISMSSendEngine>(MockBehavior.Strict);
            emailSendEngine.Setup(engine => engine.SendMessage(
            It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.SMS) == ProtocolNotificationTypes.SMS),
            It.Is<MessageCreateModel>(x => x.Action == entryModel.Action && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
            x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == name &&
           (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
            ))).Returns(Task.FromResult(new Object())).Callback(() => smsCounter++);

            var pushSendEngine = new Mock<IPushSendEngine>(MockBehavior.Strict);
            emailSendEngine.Setup(engine => engine.SendMessage(
            It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push),
            It.Is<MessageCreateModel>(x => x.Action == entryModel.Action && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
            x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == name &&
            (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
            ))).Returns(Task.FromResult(new Object())).Callback(() => pushCounter++);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                messageSendEngine.Object,
                emailSendEngine.Object,
                smsSendEngine.Object,
                pushSendEngine.Object,
                logger);

            Int32 id = await service.CreateProtocolEntry(entryModel);

            Assert.Equal(protoclEntryId, id);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message), messageCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email), emaiCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.SMS) == ProtocolNotificationTypes.SMS), smsCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push), pushCounter);
        }

        [Fact(DisplayName = "CreateProtocolEntry_CreateCollectionJob_Pass|ProtocolServiceTester")]
        public async Task CreateProtocolEntry_CreateCollectionJob_Pass()
        {
            Dictionary<Int32, ProtocolNotificationTypes> protocolNotficationMapper = new Dictionary<int, ProtocolNotificationTypes>
            {
                {  0, ProtocolNotificationTypes.Message },
                {  1, ProtocolNotificationTypes.Email },
                {  2, ProtocolNotificationTypes.SMS },
                {  3, ProtocolNotificationTypes.Push },
            };

            Random rand = new Random();

            ProtocolEntyCreateModel entryModel = new ProtocolEntyCreateModel
            {
                Action = MessageActions.Create,
                Level = ProtocolLevels.Sucesss,
                RelatedType = MessageRelatedObjectTypes.CollectionJob,
                RelatedObjectId = rand.Next(),
                Timestamp = DateTimeOffset.Now,
                TriggeredByUserId = rand.Next(),
            };

            Int32 protoclEntryId = rand.Next();

            var logger = Mock.Of<ILogger<ProtocolService>>();

            Dictionary<Int32, ProtocolNotificationTypes> usersToInform = new Dictionary<int, ProtocolNotificationTypes>();
            Int32 userAmount = rand.Next(20, 30);
            for (int i = 0; i < userAmount; i++)
            {
                ProtocolNotificationTypes type = ProtocolNotificationTypes.NoAction;
                for (int j = 0; j < protocolNotficationMapper.Count; j++)
                {
                    if (rand.NextDouble() > 0.5)
                    {
                        type |= protocolNotficationMapper[j];
                    }
                }

                if (type == ProtocolNotificationTypes.NoAction)
                {
                    type = ProtocolNotificationTypes.Message;
                }
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == entryModel.Action &&
           x.Level == entryModel.Level &&
           x.RelatedObjectId == entryModel.RelatedObjectId &&
           x.RelatedType == entryModel.RelatedType &&
           x.Timestamp == entryModel.Timestamp &&
           x.TriggeredByUserId == entryModel.TriggeredByUserId))).ReturnsAsync(protoclEntryId);

            List<Int32> usersIds = usersToInform.Select(x => x.Key).ToList();
            MessageActionJobBoundDetailsModel details = new MessageActionJobBoundDetailsModel
            {
                Id = rand.Next(),
                Name = $"Testname Nr {rand.Next()}",
                CollectionJob = new JobCollectionOverviewModel
                {
                    CollectionJobId = rand.Next(),
                    Name = "",
                },
            };

            mockStorage.Setup(ctx => ctx.GetUsersToInform(entryModel.RelatedType, entryModel.Action)).ReturnsAsync(new Dictionary<Int32, ProtocolNotificationTypes>());
            mockStorage.Setup(ctx => ctx.GetCompanyUserIdsByCollectionJobId(entryModel.RelatedObjectId.Value)).ReturnsAsync(usersIds);
            mockStorage.Setup(ctx => ctx.GetUsersToInform(MessageRelatedObjectTypes.NotSpecified, MessageActions.CollectionJobBoundToCompany, usersIds)).ReturnsAsync(usersToInform);
            mockStorage.Setup(ctx => ctx.GetMessageActionJobBoundDetails(entryModel.RelatedObjectId.Value)).ReturnsAsync(details);

            Int32 messageCounter = 0;
            Int32 emaiCounter = 0;
            Int32 smsCounter = 0;
            Int32 pushCounter = 0;

            var messageSendEngine = new Mock<IMessageSendEngine>(MockBehavior.Strict);
            messageSendEngine.Setup(engine => engine.SendMessage(
                It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message),
                It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.CollectionJobBoundToCompany && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                 x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobBoundDetailsModel) == true && ((MessageActionJobBoundDetailsModel)x.Details).CollectionJob != null &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => messageCounter++);

            var emailSendEngine = new Mock<IEmailSendEngine>(MockBehavior.Strict);
            emailSendEngine.Setup(engine => engine.SendMessage(
                It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email),
                It.Is<MessageCreateModel>(x => 
                x.Action == MessageActions.CollectionJobBoundToCompany && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                 x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobBoundDetailsModel) == true && ((MessageActionJobBoundDetailsModel)x.Details).CollectionJob != null &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => emaiCounter++);

            var smsSendEngine = new Mock<ISMSSendEngine>(MockBehavior.Strict);
            smsSendEngine.Setup(engine => engine.SendMessage(
             It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email),
                It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.CollectionJobBoundToCompany && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                 x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobBoundDetailsModel) == true && ((MessageActionJobBoundDetailsModel)x.Details).CollectionJob != null &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => smsCounter++);

            var pushSendEngine = new Mock<IPushSendEngine>(MockBehavior.Strict);
            pushSendEngine.Setup(engine => engine.SendMessage(
            It.Is<Int32>(x => usersToInform.ContainsKey(x) && (usersToInform[x] & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push),
            It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.CollectionJobBoundToCompany && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.SenderId == entryModel.TriggeredByUserId &&
                 x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobBoundDetailsModel) == true && ((MessageActionJobBoundDetailsModel)x.Details).CollectionJob != null &&
               (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => pushCounter++);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                messageSendEngine.Object,
                emailSendEngine.Object,
                smsSendEngine.Object,
                pushSendEngine.Object,
                logger);

            Int32 id = await service.CreateProtocolEntry(entryModel);

            Assert.Equal(protoclEntryId, id);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message), messageCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email), emaiCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.SMS) == ProtocolNotificationTypes.SMS), smsCounter);
            Assert.Equal(usersToInform.Count(x => (x.Value & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push), pushCounter);
        }


        [Fact(DisplayName = "CreateProtocolEntry_AcknowledgedJob_Pass|ProtocolServiceTester")]
        public async Task CreateProtocolEntry_AcknowledgedJob_Pass()
        {
            Dictionary<Int32, ProtocolNotificationTypes> protocolNotficationMapper = new Dictionary<int, ProtocolNotificationTypes>
            {
                {  0, ProtocolNotificationTypes.Message },
                {  1, ProtocolNotificationTypes.Email },
                {  2, ProtocolNotificationTypes.SMS },
                {  3, ProtocolNotificationTypes.Push },
            };

            Random rand = new Random(12345);

            ProtocolEntyCreateModel entryModel = new ProtocolEntyCreateModel
            {
                Action = MessageActions.JobAcknowledged,
                Level = ProtocolLevels.Sucesss,
                RelatedType = MessageRelatedObjectTypes.InjectJob,
                RelatedObjectId = rand.Next(),
                Timestamp = DateTimeOffset.Now,
                TriggeredByUserId = rand.Next(),
            };

            Int32 protoclEntryId = rand.Next();

            var logger = Mock.Of<ILogger<ProtocolService>>();


            ProtocolNotificationTypes type = ProtocolNotificationTypes.NoAction;
            for (int j = 0; j < protocolNotficationMapper.Count; j++)
            {
                if (rand.NextDouble() > 0.5)
                {
                    type |= protocolNotficationMapper[j];
                }
            }

            if (type == ProtocolNotificationTypes.NoAction)
            {
                type = ProtocolNotificationTypes.Message;
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == entryModel.Action &&
           x.Level == entryModel.Level &&
           x.RelatedObjectId == entryModel.RelatedObjectId &&
           x.RelatedType == entryModel.RelatedType &&
           x.Timestamp == entryModel.Timestamp &&
           x.TriggeredByUserId == entryModel.TriggeredByUserId))).ReturnsAsync(protoclEntryId);

            MessageActionJobAcknowledgedDetailsModel details = new MessageActionJobAcknowledgedDetailsModel
            {
                Id = entryModel.RelatedObjectId.Value,
                Name = $"Testname Nr {rand.Next()}",
                AcceptedAt = DateTimeOffset.Now,
                AcepptedBy = new UserOverviewModel(),
            };

            Int32 finisherUserId = rand.Next();

            mockStorage.Setup(ctx => ctx.GetUsersToInform(entryModel.RelatedType, entryModel.Action)).ReturnsAsync(new Dictionary<Int32, ProtocolNotificationTypes>());
            mockStorage.Setup(ctx => ctx.GetJobFinisherUserIdByJobId(entryModel.RelatedObjectId.Value)).ReturnsAsync(finisherUserId);
            mockStorage.Setup(ctx => ctx.GetJobFinisherUserIdByJobId(entryModel.RelatedObjectId.Value)).ReturnsAsync(finisherUserId);
            mockStorage.Setup(ctx => ctx.GetProtocolNofitcation(MessageRelatedObjectTypes.NotSpecified, MessageActions.InformJobFinisherAboutAcknlowedged, finisherUserId)).ReturnsAsync(type);
            mockStorage.Setup(ctx => ctx.GetMessageActionJobAcknowledgedDetails(entryModel.RelatedObjectId.Value)).ReturnsAsync(details);

            Int32 messageCounter = 0;
            Int32 emaiCounter = 0;
            Int32 smsCounter = 0;
            Int32 pushCounter = 0;

            var messageSendEngine = new Mock<IMessageSendEngine>(MockBehavior.Strict);
            messageSendEngine.Setup(engine => engine.SendMessage(finisherUserId,
                It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.InformJobFinisherAboutAcknlowedged && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.ReceiverId == finisherUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobAcknowledgedDetailsModel) == true && ((MessageActionJobAcknowledgedDetailsModel)x.Details).AcepptedBy != null &&
                (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => messageCounter++);

            var emailSendEngine = new Mock<IEmailSendEngine>(MockBehavior.Strict);
            emailSendEngine.Setup(engine => engine.SendMessage(finisherUserId,
                It.Is<MessageCreateModel>(x => 
                x.Action == MessageActions.InformJobFinisherAboutAcknlowedged && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.ReceiverId == finisherUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobAcknowledgedDetailsModel) == true && ((MessageActionJobAcknowledgedDetailsModel)x.Details).AcepptedBy != null &&
                (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => emaiCounter++);

            var smsSendEngine = new Mock<ISMSSendEngine>(MockBehavior.Strict);
            smsSendEngine.Setup(engine => engine.SendMessage(finisherUserId,
                It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.InformJobFinisherAboutAcknlowedged && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.ReceiverId == finisherUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobAcknowledgedDetailsModel) == true && ((MessageActionJobAcknowledgedDetailsModel)x.Details).AcepptedBy != null &&
                (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => smsCounter++);

            var pushSendEngine = new Mock<IPushSendEngine>(MockBehavior.Strict);
            pushSendEngine.Setup(engine => engine.SendMessage(finisherUserId,
                It.Is<MessageCreateModel>(x =>
                x.Action == MessageActions.InformJobFinisherAboutAcknlowedged && x.RelatedObjectType == MessageRelatedObjectTypes.NotSpecified && x.ProtocolEntryId == protoclEntryId && x.ReceiverId == finisherUserId &&
                x.Details != null && x.Details.Id == entryModel.RelatedObjectId && x.Details.Name == details.Name &&
                (x.Details is MessageActionJobAcknowledgedDetailsModel) == true && ((MessageActionJobAcknowledgedDetailsModel)x.Details).AcepptedBy != null &&
                (DateTimeOffset.Now - x.Timestamp).TotalMinutes < 2
                ))).Returns(Task.FromResult(new Object())).Callback(() => pushCounter++);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                messageSendEngine.Object,
                emailSendEngine.Object,
                smsSendEngine.Object,
                pushSendEngine.Object,
                logger);

            Int32 id = await service.CreateProtocolEntry(entryModel);

            Assert.Equal(protoclEntryId, id);
            Assert.True((type & ProtocolNotificationTypes.Message) == ProtocolNotificationTypes.Message ? 1 == messageCounter : 0 == messageCounter);
            Assert.True((type & ProtocolNotificationTypes.Email) == ProtocolNotificationTypes.Email ? 1 == emaiCounter : 0 == emaiCounter);
            Assert.True((type & ProtocolNotificationTypes.SMS) == ProtocolNotificationTypes.SMS ? 1 == smsCounter : 0 == smsCounter);
            Assert.True((type & ProtocolNotificationTypes.Push) == ProtocolNotificationTypes.Push ? 1 == pushCounter : 0 == pushCounter);
        }

        //[Fact(DisplayName = "GetConnectBuildingDetails_Fail_NotFound|ProcedureServiceTester")]
        //public async Task GetConnectBuildingDetails_Fail_NotFound()
        //{
        //    Random rand = new Random();
        //    Int32 procedureId = rand.Next();

        //    var logger = Mock.Of<ILogger<ProcedureService>>();

        //    var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
        //    mockStorage.Setup(ctx => ctx.CheckIfBuildingConnectionProcedureExists(procedureId)).ReturnsAsync(false);

        //    IProcedureService service = new ProcedureService(mockStorage.Object, logger, Mock.Of<IProtocolService>()); ;
        //    ProcedureServiceException exception = await Assert.ThrowsAsync<ProcedureServiceException>(() => service.GetConnectBuildingDetails(procedureId));
        //    Assert.Equal(ProcedureServicExceptionReasons.NotFound, exception.Reason);
        //}
    }
}
