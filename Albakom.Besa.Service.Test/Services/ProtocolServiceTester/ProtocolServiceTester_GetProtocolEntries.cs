﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProtocolServiceTester
{
    public class ProtocolServiceTester_GetProtocolEntries
    {
        [Fact(DisplayName = "GetProtocolEntries_Üass|ProtocolServiceTester")]
        public async Task GetProtocolEntries_Pass()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(30, 50);
            List<ProtocolEntryModel> expectedResult = new List<ProtocolEntryModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ProtocolEntryModel protocolEntry = new ProtocolEntryModel
                {
                    Action = MessageActions.Create,
                    Id = rand.Next(),
                    Name = $"Testname Nr. {rand.Next()}",
                    RelatedObjectId = rand.Next(),
                };
            }

            ProtocolFilterModel filterModel = new ProtocolFilterModel
            {
                Actions = new List<MessageActions> { MessageActions.AddEmployeeToCompany, MessageActions.Create },
                Amount = rand.Next(30, 50),
                Start = rand.Next(0, 100),
                EndTime = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                StartTime = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                Types = new List<MessageRelatedObjectTypes> { MessageRelatedObjectTypes.ActivationJob, },
            };

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetProtocolEntries(It.Is<ProtocolFilterModel>(x =>
          x.Actions == filterModel.Actions &&
          x.Amount == filterModel.Amount &&
          x.EndTime == filterModel.EndTime &&
          x.Start == filterModel.Start &&
          x.StartTime == filterModel.StartTime &&
          x.Types == filterModel.Types))).ReturnsAsync(expectedResult);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            IEnumerable<ProtocolEntryModel> actual = await service.GetProtocolEntries(filterModel);

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());
        }

        [Fact(DisplayName = "GetProtocolEntries_Failed_InvalidFilterModel|ProtocolServiceTester")]
        public async Task GetProtocolEntries_Failed_InvalidFilterModel()
        {
            Random rand = new Random();

            Int32 amount = rand.Next(30, 50);
            List<ProtocolEntryModel> expectedResult = new List<ProtocolEntryModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ProtocolEntryModel protocolEntry = new ProtocolEntryModel
                {
                    Action = MessageActions.Create,
                    Id = rand.Next(),
                    Name = $"Testname Nr. {rand.Next()}",
                    RelatedObjectId = rand.Next(),
                };
            }

            List<ProtocolFilterModel> invalidFilters = new List<ProtocolFilterModel>();

            {
                ProtocolFilterModel filterModel = new ProtocolFilterModel
                {
                    Actions = new List<MessageActions> { MessageActions.AddEmployeeToCompany, MessageActions.Create },
                    Amount = rand.Next(101, 1000),
                    Start = rand.Next(0, 100),
                    EndTime = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                    StartTime = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                    Types = new List<MessageRelatedObjectTypes> { MessageRelatedObjectTypes.ActivationJob, },
                };

                invalidFilters.Add(filterModel);
            }


            {
                ProtocolFilterModel filterModel = new ProtocolFilterModel
                {
                    Actions = new List<MessageActions> { MessageActions.AddEmployeeToCompany, MessageActions.Create },
                    Amount = -rand.Next(1, 30),
                    Start = rand.Next(0, 100),
                    EndTime = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                    StartTime = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                    Types = new List<MessageRelatedObjectTypes> { MessageRelatedObjectTypes.ActivationJob, },
                };

                invalidFilters.Add(filterModel);
            }

            {
                ProtocolFilterModel filterModel = new ProtocolFilterModel
                {
                    Actions = new List<MessageActions> { MessageActions.AddEmployeeToCompany, MessageActions.Create },
                    Amount = rand.Next(1, 20),
                    Start = -rand.Next(1, 100),
                    EndTime = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                    StartTime = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                    Types = new List<MessageRelatedObjectTypes> { MessageRelatedObjectTypes.ActivationJob, },
                };

                invalidFilters.Add(filterModel);
            }

            {
                ProtocolFilterModel filterModel = new ProtocolFilterModel
                {
                    Actions = new List<MessageActions> { MessageActions.AddEmployeeToCompany, MessageActions.Create },
                    Amount = rand.Next(1, 20),
                    Start = -rand.Next(1, 100),
                    EndTime = DateTimeOffset.Now.AddDays(-rand.Next(3, 10)),
                    StartTime = DateTimeOffset.Now.AddDays(rand.Next(3, 10)),
                    Types = new List<MessageRelatedObjectTypes> { MessageRelatedObjectTypes.ActivationJob, },
                };

                invalidFilters.Add(filterModel);
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            foreach (ProtocolFilterModel filter in invalidFilters)
            {
                ProtocolServiceException exception = await Assert.ThrowsAsync<ProtocolServiceException>(() => service.GetProtocolEntries(filter));
                Assert.Equal(ProtocolServicExceptionReasons.InvalidFilterModel, exception.Reason);
            }
        }
    }
}
