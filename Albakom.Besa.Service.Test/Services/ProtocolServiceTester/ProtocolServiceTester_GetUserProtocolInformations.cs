﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProtocolServiceTester
{
    public class ProtocolServiceTester_GetUserProtocolInformations
    {
        [Fact(DisplayName = "GetUserProtocolInformations_Pass|ProtocolServiceTester")]
        public async Task GetUserProtocolInformations_Pass()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            Int32 amount = rand.Next(30, 50);
            List<ProtocolUserInformModel> expectedResult = new List<ProtocolUserInformModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                ProtocolUserInformModel protocolEntry = new ProtocolUserInformModel
                {
                    Action = MessageActions.Create,
                    NotifcationType = ProtocolNotificationTypes.Message,
                    RelatedType = MessageRelatedObjectTypes.Branchable,
                };
            }

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserProtocolInformation(userAuthId)).ReturnsAsync(expectedResult);


            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            IEnumerable<ProtocolUserInformModel> actual = await service.GetUserProtocolInformations(userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());
        }

        [Fact(DisplayName = "GetUserProtocolInformations_Failed_UserNotFound|ProtocolServiceTester")]
        public async Task GetUserProtocolInformations_Failed_UserNotFound()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            ProtocolServiceException exception = await Assert.ThrowsAsync<ProtocolServiceException>(() => service.GetUserProtocolInformations(userAuthId));
            Assert.Equal(ProtocolServicExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
