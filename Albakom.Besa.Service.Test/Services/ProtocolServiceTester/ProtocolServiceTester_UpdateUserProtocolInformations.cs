﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.ProtocolServiceTester
{
    public class ProtocolServiceTester_UpdateUserProtocolInformations
    {
        [Fact(DisplayName = "UpdateUserProtocolInformations_Pass|ProtocolServiceTester")]
        public async Task UpdateUserProtocolInformations_Pass()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            ProtocolUserInformModel notExisitingEntry = new ProtocolUserInformModel
            {
                Action = MessageActions.Create,
                NotifcationType = ProtocolNotificationTypes.Message,
                RelatedType = MessageRelatedObjectTypes.Branchable,
            };

            ProtocolUserInformModel unchangedEntry = new ProtocolUserInformModel
            {
                Action = MessageActions.Delete,
                NotifcationType = ProtocolNotificationTypes.Email,
                RelatedType = MessageRelatedObjectTypes.InjectJob,
            };

            ProtocolUserInformModel changedEntry = new ProtocolUserInformModel
            {
                Action = MessageActions.JobDiscared,
                NotifcationType = ProtocolNotificationTypes.SMS,
                RelatedType = MessageRelatedObjectTypes.MainCables,
            };

            ProtocolUserInformModel changedEntryAsInput = new ProtocolUserInformModel
            {
                Action = MessageActions.JobDiscared,
                NotifcationType = changedEntry.NotifcationType | ProtocolNotificationTypes.Push,
                RelatedType = MessageRelatedObjectTypes.MainCables,
            };

            List<ProtocolUserInformModel> informationsInDb = new List<ProtocolUserInformModel> { unchangedEntry, changedEntry };
            List<ProtocolUserInformModel> input = new List<ProtocolUserInformModel> { notExisitingEntry, unchangedEntry, changedEntryAsInput };

            Int32 updateCounter = 0;

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserProtocolInformation(userAuthId)).ReturnsAsync(informationsInDb);
            mockStorage.Setup(ctx => ctx.UpdateProtocolNotification(userAuthId, It.Is< ProtocolUserInformModel>( x => 
                x.Action == changedEntryAsInput.Action && x.RelatedType == changedEntryAsInput.RelatedType  && x.NotifcationType == changedEntryAsInput.NotifcationType
            ))).ReturnsAsync(true).Callback( () => updateCounter++);


            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            IEnumerable<ProtocolUserInformModel> actual = await service.UpdateUserProtocolInformations(userAuthId, input);

            Assert.NotNull(actual);
            Assert.Equal(1, updateCounter);
            Assert.True(actual.Count() == 2);

            Assert.Equal(changedEntryAsInput.NotifcationType, changedEntry.NotifcationType);
        }

        [Fact(DisplayName = "UpdateUserProtocolInformations_Failed_UserNotFound|ProtocolServiceTester")]
        public async Task UpdateUserProtocolInformations_Failed_UserNotFound()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IProtocolService service = new ProtocolService(mockStorage.Object,
                Mock.Of<IMessageSendEngine>(),
                Mock.Of<IEmailSendEngine>(),
                Mock.Of<ISMSSendEngine>(),
                Mock.Of<IPushSendEngine>(),
                Mock.Of<ILogger<ProtocolService>>());

            ProtocolServiceException exception = await Assert.ThrowsAsync<ProtocolServiceException>(() => service.UpdateUserProtocolInformations(userAuthId, null));
            Assert.Equal(ProtocolServicExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
