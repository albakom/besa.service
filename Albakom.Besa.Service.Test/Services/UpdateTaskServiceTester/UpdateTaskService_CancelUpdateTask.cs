﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_CancelUpdateTask
    {
        private static void PrepareStorage(int taskId, bool saveResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTasksExists(taskId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskIsFinished(taskId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CancelUpdateTask(taskId)).ReturnsAsync(saveResult);
        }

        [Fact(DisplayName = "CancelUpdateTask_Pass|UpdateTaskServiceTester")]
        public async Task CancelUpdateTask_Pass()
        {
            Random rand = new Random();

            Int32 taskId = rand.Next();
            Boolean saveResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskId, saveResult, mockStorage);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.CancelUpdateTask(taskId);
            Assert.Equal(saveResult, actual);
        }

        [Fact(DisplayName = "CancelUpdateTask_WithNotifier_Pass|UpdateTaskServiceTester")]
        public async Task CancelUpdateTask_WithNotifier_Pass()
        {
            Random rand = new Random();

            Int32 taskId = rand.Next();
            Boolean saveResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskId, saveResult, mockStorage);

            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            Int32 callerCounter = 0;
            notifier.Setup(noti => noti.UpdateTaskCanceled(taskId)).Returns(Task.FromResult(true)).Callback( () => callerCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            Boolean actual = await service.CancelUpdateTask(taskId);
            Assert.Equal(saveResult, actual);
            Assert.Equal(1, callerCounter);
        }

        [Fact(DisplayName = "CancelUpdateTask_Fail_TaskNotFound|UpdateTaskServiceTester")]
        public async Task CancelUpdateTask_Fail_TaskNotFound()
        {
            Random rand = new Random();

            Int32 taskId = rand.Next();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskId, false, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTasksExists(taskId)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>()); ;
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.CancelUpdateTask(taskId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.TaskNotFound, exception.Reason);
        }

        [Fact(DisplayName = "CancelUpdateTask_Fail_TaskIsFinished|UpdateTaskServiceTester")]
        public async Task CancelUpdateTask_Fail_TaskIsFinished()
        {
            Random rand = new Random();

            Int32 taskId = rand.Next();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskId, false, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskIsFinished(taskId)).ReturnsAsync(true);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>()); ;
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.CancelUpdateTask(taskId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
