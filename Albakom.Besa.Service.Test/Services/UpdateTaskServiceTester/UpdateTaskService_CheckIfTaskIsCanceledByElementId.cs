﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_CheckIfTaskIsCanceledByElementId
    {
        [Fact(DisplayName = "CheckIfTaskIsCanceledByElementId|UpdateTaskServiceTester")]
        public async Task CheckIfTaskIsCanceledByElementId_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfTaskIsCanceledByElementId(elementId)).ReturnsAsync(expectedResult);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.CheckIfTaskIsCanceledByElementId(elementId);

            Assert.Equal(expectedResult,actual);
        }

        [Fact(DisplayName = "CheckIfTaskIsCanceledByElementId_Fail_ElementNotFound|UpdateTaskServiceTester")]
        public async Task CheckIfTaskIsCanceledByElementId_Fail_ElementNotFound()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>()); ;
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.CheckIfTaskIsCanceledByElementId(elementId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.ElementNotFound, exception.Reason);
        }
    }
}
