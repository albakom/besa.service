﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_FinishTask
    {
        private static void PrepareStorage(UpdateTasks taskType, int taskId, bool expectedResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfOpenUpdateTasksExists(taskType)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskIdByTaskTye(taskType)).ReturnsAsync(taskId);
            mockStorage.Setup(ctx => ctx.CheckIfTaskHasOpenElements(taskType)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.EditUpdateTask(It.Is<UpdateTaskFinishModel>(x =>
            x.Cancel == false &&
            x.TaskId == taskId &&
            Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
            ))).ReturnsAsync(expectedResult);
        }

        [Fact(DisplayName = "FinishTask_Pass|UpdateTaskServiceTester")]
        public async Task FinishTask_Pass()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            Int32 taskId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskType, taskId, expectedResult, mockStorage);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.FinishTask(taskType);

            Assert.Equal(expectedResult, actual);
        }

        [Fact(DisplayName = "FinishTask_WithNotifier_Pass|UpdateTaskServiceTester")]
        public async Task FinishTask_WithNotifier_Pass()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            Int32 taskId = rand.Next();
            Boolean expectedResult = true;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskType, taskId, expectedResult, mockStorage);

            Int32 notifierCallbackCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            notifier.Setup(noti => noti.UpdateTaskFinished(It.Is<UpdateTasFinishedStateChangedModel>(x => x.Id == taskId &&
           (DateTimeOffset.Now - x.FinishedAt).TotalSeconds < 60))).Returns(Task.FromResult(true)).Callback(() => notifierCallbackCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            Boolean actual = await service.FinishTask(taskType);

            Assert.Equal(expectedResult, actual);
            Assert.Equal(1, notifierCallbackCounter);

        }

        [Fact(DisplayName = "FinishTask_Fail_NoOpenTask|UpdateTaskServiceTester")]
        public async Task FinishTask_Fail_NoOpenTask()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            Int32 taskId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskType, taskId, expectedResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfTaskHasOpenElements(taskType)).ReturnsAsync(true);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>()); ;
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishTask(taskType));
            Assert.Equal(UpdateTaskServiceExceptionReasons.InvalidOperation, exception.Reason);
        }

        [Fact(DisplayName = "FinishTask_Fail_TaskHasOpenElements|UpdateTaskServiceTester")]
        public async Task FinishTask_Fail_TaskHasOpenElements()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            Int32 taskId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(taskType, taskId, expectedResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfTaskHasOpenElements(taskType)).ReturnsAsync(true);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishTask(taskType));
            Assert.Equal(UpdateTaskServiceExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
