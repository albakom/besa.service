﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_FinishUpdateElement
    {

        private static UpdateTaskResultModel GenerateResultModel(Random rand)
        {
            UpdateTaskResultModel taskResultModel = new UpdateTaskResultModel();
            Int32 idAmount = rand.Next(30, 100);
            for (int i = 0; i < idAmount; i++)
            {
                Int32 itemId = rand.Next();
                Double randomValue = rand.NextDouble();
                if (randomValue < 0.33)
                {
                    taskResultModel.AddedIds.Add(itemId);
                }
                else if (randomValue > 0.66)
                {
                    taskResultModel.DeletedIds.Add(itemId);
                }
                else
                {
                    taskResultModel.UpdatedIds.Add(itemId);
                }
            }

            return taskResultModel;
        }

        private static void PrepareStorage(UpdateTaskElementStates finishedState, Int32 elementId, bool expectedResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfTaskElementIsAlreadyFinished(elementId)).ReturnsAsync(false);

            mockStorage.Setup(ctx => ctx.EditUpdateTaskElement(It.Is<UpdateTaskElementUpdateModel>(x =>
            x.ElementId == elementId &&
            x.State == finishedState &&
            String.IsNullOrEmpty(x.Result) == false &&
            Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5
            ))).ReturnsAsync(expectedResult);

            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementOverviewById(elementId)).ReturnsAsync(new UpdateTaskElementRawOverviewModel { Id = elementId });

        }

        [Fact(DisplayName = "FinishUpdateElement_AsSuccess_Pass|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_AsSuccess_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Success;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);

            UpdateTaskResultModel taskResultModel = GenerateResultModel(rand);

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = taskResultModel,
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.FinishUpdateElement(updateModel);

            Assert.Equal(expectedResult, actual);
        }

        [Fact(DisplayName = "FinishUpdateElement_AsSuccess_WithNotifier_Pass|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_AsSuccess_WithNotifier_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Success;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);

            Int32 notifierCallbackCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            notifier.Setup(noti => noti.TaskElementChanged(It.Is<UpdateTaskElementOverviewModel>(x => x.Id == elementId))).Returns(Task.FromResult(true)).Callback(() => notifierCallbackCounter++);

            UpdateTaskResultModel taskResultModel = GenerateResultModel(rand);

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = taskResultModel,
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            Boolean actual = await service.FinishUpdateElement(updateModel);

            Assert.Equal(expectedResult, actual);
            Assert.Equal(1, notifierCallbackCounter);
        }

        [Fact(DisplayName = "FinishUpdateElement_AsWarning_Pass|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_AsWarning_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Waring;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);

            UpdateTaskResultModel taskResultModel = GenerateResultModel(rand);
            taskResultModel.Warning = $"Hier die Warnung nummer {rand.Next()} anzeigen";

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = taskResultModel,
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.FinishUpdateElement(updateModel);

            Assert.Equal(expectedResult, actual);
        }

        [Fact(DisplayName = "FinishUpdateElement_AsError_Pass|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_AsError_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Error;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);

            UpdateTaskResultModel taskResultModel = GenerateResultModel(rand);
            taskResultModel.Error = $"Hier der Fehler mit der Nummer {rand.Next()} anzeigen";

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = taskResultModel,
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.FinishUpdateElement(updateModel);

            Assert.Equal(expectedResult, actual);
        }

        [Fact(DisplayName = "FinishUpdateElement_Failed_NoModel|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_Failed_NoModel()
        {
            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());

            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishUpdateElement(null));
            Assert.Equal(UpdateTaskServiceExceptionReasons.ModelIsNull, exception.Reason);
        }

        [Fact(DisplayName = "FinishUpdateElement_Failed_InvalidModels|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_Failed_InvalidModels()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            List<UpdateTaskElementFinishModel> invalidModels = new List<UpdateTaskElementFinishModel>();
            {
                UpdateTaskResultModel taskResultModel = GenerateResultModel(rand);
                taskResultModel.AddedIds = taskResultModel.DeletedIds = taskResultModel.UpdatedIds = new List<Int32>();

                UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
                {
                    ElementId = rand.Next(),
                    State = UpdateTaskElementStates.Success,
                    Result = taskResultModel,
                };
            }
            {
                UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
                {
                    ElementId = rand.Next(),
                    State = UpdateTaskElementStates.Waring,
                    Result = GenerateResultModel(rand),
                };
            }
            {
                UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
                {
                    ElementId = rand.Next(),
                    State = UpdateTaskElementStates.Error,
                    Result = GenerateResultModel(rand),
                };
            }

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());

            foreach (UpdateTaskElementFinishModel item in invalidModels)
            {
                UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishUpdateElement(item));
                Assert.Equal(UpdateTaskServiceExceptionReasons.InvalidOperation, exception.Reason);
            }
        }

        [Fact(DisplayName = "FinishUpdateElement_Failed_ElementNotFound|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_Failed_ElementNotFound()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Success;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(false);

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = GenerateResultModel(rand),
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());

            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishUpdateElement(updateModel));
            Assert.Equal(UpdateTaskServiceExceptionReasons.ElementNotFound, exception.Reason);
        }

        [Fact(DisplayName = "FinishUpdateElement_Failed_ElementNAlreadyFinished|UpdateTaskServiceTester")]
        public async Task FinishUpdateElement_Failed_ElementNAlreadyFinished()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            UpdateTaskElementStates state = UpdateTaskElementStates.Success;
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(state, elementId, expectedResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfTaskElementIsAlreadyFinished(elementId)).ReturnsAsync(true);

            UpdateTaskElementFinishModel updateModel = new UpdateTaskElementFinishModel
            {
                ElementId = elementId,
                State = state,
                Result = GenerateResultModel(rand),
            };

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());

            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.FinishUpdateElement(updateModel));
            Assert.Equal(UpdateTaskServiceExceptionReasons.InvalidOperation, exception.Reason);
        }
    }
}
