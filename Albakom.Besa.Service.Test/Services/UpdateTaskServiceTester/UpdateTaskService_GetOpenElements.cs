﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_GetOpenElements
    {
        private static List<UpdateTaskElementModel> GenerateResults(Random rand)
        {
            Int32 elementAmount = rand.Next(10, 40);
            List<UpdateTaskElementModel> result = new List<UpdateTaskElementModel>();
            for (int i = 0; i < elementAmount; i++)
            {
                UpdateTaskElementModel element = new UpdateTaskElementModel
                {
                    AdapterId = Guid.NewGuid().ToString(),
                    Id = i + 1,
                    ObjectId = rand.Next(),
                };
                result.Add(element);
            }

            return result;
        }

        private static void PrepareStorage(String userAuthId, UpdateTasks taskType, IEnumerable<UpdateTaskElementModel> elements, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfOpenUpdateTasksExists(taskType)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetOpenUpdateTaskElement(taskType)).ReturnsAsync(elements);
        }

        [Fact(DisplayName = "GetOpenElements_TaskExists_Pass|UpdateTaskServiceTester")]
        public async Task GetOpenElements_TaskExists_Pass()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            List<UpdateTaskElementModel> result = GenerateResults(rand);
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(userAuthId, taskType, result, mockStorage);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            IEnumerable<UpdateTaskElementModel> actual = await service.GetOpenElements(taskType, userAuthId);

            Assert.NotNull(actual);
            Assert.Equal(result.Count, actual.Count());
        }

        private Boolean CompareDictionaires<TKey, TValue>(IDictionary<TKey, TValue> expected, IDictionary<TKey, TValue> actual) where TValue : IComparable<TValue>
        {
            if (expected.Count != actual.Count) { return false; }

            Boolean result = true;
            foreach (KeyValuePair<TKey, TValue> item in expected)
            {
                if (actual.ContainsKey(item.Key) == false) { return false; }

                TValue value = actual[item.Key];
                if (item.Value.CompareTo(value) != 0)
                {
                    return false;
                }
            }

            return result;
        }

        [Fact(DisplayName = "GetOpenElements_CreateTask_Pass|UpdateTaskServiceTester")]
        public async Task GetOpenElements_CreateTask_Pass()
        {
            Random rand = new Random();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            List<UpdateTasks> tasks = new List<UpdateTasks>
            {
                UpdateTasks.Cables,
                UpdateTasks.CableTypes,
                UpdateTasks.MainCableForBranchable,
                UpdateTasks.MainCableForPops,
                UpdateTasks.PatchConnections,
                UpdateTasks.PoPs,
                UpdateTasks.Splices,
                UpdateTasks.UpdateBuildings,
            };

            foreach (UpdateTasks taskType in tasks)
            {
                Int32 adapterResultAmount = rand.Next(30, 100);
                Dictionary<String, Int32> adapterResults = new Dictionary<string, Int32>();
                List<UpdateTaskElementModel> result = GenerateResults(rand);

                var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
                mockStorage.Setup(ctx => ctx.CheckIfOpenUpdateTasksExists(taskType)).ReturnsAsync(false);
                switch (taskType)
                {
                    case UpdateTasks.MainCableForBranchable:
                    case UpdateTasks.UpdateBuildings:
                    case UpdateTasks.Cables:
                    case UpdateTasks.Splices:
                        mockStorage.Setup(ctx => ctx.GetBranchablesWithProjectAdapterId()).ReturnsAsync(adapterResults);
                        break;
                    case UpdateTasks.MainCableForPops:
                        mockStorage.Setup(ctx => ctx.GetPopsWithProjectAdapterId()).ReturnsAsync(adapterResults);
                        break;
                    case UpdateTasks.PatchConnections:
                        mockStorage.Setup(ctx => ctx.GetCablesWithFlatIDs()).ReturnsAsync(adapterResults);
                        break;
                    case UpdateTasks.CableTypes:
                    case UpdateTasks.PoPs:
                        adapterResults = new Dictionary<String, Int32>();
                        result = new List<UpdateTaskElementModel>();
                        break;
                    default:
                        break;
                }

                for (int i = 0; i < adapterResults.Count; i++)
                {
                    adapterResults.Add(Guid.NewGuid().ToString(), rand.Next());
                }

                PrepareStorage(userAuthId, taskType, result, mockStorage);

                mockStorage.Setup(ctx => ctx.CreateUpdateTask(It.Is<UpdateTaskCreateModel>(
                    x =>
                    x.UserAuthId == userAuthId &&
                    x.Task == taskType &&
                    Math.Abs((DateTimeOffset.Now - x.Timestamp).TotalMinutes) <= 5 &&
                    CompareDictionaires(adapterResults, x.Elements) == true
                    ))).ReturnsAsync(rand.Next());

                IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
                IEnumerable<UpdateTaskElementModel> actual = await service.GetOpenElements(taskType, userAuthId);

                Assert.NotNull(actual);
                Assert.Equal(result.Count, actual.Count());
            }
        }


        [Fact(DisplayName = "GetOpenElements_Fail_UserNotFound|UpdateTaskServiceTester")]
        public async Task GetOpenElements_Fail_UserNotFound()
        {
            Random rand = new Random();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            List<UpdateTaskElementModel> result = GenerateResults(rand);
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(userAuthId, taskType, result, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());

            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.GetOpenElements(taskType, userAuthId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
