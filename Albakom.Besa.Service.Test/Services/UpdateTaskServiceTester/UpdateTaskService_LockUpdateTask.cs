﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_LockUpdateTask
    {
        [Fact(DisplayName = "LockUpdateTask_NoTaskExists_Pass|UpdateTaskServiceTester")]
        public async Task LockUpdateTask_NoTaskExists_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfOpenUpdateTasksExists(taskType)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.LockUpdateTask(taskType);
            Assert.False(actual);
        }

        [Fact(DisplayName = "LockUpdateTask_UpdateTaskExists_Pass|UpdateTaskServiceTester")]
        public async Task LockUpdateTask_UpdateTaskExists_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            UpdateTasks taskType = (UpdateTasks)rand.Next(1, 9);

            Boolean lockedResult = rand.NextDouble() > 0.5;
            Int32 taskId = rand.Next();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfOpenUpdateTasksExists(taskType)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskIdByTaskTye(taskType)).ReturnsAsync(taskId);
            mockStorage.Setup(ctx => ctx.SetLockStateOfTask(taskId,true)).ReturnsAsync(lockedResult);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.LockUpdateTask(taskType);
            Assert.Equal(lockedResult,actual);
        }
    }
}
