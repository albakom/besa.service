﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_RestoreLockedUpdateTaskElements
    {
        [Fact(DisplayName = "RestoreLockedUpdateTaskElements_Pass|UpdateTaskServiceTester")]
        public async Task RestoreLockedUpdateTaskElements_Pass()
        {
            Random rand = new Random();

            Int32 elementAmount = rand.Next(10, 30);
            List<Int32> ids = new List<int>(elementAmount);
            for (int i = 0; i < elementAmount; i++)
            {
                ids.Add(rand.Next());
            }

            List<Int32> copiedIds = new List<int>(ids);

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementsInProgressSince(
                It.Is<DateTimeOffset>(x => Math.Abs((DateTimeOffset.Now - x).TotalMinutes) < 10)))
                .ReturnsAsync(ids);

            mockStorage.Setup(ctx => ctx.ResetUpdateTaskElement(
              It.Is<Int32>(x => copiedIds.Contains(x) == true)))
              .ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x =>
               {
                   copiedIds.Remove(x);
                   return true;
               });

            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementOverviewById(It.Is<Int32>(x => ids.Contains(x)))).ReturnsAsync(new UpdateTaskElementRawOverviewModel { });

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.RestoreLockedUpdateTaskElements();
            Assert.True(actual);
            Assert.True(copiedIds.Count == 0);
        }

        [Fact(DisplayName = "RestoreLockedUpdateTaskElements_WithNotifier_Pass|UpdateTaskServiceTester")]
        public async Task RestoreLockedUpdateTaskElements_WithNotifier_Pass()
        {
            Random rand = new Random();

            Int32 elementAmount = rand.Next(10, 30);
            List<Int32> ids = new List<int>(elementAmount);
            Dictionary<Int32, UpdateTaskElementRawOverviewModel> taskElementsModels = new Dictionary<int, UpdateTaskElementRawOverviewModel>();
            for (int i = 0; i < elementAmount; i++)
            {
                Int32 id = rand.Next();
                ids.Add(id);

                UpdateTaskElementRawOverviewModel overviewModel = new UpdateTaskElementRawOverviewModel
                {
                    Id = id,
                };

                taskElementsModels.Add(id, overviewModel);
            }

            List<Int32> copiedIds = new List<int>(ids);

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementsInProgressSince(
                It.Is<DateTimeOffset>(x => Math.Abs((DateTimeOffset.Now - x).TotalMinutes) < 10)))
                .ReturnsAsync(ids);

            mockStorage.Setup(ctx => ctx.ResetUpdateTaskElement(
              It.Is<Int32>(x => copiedIds.Contains(x) == true)))
              .ReturnsAsync<Int32, IBesaDataStorage, Boolean>(x =>
              {
                  copiedIds.Remove(x);
                  return true;
              });

            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementOverviewById(It.Is<Int32>(x => ids.Contains(x)))).
                ReturnsAsync<Int32,IBesaDataStorage,UpdateTaskElementRawOverviewModel>( x => taskElementsModels[x]);

            Int32 notifierCallerCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);

            notifier.Setup(noti => noti.TaskElementChanged(It.Is<UpdateTaskElementOverviewModel>(x => taskElementsModels.ContainsKey(x.Id) == true 
                
            ))).Returns(Task.FromResult(true)).Callback(() => notifierCallerCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            Boolean actual = await service.RestoreLockedUpdateTaskElements();
            Assert.True(actual);
            Assert.True(copiedIds.Count == 0);
            Assert.Equal(ids.Count, notifierCallerCounter);
        }
    }
}
