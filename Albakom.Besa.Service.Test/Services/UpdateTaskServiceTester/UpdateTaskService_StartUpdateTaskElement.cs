﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_StartUpdateTaskElement
    {
        private static void PrepareStorage(Int32 elementId, bool expectedResult, Mock<IBesaDataStorage> mockStorage)
        {
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.SetUpdateTaskElementAsProcessing(elementId)).ReturnsAsync(expectedResult);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskElementOverviewById(elementId)).ReturnsAsync(new UpdateTaskElementRawOverviewModel { Id = elementId });
        }

        [Fact(DisplayName = "StartUpdateTaskElement_Pass|UpdateTaskServiceTester")]
        public async Task StartUpdateTaskElement_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(elementId, expectedResult, mockStorage);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            Boolean actual = await service.StartUpdateTaskElement(elementId);

            Assert.Equal(expectedResult, actual);
        }


        [Fact(DisplayName = "StartUpdateTaskElement_WithNotifier_Pass|UpdateTaskServiceTester")]
        public async Task StartUpdateTaskElement_WithNotifier_Pass()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(elementId, expectedResult, mockStorage);

            Int32 notifierCallbackCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            notifier.Setup(noti => noti.TaskElementChanged(It.Is<UpdateTaskElementOverviewModel>(x => x.Id == elementId))).Returns(Task.FromResult(true)).Callback(() => notifierCallbackCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            Boolean actual = await service.StartUpdateTaskElement(elementId);

            Assert.Equal(expectedResult, actual);
            Assert.Equal(1, notifierCallbackCounter);
        }

        [Fact(DisplayName = "StartUpdateTaskElement_Fail_ElementNotFound|UpdateTaskServiceTester")]
        public async Task StartUpdateTaskElement_Fail_ElementNotFound()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();
            Boolean expectedResult = rand.NextDouble() > 0.5;

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            PrepareStorage(elementId, expectedResult, mockStorage);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.StartUpdateTaskElement(elementId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.ElementNotFound, exception.Reason);
        }
    }
}
