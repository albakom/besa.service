﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_UnlockedTaskWithDeadlock
    {
        [Fact(DisplayName = "UnlockedTaskWithDeadlock_Pass|UpdateTaskServiceTester")]
        public async Task UnlockedTaskWithDeadlock_Pass()
        {
            Random rand = new Random();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            List<UpdateTasks> expectedTaskTypes = new List<UpdateTasks>
            {
                UpdateTasks.Cables,
                UpdateTasks.MainCableForBranchable,
                UpdateTasks.MainCableForPops,
                UpdateTasks.PatchConnections,
                UpdateTasks.Splices
            };

            Dictionary<UpdateTasks, Boolean> lockedStates = expectedTaskTypes.ToDictionary(x => x, x => rand.NextDouble() > 0.5);
            Dictionary<UpdateTasks, Int32> taskIds = lockedStates.Where(x => x.Value == true).ToDictionary(x => x.Key, x => rand.Next());

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfTaskHeartBeatIsOlderThen(It.Is<DateTimeOffset>(
                x => Math.Abs((DateTimeOffset.Now - x).TotalMinutes) <= 10),
                It.Is<UpdateTasks>(x => expectedTaskTypes.Contains(x))))
                .ReturnsAsync<DateTimeOffset, UpdateTasks, IBesaDataStorage, Boolean>((x, y) => lockedStates[y]);

            mockStorage.Setup(ctx => ctx.GetUpdateTaskIdByTaskTye(
              It.Is<UpdateTasks>(x => taskIds.ContainsKey(x))))
              .ReturnsAsync<UpdateTasks, IBesaDataStorage, Int32>((x) => taskIds[x]);

            mockStorage.Setup(ctx => ctx.SetLockStateOfTask(
  It.Is<Int32>(x => taskIds.ContainsValue(x)), false)).ReturnsAsync(true);

            Int32 notifierCallbackCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            notifier.Setup(noti => noti.TaskLockDetected(It.Is<Int32>(x => taskIds.ContainsValue(x)))).Returns(Task.FromResult(true)).Callback(() => notifierCallbackCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            IEnumerable<Int32> actual = await service.UnlockedTaskWithDeadlock();

            Assert.Equal(taskIds.Values.OrderBy(x => x), actual.OrderBy(x => x));
            Assert.Equal(taskIds.Count, notifierCallbackCounter);

        }
    }
}
