﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Albakom.Besa.Service.Test.Services.UpdateTaskServiceTester
{
    public class UpdateTaskService_UpdateHeartbeatByTaskElementId
    {
        [Fact(DisplayName = "UpdateHeartbeatByTaskElementId_Pass|UpdateTaskServiceTester")]
        public async Task UpdateHeartbeatByTaskElementId_Pass()
        {
            Random rand = new Random();

            Int32 elementId = rand.Next();
            Int32 taskId = rand.Next();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUpdateTaskIdByElementId(elementId)).ReturnsAsync(taskId);
            mockStorage.Setup(ctx => ctx.UpdateHeartbeatByTaskId(taskId, It.Is<DateTimeOffset>(x =>
            Math.Abs((x - DateTimeOffset.Now).TotalMinutes) <= 2))).ReturnsAsync(true);

            Int32 notifierCallbackCounter = 0;
            var notifier = new Mock<IUpdateTaskNotifier>(MockBehavior.Strict);
            notifier.Setup(noti => noti.HeartbeatChanged(It.Is<UpdateTaskHeartbeatChangedModel>(x => x.TaskId == taskId &&
            (DateTimeOffset.Now - x.Value).TotalSeconds < 60 ))).Returns(Task.FromResult(true)).Callback(() => notifierCallbackCounter++);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, notifier.Object);
            DateTimeOffset actual = await service.UpdateHeartbeatByTaskElementId(elementId);

            Assert.True(Math.Abs((actual - DateTimeOffset.Now).TotalMinutes) <= 2);
            Assert.Equal(1, notifierCallbackCounter);
        }

        [Fact(DisplayName = "UpdateHeartbeatByTaskElementId_Failed_ElementNotFound|UpdateTaskServiceTester")]
        public async Task UpdateHeartbeatByTaskElementId_Failed_ElementNotFound()
        {
            Random rand = new Random();
            Int32 elementId = rand.Next();

            var logger = Mock.Of<ILogger<UpdateTaskService>>();
            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);

            mockStorage.Setup(ctx => ctx.CheckIfUpdateTaskElementExists(elementId)).ReturnsAsync(false);

            IUpdateTaskService service = new UpdateTaskService(mockStorage.Object, logger, Mock.Of<IUpdateTaskNotifier>());
            UpdateTaskServiceException exception = await Assert.ThrowsAsync<UpdateTaskServiceException>(() => service.UpdateHeartbeatByTaskElementId(elementId));
            Assert.Equal(UpdateTaskServiceExceptionReasons.ElementNotFound, exception.Reason);
        }

    }
}
