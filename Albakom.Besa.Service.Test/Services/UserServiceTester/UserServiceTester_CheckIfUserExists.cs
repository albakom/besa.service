using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_CheckIfUserExists
    {
        #region CheckIfUserExists

        [Fact]
        public async Task CheckIfUserExists_Pass()
        {
            String userId = (new Guid()).ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();


            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            Boolean result = await userService.CheckIfUserExists(userId);
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfUserExists_Failed()
        {
            Random rand = new Random();
            String userId = (new Guid()).ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            Boolean result = await userService.CheckIfUserExists(userId);
            Assert.False(result);
        }

        [Fact]
        public async Task CheckIfUserExistsWithNullString_Failed()
        {
            Random rand = new Random();
            String userId = (new Guid()).ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            String[] emptyStrings = new string[] { null, "", String.Empty };

            foreach (String testString in emptyStrings)
            {
                Boolean result = await userService.CheckIfUserExists(testString);
                Assert.False(result);
            }
        }

        #endregion

        #region CheckUserRoles

        #endregion

    }
}
