﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_CheckIfUserHasRole
    {
        [Fact]
        public async Task CheckIfUserHasRole_Pass()
        {
            String userId = (new Guid()).ToString();

            BesaRoles existingRoles = BesaRoles.ProjectManager | BesaRoles.Admin;
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            Dictionary<BesaRoles, Boolean> exceptedResults = new Dictionary<BesaRoles, Boolean>();
            exceptedResults[BesaRoles.Admin] = (existingRoles & BesaRoles.Admin) == BesaRoles.Admin;
            exceptedResults[BesaRoles.AirInjector] = (existingRoles & BesaRoles.AirInjector) == BesaRoles.AirInjector;
            exceptedResults[BesaRoles.CivilWorker] = (existingRoles & BesaRoles.CivilWorker) == BesaRoles.CivilWorker;
            exceptedResults[BesaRoles.ProjectManager] = (existingRoles & BesaRoles.ProjectManager) == BesaRoles.ProjectManager;
            exceptedResults[BesaRoles.Unknown] = false;

            foreach (var expectedResult in exceptedResults)
            {
                var mockStorage = new Mock<IBesaDataStorage>();
                mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
                mockStorage.Setup(ctx => ctx.CheckIfUserHasRole(userId, expectedResult.Key)).ReturnsAsync(expectedResult.Value);

                IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
                Boolean result = await userService.CheckIfUserHasRole(userId, expectedResult.Key);
                Assert.Equal(expectedResult.Value, result);
            }
        }

        [Fact]
        public async Task CheckIfUserHasRole_ThrowUserNotFound()
        {
            String userId = (new Guid()).ToString();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.CheckIfUserHasRole(userId, BesaRoles.Admin));
            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }

    }
}
