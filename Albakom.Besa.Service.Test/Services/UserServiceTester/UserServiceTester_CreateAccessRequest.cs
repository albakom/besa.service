﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_CreateAccessRequest
    {
        [Fact]
        public async Task CheckIfCreateAccessRequest_Pass()
        {
            Random random = new Random();

            String AuthServiceId = (new Guid()).ToString();
            Int32 requestId = random.Next();

            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateUserAccessRequest(It.IsAny<UserAccessRequestCreateModel>())).ReturnsAsync(requestId);

            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           x.Action == MessageActions.Create &&
           x.RelatedType == MessageRelatedObjectTypes.UserRequests &&
           x.RelatedObjectId == requestId
            ), AuthServiceId)).ReturnsAsync(random.Next());

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, mockProtocolService.Object);

            UserAccessRequestCreateModel accessRequestCreateModel = new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            };

            await userService.CreateAccessRequest(accessRequestCreateModel);
        }

        [Fact]
        public async Task CheckIfCreateAccessRequest_WithCompany_Pass()
        {
            String AuthServiceId = (new Guid()).ToString();
            Int32 companyId = 4;
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();


            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserAccessRequestCreateModel accessRequestCreateModel = new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = companyId,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            };

            await userService.CreateAccessRequest(accessRequestCreateModel);
        }

        [Fact]
        public async Task CheckIfCreateAccessRequest_FailRequestExists()
        {
            String AuthServiceId = (new Guid()).ToString();
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserAccessRequestCreateModel accessRequestCreateModel = new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            };

            UserServiceException requestExists = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(accessRequestCreateModel));
            Assert.Equal(UserServiceExceptionReasons.AccessRequestAlreadyExists, requestExists.Reason);
        }

        [Fact]
        public async Task CheckIfCreateAccessRequest_ThrowInvalidArgument()
        {
            String AuthServiceId = (new Guid()).ToString();
            Int32 companyId = 4;
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException nullException = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(null));
            Assert.Equal(UserServiceExceptionReasons.UserAcccessRequestIsNull, nullException.Reason);

            List<UserAccessRequestCreateModel> invalidModels = new List<UserAccessRequestCreateModel>();
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                // AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                // EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                // Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                //  Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                // Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = companyId,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });

            foreach (UserAccessRequestCreateModel invalidRequest in invalidModels)
            {
                UserServiceException invalidException = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(invalidRequest));
                Assert.Equal(UserServiceExceptionReasons.InvalidAccessRequestModel, invalidException.Reason);
            }
        }
    }
}
