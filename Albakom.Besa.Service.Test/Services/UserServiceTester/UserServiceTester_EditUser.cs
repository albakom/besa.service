﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_EditUser
    {
        [Fact]
        public async Task EditUser_Pass()
        {
            Random random = new Random();
            Int32 userId = random.Next();
            String authId = Guid.NewGuid().ToString();
            BesaRoles role = BesaRoles.All;
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            UserEditModel editModel = new UserEditModel
            {
                Id = userId,
                EMailAddress = "neueMail@test.de",
                Lastname = "Vorname 1",
                Surname = "Nachname 2",
                Phone = "015782-487501",
                Role = role,
            };

            UserOverviewModel userOverview = new UserOverviewModel
            {
                Id = userId,
                EMailAddress = editModel.EMailAddress,
                IsMember = true,
                Lastname = editModel.Lastname,
                Surname = editModel.Surname,
                Phone = editModel.Phone,
            };

            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByUserId(userId)).ReturnsAsync(authId);
            mockStorage.Setup(ctx => ctx.GetUserRoleById(userId)).ReturnsAsync(role);
            mockStorage.Setup(ctx => ctx.GetUserOverviewById(userId)).ReturnsAsync(userOverview);

            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.Update &&
                x.RelatedType == MessageRelatedObjectTypes.User &&
                x.RelatedObjectId == userId
                ), userAuthId)).ReturnsAsync(random.Next());

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, mockProtocolService.Object);

            UserOverviewModel result = await userService.EditUser(editModel, userAuthId);

            Assert.NotNull(result);

            Assert.Equal(userOverview.Id, result.Id);
            Assert.Equal(userOverview.IsMember, result.IsMember);
            Assert.Equal(userOverview.Lastname, result.Lastname);
            Assert.Equal(userOverview.Phone, result.Phone);
            Assert.Equal(userOverview.Surname, result.Surname);
        }

        [Fact]
        public async Task EditUser_Failed_UserNotExists()
        {
            Random random = new Random();
            Int32 userId = random.Next();
            String authId = Guid.NewGuid().ToString();
            BesaRoles role = BesaRoles.All;
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            UserEditModel editModel = new UserEditModel
            {
                Id = userId,
                EMailAddress = "neueMail@test.de",
                Lastname = "Vorname 1",
                Surname = "Nachname 2",
                Phone = "015782-487501",
                Role = role,
            };

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);

            String userAuthId = Guid.NewGuid().ToString();

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.EditUser(editModel, userAuthId));

            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }

        /*
        [Fact]
        public async Task CheckIfCreateAccessRequest_FailRequestExists()
        {
            String AuthServiceId = (new Guid()).ToString();
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger);

            UserAccessRequestCreateModel accessRequestCreateModel = new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            };

            UserServiceException requestExists = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(accessRequestCreateModel));
            Assert.Equal(UserServiceExceptionReasons.AccessRequestAlreadyExists, requestExists.Reason);
        }

        [Fact]
        public async Task CheckIfCreateAccessRequest_ThrowInvalidArgument()
        {
            String AuthServiceId = (new Guid()).ToString();
            Int32 companyId = 4;
            IStorageContraints contraints = new BesaDataStorageContraints();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(contraints);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(AuthServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger);

            UserServiceException nullException = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(null));
            Assert.Equal(UserServiceExceptionReasons.UserAcccessRequestIsNull, nullException.Reason);

            List<UserAccessRequestCreateModel> invalidModels = new List<UserAccessRequestCreateModel>();
            invalidModels.Add(new UserAccessRequestCreateModel
            {
               // AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
               // EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
               // Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
              //  Surname = "Max",
                Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = null,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
               // Phone = "0156450",
            });
            invalidModels.Add(new UserAccessRequestCreateModel
            {
                AuthServiceId = AuthServiceId,
                CompanyId = companyId,
                EMailAddress = "blub@blub.de",
                GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(3)),
                Lastname = "Mustermann",
                Surname = "Max",
                Phone = "0156450",
            });

            foreach (UserAccessRequestCreateModel invalidRequest in invalidModels)
            {
                UserServiceException invalidException = await Assert.ThrowsAsync<UserServiceException>(() => userService.CreateAccessRequest(invalidRequest));
                Assert.Equal(UserServiceExceptionReasons.InvalidAccessRequestModel, invalidException.Reason);
            }
        } */
    }
}
