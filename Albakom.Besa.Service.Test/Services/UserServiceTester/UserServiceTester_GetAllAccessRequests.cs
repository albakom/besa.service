﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetAllAccessRequests
    {
        [Fact]
        public async Task GetAllAccessRequests_Pass()
        {
            Random random = new Random();
            Int32 amount = random.Next(30, 100);

            Dictionary<Int32, UserAccessRequestOverviewModel> expectedResult = new Dictionary<int, UserAccessRequestOverviewModel>();

            for (int i = 0; i < amount; i++)
            {
                UserAccessRequestOverviewModel model = new UserAccessRequestOverviewModel
                {
                    Id = i,
                    EMailAddress = $"test-{i}@test.de",
                    Lastname = $"Nachmane {i}",
                    Surname = $"Vorname {i}",
                    GeneratedAt = DateTimeOffset.Now.Subtract(TimeSpan.FromMinutes(random.Next(10, 1000))),
                    Phone = $"01545456 {i}",
                };

                if (random.NextDouble() > 0.5)
                {
                    SimpleCompanyOverviewModel companyModel = new SimpleCompanyOverviewModel
                    {
                        Id = i,
                        Name = $"Testfirma-{i}",

                    };

                    model.CompanyInfo = companyModel;
                }

                expectedResult.Add(model.Id, model);
            }

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.GetAllUserAccessRequest()).ReturnsAsync(expectedResult.Values);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            IEnumerable<UserAccessRequestOverviewModel> actual = await userService.GetAllAccessRequests();

            Assert.NotNull(actual);
            Assert.Equal(expectedResult.Count, actual.Count());

            foreach (UserAccessRequestOverviewModel actualItem in actual)
            {
                Assert.NotNull(actualItem);

                Assert.True(expectedResult.ContainsKey(actualItem.Id));
                UserAccessRequestOverviewModel expectedItem = expectedResult[actualItem.Id];

                Assert.Equal(expectedItem.Id, actualItem.Id);
                Assert.Equal(expectedItem.EMailAddress, actualItem.EMailAddress);
                Assert.Equal(expectedItem.GeneratedAt, actualItem.GeneratedAt);
                Assert.Equal(expectedItem.Lastname, actualItem.Lastname);
                Assert.Equal(expectedItem.Surname, actualItem.Surname);

                if(expectedItem.CompanyInfo == null)
                {
                    Assert.Null(actualItem.CompanyInfo);
                }
                else
                {
                    Assert.Equal(expectedItem.CompanyInfo.Id, actualItem.CompanyInfo.Id);
                    Assert.Equal(expectedItem.CompanyInfo.Name, actualItem.CompanyInfo.Name);
                }

                Assert.Equal(expectedItem.EMailAddress, actualItem.EMailAddress);
            }
        }

      
    }
}
