﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetJobsForMember
    {
        [Fact]
        public async Task GetJobsForMember_Pass()
        {
            Random random = new Random();
            Int32 amount = random.Next(30, 100);

            String userId = Guid.NewGuid().ToString();

            List<PersonalJobOverviewModel> jobs = new List<PersonalJobOverviewModel>(amount);
            for (int i = 0; i < amount; i++)
            {
                PersonalJobOverviewModel item = new PersonalJobOverviewModel
                {
                    Name = $"Aufgabe Nr. {random.Next()}",
                    EndDate = DateTimeOffset.Now.AddDays(random.Next(3, 10)),
                    CollectionJobId = i + 1,
                };

                if (random.NextDouble() > 0.5)
                {
                    item.TaskAmount = random.Next(4, 10);
                    item.CollectionJobId = random.Next();
                }
                else
                {
                    item.TaskAmount = 1;
                    item.JobId = random.Next();
                }
            }

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserHasCompany(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetJobOverviewForUser(userId)).ReturnsAsync(jobs);


            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            IEnumerable<PersonalJobOverviewModel> actual = await userService.GetJobsForMember(userId);

            Assert.NotNull(actual);
            Assert.Equal(jobs.Count, actual.Count());

            foreach (PersonalJobOverviewModel actualItem in actual)
            {
                Assert.NotNull(actualItem);
                PersonalJobOverviewModel expectedItem = jobs.FirstOrDefault(x => x.CollectionJobId == actualItem.CollectionJobId);

                Assert.NotNull(expectedItem);

                Assert.Equal(expectedItem.CollectionJobId, actualItem.CollectionJobId);
                Assert.Equal(expectedItem.JobId, actualItem.JobId);
                Assert.Equal(expectedItem.EndDate, actualItem.EndDate);
                Assert.Equal(expectedItem.Name, actualItem.Name);
                Assert.Equal(expectedItem.TaskAmount, actualItem.TaskAmount);
                Assert.Equal(expectedItem.JobType, actualItem.JobType);
                Assert.Equal(expectedItem.CollectionJobId, actualItem.CollectionJobId);
            }
        }

        [Fact]
        public async Task GetJobsForMember_Fail_UserNotFound()
        {
            String userId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserHasCompany(userId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.GetJobOverviewForUser(userId)).ReturnsAsync(new List<PersonalJobOverviewModel>());

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GetJobsForMember(userId));

            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }

        [Fact]
        public async Task GetJobsForMember_Fail_UserNotNoCompany()
        {
            String userId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserHasCompany(userId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetJobOverviewForUser(userId)).ReturnsAsync(new List<PersonalJobOverviewModel>());

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GetJobsForMember(userId));

            Assert.Equal(UserServiceExceptionReasons.BelongsToNoCompany, exception.Reason);
        }
    }
}
