﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetMemberStatus
    {
        [Fact]  
        public async Task GetMemberStatus_Member_Pass()
        {
            String authServiceId = (new Guid()).ToString();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authServiceId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            MemberStatus status = await userService.GetMemberStatus(authServiceId);

            Assert.Equal(MemberStatus.IsMember, status);
        }

        [Fact]
        public async Task GetMemberStatus_NoMember_Pass()
        {
            String authServiceId = (new Guid()).ToString();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(authServiceId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            MemberStatus status = await userService.GetMemberStatus(authServiceId);

            Assert.Equal(MemberStatus.NoMember, status);
        }

        [Fact]
        public async Task GetMemberStatus_Outstanding_Pass()
        {
            String authServiceId = (new Guid()).ToString();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authServiceId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(authServiceId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            MemberStatus status = await userService.GetMemberStatus(authServiceId);

            Assert.Equal(MemberStatus.RequestedOutstanding, status);
        }

        [Fact]
        public async Task GetMemberStatus_Fail()
        {
            String authServiceId = (new Guid()).ToString();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            String[] emptyString = new String[] { null, "", String.Empty };

            foreach (String item in emptyString)
            {
                UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>( () => userService.GetMemberStatus(item));
                Assert.Equal(UserServiceExceptionReasons.NoAuthId, exception.Reason);
            }
        }
    }
}
