﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetUserDataForEdit
    {
        [Fact]
        public async Task GetUserDataForEdit_Pass()
        {
            Random random = new Random();
            Int32 userId = random.Next();
            String authId = Guid.NewGuid().ToString();
            BesaRoles role = BesaRoles.All;
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            UserEditModel data = new UserEditModel
            {
                Id = userId,
                EMailAddress = "neueMail@test.de",
                Lastname = "Vorname 1",
                Surname = "Nachname 2",
                Phone = "015782-487501",
                Role = role,
            };

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserDataForEdit(userId)).ReturnsAsync(data);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserEditModel actual = await userService.GetUserDataForEdit(userId);

            Assert.NotNull(actual);

            Assert.Equal(data.Id, actual.Id);
            Assert.Equal(data.Lastname, actual.Lastname);
            Assert.Equal(data.Phone, actual.Phone);
            Assert.Equal(data.Surname, actual.Surname);
            Assert.Equal(data.CompanyId, actual.CompanyId);
            Assert.Equal(data.Role, actual.Role);
            Assert.Equal(data.EMailAddress, actual.EMailAddress);
        }

        [Fact]
        public async Task GetUserDataForEdit_Fail_UserNotFound()
        {
            Random random = new Random();
            Int32 userId = random.Next();

            var mockStorage = new Mock<IBesaDataStorage>(MockBehavior.Strict);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GetUserDataForEdit(userId));

            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
