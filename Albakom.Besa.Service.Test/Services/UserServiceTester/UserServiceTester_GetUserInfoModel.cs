﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetUserInfoModel
    {
        [Fact(DisplayName = "GetUserInfoModel_Pass|UserServiceTester")]
        public async Task GetUserInfoModel_Pass()
        {
            Random random = new Random();
            String userId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            UserInfoModel expected = new UserInfoModel
            {
                Surname = $"Vorname {random.Next()}",
                Lastname = $"Nachname {random.Next()}",
                Roles = BesaRoles.AirInjector | BesaRoles.CivilWorker | BesaRoles.Admin,
                IsManagement = random.NextDouble() > 0.5,
                Company = new SimpleCompanyOverviewModel
                {
                    Id = random.Next(),
                    Name = $"Firmenname {random.Next()}",
                }
            };

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserInfoModel(userId)).ReturnsAsync(expected);

            IUserService userService = new UserService(mockStorage.Object,notifier, logger, Mock.Of<IProtocolService>());
            UserInfoModel actual = await userService.GetUserInfoModel(userId);

            Assert.NotNull(actual);
            Assert.Equal(expected.Surname, actual.Surname);
            Assert.Equal(expected.Lastname, actual.Lastname);
            Assert.Equal(expected.Roles, actual.Roles);
            Assert.Equal(expected.IsManagement, actual.IsManagement);

            Assert.NotNull(actual.Company);
            Assert.Equal(expected.Company.Id, actual.Company.Id);
            Assert.Equal(expected.Company.Name, actual.Company.Name);
        }

        [Fact(DisplayName = "GetUserInfoModel_Fail_UserNotFound|UserServiceTester")]
        public async Task GetUserInfoModel_Fail_UserNotFound()
        {
            String userId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GetUserInfoModel(userId));
            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
