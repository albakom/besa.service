﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GetUserRole
    {
        [Fact]
        public async Task GetUserrole_Pass()
        {
            String userId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            BesaRoles roles = BesaRoles.AirInjector | BesaRoles.CivilWorker | BesaRoles.Admin;

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetUserRoles(userId)).ReturnsAsync(roles);

            IUserService userService = new UserService(mockStorage.Object,notifier, logger, Mock.Of<IProtocolService>());
            BesaRoles actual = await userService.GetRolesByAuthServiceId(userId);
            Assert.Equal(roles, actual);
        }

        [Fact]
        public async Task GetUserrole_Fail()
        {
            String userId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            BesaRoles roles = BesaRoles.AirInjector | BesaRoles.CivilWorker | BesaRoles.Admin;

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.GetUserRoles(userId)).ReturnsAsync(roles);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GetRolesByAuthServiceId(userId));

            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }
    }
}
