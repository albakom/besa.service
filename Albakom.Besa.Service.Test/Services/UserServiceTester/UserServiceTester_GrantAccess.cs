﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_GrantAccess
    {
        [Fact]
        public async Task GrantAccess_Pass()
        {
            Int32 requestId = 2;
            Int32 createUserId = 35;
            String authid = (new Guid()).ToString();

            GrantUserAccessModel model = new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            };

            Int32 notifierAmount = 0;

            var logger = Mock.Of<ILogger<UserService>>();
            var mockNotifier = new Mock<IAccessRequestNotifier>();
            mockNotifier.Setup(noti => noti.AccessGranted(authid)).Returns(() =>
           {
               notifierAmount++;
               return Task.CompletedTask;
           });
            String userAuthId = Guid.NewGuid().ToString();

            Random random = new Random();

            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
           (x.Action == MessageActions.Create &&
           x.RelatedType == MessageRelatedObjectTypes.User &&
           x.RelatedObjectId == createUserId) ||
           (x.Action == MessageActions.Delete &&
           x.RelatedType == MessageRelatedObjectTypes.UserRequests &&
           x.RelatedObjectId == requestId)
            ), userAuthId)).ReturnsAsync(random.Next());

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestId)).ReturnsAsync(authid);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authid)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CreateUser(It.IsAny<UserCreateModel>())).ReturnsAsync(createUserId);
            mockStorage.Setup(ctx => ctx.DeleteAuthRequest(requestId)).Returns(Task.CompletedTask);

            IUserService userService = new UserService(mockStorage.Object, mockNotifier.Object, logger, mockProtocolService.Object);
            Int32 actualId = await userService.GrantAccess(model, userAuthId);

            Assert.Equal(createUserId, actualId);
            Assert.Equal(1, notifierAmount);
        }

        [Fact]
        public async Task GrantAccess_WithCompanyID_Pass()
        {
            Int32 requestId = 2;
            Int32 companyId = 12;
            Int32 createUserId = 35;
            String authid = (new Guid()).ToString();

            GrantUserAccessModel model = new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            };

            var logger = Mock.Of<ILogger<UserService>>();
            var mockStorage = new Mock<IBesaDataStorage>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            String userAuthId = Guid.NewGuid().ToString();


            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestId)).ReturnsAsync(authid);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authid)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CreateUser(It.IsAny<UserCreateModel>())).ReturnsAsync(createUserId);
            mockStorage.Setup(ctx => ctx.DeleteAuthRequest(requestId)).Returns(Task.CompletedTask);


            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            Int32 actualId = await userService.GrantAccess(model, userAuthId);

            Assert.Equal(createUserId, actualId);
        }

        [Fact]
        public async Task GrantAccess_Fail()
        {
            Int32 requestId = 2;
            Int32 requestIdWithExistingUser = 42;
            Int32 companyId = 12;
            Int32 createUserId = 35;
            String authid = (new Guid()).ToString();
            String existingAuthid = Guid.NewGuid().ToString();
            String userAuthId2 = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestIdWithExistingUser)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authid)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId2)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(existingAuthid)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestId)).ReturnsAsync(authid);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestIdWithExistingUser)).ReturnsAsync(existingAuthid);
            mockStorage.Setup(ctx => ctx.CreateUser(It.IsAny<UserCreateModel>())).ReturnsAsync(createUserId);
            mockStorage.Setup(ctx => ctx.DeleteAuthRequest(requestId)).Returns(Task.CompletedTask);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            Dictionary<GrantUserAccessModel, UserServiceExceptionReasons> itemsToFail = new Dictionary<GrantUserAccessModel, UserServiceExceptionReasons>();
            itemsToFail.Add(new GrantUserAccessModel(), UserServiceExceptionReasons.GrantAccessIsNull);
            itemsToFail.Add(new GrantUserAccessModel
            {
                // AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.AccessRequestNotFound);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                // EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                // Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                // Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                // Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                // Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId + 4,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestIdWithExistingUser,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.UserAlreadyExists);

            String userAuthId = String.Empty;

            Boolean isFisrt = true;
            foreach (var itemToFail in itemsToFail)
            {
                GrantUserAccessModel accessModel = itemToFail.Key;
                if (isFisrt == true)
                {
                    accessModel = null;
                    isFisrt = false;
                }

                UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GrantAccess(accessModel, userAuthId2));
                Assert.Equal(itemToFail.Value, exception.Reason);
            }
        }
    }
}
