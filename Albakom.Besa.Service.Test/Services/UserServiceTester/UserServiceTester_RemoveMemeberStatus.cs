﻿using Albakom.Besa.Service.Contracts.Models;
using Albakom.Besa.Service.Contracts.Models.Exceptions;
using Albakom.Besa.Service.Contracts.Service;
using Albakom.Besa.Service.DataAccess.Context;
using Albakom.Besa.Service.DataAccess.Models;
using Albakom.Besa.Service.Implementation.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.Service.Test.Services.UserServiceTester
{
    public class UserServiceTester_RemoveMemeberStatus
    {
        [Fact]
        public async Task RemoveMemeberStatus_Pass()
        {
            Random random = new Random();

            Int32 userId = random.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfHasAuthId(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveAuthIdFromUser(userId)).ReturnsAsync(true);

            var mockProtocolService = new Mock<IProtocolService>(MockBehavior.Strict);
            mockProtocolService.Setup(svc => svc.CreateProtocolEntry(It.Is<ProtocolEntyCreateModel>(x =>
                x.Action == MessageActions.RemoveMemberState &&
                x.RelatedType == MessageRelatedObjectTypes.User &&
                x.RelatedObjectId == userId
                ), userAuthId)).ReturnsAsync(random.Next());

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, mockProtocolService.Object);
            Boolean result = await userService.RemoveMemberStatus(userId, userAuthId);

            Assert.True(result);
        }

        [Fact]
        public async Task RemoveMemeberStatus_Fail_UserAuthIdFound()
        {
            Random random = new Random();

            Int32 userId = random.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfHasAuthId(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveAuthIdFromUser(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(false);


            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.RemoveMemberStatus(userId, userAuthId));
            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }

        [Fact]
        public async Task RemoveMemeberStatus_Fail_UserNotFound()
        {
            Random random = new Random();

            Int32 userId = random.Next();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfHasAuthId(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveAuthIdFromUser(userId)).ReturnsAsync(true);

            String userAuthId = Guid.NewGuid().ToString();

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.RemoveMemberStatus(userId, userAuthId));
            Assert.Equal(UserServiceExceptionReasons.UserNotFound, exception.Reason);
        }

        [Fact]
        public async Task RemoveMemeberStatus_Fail_UserNotMember()
        {
            Random random = new Random();

            Int32 userId = random.Next();
            String userAuthId = Guid.NewGuid().ToString();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfHasAuthId(userId)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.RemoveAuthIdFromUser(userId)).ReturnsAsync(true);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.RemoveMemberStatus(userId, userAuthId));
            Assert.Equal(UserServiceExceptionReasons.UserAlreadySuspended, exception.Reason);
        }

        [Fact]
        public async Task RemoveMemeberStatus_Fail_Database()
        {
            Random random = new Random();

            Int32 userId = random.Next();

            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            String userAuthId = Guid.NewGuid().ToString();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfHasAuthId(userId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.RemoveAuthIdFromUser(userId)).ReturnsAsync(false);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            Boolean result = await userService.RemoveMemberStatus(userId, userAuthId);
            Assert.False(result);
        }

        [Fact]
        public async Task GrantAccess_WithCompanyID_Pass()
        {
            Int32 requestId = 2;
            Int32 companyId = 12;
            Int32 createUserId = 35;
            String authid = (new Guid()).ToString();
            String userAuthId = Guid.NewGuid().ToString();
            String userAuthId2 = Guid.NewGuid().ToString();

            GrantUserAccessModel model = new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            };

            var logger = Mock.Of<ILogger<UserService>>();
            var mockStorage = new Mock<IBesaDataStorage>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestId)).ReturnsAsync(authid);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authid)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId2)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CreateUser(It.IsAny<UserCreateModel>())).ReturnsAsync(createUserId);
            mockStorage.Setup(ctx => ctx.DeleteAuthRequest(requestId)).Returns(Task.CompletedTask);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());
            Int32 actualId = await userService.GrantAccess(model, userAuthId2);

            Assert.Equal(createUserId, actualId);
        }

        [Fact]
        public async Task GrantAccess_Fail()
        {
            Int32 requestId = 2;
            Int32 requestIdWithExistingUser = 42;
            Int32 companyId = 12;
            Int32 createUserId = 35;
            String authid = (new Guid()).ToString();
            String existingAuthid = Guid.NewGuid().ToString();
            String userAuthId = Guid.NewGuid().ToString();
            String userAuthId2 = Guid.NewGuid().ToString();


            var logger = Mock.Of<ILogger<UserService>>();
            var notifier = Mock.Of<IAccessRequestNotifier>();

            var mockStorage = new Mock<IBesaDataStorage>();
            mockStorage.SetupGet(ctx => ctx.Constraints).Returns(new BesaDataStorageContraints());
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserAccessRequestExists(requestIdWithExistingUser)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfCompanyExists(companyId)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(authid)).ReturnsAsync(false);
            mockStorage.Setup(ctx => ctx.CheckIfUserExists(userAuthId2)).ReturnsAsync(true);

            mockStorage.Setup(ctx => ctx.CheckIfUserExists(existingAuthid)).ReturnsAsync(true);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestId)).ReturnsAsync(authid);
            mockStorage.Setup(ctx => ctx.GetAuthServiceIdByAccessRequestId(requestIdWithExistingUser)).ReturnsAsync(existingAuthid);
            mockStorage.Setup(ctx => ctx.CreateUser(It.IsAny<UserCreateModel>())).ReturnsAsync(createUserId);
            mockStorage.Setup(ctx => ctx.DeleteAuthRequest(requestId)).Returns(Task.CompletedTask);

            IUserService userService = new UserService(mockStorage.Object, notifier, logger, Mock.Of<IProtocolService>());

            Dictionary<GrantUserAccessModel, UserServiceExceptionReasons> itemsToFail = new Dictionary<GrantUserAccessModel, UserServiceExceptionReasons>();
            itemsToFail.Add(new GrantUserAccessModel(), UserServiceExceptionReasons.GrantAccessIsNull);
            itemsToFail.Add(new GrantUserAccessModel
            {
                // AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.AccessRequestNotFound);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                // EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                // Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                // Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                // Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                // Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestId,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId + 4,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.InvalidGrantAccessModel);
            itemsToFail.Add(new GrantUserAccessModel
            {
                AuthRequestId = requestIdWithExistingUser,
                EMailAddress = "blub@blub.de",
                CompanyId = companyId,
                Lastname = "nachname",
                Surname = "vorname",
                Phone = "005456",
                Role = BesaRoles.AirInjector | BesaRoles.AirInjector,
            }, UserServiceExceptionReasons.UserAlreadyExists);


            Boolean isFisrt = true;
            foreach (var itemToFail in itemsToFail)
            {
                GrantUserAccessModel accessModel = itemToFail.Key;
                if (isFisrt == true)
                {
                    accessModel = null;
                    isFisrt = false;
                }

                UserServiceException exception = await Assert.ThrowsAsync<UserServiceException>(() => userService.GrantAccess(accessModel, userAuthId2));
                Assert.Equal(itemToFail.Value, exception.Reason);
            }
        }
    }
}
